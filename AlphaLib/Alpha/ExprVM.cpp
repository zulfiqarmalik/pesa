/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// ExprVM.cpp
///
/// Created on: 22 Jun 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "VirtualMachine/Lang/Parser.h"
#include "VirtualMachine/VM/SimpleExpression.h"

#include "Core/Lib/Profiler.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Expression Virtual Machine
	////////////////////////////////////////////////////////////////////////////
	class ExprVM : public IAlpha, vm::SimpleExpression {
	protected:
		typedef std::vector<IAlpha*> IAlphaPVec;
		IAlphaPVec				m_alphaStack;
		RuntimeData				m_rt;
		bool					m_noApplyDelay = false;

		virtual DataPtr			getVar(size_t index);

	public:
								ExprVM(const ConfigSection& config);
								ExprVM(const ConfigSection& config, const char* logChannel, const std::string& expr);
		virtual 				~ExprVM();

		////////////////////////////////////////////////////////////////////////////
		/// IAlpha overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			run(const RuntimeData& rt);
		//virtual std::string 	uuid() const;

		////////////////////////////////////////////////////////////////////////////
		/// vm::SimpleExpression overrides
		////////////////////////////////////////////////////////////////////////////
		IDataRegistry*			dataRegistry();
		virtual void			prepareAlpha(IAlpha* alpha);
		virtual IUniverse* 		universe();
		virtual const ConfigSection& getConfig();
		virtual IAlpha*			alpha();
		virtual const RuntimeData& rt() const;
		virtual bool			applyDelay();
		virtual bool			needsCleanState() const { return true; }

		////////////////////////////////////////////////////////////////////////////
		/// Inline functions
		////////////////////////////////////////////////////////////////////////////
	};

	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	ExprVM::ExprVM(const ConfigSection& config) : IAlpha(config, "EXPR_VM") {
		m_expr = config.getRequired("expr");
		m_noApplyDelay = config.getBool("noApplyDelay", m_noApplyDelay);
	}

	ExprVM::ExprVM(const ConfigSection& config, const char* logChannel, const std::string& expr) : IAlpha(config, logChannel) {
		m_expr = expr;
	}

	ExprVM::~ExprVM() {
	}

	//std::string ExprVM::uuid() const {
	//	return m_expr;
	//}

	DataPtr ExprVM::getVar(size_t index) {
		PROFILE_SCOPED();

		if (index < m_dynamicArgs.size())
			return m_dynamicArgs[index];
		return nullptr;
	}

	void ExprVM::prepareAlpha(IAlpha* alpha) {
		PROFILE_SCOPED();

		ASSERT(alpha, "Invalid alpha passed for prepare!");

		alpha->dynamicArgs()	= dynamicArgs();
		alpha->namedVars()		= namedVars();
		alpha->pinfo()			= pinfo();
		alpha->universe()		= m_data->universe;
		alpha->dataRegistry()	= m_dataRegistry;

		/// We don't apply the reversion at the alpha level ... instead, we apply it at the ExprVM level 
		/// when we're collecting alpha results in the end
		alpha->reversion()		= 1.0f;
		alpha->delay()			= 0;

		//IntDay delay = this->delay();

		//if (delay == 0) /// For d0 we have one less delta!
		//	alpha->pinfo().delta = std::max(m_pinfo.delta - 1, 0);

		m_alphaStack.push_back(alpha);
	}

	const ConfigSection& ExprVM::getConfig() {
		return m_config;
	}

	IDataRegistry* ExprVM::dataRegistry() {
		return m_dataRegistry;
	}

	IUniverse* ExprVM::universe() {
		return m_data->universe;
	}

	IAlpha* ExprVM::alpha() {
		return !m_alphaStack.size() ? nullptr : m_alphaStack[m_alphaStack.size() - 1];
	}

	const RuntimeData& ExprVM::rt() const {
		return m_rt;
	}

	bool ExprVM::applyDelay() {
		if (m_noApplyDelay)
			return false;

		IntDay delay = this->delay();
		return delay == 0 ? true : false;
	}

	void ExprVM::run(const RuntimeData& rt) {
		PROFILE_SCOPED();

		std::string expr = !m_expr.empty() ? m_expr : config().get("expr");
		ASSERT(!expr.empty(), "Invalid config for ExprVM class without an 'expr' tag with the expression!");

		vm::SimpleExpression::parseString(expr);

		m_rt = rt;
		vm::SimpleExpression::exec();

		ASSERT(m_varStack.size() == 1, "Invalid stack size. Expecting there to be just 1 item on the result stack. Found: " << m_varStack.size());
		DataPtr var = m_varStack[m_varStack.size() - 1];
		FloatMatrixData* result = dynamic_cast<FloatMatrixData*>(var.get());
		m_varStack.clear();

		if (m_alphaStack.size())
			m_alphaStack.pop_back();

		IntDay delay = this->delay();
		IntDay di = rt.di - delay;

		if (delay == 0 && !m_noApplyDelay) {
			result->applyDelay() = true;
			//m_pinfo.delta = 0;
		}

		result->flush(di);
		auto& alpha = IAlpha::alpha();
		//const float* rp = result->rowPtr(di);
		alpha[0] = (result->row(di) * reversion()).eval();

		result->applyDelay() = false;

		//ASSERT(result->rows() == 1, "Invalid alpha result has more than 1 rows! Rows found: " << result->rows());
		//m_data.alpha->ensureSize(1, result->cols(), false);

		//ASSERT(result->cols() == m_data.alpha->cols(), "Alpha result mismatch. Expecting: " << m_data.alpha->cols() << " - Found: " << result->cols());
		//ASSERT(m_data.alpha->dataSize() == result->dataSize(), "Data size mismatch! Expecting: " << m_data.alpha->dataSize() << " - Found: " << result->dataSize());

		/// DO NOT do a memcpy! The source data might be late-bound. We wanna resolve it
		///// by doing an item-wise copy
		//size_t size = m_data.alpha->cols();
		//for (size_t ii = 0; ii < size; ii++) {
		//	//float srcValue = (*result)(rt.di - delay(), ii);
		//	float srcValue = (*result)(rt.di - delay(), ii);
		//	float& dstValue = (*m_data.alpha)(0, ii);
		//	dstValue = srcValue * m_pinfo.reversion;
		//}
			//(*m_data.alpha)(0, ii) = (*result)(rt.di - delay(), ii);

		// std::cout << alpha[0] << std::endl;
	}

	//////////////////////////////////////////////////////////////////////////
	/// DynamicExprAlpha
	//////////////////////////////////////////////////////////////////////////
	class DynamicExprAlpha : public ExprVM {
	public:
								DynamicExprAlpha(const ConfigSection& config, const std::string& expr);
		virtual 				~DynamicExprAlpha();

		virtual void 			run(const RuntimeData& rt);
	};

	DynamicExprAlpha::DynamicExprAlpha(const ConfigSection& config, const std::string& expr) 
		: ExprVM(config, "DYN_EXPR", expr) {
	}

	DynamicExprAlpha::~DynamicExprAlpha() {
	}

	void DynamicExprAlpha::run(const RuntimeData& rt) {
		size_t varIndex = 0;

		//for (size_t i = 0; i < m_dynamicArgs.size(); i++) {
		//	ConfigSection& config = const_cast<ConfigSection&>(m_config);
		//	std::string varName = m_dynamicArgs[i]->name();

		//	if (!varName.empty() && varName.find("CFG.") == std::string::npos) {
		//		std::string varId = "x" + Util::cast(varIndex);
		//		std::string existingValue;

		//		while (config.get(varId, existingValue)) {
		//			varIndex++;
		//			varId = "x" + Util::cast(varIndex);
		//		}

		//		config.set(varId, m_dynamicArgs[i]->name());
		//	}
		//}

		ExprVM::run(rt);
	}

} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createExprVM(const pesa::ConfigSection& config) {
	return new pesa::ExprVM((const pesa::ConfigSection&)config);
}

#define EXPR_FUNC(Name, Expr)	EXPORT_FUNCTION pesa::IComponent* create##Name(const pesa::ConfigSection& config) { \
	return new pesa::DynamicExprAlpha((const pesa::ConfigSection&)config, Expr); \
}

//EXPR_FUNC(DeltaPct,		"Delta(CFG.x0) / FirstValidValue(CFG.x0)");

//EXPR_FUNC(Returns,		"(CFG.x0 / PrevValue(CFG.x0)) - 1");
/// Original Formula AvgReturns: alpha += -data[dij, ii] * exp(1.0 + (D - jj) * 0.1)
//EXPR_FUNC(AvgReturns,	"Sum(CFG.x0 * math.exp(1.0f + (RT.days - RT.jj) * 0.1f))");
//EXPR_FUNC(IntAverage,	"0.5f * (math.abs(CFG.x0 - FirstValidValue(CFG.x0)) + math.abs(Max(CFG.x0) - Min(CFG.x0))) / Median(CFG.x0)");
//EXPR_FUNC(CumReturns,	"Sum(Returns(CFG.x0))");

//EXPR_FUNC(UpDownDays,	"(UpDays(CFG.x0) - DownDays(CFG.x0)) / DownDays(CFG.x0)");
//EXPR_FUNC(Spread,		"(Max(CFG.x0) - Min(CFG.x0)) / (Max(CFG.x0) + Min(CFG.x0))");
//
//EXPR_FUNC(SignedPow,	"Pow(CFG.x0, exponent = RT.days) * math.sign(CFG.x0)");

//EXPR_FUNC(Momentum,		"Sum(CFG.x0 - PrevValue(CFG.x0))");
//
//EXPR_FUNC(Rank,			"Sum(CFG.x0)");
