/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Delta.cpp
///
/// Created on: 31 Oct 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Delta = CurrentValue - PrevValue
	////////////////////////////////////////////////////////////////////////////
	class Delta : public IAlpha {
	public:
								Delta(const ConfigSection& config) : IAlpha(config, "Delta") {}
		virtual 				~Delta() {}

		virtual void 			run(const RuntimeData& rt);
	};

	void Delta::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& data 		= F_ARG0();
		auto& alpha		= this->alpha();

		CHK_DATA_ALPHA(alpha, data);

		auto currValue = data[di];
		auto prevValue = data[di - (m_pinfo.omitDays + m_pinfo.delta)];
		alpha[0] = ((currValue - prevValue).eval() * reversion()).eval();
		//II_LOOP() {
		//	alpha(0, ii) = (data(di - m_pinfo.omitDays, ii) - IAlpha::firstValidValue(data, di, ii, m_pinfo.omitDays + 1)) * reversion();
		//}
	}

	////////////////////////////////////////////////////////////////////////////
	/// DeltaSum = Sum(CurrentValue - Last valid value, days = N)
	////////////////////////////////////////////////////////////////////////////
	class DeltaSum : public IAlpha {
	public:
								DeltaSum(const ConfigSection& config) : IAlpha(config, "DeltaSum") {}
		virtual 				~DeltaSum() {}

		virtual void 			run(const RuntimeData& rt);
	};

	void DeltaSum::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& data 		= F_ARG0();
		auto& alpha		= this->alpha();

		CHK_DATA_ALPHA(alpha, data);

		alpha.zeroMemory();

		JJ_LOOP() {
			auto currValue = data[di - jj];
			auto prevValue = data[di - jj - m_pinfo.delta];
			alpha[0] = (alpha[0] + (currValue - prevValue).eval()).eval();	
		}

		alpha[0] = (alpha[0] * reversion()).eval();
	}

	////////////////////////////////////////////////////////////////////////////
	/// DeltaPct = CurrentValue / PrevValue
	////////////////////////////////////////////////////////////////////////////
	class DeltaPct : public IAlpha {
	public:
								DeltaPct(const ConfigSection& config) : IAlpha(config, "DeltaPct") {}
		virtual 				~DeltaPct() {}

		virtual void 			run(const RuntimeData& rt);
	};

	void DeltaPct::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& data 		= F_ARG0();
		auto& alpha		= this->alpha();

		CHK_DATA_ALPHA(alpha, data);

		auto currValue = data[di];
		auto prevValue = data[di - (m_pinfo.omitDays + m_pinfo.delta)];
		alpha[0] = ((currValue - prevValue) / prevValue) * reversion();
		//II_LOOP() {
		//	float currValue = data(di - m_pinfo.omitDays, ii);
		//	float firstValidValue = IAlpha::firstValidValue(data, di, ii, m_pinfo.omitDays + 1);
		//	
		//	alpha(0, ii) = ((currValue - firstValidValue) / firstValidValue) * reversion();
		//}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createDelta(const pesa::ConfigSection& config) {
	return new pesa::Delta((const pesa::ConfigSection&)config);
}

EXPORT_FUNCTION pesa::IComponent* createDeltaSum(const pesa::ConfigSection& config) {
	return new pesa::DeltaSum((const pesa::ConfigSection&)config);
}

EXPORT_FUNCTION pesa::IComponent* createDeltaPct(const pesa::ConfigSection& config) {
	return new pesa::DeltaPct((const pesa::ConfigSection&)config);
}

