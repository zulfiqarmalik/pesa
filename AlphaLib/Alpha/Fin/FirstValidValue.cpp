/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// FirstValidValue.cpp
///
/// Created on: 02 Aug 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class FirstValidValue : public IAlpha {
	public:
								FirstValidValue(const ConfigSection& config);
		virtual 				~FirstValidValue();

		virtual void 			run(const RuntimeData& rt);
	};

	FirstValidValue::FirstValidValue(const ConfigSection& config) : IAlpha(config, "FirstValidValue") {
	}

	FirstValidValue::~FirstValidValue() {
	}

	void FirstValidValue::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& data 		= F_ARG0();
		ASSERT(false, "FirstValidValue has been deprecated!");

		//II_LOOP() {
		//	JJ_LOOP_O(1) {
		//		float value = data(di - jj, ii);
		//		if (!std::isnan(value)) {
		//			alpha(0, ii) = value;
		//			break;
		//		}
		//	}
		//}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createFirstValidValue(const pesa::ConfigSection& config) {
	return new pesa::FirstValidValue((const pesa::ConfigSection&)config);
}

