/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// History.cpp
///
/// Created on: 27 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class History : public IAlpha {
	public:
								History(const ConfigSection& config);
		virtual 				~History();

		virtual void 			run(const RuntimeData& rt);
	};

	History::History(const ConfigSection& config) : IAlpha(config, "History") {
	}

	History::~History() {
	}

	void History::run(const RuntimeData& rt) {
		//IntDay di 		= rt.di - delay();
		//auto& data 		= F_ARG0();
		//size_t cols		= m_data.alpha->cols();

		//m_data.ensureAlphaSizeWithIndex((size_t)std::max(m_pinfo.days, di) + 1, m_data.alpha->matCols(), m_data.alpha->cols());

		//II_LOOP() {
		//	JJ_LOOP() {
		//		float value = data(di - jj, ii);
		//		alpha(di - jj, ii) = value;
		//	}
		//}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createHistory(const pesa::ConfigSection& config) {
	return new pesa::History((const pesa::ConfigSection&)config);
}

