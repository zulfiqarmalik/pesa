/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// IntAverage.cpp
///
/// Created on: 31 Oct 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// IntAverage
	////////////////////////////////////////////////////////////////////////////
	class IntAverage : public IAlpha {
	public:
								IntAverage(const ConfigSection& config) : IAlpha(config, "IntAverage") {}
		virtual 				~IntAverage() {}

		virtual void 			run(const RuntimeData& rt);
	};

	void IntAverage::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& data 		= F_ARG0();

		II_LOOP() {
			float median = IAlpha::median(data, di, ii);
			float min 	= IAlpha::min(data, di, ii);
			float max 	= IAlpha::max(data, di, ii);
			float currValue = data(di, ii);
			float prevValue = IAlpha::firstValidValue(data, di, ii, 1);
			float iavg = 0.5f * (std::abs(currValue - prevValue) + std::abs(max - min)) / median;

			alpha(0, ii) = iavg * reversion();
		}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createIntAverage(const pesa::ConfigSection& config) {
	return new pesa::IntAverage((const pesa::ConfigSection&)config);
}

