/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// MCap.cpp
///
/// Created on: 31 Oct 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// MCap
	////////////////////////////////////////////////////////////////////////////
	class MCap : public IAlpha {
	public:
								MCap(const ConfigSection& config) : IAlpha(config, "MCap") {}
		virtual 				~MCap() {}

		virtual void 			run(const RuntimeData& rt);
	};

	void MCap::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& data 		= F_ARG0();
		auto& alpha		= this->alpha();

		CHK_DATA_ALPHA(alpha, data);

		auto value = data[di];
		alpha[0] = ((data[di] * -0.001f).exp() * reversion()).eval();

		//II_LOOP() {
		//	float value	= data(di, ii);
		//	float avalue = 1.0e4f * std::exp(-value * 0.001f);
		//	alpha(0, ii) = avalue * reversion();
		//}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createMCap(const pesa::ConfigSection& config) {
	return new pesa::MCap((const pesa::ConfigSection&)config);
}

