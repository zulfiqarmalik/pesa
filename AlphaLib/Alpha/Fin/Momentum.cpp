/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Momentums.cpp
///
/// Created on: 25 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class Momentum : public IAlpha {
	public:
								Momentum(const ConfigSection& config);
		virtual 				~Momentum();

		virtual void 			run(const RuntimeData& rt);
	};

	Momentum::Momentum(const ConfigSection& config) : IAlpha(config, "Momentum") {
	}

	Momentum::~Momentum() {
	}

	void Momentum::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& data 		= F_ARG0();
		auto& alpha		= this->alpha();

		CHK_DATA_ALPHA(alpha, data);

		auto currValue = data[di];
		auto prevValue = data[di - m_pinfo.days]; 
		float rcp = (1.0f / (float)m_pinfo.days);
		alpha[0] = ((currValue - prevValue).eval() * (rcp * reversion())).eval();

		//II_LOOP() {
		//	float currValue = data(di, ii);
		//	float prevValue = data(di - m_pinfo.days, ii);

		//	if (!std::isnan(currValue) && !std::isnan(prevValue))
		//		alpha(0, ii) = ((currValue - prevValue) / (float)m_pinfo.days) * reversion();
		//	else
		//		alpha(0, ii) = IAlpha::invalid();
		//}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createMomentum(const pesa::ConfigSection& config) {
	return new pesa::Momentum((const pesa::ConfigSection&)config);
}

