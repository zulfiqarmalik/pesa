/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// MomentumCums.cpp
///
/// Created on: 25 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class MomentumCum : public IAlpha {
	public:
								MomentumCum(const ConfigSection& config);
		virtual 				~MomentumCum();

		virtual void 			run(const RuntimeData& rt);
	};

	MomentumCum::MomentumCum(const ConfigSection& config) : IAlpha(config, "MomentumCum") {
	}

	MomentumCum::~MomentumCum() {
	}

	void MomentumCum::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& data 		= F_ARG0();
		auto& alpha		= this->alpha();

		CHK_DATA_ALPHA(alpha, data);

		alpha.zeroMemory();
		float rcpDelta = 1.0f / (float)m_pinfo.delta;

		JJ_LOOP() {
			auto currValue = data[di - jj];
			auto prevValue = data[di - jj - m_pinfo.delta];
			alpha[0] = (alpha[0] + (currValue - prevValue) * rcpDelta).eval();
		}

		alpha[0] = (alpha[0] * reversion()).eval();

		//II_LOOP() {
		//	float avalue = 0.0f;

		//	JJ_LOOP() {
		//		float currValue = data(di - jj, ii);
		//		float prevValue = data(di - jj - m_pinfo.delta, ii);

		//		if (!std::isnan(currValue) && !std::isnan(prevValue))
		//			avalue += (currValue - prevValue) / (float)(m_pinfo.delta);
		//	}

		//	alpha(0, ii) = avalue * reversion();
		//}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createMomentumCum(const pesa::ConfigSection& config) {
	return new pesa::MomentumCum((const pesa::ConfigSection&)config);
}

