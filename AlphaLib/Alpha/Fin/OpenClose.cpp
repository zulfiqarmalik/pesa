/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// OpenClose.cpp
///
/// Created on: 31 Oct 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// OpenClose
	////////////////////////////////////////////////////////////////////////////
	class OpenClose : public IAlpha {
	public:
								OpenClose(const ConfigSection& config) : IAlpha(config, "OpenClose") {}
		virtual 				~OpenClose() {}

		virtual void 			run(const RuntimeData& rt);
	};

	void OpenClose::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& data1		= F_ARG0_OR("baseData.adjOpen");
		auto& data2		= F_ARG0_OR("baseData.adjClose");
		auto& alpha		= this->alpha();

		CHK_DATA_ALPHA(alpha, data1);
		CHK_DATA_ALPHA(alpha, data2);

		alpha.zeroMemory();

		JJ_LOOP() {
			float power = 1.0f - (float)jj * 0.1f;
			float expValue = std::exp(power);

			auto open = data1[di - jj];
			auto close = data2[di - jj];

			alpha[0] = (alpha[0] + ((open / close) * expValue)).eval();
		}

		alpha[0] = (alpha[0] * reversion()).eval();

		//II_LOOP() {
		//	JJ_LOOP() {
		//		float open 	= data1(di - jj, ii);
		//		float close = data2(di - jj, ii);
		//		float ratio = open / close;
		//		float power = 1.0f - (float)jj * 0.1f;

		//		alpha(0, ii) += ratio * std::exp(power);
		//	}

		//	alpha(0, ii) *= reversion();
		//}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createOpenClose(const pesa::ConfigSection& config) {
	return new pesa::OpenClose((const pesa::ConfigSection&)config);
}

