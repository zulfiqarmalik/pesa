/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Rank.cpp
///
/// Created on: 31 Oct 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// TS_Rank: For backwards compatibility, it's also called just Rank
	/// but it's essentially a Time Series Rank of the each of the securities
	////////////////////////////////////////////////////////////////////////////
	class TS_Rank : public IAlpha {
	public:
								TS_Rank(const ConfigSection& config) : IAlpha(config, "Rank") {}
		virtual 				~TS_Rank() {}

		virtual void 			run(const RuntimeData& rt);
	};

	void TS_Rank::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& data 		= F_ARG0();
		auto& alpha		= this->alpha();

		CHK_DATA_ALPHA(alpha, data);

		alpha.zeroMemory();
		FloatMatrixData one(nullptr, DataDefinition(), 1, alpha.cols());
		FloatMatrixData minusOne(nullptr, DataDefinition(), 1, alpha.cols());
		FloatMatrixData zero(nullptr, DataDefinition(), 1, alpha.cols());

		one.setMemory(1.0f);
		zero.setMemory(1.0f);
		minusOne.setMemory(-1.0f);

		auto& zeroMat = zero.innerMat();
		auto& oneMat = one.innerMat();
		auto& minusOneMat = minusOne.innerMat();
		auto startIndex = di - m_pinfo.days - m_pinfo.omitDays - m_pinfo.delta;
		auto count = m_pinfo.days + m_pinfo.delta + 1;
		auto currValues = data.middleRows(startIndex, count).eval();
		auto mask = currValues.nanMask().eval();

		JJ_LOOP() {
			auto index = count - jj - 1;
			auto prevIndex = index - m_pinfo.delta;
			auto currValue = currValues.row(index);
			auto prevValue = currValues.row(prevIndex);

			/// If either currValue or prevValue at any index is NaN then it will essentially 
			/// not count towards the rank ... which is what we want
			auto cmask = (mask.row(index) * mask.row(prevIndex)).eval();
			auto incr = (currValue < prevValue).select(oneMat * cmask, minusOneMat * cmask).eval();
			alpha[0] = (alpha[0] + ((currValue == prevValue).select(0.0f, incr).eval())).eval();
		}

		alpha[0] = (alpha[0] * reversion()).eval();
	}

	////////////////////////////////////////////////////////////////////////////
	/// CS_Rank: Cross-Sectional Rank of the vector
	////////////////////////////////////////////////////////////////////////////
	struct AscCmp {
		bool operator() (float* lhs, float* rhs) { return (*lhs) < (*rhs); }
	};

	struct DescCmp {
		bool operator() (float* lhs, float* rhs) { return (*lhs) > (*rhs); }
	};

	class CS_Rank : public IAlpha {
	private:
		bool 					m_desc = false;
		bool 					m_shrink = false;
		float 					m_skew = 0.0f;
		float 					m_power = 1.0f;

	public:
								CS_Rank(const ConfigSection& config) : IAlpha(config, "Rank") {}
		virtual 				~CS_Rank() {}

		virtual void 			run(const RuntimeData& rt);
	};

	void CS_Rank::run(const RuntimeData& rt) {
		auto& data = F_ARG0();
		auto& alpha = this->alpha();
		IntDay di = rt.di - delay();

		alpha[0] = (data[di] * reversion()).eval();

		std::vector<float*> alphaPtrValues;

		for (size_t ii = 0; ii < alpha.cols(); ii++) {
			float& value = alpha(0, ii);
			if (std::isfinite(value)) 
				alphaPtrValues.push_back(&value);
		}

		if (alphaPtrValues.empty())
			return;

		/// Sort using the pointers ...
		if (!m_desc)
			std::sort(alphaPtrValues.begin(), alphaPtrValues.end(), AscCmp());
		else
			std::sort(alphaPtrValues.begin(), alphaPtrValues.end(), DescCmp());


		for (size_t ii = 0; ii < alphaPtrValues.size(); ii++)
			*(alphaPtrValues[ii]) = (float)ii;
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createRank(const pesa::ConfigSection& config) {
	return new pesa::TS_Rank((const pesa::ConfigSection&)config);
}

EXPORT_FUNCTION pesa::IComponent* createTS_Rank(const pesa::ConfigSection& config) {
	return new pesa::TS_Rank((const pesa::ConfigSection&)config);
}

EXPORT_FUNCTION pesa::IComponent* createCS_Rank(const pesa::ConfigSection& config) {
	return new pesa::CS_Rank((const pesa::ConfigSection&)config);
}

