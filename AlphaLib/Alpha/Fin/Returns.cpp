/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Returns.cpp
///
/// Created on: 02 Aug 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Returns
	////////////////////////////////////////////////////////////////////////////
	class Returns : public IAlpha {
	public:
								Returns(const ConfigSection& config) : IAlpha(config, "Returns") {}
		virtual 				~Returns() {}

		virtual void 			run(const RuntimeData& rt);
	};

	void Returns::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& data 		= F_ARG0();
		auto& alpha		= this->alpha();

		CHK_DATA_ALPHA(alpha, data);

		if (di == 0)
			return;

		auto value = data[di];
		auto prevValue = data[di - 1];
		auto rcpPrevValue = 1.0f / prevValue;
		alpha[0] = (((value * rcpPrevValue).eval() - 1.0f).eval() * reversion()).eval();

		//II_LOOP() {
		//	float value = data(di, ii);
		//	float prevValue = data(di - 1, ii);
		//	float returns = (value / prevValue) - 1.0f;
		//	alpha(0, ii) = returns;
		//}
	}

	////////////////////////////////////////////////////////////////////////////
	/// CumReturns
	////////////////////////////////////////////////////////////////////////////
	class CumReturns : public IAlpha {
	public:
								CumReturns(const ConfigSection& config) : IAlpha(config, "CumReturns") {}
		virtual 				~CumReturns() {}

		virtual void 			run(const RuntimeData& rt);
	};

	void CumReturns::run(const RuntimeData& rt) {
		IntDay di = rt.di - delay();
		auto& data = F_ARG0();
		auto& alpha = this->alpha();

		CHK_DATA_ALPHA(alpha, data);

		if (di < (m_pinfo.days + m_pinfo.delta))
			return;

		alpha.zeroMemory();
		JJ_LOOP() {
			auto currValue = data[di - jj];
			auto prevValue = data[di - jj - m_pinfo.delta];
			alpha[0] = (alpha[0] + ((currValue / prevValue).eval() - 1.0f)).eval();
		}

		alpha[0] = (alpha[0] * reversion()).eval();

		//II_LOOP() {
		//	float returns = 0.0f;
		//	
		//	JJ_LOOP() {
		//		float value = data(di - jj, ii);
		//		float prevValue = data(di - jj - 1, ii);
		//		float lastReturns = (value / prevValue) - 1.0f;

		//		if (!std::isnan(lastReturns))
		//			returns += lastReturns;
		//	}

		//	alpha(0, ii) = returns;
		//}
	}

	////////////////////////////////////////////////////////////////////////////
	/// AvgReturns
	////////////////////////////////////////////////////////////////////////////
	class AvgReturns : public IAlpha {
	public:
								AvgReturns(const ConfigSection& config) : IAlpha(config, "AvgReturns") {}
		virtual 				~AvgReturns() {}

		virtual void 			run(const RuntimeData& rt);
	};

	void AvgReturns::run(const RuntimeData& rt) {
		IntDay di = rt.di - delay();
		auto& data = F_ARG0();

		II_LOOP() {
			float returns = 0.0f;
			size_t numValidDays = 0;
			JJ_LOOP() {
				float value = data(di - jj, ii);
				float prevValue = data(di - jj - 1, ii);
				float lastReturns = (value / prevValue) - 1.0f;

				if (!std::isnan(lastReturns)) {
					numValidDays++;
					returns += lastReturns;
				}
			}

			if (numValidDays)
				alpha(0, ii) = (returns / (float)numValidDays);
			else
				alpha(0, ii) = std::nanf("");
		}
	}

} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createReturns(const pesa::ConfigSection& config) {
	return new pesa::Returns((const pesa::ConfigSection&)config);
}

EXPORT_FUNCTION pesa::IComponent* createCumReturns(const pesa::ConfigSection& config) {
	return new pesa::CumReturns((const pesa::ConfigSection&)config);
}

EXPORT_FUNCTION pesa::IComponent* createAvgReturns(const pesa::ConfigSection& config) {
	return new pesa::AvgReturns((const pesa::ConfigSection&)config);
}

