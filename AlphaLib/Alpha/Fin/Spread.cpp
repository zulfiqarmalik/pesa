/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Spread.cpp
///
/// Created on: 31 Oct 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"

namespace pesa {
	#pragma warning(disable : 4503)

	////////////////////////////////////////////////////////////////////////////
	/// Spread
	////////////////////////////////////////////////////////////////////////////
	class Spread : public IAlpha {
	public:
								Spread(const ConfigSection& config) : IAlpha(config, "Spread") {}
		virtual 				~Spread() {}

		virtual void 			run(const RuntimeData& rt);
	};

	void Spread::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& data 		= F_ARG0();
		auto& alpha		= this->alpha();

		CHK_DATA_ALPHA(alpha, data);

		auto startIndex = di - m_pinfo.days + m_pinfo.delta;
		auto count = m_pinfo.days;
  		auto dataBlock = data.middleRows(startIndex, count).eval();
  		auto min = dataBlock.nanMinClear().colwise().minCoeff().eval();
  		auto max = dataBlock.nanMinClear().colwise().maxCoeff().eval();
		alpha[0] = (((max - min).eval() / (max + min).eval()) * reversion()).eval();

		//II_LOOP() {
		//	float min 	= IAlpha::min(data, di, ii);
		//	float max 	= IAlpha::max(data, di, ii);
		//	float spr 	= (max - min) / (max + min);

		//	alpha(0, ii) = spr * reversion();
		//}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createSpread(const pesa::ConfigSection& config) {
	return new pesa::Spread((const pesa::ConfigSection&)config);
}

