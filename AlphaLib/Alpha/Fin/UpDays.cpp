/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// UpDays.cpp
///
/// Created on: 27 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class UpDays : public IAlpha {
	public:
								UpDays(const ConfigSection& config);
		virtual 				~UpDays();

		virtual void 			run(const RuntimeData& rt);
	};

	UpDays::UpDays(const ConfigSection& config) : IAlpha(config, "UpDays") {
	}

	UpDays::~UpDays() {
	}

	void UpDays::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& data 		= F_ARG0();
		auto& alpha		= this->alpha();

		CHK_DATA_ALPHA(alpha, data);

		alpha.zeroMemory();

		FloatMatrixData zero(nullptr, DataDefinition(), 1, alpha.cols());
		zero.setMemory(0.0f);

		JJ_LOOP_D(m_pinfo.omitDays, m_pinfo.days, m_pinfo.delta) {
			auto value = data[di - jj];
			auto prevValue = data[di - jj - m_pinfo.delta];
			alpha[0] = (alpha[0] + ((value > prevValue).select(1.0f, zero.innerMat()))).eval();
		}

		alpha[0] = (alpha[0] * reversion()).eval();
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createUpDays(const pesa::ConfigSection& config) {
	return new pesa::UpDays((const pesa::ConfigSection&)config);
}

