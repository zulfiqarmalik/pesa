/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Value.cpp
///
/// Created on: 27 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"

namespace pesa {
	class Value : public IAlpha {
	public:
								Value(const ConfigSection& config);
		virtual 				~Value();

		virtual void 			run(const RuntimeData& rt);
	};

	Value::Value(const ConfigSection& config) : IAlpha(config, "Value") {
	}

	Value::~Value() {
	}

	void Value::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& data 		= F_ARG0();
		auto& alpha		= this->alpha();

		CHK_DATA_ALPHA(alpha, data);

		alpha[0] = (data[di] * reversion()).eval();
	}

	class PrevValue : public IAlpha {
	public:
								PrevValue(const ConfigSection& config);
		virtual 				~PrevValue();

		virtual void 			run(const RuntimeData& rt);
	};

	PrevValue::PrevValue(const ConfigSection& config) : IAlpha(config, "Value") {
	}

	PrevValue::~PrevValue() {
	}

	void PrevValue::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& data 		= F_ARG0();
		auto& alpha		= this->alpha();

		CHK_DATA_ALPHA(alpha, data);

		alpha[0] = (data[di - m_pinfo.delta] * reversion()).eval();
		// IntDay di 		= rt.di - delay();
		// auto& data 		= F_ARG0();

		// II_LOOP() {
		// 	float value = data(di - m_pinfo.delta, ii);
		// 	alpha(0, ii) = value * reversion();
		// }
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createValue(const pesa::ConfigSection& config) {
	return new pesa::Value((const pesa::ConfigSection&)config);
}

EXPORT_FUNCTION pesa::IComponent* createPrevValue(const pesa::ConfigSection& config) {
	return new pesa::PrevValue((const pesa::ConfigSection&)config);
}

