/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Abs.cpp
///
/// Created on: 22 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class Abs : public IAlpha {
	public:
								Abs(const ConfigSection& config);
		virtual 				~Abs();

		virtual void 			run(const RuntimeData& rt);
	};

	Abs::Abs(const ConfigSection& config) : IAlpha(config, "Abs") {
	}

	Abs::~Abs() {
	}

	void Abs::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& data 		= F_ARG0();
		auto& alpha		= this->alpha();

		CHK_DATA_ALPHA(alpha, data);

		alpha[0] = ((data[di].abs()).eval() * reversion()).eval(); 

		//for (size_t ii = 0; ii < alpha.cols(); ii++) {
		//	float value = data(di, ii);
		//	alpha(0, ii) = std::abs(value) * reversion();
		//}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createAbs(const pesa::ConfigSection& config) {
	return new pesa::Abs((const pesa::ConfigSection&)config);
}

