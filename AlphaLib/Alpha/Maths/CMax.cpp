/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// CMax.cpp
///
/// Created on: 24 Jan 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"

namespace pesa {
	inline float getMax(float value1, float value2) {
		if (std::isnan(value1))
			return value2;
		else if (std::isnan(value2))
			return value1;
		return std::max(value1, value2);
	}

	////////////////////////////////////////////////////////////////////////////
	/// Max between two datasets
	////////////////////////////////////////////////////////////////////////////
	class CMax2 : public IAlpha {
	public:
								CMax2(const ConfigSection& config) : IAlpha(config, "CMax") {}
		virtual 				~CMax2() {}

		virtual void 			run(const RuntimeData& rt) {
			IntDay di 		= rt.di - delay();
			auto& data0		= F_ARG0();
			auto& data1		= F_ARG1();
			auto& alpha 	= this->alpha();

			CHK_DATA_ALPHA(alpha, data0);
			CHK_DATA_ALPHA(alpha, data1);

			auto numStocks 	= alpha.cols();

			for (auto ii = 0; ii < numStocks; ii++) {
				alpha(0, ii) = getMax(data0(di, ii), data1(di, ii));
			}
		}
	};

	////////////////////////////////////////////////////////////////////////////
	/// Max between three datasets
	////////////////////////////////////////////////////////////////////////////
	class CMax3 : public IAlpha {
	public:
								CMax3(const ConfigSection& config) : IAlpha(config, "CMax") {}
		virtual 				~CMax3() {}

		virtual void 			run(const RuntimeData& rt) {
			IntDay di 		= rt.di - delay();
			auto& data0		= F_ARG0();
			auto& data1		= F_ARG1();
			auto& data2		= F_ARG2();
			auto& alpha 	= this->alpha();

			CHK_DATA_ALPHA(alpha, data0);
			CHK_DATA_ALPHA(alpha, data1);
			CHK_DATA_ALPHA(alpha, data2);

			auto numStocks 	= alpha.cols();

			for (auto ii = 0; ii < numStocks; ii++) {
				alpha(0, ii) = getMax(getMax(data0(di, ii), data1(di, ii)), data2(di, ii));
			}
		}
	};
	
	////////////////////////////////////////////////////////////////////////////
	/// Max between four datasets
	////////////////////////////////////////////////////////////////////////////
	class CMax4 : public IAlpha {
	public:
								CMax4(const ConfigSection& config) : IAlpha(config, "CMax") {}
		virtual 				~CMax4() {}

		virtual void 			run(const RuntimeData& rt) {
			IntDay di 		= rt.di - delay();
			auto& data0		= F_ARG0();
			auto& data1		= F_ARG1();
			auto& data2		= F_ARG2();
			auto& data3		= F_ARG3();
			auto& alpha 	= this->alpha();

			CHK_DATA_ALPHA(alpha, data0);
			CHK_DATA_ALPHA(alpha, data1);
			CHK_DATA_ALPHA(alpha, data2);
			CHK_DATA_ALPHA(alpha, data3);

			auto numStocks 	= alpha.cols();

			for (auto ii = 0; ii < numStocks; ii++) {
				alpha(0, ii) = getMax(getMax(getMax(data0(di, ii), data1(di, ii)), data2(di, ii)), data3(di, ii));
			}
		}
	};

	////////////////////////////////////////////////////////////////////////////
	/// Max between five datasets
	////////////////////////////////////////////////////////////////////////////
	class CMax5 : public IAlpha {
	public:
								CMax5(const ConfigSection& config) : IAlpha(config, "CMax") {}
		virtual 				~CMax5() {}

		virtual void 			run(const RuntimeData& rt) {
			IntDay di 		= rt.di - delay();
			auto& data0		= F_ARG0();
			auto& data1		= F_ARG1();
			auto& data2		= F_ARG2();
			auto& data3		= F_ARG3();
			auto& data4		= F_ARG4();
			auto& alpha 	= this->alpha();

			CHK_DATA_ALPHA(alpha, data0);
			CHK_DATA_ALPHA(alpha, data1);
			CHK_DATA_ALPHA(alpha, data2);
			CHK_DATA_ALPHA(alpha, data3);
			CHK_DATA_ALPHA(alpha, data4);

			auto numStocks 	= alpha.cols();

			for (auto ii = 0; ii < numStocks; ii++) {
				alpha(0, ii) = getMax(getMax(getMax(getMax(data0(di, ii), data1(di, ii)), data2(di, ii)), data3(di, ii)), data4(di, ii));
			}
		}
	};
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createCMax2(const pesa::ConfigSection& config) {
	return new pesa::CMax2((const pesa::ConfigSection&)config);
}

EXPORT_FUNCTION pesa::IComponent* createCMax3(const pesa::ConfigSection& config) {
	return new pesa::CMax3((const pesa::ConfigSection&)config);
}

EXPORT_FUNCTION pesa::IComponent* createCMax4(const pesa::ConfigSection& config) {
	return new pesa::CMax4((const pesa::ConfigSection&)config);
}

EXPORT_FUNCTION pesa::IComponent* createCMax5(const pesa::ConfigSection& config) {
	return new pesa::CMax5((const pesa::ConfigSection&)config);
}

