/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// GT.cpp
///
/// Created on: 22 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class GT : public IAlpha {
	public:
								GT(const ConfigSection& config);
		virtual 				~GT();

		virtual void 			run(const RuntimeData& rt);
	};

	GT::GT(const ConfigSection& config) : IAlpha(config, "GT") {
	}

	GT::~GT() {
	}

	void GT::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& alpha 	= this->alpha();
		auto& lhs 		= F_ARG0();
		auto& rhs 		= F_ARG1();
		auto& thenValue	= F_ARG2();
		auto& elseValue = F_ARG3();

		//const_cast<FloatMatrixData&>(lhs).ensureCols(di, alpha.cols());
		//const_cast<FloatMatrixData&>(rhs).ensureCols(di, alpha.cols());
		//const_cast<FloatMatrixData&>(thenValue).ensureCols(di, alpha.cols());
		//const_cast<FloatMatrixData&>(elseValue).ensureCols(di, alpha.cols());

		//CHK_DATA_ALPHA(alpha, lhs);
		//CHK_DATA_ALPHA(alpha, rhs);

		alpha[0]		= (lhs[di] > rhs[di]).select(thenValue[di], elseValue[di]);

		//for (size_t ii = 0; ii < alpha.cols(); ii++) {
		//	float lhsValue = lhs(di, ii);
		//	float rhsValue = rhs(di, ii);
		//	float tv = thenValue(di, ii);
		//	float ev = elseValue(di, ii);
		//	alpha(0, ii) = lhsValue > rhsValue ? thenValue(di, ii) : elseValue(di, ii);
		//}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createGT(const pesa::ConfigSection& config) {
	return new pesa::GT((const pesa::ConfigSection&)config);
}

