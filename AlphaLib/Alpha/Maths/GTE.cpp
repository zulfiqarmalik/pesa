/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// GTE.cpp
///
/// Created on: 22 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class GTE : public IAlpha {
	public:
								GTE(const ConfigSection& config);
		virtual 				~GTE();

		virtual void 			run(const RuntimeData& rt);
	};

	GTE::GTE(const ConfigSection& config) : IAlpha(config, "GTE") {
	}

	GTE::~GTE() {
	}

	void GTE::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& alpha 	= this->alpha();
		auto& lhs 		= F_ARG0();
		auto& rhs 		= F_ARG1();
		auto& thenValue	= F_ARG2();
		auto& elseValue = F_ARG3();

		CHK_DATA_ALPHA(alpha, lhs);
		CHK_DATA_ALPHA(alpha, rhs);

		alpha[0]		= (lhs[di] >= rhs[di]).select(thenValue[di], elseValue[di]);

		//for (size_t ii = 0; ii < alpha.cols(); ii++) {
		//	float lhsValue = lhs(di, ii);
		//	float rhsValue = rhs(di, ii);
		//	alpha(0, ii) = lhsValue >= rhsValue ? 1.0f : 0.0f;
		//}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createGTE(const pesa::ConfigSection& config) {
	return new pesa::GTE((const pesa::ConfigSection&)config);
}

