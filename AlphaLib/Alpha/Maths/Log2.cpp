/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Log2.cpp
///
/// Created on: 22 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class Log2 : public IAlpha {
	public:
								Log2(const ConfigSection& config);
		virtual 				~Log2();

		virtual void 			run(const RuntimeData& rt);
	};

	Log2::Log2(const ConfigSection& config) : IAlpha(config, "Log2") {
	}

	Log2::~Log2() {
	}

	void Log2::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& alpha 	= this->alpha();
		auto& data 		= F_ARG0();

		for (size_t ii = 0; ii < alpha.cols(); ii++) {
			float value = data(di, ii);
			alpha(0, ii) = std::log2(value) * reversion();
		}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createLog2(const pesa::ConfigSection& config) {
	return new pesa::Log2((const pesa::ConfigSection&)config);
}

