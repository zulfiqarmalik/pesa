/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// MathOps.cpp
///
/// Created on: 25 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// OpMul
	////////////////////////////////////////////////////////////////////////////
	class OpMul : public IAlpha {
	public:
							OpMul(const ConfigSection& config) : IAlpha(config, "OpMul") {}
		virtual 			~OpMul() {}

		virtual void 		run(const RuntimeData& rt) {
			IntDay di 		= rt.di - delay();
			auto& arg0 		= F_ARG0();
			auto& arg1 		= F_ARG1();
			auto& alpha 	= this->alpha();

			CHK_DATA_ALPHA(alpha, arg0);
			CHK_DATA_ALPHA(alpha, arg1);

			alpha[0] 		= (arg0[di] * arg1[di]).eval();
		}
	};

	////////////////////////////////////////////////////////////////////////////
	/// OpDiv
	////////////////////////////////////////////////////////////////////////////
	class OpDiv : public IAlpha {
	public:
							OpDiv(const ConfigSection& config) : IAlpha(config, "OpDiv") {}
		virtual 			~OpDiv() {}

		virtual void 		run(const RuntimeData& rt) {
			IntDay di 		= rt.di - delay();
			auto& arg0 		= F_ARG0();
			auto& arg1 		= F_ARG1();
			auto& alpha 	= this->alpha();

			CHK_DATA_ALPHA(alpha, arg0);
			CHK_DATA_ALPHA(alpha, arg1);

			alpha[0] 		= (arg0[di] / arg1[di]).eval();
		}
	};

	////////////////////////////////////////////////////////////////////////////
	/// OpAdd
	////////////////////////////////////////////////////////////////////////////
	class OpAdd : public IAlpha {
	public:
							OpAdd(const ConfigSection& config) : IAlpha(config, "OpAdd") {}
		virtual 			~OpAdd() {}

		virtual void 		run(const RuntimeData& rt) {
			IntDay di 		= rt.di - delay();
			auto& arg0 		= F_ARG0();
			auto& arg1 		= F_ARG1();
			auto& alpha 	= this->alpha();

			CHK_DATA_ALPHA(alpha, arg0);
			CHK_DATA_ALPHA(alpha, arg1);

			alpha[0] 		= (arg0[di] + arg1[di]).eval();
		}
	};

	////////////////////////////////////////////////////////////////////////////
	/// OpSub
	////////////////////////////////////////////////////////////////////////////
	class OpSub : public IAlpha {
	public:
							OpSub(const ConfigSection& config) : IAlpha(config, "OpSub") {}
		virtual 			~OpSub() {}

		virtual void 		run(const RuntimeData& rt) {
			IntDay di 		= rt.di - delay();
			auto& arg0 		= F_ARG0();
			auto& arg1 		= F_ARG1();
			auto& alpha 	= this->alpha();

			CHK_DATA_ALPHA(alpha, arg0);
			CHK_DATA_ALPHA(alpha, arg1);

			alpha[0] 		= (arg0[di] - arg1[di]).eval();
		}
	};
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createOpMul(const pesa::ConfigSection& config) {
	return new pesa::OpMul((const pesa::ConfigSection&)config);
}

EXPORT_FUNCTION pesa::IComponent* createOpDiv(const pesa::ConfigSection& config) {
	return new pesa::OpDiv((const pesa::ConfigSection&)config);
}

EXPORT_FUNCTION pesa::IComponent* createOpAdd(const pesa::ConfigSection& config) {
	return new pesa::OpAdd((const pesa::ConfigSection&)config);
}

EXPORT_FUNCTION pesa::IComponent* createOpSub(const pesa::ConfigSection& config) {
	return new pesa::OpSub((const pesa::ConfigSection&)config);
}
