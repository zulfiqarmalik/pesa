/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Max.cpp
///
/// Created on: 22 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// DEPRECATED - Historical Max per stock
	////////////////////////////////////////////////////////////////////////////
	class Max2 : public IAlpha {
	public:
								Max2(const ConfigSection& config) : IAlpha(config, "Max2") {}
								~Max2() {}

		virtual void 			run(const RuntimeData& rt) {
			IntDay di = rt.di - delay();
			auto& data = F_ARG0();
			auto& alpha = this->alpha();

			CHK_DATA_ALPHA(alpha, data);

			auto startIndex = di - m_pinfo.days + m_pinfo.delta;
			if (startIndex < 0)
				return;
			auto count = m_pinfo.days;
			alpha[0] = (data.middleRows(startIndex, count).nanColMax().eval() * reversion()).eval();
		}
	};

	////////////////////////////////////////////////////////////////////////////
	/// !DEPRECATED! - Historical Max per stock
	////////////////////////////////////////////////////////////////////////////
	class Max : public IAlpha {
	public:
								Max(const ConfigSection& config) : IAlpha(config, "Max") {}
								~Max() {}

		virtual void 			run(const RuntimeData& rt) {
			IntDay di = rt.di - delay();
			auto& data = F_ARG0();
			auto& alpha = this->alpha();

			CHK_DATA_ALPHA(alpha, data);

			auto startIndex = di - m_pinfo.days + m_pinfo.delta;
			if (startIndex < 0)
				return;
			auto count = m_pinfo.days;

			/// The bug with this Max function was that nanColMax (which it used originally) was essentially
			/// doing a colwise().minCoeff() instead of the colwise().maxCoeff(). That has now been fixed in 
			/// E_ArrayPlugins but this has to be kept like this for backward compatibility purposes. 
			/// The new max function is Max2 above which uses the corrected nanColMax() function now!
			alpha[0] = (data.middleRows(startIndex, count).nanColMin().eval() * reversion()).eval();
		}
	};

} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createMax2(const pesa::ConfigSection& config) {
	return new pesa::Max((const pesa::ConfigSection&)config);
}

/// !DEPRECATED!
EXPORT_FUNCTION pesa::IComponent* createMax(const pesa::ConfigSection& config) {
	return new pesa::Max((const pesa::ConfigSection&)config);
}

