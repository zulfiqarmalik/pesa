/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Min.cpp
///
/// Created on: 22 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Columnwise Min
	////////////////////////////////////////////////////////////////////////////
	class Min : public IAlpha {
	public:
								Min(const ConfigSection& config) : IAlpha(config, "Min") {}
		virtual 				~Min() {}

		virtual void 			run(const RuntimeData& rt) {
			IntDay di 		= rt.di - delay();
			auto& data 		= F_ARG0();
			auto& alpha 	= this->alpha();

			CHK_DATA_ALPHA(alpha, data);

			auto startIndex = di - m_pinfo.days + m_pinfo.delta;
			if (startIndex < 0)
				return;
			auto count = m_pinfo.days;
			alpha[0] = (data.middleRows(startIndex, count).nanColMin().eval() * reversion()).eval();
		}
	};
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createMin(const pesa::ConfigSection& config) {
	return new pesa::Min((const pesa::ConfigSection&)config);
}

