/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Mul.cpp
///
/// Created on: 25 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class Mul : public IAlpha {
	public:
								Mul(const ConfigSection& config);
		virtual 				~Mul();

		virtual void 			run(const RuntimeData& rt);
	};

	Mul::Mul(const ConfigSection& config) : IAlpha(config, "Mul") {
	}

	Mul::~Mul() {
	}

	void Mul::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& data 		= F_ARG0();
		auto& alpha 	= this->alpha();

		CHK_DATA_ALPHA(alpha, data);

		CHK_DATA_ALPHA(alpha, data);

		auto startIndex = (di - m_pinfo.days) + m_pinfo.delta;
		auto count = m_pinfo.days;
		if (startIndex < 0)
			return;

		alpha[0] = (data.middleRows(startIndex, count).nanClear().colwise().prod().eval() * reversion()).eval();
		//alpha.setMemory(1.0f);

		//JJ_LOOP() {
		//	alpha[0] = (alpha[0] * data[di - jj]).eval();
		//}

		//II_LOOP() {
		//	float value = 1.0f;

		//	JJ_LOOP() {
		//		float backValue = data(di - jj, ii);

		//		if (!std::isnan(backValue))
		//			value *= backValue;
		//	}

		//	alpha(0, ii) = value * reversion();
		//}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createMul(const pesa::ConfigSection& config) {
	return new pesa::Mul((const pesa::ConfigSection&)config);
}

