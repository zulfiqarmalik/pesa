/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Pow.cpp
///
/// Created on: 27 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class Pow : public IAlpha {
	public:
								Pow(const ConfigSection& config);
		virtual 				~Pow();

		virtual void 			run(const RuntimeData& rt);
	};

	Pow::Pow(const ConfigSection& config) : IAlpha(config, "Pow") {
	}

	Pow::~Pow() {
	}

	void Pow::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& data 		= F_ARG0();
		auto exponent 	= arg("exponent", 0.0f);
		auto& alpha		= this->alpha();

		CHK_DATA_ALPHA(alpha, data);

		alpha[0] = data[di].pow(exponent) * reversion();

		//ASSERT(!exponent.isValid, "Invalid power function value specified: " << exponent);

		//II_LOOP() {
		//	float base = data(di, ii);
		//	alpha(0, ii) = std::pow(base, exponent) * reversion();
		//}
	}
	
	////////////////////////////////////////////////////////////////////////////
	/// SignedPow
	////////////////////////////////////////////////////////////////////////////
	class SignedPow : public IAlpha {
	public:
								SignedPow(const ConfigSection& config) : IAlpha(config, "SignedPow") {}
		virtual 				~SignedPow() {}

		virtual void 			run(const RuntimeData& rt);
	};

	void SignedPow::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& data 		= F_ARG0();
		auto exponent 	= arg("exponent", (float)m_pinfo.days);
		auto& alpha		= this->alpha();

		CHK_DATA_ALPHA(alpha, data);

		alpha[0] = ((data[di].pow(exponent)).eval() * (data[di] / data[di].abs()).eval() * reversion()).eval(); /// the division is to just get the sign
		//ASSERT(!exponent.isValid, "Invalid power function value specified: " << exponent);

		//II_LOOP() {
		//	float base = data(di, ii);
		//	float value = std::pow(base, exponent) * std::copysign(1.0f, base);
		//	//float expValue = exponent(this, rt, di, ii);
		//	alpha(0, ii) = value * reversion();
		//}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createPow(const pesa::ConfigSection& config) {
	return new pesa::Pow((const pesa::ConfigSection&)config);
}

EXPORT_FUNCTION pesa::IComponent* createSignedPow(const pesa::ConfigSection& config) {
	return new pesa::SignedPow((const pesa::ConfigSection&)config);
}

