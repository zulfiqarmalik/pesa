/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Sign.cpp
///
/// Created on: 27 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class Sign : public IAlpha {
	public:
								Sign(const ConfigSection& config);
		virtual 				~Sign();

		virtual void 			run(const RuntimeData& rt);
	};

	Sign::Sign(const ConfigSection& config) : IAlpha(config, "Sign") {
	}

	Sign::~Sign() {
	}

	void Sign::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& data 		= F_ARG0();

		auto& alpha		= this->alpha();

		CHK_DATA_ALPHA(alpha, data);

		alpha[0] = ((data[di] / data[di].abs()).eval() * reversion()).eval(); /// the division is to just get the sign
		//II_LOOP() {
		//	float value = data(di, ii);
		//	
		//	if (value < 0.0f)
		//		value = -1.0f;
		//	else if (value > 0.0f)
		//		value = 1.0f;

		//	alpha(0, ii) = value * reversion();
		//}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createSign(const pesa::ConfigSection& config) {
	return new pesa::Sign((const pesa::ConfigSection&)config);
}

