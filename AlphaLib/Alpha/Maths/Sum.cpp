/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Sum.cpp
///
/// Created on: 27 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class Sum : public IAlpha {
	public:
								Sum(const ConfigSection& config);
		virtual 				~Sum();

		virtual void 			run(const RuntimeData& rt);
	};

	Sum::Sum(const ConfigSection& config) : IAlpha(config, "Sum") {
	}

	Sum::~Sum() {
	}

	void Sum::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& data 		= F_ARG0();
		auto& alpha		= this->alpha();

		CHK_DATA_ALPHA(alpha, data);

		auto startIndex = (di - m_pinfo.days) + m_pinfo.delta;
		auto count = m_pinfo.days;
		if (startIndex < 0)
			return;

		alpha[0] = (data.middleRows(startIndex, count).nanClear().colwise().sum().eval() * reversion()).eval();

		//alpha.setMemory(0.0f);

		//JJ_LOOP() {
		//	alpha[0] += data[di - jj].nanClear();
		//}

		//alpha[0] *= reversion();

		//II_LOOP() {
		//	float value = 0.0f;

		//	JJ_LOOP() {
		//		float backValue = data(di - jj, ii);

		//		if (!std::isnan(backValue))
		//			value += backValue;
		//	}

		//	alpha(0, ii) = value * reversion();
		//}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createSum(const pesa::ConfigSection& config) {
	return new pesa::Sum((const pesa::ConfigSection&)config);
}

