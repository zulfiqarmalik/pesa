/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// PosAlpha.cpp
///
/// Created on: 22 Jun 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Poco/String.h"
#include "Poco/File.h"
#include "Poco/FileStream.h"
#include <sstream>

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class PosAlpha : public IAlpha {
	private:
		std::string				m_tickerDataId = "secData.bbTickers";
		std::string				m_priceDataId = "baseData.adjClosePrice";
		float					m_bookSize = 0.0f;
		bool					m_calcBookSize = false;
		std::unordered_map<std::string, size_t> m_uuidMap;

		void					loadTickerMap(const RuntimeData& rt);

	public:
								PosAlpha(const ConfigSection& config);
		virtual 				~PosAlpha();

		virtual void 			run(const RuntimeData& rt);
		virtual float			bookSize();
	};

	PosAlpha::PosAlpha(const ConfigSection& config) : IAlpha(config, "PosAlpha") {
		m_bookSize = parentConfig()->portfolio().bookSize();
	}

	PosAlpha::~PosAlpha() {
	}

	float PosAlpha::bookSize() {
		return m_bookSize;
	}

	void PosAlpha::loadTickerMap(const RuntimeData& rt) {
		if (m_uuidMap.size())
			return;

		auto* secMaster = rt.dataRegistry->getSecurityMaster();
		StringData* tickers = rt.dataRegistry->stringData(secMaster, m_tickerDataId);
		ASSERT(tickers, "Unable to load ticker data: " << m_tickerDataId);

		//size_t numCols = tickers->cols();
		size_t numSecurities = secMaster->size(rt.di);

		for (size_t ii = 0; ii < numSecurities; ii++) {
			auto sec = secMaster->security(rt.di, (Security::Index)ii);
			auto secUuid = sec.uuidStr();
			std::string ticker = tickers->getValue(rt.di, ii);

			if (!ticker.empty()) {
				auto tickerParts = Util::split(ticker, " ");
				m_uuidMap[ticker] = ii;

				if (tickerParts.size() > 1)
					m_uuidMap[tickerParts[0]] = ii;
			}
		}

	}

	void PosAlpha::run(const RuntimeData& rt) {
		std::string workingDir = config().getRequired("workingDir");
		std::string posDir = parentConfig()->defs().resolve(config().getRequired("posDir"), 0u);
		std::string posFilename = config().getRequired("posFilename");
		Poco::Path posPath; ///(workingDir);

		if (!workingDir.empty()) {
			posPath = Poco::Path(workingDir);
			posPath.append(posDir);
		}
		else {
			posPath = Poco::Path(posDir);
		}

		IntDate date = rt.dates[rt.di];
		std::string sdate = Util::cast(date);

		posPath.append(posFilename + "_" + sdate + ".pos");

		Poco::File posFile(posPath.toString());

		if (!posFile.exists()) {
			std::string orgPosFilename = posPath.toString();
			posPath.setFileName(posFilename);
			posPath.setExtension("pos");
			Poco::File posFile(posPath.toString());
			CDebug("Pos file not found: %s - reverting to last day's position: %s", orgPosFilename, posPath.toString());

			ASSERT(posFile.exists(), "Unable to find pos file: " << posPath.toString() << " ABORTING!");
		}

		Poco::FileInputStream posStream(posPath.toString());
		static const size_t maxLine = 8192;
		char cline[maxLine];
		auto& alpha = this->alpha();
		auto* universe = this->universe();
		//std::unordered_map<std::string, float> positions;
		alpha.invalidateMemory();
		auto* secMaster = rt.dataRegistry->getSecurityMaster();

		while (!posStream.eof()) {
			posStream.getline(cline, maxLine);

			/// Lines strating with # are comments
			if (cline[0] != '#') {
				std::string line(cline);
				auto parts = Util::split(line, "|");

				if (parts.size() >= 4) {
					std::string bbTicker = parts[0];
					std::string uuid = parts[1];
					std::string allocation = parts[3];
					std::string numSharesStr = parts[2];
					float dollarAllocation = 0.0f;
					float numShares = 0.0f;
					Security::Index ii = Security::s_invalidIndex;

					if (!allocation.empty())
						dollarAllocation = Util::cast<float>(allocation);

					if (!numSharesStr.empty())
						numShares = Util::cast<float>(numSharesStr);

					/// OK, this is a situation where the UUID has turned out to be empty. 
					/// In this case we try to match the ticker for the date to get the most
					/// accurate FIGI back 
					if (uuid.empty()) {
						this->loadTickerMap(rt);
						auto iiIter = m_uuidMap.find(bbTicker);

						if (iiIter == m_uuidMap.end()) {
							bbTicker = Poco::replace(bbTicker, " GY", " GR");

							iiIter = m_uuidMap.find(bbTicker);

							if (iiIter == m_uuidMap.end()) {
								CInfo("[PosAlpha] Unable to map ticker: %s", bbTicker);
								continue;
							}
						}

						ii = (Security::Index)iiIter->second;

						auto sec = secMaster->security(rt.di, ii);
						const FloatMatrixData* prices = rt.dataRegistry->floatData(secMaster, m_priceDataId);
						float price = (*prices)(rt.di - this->getDelay(), ii);
						dollarAllocation = numShares * price;
					}
					else {
						ii = secMaster->mapUuid(uuid)->index;
					}

					ASSERT(ii != Security::s_invalidIndex, "Unable to find uuid: " << (!uuid.empty() ? uuid : bbTicker) << " on di: " << rt.di << "[Date = " << rt.dates[rt.di] << "]");
						
					/// We shall NaN-ify small allocations. This is to avoid having positions in these 
					/// stocks if we were to normalise this again ...
					if (std::abs(dollarAllocation) < 0.001)
						dollarAllocation = std::nanf("");

					size_t ai = universe->alphaIndex(rt.di, (size_t)ii);

					alpha(0, ai) = dollarAllocation;

					if (m_calcBookSize && !std::isnan(dollarAllocation))
						m_bookSize += std::abs(dollarAllocation);
					///// get the index of this uuid in this univers ...
					//ASSERT(positions.find(uuid) == positions.end(), "UUID already exists: " << uuid);
					//positions[uuid] = Util::cast<float>(allocation);
				}
			}
			else {
				auto parts = Util::split(std::string(cline), ":");
				std::string prefix = Poco::toLower(parts[0]);
				if (parts.size() == 2 && prefix == "# booksize") {
					std::istringstream iss(parts[1]);
					iss >> m_bookSize;

					if (m_bookSize <= 0.0001f)
						m_calcBookSize = true;
				}
			}
		}

		/// Clear the ticker map. We'll set it up again if we need it in the next iteration!
		m_uuidMap.clear();
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createPosAlpha(const pesa::ConfigSection& config) {
	return new pesa::PosAlpha((const pesa::ConfigSection&)config);
}

