/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// PosAlphaDB.cpp
///
/// Created on: 26 Apr 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Poco/String.h"
#include "Poco/File.h"
#include "Poco/FileStream.h"
#include "Helper/DB/MongoStream.h"
#include <sstream>

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class PosAlphaDB : public IAlpha {
	private:
		std::unordered_map<std::string, size_t> m_uuidMap;

	public:
								PosAlphaDB(const ConfigSection& config);
		virtual 				~PosAlphaDB();

		virtual void 			run(const RuntimeData& rt);
	};

	PosAlphaDB::PosAlphaDB(const ConfigSection& config) : IAlpha(config, "PosAlphaDB") {
	}

	PosAlphaDB::~PosAlphaDB() {
	}

	void PosAlphaDB::run(const RuntimeData& rt) {
		std::string iid = config().getRequired("iid");
		std::string dbName = config().getRequired("dbName");
		std::string sid = iid + "_" + Util::cast<int>(rt.dates[rt.di]);
		auto& alpha = this->alpha();

		alpha.invalidateMemory();

		io::InputMongoStream is(std::string("POS_Notionals"), sid, dbName.c_str());

		if (is.isValid()) {
			size_t dataLength = 0;
			uint8_t* data = nullptr;

			is.read("data", &data, dataLength);

			/// now that we have read the data ... convert it into floating point
			float* fdata = reinterpret_cast<float*>(data);

			memcpy(alpha.fdata(0), (const void*)data, dataLength);

			delete[] data;
		}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createPosAlphaDB(const pesa::ConfigSection& config) {
	return new pesa::PosAlphaDB((const pesa::ConfigSection&)config);
}

