/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Correlations.cpp
///
/// Created on: 25 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class Correlation : public IAlpha {
	public:
								Correlation(const ConfigSection& config);
		virtual 				~Correlation();

		virtual void 			run(const RuntimeData& rt);
	};

	Correlation::Correlation(const ConfigSection& config) : IAlpha(config, "Correlation") {
	}

	Correlation::~Correlation() {
	}

	void Correlation::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& data1 	= F_ARG0();
		auto& data2 	= F_ARG1();
		auto& alpha		= this->alpha();

		CHK_DATA_ALPHA(alpha, data1);
		CHK_DATA_ALPHA(alpha, data2);

		auto startIndex = (di - m_pinfo.days) + m_pinfo.delta;

		if (startIndex < 0)
			return;

		auto count	= m_pinfo.days;
		auto x		= data1.middleRows(startIndex, count);
		auto xMean	= x.nanMean<float>().eval();
		auto xSD	= x.nanStdDev<float>().eval();

		auto y		= data2.middleRows(startIndex, count);
		auto yMean	= y.nanMean<float>().eval();
		auto ySD	= y.nanStdDev<float>().eval();

		auto numer	= ((x(0) - xMean) * (y(0) - yMean)).eval();

		for (auto jj = 1; jj < count; jj++) {
			numer	= (numer + ((x(jj) - xMean) * (y(jj) - yMean))).eval();
		}

		//auto numer	= ((x - xMean) * (y - yMean)).nanClear().colwise().sum().eval();
		auto denom	= (xSD * ySD).nanClear().eval();

		alpha[0]	= ((numer / denom) * reversion()).eval();
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createCorrelation(const pesa::ConfigSection& config) {
	return new pesa::Correlation((const pesa::ConfigSection&)config);
}

