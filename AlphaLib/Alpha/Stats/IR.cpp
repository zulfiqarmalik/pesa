/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// IR.cpp
///
/// Created on: 25 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class IR : public IAlpha {
	public:
								IR(const ConfigSection& config);
		virtual 				~IR();

		virtual void 			run(const RuntimeData& rt);
	};

	IR::IR(const ConfigSection& config) : IAlpha(config, "IR") {
	}

	IR::~IR() {
	}

	void IR::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& data 		= F_ARG0();
		auto& alpha		= this->alpha();

		CHK_DATA_ALPHA(alpha, data);

		auto startIndex = di - m_pinfo.days + m_pinfo.delta;
		auto count = m_pinfo.days;
		auto block = data.middleRows(startIndex, count).eval();
		EigenRowMatrixf mean(1, block.cols());
		auto stddev = block.nanStdDev(&mean).eval();

		alpha[0] = ((mean.row(0) / stddev).eval() * reversion()).eval();

		//II_LOOP() {
		//	std::vector<float> history;

		//	JJ_LOOP() {
		//		float backValue = data(di - jj, ii);

		//		if (!std::isnan(backValue))
		//			history.push_back(backValue);
		//	}

		//	if (!history.size()) {
		//		alpha(0, ii) = IAlpha::invalid();
		//		continue;
		//	}

		//	float mean = Stats::mean(&history[0], history.size());
		//	float stddev = Stats::standardDeviation(&history[0], history.size());

		//	if (stddev != 0.0f)
		//		alpha(0, ii) = (mean / stddev) * reversion();
		//	else
		//		alpha(0, ii) = IAlpha::invalid();
		//}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the siIRator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createIR(const pesa::ConfigSection& config) {
	return new pesa::IR((const pesa::ConfigSection&)config);
}

