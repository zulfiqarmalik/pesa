/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// M_VWAPs.cpp
///
/// Created on: 25 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class M_VWAP : public IAlpha {
	public:
								M_VWAP(const ConfigSection& config);
		virtual 				~M_VWAP();

		virtual void 			run(const RuntimeData& rt);
	};

	M_VWAP::M_VWAP(const ConfigSection& config) : IAlpha(config, "M_VWAP") {
	}

	M_VWAP::~M_VWAP() {
	}

	void M_VWAP::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& data 		= F_ARG0();
		auto& alpha		= this->alpha();

		CHK_DATA_ALPHA(alpha, data);

		alpha.setMemory(0.0f);

		JJ_LOOP_O(1) {
			alpha[0] = alpha[0] + data[di - jj];
		}

		alpha[0] = ((alpha[0] - data[di]) * reversion()).eval();

		//II_LOOP() {
		//	/// Calculate the momentum of the previous days ...
		//	JJ_LOOP_O(1) {
		//		alpha(0, ii) += data(di - jj, ii);
		//	}

		//	/// Then subtract the current value from it!
		//	alpha(0, ii) -= data(di, ii) * reversion();
		//}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createM_VWAP(const pesa::ConfigSection& config) {
	return new pesa::M_VWAP((const pesa::ConfigSection&)config);
}

