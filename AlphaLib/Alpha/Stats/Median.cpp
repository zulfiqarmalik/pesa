/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Median.cpp
///
/// Created on: 25 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class Median : public IAlpha {
	public:
								Median(const ConfigSection& config);
		virtual 				~Median();

		virtual void 			run(const RuntimeData& rt);
	};

	Median::Median(const ConfigSection& config) : IAlpha(config, "Median") {
	}

	Median::~Median() {
	}

	void Median::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& data 		= F_ARG0();
		//auto& alpha		= this->alpha();

		//CHK_DATA_ALPHA(alpha, data);

		//auto startIndex = di - m_pinfo.days + m_pinfo.delta;
		//auto count = m_pinfo.days;
		//auto block = data.innerMat().middleRows(startIndex, count); 

		//alpha[0] = block.nanMedianUnsorted<float>();

		II_LOOP() {
			alpha(0, ii) = IAlpha::median(data, di, ii) * reversion();
		}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createMedian(const pesa::ConfigSection& config) {
	return new pesa::Median((const pesa::ConfigSection&)config);
}

