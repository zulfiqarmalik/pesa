/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Skews.cpp
///
/// Created on: 25 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"

namespace pesa {
	#pragma warning(disable : 4503)

	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class Skew : public IAlpha {
	public:
								Skew(const ConfigSection& config);
		virtual 				~Skew();

		virtual void 			run(const RuntimeData& rt);
	};

	Skew::Skew(const ConfigSection& config) : IAlpha(config, "Skew") {
	}

	Skew::~Skew() {
	}

	void Skew::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& data 		= F_ARG0();
		auto& alpha		= this->alpha();

		CHK_DATA_ALPHA(alpha, data);

		auto startIndex = di - m_pinfo.days + m_pinfo.delta;
		if (startIndex < 0)                 
			return;                         
		auto count = m_pinfo.days;
		auto block = data.middleRows(startIndex, count);

		alpha[0] = ((block.nanSkew<float>()) * reversion()).eval();

		//II_LOOP() {
		//	std::vector<float> history;

		//	JJ_LOOP() {
		//		float backValue = data(di - jj, ii);

		//		if (!std::isnan(backValue))
		//			history.push_back(backValue);
		//	}

		//	if (!history.size()) {
		//		alpha(0, ii) = IAlpha::invalid();
		//		continue;
		//	}

		//	alpha(0, ii) = Stats::skew(&history[0], history.size()) * reversion();
		//}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the siSkewator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createSkew(const pesa::ConfigSection& config) {
	return new pesa::Skew((const pesa::ConfigSection&)config);
}

