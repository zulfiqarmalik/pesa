/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// StdDevs.cpp
///
/// Created on: 25 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class StdDev : public IAlpha {
	public:
								StdDev(const ConfigSection& config);
		virtual 				~StdDev();

		virtual void 			run(const RuntimeData& rt);
	};

	StdDev::StdDev(const ConfigSection& config) : IAlpha(config, "StdDev") {
	}

	StdDev::~StdDev() {
	}

	void StdDev::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& data 		= F_ARG0();
		auto& alpha		= this->alpha();

		CHK_DATA_ALPHA(alpha, data);

		auto startIndex = (di - m_pinfo.days) + m_pinfo.delta;
        if (startIndex < 0)                 
			return;

		auto count = m_pinfo.days;
		auto block = data.middleRows(startIndex, count).eval();
		//alpha[0] = block.nanMean<float>();
		alpha[0] = (block.nanStdDev<float>().eval() * reversion()).eval();

		//II_LOOP() {
		//	alpha(0, ii) = IAlpha::stddev(data, di, ii) * reversion();
		//}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createStdDev(const pesa::ConfigSection& config) {
	return new pesa::StdDev((const pesa::ConfigSection&)config);
}

