/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// ZScore.cpp
///
/// Created on: 31 Oct 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// ZScore
	////////////////////////////////////////////////////////////////////////////
	class ZScore : public IAlpha {
	public:
								ZScore(const ConfigSection& config) : IAlpha(config, "ZScore") {}
		virtual 				~ZScore() {}

		virtual void 			run(const RuntimeData& rt);
	};

	void ZScore::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& data 		= F_ARG0();
		auto& alpha		= this->alpha();

		CHK_DATA_ALPHA(alpha, data);

		auto stStartIndex = di - m_pinfo.shortTermDays + m_pinfo.delta;
		auto ltStartIndex = di - m_pinfo.longTermDays + m_pinfo.delta;
		auto stBlock = data.middleRows(stStartIndex, m_pinfo.shortTermDays).eval();
		auto ltBlock = data.middleRows(ltStartIndex, m_pinfo.longTermDays).eval();

		auto stMean = stBlock.nanMean<float>();
		EigenRowMatrixf ltMean(1, ltBlock.cols());

		auto ltStdDev = ltBlock.nanStdDev<float>(&ltMean).eval();

		alpha[0] = (((stMean - ltMean) / ltStdDev) * reversion()).eval();

		//II_LOOP() {
		//	float stMean = IAlpha::mean(data, di, ii, m_pinfo.shortTermDays);
		//	float ltMean = IAlpha::mean(data, di, ii, m_pinfo.longTermDays);
		//	float ltStdDev = IAlpha::stddev(data, di, ii, m_pinfo.longTermDays);
		//	float ratio = (stMean - ltMean) / ltStdDev;

		//	alpha(0, ii) = ratio * reversion();
		//}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createZScore(const pesa::ConfigSection& config) {
	return new pesa::ZScore((const pesa::ConfigSection&)config);
}

