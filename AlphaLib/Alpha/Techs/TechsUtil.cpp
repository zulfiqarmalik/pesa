/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// TechsUtil.cpp
///
/// Created on: 26 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "TechsUtil.h"
#include "Poco/String.h"

namespace pesa {
	//////////////////////////////////////////////////////
	/// Techs_Base
	//////////////////////////////////////////////////////
	Techs_Base::Techs_Base(const ConfigSection& config, const char* logChannel) : IAlpha(config, logChannel) {
	}

	Techs_Base::~Techs_Base() {
	}

	void Techs_Base::getHistory(const RuntimeData& rt, const FloatMatrixData& data, FloatVec& history, size_t& startIndex, IntDay& numHistoryDays, IntDay di, size_t ii) {
		ASSERT(numHistoryDays > 0, "Num history days must be > 0!");

		if (numHistoryDays > di)
			numHistoryDays = di;

		history.resize(numHistoryDays);

		memset(&history[0], 0, sizeof(float) * history.size());
		startIndex = 0;
		IntDay numValidValues = 0;
		
		JJ_LOOP_N(numHistoryDays) {
			float backValue = data(di - jj, ii);
			
			if (!std::isnan(backValue)) {
				startIndex = (history.size() - 1) - numValidValues++;
				history[startIndex] = backValue;
			}
		}

		numHistoryDays = numValidValues;
	}

	float Techs_Base::calc(const RuntimeData& rt, size_t ii, size_t) {
		return 0.0f;
	}

	void Techs_Base::run(const RuntimeData& rt) {
		size_t resultIndex = arg<size_t>("index", 0);

		II_LOOP() {
			alpha(0, ii) = calc(rt, ii, resultIndex);
		}
	}

	//////////////////////////////////////////////////////
	/// Techs_MovingAverage
	//////////////////////////////////////////////////////
	float Techs_MovingAverage::maCalc(const RuntimeData& rt, size_t ii, IntDay days, IntDay numHistoryDays) {
		IntDay di = rt.di - delay();

		if (!universe()->isValid(rt.di, ii))
			return std::nanf("");

		IntDay numValidValues = numHistoryDays; /// We input the number of days and get the number of valid days back ...
		auto& data = F_ARG0();
		FloatVec history;
		size_t startIndex = 0;

		//getHistory(rt, data, history, startIndex, numValidValues, di, ii);
		const FloatMatrixData* dataTranspose = data.nanTranspose(0.0f);
		size_t colSize = dataTranspose->cols();
		size_t offset = colSize * ii + (di - days); /// We need data starting from [di - days, di]
		const float* tdataPtr = dataTranspose->fdata() + offset;

		TA_RealVec result(numValidValues); //= IAlpha::invalid();
		TA_Integer outBeg;
		TA_Integer outNbElement;
		auto& alpha = this->alpha();

		/// Only calculate the moving average if we have some valid data ...
		if (result.size())
			memset(&result[0], 0, sizeof(PTA_Real) * result.size());
		else
			return 0.0f; 

		TA_RetCode	errCode = m_func((int)startIndex, (int)numValidValues, tdataPtr, (int)days, maType(), &outBeg, &outNbElement, &result[0]);

		if (errCode == TA_SUCCESS && outNbElement > 0) 
			return (float)result[0] * reversion();

		return IAlpha::invalid();
	}

	void Techs_MovingAverage::run(const RuntimeData& rt) {
		IntDay numHistoryDays = historyCount();

		II_LOOP() {
			alpha(0, ii) = maCalc(rt, ii, m_pinfo.days, numHistoryDays);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	/// Techs_Generic
	//////////////////////////////////////////////////////////////////////////
	Techs_Generic::Techs_Generic(const ConfigSection& config, const char* logChannel, TA_FuncGeneric* func)
		: Techs_Base(config, logChannel)
		, m_func(func) {
		//ASSERT(defArgsLength, "Invalid defArgs passed to generic Techs function!");
		//for (size_t i = 0; i < defArgsLength; i++)
		//	m_defArgs.push_back(std::string(defArgs[i]));
	}

	void Techs_Generic::initOptArgs() {
		if (m_optArgNames.size() == m_optArgs.size())
			return;

		for (const auto& name : m_optArgNames) {
			bool isInt = true;
			bool isMA = false;
			std::string cname = name;

			if (cname.find("f.") != std::string::npos) {
				cname = Poco::replace(cname, "f.", "");
				isInt = false;
			}
			else if (cname.find("ma.") != std::string::npos) {
				cname = Poco::replace(cname, "ma.", "");
				isMA = true;
				/// Its still an integer so we let it pass through
			}

			if (isInt) {
				if (!isMA)
					m_optArgs.push_back((float)arg<int>(cname, TA_INTEGER_DEFAULT));
				else
					m_optArgs.push_back((float)arg<int>(cname, (int)TA_MAType_SMA));
			}
			else 
				m_optArgs.push_back(arg<float>(cname, (float)TA_REAL_DEFAULT));
		}
	}

	float Techs_Generic::calc(const RuntimeData& rt, size_t ii, size_t resultIndex) {
		IntDay di = rt.di - delay();
		TAInput input;

		if (m_optArgNames.size() && !m_optArgs.size())
			initOptArgs();

		input.optArgs = m_optArgs;
		input.inData.resize(m_defArgs.size());
		FloatMatrixDataPtrVec data(m_defArgs.size());
		SizeVec startIndex(m_defArgs.size());
		IntVec numValidValues(m_defArgs.size());

		for (size_t i = 0; i < m_defArgs.size(); i++) {
			data[i] = F_ARG_OR(i, m_defArgs[i]);
			ASSERT(data[i], "Unable to get argument: " << i << " / " << m_defArgs[i]);

			startIndex[i] = 0;
			numValidValues[i] = m_pinfo.days;
			//getHistory(rt, *data[i].get(), input.inData[i], startIndex[i], numValidValues[i], di, ii);

			const FloatMatrixData* dataTranspose = data[i]->nanTranspose(0.0f);
			size_t colSize = dataTranspose->cols();
			size_t offset = colSize * ii + (di - m_pinfo.days); /// We need data starting from [di - days, di]
			const float* tdataPtr = dataTranspose->fdata() + offset;
			input.inData[i] = tdataPtr;

			if (input.startIdx == -1 || startIndex[i] > input.startIdx) {
				input.startIdx = (int)startIndex[i];
				input.endIdx = numValidValues[i];
			}
		}

		TAOutput output;

		output.results.resize(m_numOutputs);

		for (size_t i = 0; i < m_numOutputs; i++) {
			output.results[i].resize(input.endIdx + 1);
			memset(&output.results[i][0], 0, sizeof(PTA_Real) * output.results[i].size());
		}

		TA_RetCode errCode = m_func(input, output);

		if (errCode == TA_SUCCESS && output.outNBElement > 0) {
			ASSERT(resultIndex < output.results.size(), "Cannot fetch index: " << resultIndex << " as output. Max available: " << output.results.size());
			return (float)output.results[resultIndex][0] * reversion();
		}

		return 0.0f;
	}

	//class Techs_AD : public Techs_Generic {
	//public:
	//	Techs_AD(const ConfigSection& config, const char* logChannel)
	//		: Techs_Generic(config, logChannel, TA_AD) {
	//		Techs_Generic::m_defArgs = g_args4;
	//		Techs_Generic::m_optArgNames = g_optArgs0;
	//	}
	//};

	//////////////////////////////////////////////////////
	/// Techs_In4_NoTP_Out1
	//////////////////////////////////////////////////////
	//float Techs_In4_NoTP_Out1::calc(const RuntimeData& rt, size_t ii) {
	//	IntDay di = rt.di - delay();
	//	static const size_t count = 4;

	//	FloatMatrixDataPtr data[count] = {
	//		F_ARG0_OR("price.high"), F_ARG1_OR("price.low"), F_ARG2_OR("price.close"), F_ARG2_OR("price.volume"),
	//	};

	//	FloatVec history[count];
	//	size_t startIndex[count];
	//	IntDay numValidValues[count];
	//	size_t input.startIdx = -1;
	//	IntDay input.endIdx = -1;

	//	for (size_t i = 0; i < count; i++) {
	//		startIndex[i] = 0;
	//		numValidValues[i] = m_pinfo.days;
	//		getHistory(rt, *data[i].get(), history[i], startIndex[i], numValidValues[i], di, ii);

	//		if (input.startIdx == -1 || startIndex[i] > input.startIdx) {
	//			input.startIdx = startIndex[i];
	//			input.endIdx = numValidValues[i];
	//		}
	//	}

	//	ASSERT(input.startIdx > 0, "Unable to get a good starting index to calculate techs!");

	//	std::vector<PTA_Real> result(input.endIdx); //= IAlpha::invalid();
	//	TA_Integer outBeg;
	//	TA_Integer outNbElement;
	//	auto& alpha = this->alpha();
	//	memset(&result[0], 0, sizeof(PTA_Real) * result.size());
	//	TA_RetCode	errCode = TA_SUCCESS; //m_func((int)input.startIdx, (int)input.endIdx, &history[0][0], &history[1][0], &history[2][0], &history[3][0], &outBeg, &outNbElement, &result[0]);

	//	if (errCode == TA_SUCCESS && outNbElement > 0)
	//		return (float)result[0] * reversion();

	//	return IAlpha::invalid();
	//}
} /// namespace pesa

