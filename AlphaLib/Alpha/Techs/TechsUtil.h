/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// TechsUtil.h
///
/// Created on: 26 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#ifndef AlphaLib_TechsUtil_h_
#define AlphaLib_TechsUtil_h_

#include "Framework/Framework.h"
#include "ta_func.h"

#define DECL_TECHS_CLASS(Klass, Func) \
	protected: \
		Func*					m_func; \
	public: \
	\
								Klass(const ConfigSection& config, const char* logChannel, Func* func) : Techs_Base(config, logChannel), m_func(func) { \
									ASSERT(m_func, "Invalid TA-LIB callback function specified!"); \
								} \
		virtual 				~Klass() {} \

namespace pesa {
	typedef TA_Real				PTA_Real;
	typedef std::vector<PTA_Real> TA_RealVec;

	struct TAOutput {
		int						outBegIdx = 0; 
		int						outNBElement = 0; 
		std::vector<TA_RealVec>	results;
	};

	struct TAInput {
		int 					startIdx = -1;
		int 					endIdx = -1;
		std::vector<const float*> inData;
		FloatVec				optArgs;
	};

	typedef TA_RetCode			TA_MA1(int startIdx, int endIdx, const float inReal[], int optInTimePeriod, TA_MAType optInMAType, int *outBegIdx, int *outNBElement, double out[]);

	typedef TA_RetCode			TA_In1_Out1(int startIdx, int endIdx, const float in1[], int optInTimePeriod, int *outBegIdx, int *outNBElement, double out[]);
	typedef TA_RetCode			TA_In1_NoTP_Out1(int startIdx, int endIdx, const float in1[], int *outBegIdx, int *outNBElement, double out[]);
	typedef TA_RetCode			TA_In1_NoTP_Out1i(int startIdx, int endIdx, const float in1[], int *outBegIdx, int *outNBElement, int out[]);
	typedef TA_RetCode			TA_In1_NoTP_Out2(int startIdx, int endIdx, const float in1[], int *outBegIdx, int *outNBElement, double out1[], double out2[]);
	typedef TA_RetCode			TA_In1_BBANDS(int startIdx, int endIdx, const float in1[], int optArg0, double optArg1, double optArg2, TA_MAType maType, int *outBegIdx, int *outNBElement, double out1[], double out2[], double out3[]);
	typedef TA_RetCode			TA_In1_MACD(int startIdx, int endIdx, const float in1[], int optArg0, int optArg1, int optArg2, int *outBegIdx, int *outNBElement, double out1[], double out2[], double out3[]);
	typedef TA_RetCode			TA_In1_MACDFix(int startIdx, int endIdx, const float in1[], int optArg0, int *outBegIdx, int *outNBElement, double out1[], double out2[], double out3[]);
	typedef TA_RetCode			TA_In1_MACDEXT(int startIdx, int endIdx, const float in1[], int opt0, TA_MAType opt1, int opt2, TA_MAType opt3, int opt4, TA_MAType opt5, int *outBegIdx, int *outNBElement, double out1[], double out2[], double out3[]);
	typedef TA_RetCode			TA_In1_APO(int startIdx, int endIdx, const float in1[], int optArg0, int optArg1, TA_MAType maType, int *outBegIdx, int *outNBElement, double out1[]);

	typedef TA_RetCode			TA_In2_Out1(int startIdx, int endIdx, const float in1[], const float in2[], int optInTimePeriod, int *outBegIdx, int *outNBElement, double out[]);
	typedef TA_RetCode			TA_In2_Out2(int startIdx, int endIdx, const float in1[], const float in2[], int optInTimePeriod, int *outBegIdx, int *outNBElement, double out1[], double out2[]);
	typedef TA_RetCode			TA_In2_NoTP_Out1(int startIdx, int endIdx, const float in1[], const float in2[], int *outBegIdx, int *outNBElement, double out[]);
	typedef TA_RetCode			TA_In2_MAVP(int startIdx, int endIdx, const float in1[], const float in2[], int opt0, int opt1, TA_MAType ma, int *outBegIdx, int *outNBElement, double out[]);
	typedef TA_RetCode			TA_In2_SAR(int startIdx, int endIdx, const float in1[], const float in2[], double opt0, double opt1, int *outBegIdx, int *outNBElement, double out[]);
	typedef TA_RetCode			TA_In2_SARE(int startIdx, int endIdx, const float in1[], const float in2[], double opt0, double opt1, double opt2, double opt3, double opt4, double opt5, double opt6, double opt7,
		int *outBegIdx, int *outNBElement, double out[]);

	typedef TA_RetCode			TA_In3_Out1(int startIdx, int endIdx, const float in1[], const float in2[], const float in3[], int optInTimePeriod, int *outBegIdx, int *outNBElement, double out[]);
	typedef TA_RetCode			TA_In3_Out3(int startIdx, int endIdx, const float in1[], const float in2[], const float in3[], int optInTimePeriod, int *outBegIdx, int *outNBElement, double out1[], double out2[], double out3[]);
	typedef TA_RetCode			TA_In3_USOC(int startIdx, int endIdx, const float in1[], const float in2[], const float in3[], int opt0, int opt1, int opt2, int *outBegIdx, int *outNBElement, double out1[]);
	typedef TA_RetCode			TA_In3_STOCH(int startIdx, int endIdx, const float in1[], const float in2[], const float in3[], int opt0, int opt1, TA_MAType opt2, int opt3, TA_MAType opt4, int *outBegIdx, int *outNBElement, double out1[], double out2[]);
	typedef TA_RetCode			TA_In3_STOCHF(int startIdx, int endIdx, const float in1[], const float in2[], const float in3[], int opt0, int opt1, TA_MAType opt2, int *outBegIdx, int *outNBElement, double out1[], double out2[]);
	typedef TA_RetCode			TA_In3_NoTP_Out1(int startIdx, int endIdx, const float in1[], const float in2[], const float in3[], int *outBegIdx, int *outNBElement, double out[]);

	typedef TA_RetCode			TA_In4_Out1(int startIdx, int endIdx, const float in1[], const float in2[], const float in3[], const float in4[], int optInTimePeriod, int *outBegIdx, int *outNBElement, double out[]);
	typedef TA_RetCode			TA_In4_NoTP_Out1(int startIdx, int endIdx, const float in1[], const float in2[], const float in3[], const float in4[], int *outBegIdx, int *outNBElement, double out[]);
	typedef TA_RetCode			TA_In4_D0_Out1i(int startIdx, int endIdx, const float in1[], const float in2[], const float in3[], const float in4[], double arg, int *outBegIdx, int *outNBElement, int out[]);
	typedef TA_RetCode			TA_In4_NoTP_Out1i(int startIdx, int endIdx, const float in1[], const float in2[], const float in3[], const float in4[], int *outBegIdx, int *outNBElement, int out[]);

	typedef TA_RetCode			TA_In1_FS_Out1(int startIdx, int endIdx, const float in1[], int optFast, int optSlow, int *outBegIdx, int *outNBElement, double out[]);
	typedef TA_RetCode			TA_In2_FS_Out1(int startIdx, int endIdx, const float in1[], const float in2[], int optFast, int optSlow, int *outBegIdx, int *outNBElement, double out[]);
	typedef TA_RetCode			TA_In3_FS_Out1(int startIdx, int endIdx, const float in1[], const float in2[], const float in3[], int optFast, int optSlow, int *outBegIdx, int *outNBElement, double out[]);
	typedef TA_RetCode			TA_In4_FS_Out1(int startIdx, int endIdx, const float in1[], const float in2[], const float in3[], const float in4[], int optFast, int optSlow, int *outBegIdx, int *outNBElement, double out[]);

	typedef TA_RetCode			TA_FuncGeneric(const TAInput& input, TAOutput& output);

	//////////////////////////////////////////////////////////////////////////
#define DECL_TA_GENERIC(Name)		extern TA_RetCode Name(int startIdx, int endIdx, const std::vector<FloatVec>& inputs, IntVec& optArgs, int *outBegIdx, int *outNBElement, double out[]);

	DECL_TA_GENERIC(TA_AD);

	//////////////////////////////////////////////////////
	/// Techs_Base
	//////////////////////////////////////////////////////
	class Techs_Base : public IAlpha {
	protected:
		void 					getHistory(const RuntimeData& rt, const FloatMatrixData& data, FloatVec& history, size_t& startIndex, IntDay& numHistoryDays, IntDay di, size_t ii);

	public:
								Techs_Base(const ConfigSection& config, const char* logChannel);
		virtual 				~Techs_Base();

		virtual float			calc(const RuntimeData& rt, size_t ii, size_t resultIndex);
		virtual void 			run(const RuntimeData& rt);
	};

	//////////////////////////////////////////////////////
	/// Techs_MovingAverage
	//////////////////////////////////////////////////////
	class Techs_MovingAverage : public Techs_Base {
		DECL_TECHS_CLASS(Techs_MovingAverage, TA_MA1);

	protected:
		virtual float			maCalc(const RuntimeData& rt, size_t ii, IntDay days, IntDay historyCount);

	public:
		virtual void 			run(const RuntimeData& rt);
		virtual TA_MAType		maType() const = 0;
		virtual IntDay			historyCount() const { return m_pinfo.days; }
	};

	//////////////////////////////////////////////////////
	class Techs_Generic : public Techs_Base {
	protected:
		TA_FuncGeneric*			m_func = nullptr;
		StringVec				m_defArgs;
		StringVec				m_optArgNames;
		FloatVec				m_optArgs;
		size_t					m_numOutputs = 1;

		void					initOptArgs();

	public:
								Techs_Generic(const ConfigSection& config, const char* logChannel, TA_FuncGeneric* func);
		float					calc(const RuntimeData& rt, size_t ii, size_t resultIndex);
	};
} /// namespace pesa 

#undef DECL_TECHS_CLASS

#endif /// AlphaLib_TechsUtil_h_
