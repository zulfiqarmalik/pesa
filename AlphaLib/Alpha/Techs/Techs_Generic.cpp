/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Techs_MA.cpp
///
/// Created on: 25 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "TechsUtil.h"

namespace pesa { 
#define RESULT_ITOF() \
	if (rc != TA_SUCCESS) \
		return rc; \
	for (size_t i = 0; i < result.size(); i++) \
		output.results[0][i] = result[i]; \
	return rc;

	//////////////////////////////////////////////////////////////////////////
	/// Generic wrappers!
	//////////////////////////////////////////////////////////////////////////
	TA_RetCode TA_FuncGeneric1(TA_In1_Out1* func, const TAInput& input, TAOutput& output) {
		return func(input.startIdx, input.endIdx, input.inData[0], (int)input.optArgs[0], &output.outBegIdx, &output.outNBElement, &output.results[0][0]);
	}

	TA_RetCode TA_FuncGeneric1(TA_In1_NoTP_Out1* func, const TAInput& input, TAOutput& output) {
		return func(input.startIdx, input.endIdx, input.inData[0], &output.outBegIdx, &output.outNBElement, &output.results[0][0]);
	}

	TA_RetCode TA_FuncGeneric1(TA_In1_NoTP_Out1i* func, const TAInput& input, TAOutput& output) {
		IntVec result(output.results[0].size());
		TA_RetCode rc = func(input.startIdx, input.endIdx, input.inData[0], &output.outBegIdx, &output.outNBElement, &result[0]);
		RESULT_ITOF();
	}

	TA_RetCode TA_FuncGeneric1(TA_In1_BBANDS* func, const TAInput& input, TAOutput& output) {
		return func(input.startIdx, input.endIdx, input.inData[0], (int)input.optArgs[0], input.optArgs[1], input.optArgs[2], (TA_MAType)(int)input.optArgs[2], &output.outBegIdx, &output.outNBElement, &output.results[0][0], &output.results[1][0], &output.results[2][0]);
	}

	TA_RetCode TA_FuncGeneric1(TA_In1_MACD* func, const TAInput& input, TAOutput& output) {
		return func(input.startIdx, input.endIdx, input.inData[0], (int)input.optArgs[0], (int)input.optArgs[1], (int)input.optArgs[2], &output.outBegIdx, &output.outNBElement, &output.results[0][0], &output.results[1][0], &output.results[2][0]);
	}

	TA_RetCode TA_FuncGeneric1(TA_In1_MACDEXT* func, const TAInput& input, TAOutput& output) {
		return func(input.startIdx, input.endIdx, input.inData[0], 
			(int)input.optArgs[0], (TA_MAType)(int)input.optArgs[1], (int)input.optArgs[2], (TA_MAType)(int)input.optArgs[3], (int)input.optArgs[4], (TA_MAType)(int)input.optArgs[5],
			&output.outBegIdx, &output.outNBElement, &output.results[0][0], &output.results[1][0], &output.results[2][0]);
	}

	TA_RetCode TA_FuncGeneric1(TA_In1_MACDFix* func, const TAInput& input, TAOutput& output) {
		return func(input.startIdx, input.endIdx, input.inData[0], (int)input.optArgs[0], &output.outBegIdx, &output.outNBElement, &output.results[0][0], &output.results[1][0], &output.results[2][0]);
	}

	TA_RetCode TA_FuncGeneric1(TA_In1_NoTP_Out2* func, const TAInput& input, TAOutput& output) {
		return func(input.startIdx, input.endIdx, input.inData[0], &output.outBegIdx, &output.outNBElement, &output.results[0][0], &output.results[1][0]);
	}

	TA_RetCode TA_FuncGeneric1(TA_In1_APO* func, const TAInput& input, TAOutput& output) {
		return func(input.startIdx, input.endIdx, input.inData[0], (int)input.optArgs[0], (int)input.optArgs[1], (TA_MAType)(int)input.optArgs[2], &output.outBegIdx, &output.outNBElement, &output.results[0][0]);
	}

	TA_RetCode TA_FuncGeneric1(TA_In1_FS_Out1* func, const TAInput& input, TAOutput& output) {
		return func(input.startIdx, input.endIdx, input.inData[0], (int)input.optArgs[0], (int)input.optArgs[1], &output.outBegIdx, &output.outNBElement, &output.results[0][0]);
	}

	TA_RetCode TA_FuncGeneric1(TA_In2_FS_Out1* func, const TAInput& input, TAOutput& output) {
		return func(input.startIdx, input.endIdx, input.inData[0], input.inData[1], (int)input.optArgs[0], (int)input.optArgs[1], &output.outBegIdx, &output.outNBElement, &output.results[0][0]);
	}

	TA_RetCode TA_FuncGeneric1(TA_In3_FS_Out1* func, const TAInput& input, TAOutput& output) {
		return func(input.startIdx, input.endIdx, input.inData[0], input.inData[1], input.inData[2], (int)input.optArgs[0], (int)input.optArgs[1], &output.outBegIdx, &output.outNBElement, &output.results[0][0]);
	}

	//////////////////////////////////////////////////////////////////////////
	TA_RetCode TA_FuncGeneric2(TA_In2_Out1* func, const TAInput& input, TAOutput& output) {
		return func(input.startIdx, input.endIdx, input.inData[0], input.inData[1], (int)input.optArgs[0], &output.outBegIdx, &output.outNBElement, &output.results[0][0]);
	}

	TA_RetCode TA_FuncGeneric2(TA_In2_Out2* func, const TAInput& input, TAOutput& output) {
		return func(input.startIdx, input.endIdx, input.inData[0], input.inData[1], (int)input.optArgs[0], &output.outBegIdx, &output.outNBElement, &output.results[0][0], &output.results[1][0]);
	}

	TA_RetCode TA_FuncGeneric2(TA_In2_MAVP* func, const TAInput& input, TAOutput& output) {
		return func(input.startIdx, input.endIdx, input.inData[0], input.inData[1], (int)input.optArgs[0], (int)input.optArgs[1], (TA_MAType)(int)input.optArgs[2], &output.outBegIdx, &output.outNBElement, &output.results[0][0]);
	}

	TA_RetCode TA_FuncGeneric2(TA_In2_SAR* func, const TAInput& input, TAOutput& output) {
		return func(input.startIdx, input.endIdx, input.inData[0], input.inData[1], input.optArgs[0], input.optArgs[1], &output.outBegIdx, &output.outNBElement, &output.results[0][0]);
	}

	TA_RetCode TA_FuncGeneric2(TA_In2_SARE* func, const TAInput& input, TAOutput& output) {
		return func(input.startIdx, input.endIdx, input.inData[0], input.inData[1], input.optArgs[0], 
			input.optArgs[1], input.optArgs[2], input.optArgs[3], input.optArgs[4], input.optArgs[5], input.optArgs[6], input.optArgs[7],
			&output.outBegIdx, &output.outNBElement, &output.results[0][0]);
	}

	TA_RetCode TA_FuncGeneric2(TA_In2_NoTP_Out1* func, const TAInput& input, TAOutput& output) {
		return func(input.startIdx, input.endIdx, input.inData[0], input.inData[1], &output.outBegIdx, &output.outNBElement, &output.results[0][0]);
	}

	//////////////////////////////////////////////////////////////////////////
	TA_RetCode TA_FuncGeneric3(TA_In3_Out1* func, const TAInput& input, TAOutput& output) {
		return func(input.startIdx, input.endIdx, input.inData[0], input.inData[1], input.inData[2], (int)input.optArgs[0], &output.outBegIdx, &output.outNBElement, &output.results[0][0]);
	}

	TA_RetCode TA_FuncGeneric3(TA_In3_Out3* func, const TAInput& input, TAOutput& output) {
		return func(input.startIdx, input.endIdx, input.inData[0], input.inData[1], input.inData[2], (int)input.optArgs[0], &output.outBegIdx, &output.outNBElement, &output.results[0][0], &output.results[1][0], &output.results[2][0]);
	}

	TA_RetCode TA_FuncGeneric3(TA_In3_STOCH* func, const TAInput& input, TAOutput& output) {
		return func(input.startIdx, input.endIdx, input.inData[0], input.inData[1], input.inData[2], 
			(int)input.optArgs[0], (int)input.optArgs[1], (TA_MAType)(int)input.optArgs[2], (int)input.optArgs[3], (TA_MAType)(int)input.optArgs[4],
			&output.outBegIdx, &output.outNBElement, &output.results[0][0], &output.results[1][0]);
	}

	TA_RetCode TA_FuncGeneric3(TA_In3_STOCHF* func, const TAInput& input, TAOutput& output) {
		return func(input.startIdx, input.endIdx, input.inData[0], input.inData[1], input.inData[2],
			(int)input.optArgs[0], (int)input.optArgs[1], (TA_MAType)(int)input.optArgs[2], &output.outBegIdx, &output.outNBElement, &output.results[0][0], &output.results[1][0]);
	}

	TA_RetCode TA_FuncGeneric3(TA_In3_USOC* func, const TAInput& input, TAOutput& output) {
		return func(input.startIdx, input.endIdx, input.inData[0], input.inData[1], input.inData[2], (int)input.optArgs[0], (int)input.optArgs[1], (int)input.optArgs[2], &output.outBegIdx, &output.outNBElement, &output.results[0][0]);
	}

	TA_RetCode TA_FuncGeneric3(TA_In3_NoTP_Out1* func, const TAInput& input, TAOutput& output) {
		return func(input.startIdx, input.endIdx, input.inData[0], input.inData[1], input.inData[2], &output.outBegIdx, &output.outNBElement, &output.results[0][0]);
	}

	//////////////////////////////////////////////////////////////////////////
	TA_RetCode TA_FuncGeneric4(TA_In4_Out1* func, const TAInput& input, TAOutput& output) {
		return func(input.startIdx, input.endIdx, input.inData[0], input.inData[1], input.inData[2], input.inData[3], (double)input.optArgs[0], &output.outBegIdx, &output.outNBElement, &output.results[0][0]);
	}

	TA_RetCode TA_FuncGeneric4(TA_In4_NoTP_Out1* func, const TAInput& input, TAOutput& output) {
		return func(input.startIdx, input.endIdx, input.inData[0], input.inData[1], input.inData[2], input.inData[3], &output.outBegIdx, &output.outNBElement, &output.results[0][0]);
	}

	TA_RetCode TA_FuncGeneric4(TA_In4_FS_Out1* func, const TAInput& input, TAOutput& output) {
		return func(input.startIdx, input.endIdx, input.inData[0], input.inData[1], input.inData[2], input.inData[3], (int)input.optArgs[0], (int)input.optArgs[1], &output.outBegIdx, &output.outNBElement, &output.results[0][0]);
	}

	TA_RetCode TA_FuncGeneric4(TA_In4_NoTP_Out1i* func, const TAInput& input, TAOutput& output) {
		IntVec result(output.results[0].size());
		TA_RetCode rc = func(input.startIdx, input.endIdx, input.inData[0], input.inData[1], input.inData[2], input.inData[3], &output.outBegIdx, &output.outNBElement, &result[0]);
		RESULT_ITOF();
	}

	TA_RetCode TA_FuncGeneric4(TA_In4_D0_Out1i* func, const TAInput& input, TAOutput& output) {
		IntVec result(output.results[0].size());
		TA_RetCode rc = func(input.startIdx, input.endIdx, input.inData[0], input.inData[1], input.inData[2], input.inData[3], input.optArgs[0], &output.outBegIdx, &output.outNBElement, &result[0]);
		RESULT_ITOF();
	}

	//////////////////////////////////////////////////////////////////////////
	static StringVec g_optArgs0 	= { };
	static StringVec g_optArgs1 	= { "timePeriod" };
	static StringVec g_optArgsUOSC 	= { "timePeriod1", "timePeriod2", "timePeriod3" };
	static StringVec g_optArgs1Cdl 	= { "f.penetration" };
	static StringVec g_optArgs2 	= { "fastPeriod", "slowPeriod" };
	static StringVec g_optArgs3APO 	= { "fastPeriod", "slowPeriod", "ma.maType" };
	static StringVec g_optArgsBB	= { "timePeriod", "f.numDevUp", "f.numDevDown", "ma.maType" };
	static StringVec g_optArgs3MACD	= { "fastPeriod", "slowPeriod", "signalPeriod" };
	static StringVec g_optArgsMACDE	= { "fastPeriod", "ma.fastMAType", "slowPeriod", "ma.slowMAType", "signalPeriod", "ma.signalMAType" };
	static StringVec g_optArgs1MACD	= { "signalPeriod" };
	static StringVec g_optArgsMAVP	= { "minPeriod", "maxPeriod", "ma.maType" };
	static StringVec g_optArgsSAR	= { "acceleration", "maximum" };
	static StringVec g_optArgsSARE	= { "startValue", "offsetOnReverse", "accelerationInitLong", "accelerationLong", "accelerationMaxLong", "accelerationInitShort", "accelerationShort", "accelerationMaxShort" };
	static StringVec g_optArgsSTOCH	= { "fastK_Period", "slowK_Period", "ma.slowK_MAType", "slowD_Period", "ma.slowD_MAType" };
	static StringVec g_optArgsSTOCHF= { "fastK_Period", "fastD_Period", "ma.fastD_MAType" };

	static StringVec g_args1		= { Constants::s_defClosePrice };
	static StringVec g_args2		= { Constants::s_defHighPrice, Constants::s_defLowPrice };
	static StringVec g_args3		= { Constants::s_defHighPrice, Constants::s_defLowPrice, Constants::s_defClosePrice };
	static StringVec g_args4		= { Constants::s_defHighPrice, Constants::s_defLowPrice, Constants::s_defClosePrice, Constants::s_defVolume };

}

#define IMPL_TA_GENERIC(Name, TAFuncName, C) \
	TA_RetCode Name(const TAInput& input, TAOutput& output) { \
		return TA_FuncGeneric##C(TAFuncName, input, output); \
	}

#define IMPL_TA_GENERIC_NOTP(Name, TAFuncName, C) \
	TA_RetCode Name(const TAInput& input, TAOutput& output) { \
		return TA_FuncGeneric##C(TAFuncName, input, output); \
	}

#define Techs_Class(Name, InCount, OutCount, DefArgs, OptArgs) \
namespace pesa { \
	IMPL_TA_GENERIC(TA_##Name, ::TA_S_##Name, InCount); \
	class Techs_##Name : public Techs_Generic { \
	public: \
		Techs_##Name(const pesa::ConfigSection& config) \
			: Techs_Generic(config, (std::string("Techs_") + #Name).c_str(), pesa::TA_##Name) { \
			Techs_Generic::m_defArgs = DefArgs; \
			Techs_Generic::m_optArgNames = OptArgs; \
			Techs_Generic::m_numOutputs = OutCount; \
		} \
	}; \
} \
EXPORT_FUNCTION pesa::IComponent* createTechs_##Name(const pesa::ConfigSection& config) { \
	return new pesa::Techs_##Name((const pesa::ConfigSection&)config); \
}

#define Techs_ClassDef(Name, InCount, OutCount, OptCount)	Techs_Class(Name, InCount, OutCount, pesa::g_args##InCount, pesa::g_optArgs##OptCount)

Techs_ClassDef(ACOS, 				1, 1, 0);		/// ACOS - Vector Trigonometric ACos
Techs_ClassDef(AD, 					4, 1, 0);		/// AD - Chaikin A/D Line
Techs_ClassDef(ADOSC, 				4, 1, 1);		/// ADOSC - Chaikin A/D Oscillator
Techs_ClassDef(ADX, 				3, 1, 1);		/// ADX - Average Directional Movement Index
Techs_ClassDef(ADXR, 				3, 1, 1);		/// ADXR - Average Directional Movement Index Rating
Techs_ClassDef(APO, 				1, 1, 3APO);	/// APO - Absolute Price Oscillator
Techs_ClassDef(AROON, 				2, 2, 1);		/// AROON - Aroon
Techs_ClassDef(AROONOSC,			2, 2, 1);		/// AROONOSC - Aroon Oscillator
Techs_ClassDef(ATR,					3, 1, 1);		/// ATR - Average True Range
Techs_ClassDef(AVGPRICE,			4, 1, 0);		/// AVGPRICE - Average Price

Techs_ClassDef(BBANDS,				1, 3, BB);		/// BBANDS - Bollinger Bands
Techs_ClassDef(BETA,				2, 1, 1);		/// BETA - Beta
Techs_ClassDef(BOP,					4, 1, 0);		/// BOP - Balance Of Power
Techs_ClassDef(CCI,					3, 1, 1);		/// CCI - Commodity Channel Index

Techs_ClassDef(CDL2CROWS,			4, 1, 0);		/// CDL2CROWS - Two Crows
Techs_ClassDef(CDL3BLACKCROWS,		4, 1, 0);		/// CDL3BLACKCROWS - Three Black Crows
Techs_ClassDef(CDL3INSIDE,			4, 1, 0);		/// CDL3INSIDE - Three Inside Up/Down
Techs_ClassDef(CDL3LINESTRIKE,		4, 1, 0);		/// CDL3LINESTRIKE - Three-Line Strike 
Techs_ClassDef(CDL3OUTSIDE,			4, 1, 0);		/// CDL3OUTSIDE - Three Outside Up/Down
Techs_ClassDef(CDL3STARSINSOUTH,	4, 1, 0);		/// CDL3STARSINSOUTH - Three Stars In The South
Techs_ClassDef(CDL3WHITESOLDIERS,	4, 1, 0);		/// CDL3WHITESOLDIERS - Three Advancing White Soldiers
Techs_ClassDef(CDLABANDONEDBABY,	4, 1, 1Cdl);	/// CDLABANDONEDBABY - Abandoned Baby
Techs_ClassDef(CDLADVANCEBLOCK,		4, 1, 0);		/// CDLADVANCEBLOCK - Advance Block
Techs_ClassDef(CDLBELTHOLD,			4, 1, 0);		/// CDLBELTHOLD - Belt-hold
Techs_ClassDef(CDLBREAKAWAY,		4, 1, 0);		/// CDLBREAKAWAY - Breakaway
Techs_ClassDef(CDLCLOSINGMARUBOZU,	4, 1, 0);		/// CDLCLOSINGMARUBOZU - Closing Marubozu
Techs_ClassDef(CDLCONCEALBABYSWALL,	4, 1, 0);		/// CDLCONCEALBABYSWALL - Concealing Baby Swallow
Techs_ClassDef(CDLCOUNTERATTACK,	4, 1, 0);		/// CDLCOUNTERATTACK - Counterattack
Techs_ClassDef(CDLDARKCLOUDCOVER,	4, 1, 1Cdl);	/// CDLDARKCLOUDCOVER - Dark Cloud Cover
Techs_ClassDef(CDLDOJI,				4, 1, 0);		/// CDLDOJI - Doji
Techs_ClassDef(CDLDOJISTAR,			4, 1, 0);		/// CDLDOJISTAR - Doji Star
Techs_ClassDef(CDLDRAGONFLYDOJI,	4, 1, 0);		/// CDLDRAGONFLYDOJI - Dragonfly Doji
Techs_ClassDef(CDLENGULFING,		4, 1, 0);		/// CDLENGULFING - Engulfing Pattern
Techs_ClassDef(CDLEVENINGDOJISTAR, 	4, 1, 1Cdl);	/// CDLEVENINGDOJISTAR - Evening Doji Star
Techs_ClassDef(CDLEVENINGSTAR, 		4, 1, 1Cdl);	/// CDLEVENINGSTAR - Evening Star
Techs_ClassDef(CDLGAPSIDESIDEWHITE, 4, 1, 0);		/// CDLGAPSIDESIDEWHITE - Up/Down-gap side-by-side white lines
Techs_ClassDef(CDLGRAVESTONEDOJI, 	4, 1, 0);		/// CDLGRAVESTONEDOJI - Gravestone Doji
Techs_ClassDef(CDLHAMMER, 			4, 1, 0);		/// CDLHAMMER - Hammer
Techs_ClassDef(CDLHANGINGMAN, 		4, 1, 0);		/// CDLHANGINGMAN - Hanging Man
Techs_ClassDef(CDLHARAMI, 			4, 1, 0);		/// CDLHARAMI - Harami Pattern
Techs_ClassDef(CDLHARAMICROSS, 		4, 1, 0);		/// CDLHARAMICROSS - Harami Cross Pattern
Techs_ClassDef(CDLHIGHWAVE, 		4, 1, 0);		/// CDLHIGHWAVE - High-Wave Candle
Techs_ClassDef(CDLHIKKAKE, 			4, 1, 0);		/// CDLHIKKAKE - Hikkake Pattern
Techs_ClassDef(CDLHIKKAKEMOD, 		4, 1, 0);		/// CDLHIKKAKEMOD - Modified Hikkake Pattern
Techs_ClassDef(CDLHOMINGPIGEON, 	4, 1, 0);		/// CDLHOMINGPIGEON - Homing Pigeon
Techs_ClassDef(CDLIDENTICAL3CROWS, 	4, 1, 0);		/// CDLIDENTICAL3CROWS - Identical Three Crows
Techs_ClassDef(CDLINNECK, 			4, 1, 0);		/// CDLINNECK - In-Neck Pattern
Techs_ClassDef(CDLINVERTEDHAMMER, 	4, 1, 0);		/// CDLINVERTEDHAMMER - Inverted Hammer
Techs_ClassDef(CDLKICKING, 			4, 1, 0);		/// CDLKICKING - Kicking
Techs_ClassDef(CDLKICKINGBYLENGTH, 	4, 1, 0);		/// CDLKICKINGBYLENGTH - Kicking - bull/bear determined by the longer marubozu
Techs_ClassDef(CDLLADDERBOTTOM, 	4, 1, 0);		/// CDLLADDERBOTTOM - Ladder Bottom
Techs_ClassDef(CDLLONGLEGGEDDOJI, 	4, 1, 0);		/// CDLLONGLEGGEDDOJI - Long Legged Doji
Techs_ClassDef(CDLLONGLINE, 		4, 1, 0);		/// CDLLONGLINE - Long Line Candle
Techs_ClassDef(CDLMARUBOZU, 		4, 1, 0);		/// CDLMARUBOZU - Marubozu
Techs_ClassDef(CDLMATCHINGLOW, 		4, 1, 0);		/// CDLMATCHINGLOW - Matching Low
Techs_ClassDef(CDLMATHOLD, 			4, 1, 1Cdl);	/// CDLMATHOLD - Mat Hold
Techs_ClassDef(CDLMORNINGDOJISTAR, 	4, 1, 1Cdl);	/// CDLMORNINGDOJISTAR - Morning Doji Star
Techs_ClassDef(CDLMORNINGSTAR, 		4, 1, 1Cdl);	/// CDLMORNINGSTAR - Morning Star
Techs_ClassDef(CDLONNECK, 			4, 1, 0);		/// CDLONNECK - On-Neck Pattern
Techs_ClassDef(CDLPIERCING, 		4, 1, 0);		/// CDLPIERCING - Piercing Pattern
Techs_ClassDef(CDLRICKSHAWMAN, 		4, 1, 0);		/// CDLRICKSHAWMAN - Rickshaw Man
Techs_ClassDef(CDLRISEFALL3METHODS, 4, 1, 0);		/// CDLRISEFALL3METHODS - Rising/Falling Three Methods
Techs_ClassDef(CDLSEPARATINGLINES, 	4, 1, 0);		/// CDLSEPARATINGLINES - Separating Lines
Techs_ClassDef(CDLSHOOTINGSTAR, 	4, 1, 0);		/// CDLSHOOTINGSTAR - Shooting Star
Techs_ClassDef(CDLSHORTLINE, 		4, 1, 0);		/// CDLSHORTLINE - Short Line Candle
Techs_ClassDef(CDLSPINNINGTOP, 		4, 1, 0);		/// CDLSPINNINGTOP - Spinning Top
Techs_ClassDef(CDLSTALLEDPATTERN, 	4, 1, 0);		/// CDLSTALLEDPATTERN - Stalled Pattern
Techs_ClassDef(CDLSTICKSANDWICH, 	4, 1, 0);		/// CDLSTICKSANDWICH - Stick Sandwich
Techs_ClassDef(CDLTAKURI, 			4, 1, 0);		/// CDLTAKURI - Takuri (Dragonfly Doji with very long lower shadow)
Techs_ClassDef(CDLTASUKIGAP, 		4, 1, 0);		/// CDLTASUKIGAP - Tasuki Gap
Techs_ClassDef(CDLTHRUSTING, 		4, 1, 0);		/// CDLTHRUSTING - Thrusting Pattern
Techs_ClassDef(CDLTRISTAR, 			4, 1, 0);		/// CDLTRISTAR - Tristar Pattern
Techs_ClassDef(CDLUNIQUE3RIVER, 	4, 1, 0);		/// CDLUNIQUE3RIVER - Unique 3 River
Techs_ClassDef(CDLUPSIDEGAP2CROWS, 	4, 1, 0);		/// CDLUPSIDEGAP2CROWS - Upside Gap Two Crows
Techs_ClassDef(CDLXSIDEGAP3METHODS, 4, 1, 0);		/// CDLXSIDEGAP3METHODS - Upside/Downside Gap Three Methods

Techs_ClassDef(CMO, 				1, 1, 1);		/// CMO - Chande Momentum Oscillator
Techs_ClassDef(CORREL, 				2, 1, 1);		/// CORREL - Pearson's Correlation Coefficient (r)
Techs_ClassDef(COS, 				1, 1, 0);		/// COS - Vector Trigonometric Cos
Techs_ClassDef(COSH, 				1, 1, 0);		/// COSH - Vector Trigonometric Cosh

Techs_ClassDef(DX, 					3, 1, 1);		/// DX - Directional Movement Index

Techs_ClassDef(HT_DCPERIOD, 		1, 1, 0);		/// HT_DCPERIOD - Hilbert Transform - Dominant Cycle Period
Techs_ClassDef(HT_DCPHASE, 			1, 1, 0);		/// HT_DCPHASE - Hilbert Transform - Dominant Cycle Phase
Techs_ClassDef(HT_PHASOR, 			1, 2, 0);		/// HT_PHASOR - Hilbert Transform - Phasor Components
Techs_ClassDef(HT_SINE, 			1, 2, 0);		/// HT_SINE - Hilbert Transform - SineWave
Techs_ClassDef(HT_TRENDLINE, 		1, 1, 0);		/// HT_TRENDLINE - Hilbert Transform - Instantaneous Trendline
Techs_ClassDef(HT_TRENDMODE, 		1, 1, 0);		/// HT_TRENDMODE - Hilbert Transform - Trend vs Cycle Mode

Techs_ClassDef(LINEARREG, 			1, 1, 1);		/// LINEARREG - Linear Regression
Techs_ClassDef(LINEARREG_ANGLE, 	1, 1, 1);		/// LINEARREG_ANGLE - Linear Regression Angle
Techs_ClassDef(LINEARREG_INTERCEPT, 1, 1, 1);		/// LINEARREG_INTERCEPT - Linear Regression Intercept
Techs_ClassDef(LINEARREG_SLOPE, 	1, 1, 1);		/// LINEARREG_SLOPE - Linear Regression Slope

Techs_ClassDef(MACD, 				1, 3, 3MACD);	/// MACD - Moving Average Convergence/Divergence
Techs_ClassDef(MACDEXT, 			1, 3, MACDE);	/// MACDEXT - MACD with controllable MA type
Techs_ClassDef(MACDFIX, 			1, 3, 1MACD);	/// MACDFIX - Moving Average Convergence/Divergence Fix 12/26
Techs_ClassDef(MAVP, 				2, 1, MAVP);	/// MAVP - Moving average with variable period
Techs_ClassDef(MEDPRICE, 			2, 1, 0);		/// MEDPRICE - Median Price
Techs_ClassDef(MFI, 				4, 1, 1);		/// MFI - Money Flow Index
Techs_ClassDef(MIDPOINT, 			1, 1, 1);		/// MIDPOINT - MidPoint over period
Techs_ClassDef(MIDPRICE, 			2, 1, 1);		/// MIDPRICE - Midpoint Price over period
Techs_ClassDef(MINUS_DI, 			3, 1, 1);		/// MINUS_DI - Minus Directional Indicator
Techs_ClassDef(MINUS_DM, 			2, 1, 1);		/// MINUS_DM - Minus Directional Movement
Techs_ClassDef(MOM, 				1, 1, 1);		/// MOM - Momentum

Techs_ClassDef(NATR, 				3, 1, 1);		/// NATR - Normalized Average True Range
Techs_ClassDef(OBV, 				2, 1, 0);		/// OBV - On Balance Volume
Techs_ClassDef(PLUS_DI, 			3, 1, 1);		/// PLUS_DI - Plus Directional Indicator
Techs_ClassDef(PLUS_DM, 			2, 1, 1);		/// PLUS_DM - Plus Directional Movement
Techs_ClassDef(PPO, 				1, 1, 3APO);	/// PPO - Percentage Price Oscillator
Techs_ClassDef(RSI, 				1, 1, 1);		/// RSI - Relative Strength Index

Techs_ClassDef(SAR, 				2, 1, SAR);		/// SAR - Parabolic SAR
Techs_ClassDef(SAREXT, 				2, 1, SARE);	/// SAREXT - Parabolic SAR - Extended
Techs_ClassDef(SIN, 				1, 1, 0);		/// SIN - Vector Trigonometric Sin
Techs_ClassDef(SINH, 				1, 1, 0);		/// SINH - Vector Trigonometric Sinh
Techs_ClassDef(STOCH, 				3, 2, STOCH);	/// STOCH - Stochastic
Techs_ClassDef(STOCHF, 				3, 2, STOCHF);	/// STOCHF - Stochastic Fast

Techs_ClassDef(TAN, 				1, 1, 0);		/// TAN - Vector Trigonometric Tan
Techs_ClassDef(TANH, 				1, 1, 0);		/// TANH - Vector Trigonometric Tanh
Techs_ClassDef(TRANGE, 				3, 1, 0);		/// TRANGE - True Range
Techs_ClassDef(TRIX, 				1, 1, 1);		/// TRIX - 1-day Rate-Of-Change (ROC) of a Triple Smooth EMA
Techs_ClassDef(TSF, 				1, 1, 1);		/// TSF - Time Series Forecast
Techs_ClassDef(TYPPRICE, 			3, 1, 0);		/// TYPPRICE - Typical Price

Techs_ClassDef(ULTOSC, 				3, 1, UOSC);	/// ULTOSC - Ultimate Oscillator
Techs_ClassDef(WCLPRICE, 			3, 1, 0);		/// WCLPRICE - Weighted Close Price
Techs_ClassDef(WILLR, 				3, 1, 1);		/// WILLR - Williams' %R


