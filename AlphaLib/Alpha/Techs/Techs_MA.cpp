/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Techs_MA.cpp
///
/// Created on: 25 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "TechsUtil.h"

#define MAClass(Name, HistoryCountMultiplier) \
namespace pesa { \
	class Techs_##Name : public Techs_MovingAverage { \
	public: \
		Techs_##Name(const ConfigSection& config) : Techs_MovingAverage(config, #Name, TA_S_MA) {} \
		virtual ~Techs_##Name() {} \
		virtual TA_MAType maType() const { return TA_MAType_##Name; } \
		virtual IntDay historyCount() const { return (IntDay)(m_pinfo.days * HistoryCountMultiplier); } \
	}; \
} \
EXPORT_FUNCTION pesa::IComponent* createTechs_##Name(const pesa::ConfigSection& config) { \
	return new pesa::Techs_##Name((const pesa::ConfigSection&)config); \
}


enum {
	SMA,		/// Simple MA
	EMA,		/// Exponential MA
	KAMA,		/// Kaufmann's Adaptive MA
	DEMA,		/// Double Exponential MA
	TEMA,		/// Triple Exponential MA
	WMA,		/// Weighted MA
	TRIMA,		/// Triangular MA
	MAMA,		/// 
	T3,			///
};

MAClass(SMA, 1);
MAClass(EMA, 1);
MAClass(KAMA, 1.2f);
MAClass(WMA, 1);
MAClass(DEMA, 2);
MAClass(TEMA, 3);
MAClass(TRIMA, 3);
MAClass(MAMA, 1.5f);
MAClass(T3, 3);
