/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Techs_StLtEMA.cpp
///
/// Created on: 31 Oct 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"
#include "TechsUtil.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Techs_StLtEMA
	////////////////////////////////////////////////////////////////////////////
	class Techs_StLtEMA : public Techs_MovingAverage {
	public:
								Techs_StLtEMA(const ConfigSection& config) : Techs_MovingAverage(config, "Techs_StLtEMA", TA_S_MA) {}
		virtual 				~Techs_StLtEMA() {}

		virtual TA_MAType		maType() const { return TA_MAType_EMA; } 
		virtual void 			run(const RuntimeData& rt);
	};

	void Techs_StLtEMA::run(const RuntimeData& rt) {
		IntDay di 		= rt.di - delay();
		auto& data 		= F_ARG0();

		II_LOOP() {
			float stEma = maCalc(rt, ii, m_pinfo.shortTermDays, m_pinfo.shortTermDays);
			float ltEma = maCalc(rt, ii, m_pinfo.longTermDays, m_pinfo.longTermDays);
			float ratio = stEma / ltEma;

			alpha(0, ii) = ratio * reversion();
		}
	}

	////////////////////////////////////////////////////////////////////////////
	/// Techs_StLtMA
	////////////////////////////////////////////////////////////////////////////
	class Techs_StLtMA : public Techs_MovingAverage {
	public:
								Techs_StLtMA(const ConfigSection& config) : Techs_MovingAverage(config, "Techs_StLtMA", TA_S_MA) {}
		virtual 				~Techs_StLtMA() {}

		virtual TA_MAType		maType() const { return TA_MAType_SMA; }
		virtual void 			run(const RuntimeData& rt);
	};

	void Techs_StLtMA::run(const RuntimeData& rt) {
		IntDay di = rt.di - delay();
		auto& data = F_ARG0();

		II_LOOP() {
			float stEma = maCalc(rt, ii, m_pinfo.shortTermDays, m_pinfo.shortTermDays);
			float ltEma = maCalc(rt, ii, m_pinfo.longTermDays, m_pinfo.longTermDays);
			float ratio = stEma / ltEma;

			alpha(0, ii) = ratio * reversion();
		}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createTechs_StLtEMA(const pesa::ConfigSection& config) {
	return new pesa::Techs_StLtEMA((const pesa::ConfigSection&)config);
}

EXPORT_FUNCTION pesa::IComponent* createTechs_StLtMA(const pesa::ConfigSection& config) {
	return new pesa::Techs_StLtMA((const pesa::ConfigSection&)config);
}

