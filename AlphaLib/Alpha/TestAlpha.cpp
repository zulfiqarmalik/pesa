/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// TestAlpha.cpp
///
/// Created on: 22 Jun 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
// #include "ta_func.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class TestAlpha : public IAlpha {
	public:
								TestAlpha(const ConfigSection& config);
		virtual 				~TestAlpha();

		virtual void 			run(const RuntimeData& rt);
	};

	TestAlpha::TestAlpha(const ConfigSection& config) : IAlpha(config, "TEST_ALPHA") {
	}

	TestAlpha::~TestAlpha() {
	}

	void TestAlpha::run(const RuntimeData& rt) {
		IntDay di = rt.di - delay();
		auto& alpha = this->alpha();
		const auto& closePrice = *getData<FloatMatrixData>("baseData.adjClosePrice");
		size_t numAlphas = alpha.cols();

		for (size_t ii = 0; ii < numAlphas; ii++) {
			auto sec = universe()->security(di, (Security::Index)ii);
			float cp1 = closePrice(di, ii);
			//float cp0 = closePrice(di - 1, ii);

			//float& lhs = alpha(0, ii);
			//float rhs = (cp1 / cp0) - 1.0f;
			float alphaValue = 1.0f / cp1;

			alpha(0, ii) = alphaValue;
		}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createTestAlpha(const pesa::ConfigSection& config) {
	return new pesa::TestAlpha((const pesa::ConfigSection&)config);
}

