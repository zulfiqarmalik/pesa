/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// DummyCombination.cpp
///
/// Created on: 02 Feb 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class DummyCombination : public ICombination {
	public:
								DummyCombination(const ConfigSection& config);
		virtual 				~DummyCombination();

		virtual void 			run(const RuntimeData& rt);
	};

	DummyCombination::DummyCombination(const ConfigSection& config) : ICombination(config, "SIMPLE_COMBINATION") {
	}

	DummyCombination::~DummyCombination() {
	}

	void DummyCombination::run(const RuntimeData& rt) {
		auto& ar = this->alphas();

		if (!ar.childAlphas.size())
			return;

		ASSERT(ar.alpha, "Must have created a ComboResult or other equivalent type alpha to store the result!");

		const auto& calpha = ar.childAlphas[0].alpha->remappedAlpha();
		size_t rows = calpha.rows();
		size_t cols = calpha.cols();

		/// allocate the alpha array and resize
		ar.alpha->data().ensureAlphaSize(rows, cols);
		auto& result = ar.alpha->alpha();
		result.zeroMemory();
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createDummyCombination(const pesa::ConfigSection& config) {
	return new pesa::DummyCombination((const pesa::ConfigSection&)config);
}

