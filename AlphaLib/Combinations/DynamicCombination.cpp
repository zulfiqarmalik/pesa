/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// DynamicCombination.cpp
///
/// Created on: 10 Apr 7
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "./SimpleCombination.h"
#include "Poco/String.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// DynamicCombionation
	////////////////////////////////////////////////////////////////////////////
	class DynamicCombination : public SimpleCombination {
	protected:
		typedef std::map<std::string, float> WeightMap;
		void					adjustWeights(const RuntimeData& rt, AlphaResult& ar);

		WeightMap				m_weights;			/// The existing weights that we have for each alpha 
		bool					m_trackDD = true;	/// Track drawdown or not
		bool					m_trackIR = true;	/// Track IR or not
		bool					m_trackVol = true;	/// Track volatility

		IntDay					m_minDays = 250;	/// How many days to wait before triggering the combination's logic

		float					m_D55Weight = 1.75f;
		float					m_D21Weight = 1.25f;
		float					m_D5Weight = 1.0f;

		float					getWeight(IAlphaPtr alpha, float defaultWeight);
		float					calculateWeight(const RuntimeData& rt, const LifetimeStatistics* ltStats, float existingWeight) const;
		float					calculateWeightForStats(const RuntimeData& rt, const CumStatistics& istats, const LifetimeStatistics* ltStats, float existingWeight) const;

	public:
								DynamicCombination(const ConfigSection& config);
		virtual 				~DynamicCombination();

		virtual void 			run(const RuntimeData& rt);
	};

	DynamicCombination::DynamicCombination(const ConfigSection& config) : SimpleCombination(config) {
		setLogChannel("DYNAMIC_COMBINATION");

		const ConfigSection& portConfig = parentConfig()->portfolio();
		ASSERT(portConfig.getBool("runningWindowCalculations", false), "runningWindowCalculations must be set to \"true\" for DynamicCombination!");

		m_minDays = (IntDay)config.get<int>("minDays", m_minDays);
	}

	DynamicCombination::~DynamicCombination() {
	}

	float DynamicCombination::getWeight(IAlphaPtr alpha, float defaultWeight) {
		std::string alphaUuid = alpha->uuid();
		auto iter = m_weights.find(alphaUuid);

		if (iter == m_weights.end()) {
			m_weights[alphaUuid] = defaultWeight;
			return defaultWeight;
		}

		return iter->second;
	}

	float DynamicCombination::calculateWeightForStats(const RuntimeData& rt, const CumStatistics& istats, const LifetimeStatistics* ltStats, float existingWeight) const {
		float dhFactor = ltStats->maxDH ? 1.0f - (float)istats.dh / (float)ltStats->maxDH : 1.0f;
		float ddFactor = ltStats->maxDD ? std::max(0.15f - std::abs(istats.dd / ltStats->maxDD), 0.0f) * dhFactor : 1.0f;

		float irFactor = ltStats->ir ? istats.ir / ltStats->ir : 1.0f;
		float volFactor = istats.volatility;

		float weight = 1.0f;

		if (!m_trackDD)
			ddFactor = 1.0f;

		if (!m_trackIR)
			irFactor = 1.0f;

		if (!m_trackVol)
			volFactor = 1.0f;

		if (volFactor == 0.0f)
			return 0.0f;

		return ddFactor; ///(ddFactor * irFactor) / volFactor;
	}

	float DynamicCombination::calculateWeight(const RuntimeData& rt, const LifetimeStatistics* ltStats, float existingWeight) const {
		IntDay runningDays = rt.di - rt.diStart;

		if (runningDays < m_minDays)
			return existingWeight; 

		float weight = existingWeight;
		float d55Weight = 1.0f;
		float d21Weight = 1.0f;
		float d5Weight	= existingWeight;
        float d10Weight = existingWeight;

        //		if (runningDays >= 5)
		d5Weight = calculateWeightForStats(rt, ltStats->D5, ltStats, existingWeight);
        d10Weight = calculateWeightForStats(rt, ltStats->D10, ltStats, existingWeight);

		if (runningDays >= 21)
			d21Weight = calculateWeightForStats(rt, ltStats->D21, ltStats, existingWeight);
		if (runningDays >= 63) 
			d55Weight = calculateWeightForStats(rt, ltStats->D63, ltStats, existingWeight);

        float ltWeight = calculateWeightForStats(rt, *ltStats, ltStats, existingWeight);

        //        return ltStats->D5.pnl * -1.0f;
        return d5Weight;
            ////d5Weight; ///m_D5Weight * d5Weight; ///m_ight * d55Weight * m_D21Weight * d21Weight * m_D5Weight * d5Weight;
	}

	void DynamicCombination::adjustWeights(const RuntimeData& rt, AlphaResult& ar) {
		if (!ar.childAlphas.size())
			return;

		ASSERT(ar.alpha, "Must have created a ComboResult or other equivalent type alpha to store the result!");

		const auto& calpha = ar.childAlphas[0].alpha->remappedAlpha();
		size_t rows = calpha.rows();
		size_t cols = calpha.cols();

		/// allocate the alpha array and resize
		ar.alpha->data().ensureAlphaSize(rows, cols);
		auto& result = ar.alpha->alpha();
		result.zeroMemory();

		//std::string debugStr = Poco::format("%u - [di = %d]: ", rt.dates[rt.di], rt.di);

		for (auto& car : ar.childAlphas) {
			IAlphaPtr alpha		= car.alpha;
			const auto& calpha	= alpha->remappedAlpha();
			const auto& uuid	= alpha->uuid();

			ASSERT(calpha.cols() == result.cols(), "Array size mismatch. Results to be combined should have the same length. Expecting: " << result.cols() << ", found: " << calpha.cols());

			float weight		= alpha->weight();
			auto* ydata			= alpha->yesterdayData();

			/// If we don't have any data from yesterday then we don't do anything ...
			if (!ydata) 
				continue;

			float existingWeight= getWeight(alpha, weight);
			const LifetimeStatistics* ltStats = alpha->cumStats();

			/// LifetimeStatisticse have not been started yet ... 
			if (!ltStats)
				continue;

			float newWeight		= calculateWeight(rt, ltStats, existingWeight);

			if (std::abs(newWeight - weight) > 0.001f) {
                //				CDebug("%s: %0.02hf => 0.02hf", alpha->uuid(), weight, newWeight);
			}

			//std::string weightStr;
			//Poco::format(weightStr, "%s = %0.2hf, ", alpha->uuid(), newWeight);
			//debugStr			+= weightStr;
			alpha->weight()		= newWeight;

			m_weights[alpha->uuid()] = newWeight;
		} 
		
		//CTrace("Weights - %s", debugStr);
	}

	void DynamicCombination::run(const RuntimeData& rt) {
		adjustWeights(rt, this->alphas());

		SimpleCombination::run(rt);
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createDynamicCombination(const pesa::ConfigSection& config) {
	return new pesa::DynamicCombination((const pesa::ConfigSection&)config);
}

