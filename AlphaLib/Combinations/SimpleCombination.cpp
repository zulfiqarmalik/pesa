/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// SimpleCombination.cpp
///
/// Created on: 14 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "SimpleCombination.h"

namespace pesa {

	SimpleCombination::SimpleCombination(const ConfigSection& config) : ICombination(config, "SIMPLE_COMBINATION") {
		const auto& func = config.getString("func", "add");

		if (func == "add" || func == "+")
			m_combineFunc = SimpleCombination::add;
		else if (func == "sub" || func == "-")
			m_combineFunc = SimpleCombination::sub;
		else if (func == "mul" || func == "*")
			m_combineFunc = SimpleCombination::mul;
		else if (func == "div" || func == "/")
			m_combineFunc = SimpleCombination::div;
		else
			ASSERT(false, "Unsupported SimpleCombination func: " << func);
	}

	SimpleCombination::~SimpleCombination() {
	}

	void SimpleCombination::combine(AlphaResult& ar) {
		if (!ar.childAlphas.size())
			return;

		//for (auto& car : ar.childAlphas)
		//	combine(car);

		ASSERT(ar.alpha, "Must have created a ComboResult or other equivalent type alpha to store the result!");

		const auto& calpha = ar.childAlphas[0].alpha->remappedAlpha();
		size_t rows = calpha.rows();
		size_t cols = calpha.cols();

		/// allocate the alpha array and resize
		ar.alpha->data().ensureAlphaSize(rows, cols);
		auto& result = ar.alpha->alpha();
		result.zeroMemory();

		for (auto& car : ar.childAlphas) {
			const auto& calpha = car.alpha->remappedAlpha();
			const auto& uuid = car.alpha->uuid();

			ASSERT(calpha.cols() == result.cols(), "Array size mismatch. Results to be combined should have the same length. Expecting: " << result.cols() << ", found: " << calpha.cols());

			for (IntDay di = 0; di < (IntDay)result.rows(); di++) {
				for (size_t ii = 0; ii < result.cols(); ii++) {
					float value = calpha(di, ii);

					//CDebug("%s - %d - %z : %0.2hf", uuid, di, ii, value);

					if (std::isnan(value))
						continue;

					float wvalue = value * car.alpha->weight();
					float cvalue = result(di, ii);

					result(di, ii) = m_combineFunc(cvalue, wvalue);
				}
			}
		}
	}

	void SimpleCombination::run(const RuntimeData& rt) {
		combine(this->alphas());
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createSimpleCombination(const pesa::ConfigSection& config) {
	return new pesa::SimpleCombination((const pesa::ConfigSection&)config);
}

