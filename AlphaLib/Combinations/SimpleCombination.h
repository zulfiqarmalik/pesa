/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// SimpleCombination.h
///
/// Created on: 10 Apr 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// SimpleCombination
	////////////////////////////////////////////////////////////////////////////
	class SimpleCombination : public ICombination {
	protected:
		typedef float			CombineFunc(float lhs, float rhs);

		static float			add(float lhs, float rhs) { return lhs + rhs; }
		static float			sub(float lhs, float rhs) { return lhs - rhs; }
		static float			mul(float lhs, float rhs) { return lhs * rhs; }
		static float			div(float lhs, float rhs) { return lhs / rhs; }

		CombineFunc*			m_combineFunc = SimpleCombination::add;

		void					combine(AlphaResult& result);

	public:
								SimpleCombination(const ConfigSection& config);
		virtual 				~SimpleCombination();

		virtual void 			run(const RuntimeData& rt);
	};
} /// namespace pesa

