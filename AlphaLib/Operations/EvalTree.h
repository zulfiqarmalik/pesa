/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// EvalTree.h
///
/// Created on: 16 Jun 2017
/// 	Author: akustarev
//////////////////////////////////////////////////////

#include <memory>

namespace pesa 
{

namespace EvalTree 
{

class Tree
{
public:
	std::vector<std::shared_ptr<Tree>> children;
	virtual int childNum() = 0;
	virtual float calc(int di, int ii) = 0;
	virtual ~Tree() {}
};

class TreeData : public Tree
{
private:
	const FloatMatrixData &data;
public:
	virtual int childNum()
	{
		return 0;
	}
	virtual float calc(int di, int ii)
	{
		return data[di][ii];
	}
	TreeData(
		const RuntimeData& rt, IUniverse* univ, 
		const std::string &name) : data(*(rt.dataRegistry->floatData(univ, name))) {}
};

class TreeIntData : public Tree
{
private:
        const IntMatrixData &data;
public:
        virtual int childNum()
        {
			return 0;
        }
        virtual float calc(int di, int ii)
        {
			return (float)(data[di][ii]);
        }
        TreeIntData(
                const RuntimeData& rt, IUniverse* univ,
                const std::string &name) : data(*(rt.dataRegistry->intData(univ, name))) {}
};

class TreeConst : public Tree
{
private:
	float res;
public:
	virtual int childNum()
	{
		return 0;
	}
	virtual float calc(int di, int ii)
	{
		return res;
	}
	TreeConst(float r) : res(r) {} 
};

class TreeToday : public Tree
{
public:
        virtual int childNum()
        {
            return 0;
        }
        virtual float calc(int di, int ii)
        {
			return (float)di;
        }
};

class TreeScale : public Tree
{
private:
        float a;
	float b;
public:
        virtual int childNum()
        {
                return 1;
        }
        virtual float calc(int di, int ii)
        {
                return a * children[0]->calc(di, ii) + b;
        }
        TreeScale(float a_, float b_) : a(a_), b(b_) {}
};

class TreeDelay : public Tree
{
private:
        int days;
public:
        virtual int childNum()
        {
                return 1;
        }
        virtual float calc(int di, int ii)
        {
		return children[0]->calc(di - days, ii);
        }
        TreeDelay(int d) : days(d) {}
};

class TreeTS : public Tree
{
private: 
	int days;
protected:
	std::vector<std::deque<float>> v;
public:
	virtual int childNum()
	{
		return 1;
	}
	virtual float calc(int di, int ii)
	{
		if (ii >= v.size())
			v.resize(ii + 1);
		if (v[ii].empty())
			for (int i = 0; i < days; i++)
				v[ii].push_back(children[0]->calc(di - i, ii));
		else
		{
			v[ii].pop_back();
			v[ii].push_front(children[0]->calc(di, ii));
		}
		return tsCalc(di, ii);
	}
	virtual float tsCalc(int di, int ii) = 0;
	TreeTS(int d) : days(d) {}
};

class TreeUnary : public TreeTS
{
private:
	EvalUtil::CalcMode mode;
public:
	TreeUnary(EvalUtil::CalcMode m, int d) : mode(m), TreeTS(d) {}
	virtual float tsCalc(int di, int ii)
	{
		return EvalUtil::calc(v[ii], mode);
	}
};

class TreeRank : public TreeTS
{
private:
	std::vector<float> tmpVals;
public:
	virtual float tsCalc(int di, int ii)
	{
		return EvalUtil::tsRank(v[ii], tmpVals);
	}
	TreeRank(int d) : TreeTS(d) {}
};

class TreeBinary : public Tree
{
public:
	enum class Mode { plus, minus, mult, div };
private: 
	Mode mode;
public:
	virtual int childNum()
	{
		return 2;
	}
	virtual float calc(int di, int ii)
	{
		float x = children[0]->calc(di, ii);
		float y = children[1]->calc(di, ii);

		switch (mode)
		{
			case Mode::plus: return x + y;
			case Mode::minus: return x - y;
			case Mode::mult: return x * y;
			case Mode::div: return x / y;
			default : {}
		}

		return 0.0f;
	}
	TreeBinary(Mode m) : mode(m) {}
};

class TreeBinaryTS : public Tree
{
private:
        int days;
        std::vector<std::deque<float>> v0;
        std::vector<std::deque<float>> v1;
	EvalUtil::CalcMode mode;
public:
        virtual int childNum()
        {
                return 2;
        }
        virtual float calc(int di, int ii)
        {
                if (ii >= v0.size())
                        v0.resize(ii + 1);
                if (ii >= v1.size())
                        v1.resize(ii + 1);
                if (v0[ii].empty())
                        for (int i = 0; i < days; i++)
                                v0[ii].push_back(children[0]->calc(di - i, ii));
                else
                {
                        v0[ii].pop_back();
                        v0[ii].push_front(children[0]->calc(di, ii));
                }
                if (v1[ii].empty())
                        for (int i = 0; i < days; i++)
                                v1[ii].push_back(children[1]->calc(di - i, ii));
                else
                {
                        v1[ii].pop_back();
                        v1[ii].push_front(children[1]->calc(di, ii));
                }
		return EvalUtil::calc(v0[ii], mode, &(v1[ii]));
        }
        TreeBinaryTS(EvalUtil::CalcMode m, int d) : mode(m), days(d) {}
};


std::shared_ptr<Tree> factory(const std::string& token, const RuntimeData& rt, IUniverse* univ)
{
	std::vector<std::string> tokens;
	EvalUtil::split(token, ' ', &tokens);
	if (tokens[0] == "Data")
		return std::make_shared<TreeData>(rt, univ, tokens[1]);
        if (tokens[0] == "IntData")
                return std::make_shared<TreeIntData>(rt, univ, tokens[1]);
	if (tokens[0] == "Const")
		return std::make_shared<TreeConst>(std::stof(tokens[1]));
        if (tokens[0] == "Scale")
                return std::make_shared<TreeScale>(std::stof(tokens[1]), std::stof(tokens[2]));
        if (tokens[0] == "Opp")
                return std::make_shared<TreeScale>(-1.0f, 0.0f);
        if (tokens[0] == "Delay")
                return std::make_shared<TreeDelay>(std::stoi(tokens[1]));
	if (tokens[0] == "Min")
		return std::make_shared<TreeUnary>(EvalUtil::CalcMode::min, std::stoi(tokens[1]));
	if (tokens[0] == "Max")
		return std::make_shared<TreeUnary>(EvalUtil::CalcMode::max, std::stoi(tokens[1]));
	if (tokens[0] == "Sum")
		return std::make_shared<TreeUnary>(EvalUtil::CalcMode::sum, std::stoi(tokens[1]));
	if (tokens[0] == "Mean")
		return std::make_shared<TreeUnary>(EvalUtil::CalcMode::mean, std::stoi(tokens[1]));
	if (tokens[0] == "Decay")
		return std::make_shared<TreeUnary>(EvalUtil::CalcMode::decay, std::stoi(tokens[1]));
	if (tokens[0] == "Std")
		return std::make_shared<TreeUnary>(EvalUtil::CalcMode::std, std::stoi(tokens[1]));
	if (tokens[0] == "Var")
		return std::make_shared<TreeUnary>(EvalUtil::CalcMode::var, std::stoi(tokens[1]));
	if (tokens[0] == "Ir")
		return std::make_shared<TreeUnary>(EvalUtil::CalcMode::ir, std::stoi(tokens[1]));
	if (tokens[0] == "Zscore")
		return std::make_shared<TreeUnary>(EvalUtil::CalcMode::zscore, std::stoi(tokens[1]));
	if (tokens[0] == "Rank")
		return std::make_shared<TreeRank>(std::stoi(tokens[1]));
	if (tokens[0] == "Plus")
		return std::make_shared<TreeBinary>(TreeBinary::Mode::plus);
	if (tokens[0] == "Minus")
		return std::make_shared<TreeBinary>(TreeBinary::Mode::minus);
	if (tokens[0] == "Mult")
		return std::make_shared<TreeBinary>(TreeBinary::Mode::mult);
	if (tokens[0] == "Div")
		return std::make_shared<TreeBinary>(TreeBinary::Mode::div);
        if (tokens[0] == "Today")
                return std::make_shared<TreeToday>();
        if (tokens[0] == "Corr")
                return std::make_shared<TreeBinaryTS>(EvalUtil::CalcMode::corr, std::stoi(tokens[1]));

	std::cerr << "Failed" << std::endl;
	return std::make_shared<TreeData>(rt, univ, "");
}

std::shared_ptr<Tree> glue(std::deque<std::string>& tokens, const RuntimeData& rt, IUniverse* univ)
{
	auto tree = factory(tokens[0], rt, univ);
	tokens.pop_front();
	for (int i = 0; i < tree->childNum(); i++)
		tree->children.push_back(glue(tokens, rt, univ));
	return tree;
}

std::shared_ptr<Tree> formTree(const std::string& evalline, const RuntimeData& rt, IUniverse* univ)
{
	std::deque<std::string> tokens;
	EvalUtil::split(evalline, ',', &tokens);
	return glue(tokens, rt, univ);
}

}

}
