/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// EvalUtil.h
///
/// Created on: 16 Jun 2017
/// 	Author: akustarev
//////////////////////////////////////////////////////

namespace pesa 
{

namespace EvalUtil
{

template<class T>
void split(const std::string &st, char c, T *res)
{
	res->clear();
	std::string t;
	for (int i = 0; i < st.size(); i++)
	{
		if (st[i] == c)
		{
			if (t.size())
			{
				res->push_back(t);
				t.clear();
			}
		}
		else
		{
			t.push_back(st[i]);
		}
	}
	if (t.size())
		res->push_back(t);
}

template<class T>
int sgn(T x)
{
	return x >= 0 ? 1 : -1;
}

template<typename T>
float dot(const T& x, const T& y)
{
	float res = 0;
	for (int i = 0; i < x.size(); i++)
		res += x[i] * y[i];
	return res;
}

enum class CalcMode { sum, mean, std, var, ir, decay, max, min, zscore, cov, corr, regr, dot, signcorr, signcov };

template<typename T>
float calc(const T& p, CalcMode mode, const T *other = nullptr)
{
	int n = (int)p.size();
	if (!n) 
		return 0;
	float s = 0, ss = 0, sy = 0, sxy = 0, syy = 0;
	float d = 0;
	float x = 0, y = 0;
	float m = p[0];
	int sign = (mode == CalcMode::min) ? -1 : 1;
	for (int i = 0; i < n; i++)
	{
		x = p[i];
		if (sign * x > sign * m)
			m = x;
		s += x;
		ss += x * x;
		d += (n - i) * x;
		if (other)
		{
			y = (*other)[i];
			sy += y; 
			sxy += x * y;
			syy += y * y;
		}
	}
	if ((mode == CalcMode::min) || (mode == CalcMode::max))
		return m;
	if (mode == CalcMode::decay)
		return d;
	s /= n;
	ss /= n;
	ss -= s * s;
	ss = std::max(ss, float(0.0f));
	ss = sqrt(ss);
	switch(mode)
	{
		case CalcMode::ir : return s / ss;
		case CalcMode::sum : return s * n;
		case CalcMode::mean : return s;
		case CalcMode::std : return ss;
		case CalcMode::var : return ss * ss;
		case CalcMode::zscore : return (p[0] - s) / ss;
		default : {}
	}
	if (!other)
		return 0;
	{
		sy /= n; 
		syy /= n;
		syy -= sy * sy;
		syy = std::max(syy, float(0.0f));
		syy = sqrt(syy);
		sxy /= n;
		switch(mode) 
		{
			case CalcMode::dot : return sxy * n;
			case CalcMode::cov : return sxy - s * sy;
			case CalcMode::corr : return (sxy - s * sy) / (ss * syy);
			case CalcMode::signcorr : return sgn(s) * sgn(sy) * (sxy - s * sy) / (ss * syy);
			case CalcMode::signcov : return sgn(s) * sgn(sy) * (sxy - s * sy);
			case CalcMode::regr : return (sxy - s * sy) * (ss * ss);
			default : return 0;
		}
	}
	return 0;
}		

template<class T>
float tsRank(const T& a, std::vector<float>& v, float shift = 0.5)
{
	float firstVal = a[0];
	v.clear();
	for (int ii = 0; ii < a.size(); ii++)
		if (std::isfinite(a[ii]))
			v.push_back(a[ii]);
	if (v.empty())
		return 0;
	std::sort(v.begin(), v.end());
	float denom = 1.0f / ((float)v.size() - 1.0f);
	for (int i = 0; i < v.size(); i++)
	{
		if (v[i] == firstVal)
		{
			int j = i + 1;
			while ((v[j] == firstVal) && (j < v.size()))
				j++;
			float value = 0.5f * (float)(i + j - 1);
			return denom * value - shift;
		}
	}
	return 0;
}

}

}
