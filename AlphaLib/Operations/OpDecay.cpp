/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// OpDecay.cpp
///
/// Created on: 13 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"
#include <deque>

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class OpDecay : public IOperation {
	private:
		typedef std::deque<FloatMatrixDataPtr> AlphaDeque;
		static const size_t		s_maxDays = 300;
		size_t 					m_days = 5;
		float					m_denom;			/// Linear decay denom
		AlphaDeque 				m_history; 			/// History for the last n days
		float					m_exp = 0.0f;

	public:
								OpDecay(const ConfigSection& config);
		virtual 				~OpDecay();

		virtual void 			run(const RuntimeData& rt);
	};

	OpDecay::OpDecay(const ConfigSection& config) 
		: IOperation(config, "OP_DECAY") {
		m_days = config.get<size_t>("days", m_days);
		m_exp = config.get<float>("exp", m_exp);

		ASSERT(m_days > 0 && m_days < s_maxDays, "The range for days is [0, " << s_maxDays << ") - Specified: " << m_days);
		m_denom = (float)(m_days * (1 + m_days)) * 0.5f; ///  Sum of series Sn = n(a1 + an) / 2
	}

	OpDecay::~OpDecay() {
	}

	void OpDecay::run(const RuntimeData& rt) {
		/// Make a copy of the input alpha since we'll be modifying values as we go along
		auto& output = alpha();
		m_history.push_back(this->data().alpha);

		/// remove the oldest entry ...
		if (m_history.size() > m_days)
			m_history.pop_front();

		size_t days = m_history.size();
		ASSERT(m_days > 0 && m_days < s_maxDays, "The range for days is [0, " << s_maxDays << ") - Specified: " << m_days);
		//float denom = (float)(days * (1 + days)) * 0.5f; ///  Sum of series Sn = n(a1 + an) / 2

		/// We use the linear function if the exponent is very low
		if (std::abs(m_exp) < 0.00001f) {
			for (size_t ii = 0; ii < output.cols(); ii++) {
				//float numer = (*m_history[days - 1])(0, ii) * days;
				float dalpha = 0.0f;

				for (size_t jj = 0; jj < days; jj++) {
					//numer += (*m_history[days - jj - 1])(0, ii) * (days - jj);
					float iiValue = (*m_history[days - jj - 1])(0, ii) * (days - jj);
					if (this->data().universe->isValid(this->data().di - (IntDay)jj, ii) && !std::isnan(iiValue))
						dalpha += iiValue;
				}

				output(0, ii) = dalpha;
			}
		}
		else { /// otherwise we use the exponent function
			for (size_t ii = 0; ii < output.cols(); ii++) {
				//float numer = (*m_history[days - 1])(0, ii) / std::exp(m_exp * 1.0f);
				float dalpha = 0.0f;

				for (size_t jj = 0; jj < days; jj++) {
					//numer += (*m_history[days - jj - 1])(0, ii) / std::exp(m_exp * (jj + 1));
					float iiValue = (*m_history[days - jj - 1])(0, ii) / std::exp(m_exp * (jj + 1));
					if (this->data().universe->isValid(this->data().di - (IntDay)jj, ii) && !std::isnan(iiValue))
						dalpha += iiValue;
				}

				output(0, ii) = dalpha;
			}
		}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createOpDecay(const pesa::ConfigSection& config) {
	return new pesa::OpDecay((const pesa::ConfigSection&)config);
}

