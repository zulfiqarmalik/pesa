/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// OpEval.cpp
///
/// Created on: 16 Jun 2017
/// 	Author: akustarev
//////////////////////////////////////////////////////

#include "Framework/Framework.h"

#include "EvalUtil.h"
#include "EvalTree.h"

namespace pesa {

	class OpEval : public IOperation {

	private:
		std::string m_evalline;		
		std::shared_ptr<EvalTree::Tree> tree;
		
	public:
		OpEval(const ConfigSection& config);
		virtual	~OpEval();
		virtual void run(const RuntimeData& rt);
	};

	OpEval::OpEval(const ConfigSection& config) : IOperation(config, "OP_EVAL")
	{
		m_evalline = config.getString("evalline", "");
	}

	OpEval::~OpEval() {}

	void OpEval::run(const RuntimeData& rt) 
	{
		FloatMatrixData& alpha = this->alpha();
		auto di = rt.di - 1;

		if (!tree)
			tree = EvalTree::formTree(m_evalline, rt, this->data().universe);

		for (size_t ii = 0; ii < alpha.cols(); ii++) {
			float& value = alpha(0, ii);
			value = tree->calc(di, (int)ii);
		}
	}

} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createOpEval(const pesa::ConfigSection& config) {
	return new pesa::OpEval((const pesa::ConfigSection&)config);
}

