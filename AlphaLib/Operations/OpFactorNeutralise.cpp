/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// OpFactorNeutralise.cpp
///
/// Created on: 16 Sep 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"

namespace pesa {
	class OpFactorNeutralise : public IOperation {
	private:
		std::string				m_dataId;				/// The data that we're going to neutralise against
		float					m_factorScale = 100.0f;	/// The scale of the factor
		bool					m_useResidual = false;	/// Use residual neutralisation!

	public:
								OpFactorNeutralise(const ConfigSection& config);
		virtual 				~OpFactorNeutralise();

		virtual void 			run(const RuntimeData& rt);
	};

	OpFactorNeutralise::OpFactorNeutralise(const ConfigSection& config) : IOperation(config, "OP_FACTOR_NEUTRALISE") {
		m_dataId = config.getRequired("dataId");
		auto parts = Util::split(m_dataId, ":");
		ASSERT(parts.size() <= 2, "Invalid dataId: " << m_dataId);

		if (parts.size() == 2) {
			m_dataId = parts[0];
			m_factorScale = Util::percent(parts[1]);
		}

		m_useResidual = config.getBool("useResidual", m_useResidual);
	}

	OpFactorNeutralise::~OpFactorNeutralise() {
	}

	void OpFactorNeutralise::run(const RuntimeData& rt) {
		IntDay di = rt.di - this->data().delay;
		const auto* data = rt.dataRegistry->get<FloatMatrixData>(this->data().universe, m_dataId);
		ASSERT(data, "Unable to get data: " << m_dataId);

		FloatMatrixData& alpha = this->alpha();
		Stats::neutralise(alpha.mat().mat(), data->mat().mat(), di, m_factorScale, m_useResidual);
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createOpFactorNeutralise(const pesa::ConfigSection& config) {
	return new pesa::OpFactorNeutralise((const pesa::ConfigSection&)config);
}

