/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// OpLongOnly.cpp
///
/// Created on: 22 Jun 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class OpLongOnly : public IOperation {
	public:
								OpLongOnly(const ConfigSection& config);
		virtual 				~OpLongOnly();

		virtual void 			run(const RuntimeData& rt);
	};

	OpLongOnly::OpLongOnly(const ConfigSection& config) : IOperation(config, "OP_NORMALISE") {
	}

	OpLongOnly::~OpLongOnly() {
	}

	void OpLongOnly::run(const RuntimeData& rt) {
		FloatMatrixData& alpha = this->alpha();
		float* fdata = alpha.fdata();
		size_t count = alpha.cols();
		float min = Stats::min(fdata, count);

		/// We don't do anything if we get a NaN mean
		if (std::isnan(min))
			return;

		for (size_t i = 0; i < count; i++) {
			float& f = *(fdata + i);
			f -= min;
			ASSERT(f >= 0.0f || std::isnan(f), "Invalid Long only operation result. At index: " << i << ", value is: " << f << "[Min: " << min << "]. This should not happen in long only operation");
		}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createOpLongOnly(const pesa::ConfigSection& config) {
	return new pesa::OpLongOnly((const pesa::ConfigSection&)config);
}

