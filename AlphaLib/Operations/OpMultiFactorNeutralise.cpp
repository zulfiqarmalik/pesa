/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// OpMultiFactorNeutralise.cpp
///
/// Created on: 08 May 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"

namespace pesa {
	class OpMultiFactorNeutralise : public IOperation {
	private:
		StringVec				m_factorIds; 
		std::map<std::string, std::shared_ptr<Eigen::VectorXf> > m_groupFactors;

		Eigen::VectorXf 		getFactor(const RuntimeData& rt, size_t fid, IntDay di);
		Eigen::VectorXf 		getGroupFactor(const RuntimeData& rt, size_t fid, IntDay di);

		void					loadGroupFactors(const RuntimeData& rt);

	public:
								OpMultiFactorNeutralise(const ConfigSection& config);
		virtual 				~OpMultiFactorNeutralise();

		virtual void 			run(const RuntimeData& rt);
	};

	OpMultiFactorNeutralise::OpMultiFactorNeutralise(const ConfigSection& config) : IOperation(config, "OP_MULTI_FACTOR_NEUTRALISE") {
		std::string factorIds = config.getRequired("factors");
		m_factorIds = Util::split(factorIds, ",");
		ASSERT(m_factorIds.size(), "Invalid factors: " << factorIds);
	}

	OpMultiFactorNeutralise::~OpMultiFactorNeutralise() {
	}

	void OpMultiFactorNeutralise::loadGroupFactors(const RuntimeData& rt) {
		if (m_groupFactors.size())
			return;

		std::string groupIdsStr = config().getString("groups", "");
		if (groupIdsStr.empty())
			return;

		auto groupIds = Util::split(groupIdsStr, ",");
		if (!groupIds.size())
			return;

		for (const auto& groupId : groupIds) {
			const StringData* groupData = rt.dataRegistry->stringData(this->data().universe, groupId);
			size_t numStocks = this->data().universe->size(0);

			for (size_t ii = 0; ii < numStocks; ii++) {
				std::string iiValue = groupData->getValue(0, ii);
				auto iter = m_groupFactors.find(iiValue);

				/// If there is no existing factor then create one
				if (iter == m_groupFactors.end()) {
					std::shared_ptr<Eigen::VectorXf> groupFactor = std::make_shared<Eigen::VectorXf>(numStocks);
					groupFactor->setConstant(0.0f);
					m_groupFactors[iiValue] = groupFactor;
				}

				(*m_groupFactors[iiValue])(ii) = 1.0f;
			}
		}
	}

	Eigen::VectorXf OpMultiFactorNeutralise::getFactor(const RuntimeData& rt, size_t fid, IntDay di) {
		std::string factorId = m_factorIds[fid];
		factorId = factorId.find(".") == std::string::npos ? "gs_axioma." + factorId : factorId;
		const FloatMatrixData* factor = rt.dataRegistry->floatData(this->data().universe, factorId);
		return factor->mat().mat().row(di).nanClear();
	}

	void OpMultiFactorNeutralise::run(const RuntimeData& rt) {
		FloatMatrixData& alpha = this->alpha();
		Eigen::VectorXf cleanAlpha = alpha[0].nanClear();
		float len = std::sqrt(cleanAlpha.dot(cleanAlpha));

		/// Vector is almost 0 ... ignore it
		if (len < 0.001f) {
			return;
		}

		std::vector<std::shared_ptr<Eigen::VectorXf>> m;

		/// Now work with all the group factors ...
		loadGroupFactors(rt);
		for (auto iter = m_groupFactors.begin(); iter != m_groupFactors.end(); iter++)
			m.push_back(iter->second);

		for (size_t fid = 0; fid < m_factorIds.size(); fid++)
			m.push_back(std::make_shared<Eigen::VectorXf>(getFactor(rt, fid, rt.di - 1)));

		m.push_back(std::make_shared<Eigen::VectorXf>(cleanAlpha));

		for (int i = 0; i < m.size(); i++) 
			for (int j = 0; j < i; j++)
				*(m[i]) = Stats::neutralise(*(m[j]), *(m[i]));


		cleanAlpha = *(m[m.size() - 1]);

		float newAlphaLen = std::sqrt(cleanAlpha.dot(cleanAlpha));
		auto mask = alpha[0].nanMask(std::nanf("")).eval();
		alpha[0] = cleanAlpha; ///  * (len / newAlphaLen); 
		alpha[0] *= mask;

		//Stats::neutralise(alpha.mat().mat(), data->mat().mat(), di, m_factorScale, m_useResidual);
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createOpMultiFactorNeutralise(const pesa::ConfigSection& config) {
	return new pesa::OpMultiFactorNeutralise((const pesa::ConfigSection&)config);
}

