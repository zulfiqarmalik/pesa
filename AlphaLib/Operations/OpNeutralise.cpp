/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// OpNeutralise.cpp
///
/// Created on: 13 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"

namespace pesa {
	class OpNeutralise : public IOperation {
		typedef std::vector<float*> FloatPVec;

		struct GData {
			FloatVec			fdata;
			FloatPVec			fpdata;
		};

		typedef std::unordered_map<std::string, GData> FloatPVecMap;
	private:
		std::string				m_dataId = "classification.bbSector";
		bool					m_useMean = false;
		bool					m_groupNormalise = true;

		void					neutralise(const std::string& group, GData& data);

	public:
								OpNeutralise(const ConfigSection& config);
		virtual 				~OpNeutralise();

		virtual void 			run(const RuntimeData& rt);
	};

	OpNeutralise::OpNeutralise(const ConfigSection& config) : IOperation(config, "OP_NEUTRALISE") {
		m_dataId = config.getString("dataId", m_dataId);
		m_useMean = config.getBool("useMean", m_useMean);
		m_groupNormalise = config.getBool("groupNormalise", m_groupNormalise);
	}

	OpNeutralise::~OpNeutralise() {
	}

	void OpNeutralise::neutralise(const std::string&, GData& data) {
		size_t count = data.fdata.size();
		if (!count)
			return;

		float mean = 0.0f;
		if (!m_useMean)
			mean = Stats::medianUnsorted(&data.fdata[0], count);
		else
			mean = Stats::mean(&data.fdata[0], count);

		if (std::isnan(mean)) 
			return;

		float longSum = 0.0f, shortSum = 0.0f;
		for (size_t i = 0; i < count; i++) {
			float& f = *(data.fpdata[i]);
			f -= mean;

			/// The if conditions will handle NaNs correctly
			if (f > 0.0f)
				longSum += f;
			else if (f < 0.0f)
				shortSum += f;
		}
		
		if (!m_groupNormalise)
			return;
		float invLongSum = 1.0f;
		float invShortSum = 1.0f;
		if (longSum != 0.0f)
			invLongSum = 1.0f / longSum;

		if (shortSum != 0.0f)
			invShortSum = -1.0f / shortSum;

		for (size_t i = 0; i < count; i++) {
			float& f = *(data.fpdata[i]);
			if (f > 0.0f)
				f *= invLongSum;
			else if (f < 0.0f)
				f *= invShortSum;
		}
	}

	void OpNeutralise::run(const RuntimeData& rt) {
		FloatPVecMap groupAlpha;
		DataPtr data = rt.dataRegistry->getDataPtr(this->data().universe, m_dataId);
		ASSERT(data, "Unable to load data: " << m_dataId);
		UIntMatrixData* idata = nullptr;
		StringData* sdata = nullptr;
		auto type = data->def().itemType;

		if (type == DataType::kString)
			sdata = dynamic_cast<StringData*>(data.get());
		else if (type == DataType::kUInt)
			idata = dynamic_cast<UIntMatrixData*>(data.get());
		else
			ASSERT(false, "Unsupported data type: " << type << " - for dataId: " << m_dataId);

		II_LOOP() {
			float& avalue = alpha(0, ii);
			std::string group;
			if (sdata)
				group = (*sdata)[ii];
			else
				group = Util::cast<unsigned int>((*idata)(0, ii));

			//if (group == 0) {
			//	auto sec = m_data.universe->security(rt.di, ii);
			//	CWarning("Unable to find group for security: %s", sec.debugString());
			//}

			groupAlpha[group].fdata.push_back(avalue);
			groupAlpha[group].fpdata.push_back(&avalue);
		}

		CTrace("Number of groups to neutralise: %z", groupAlpha.size());

		/// OK now that we have all the groups ... we neutralise them 
		for (auto iter = groupAlpha.begin(); iter != groupAlpha.end(); iter++)
			neutralise(iter->first, iter->second);
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createOpNeutralise(const pesa::ConfigSection& config) {
	return new pesa::OpNeutralise((const pesa::ConfigSection&)config);
}

