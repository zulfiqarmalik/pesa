/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// OpRank.cpp
///
/// Created on: 22 Jun 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"

namespace pesa {
	struct AscCmp {
		bool operator() (float* lhs, float* rhs) { return (*lhs) < (*rhs); }
	};

	struct DescCmp {
		bool operator() (float* lhs, float* rhs) { return (*lhs) > (*rhs); }
	};
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class OpRank : public IOperation {
	private:
		bool 					m_desc = false;
		bool 					m_shrink = false;
		float 					m_skew = 0.0f;
        float 					m_power = 1.0f;
		void					assignRange(const std::vector<float*> &alphaPtrValues, size_t iiLast, size_t iiCurr);
		
	public:
								OpRank(const ConfigSection& config);
		virtual 				~OpRank();

		virtual void 			run(const RuntimeData& rt);
	};

	OpRank::OpRank(const ConfigSection& config) : IOperation(config, "OP_RANK") {
		m_desc = config.getBool("desc", m_desc);
		m_shrink = config.getBool("shrink", m_shrink);
		m_skew = config.get<float>("skew", m_skew);
        m_power = config.get<float>("power", m_power);
	}

	OpRank::~OpRank() {
	}

	void OpRank::assignRange(const std::vector<float*>& alphaPtrValues, size_t iiLast, size_t iiCurr) {
		float valueToAssign = 0.5f * (iiCurr + iiLast - 1.0f);
		for (size_t ii = iiLast; ii < iiCurr; ii++) {
			*alphaPtrValues[ii] = valueToAssign;
		}
	}

	void OpRank::run(const RuntimeData& rt) {
		FloatMatrixData& alpha = this->alpha();
		std::vector<float*> alphaPtrValues;

		for (size_t ii = 0; ii < alpha.cols(); ii++) {
			float& value = alpha(0, ii);
			if (std::isfinite(value)) {
				alphaPtrValues.push_back(&value);
			}
		}

		if (alphaPtrValues.empty())
			return;

		if (!m_desc)
			std::sort(alphaPtrValues.begin(), alphaPtrValues.end(), AscCmp());
		else
			std::sort(alphaPtrValues.begin(), alphaPtrValues.end(), DescCmp());

		size_t iiLast = 0;
		float lastValue = *alphaPtrValues[0];
		for (size_t ii = 1; ii < alphaPtrValues.size(); ii++) {
			float* value = alphaPtrValues[ii];

			if (*value != lastValue) {
				assignRange(alphaPtrValues, iiLast, ii);
				iiLast = ii;
				lastValue = *value;
			}
		}

		assignRange(alphaPtrValues, iiLast, alphaPtrValues.size());

		if (m_shrink && (alphaPtrValues.size() > 1)) {
			float invsize = 1.0f / (alphaPtrValues.size() - 1.0f);
			float skewParam = exp(m_skew) - 1.0f;

			for (auto &it : alphaPtrValues) {
				*it *= invsize;
				if (m_skew != 0.0f) {
					*it = *it * (skewParam + 1.0f) / (*it * skewParam + 1.0f);
				}
				if (m_power != 1.0f) {
					*it -= 0.5f;
					float sign = (*it > 0) ? 1.0f : -1.0f;
					*it = sign * powf(fabs(*it), m_power);
				}
			}
		}
	}

} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createOpRank(const pesa::ConfigSection& config) {
	return new pesa::OpRank((const pesa::ConfigSection&)config);
}

