/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// OpWinsorise.cpp
///
/// Created on: 13 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class OpWinsorise : public IOperation {
	private:
		float 					m_std = 3.0f;
		std::string				m_shrink = "clamp";

	public:
								OpWinsorise(const ConfigSection& config);
		virtual 				~OpWinsorise();

		virtual void 			run(const RuntimeData& rt);
	};

	OpWinsorise::OpWinsorise(const ConfigSection& config) 
		: IOperation(config, "OP_WINSORISE") {
		m_std = config.get<float>("std", m_std);
		m_shrink = config.getString("shrink", m_shrink);
	}

	OpWinsorise::~OpWinsorise() {
	}

	void OpWinsorise::run(const RuntimeData& rt) {
		auto& alpha = this->alpha();
		size_t numAlpha = alpha.cols();

		/// Nothing to do over here!
		if (!numAlpha)
			return;

		float stddev = Stats::standardDeviation(alphaPtr(), numAlpha);
		float median = Stats::medianUnsorted(alphaPtr(), numAlpha);

		if (std::isnan(median) || std::isnan(stddev))
			return;

		ASSERT(!std::isnan(median), "How can median be NaN!?");

		for (size_t ii = 0; ii < numAlpha; ii++) {
			float& f = alpha(0, ii);
			float stddevOffset = std::abs(f - median) / stddev;

			if (stddevOffset >= m_std) {
				/// limit it to the max stddev from the median (and copy the sign of the original alpha)
				if (m_shrink == "clamp") {
					float newValue = median + (stddev * m_std * std::copysign(1.0f, f));
					f = newValue;
				}
				else if (m_shrink == "median")
					f = median;
				else if (m_shrink == "nan")
					f = std::nanf("");
				else {
					ASSERT(false, "Unsupported shrink operation: " << m_shrink << " - Supported shrink values are 'clamp' (default), 'median' and 'nan')");
				}
			}
		}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createOpWinsorise(const pesa::ConfigSection& config) {
	return new pesa::OpWinsorise((const pesa::ConfigSection&)config);
}

