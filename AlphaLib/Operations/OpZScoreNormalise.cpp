/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// OpZScoreNormalise.cpp
///
/// Created on: 22 Jun 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class OpZScoreNormalise : public IOperation {
	private:
		bool 					m_useMean = false;

	public:
								OpZScoreNormalise(const ConfigSection& config);
		virtual 				~OpZScoreNormalise();

		virtual void 			run(const RuntimeData& rt);
	};

	OpZScoreNormalise::OpZScoreNormalise(const ConfigSection& config) : IOperation(config, "OP_NORMALISE") {
		m_useMean = config.getBool("useMean", m_useMean);
	}

	OpZScoreNormalise::~OpZScoreNormalise() {
	}

	void OpZScoreNormalise::run(const RuntimeData& rt) {
		FloatMatrixData& alpha = this->alpha();
		float* fdata = alpha.fdata();
		size_t count = alpha.cols();
		float distMean = Stats::mean(fdata, count);
		float mean = distMean;

		if (!m_useMean) 
			mean = Stats::medianUnsorted(fdata, count);

		/// We don't do anything if we get a NaN mean
		if (std::isnan(mean))
			return;

		float stddev = Stats::standardDeviation(fdata, count, distMean);
		if (stddev == 0.0f)
			stddev = 1.0f;

		//CTrace("Pre normalise long/short count: %z/%z", Stats::positiveCount(fdata, count), Stats::negativeCount(fdata, count));
		//CTrace("Mean: %0.2hf", mean);

		/// The if conditions will handle NaNs correctly
		float longSum = 0.0f;
		float shortSum = 0.0f;

		for (size_t i = 0; i < count; i++) {
			float& f = *(fdata + i);
			f = (f - mean) / stddev;

			if (f > 0.0f)
				longSum += f;
			else if (f < 0.0f)
				shortSum += f;
		}

		float invLongSum = 1.0f;
		float invShortSum = 1.0f;
		if (longSum != 0.0f)
			invLongSum = 1.0f / longSum;

		if (shortSum != 0.0f)
			invShortSum = -1.0f / shortSum;

		for (size_t i = 0; i < count; i++) {
			float& f = *(fdata + i);
			if (f > 0.0f)
				f *= invLongSum;
			else if (f < 0.0f)
				f *= invShortSum;
		}

		//CDebug("Post normalise long/short count: %z/%z", Stats::positiveCount(fdata, count), Stats::negativeCount(fdata, count));
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createOpZScoreNormalise(const pesa::ConfigSection& config) {
	return new pesa::OpZScoreNormalise((const pesa::ConfigSection&)config);
}

