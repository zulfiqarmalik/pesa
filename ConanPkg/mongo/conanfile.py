from conans import ConanFile, AutoToolsBuildEnvironment, CMake, tools
import os, shutil

class MongoConan(ConanFile):
    name            = "mongocxx"
    version         = "3.2.0"
    cdriver_version = "1.9.2"

    url             = "https://github.com/zulfi/conan-mongocxx"
    license         = "Apache License 2.0 (https://github.com/mongodb/libbson/blob/master/COPYING)"
    homepage        = "https://github.com/mongodb/mongo-cxx-driver"
    description     = "Full mongocxx driver."
    settings        = "os", "compiler", "build_type", "arch"
    options         = {"shared": [True, False]}
    default_options = "shared=True"
    exports         = ["LICENSE.md"]
    exports_sources = ["CMakeLists.txt", "CMakeLists_cxx.txt"]
    boost_root      = os.environ["BOOST_INCLUDEDIR"]
    generators      = "cmake", "txt"

    assert boost_root, "Invalid boost include directory. Must define BOOST_INCLUDEDIR environment variable!"

    def config_options(self):
       del self.settings.compiler.libcxx
        # self.settings.compiler.version = "14"

    def source_libbson(self):
        tools.get("https://github.com/mongodb/libbson/releases/download/%s/libbson-%s.tar.gz" % (self.cdriver_version, self.cdriver_version))
        os.rename("libbson-%s" % self.cdriver_version, "libbson_sources")
        os.rename("libbson_sources/CMakeLists.txt", "libbson_sources/CMakeListsOriginal.txt")
        shutil.copy("CMakeLists.txt", "libbson_sources/CMakeLists.txt")

    def source_cdriver(self):
        tools.get("https://github.com/mongodb/mongo-c-driver/releases/download/%s/mongo-c-driver-%s.tar.gz" % (self.cdriver_version, self.cdriver_version))
        os.rename("mongo-c-driver-%s" % self.cdriver_version, "cdriver_sources")
        os.rename("cdriver_sources/CMakeLists.txt", "cdriver_sources/CMakeListsOriginal.txt")
        shutil.copy("CMakeLists.txt", "cdriver_sources/CMakeLists.txt")

    def source_cxxdriver(self):
        tools.get("https://github.com/mongodb/mongo-cxx-driver/archive/r%s.tar.gz" % (self.version))
        os.rename("mongo-cxx-driver-r%s" % self.version, "cxxdriver_sources")
        os.rename("cxxdriver_sources/CMakeLists.txt", "cxxdriver_sources/CMakeListsOriginal.txt")
        shutil.copy("CMakeLists_cxx.txt", "cxxdriver_sources/CMakeLists.txt")

    def source(self):
        self.source_libbson()
        self.source_cdriver()
        self.source_cxxdriver()

    def build(self):
        self.build_libbson()
        self.build_cdriver()
        self.build_cxxdriver()

    def build_libbson(self):
        self.build_component(base_path = "libbson_sources")

    def build_cdriver(self):
        self.build_component(base_path = "cdriver_sources")

    def build_cxxdriver(self):
        self.build_component(base_path = "cxxdriver_sources/build", relative_path = "..", additional_defs = {
            "LIBBSON_DIR": ("%s/_inst" % self.build_folder),
            "LIBMONGOC_DIR": ("%s/_inst" % self.build_folder),
            "BOOST_INCLUDEDIR": self.boost_root
        }, force_cmake = True)

    def build_component(self, base_path, relative_path = "", additional_defs = None, force_cmake = False):
        # cmake is supported only for Visual Studio
        use_cmake = self.settings.compiler == "Visual Studio" or self.settings.os == "Macos" or force_cmake
        source_path = os.path.join(base_path, relative_path)
        build_dir = os.path.join(self.build_folder, base_path)
        print("Build Dir: %s" % (build_dir))

        if use_cmake:
            cmake = CMake(self)
            # upstream cmake is flawed and doesn't understand boolean values other than ON/OFF
            cmake.definitions["ENABLE_STATIC"] = "OFF" if self.options.shared else "ON"
            cmake.definitions["ENABLE_TESTS"] = False
            cmake.definitions["CMAKE_INSTALL_PREFIX"] = ("%s/_inst" % self.build_folder)

            if additional_defs is not None:
                for key, value in additional_defs.items():
                    cmake.definitions[key] = value

            cmake.verbose = True
            cmake.configure(source_folder = source_path, build_folder = build_dir)
            cmake.build()
            cmake.install()

        else:

            env_build = AutoToolsBuildEnvironment(self, win_bash=self.settings.os == 'Windows')

            # compose configure options
            prefix = os.path.abspath(os.path.join(self.build_folder, "_inst"))
            if self.settings.os == 'Windows':
                prefix = tools.unix_path(prefix)
            configure_args = ['--prefix=%s' % prefix]

            if self.options.shared:
                configure_args.extend(["--enable-shared", "--disable-static"])
            else:
                configure_args.extend(["--disable-shared", "--enable-static"])

            configure_args.extend(["--enable-examples=no", "--enable-tests=no"])

            with tools.chdir(source_path):
                # refresh configure
                # self.run('autoreconf --force --verbose --install -I build/autotools', win_bash=self.settings.os == 'Windows')

                # disable rpath build
                # tools.replace_in_file("configure", r"-install_name \$rpath/", "-install_name ")

                env_build.configure(args=configure_args)
                env_build.make(args=['install'])


    def package(self):
        # self.copy("copying*", src="sources", dst="licenses", ignore_case=True, keep_path=False)
        self.copy(pattern="*", dst="include", src="_inst/include", keep_path=True)

        # autotools has a bug on mingw: it does not copy bson-stdint.h so copy it manually
        if self.settings.os == "Windows" and self.settings.compiler != "Visual Studio":
            self.copy(pattern="bson-stdint.h", dst=os.path.join("include", "libbson-1.0"), src=os.path.join("sources", "build", "cmake", "bson"), keep_path=False)

        if self.options.shared:
            if self.settings.os == "Macos":
                self.copy(pattern="*.dylib", src="_inst/lib", dst="lib", keep_path=False)

            elif self.settings.os == "Windows":
                self.copy(pattern="*.dll*", src="_inst/bin", dst="bin", keep_path=False, excludes=['vcruntime*.dll', 'msvcp*.dll', 'concrt*.dll'])
                # mingw dll import libraries: libbson*.dll.a
                self.copy(pattern="*.dll.a", src="_inst/lib", dst="lib", keep_path=False)
            else:
                self.copy(pattern="*.so*", src="_inst/lib", dst="lib", keep_path=False)
        else:
            self.copy(pattern="*.a", src="_inst/lib", dst="lib", keep_path=False, excludes='*dll*')

        if self.settings.compiler == "Visual Studio":
            self.copy(pattern="*.lib*", src="_inst/lib", dst="lib", keep_path=False)

    def package_info(self):
        if self.options.shared:
            self.cpp_info.libs = ['bson-1.0', 'mongoc-1.0', 'bsoncxx', 'mongocxx']
        else:
            self.cpp_info.libs = ['bson-static-1.0', 'mongoc-static-1.0', 'bsoncxx', 'mongocxx']

        self.cpp_info.includedirs = ['include/libbson-1.0', 'include/libmongoc-1.0', 'include/bsoncxx/v_noabi', 'include/mongocxx/v_noabi']

        if self.settings.os == "Linux":
            self.cpp_info.libs.extend(["pthread", "rt"])

        if self.settings.os == "Windows":
            if not self.options.shared:
                self.cpp_info.libs.extend(["ws2_32"])
                self.cpp_info.defines.append("BSON_STATIC=1")
