from conans import ConanFile, AutoToolsBuildEnvironment, tools, VisualStudioBuildEnvironment
import os

class MongoConan(ConanFile):
    name            = "python"
    version         = "3.5.1"

    url             = "https://github.com/zulfi/conan-mongocxx"
    license         = "Apache License 2.0 (https://github.com/mongodb/libbson/blob/master/COPYING)"
    homepage        = "https://github.com/mongodb/mongo-cxx-driver"
    description     = "CPython 3.5.1 embedded."
    settings        = "os", "compiler", "build_type", "arch"
    exports         = ["LICENSE.md"]
    generators      = "txt"
    options         = {"shared": [True, False]}
    default_options = "shared=True"

    def config_options(self):
        del self.settings.compiler.libcxx

    def source(self):
        tools.get("https://www.python.org/ftp/python/%s/Python-%s.tgz" % (self.version, self.version))
        os.rename("Python-%s" % self.version, "sources")

    def build(self):
        # cmake is supported only for Visual Studio
        source_path = "sources"

        if self.settings.os == 'Windows':
# msbuild "D:\Work\Python-3.5.1\Python-3.5.1\PCbuild\pcbuild.proj" /t:Build /m /nologo /v:m /p:Configuration=Release /p:Platform=Win32 /p:IncludeExternals=false /p:IncludeSSL=true /p:IncludeTkinter=true /p:UseTestMarker=
            build_env = VisualStudioBuildEnvironment(self)

            with tools.chdir("sources"):
                with tools.environment_append(build_env.vars):
                    command = tools.msvc_build_command(self.settings, "PCbuild/pcbuild.proj", targets=None, upgrade_project=True, build_type="Release", arch=None, parallel=True, force_vcvars=False, toolset=None)

                    command = "cd " + os.path.join(self.build_folder, "sources") + " && " + command + " /p:Platform=x64 /p:IncludeExternals=false /p:IncludeSSL=true"
                    print("Running Command: %s" % (command))

                    self.run(command)

                    # import subprocess

                    # command = ["msbuild", "PCbuild/pcbuild.proj", "/p:Platform=x64", "/p:IncludeExternals=false", "/p:IncludeSSL=true"]
                    # print("Running command: %s" % (" ".join(command)))
                    # msbuild_proc = subprocess.Popen(command, shell = True, cwd = os.path.join(self.build_folder, "sources"))
                    # while msbuild_proc.poll() is None:
                    #     pass

                    # assert msbuild_proc.return_code == 0, "MSBuild returned error: %d" % (msbuild_proc.returncode)

            # msbuild = MSBuild(self)
            # msbuild.build(project_file = os.path.join("sources/PCbuild/pcbuild.proj"), arch = "x64")

        else:

            env_build = AutoToolsBuildEnvironment(self, win_bash=self.settings.os == 'Windows')

            # compose configure options
            prefix = os.path.abspath(os.path.join(self.build_folder, "_inst"))
            if self.settings.os == 'Windows':
                prefix = tools.unix_path(prefix)
            configure_args = ['--prefix=%s' % prefix]

            if self.options.shared:
                configure_args.extend(["--enable-shared", "--disable-static"])
            else:
                configure_args.extend(["--disable-shared", "--enable-static"])

            configure_args.extend(["--enable-examples=no", "--enable-tests=no"])

            with tools.chdir(source_path):
                # refresh configure
                self.run('autoreconf --force --verbose --install -I build/autotools', win_bash=self.settings.os == 'Windows')

                # disable rpath build
                # tools.replace_in_file("configure", r"-install_name \$rpath/", "-install_name ")

                env_build.configure(args=configure_args)
                env_build.make(args=['install'])

    def package(self):
        # self.copy("copying*", src="sources", dst="licenses", ignore_case=True, keep_path=False)
        win_output_dir = os.path.join(self.build_folder, "sources", "PCbuild", "amd64") 
        self.copy(pattern="*.h", dst="include", src=os.path.join(self.build_folder, "sources", "Include"), keep_path=True)

        if self.settings.os == "Windows"        :
            self.copy(pattern="pyconfig.h", dst="include", src=os.path.join(self.build_folder, "sources", "PC"), keep_path=True)
        else:
            self.copy(pattern="pyconfig.h", dst="include", src=os.path.join(self.build_folder, "sources"), keep_path=True)

        if self.settings.os == "Macos":
            self.copy(pattern="*.dylib", src="_inst/lib", dst="lib", keep_path=False)

        elif self.settings.os == "Windows":
            self.copy(pattern="python*.dll", src=win_output_dir, dst="bin", keep_path=False)
            self.copy(pattern="python*.lib", src=win_output_dir, dst="lib", keep_path=False)

        else:
            self.copy(pattern="*.so*", src="_inst/lib", dst="lib", keep_path=False)

    def package_info(self):
        if self.settings.os == "Macos":
            self.cpp_info.libs = ['libpython3.5m.dylib'] #['python_' + self.version]

        elif self.settings.os == "Windows":
            # if self.settings.build_type == "Debug":
            #     self.cpp_info.libs = ['python35_d']
            #     # self.cpp_info.libs = ['python3_d.lib']
            #     # self.cpp_info.libs = ['python3_dstub.lib']
            # else:
            #     self.cpp_info.libs = ['python35']
            #     # self.cpp_info.libs = ['python3.lib']
            #     # self.cpp_info.libs = ['python3stub.lib']

            # Right now we always build the release build
            self.cpp_info.libs = ['python35']
        else:
            self.cpp_info.libs = ['python3.so', 'python3.5m.so']
        # elif self.settings == "Windows":
        #     self.cpp_info.libs = ['python3'] #['python_' + self.version]

        self.cpp_info.includedirs = ['include']
