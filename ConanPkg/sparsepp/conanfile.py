import os
import shutil

from conans import CMake
from conans import ConanFile, tools


class SparsePPConan(ConanFile):
    name = "sparsepp"
    version = "master"
    url = "https://github.com/greg7mdp/sparsepp"

    def source(self):
        zip_name = "sparsepp-%s.zip" % self.version
        downloadFilename = "https://github.com/greg7mdp/sparsepp/archive/%s.zip" % self.version
        tools.download(downloadFilename, zip_name)
        tools.unzip(zip_name)
        shutil.move("sparsepp-%s" % self.version, "sparsepp")
        os.unlink(zip_name)

    def package(self):
        self.copy(pattern="*.h", dst="include", src="sparsepp")

    def package_info(self):
        self.cpp_info.includedirs = ['include']
