@baseDataD0 | ravenPack | baseData

$x0 / $x1
$x0 * $x1
Sum($x0 / $x1) [days=5|10|21|63]
IR($x0 / $x1) [days=5|10|21|63]
Sign((Delta($x0,days=1))*(-1*Delta($x1))) [days = 1|3|5|21|63]
Sign((Delta($x0,days=3))*(-1*Delta($x1))) [days = 1|3|5|21|63]
GT(Sum($x0, days=21)/21 , $x0,  $x1, (-1 * Delta($x1))) [days = 1|3|5|21|63]
GT(Mean($x0 / $x1, days=5), Mean($x0 / $x1), Mean($x0 / $x1, days=5), -1*Mean($x0 / $x1, days=5)) [days = 1|3|5|21|63]
Log(Sum(Delta(Pow($x0,exponent=3),days=2),days=5) * Sum(Delta(Pow($x1,exponent=3),days=2),days=5))

