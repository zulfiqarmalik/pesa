@baseData | derivedData | shortTrends | qsg | ford | ER | axioma | ravenPack | starmine | tim | factset

$x0
$x0 * baseData.adv20
$x0 / baseData.adv20
$x0 * Sum(baseData.returns) [days = 5|21|63|120|250, omitDays = 0|5|21]
$x0 * Sum(baseData.adjVolume / baseData.adv20, days = 5)
$x0 * (baseData.adjMCap)
$x0 * (derivedData.linMCap)
$x0 * (derivedData.r_std)
$x0 / (derivedData.r_std)
$x0 * (derivedData.p_std)
$x0 / (derivedData.p_std)
$x0 / (derivedData.pH)
$x0 / (derivedData.pL)
$x0 / (derivedData.pH / derivedData.pL)
$x0 / (derivedData.dH)
$x0 / (derivedData.dL)
$x0 / (derivedData.dH / derivedData.dL)
$x0 / (derivedData.pdiffH)
$x0 / (derivedData.pdiffL)
$x0 / (derivedData.pdiffH / derivedData.pdiffL)
$x0 * (derivedData.r_std) * baseData.adv20
$x0 * (derivedData.r_std) * (derivedData.linMCap)
$x0 / (baseData.adjClosePrice)
$x0 * (baseData.adjClosePrice)
// $x0 * Sum(baseData.adjClosePrice/baseData.adjVWAP) [days = 3|5|10|21|63]


$x0 - Mean($x0) [days = 5|21|63|250]
($x0 - Mean($x0)) * baseData.adv20 [days = 5|21|63|250]
($x0 - Mean($x0)) / baseData.adv20 [days = 5|21|63|250]
// ($x0 - Mean($x0)) * Sum(baseData.returns, {days=5|21|63|120|250}, {omitDays=0|5|21}) [days = 5|21|63|250]
($x0 - Mean($x0)) * Sum(baseData.adjVolume / baseData.adv20, days = 5) [days = 5|21|63|250]


Mean($x0)   [days = 5|21|63|250]
Mean($x0) * baseData.adv20 [days = 5|21|63|250]
Mean($x0) / baseData.adv20 [days = 5|21|63|250]


(Mean($x0, days = 5) / Mean($x0)) [days=21|63|120]
(Mean($x0, days = 5) / Mean($x0)) * baseData.adv20 [days=21|63|120]
(Mean($x0, days = 5) / Mean($x0)) / baseData.adv20 [days=21|63|120]


(Mean($x0, days = 21) / Mean($x0)) [days=63|120|250]
(Mean($x0, days = 21) / Mean($x0)) * baseData.adv20 [days=63|120|250]
(Mean($x0, days = 21) / Mean($x0)) / baseData.adv20 [days=63|120|250]


(Mean($x0, days = 5) - Mean($x0)) [days=21|63|120|250]
(Mean($x0, days = 5) - Mean($x0)) * baseData.adv20 [days=21|63|120|250]
(Mean($x0, days = 5) - Mean($x0)) / baseData.adv20 [days=21|63|120|250]



(Mean($x0, days = 21) - Mean($x0)) [days=63|120|250]
(Mean($x0, days = 21) - Mean($x0)) * baseData.adv20 [days=63|120|250]
(Mean($x0, days = 21) - Mean($x0)) / baseData.adv20 [days=63|120|250]


Returns($x0) [days = 5|21|63|120|250]
Returns($x0) * baseData.adv20 [days = 5|21|63|120|250]
Returns($x0) / baseData.adv20 [days = 5|21|63|120|250]

Spread($x0) [days = 5|21|63|120|250]
Spread($x0) * baseData.adv20 [days = 5|21|63|120|250]
Spread($x0) / baseData.adv20 [days = 5|21|63|120|250]


ZScore($x0) [days = 5|21|63|120|250]
ZScore($x0) * baseData.adv20 [days = 5|21|63|120|250]
ZScore($x0) / baseData.adv20 [days = 5|21|63|120|250]


StdDev($x0) [days = 5|21|63|120|250]
StdDev($x0) * baseData.adv20 [days = 5|21|63|120|250]
StdDev($x0) / baseData.adv20 [days = 5|21|63|120|250]

Skew($x0) [days = 5|21|63|120|250]
Skew($x0) * baseData.adv20 [days = 5|21|63|120|250]
Skew($x0) / baseData.adv20 [days = 5|21|63|120|250]

Kurtosis($x0) [days = 5|21|63|120|250]
Kurtosis($x0) * baseData.adv20 [days = 5|21|63|120|250]
Kurtosis($x0) / baseData.adv20 [days = 5|21|63|120|250]


Momentum($x0)  [days = 5|21|63|250]
Momentum($x0) * baseData.adv20 [days = 5|21|63|250]
Momentum($x0) / baseData.adv20 [days = 5|21|63|250]

Rank($x0) [days = 21|63|120|250]
Rank($x0) * baseData.adv20 [days = 21|63|120|250]
Rank($x0) / baseData.adv20 [days = 21|63|120|250]

DeltaSum($x0) [days = 21|63|120|250]
DeltaSum($x0) * baseData.adv20 [days = 21|63|120|250]
DeltaSum($x0) / baseData.adv20 [days = 21|63|120|250]

AvgReturns($x0) [days = 21|63|120|250]
AvgReturns($x0) * baseData.adv20 [days = 21|63|120|250]
AvgReturns($x0) / baseData.adv20 [days = 21|63|120|250]

DeltaPct($x0) [days = 21|63|120|250]
DeltaPct($x0) * baseData.adv20 [days = 21|63|120|250]
DeltaPct($x0) / baseData.adv20 [days = 21|63|120|250]

IntAverage($x0) [days = 21|63|120|250]
IntAverage($x0) * baseData.adv20 [days = 21|63|120|250]
IntAverage($x0) / baseData.adv20 [days = 21|63|120|250]


Min($x0) [days = 21|63|120|250]
Min($x0) * baseData.adv20 [days = 21|63|120|250]
Min($x0) / baseData.adv20 [days = 21|63|120|250]


Max2($x0) [days = 21|63|120|250]
Max2($x0) * baseData.adv20 [days = 21|63|120|250]
Max2($x0) / baseData.adv20 [days = 21|63|120|250]

(Max2($x0) - Min($x0)) [days = 21|63|120|250]
(Max2($x0) - Min($x0)) * baseData.adv20 [days = 21|63|120|250]
(Max2($x0) - Min($x0)) / baseData.adv20 [days = 21|63|120|250]


(Max2($x0) - Mean($x0)) [days = 21|63|120|250]
(Max2($x0) - Mean($x0)) * baseData.adv20 [days = 21|63|120|250]
(Max2($x0) - Mean($x0)) / baseData.adv20 [days = 21|63|120|250]

(Min($x0) - Mean($x0) ) [days = 21|63|120|250]
(Min($x0) - Mean($x0)) * baseData.adv20 [days = 21|63|120|250]
(Min($x0) - Mean($x0)) / baseData.adv20 [days = 21|63|120|250]

IR($x0)   [days = 5|21|63|250]
IR($x0) * baseData.adv20 [days = 5|21|63|250]
IR($x0) / baseData.adv20 [days = 5|21|63|250]

(IR($x0, days = 5) / IR($x0)) [days=21|63|120|250]
(IR($x0, days = 21) / IR($x0)) [days=63|120|250]
(IR($x0, days = 21) / IR($x0)) * baseData.adv20 [days=63|120|250]
(IR($x0, days = 21) / IR($x0)) / baseData.adv20 [days=63|120|250]

(IR($x0, days = 63) / IR($x0)) [days = 120|250]
(IR($x0, days = 63) / IR($x0)) * baseData.adv20 [days = 120|250]
(IR($x0, days = 63) / IR($x0)) / baseData.adv20 [days = 120|250]

(ZScore($x0, days = 63) / ZScore($x0)) [days = 120|250]
(ZScore($x0, days = 63) / ZScore($x0)) * baseData.adv20 [days = 120|250]
(ZScore($x0, days = 63) / ZScore($x0)) / baseData.adv20 [days = 120|250]

(ZScore($x0, days = 63) - ZScore($x0)) [days = 120|250]
(ZScore($x0, days = 63) - ZScore($x0)) * baseData.adv20 [days = 120|250]
(ZScore($x0, days = 63) - ZScore($x0)) / baseData.adv20 [days = 120|250]

(Rank($x0, days = 21) - Rank($x0)) [days = 120|250]
(Rank($x0, days = 21) - Rank($x0)) * baseData.adv20 [days = 120|250]
(Rank($x0, days = 21) - Rank($x0)) / baseData.adv20 [days = 120|250]

(Rank($x0, days = 63) - Rank($x0)) [days = 120|250]
(Rank($x0, days = 63) - Rank($x0)) * baseData.adv20 [days = 120|250]
(Rank($x0, days = 63) - Rank($x0)) / baseData.adv20 [days = 120|250]


(Rank($x0, days = 63) * IR($x0)) [days = 120|250]
(Rank($x0, days = 63) * IR($x0)) * baseData.adv20 [days = 120|250]
(Rank($x0, days = 63) * IR($x0)) / baseData.adv20 [days = 120|250]

(DeltaSum($x0, days = 21) - DeltaSum($x0)) [days = 120|250]
(DeltaSum($x0, days = 21) - DeltaSum($x0)) * baseData.adv20 [days = 120|250]
(DeltaSum($x0, days = 21) - DeltaSum($x0)) / baseData.adv20 [days = 120|250]

(DeltaSum($x0, days = 63) - DeltaSum($x0)) [days = 120|250]
(DeltaSum($x0, days = 63) - DeltaSum($x0)) * baseData.adv20 [days = 120|250]
(DeltaSum($x0, days = 63) - DeltaSum($x0)) / baseData.adv20 [days = 120|250]

(DeltaSum($x0, days = 63) * IR($x0)) [days = 120|250]
(DeltaSum($x0, days = 63) * IR($x0)) * baseData.adv20 [days = 120|250]
(DeltaSum($x0, days = 63) * IR($x0)) / baseData.adv20 [days = 120|250]


Delta($x0) [days = 21|63|120|250]
Delta($x0) * baseData.adv20 [days = 21|63|120|250]
Delta($x0) / baseData.adv20 [days = 21|63|120|250]


Sum($x0) [days=5|10|21|63|120|250]
Sum($x0) * Sum*($x0) [days=5|10|21|63|120|250]
Sum($x0) * baseData.adv20 [days=5|10|21|63|120|250]
Sum($x0) / baseData.adv20 [days=5|10|21|63|120|250]


SignedPow($x0)  [exponent = 0.5|2|3]
SignedPow($x0) * Sum*($x0)  [exponent = 0.5|2|3]
SignedPow($x0) * baseData.adv20  [exponent = 0.5|2|3]
SignedPow($x0) / baseData.adv20  [exponent = 0.5|2|3]


Pow($x0)   [exponent = 0.5|2|3]
Pow($x0) * Sum*($x0) [exponent = 0.5|2|3]
Pow($x0) * baseData.adv20 [exponent = 0.5|2|3]
Pow($x0) / baseData.adv20 [exponent = 0.5|2|3]


(Delta($x0, days=5|10|21) * IR($x0)) [days = 120|250]
(Delta($x0, days=5|10|21) * IR($x0)) * baseData.adv20 [days = 120|250]
(Delta($x0, days=5|10|21) * IR($x0)) / baseData.adv20 [days = 120|250]


(Delta($x0, days=5|10|21|63) * Rank($x0)) [days=21|63|120|250]
(Delta($x0, days=5|10|21|63) * Rank($x0)) * baseData.adv20 [days=21|63|120|250]
(Delta($x0, days=5|10|21|63) * Rank($x0)) / baseData.adv20 [days=21|63|120|250]


UpDownDays(baseData.returns) [days = 5|21|120]
UpDownDays(Delta($x0)) [days = 5|21|120]
MomentumCum($x0) - StdDev($x0, days = 6) [days = 5|21|120|250]
Delta($x0) * Exp(1.0 - (baseData.adjVolume / baseData.adv20))
($x0 - Mean($x0)) * Exp(1.0 - (baseData.adjVolume / baseData.adv20))

Rank($x0, days = 63) - Rank($x0, days = 250)
Rank($x0, days = 63) - Rank($x0, days = 120)
Rank($x0, days = 120) - Rank($x0, days = 250)

Rank($x0, days = 30) * Sum(baseData.adjVolume / baseData.adv20, days = 5)
Rank($x0, days = 63) * Sum(baseData.adjVolume / baseData.adv20, days = 5)
Rank($x0, days = 120) * Sum(baseData.adjVolume / baseData.adv20, days = 5)

Sum(Delta($x0)) [days = 21|120|250]
Sum(Abs(Delta($x0))) [days = 21|120|250]
Sum(Sign(Delta($x0))) [days = 21|120|250]
Sum(Ln(Delta($x0))) [days = 21|120|250]
Sum(Exp($x0)) [days = 21|120|250]
Sum(Log10($x0)) [days = 21|120|250]
Sum(DeltaPct($x0)) [days = 21|120|250]
Sum(Min($x0)) [days = 21|120|250]
Sum(Max2($x0)) [days = 21|120|250]
Sum(MCap($x0)) [days = 21|120|250]
Sum(IntAverage($x0 [days = 21|120|250])
Sum(UpDownDays($x0 [days = 21|120|250])


Rank($x0, days = 5|21|63|120)  - Rank(baseData.adjClosePrice,days=5|21||63|120|250, omitDays=0|5|21)

Sum($x0 / baseData.adv20, days = 5)
Sum(($x0 / baseData.adv20) * baseData.returns, days = 5)
StdDev(baseData.returns, days = 60) * Sum($x0 / baseData.adv20, days = 5)
StdDev(baseData.returns, days = 60) * $x0
Rank($x0, days = 250, omitDays = 5)
Rank($x0, days = 250, omitDays = 5) * Sum(baseData.adjVolume / baseData.adv20, days = 5)
Rank($x0, days = 250, omitDays = 5) / StdDev(baseData.returns, days = 60)
(Rank($x0, days = 250, omitDays = 5) / StdDev(baseData.returns, days = 60)) * Sum(baseData.adjVolume / baseData.adv20, days = 5)
MomentumCum($x0, days = 250) / StdDev(baseData.returns, days = 60)
Rank($x0, days = 21)
Rank($x0, days = 21) * Sum(baseData.adjVolume / baseData.adv20, days = 5)
Mean($x0, days = 21) / StdDev(baseData.returns, days = 21)
Mean($x0, days = 120) / StdDev(baseData.returns, days = 21)
Mean($x0, days = 5) - Mean(baseData.adjClosePrice, days = 21)

CumReturns($x0) [days = 5|21|63|120]


Techs_EMA(baseData.adjHighPrice / baseData.adjClosePrice) [days = 5|21|63|120]
Rank(baseData.adjHighPrice / baseData.adjClosePrice) [days = 5|21|63|120]
StdDev(baseData.adjHighPrice / baseData.adjClosePrice) [days = 5|21|63|120]
Mean(baseData.adjHighPrice / baseData.adjClosePrice) [days = 5|21|63|120]
Spread(baseData.adjHighPrice / baseData.adjClosePrice) [days = 5|21|63|120]
ZScore(baseData.adjHighPrice / baseData.adjClosePrice) [days = 5|21|63|120]
Delta(baseData.adjHighPrice / baseData.adjClosePrice) [days = 5|21|63|120]
Min(baseData.adjHighPrice / baseData.adjClosePrice) [days = 5|21|63|120]
Max2(baseData.adjHighPrice / baseData.adjClosePrice) [days = 5|21|63|120]
Mean(baseData.adjHighPrice / baseData.adjClosePrice, days = 5) - Mean(baseData.adjHighPrice / baseData.adjClosePrice) [days = 21|63|120]
UpDownDays(Delta(baseData.adjHighPrice / baseData.adjClosePrice, days = 5)) - UpDownDays(Delta(baseData.adjHighPrice / baseData.adjClosePrice)) [days = 21|63|120]



Rank(baseData.adjLowPrice / baseData.adjClosePrice) [days = 5|21|63|120]
StdDev(baseData.adjLowPrice / baseData.adjClosePrice) [days = 5|21|63|120]
Mean(baseData.adjLowPrice / baseData.adjClosePrice) [days = 5|21|63|120]
Spread(baseData.adjLowPrice / baseData.adjClosePrice) [days = 5|21|63|120]
ZScore(baseData.adjLowPrice / baseData.adjClosePrice) [days = 5|21|63|120]
Delta(baseData.adjLowPrice / baseData.adjClosePrice) [days = 5|21|63|120]
Min(baseData.adjLowPrice / baseData.adjClosePrice) [days = 5|21|63|120]
Max2(baseData.adjLowPrice / baseData.adjClosePrice) [days = 5|21|63|120]
Mean(baseData.adjLowPrice / baseData.adjClosePrice, days = 5) - Mean(baseData.adjLowPrice / baseData.adjClosePrice) [days = 21|63|120]
UpDownDays(Delta(baseData.adjLowPrice / baseData.adjClosePrice, days = 5)) - UpDownDays(Delta(baseData.adjLowPrice / baseData.adjClosePrice)) [days = 21|63|120]


// Rank(baseData.adjVWAP / baseData.adjClosePrice) [days = 5|21|63|120]
// StdDev(baseData.adjVWAP / baseData.adjClosePrice) [days = 5|21|63|120]
// Mean(baseData.adjVWAP / baseData.adjClosePrice) [days = 5|21|63|120]
// Spread(baseData.adjVWAP / baseData.adjClosePrice) [days = 5|21|63|120]
// ZScore(baseData.adjVWAP / baseData.adjClosePrice) [days = 5|21|63|120]
// Delta(baseData.adjVWAP / baseData.adjClosePrice) [days = 5|21|63|120]
// Min(baseData.adjVWAP / baseData.adjClosePrice) [days = 5|21|63|120]
// Max2(baseData.adjVWAP / baseData.adjClosePrice) [days = 5|21|63|120]
// Mean(baseData.adjVWAP / baseData.adjClosePrice, days = 5) - Mean(baseData.adjVWAP / baseData.adjClosePrice) [days = 21|63|120]
// UpDownDays(Delta(baseData.adjVWAP / baseData.adjClosePrice, days = 5)) - UpDownDays(Delta(baseData.adjVWAP / baseData.adjClosePrice)) [days = 21|63|120]


Techs_EMA(baseData.adjHighPrice / baseData.adjLowPrice) [days = 5|21|63|120]
Rank(baseData.adjHighPrice / baseData.adjLowPrice) [days = 5|21|63|120]
StdDev(baseData.adjHighPrice / baseData.adjLowPrice) [days = 5|21|63|120]
Mean(baseData.adjHighPrice / baseData.adjLowPrice) [days = 5|21|63|120]
Spread(baseData.adjHighPrice / baseData.adjLowPrice) [days = 5|21|63|120]
ZScore(baseData.adjHighPrice / baseData.adjLowPrice) [days = 5|21|63|120]
Delta(baseData.adjHighPrice / baseData.adjLowPrice) [days = 5|21|63|120]
Min(baseData.adjHighPrice / baseData.adjLowPrice) [days = 5|21|63|120]
Max2(baseData.adjHighPrice / baseData.adjLowPrice) [days = 5|21|63|120]
Mean(baseData.adjHighPrice / baseData.adjLowPrice, days = 5) - Mean(baseData.adjHighPrice / baseData.adjLowPrice) [days = 21|63|120]
UpDownDays(Delta(baseData.adjHighPrice / baseData.adjLowPrice, days = 5)) - UpDownDays(Delta(baseData.adjHighPrice / baseData.adjLowPrice)) [days = 21|63|120]



Techs_EMA(baseData.adjVolume / baseData.adv5  days = 1|5|21|63|120)
Rank(baseData.adjVolume / baseData.adv5  days = 1|5|21|63|120)
StdDev(baseData.adjVolume / baseData.adv5  days = 1|5|21|63|120)
Mean(baseData.adjVolume / baseData.adv5  days = 1|5|21|63|120)
Spread(baseData.adjVolume / baseData.adv5  days = 1|5|21|63|120)
ZScore(baseData.adjVolume / baseData.adv5  days = 1|5|21|63|120)
Delta(baseData.adjVolume / baseData.adv5  days = 1|5|21|63|120)
Min(baseData.adjVolume / baseData.adv5  days = 1|5|21|63|120)
Max2(baseData.adjVolume / baseData.adv5  days = 1|5|21|63|120)
Mean(baseData.adjVolume / baseData.adv5, days = 5) - Mean(baseData.adjVolume / baseData.adv5) [days = 21|63|120]
UpDownDays(Delta(baseData.adjVolume / baseData.adv5, days = 5)) - UpDownDays(Delta(baseData.adjVolume / baseData.adv5)) [days = 21|63|120]



Techs_EMA(baseData.adjVolume / baseData.avd20  days = 1|5|21|63|120)
Rank(baseData.adjVolume / baseData.avd20  days = 1|5|21|63|120)
StdDev(baseData.adjVolume / baseData.avd20  days = 1|5|21|63|120)
Mean(baseData.adjVolume / baseData.avd20  days = 1|5|21|63|120)
Spread(baseData.adjVolume / baseData.avd20  days = 1|5|21|63|120)
ZScore(baseData.adjVolume / baseData.avd20  days = 1|5|21|63|120)
Delta(baseData.adjVolume / baseData.avd20  days = 1|5|21|63|120)
Min(baseData.adjVolume / baseData.avd20  days = 1|5|21|63|120)
Max2(baseData.adjVolume / baseData.avd20  days = 1|5|21|63|120)
Mean(baseData.adjVolume / baseData.avd20, days = 5) - Mean(baseData.adjVolume / baseData.avd20) [days = 21|63|120]
UpDownDays(Delta(baseData.adjVolume / baseData.avd20, days = 5)) - UpDownDays(Delta(baseData.adjVolume / baseData.avd20)) [days = 21|63|120]


Techs_EMA(baseData.adv5 / baseData.avd20  days = 1|5|21|63|120)
Rank(baseData.adv5 / baseData.avd20  days = 1|5|21|63|120)
StdDev(baseData.adv5 / baseData.avd20  days = 1|5|21|63|120)
Mean(baseData.adv5 / baseData.avd20  days = 1|5|21|63|120)
Spread(baseData.adv5 / baseData.avd20  days = 1|5|21|63|120)
ZScore(baseData.adv5 / baseData.avd20  days = 1|5|21|63|120)
Delta(baseData.adv5 / baseData.avd20  days = 1|5|21|63|120)
Min(baseData.adv5 / baseData.avd20  days = 1|5|21|63|120)
Max2(baseData.adv5 / baseData.avd20  days = 1|5|21|63|120)
Mean(baseData.adv5 / baseData.avd20, days = 5) - Mean(baseData.adv5 / baseData.avd20) [days = 21|63|120]
UpDownDays(Delta(baseData.adv5 / baseData.avd20, days = 5)) - UpDownDays(Delta(baseData.adv5 / baseData.avd20))) [days = 21|63|120]


Techs_EMA(baseData.adjHighPrice * baseData.adjClosePrice) [days = 5|21|63|120]
Rank(baseData.adjHighPrice * baseData.adjClosePrice) [days = 5|21|63|120]
StdDev(baseData.adjHighPrice * baseData.adjClosePrice) [days = 5|21|63|120]
Mean(baseData.adjHighPrice * baseData.adjClosePrice) [days = 5|21|63|120]
Spread(baseData.adjHighPrice * baseData.adjClosePrice) [days = 5|21|63|120]
ZScore(baseData.adjHighPrice * baseData.adjClosePrice) [days = 5|21|63|120]
Delta(baseData.adjHighPrice * baseData.adjClosePrice) [days = 5|21|63|120]
Min(baseData.adjHighPrice * baseData.adjClosePrice) [days = 5|21|63|120]
Max2(baseData.adjHighPrice * baseData.adjClosePrice) [days = 5|21|63|120]
Mean(baseData.adjHighPrice * baseData.adjClosePrice, days = 5) - Mean(baseData.adjHighPrice * baseData.adjClosePrice) [days = 21|63|120]
UpDownDays(Delta(baseData.adjHighPrice * baseData.adjClosePrice, days = 5)) - UpDownDays(Delta(baseData.adjHighPrice * baseData.adjClosePrice)) [days = 21|63|120]


Techs_EMA(baseData.adjLowPrice * baseData.adjClosePrice) [days = 5|21|63|120]
Rank(baseData.adjLowPrice * baseData.adjClosePrice) [days = 5|21|63|120]
StdDev(baseData.adjLowPrice * baseData.adjClosePrice) [days = 5|21|63|120]
Mean(baseData.adjLowPrice * baseData.adjClosePrice) [days = 5|21|63|120]
Spread(baseData.adjLowPrice * baseData.adjClosePrice) [days = 5|21|63|120]
ZScore(baseData.adjLowPrice * baseData.adjClosePrice) [days = 5|21|63|120]
Delta(baseData.adjLowPrice * baseData.adjClosePrice) [days = 5|21|63|120]
Min(baseData.adjLowPrice * baseData.adjClosePrice) [days = 5|21|63|120]
Max2(baseData.adjLowPrice * baseData.adjClosePrice) [days = 5|21|63|120]
Mean(baseData.adjLowPrice * baseData.adjClosePrice, days = 5) - Mean(baseData.adjLowPrice * baseData.adjClosePrice) [days = 21|63|120]
UpDownDays(Delta(baseData.adjLowPrice * baseData.adjClosePrice, days = 5)) - UpDownDays(Delta(baseData.adjLowPrice * baseData.adjClosePrice)) [days = 21|63|120]



// Techs_EMA(baseData.adjVWAP * baseData.adjClosePrice) [days = 5|21|63|120]
// Rank(baseData.adjVWAP * baseData.adjClosePrice) [days = 5|21|63|120]
// StdDev(baseData.adjVWAP * baseData.adjClosePrice) [days = 5|21|63|120]
// Mean(baseData.adjVWAP * baseData.adjClosePrice) [days = 5|21|63|120]
// Spread(baseData.adjVWAP * baseData.adjClosePrice) [days = 5|21|63|120]
// ZScore(baseData.adjVWAP * baseData.adjClosePrice) [days = 5|21|63|120]
// Delta(baseData.adjVWAP * baseData.adjClosePrice) [days = 5|21|63|120]
// Min(baseData.adjVWAP * baseData.adjClosePrice) [days = 5|21|63|120]
// Max2(baseData.adjVWAP * baseData.adjClosePrice) [days = 5|21|63|120]
// Mean(baseData.adjVWAP * baseData.adjClosePrice, days = 5) - Mean(baseData.adjVWAP * baseData.adjClosePrice) [days = 21|63|120]
// UpDownDays(Delta(baseData.adjVWAP * baseData.adjClosePrice, days = 5)) - UpDownDays(Delta(baseData.adjVWAP * baseData.adjClosePrice))) [days = 21|63|120]




Techs_EMA(baseData.adjOpenPrice * baseData.adjClosePrice) [days = 5|21|63|120]
Rank(baseData.adjOpenPrice * baseData.adjClosePrice) [days = 5|21|63|120]
StdDev(baseData.adjOpenPrice * baseData.adjClosePrice) [days = 5|21|63|120]
Mean(baseData.adjOpenPrice * baseData.adjClosePrice) [days = 5|21|63|120]
Spread(baseData.adjOpenPrice * baseData.adjClosePrice) [days = 5|21|63|120]
ZScore(baseData.adjOpenPrice * baseData.adjClosePrice) [days = 5|21|63|120]
Delta(baseData.adjOpenPrice * baseData.adjClosePrice) [days = 5|21|63|120]
Min(baseData.adjOpenPrice * baseData.adjClosePrice) [days = 5|21|63|120]
Max2(baseData.adjOpenPrice * baseData.adjClosePrice) [days = 5|21|63|120]
Mean(baseData.adjOpenPrice * baseData.adjClosePrice, days = 5) - Mean(baseData.adjOpenPrice * baseData.adjClosePrice) [days = 21|63|120]
UpDownDays(Delta(baseData.adjOpenPrice * baseData.adjClosePrice, days = 5)) - UpDownDays(Delta(baseData.adjOpenPrice * baseData.adjClosePrice))) [days = 21|63|120]


Mean(baseData.adjReturns) / derivedData.r_std [days = 5|21|63]

Techs_EMA($x0) [days = 30|63|120|255]
Techs_EMA($x0, days = 63) / Techs_EMA($x0, days = 250)
Techs_EMA($x0, days = 120) / Techs_EMA($x0, days = 250)
(Techs_EMA($x0, days = 5) - Techs_EMA($x0, days = 63)) / Techs_EMA($x0, days = 63)
(Techs_EMA($x0, days = 21) - Techs_EMA($x0, days = 120)) / Techs_EMA($x0, days = 120)

Exp(Techs_SMA($x0, days = 50) - Techs_SMA($x0, days = 200))
Exp($x0 - Techs_SMA($x0, days = 200))
Exp($x0 - Techs_KAMA($x0, days = 21))
Exp($x0 - Techs_KAMA($x0, days = 120))

Techs_KAMA(baseData.adjClosePrice, days = 21)
Techs_KAMA(baseData.returns, days = 21)
Techs_KAMA(baseData.adjClosePrice, days = 120)
Techs_KAMA(baseData.returns, days = 120)
// Techs_KAMA(baseData.adjVWAP, days = 21)
Techs_KAMA(baseData.adjClosePrice, days = 21) - Techs_KAMA(returns, days = 21)
Techs_KAMA(baseData.adjClosePrice, days = 120) - Techs_KAMA(returns, days = 120)

// (baseData.adv5 / Sum(baseData.adjVolume)) * Mean(baseData.adjReturns) / (derivedData.r_std) [days = 5|21|63]

// GT(Sum($x0, days=21)/21 , $x0,(-1 * Delta($x0))) [days = 1|3|5|21|63]


