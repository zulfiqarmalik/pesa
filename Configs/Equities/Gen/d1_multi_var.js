@baseData

(($x0 - $x1) /  $x2) / (($x0 + $x1) /  $x3)
(($x0 - $x1) /  $x2) / (($x0 - $x2) /  $x3)
(($x0 - $x1) /  $x2) / (($x0 + $x2) /  $x3)

Rank((($x0 - $x1) /  $x2) / (($x0 + $x1) /  $x3)) [days = 5|21]
Rank((($x0 - $x1) /  $x2) / (($x0 - $x2) /  $x3)) [days = 5|21]



(($x0 - $x1) / $x2) / (($x0 + $x1) / $x3)
(($x0 - $x1) / $x2) / (($x0 + $x1) / $x3)

($x0 - $x1) / ($x0 + $x1) 
($x0 - $x1) / ($x0 * $x1)
($x0 + $x1) / ($x0 * $x1)

Rank(($x0 + $x1) / ($x0 * $x1)) [days=5|21]
Rank(($x0 - $x1) / ($x0 * $x1)) [days=5|21]


($x0 - $x1) / ($x2 + $x3)
($x0 - $x1) / ($x2 - $x3)
($x0 + $x1) / ($x2 + $x3)
($x0 - $x1) / ($x2 * $x3)
($x0 + $x1) / ($x2 * $x3)


Rank($x0, days = 21) / Rank($x1, days = 21)
Rank($x0, days = 5) / Rank($x1, days = 5)
Rank($x0, days = 120) / Rank($x1, days = 120)
Rank($x0, days = 2501) / Rank($x1, days = 250)

Rank($x0 * $x1,days = 5) / Rank($x2 * $x3, days = 5)
Rank($x0 * $x1,days = 21) / Rank($x2 * $x3, days = 21)
Rank($x0 * $x1,days = 120) / Rank($x2 * $x3, days = 120)
Rank($x0 * $x1,days = 250) / Rank($x2 * $x3, days = 250)

Rank((($x0 - $x1) / $x2), days = 5) / Rank(($x0 + $x1) / $x3, days = 5)

