<?xml version="1.0" encoding="UTF-8"?>

<!-- *** BEGIN LICENSE *** -->
<!-- Copyright (C) QVTechnologies Inc. - All Rights Reserved -->
<!-- Unauthorized copying, usage, dissemination of information or reproduction of this material, -->
<!-- via any medium, including but not limited to, compiled form is strictly prohibited without prior permission. -->
<!-- Proprietary and confidential. -->
<!-- *** END LICENSE *** -->

<!-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, -->
<!-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND -->
<!-- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, -->
<!-- DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF -->
<!-- OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. -->

<Config>
    <Modules>
        
        <Data 
            id                          = "Quandl_SharadarFundamental" 
            datasetName                 = "SF1"
            optimiseForUniverse         = "true" 
            serverUrl                   = "https://www.quandl.com/api/v3/datatables/SHARADAR/SF1.json"
            alwaysOverwrite             = "false"
            useLastDayData              = "true"
            useLastDayDataFrameIfEmpty  = "true"
            exactTimestamp              = "false" 
            maxPrecache                 = "8"
            jsonParser                  = "Quandl_JsonParser"
            useHttpGet                  = "true"
            datasets                    = "datetime.datekey.daily, datetime.reportperiod.quarterly, datetime.lastupdated.quarterly, float.accoci.quarterly, float.assets.quarterly, float.assetsavg.quarterly, float.assetsc.quarterly, float.assetsnc.quarterly, float.assetturnover.quarterly, float.bvps.quarterly, float.capex.quarterly, float.cashneq.quarterly, float.cashnequsd.quarterly, float.cor.quarterly, float.consolinc.quarterly, float.currentratio.quarterly, float.de.quarterly, float.debt.quarterly, float.debtc.quarterly, float.debtnc.quarterly, float.debtusd.quarterly, float.deferredrev.quarterly, float.depamor.quarterly, float.deposits.quarterly, float.divyield.quarterly, float.dps.quarterly, float.ebit.quarterly, float.ebitda.quarterly, float.ebitdamargin.quarterly, float.ebitdausd.quarterly, float.ebitusd.quarterly, float.ebt.quarterly, float.eps.quarterly, float.epsdil.quarterly, float.epsusd.quarterly, float.equity.quarterly, float.equityavg.quarterly, float.equityusd.quarterly, float.ev.quarterly, float.evebit.quarterly, float.evebitda.quarterly, float.fcf.quarterly, float.fcfps.quarterly, float.fxusd.quarterly, float.gp.quarterly, float.grossmargin.quarterly, float.intangibles.quarterly, float.intexp.quarterly, float.invcap.quarterly, float.invcapavg.quarterly, float.inventory.quarterly, float.investments.quarterly, float.investmentsc.quarterly, float.investmentsnc.quarterly, float.liabilities.quarterly, float.liabilitiesc.quarterly, float.liabilitiesnc.quarterly, float.marketcap.quarterly, float.ncf.quarterly, float.ncfbus.quarterly, float.ncfcommon.quarterly, float.ncfdebt.quarterly, float.ncfdiv.quarterly, float.ncff.quarterly, float.ncfi.quarterly, float.ncfinv.quarterly, float.ncfo.quarterly, float.ncfx.quarterly, float.netinc.quarterly, float.netinccmn.quarterly, float.netinccmnusd.quarterly, float.netincdis.quarterly, float.netincnci.quarterly, float.netmargin.quarterly, float.opex.quarterly, float.opinc.quarterly, float.payables.quarterly, float.payoutratio.quarterly, float.pb.quarterly, float.pe.quarterly, float.pe1.quarterly, float.ppnenet.quarterly, float.prefdivis.quarterly, float.price.quarterly, float.ps.quarterly, float.ps1.quarterly, float.receivables.quarterly, float.retearn.quarterly, float.revenue.quarterly, float.revenueusd.quarterly, float.rnd.quarterly, float.roa.quarterly, float.roe.quarterly, float.roic.quarterly, float.ros.quarterly, float.sbcomp.quarterly, float.sgna.quarterly, float.sharefactor.quarterly, float.sharesbas.quarterly, float.shareswa.quarterly, float.shareswadil.quarterly, float.sps.quarterly, float.tangibles.quarterly, float.taxassets.quarterly, float.taxexp.quarterly, float.taxliabilities.quarterly, float.tbvps.quarterly, float.workingcapital.quarterly"
        >
            <Args 
                calendardate            = "{$lastFiscalQuarterEndDate}" 
                dimension               = "ARQ"
                api_key                 = "{$QUANDL_API_KEY}" 
            />
        </Data>

        <Data 
            id                          = "Quandl_SharadarNextED" 
            datasetName                 = "wsh"
            optimiseForUniverse         = "false" 
            alwaysOverwrite             = "false"
            useLastDayData              = "true"
            useLastDayDataFrameIfEmpty  = "true"
        />

<!-- 
        <Data 
            id                          = "SpGeneric" 
            datasetName                 = "quandlWikiEOD"
            optimiseForUniverse         = "false" 
            serverUrl                   = "https://www.quandl.com/api/v3/datatables/WIKI/PRICES.json"
            useLastDayData              = "false"
            jsonParser                  = "Quandl_JsonParser"
            useHttpGet                  = "true"
            useLastDayDataFrameIfEmpty  = "true"
            datasets                    = "float.open, float.close, float.low, float.high, float.volume, float.adj_close, float.adj_volume"
            mappings                    = "open=openPrice, close=closePrice, low=lowPrice, high=highPrice, adj_close=prelim_adjClosePrice, adj_volume=prelim_adjVolume"
        >
            <Args 
                date                    = "{$diDate}" 
                api_key                 = "{$QUANDL_API_KEY}" 
            />
        </Data>

        <Data 
            id                          = "Quandl_AdjFactor" 
            datasetName                 = "quandlWikiEOD"
            optimiseForUniverse         = "false" 
        />

        <Data 
            id                          = "AdjustedPrices" 
            datasetName                 = "quandlWikiEOD"
            adjFactor                   = "quandlWikiEOD.f_adj"
            alwaysOverwrite             = "true"
            optimiseForUniverse         = "true"
            copyFromPrevVersion         = "false"
            datasets                    = "float.adjOpenPrice.daily, float.adjClosePrice.daily, float.adjLowPrice.daily, float.adjHighPrice.daily"
            mappings                    = "quandlWikiEOD.openPrice = adjOpenPrice, quandlWikiEOD.closePrice = adjClosePrice, quandlWikiEOD.lowPrice = adjLowPrice, quandlWikiEOD.highPrice = adjHighPrice"
        />

        <Data 
            id                          = "AdjustedPrices" 
            datasetName                 = "quandlWikiEOD"
            adjFactor                   = "quandlWikiEOD.f_adjVolume"
            alwaysOverwrite             = "true"
            optimiseForUniverse         = "true"
            datasets                    = "float.adjVolume.daily"
            mappings                    = "quandlWikiEOD.volume = adjVolume"
            copyFromPrevVersion         = "false"
        />

        <Data
            id                          = "AlphaData" 
            a_id                        = "ExprVM" 
            datasetName                 = "quandlWikiEOD"
            useLastDayData              = "false"
            dataId                      = "adjMCap"
            alwaysOverwrite             = "true"
            optimiseForUniverse         = "true"
            copyFromPrevVersion         = "false"
            expr                        = "quandlWikiEOD.adjClosePrice * quandlWikiEOD.adjVolume"
        />
 -->

<!--        
        <Data 
            id                  = "AdjustedPrices" 
            datasetName         = "quandlWikiEOD"
            alwaysOverwrite     = "false"
            optimiseForUniverse = "true"
            datasets            = "float.adjMCap.daily"
            mappings            = "quandlWikiEOD.mcap = adjMCap"
            copyFromPrevVersion = "false"
        />

        <Data 
            id                  = "AdjustedPrices" 
            datasetName         = "quandlWikiEOD"
            adjFactor           = "quandlWikiEOD.f_adj"
            alwaysOverwrite     = "true"
            optimiseForUniverse = "true"
            copyFromPrevVersion = "false"
            datasets            = "float.adjOpenPrice.daily, float.adjClosePrice.daily, float.adjLowPrice.daily, float.adjHighPrice.daily, float.adjVWAP.daily"
            mappings            = "quandlWikiEOD.openPrice = adjOpenPrice, quandlWikiEOD.closePrice = adjClosePrice, quandlWikiEOD.lowPrice = adjLowPrice, quandlWikiEOD.highPrice = adjHighPrice, quandlWikiEOD.vwap = adjVWAP"
        />

        <Data 
            id                  = "AdjustedPrices" 
            datasetName         = "quandlWikiEOD"
            adjFactor           = "quandlWikiEOD.f_adjVolume"
            alwaysOverwrite     = "true"
            optimiseForUniverse = "true"
            datasets            = "float.adjVolume.daily"
            mappings            = "quandlWikiEOD.volume = adjVolume"
            copyFromPrevVersion = "false"
        />
 -->
    </Modules>
</Config>
