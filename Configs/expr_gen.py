### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

days = [5, 21, 63]
exponents = [2, 3]
operators = ["+", "*"] # Subtract (-) and division (/) behave the same
filename = "./gen_expressions.js"

functions = [
    { "name": "Mean",     "args": [{"key": "days", "values": days}] },
    { "name": "Momentum", "args": [{"key": "days", "values": days}] },
    { "name": "Rank",     "args": [{"key": "days", "values": days}] },
    { "name": "Min",      "args": [{"key": "days", "values": days}] },
    { "name": "Max2",     "args": [{"key": "days", "values": days}] },
    { "name": "DeltaSum", "args": [{"key": "days", "values": days}] },
    { "name": "Rank",     "args": [{"key": "days", "values": days}] },
    { "name": "Pow",      "args": [{"key": "exponent", "values": exponents}] },
    { "name": "Mean",     "args": [{"key": "days", "values": days}] },
    { "name": "StdDev",   "args": [{"key": "days", "values": days}] },
    { "name": "Sum",      "args": [{"key": "days", "values": days}] },
    { "name": "Skew",     "args": [{"key": "days", "values": days}] },
    { "name": "Kurtosis", "args": [{"key": "days", "values": days}] },
    { "name": "ZScore",   "args": [{"key": "days", "values": days}] },
    { "name": "Momentum", "args": [{"key": "days", "values": days}] },
    { "name": "DeltaSum", "args": [{"key": "days", "values": days}] },
    { "name": "Rank",     "args": [{"key": "days", "values": days}] },
    { "name": "Log",      "args": [{"key": "days", "values": days}] },
    { "name": "Spread",   "args": [{"key": "days", "values": days}] },
]

expandedFunctions = []

def combineArgs(args, index, string, allArgs):
    for i in range(index, len(args)):
        arg = args[i]
        key = arg["key"]
        values = arg["values"]

        for j in range(len(values)):
            string += ", " + key + " = " + str(values[j])
            combineArgs(args, index + 1, string, allArgs)

            if index == 0:
                allArgs.append(string)
                string = ""

def expandFunctions(functions):
    expandedFunctions = []

    for fi in range(len(functions)):
        function = functions[fi]

        cargs = []
        combineArgs(function["args"], 0, "", cargs)

        for carg in cargs:
            functionStr = function["name"] + "($x0" + carg + ")"
            expandedFunctions.append(functionStr)

    return expandedFunctions

def combineFunctions(lhsFunctions, lhsKey, rhsFunctions, rhsKey, ops, lhsRepKey = "$x0", rhsRepKey = "$x0"):
    cfunctions = []
    for op in ops:
        for lhsFunction in lhsFunctions:
            if lhsKey:
                lhsFunc = lhsFunction.replace(lhsRepKey, lhsKey)
            else:
                lhsFunc = lhsFunction

            for rhsFunction in rhsFunctions:
                if rhsKey:
                    rhsFunc = rhsFunction.replace(rhsRepKey, rhsKey)
                else:
                    rhsFunc = rhsFunction

                cfunc = "(" + lhsFunc + ") " + op + " (" + rhsFunc + ")"
                cfunctions.append(cfunc)

    return cfunctions

def writeFunctions(file, funcs):
    for f in funcs:
        file.write(f + "\n")


baseFunctions = expandFunctions(functions)
print("Num base functions: %d" % (len(baseFunctions)))

l1Functions = combineFunctions(baseFunctions, "$x0", baseFunctions, "$x1", operators)
print("Num L1 functions: %d" % (len(l1Functions)))

# l2Functions = combineFunctions(baseFunctions, "($x0 * $x1)", l1Functions, "", operators)
# print("Num L2 functions: %d" % (len(l2Functions)))

# l3Functions = combineFunctions(l1Functions, "", l2Functions, "", operators)
# print("Num L3 functions: %d" % (len(l3Functions)))

file = open("./gen_expressions.js", "w")
writeFunctions(file, l1Functions)
# writeFunctions(file, l2Functions)
# writeFunctions(l3Functions)

file.close()
