### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 23 15:57:56 2017

@author: zfu
"""

def expressionGenerator():
    
    functions = getFunctions()
    operations = getOperations()
    expression_list = []
    
    n = 2
    
    # Variable 1
    i = 0 
    expression_list1 = ['$x0']
    last_list1 = ['$x0']
    new_list1 = []
  
    while i < n:
        for expression in last_list1:    
            for function in list(functions.keys()):
                if expression == '$x0':
                    new_expression = function + '(' + expression + ')'
                else:
                    if expression[:3] in functions[function] or expression[:4] in functions[function] or \
                       expression[:6] in functions[function] or expression[:8] in functions[function]: 
                        if expression[:3] == 'Pow':
                            new_expression = function + '(' + expression[:-1] +', exponent = 2'+ '))'
                        elif expression[:3] == 'Log' or expression[:4] == 'Sign':
                            new_expression = function + '(' + expression + ')'
                        else:
                            new_expression = function + '(' + expression[:-1] +', days = 21'+ '))'
                new_list1.append(new_expression)
                
        expression_list1 = expression_list1 + new_list1
        last_list1 = new_list1.copy()
        new_list1 = []        
        i = i + 1
    
    # Variable 2    
    i = 0 
    expression_list2 = ['$x1']
    last_list2 = ['$x1']
    new_list2 = []
    
    while i < n:
        for expression in last_list2:    
            for function in list(functions.keys()):
                if expression == '$x1': 
                    new_expression = function + '(' + expression + ')'
                else:
                    if expression[:3] in functions[function] or expression[:4] in functions[function] or \
                       expression[:6] in functions[function] or expression[:8] in functions[function]:
                        if expression[:3] == 'Pow':
                            new_expression = function + '(' + expression[:-1] +', exponent = 2'+ '))'
                        elif expression[:3] == 'Log' or expression[:4] == 'Sign':
                            new_expression = function + '(' + expression + ')'
                        else:
                            # new_expression = function + '(' + expression[:-1] +', days = 21'+ '))'
                            new_expression = function + '(' + expression[:-1] + '))'
                new_list2.append(new_expression)
                
                
        expression_list2 = expression_list2 + new_list2
        #print(new_list2)
        last_list2 = new_list2.copy()
        new_list2 = []        
        i = i + 1
        
    # Variable 3
    i = 0 
    expression_list3 = ['$x0/$x1']
    last_list3 = ['$x0/$x1']
    new_list3 = []

    while i < n:
        for expression in last_list3:    
            for function in list(functions.keys()):
                if expression == '$x0/$x1': 
                    new_expression = function + '(' + expression + ')'
                else:
                    if expression[:3] in functions[function] or expression[:4] in functions[function] or \
                       expression[:6] in functions[function] or expression[:8] in functions[function]:
                        if expression[:3] == 'Pow':
                            new_expression = function + '(' + expression[:-1] +', exponent = 2'+ '))'
                        elif expression[:3] == 'Log' or expression[:4] == 'Sign':
                            new_expression = function + '(' + expression + ')'
                        else:
                            new_expression = function + '(' + expression[:-1] + '))'
                new_list3.append(new_expression)
                
                
        expression_list3 = expression_list3 + new_list3
        #print(new_list2)
        last_list3 = new_list3.copy()
        new_list3 = []        
        i = i + 1
    
    # add operations and combine together
    # Scenario 1:
    # Both are from x0    
    for expression1 in expression_list1:
        for expression2 in expression_list1:
            if expression2 != expression1:                 
                for operation in list(operations.keys()):
                    if expression1 == '$x0' or expression1[:3] == 'Log' or expression1[:4] == 'Sign':
                        new_expression1 = expression1
                    elif expression1[:3] == 'Pow':
                        new_expression1 = expression1[:-1] +', exponent = 2)'
                    else:    
                        new_expression1 = expression1[:-1] +', days = 21)'
                   
                    if expression2[:3] == 'Pow':
                        new_expression2 = expression2[:-1] + ', exponent = 2)'
                    else:
                        new_expression2 = expression2
                        
                    if operation == '+' or operation == '-':
                        new_expression = '(' + new_expression1 + ' ' + operation + ' ' + new_expression2 + ')'
                    else:
                        new_expression = new_expression1 + ' ' + operation + ' ' + new_expression2
                        
                    if new_expression2 == '$x0' or new_expression2[:3] == 'Log' or new_expression2[:4] == 'Sign' or new_expression2[:3] == 'Pow':
                        new_expression = new_expression
                    else:
                        new_expression = new_expression + ' ' + '[days = 5|21|63]'
                    expression_list.append(new_expression)
            else:
                if expression1 == '$x0' or expression1[:3] == 'Log' or expression1[:4] == 'Sign':
                    new_expression = expression1
                elif expression1[:3] == 'Pow':
                    new_expression = expression1[:-1] +', exponent = 2)'
                else:
                    new_expression = expression1 + ' ' + '[days = 5|21|63]'
                if new_expression not in expression_list:
                    expression_list.append(new_expression)
                
    # Scenario 2:
    # Both are from x0/x1
    for expression1 in expression_list3:
        for expression2 in expression_list3:
            if expression2 != expression1:                 
                for operation in list(operations.keys()):
                    if expression1 == '$x0/$x1' or expression1[:3] == 'Log' or expression1[:4] == 'Sign':
                        new_expression1 = expression1
                    elif expression1[:3] == 'Pow':
                        new_expression1 = expression1[:-1] +', exponent = 2)'
                    else:    
                        new_expression1 = expression1[:-1] +', days = 21)'
                   
                    if expression2[:3] == 'Pow':
                        new_expression2 = expression2[:-1] + ', exponent = 2)'
                    else:
                        new_expression2 = expression2
                        
                    if operation == '+' or operation == '-':
                        new_expression = '(' + new_expression1 + ' ' + operation + ' ' + new_expression2 + ')'
                    else:
                        if new_expression2 == '$x0/$x1' and operation == '/':
                            new_expression = new_expression1 + ' ' + operation + ' (' + new_expression2 + ')'
                        else:
                            new_expression = new_expression1 + ' ' + operation + ' ' + new_expression2
                        
                    if new_expression2 == '$x0/$x1' or new_expression2[:3] == 'Log' or new_expression2[:4] == 'Sign' or new_expression2[:3] == 'Pow':
                        new_expression = new_expression
                    else:
                        new_expression = new_expression + ' ' + '[days = 5|21|63]'
                    expression_list.append(new_expression)
            else:
                if expression1 == '$x0/$x1' or expression1[:3] == 'Log' or expression1[:4] == 'Sign':
                    new_expression = expression1
                elif expression1[:3] == 'Pow':
                    new_expression = expression1[:-1] +', exponent = 2)'
                else:
                    new_expression = expression1 + ' ' + '[days = 5|21|63]'
                if new_expression not in expression_list:
                    expression_list.append(new_expression)
                    
    # Scenario 3:
    # one is from x0, the other from x1
    for expression1 in expression_list1:
        for expression2 in expression_list2:
            for operation in list(operations.keys()):
                if expression1 == '$x0' or expression1[:3] == 'Log' or expression1[:4] == 'Sign':
                    new_expression1 = expression1
                elif expression1[:3] == 'Pow':
                    new_expression1 = expression1[:-1] +', exponent = 2)'
                else:    
                    new_expression1 = expression1[:-1] +', days = 21)'
                   
                if expression2[:3] == 'Pow':
                    new_expression2 = expression2[:-1] + ', exponent = 2)'
                else:
                    new_expression2 = expression2
                        
                if operation == '+' or operation == '-':
                    new_expression = '(' + new_expression1 + ' ' + operation + ' ' + new_expression2 + ')'
                else:
                    new_expression = new_expression1 + ' ' + operation + ' ' + new_expression2
                        
                if new_expression2 == '$x1' or new_expression2[:3] == 'Log' or new_expression2[:4] == 'Sign' or new_expression2[:3] == 'Pow':
                    new_expression = new_expression
                else:
                    new_expression = new_expression + ' ' + '[days = 5|21|63]'
                expression_list.append(new_expression)
                    
    # Scenario 4:
    # one is from x0, the other from x0/x1
    for expression1 in expression_list1:
        for expression2 in expression_list3:
            for operation in list(operations.keys()):
                if expression1 == '$x0' or expression1[:3] == 'Log' or expression1[:4] == 'Sign':
                    new_expression1 = expression1
                elif expression1[:3] == 'Pow':
                    new_expression1 = expression1[:-1] +', exponent = 2)'
                else:    
                    new_expression1 = expression1[:-1] +', days = 21)'
                   
                if expression2[:3] == 'Pow':
                    new_expression2 = expression2[:-1] + ', exponent = 2)'
                else:
                    new_expression2 = expression2
                    
                if operation == '+' or operation == '-':
                    new_expression = '(' + new_expression1 + ' ' + operation + ' ' + new_expression2 + ')'
                else:
                    if new_expression2 == '$x0/$x1' and operation == '/':
                        new_expression = new_expression1 + ' ' + operation + ' (' + new_expression2 + ')'
                    else:
                        new_expression = new_expression1 + ' ' + operation + ' ' + new_expression2
                
                        
                if new_expression2 == '$x0/$x1' or new_expression2[:3] == 'Log' or new_expression2[:4] == 'Sign' or new_expression2[:3] == 'Pow':
                    new_expression = new_expression
                else:
                    new_expression = new_expression + ' ' + '[days = 5|21|63]'
                expression_list.append(new_expression)
#    

        
    
    #print(expression_list3)
    file = open("./gen_expressions.js", "w")
    count = len(expression_list)
    file.write("@baseDataD0 | ravenPack | baseData | derivedData | shortTrends | qsg | ford | ER | axioma | ravenPack | thinknum\n")

    alreadyUsedExprs = {}

    for i in range(0, count):
        expr = expression_list[i]
        if "$x1" in expr and "$x0" in expr and expr not in alreadyUsedExprs:
            alreadyUsedExprs[expr] = 1
            file.write("%s\n" % (expr))

    file.close()
    
    
    
def getFunctions():

    functionDict = {'Mean':['Momentum','Rank'],'Max2':['Mean','DeltaSum','Momentum','Rank','StdDev'], 
                    'Min':['Mean','DeltaSum','Momentum','Rank'], 'DeltaSum':['ZScore'], 'Sum':['Momentum','Rank'],
                   'Spread':['DeltaSum','Momentum','Rank'], 'Momentum': ['DeltaSum','Spread','Rank','ZScore'], 
                    'Rank':['DeltaSum','Momentum','Spread','ZScore'], 'ZScore': ['DeltaSum','Momentum','Rank'],
                    'StdDev':['DeltaSum','Momentum','Rank'], 'Pow':['Mean','Sum'],'Log':['Mean','Sum'],'Sign':['Mean','Sum']}  
              
#    functionDict =  {'Pow':['Mean'],'Mean':['Pow']}
    return functionDict
    
    
def getOperations():
    # operationDict = {'+':[], '-':[], '*':[], '/':[]}
    operationDict = {'+':[], '*':[]}

    return operationDict
    
#======================================================
if __name__ == '__main__':
    expressionGenerator()
