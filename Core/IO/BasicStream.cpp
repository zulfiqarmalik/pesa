/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// InputStream.cpp
///
/// Created on: 2 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "./BasicStream.h"
#include <sstream>
#include <fstream>
#include "Poco/Base64Encoder.h"
#include "Poco/Base64Decoder.h"
#include "Poco/DeflatingStream.h"
#include "Poco/BinaryWriter.h"
#include "Poco/BinaryReader.h"
#include "Poco/FileStream.h"

namespace pesa {
namespace io {

	////////////////////////////////////////////////////////////////////////////
	/// BasicInputStream: Basic wrapper over istream
	////////////////////////////////////////////////////////////////////////////
	BasicInputStream::BasicInputStream(IStreamPtr& stream)
		: m_stream(stream) {
	}

	BasicInputStream::~BasicInputStream() {
	}

	size_t BasicInputStream::size(const char* name) {
		size_t size;
		read(name, &size);
		return size;
	}

	InputStream& BasicInputStream::read(const char* name, size_t* value) {
		return BasicInputStream::readGeneric(value);
	}

	InputStream& BasicInputStream::read(const char* name, unsigned int* value) {
		return BasicInputStream::readGeneric(value);
	}

	InputStream& BasicInputStream::read(const char* name, bool* value) {
		return BasicInputStream::readGeneric(value);
	}

	InputStream& BasicInputStream::read(const char* name, int* value) {
		return BasicInputStream::readGeneric(value);
	}

	InputStream& BasicInputStream::read(const char* name, int64_t* value) {
		return BasicInputStream::readGeneric(value);
	}

	InputStream& BasicInputStream::read(const char* name, float* value) {
		return BasicInputStream::readGeneric(value);
	}

	InputStream& BasicInputStream::read(const char* name, double* value) {
		return BasicInputStream::readGeneric(value);
	}

	InputStream& BasicInputStream::read(const char* name, std::string* value) {
		size_t length;
		readGeneric(&length);
		if (length > 0) {
			char* buffer = new char[length + 1];
			read(name, buffer, length);
			buffer[length] = '\0';
			*value = buffer;
			delete[] buffer;
		}
		return *this;
	}

	bool BasicInputStream::doesSupportBufferedRead() const {
		return true;
	}

	InputStream& BasicInputStream::read(const char* name, char* buffer, size_t length) {
		m_stream->read(buffer, length);
		return *this;
	}

	////////////////////////////////////////////////////////////////////////////
	/// Some helper static methods
	////////////////////////////////////////////////////////////////////////////
	BasicInputStreamPtr BasicInputStream::createBase64Decoder(const std::string& str) {
		std::istringstream ss(str);
		std::shared_ptr<std::istream> decoder = std::make_shared<Poco::Base64Decoder>(ss);
		return std::make_shared<BasicInputStream>(decoder);
	}

	BasicInputStreamPtr BasicInputStream::createGZip(const std::string& str) {
		std::istringstream ss(str);
		std::shared_ptr<std::istream> deflater = std::make_shared<Poco::DeflatingInputStream>(ss);
		return std::make_shared<BasicInputStream>(deflater);
	}

	BasicInputStreamPtr BasicInputStream::createBinaryReader(const std::string& filename) {
		std::shared_ptr<std::istream> inputStream = std::make_shared<Poco::FileInputStream>(filename, std::ios::in | std::ios::binary);
		return std::make_shared<BasicInputStream>(inputStream);
	}

	BasicInputStreamPtr BasicInputStream::createFileReader(const std::string& filename) {
		std::shared_ptr<std::istream> inputStream = std::make_shared<Poco::FileInputStream>(filename);
		return std::make_shared<BasicInputStream>(inputStream);
	}

	////////////////////////////////////////////////////////////////////////////
	/// BasicOutputStream: Basic wrapper over ostream
	////////////////////////////////////////////////////////////////////////////
	BasicOutputStream::BasicOutputStream(OStreamPtr& stream) 
		: m_stream(stream) {
	}

	BasicOutputStream::~BasicOutputStream() {
	}

	OutputStream& BasicOutputStream::write(const char* name, const size_t& value) {
		return BasicOutputStream::writeGeneric(value);
	}
	
	OutputStream& BasicOutputStream::write(const char* name, const unsigned int& value) {
		return BasicOutputStream::writeGeneric(value);
	}
	
	OutputStream& BasicOutputStream::write(const char* name, const bool& value) {
		return BasicOutputStream::writeGeneric(value);
	}
	
	OutputStream& BasicOutputStream::write(const char* name, const int& value) {
		return BasicOutputStream::writeGeneric(value);
	}

	OutputStream& BasicOutputStream::write(const char* name, const int64_t& value) {
		return BasicOutputStream::writeGeneric(value);
	}

	OutputStream& BasicOutputStream::write(const char* name, const float& value) {
		return BasicOutputStream::writeGeneric(value);
	}

	OutputStream& BasicOutputStream::write(const char* name, const double& value) {
		return BasicOutputStream::writeGeneric(value);
	}

	OutputStream& BasicOutputStream::write(const char* name, const std::string& value) {
		size_t length = value.length();
		writeGeneric(length);
		return write(name, value.c_str(), length);
	}

	bool BasicOutputStream::doesSupportBufferedWrite() const {
		return true;
	}

	OutputStream& BasicOutputStream::write(const char* name, const char* buffer, size_t length) {
		m_stream->write(buffer, length);
		return *this;
	}

	////////////////////////////////////////////////////////////////////////////
	/// Some helper static methods
	////////////////////////////////////////////////////////////////////////////
	BasicOutputStreamPtr BasicOutputStream::createBase64Encoder(const std::string& str) {
		std::ostringstream ss;
		std::shared_ptr<std::ostream> encoder = std::make_shared<Poco::Base64Encoder>(ss);
		return std::make_shared<BasicOutputStream>(encoder);
	}

	BasicOutputStreamPtr BasicOutputStream::createGZip(const std::string& str) {
		std::ostringstream ss;
		std::shared_ptr<std::ostream> deflater = std::make_shared<Poco::DeflatingOutputStream>(ss);
		return std::make_shared<BasicOutputStream>(deflater);
	}

	BasicOutputStreamPtr BasicOutputStream::createBinaryWriter(const std::string& filename, std::ios::openmode mode /* = std::ios::out | std::ios::trunc */) {
		std::shared_ptr<std::ostream> outputStream = std::make_shared<Poco::FileOutputStream>(filename, mode | std::ios::binary);
		return std::make_shared<BasicOutputStream>(outputStream);
	}

	BasicOutputStreamPtr BasicOutputStream::createFileWriter(const std::string& filename, std::ios::openmode mode /* = std::ios::out | std::ios::trunc */) {
		std::shared_ptr<std::ostream> outputStream = std::make_shared<Poco::FileOutputStream>(filename, mode);
		return std::make_shared<BasicOutputStream>(outputStream);
	}

} /// namespace io 
} /// namespace pesa 
