/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// InputStream.h
///
/// Created on: 2 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Core/CoreDefs.h"
#include "./Stream.h"
#include <memory>

#ifdef __WINDOWS__
    #pragma warning(disable : 4251) /// class needs to have a dll-interface 
#endif 

namespace pesa {
namespace io {

	class BasicInputStream;
	class BasicOutputStream;

	typedef std::shared_ptr<std::istream>		IStreamPtr;
	typedef std::shared_ptr<std::ostream>		OStreamPtr;
	typedef std::shared_ptr<BasicInputStream>	BasicInputStreamPtr;
	typedef std::shared_ptr<BasicOutputStream>	BasicOutputStreamPtr;

	////////////////////////////////////////////////////////////////////////////
	/// BasicInputStream: Basic wrapper over istream
	////////////////////////////////////////////////////////////////////////////
	class Core_API BasicInputStream : public InputStream {
	protected:
		IStreamPtr 				m_stream;

	public:
		BasicInputStream() {}
								BasicInputStream(IStreamPtr& stream);
		virtual 				~BasicInputStream();

		template <typename t_type>
		BasicInputStream&		readGeneric(t_type* value) {
			//(*m_stream) >> *value;
			//return *this;
			read(nullptr, (char*)value, sizeof(t_type));
			return *this;
		}

		inline IStreamPtr& 		stream() 		{ return m_stream; }
		inline const IStreamPtr& stream() const { return m_stream; }

		virtual InputStream& 	read(const char* name, size_t* value);
		virtual InputStream& 	read(const char* name, unsigned int* value);
		virtual InputStream& 	read(const char* name, bool* value);
		virtual InputStream& 	read(const char* name, int* value);
		virtual InputStream& 	read(const char* name, int64_t* value);
		virtual InputStream& 	read(const char* name, float* value);
		virtual InputStream& 	read(const char* name, double* value);
		virtual InputStream& 	read(const char* name, std::string* value);
		virtual size_t			size(const char* name);

		virtual bool			doesSupportBufferedRead() const;
		virtual InputStream&	read(const char* name, char* buffer, size_t length);

		////////////////////////////////////////////////////////////////////////////
		/// Some helper static methods
		////////////////////////////////////////////////////////////////////////////
		static BasicInputStreamPtr	createBase64Decoder(const std::string& str);
		static BasicInputStreamPtr	createGZip(const std::string& str);
		static BasicInputStreamPtr	createBinaryReader(const std::string& filename);
		static BasicInputStreamPtr	createFileReader(const std::string& filename);
	};

	////////////////////////////////////////////////////////////////////////////
	/// BasicOutputStream: Basic wrapper over ostream
	////////////////////////////////////////////////////////////////////////////
	class Core_API BasicOutputStream : public OutputStream {
	protected:
		OStreamPtr 				m_stream;

	public:
								BasicOutputStream() {}
								BasicOutputStream(OStreamPtr& stream);
		virtual 				~BasicOutputStream();

		template <typename t_type>
		BasicOutputStream& 		writeGeneric(const t_type& value) {
			//(*m_stream) << value;
			//return *this;
			write(nullptr, (char*)&value, sizeof(t_type));
			return *this;
		}

		inline OStreamPtr& 		stream() 		{ return m_stream; }
		inline const OStreamPtr& stream() const { return m_stream; }

		virtual OutputStream& 	write(const char* name, const size_t& value);
		virtual OutputStream& 	write(const char* name, const unsigned int& value);
		virtual OutputStream& 	write(const char* name, const bool& value);
		virtual OutputStream& 	write(const char* name, const int& value);
		virtual OutputStream& 	write(const char* name, const int64_t& value);
		virtual OutputStream& 	write(const char* name, const float& value);
		virtual OutputStream& 	write(const char* name, const double& value);
		virtual OutputStream& 	write(const char* name, const std::string& value);

		virtual bool			doesSupportBufferedWrite() const;
		virtual OutputStream&	write(const char* name, const char* buffer, size_t length);

		////////////////////////////////////////////////////////////////////////////
		/// Some helper static methods
		////////////////////////////////////////////////////////////////////////////
		static BasicOutputStreamPtr	createBase64Encoder(const std::string& str);
		static BasicOutputStreamPtr	createGZip(const std::string& str);
		static BasicOutputStreamPtr	createBinaryWriter(const std::string& filename, std::ios::openmode mode = std::ios::out | std::ios::trunc);
		static BasicOutputStreamPtr	createFileWriter(const std::string& filename, std::ios::openmode mode = std::ios::out | std::ios::trunc);
	};
} /// namespace io 
} /// namespace pesa 

