/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Stream.cpp
///
/// Created on: 2 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Stream.h"

namespace pesa {

namespace io {
	Stream::Stream() {
	}

	Stream::~Stream() {
	}

	////////////////////////////////////////////////////////////////////////////
	/// InputStream: Input streams
	////////////////////////////////////////////////////////////////////////////
	InputStream::InputStream() {
	}

	InputStream::~InputStream() {
	}

	InputStream& InputStream::read(const char* name, Streamable* value) {
		return value->read(*this);
	}

	size_t InputStream::size(const char* name) {
		return 0;
	}

	void InputStream::flush() {
	}

	InputStream& InputStream::read(const char* name, std::vector<int>* values) {
		return readVecGeneric(name, values);
	}

	InputStream& InputStream::read(const char* name, std::vector<float>* values) {
		return readVecGeneric(name, values);
	}

	InputStream& InputStream::read(const char* name, std::vector<double>* values) {
		return readVecGeneric(name, values);
	}

	InputStream& InputStream::read(const char* name, std::vector<int64_t>* values) {
		return readVecGeneric(name, values);
	}

	InputStream& InputStream::read(const char* name, std::vector<std::string>* values) {
		size_t vecSize;
		read((std::string(name) + "_size").c_str(), &vecSize);
		values->resize(vecSize);

		size_t ii = 0;
		try {
			for (auto& value : *values) {
				ii++;
				read(name, &value);
			}
		}
		catch (...) {
		}

		return *this;
	}

	InputStream& InputStream::read(const char* name, std::vector<Streamable*>* values) {
		for (size_t i = 0; i < values->size(); i++)
			read(name, (values->at(i)));
		return *this;
	}

	InputStream& InputStream::read(const char* name, Poco::Timestamp* value) {
		int64_t microseconds; /// = value->epochMicroseconds();
		read(name, &microseconds);
		*value = Poco::Timestamp(microseconds);
		return *this;
	}

	bool InputStream::doesSupportBufferedRead() const {
		return false;
	}

	bool InputStream::doesSupportNativeHashtables() const {
		return false;
	}

	bool InputStream::isDB() const {
		return false;
	}

	InputStream& InputStream::read(const char* name, char* buffer, size_t length) {
		return *this;
	}

	InputStream& InputStream::read(const char* name, StrStr_UnMap* map) {
		return readMapGeneric<StrStr_UnMap, std::string>(name, map);
	}

	InputStream& InputStream::read(const char* name, StrStr_Map* map) {
		return readMapGeneric<StrStr_Map, std::string>(name, map);
	}

	InputStream& InputStream::read(const char* name, StrInt_UnMap* map) {
		return readMapGeneric<StrInt_UnMap, int>(name, map);
	}

	InputStream& InputStream::read(const char* name, StrInt_Map* map) {
		return readMapGeneric<StrInt_Map, int>(name, map);
	}

	InputStream& InputStream::read(const char* name, StrFloat_UnMap* map) {
		return readMapGeneric<StrFloat_UnMap, float>(name, map);
	}

	InputStream& InputStream::read(const char* name, StrFloat_Map* map) {
		return readMapGeneric<StrFloat_Map, float>(name, map);
	}

	////////////////////////////////////////////////////////////////////////////
	/// OutputStream: Output streams
	////////////////////////////////////////////////////////////////////////////
	OutputStream::OutputStream() {
	}

	OutputStream::~OutputStream() {
	}

	OutputStream& OutputStream::write(const char* name, Streamable& value) {
		return value.write(*this);
	}

	OutputStream& OutputStream::write(const char* name, const std::vector<int>& values) {
		return writeVecGeneric(name, values);
	}

	OutputStream& OutputStream::write(const char* name, const std::vector<float>& values) {
		return writeVecGeneric(name, values);
	}

	OutputStream& OutputStream::write(const char* name, const std::vector<double>& values) {
		return writeVecGeneric(name, values);
	}

	OutputStream& OutputStream::write(const char* name, const std::vector<int64_t>& values) {
		return writeVecGeneric(name, values);
	}

	OutputStream& OutputStream::write(const char* name, const std::vector<std::string>& values) {
		write((std::string(name) + "_size").c_str(), values.size());

		for (const auto& value : values)
			write(name, value);

		return *this;
	}

	OutputStream& OutputStream::write(const char* name, const std::vector<Streamable*>& values) {
		write((std::string(name) + "_size").c_str(), values.size());
		for (auto* value : values)
			write(name, *value);
		return *this;
	}

	OutputStream& OutputStream::write(const char* name, Poco::Timestamp& value) {
		return write(name, (size_t)value.epochMicroseconds());
	}

	OutputStream& OutputStream::erase(const char* name) {
		return *this;
	}

	OutputStream& OutputStream::eraseIndex(const char* name, size_t index) {
		return *this;
	}

	bool OutputStream::doesSupportBufferedWrite() const {
		return false;
	}

	bool OutputStream::doesSupportNativeHashtables() const {
		return false;
	}

	bool OutputStream::isDB() const {
		return false;
	}

	OutputStream& OutputStream::write(const char* name, const char* buffer, size_t length) {
		return *this;
	}

	OutputStream& OutputStream::write(const char* name, const StrStr_UnMap& map) {
		return writeMapGeneric(name, map);
	}

	OutputStream& OutputStream::write(const char* name, const StrStr_Map& map) {
		return writeMapGeneric(name, map);
	}

	OutputStream& OutputStream::write(const char* name, const StrInt_UnMap& map) {
		return writeMapGeneric(name, map);
	}

	OutputStream& OutputStream::write(const char* name, const StrInt_Map& map) {
		return writeMapGeneric(name, map);
	}

	OutputStream& OutputStream::write(const char* name, const StrFloat_UnMap& map) {
		return writeMapGeneric(name, map);
	}

	OutputStream& OutputStream::write(const char* name, const StrFloat_Map& map) {
		return writeMapGeneric(name, map);
	}

	void OutputStream::flush() {
	}
}


} /// namespace pesa 
