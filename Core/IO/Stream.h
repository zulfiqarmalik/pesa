/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Stream.h
///
/// Created on: 2 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Core/CoreDefs.h"

#include <iostream>
#include <unordered_map>
#include <map>
#include <vector>
#include <memory>

#include "Poco/Timestamp.h"

namespace pesa {
namespace io {
	typedef std::unordered_map<std::string, std::string>	StrStr_UnMap;
	typedef std::map<std::string, std::string>				StrStr_Map;

	typedef std::unordered_map<std::string, int>			StrInt_UnMap;
	typedef std::map<std::string, int>						StrInt_Map;

	typedef std::unordered_map<std::string, float>			StrFloat_UnMap;
	typedef std::map<std::string, float>					StrFloat_Map;

	////////////////////////////////////////////////////////////////////////////
	/// Stream: Base class for all streams
	////////////////////////////////////////////////////////////////////////////
	class Core_API Stream {
	public:
		Stream();
		virtual ~Stream();
	};

	class InputStream;
	class OutputStream;
	////////////////////////////////////////////////////////////////////////////
	/// Streamable: A streamable object	
	////////////////////////////////////////////////////////////////////////////
	class Core_API Streamable {
	public:
		virtual InputStream& 	read(InputStream& is) = 0;
		virtual OutputStream& 	write(OutputStream& os) = 0;
	};

	typedef std::vector<Streamable*> StreamablePVec;

	////////////////////////////////////////////////////////////////////////////
	/// InputStream: Input streams
	////////////////////////////////////////////////////////////////////////////
	class Core_API InputStream : public Stream {
	public:
								InputStream();
		virtual 				~InputStream();

		virtual InputStream& 	read(const char* name, size_t* value) = 0;
		virtual InputStream& 	read(const char* name, unsigned int* value) = 0;
		virtual InputStream& 	read(const char* name, bool* value) = 0;
		virtual InputStream& 	read(const char* name, int* value) = 0;
		virtual InputStream& 	read(const char* name, int64_t* value) = 0;
		virtual InputStream& 	read(const char* name, float* value) = 0;
		virtual InputStream& 	read(const char* name, double* value) = 0;
		virtual InputStream& 	read(const char* name, std::string* value) = 0;
		virtual InputStream& 	read(const char* name, Streamable* object);
		virtual InputStream& 	read(const char* name, Poco::Timestamp* value);

		virtual bool			doesSupportBufferedRead() const;
		virtual bool			doesSupportNativeHashtables() const;
		virtual bool			isDB() const;
		virtual InputStream&	read(const char* name, char* buffer, size_t length);

		template <class T>
		InputStream&			readObj(const char* name, std::vector<T*>* values) {
			std::vector<Streamable*> svalues(values->size());
			for (size_t i = 0; i < values->size(); i++)
				svalues[i] = dynamic_cast<Streamable*>(values->at(i));
			return read(name, &svalues);
		}

		template <class T>
		InputStream&			readObj(const char* name, std::vector<T>* values) {
			std::vector<Streamable*> svalues(values->size());
			for (size_t i = 0; i < values->size(); i++) {
				auto& value = values->at(i);
				svalues[i] = dynamic_cast<Streamable*>(&value);
			}
			return read(name, &svalues);
		}

		virtual size_t			size(const char* name);
		virtual InputStream&	read(const char* name, std::vector<int>* values);
		virtual InputStream&	read(const char* name, std::vector<float>* values);
		virtual InputStream&	read(const char* name, std::vector<double>* values);
		virtual InputStream&	read(const char* name, std::vector<int64_t>* values);
		virtual InputStream&	read(const char* name, std::vector<std::string>* values);
		virtual InputStream&	read(const char* name, std::vector<Streamable*>* values);

		//////////////////////////////////////////////////////////////////////////
		/// Flush the contents of the stream from Disk/DB to MEMORY
		//////////////////////////////////////////////////////////////////////////
		virtual void 			flush();

		//////////////////////////////////////////////////////////////////////////
		virtual InputStream& 	read(const char* name, StrStr_UnMap* map);
		virtual InputStream&	read(const char* name, StrStr_Map* map);
		virtual InputStream& 	read(const char* name, StrInt_UnMap* map);
		virtual InputStream&	read(const char* name, StrInt_Map* map);
		virtual InputStream& 	read(const char* name, StrFloat_UnMap* map);
		virtual InputStream&	read(const char* name, StrFloat_Map* map);

		template <typename type> 
		InputStream& 			readVecGeneric(const char* name, std::vector<type>* values) { 
			size_t vecSize;
			read((std::string(name) + "_size").c_str(), &vecSize);
			values->resize(vecSize);

			if (!doesSupportBufferedRead()) {
				for (auto& value : *values)
					read(name, &value);
			}
			else {
				if (values->size())
					read(name, (char*)&(*values)[0], sizeof(type) * values->size());
			}

			return *this;
		} 

		template <typename MapType, typename ValueType> 
		InputStream& 			readMapGeneric(const char* name, MapType* map) {
			std::vector<std::string> keys;
			read((std::string(name) + "_keys").c_str(), &keys);

			//for (auto iter = map.begin(); iter != map.end(); iter++)
			for (const auto& key : keys) {
				ValueType value;
				read(key.c_str(), &value);
				(*map)[key] = value;
			}

			return *this;
		}
	};

	typedef std::shared_ptr<InputStream> InputStreamPtr;

	////////////////////////////////////////////////////////////////////////////
	/// OutputStream: Output streams
	////////////////////////////////////////////////////////////////////////////
	class Core_API OutputStream : public Stream {
	public:
								OutputStream();
		virtual 				~OutputStream();

		virtual OutputStream& 	write(const char* name, const size_t& value) = 0;
		virtual OutputStream& 	write(const char* name, const unsigned int& value) = 0;
		virtual OutputStream& 	write(const char* name, const bool& value) = 0;
		virtual OutputStream&	write(const char* name, const int& value) = 0;
		virtual OutputStream&	write(const char* name, const int64_t& value) = 0;
		virtual OutputStream&	write(const char* name, const float& value) = 0;
		virtual OutputStream&	write(const char* name, const double& value) = 0;
		virtual OutputStream&	write(const char* name, const std::string& value) = 0;
		virtual OutputStream&	write(const char* name, Streamable& value);
		virtual OutputStream&	write(const char* name, Poco::Timestamp& value);

		virtual bool			doesSupportBufferedWrite() const;
		virtual bool			doesSupportNativeHashtables() const; 
		virtual bool			isDB() const;
		virtual OutputStream&	write(const char* name, const char* buffer, size_t length);

		virtual OutputStream& 	erase(const char* name);
		virtual OutputStream& 	eraseIndex(const char* name, size_t index);

		template <class T>
		OutputStream&			writeObj(const char* name, std::vector<T*>& values) {
			std::vector<Streamable*> svalues(values.size());
			for (size_t i = 0; i < values.size(); i++)
				svalues[i] = dynamic_cast<Streamable*>(values[i]);
			return write(name, svalues);
		}

		template <class T>
		OutputStream&			writeObj(const char* name, std::vector<T>& values) {
			std::vector<Streamable*> svalues(values.size());
			for (size_t i = 0; i < values.size(); i++) {
				auto& value = values[i];
				svalues[i] = dynamic_cast<Streamable*>(&value);
			}
			return write(name, svalues);
		}

		virtual OutputStream&	write(const char* name, const std::vector<int>& values);
		virtual OutputStream&	write(const char* name, const std::vector<float>& values);
		virtual OutputStream&	write(const char* name, const std::vector<double>& values);
		virtual OutputStream&	write(const char* name, const std::vector<int64_t>& values);
		virtual OutputStream&	write(const char* name, const std::vector<std::string>& values);
		virtual OutputStream&	write(const char* name, const std::vector<Streamable*>& values);

		//////////////////////////////////////////////////////////////////////////
		/// Writing different kinds of std::map<KEY, VALUE> variants
		//////////////////////////////////////////////////////////////////////////
		virtual OutputStream& 	write(const char* name, const StrStr_UnMap& map);
		virtual OutputStream&	write(const char* name, const StrStr_Map& map);
		virtual OutputStream& 	write(const char* name, const StrInt_UnMap& map);
		virtual OutputStream&	write(const char* name, const StrInt_Map& map);
		virtual OutputStream& 	write(const char* name, const StrFloat_UnMap& map);
		virtual OutputStream&	write(const char* name, const StrFloat_Map& map);

		template <typename type> 
		OutputStream& 			writeVecGeneric(const char* name, const std::vector<type>& values) {
			write((std::string(name) + "_size").c_str(), values.size());

			if (!doesSupportBufferedWrite()) {
				for (const auto& value : values)
					write(name, value);
			}
			else {
				if (values.size())
					write(name, (const char*)&values[0], sizeof(type) * values.size());
			}
			return *this;
		} 

		template <typename MapType> 
		OutputStream& 			writeMapGeneric(const char* name, const MapType& map) {
			std::vector<std::string> keys;
			for (auto iter = map.begin(); iter != map.end(); iter++)
				keys.push_back(iter->first);

			write((std::string(name) + "_keys").c_str(), keys);

			for (auto iter = map.begin(); iter != map.end(); iter++)
				write(iter->first.c_str(), iter->second);

			return *this;
		}

		//////////////////////////////////////////////////////////////////////////
		/// Flush the contents of the stream to Disk/DB
		//////////////////////////////////////////////////////////////////////////
		virtual void 			flush();
	};

	typedef std::shared_ptr<OutputStream> OutputStreamPtr;

} /// namespace io
} /// namespace pesa 

#define PESA_WRITE_VAR(Stream, Name) 			Stream.write(#Name, Name)
#define PESA_READ_VAR(Stream, Name) 			Stream.read(#Name, &Name)

#define PESA_WRITE_CVAR(Stream, Name) 			Stream.write(#Name, m_##Name)
#define PESA_READ_CVAR(Stream, Name) 			Stream.read(#Name, &m_##Name)

#define PESA_WRITE_OBJ_VEC(Stream, Name) 		Stream.writeObj(#Name, Name)

#define PESA_READ_OBJ_VEC(Stream, Name, Ctor)	{ \
													size_t __vecSize__ = Stream.size(#Name); \
													Name.resize(__vecSize__); \
													for (size_t i = 0; i < __vecSize__; i++) { \
														Name[i] = Ctor; \
													} \
													Stream.readObj(#Name, &Name); \
												} 

