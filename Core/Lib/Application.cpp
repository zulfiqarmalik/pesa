/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Application.cpp
///
/// Created on: 1 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Application.h"
#include "./Util.h"

namespace pesa {
	const int AppOptions::s_maxThreads = 32;
	Application* Application::s_instance = nullptr;

	Application::Application() {
		ASSERT(s_instance == nullptr, "Cannot create multiple instances of the main application!");
		s_instance = this;
		setUnixOptions(true); 
	}

	Application::~Application() {
	}

	Application* Application::instance() {
		ASSERT(s_instance != nullptr, "Applicaiton instance has not been created!");
		return s_instance;
	}

	Poco::Path Application::cmdPath() const {
		if (m_appPath.empty())
			return Poco::Path(Poco::Util::Application::commandPath());
		return m_appPath;
	}
	
	Poco::Path Application::cmdDir() const {
		return cmdPath().makeParent();
	}

	std::string Application::prevVersion() {
		/// since the prev version can be empty, we want to check the version
		if (!m_version.empty())
			return m_prevVersion;

		/// Otherwise, load the version
		version();
		return m_prevVersion;
	}

	std::string Application::version() {
		if (!m_version.empty())
			return m_version;

		/// Load the version
		Poco::Path versionPath(cmdDir());
		versionPath.setFileName("VERSION");
		std::string versionFilename = versionPath.toString();
		FILE* f = fopen(versionFilename.c_str(), "r");
		ASSERT(f, "Unable to load version from file: " << versionFilename);
		char versionStr[64];
		fscanf(f, "%s", versionStr);
		m_version = versionStr;
		versionStr[0] = '\0';
		fscanf(f, "%s", versionStr);
		m_prevVersion = versionStr;
		ASSERT(!m_version.empty(), "Invalid version in the version file!");
		fclose(f);

		return m_version;
	}
} /// namespace pesa 
