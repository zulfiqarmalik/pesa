/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Application.h
///
/// Created on: 1 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Core/CoreDefs.h"
#include "Util.h"
#include <Poco/Util/Application.h>

namespace pesa {
	//////////////////////////////////////////////////////////////////////////
	/// PipelineOptions - The options that were passed to the application
	//////////////////////////////////////////////////////////////////////////
	struct Core_API AppOptions {
		bool 					updateDataOnly = false;
		std::string				dumpData;
		std::string				dumpFilename;

		std::string				dbPath;
		std::string				dbName = "pesa";

		std::string				username;
		std::string				password;

		std::string				simInstanceName = "PSim";		/// Optional instance name that is assigned by the PDaemon

		int						limit = 0;
		static const int		s_maxThreads;
		int						numThreads = 1;
		int						maxChildProcesses = 4;
		Poco::Message::Priority	logLevel = Poco::Message::PRIO_DEBUG;
		std::string				logFilename;			/// The filename that this instance is logging to

		bool					runDiagnostics = false; /// If this value is set then run diagnostics on that specified in dumpData
		bool					headerOnly = false;		/// Perform various operations only on the header
		bool					force = false;			/// Force certain operations. This is picked up by various modules

		bool					quiet = false;			/// Don't output anything on the console
		bool					waitForTimestamp = false; /// Wait for the data registry TIMESTAMP
		int						waitInterval = 100;		/// The wait interval in milliseconds
		std::string				jobId;					/// The ID of the job that we're going to run

		int						days = 5;				/// Days parameter (number of days to update the data. Default = 2)

		bool					forceLocates = false;	/// Force the use of locates on the day
		bool					noRunChildStrats = false;/// Whether to override the config variable of noRunStrategies or not
		bool					runCmds = false;		/// Run PyCmdRunner or not ...
		bool					useShMem = false;		/// Use shared memory or not
		bool					useComboCheckpoint = false; /// Use combo checkpoints or not 
		bool					clampEndDate = false;	/// Clamp the end date
		bool					runServer = false;		/// Run in daemon/server mode 

		std::map<std::string, std::string> userMacros;	/// User defined macros
		StringVec				uuids; 					/// A specific set of UUIDs that need to be updated (or have some other operaton performed)!

		std::string				ovrStartDate;			/// Overridden start date of the config
		std::string				ovrEndDate;				/// Overridden end date of the config

		std::string				configOverrides;		/// Config overrides

		bool					disableOptimisation = false; /// Disable DataRegistry optimisation or not
		bool					noDownload = false;		/// Don't download thirdparty dependencies
		bool					onlyValid = false;		/// Only show valid values while dumping data contents
		bool					onlyInvalid = false;	/// Only show invalid values while dumping data contents
		bool					ignoreDefault = false;	/// Ignore the default values
		StringVec				defaultValues;			/// The default values, which to ignore
	};

	//////////////////////////////////////////////////////////////////////////
	/// Application: The base application class
	//////////////////////////////////////////////////////////////////////////
	class Core_API Application : public Poco::Util::Application {
	protected:
		static Application*		s_instance;				/// Static singleton instance

		std::string				m_prevVersion;			/// The previous version
		std::string				m_version;				/// The version of the application
		std::string				m_appPath;				/// The path of the application

								Application();
		virtual 				~Application();

	public:
		static Application*		instance();

		Poco::Path				cmdPath() const;
		Poco::Path 				cmdDir() const;

		virtual void			exit(int code) = 0;
		virtual void			abort() = 0;
		std::string				version();
		std::string				prevVersion();

		virtual pesa::AppOptions appOptions() const = 0;
	};

} /// namespace pesa

