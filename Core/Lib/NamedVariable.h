/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// NamedVariable.h
///
/// Created on: 2 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#ifndef CORE_NAMEDVARIABLE_H_
#define CORE_NAMEDVARIABLE_H_

#include "Poco/Any.h"
#include <utility>

#if 0
namespace pesa {

	template <char... chars>
	using TString = std::integer_sequence<char, chars...>;

	template <typename T, T... chars>
	constexpr TString<chars...> operator""_T() { return { }; }

	template <typename, typename>
	class NamedVariable;

	template <typename t_type, char... elements>
	class NamedVariable<t_type, TString<elements...>> : public Poco::Any {
	protected:
		bool 				m_dirty;

	public:
		NamedVariable() : m_dirty(false) {
		}

		NamedVariable(const t_type& rhs) : m_dirty(true) {
			*this = rhs;
		}

		// template <typename t_type>
		// NamedVariable(t_type rhs) : m_dirty(true) {
		// 	*this = rhs;
		// }

		virtual ~NamedVariable() {
		}

		void operator = (const t_type& rhs) {
			Poco::Any::operator=(rhs);
			m_dirty = true;
			// return *this;
		}

		operator t_type() const {
			return value();
		}

		t_type value() const {
			ASSERT(this->type() == typeid(t_type), "Type mismatch. Original Variable: " << this->type().name() << ", Casting Type: " << typeid(t_type).name());
			return Poco::AnyCast<t_type>(*this);
		}

		inline const char* name() const { 
        	static constexpr char str[sizeof...(elements) + 1] = { elements..., '\0' };
        	return str;
		}
	};

} /// namespace pesa

#define NAMED_FLOAT(name) 		NamedVariable<float, decltype(#name##_T)> 	name

#endif 

#endif /// CORE_NAMEDVARIABLE_H_ 
