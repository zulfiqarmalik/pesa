/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Util.cpp
///
/// Created on: 31 Mar 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Util.h"

#include <stdio.h>
#include <iostream>
#include <string>
#include <functional>
#include <algorithm>
#include <signal.h>
#include <stdlib.h>
#include <regex>
#include <iomanip>
#include <cmath>
#include <numeric>
#include <fstream>

#include "Application.h"

#include "Poco/NestedDiagnosticContext.h"
#include "Poco/String.h"
#include "Poco/File.h"
#include "Poco/FileStream.h"
#include "Poco/TemporaryFile.h"
#include "Poco/DateTimeParser.h"
#include "Poco/Base64Encoder.h"
#include "Poco/String.h"
#include "Poco/StringTokenizer.h"
#include "Poco/String.h"
#include "Poco/Zip/ZipArchive.h"
#include "Poco/Zip/ZipStream.h"
#include "Poco/StreamCopier.h"

#include <iostream>
#include <unordered_map>

struct MemNode {
	void* ptr = nullptr;
	size_t size = 0;
	const char* filename = nullptr;
	int line = 0;

	void print() {
		std::cerr << "Memory not freed: " << filename << ":" << line << " - " << size << " bytes" << std::endl;
	}
};

namespace pesa {
	void Util::printStackTrace() {
#ifndef __WINDOWS__
		void *array[10];
		size_t size;

		// get void*'s for all entries on the stack
		size = backtrace(array, 10);

		// print out all the frames to stderr
//		fprintf(stderr, "Error: signal %d:\n", sig);
		backtrace_symbols_fd(array, size, STDERR_FILENO);
#endif /// __WINDOWS__
	}

	void Util::reportException(const std::exception& e) {
		Poco::Logger::root().critical("Application Exception: %s", std::string(e.what()));
        std::cerr << "Application Exception: " << e.what() << std::endl;
		printStackTrace();
	}

	void Util::reportException(const Poco::Exception& e) {
		Poco::Logger::root().critical("Application Exception: %s", e.displayText());
        std::cerr << "Application Exception: " << e.displayText() << std::endl;
		printStackTrace();
	}

	void Util::exit(int errCode) {
		std::cerr << "Log file: " << Application::instance()->appOptions().logFilename << std::endl;
		Application::instance()->exit(errCode);
		//std::exit(errCode);
	}

	void Util::abort() {
		std::cerr << "Log file: " << Application::instance()->appOptions().logFilename << std::endl;
		Application::instance()->abort();
		//std::abort();
	}

	////////////////////////////////////////////////////////////////////////////
	/// Date utility functions ...
	////////////////////////////////////////////////////////////////////////////
	Poco::Timespan Util::daysSpan(unsigned int days) {
		return Poco::Timespan(days, 0, 0, 0, 0);
	}

	Poco::DateTime Util::py_timestampToDateTime(int64_t ts) {
		return Poco::DateTime(Poco::Timestamp(ts / 1000));
	}

	unsigned int Util::py_timestampToIntDate(int64_t ts) {
		return timestampToIntDate(ts / 1000);
	}

	unsigned int Util::timestampToIntDate(const Poco::Timestamp& ts) {
		return dateToInt(Poco::DateTime(ts));
	}

	unsigned int Util::timestampToIntDate(int64_t ts) {
		return dateToInt(Poco::DateTime(Poco::Timestamp(ts)));
	}

	std::string Util::dateToStr(const Poco::DateTime& date) {
		std::ostringstream ss;
		ss << 
			date.year() <<
			std::setfill('0') << std::setw(2) << date.month() <<
			std::setfill('0') << std::setw(2) << date.day();
		return ss.str();
	}

	std::string Util::dateToStr(const Poco::DateTime& date, std::string sep) {
		std::ostringstream ss;
		ss <<
			date.year() << sep <<
			std::setfill('0') << std::setw(2) << date.month() << sep <<
			std::setfill('0') << std::setw(2) << date.day();
		return ss.str();
	}

	unsigned int Util::dateToInt(const Poco::DateTime& date) {
		return Util::cast<unsigned int>(std::string(dateToStr(date)));
	}

	Poco::DateTime Util::intToDate(unsigned int date) {
		unsigned int year, month, day;
		splitIntDate(date, year, month, day);
		return Poco::DateTime((int)year, (int)month, (int)day);
	}

	std::string Util::dateTimeToStr(const Poco::DateTime& date) {
		std::ostringstream ss;
		ss <<
				date.year() <<
				std::setfill('0') << std::setw(2) << date.month() <<
				std::setfill('0') << std::setw(2) << date.day() << "-" <<
				std::setfill('0') << std::setw(2) << date.hour() << ":" <<
				std::setfill('0') << std::setw(2) << date.minute() << ":" <<
				std::setfill('0') << std::setw(2) << date.second();
		return ss.str();
	}

	void Util::splitIntDate(unsigned int date, unsigned int& year, unsigned int& month, unsigned int& day) {
		year = date / 10000;
		date %= 10000;
		month = date / 100;
		day = date % 100;
	}

	Poco::DateTime Util::strToDate(const std::string& date) {
		return intToDate(Util::cast<unsigned int>(date));
	}

	Poco::DateTime Util::parseFormattedDate(std::string format, std::string date) {
		Poco::DateTime dt;
		int tz = 0;
		Poco::DateTimeParser::parse(format, date, dt, tz);
		dt.makeUTC(tz);
		return dt;
	}

	Poco::DateTime Util::strDateTimeToUTCDateTime(const std::string& dtStr, int* ptz /* = nullptr */) {
		Poco::DateTime dt;
		int tz = 0;
		Poco::DateTimeParser::parse(dtStr, dt, tz);
		dt.makeUTC(tz);
		if (ptz)
			*ptz = tz;
		return dt;
	}

	unsigned int Util::strDateTimeToIntDate(const std::string& dtStr) {
		Poco::DateTime dt;
		int tz = 0;
		Poco::DateTimeParser::parse(dtStr, dt, tz);
		dt.makeUTC(tz);
		return dateToInt(dt);
	}

	unsigned int Util::strDateToIntDate(std::string dtStr, const char sep /* = '-' */) {
		dtStr.erase(std::remove(dtStr.begin(), dtStr.end(), sep), dtStr.end());
		return Util::cast<unsigned int>(dtStr);
	}

	unsigned int Util::parseDate(const std::string& date_, int* offset /* = nullptr */) {
		const std::string todayStr = "TODAY";
		std::string date = Poco::toUpper(date_);

		date = Util::trim(date);
		date = Poco::remove(date, '.');
		//date = Poco::remove(date, '-');
		date = Poco::remove(date, '/');

		if (date == todayStr && date.length() == todayStr.length())
			return Util::dateToInt(Poco::DateTime());

		size_t pos = date.find("-");
		bool isSubtract = true;

		/// Try to find a - sign
		if (pos == std::string::npos) {
			pos = date.find("+");

			/// or a plus sign
			if (pos == std::string::npos) {
				/// If none was found then just convert to int
				return Util::cast<unsigned int>(date);
			}

			isSubtract = false;
		}

		std::string part1 = date.substr(0, pos);
		std::string part2 = date.substr(pos + 1, date.length() - pos);
		part1 = Util::trim(part1);
		part2 = Util::trim(part2);

		Poco::DateTime d = 0;

		if (part1 == todayStr)
			d = Poco::DateTime();
		else
			d = intToDate(Util::cast<unsigned int>(part1));

		if (!offset) {
			Poco::Timespan ts(Util::cast<unsigned int>(part2), 0, 0, 0, 0);

			if (isSubtract)
				d = d - ts;
			else
				d = d + ts;
		}
		else {
			*offset = Util::cast<int>(part2);
			if (isSubtract)
				*offset *= -1;
		}

		return Util::dateToInt(d);
	}

	unsigned int Util::strToTime(const std::string& time_) {
		std::string time = Poco::remove(time_, ':');
		return Util::cast<unsigned int>(time);
	}

	Poco::Timespan Util::parseTime(const std::string& time) {
		return parseTime(strToTime(time));
	}

	unsigned int Util::timespanToTime(const Poco::Timespan& span) {
		std::ostringstream ss;
		ss	<< std::setfill('0') << std::setw(2) << span.hours() 
			<< std::setfill('0') << std::setw(2) << span.minutes() 
			<< std::setfill('0') << std::setw(2) << span.seconds();
		return Util::cast<unsigned int>(ss.str());
	}

	unsigned int Util::dateToTime(const Poco::DateTime& date) {
		Poco::Timespan span(0, date.hour(), date.minute(), date.second(), 0);
		return timespanToTime(span);
	}

	Poco::DateTime Util::intToDateTime(unsigned int date, unsigned int time) {
		auto dt = Util::intToDate(date);
		auto ts = Util::parseTime(time);
		return Poco::DateTime(dt.year(), dt.month(), dt.day(), ts.hours(), ts.minutes(), ts.seconds(), ts.milliseconds(), ts.microseconds());
	}

	Poco::Timestamp Util::intToTimestamp(unsigned int date, unsigned int time) {
		return intToDateTime(date, time).timestamp();
	}

	Poco::Timespan Util::parseTime(unsigned int time) {
		int atime = std::abs((int)time);

		if (atime < 10)
			atime *= 10000;
		else if (atime < 100)
			atime *= 100;
		else if (atime < 10000)
			atime *= 100;

		int hours = (int)(((double)atime / 10000.0) * std::copysign(1, time));
		atime %= 10000;
		int minutes = atime / 100;
		int seconds = atime % 100;

		return Poco::Timespan(0, hours, minutes, seconds, 0);
	}

	std::string Util::intDateToStr(unsigned int date, const char* separator /* = nullptr */) {
		if (!separator)
			return Util::cast(date);

		unsigned int year, month, day;
		splitIntDate(date, year, month, day);
		std::ostringstream ss;
		ss 		<< year << separator
				<< std::setfill('0') << std::setw(2) << month << separator
				<< std::setfill('0') << std::setw(2) << day;

		return ss.str();
	}

	std::string Util::intDateToStr(std::string format, unsigned int date) {
		unsigned int year, month, day;
		splitIntDate(date, year, month, day);

		std::ostringstream ssMM, ssDD;

		ssMM << std::setfill('0') << std::setw(2) << month;
		ssDD << std::setfill('0') << std::setw(2) << day;

		std::string fmtYYYY = Poco::replace(format, "{$YYYY}", Util::cast(year).c_str());
		std::string fmtMM = Poco::replace(fmtYYYY, "{$MM}", ssMM.str().c_str());
		return Poco::replace(fmtMM, "{$DD}", ssDD.str().c_str());
	}

	unsigned int Util::daysInYear(unsigned int idate) {
		auto date = intToDate(idate);
		Poco::DateTime startDate(date.year(), 1, 1);
		Poco::DateTime endDate(date.year(), 12, 31);
		return (endDate - startDate).days() + 1;
	}

	unsigned int Util::daysInBetween(unsigned int istart, unsigned int iend) {
		auto start = intToDate(istart);
		auto end = intToDate(iend);
		return (end - start).days();
	}

	////////////////////////////////////////////////////////////////////////////
	/// IO related
	////////////////////////////////////////////////////////////////////////////
	size_t Util::getStreamSize(std::istream& stream) {
		std::streampos currPos = stream.tellg();
		stream.seekg(0, stream.end);
		std::streampos endPos = stream.tellg();
		stream.seekg(currPos, stream.beg);
		return (size_t)(endPos);
	}

	size_t Util::getStreamSize(std::ostream& stream) {
		std::streampos currPos = stream.tellp();
		stream.seekp(0, stream.end);
		std::streampos endPos = stream.tellp();
		stream.seekp(currPos, stream.beg);
		return (size_t)(endPos);
	}

	void Util::ensureDirExists(const Poco::Path& path) {
		Poco::Path parent = path.parent();
		Poco::File file(parent.toString());

		if (!file.exists())
			file.createDirectories();
	}

	bool Util::doesFileExist(const std::string& filename) {
		Poco::File file(filename);
		return file.isFile();
	}

	bool Util::doesDirExist(const std::string& dirname) {
		Poco::File file(dirname);
		return file.isDirectory();
	}

	char* Util::readEntireFile(const std::string& filename, size_t& fileSize) {
		Poco::File file(filename);
		fileSize = 0;

		if (!file.exists())
			return nullptr;

		fileSize = file.getSize();
		Poco::FileInputStream fis(filename);
		char* fileData = new char[fileSize];
		fis.read(fileData, fileSize);
		return fileData;
	}

	void Util::deleteFile(const std::string& filename) {
		Poco::File f(filename);
		if (f.exists())
			f.remove(false);
	}

	std::string Util::readEntireTextFile(const std::string& filename) {
		char* fileData = nullptr;
		size_t fileSize = readEntireTextFile(filename, &fileData);
		return std::move(std::string(fileData, fileSize));
	}

	size_t Util::readEntireTextFile(const std::string& filename, char** buffer) {
		Poco::File file(filename);

		if (!file.exists())
			return 0;
		//ASSERT(file.exists(), "File does not exist: " << filename);

		size_t fileSize = file.getSize();
		Poco::FileInputStream fis(filename);
		*buffer = new char[fileSize + 1]; /// 1 for the null terminator
		fis.read(*buffer, fileSize);

		(*buffer)[fileSize] = '\0';

		return fileSize;
	}

	size_t Util::readEntireTextFileFromZip(const std::string& zipFilename, const std::string& contentsFilename, char** buffer) {
		/// Create a temporary file to copy the data
		auto tmp = std::make_unique<Poco::TemporaryFile>();
		auto path = tmp->path();

		{
			std::ifstream inp(zipFilename, std::ios::binary);
			ASSERT(inp, "Unable to open zip file: " << zipFilename);

			using namespace Poco::Zip;
			ZipArchive arch(inp);
			ZipArchive::FileHeaders::const_iterator it = arch.findHeader(contentsFilename);
			ASSERT(it != arch.headerEnd(), "Unable to find file: " << contentsFilename << " inside zip archive: " << zipFilename);

			ZipInputStream zipin(inp, it->second);
			std::ofstream out(path, std::ios::binary);
			Poco::StreamCopier::copyStream(zipin, out);

			inp.close();
			out.close();
		}

		/// Now we just read the data from the tmp file that we have extracted!
		size_t bufferLength = readEntireTextFile(path, buffer);

		/// Delete the temporary file, if it still exists! Poco doesn't always do that for some reason!
		Util::deleteFile(path);

		return bufferLength;
	}

	std::string Util::readEntireTextFileFromZip(const std::string& zipFilename, const std::string& contentsFilename) {
		char* fileData = nullptr;
		size_t fileSize = readEntireTextFileFromZip(zipFilename, contentsFilename, &fileData);
		return std::move(std::string(fileData, fileSize));
	}

	std::string Util::base64Encode(const std::string& str) {
		std::ostringstream ss;
		Poco::Base64Encoder encoder(ss);
		encoder << str;
		encoder.close();
		return ss.str();
	}

	////////////////////////////////////////////////////////////////////////////
	/// Money related
	////////////////////////////////////////////////////////////////////////////
	float Util::amount(const std::string& str) {
		std::string amount = Poco::replace(str, ",", "");
		Poco::removeInPlace(amount, '$'); /// remove all $ symbols
		Poco::replace(str, ",", "");
		float multiplier = 1.0f;

		if (amount.find("MM") != std::string::npos) {
			amount = Poco::replace(str, "MM", "");
			multiplier = 1.0e12f;
		}
		else if (amount.find("mm") != std::string::npos) {
			amount = Poco::replace(str, "mm", "");
			multiplier = 1.0e12f;
		}
		else if (amount.find("B") != std::string::npos) {
			amount = Poco::replace(str, "B", "");
			multiplier = 1.0e12f;
		}
		else if (amount.find("b") != std::string::npos) {
			amount = Poco::replace(str, "b", "");
			multiplier = 1.0e12f;
		}
		else if (amount.find("M") != std::string::npos) {
			amount = Poco::replace(str, "M", "");
			multiplier = 1.0e6f;
		}
		else if (amount.find("_") != std::string::npos) {
			amount = Poco::replace(str, "m", "");
			multiplier = 1.0e6f;
		}
		else if (amount.find("K") != std::string::npos) {
			amount = Poco::replace(str, "K", "");
			multiplier = 1000.0f;
		}
		else if (amount.find("k") != std::string::npos) {
			amount = Poco::replace(str, "k", "");
			multiplier = 1000.0f;
		}

		float value = Util::cast<float>(amount);
		return value * multiplier;
	}

	float Util::basisPoints(const std::string& str) {
		std::string amount = Poco::replace(str, ",", "");
		Poco::removeInPlace(amount, '$'); /// remove all $ symbols
		Poco::replace(str, ",", "");
		double multiplier = 1.0;

		if (amount.find("bps") != std::string::npos) {
			amount = Poco::replace(str, "bps", "");
		}
		else if (amount.find("%") != std::string::npos) {
			amount = Poco::replace(str, "%", "");
			multiplier = 100.0;
		}

		double value = Util::cast<double>(amount);
		return (float)(value * multiplier);
	}

	float Util::percent(const std::string& str) {
		return basisPoints(str) * 0.01f;
	}

	////////////////////////////////////////////////////////////////////////////
	/// String utility functions ...
	////////////////////////////////////////////////////////////////////////////
	std::string Util::XOR(const std::string& value, const std::string& key) {
		std::string retVal(value);

		size_t klen = key.length();
		size_t vlen = value.length();
		size_t k = 0;
		size_t v = 0;

		for (; v < vlen; v++) {
			retVal[v] = value[v] ^ key[k];
			k = (++k < klen ? k : 0);
		}

		return retVal;
	}

	std::string& Util::ltrim(std::string& s) {
		s = Poco::trimLeft(s);
		return s;
	}

	std::string& Util::rtrim(std::string& s) {
		s = Poco::trimRight(s);
		return s;
	}

	std::string& Util::trim(std::string& s) {
		s = Poco::trim(s);
		return s;
	}

	StringVec Util::split(const std::string& str, const std::string& sep) {
		Poco::StringTokenizer tok(str, sep, Poco::StringTokenizer::TOK_TRIM);
		StringVec tokens;
		tokens.reserve(tok.count());

		for (Poco::StringTokenizer::Iterator iter = tok.begin(); iter != tok.end(); iter++) {
			std::string token = *iter;

			if (token != "\n" && token != "\r" && token != "\r\n") {
				token = Poco::remove(token, '"');
				if (Poco::toLower(token) == "n/a")
					token = "";
				token = Poco::trim(token);
				tokens.push_back(token);
			}
		}

		return tokens;
	}

	StringVec Util::regexSplit(const std::string& str, const std::string& regexStr) {
#ifdef __WINDOWS__
		std::regex regex(regexStr);
		std::string s = str;
		std::regex_token_iterator<std::string::iterator> rend;
		std::regex_token_iterator<std::string::iterator> iter(s.begin(), s.end(), regex);
		StringVec tokens;

		while (iter != rend) {
			std::string token = *iter++;
			if (!token.empty() && token != "\n" && token != "\r" && token != "\r\n" && token != ",") {
				token = Poco::remove(token, '"');
				if (Poco::toLower(token) == "n/a")
					token = "";
				tokens.push_back(token);
			}
		}

		return tokens;
#else 
		/// TODO: Linking issues on a custom installation of gcc 5.4.0
		return StringVec();
#endif
	}

	std::string Util::combine(const StringVec& vector, const char* sep_ /* = "." */, size_t start /* = 0 */, size_t end /* = 0 */) {
		std::string ret = "";
		std::string sep(sep_);

		if (end == start)
			end = vector.size();

		for (size_t i = start; i < end - 1; i++)
			ret += vector[i] + sep;

		if (end > start)
			ret += vector[end - 1];

		return ret;
	}

	bool Util::isInVector(const StringVec& vector, const std::string& str) {
		//for (const auto& vstr : vector) {
		//	if (vstr == str)
		//		return true;
		//}

		//return false;
		return !vector.empty() && std::find(vector.begin(), vector.end(), str) != vector.end();
	}

	int Util::levenshteinDistance(const std::string& s1, const std::string& s2) {
		// To change the type this function manipulates and returns, change
		// the return type and the types of the two variables below.
		auto s1len = s1.size();
		auto s2len = s2.size();

		auto columnStart = (decltype(s1len))1;

		auto column = new decltype(s1len)[s1len + 1];
		std::iota(column + columnStart - 1, column + s1len + 1, columnStart - 1);

		for (auto x = columnStart; x <= s2len; x++) {
			column[0] = x;

			auto lastDiagonal = x - columnStart;

			for (auto y = columnStart; y <= s1len; y++) {
				auto oldDiagonal = column[y];

				auto possibilities = {
					column[y] + 1,
					column[y - 1] + 1,
					lastDiagonal + (s1[y - 1] == s2[x - 1] ? 0 : 1)
				};

				column[y] = std::min(possibilities);
				lastDiagonal = oldDiagonal;
			}
		}

		auto result = column[s1len];
		delete[] column;

		return (int)result;
	}
} /// namespace pesa 
