/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Util.h
///
/// Created on: 31 Mar 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Core/CoreDefs.h"

#include <iostream>
#include <string>
#include <sstream>
#include <string>
#include <chrono>
#include <thread>
#include <mutex>
#include <iomanip>

#include <Poco/Logger.h>
#include <Poco/DateTime.h>
#include <Poco/Timespan.h>
#include <Poco/Timestamp.h>
#include <Poco/Path.h>
#include <Poco/Debugger.h>

#ifndef __WINDOWS__
	#include <unistd.h>
	#include <execinfo.h>
#endif

// #ifndef NDEBUG
#define ASSERT(condition, message) \
    do { \
        if (! (condition)) { \
			std::ostringstream ss; \
            ss << "Assertion `" #condition "` failed in " << __FILE__ \
                      << " line " << __LINE__ << ": " << message; \
			pesa::Util::critical("ASSERT", "%s", ss.str()); \
            Poco::Debugger::enter(); \
            pesa::Util::printStackTrace(); \
            pesa::Util::abort(); \
        } \
    } while (false)
// #else
// #   define ASSERT(condition, message) do { } while (false)
// #endif

// #define ASSERT_EXCEPTION(condition, message, ...) if (!condition) throw Poco::Exception(Poco::format(message, __VA_ARGS__))
    
#define BIT(i) 		(1 << i) 

#include <string>

typedef unsigned long ulong_t;

namespace pesa {

	template<typename t_type>
	struct SharedPtr_ArrayDeleter
	{
		void operator ()(t_type const * p) {
			delete[] p;
		}
	};

	typedef std::vector<std::string> 		StringVec;

	class Core_API Util {
	public:
		enum Priority {
			kLowest				= 0,
			kLow				= 25,
			kNormal				= 50,
			kHigh				= 75,
			kHighest			= 100,
		};

		////////////////////////////////////////////////////////////////////////////
		/// Exception utility functions ...
		////////////////////////////////////////////////////////////////////////////
		static void 			reportException(const std::exception& e);
		static void 			reportException(const Poco::Exception& e);
		static void 			printStackTrace();
		static void				exit(int errCode);
		static void				abort();

		////////////////////////////////////////////////////////////////////////////
		/// Date utility functions ...
		////////////////////////////////////////////////////////////////////////////
		static Poco::Timespan	daysSpan(unsigned int days);
		static Poco::DateTime 	strToDate(const std::string& date);
		static Poco::DateTime 	intToDate(unsigned int date);
		static unsigned int 	timestampToIntDate(const Poco::Timestamp& ts);
		static unsigned int 	timestampToIntDate(int64_t ts);
		static unsigned int		py_timestampToIntDate(int64_t ts);
		static Poco::DateTime	py_timestampToDateTime(int64_t ts);
		static unsigned int 	dateToInt(const Poco::DateTime& date);
		static unsigned int 	parseDate(const std::string& date, int* offset = nullptr);
		static Poco::Timespan 	parseTime(const std::string& time);
		static Poco::Timespan 	parseTime(unsigned int time);
		static Poco::DateTime	intToDateTime(unsigned int date, unsigned int time);
		static Poco::Timestamp	intToTimestamp(unsigned int date, unsigned int time);
		static unsigned int		timespanToTime(const Poco::Timespan& span);
		static unsigned int		strToTime(const std::string& time);
		static unsigned int		dateToTime(const Poco::DateTime& date);
		static void 			splitIntDate(unsigned int date, unsigned int& year, unsigned int& month, unsigned int& day);
		static std::string 		dateTimeToStr(const Poco::DateTime& date);
		static std::string 		dateToStr(const Poco::DateTime& date);
		static std::string 		dateToStr(const Poco::DateTime& date, std::string sep);
		static std::string 		intDateToStr(unsigned int date, const char* separator = nullptr);
		static std::string 		intDateToStr(std::string format, unsigned int date);
		static unsigned int		daysInYear(unsigned int date);
		static unsigned int		daysInBetween(unsigned int start, unsigned int end);
		static Poco::DateTime	strDateTimeToUTCDateTime(const std::string& dtStr, int* tz = nullptr);
		static unsigned int		strDateTimeToIntDate(const std::string& dtStr);
		static unsigned int		strDateToIntDate(std::string dtStr, const char sep = '-');
		static Poco::DateTime	parseFormattedDate(std::string format, std::string date);

		////////////////////////////////////////////////////////////////////////////
		/// IO related
		////////////////////////////////////////////////////////////////////////////
		static size_t 			getStreamSize(std::istream& stream);
		static size_t 			getStreamSize(std::ostream& stream);
		static void 			ensureDirExists(const Poco::Path& path);
		static std::string		readEntireTextFile(const std::string& filename);
		static size_t			readEntireTextFile(const std::string& filename, char** buffer);
		static std::string		readEntireTextFileFromZip(const std::string& zipFilename, const std::string& contentsFilename);
		static size_t			readEntireTextFileFromZip(const std::string& zipFilename, const std::string& contentsFilename, char** buffer);
		static char*			readEntireFile(const std::string& filename, size_t& fileSize);
		static void				deleteFile(const std::string& filename);
		static std::string		base64Encode(const std::string& str);
		static bool				doesFileExist(const std::string& filename);
		static bool				doesDirExist(const std::string& dirname);

		////////////////////////////////////////////////////////////////////////////
		/// Money related
		////////////////////////////////////////////////////////////////////////////
		static float 			amount(const std::string& str);
		static float 			percent(const std::string& str);
		static float 			basisPoints(const std::string& str);

		////////////////////////////////////////////////////////////////////////////
		/// String utility functions ...
		////////////////////////////////////////////////////////////////////////////
		static std::string		XOR(const std::string& value, const std::string& key);

		// Trim from the start
		static std::string&		ltrim(std::string& s);
		// Trim from end
		static std::string& 	rtrim(std::string& s);
		// Trim from both ends
		static std::string&		trim(std::string& s);

		static int				levenshteinDistance(const std::string& s1, const std::string& s2);

		static StringVec 		split(const std::string& s, const std::string& sep);
		static StringVec 		regexSplit(const std::string& s, const std::string& regex);
		static bool 			isInVector(const StringVec& vector, const std::string& str);
		static std::string 		combine(const StringVec& vector, const char* sep = ".", size_t start = 0, size_t end = 0);

		static inline void		copyString(char* dest, const std::string& src, size_t maxLen) {
			ASSERT(src.length() <= maxLen - 1, "Max uuid buffer overflow: " << maxLen << " - Given length: " << src.length() <<  " - String: " << src);
			strncpy(dest, src.c_str(), maxLen - 1);
			dest[maxLen - 1] = '\0';
		}

		template <typename t_type>
		static t_type 			cast(const std::string& str) {
			t_type ret;
			std::istringstream ss(str);
			ss >> ret;

			return ret;
		}

		template <typename t_type>
		static std::string 		cast(const t_type& t) {
			std::ostringstream ss;
			ss << t;
			return ss.str();
		}

		template <typename t_type>
		static std::string 		castPadded(const t_type& t, size_t width, const char padding = '0') {
			std::ostringstream ss;
			ss << std::setfill('0') << std::setw(2) << t;
			return ss.str();
		}

		////////////////////////////////////////////////////////////////////////////
		/// Log utility functions ...
		////////////////////////////////////////////////////////////////////////////
		static inline std::string tsstr() {
			Poco::DateTime dt;
			return "[" + Util::dateTimeToStr(dt) + "]-";
		}

		template <typename... Args>
		static void trace(const char* channel, const std::string& message, Args... args) {
			if (Poco::Logger::root().getLevel() < Poco::Message::PRIO_TRACE)
				return;
			Poco::Logger::root().trace(tsstr() + "[" + std::string(channel) + "] " + message, args...);
		}

		template <typename... Args>
		static void debug(const char* channel, const std::string& message, Args... args) {
			if (Poco::Logger::root().getLevel() < Poco::Message::PRIO_DEBUG)
				return;
			Poco::Logger::root().debug(tsstr() + "[" + std::string(channel) + "] " + message, args...);
		}

		template <typename... Args>
		static void info(const char* channel, const std::string& message, Args... args) {
			Poco::Logger::root().information(tsstr() + "[" + std::string(channel) + "] " + message, args...);
		}

		template <typename... Args>
		static void warning(const char* channel, const std::string& message, Args... args) {
			Poco::Logger::root().warning(tsstr() + "[" + std::string(channel) + "] " + message, args...);
		}

		template <typename... Args>
		static void error(const char* channel, const std::string& message, Args... args) {
			Poco::Logger::root().error(tsstr() + "[" + std::string(channel) + "] " + message, args...);
		}

		template <typename... Args>
		static void critical(const char* channel, const std::string& message, Args... args) {
			Poco::Logger::root().critical(tsstr() + "[" + std::string(channel) + "] " + message, args...);
		}
	};
} /// namespace pesa

#ifdef __WINDOWS__
	#define Inline			__forceinline
#else
	#define Inline			inline
#endif 


#define Trace 				Util::trace
#define Debug				Util::debug
#define Info 				Util::info
#define Warning				Util::warning
#define Error 				Util::error
#define Critical 			Util::critical

#define AUTO_LOCK(m)		std::lock_guard<std::mutex> __guard__(m);
#define AUTO_REC_LOCK(m)	std::lock_guard<std::recursive_mutex> __rec_guard__(m);

#ifdef __WINDOWS__
	#define EXPORT_FUNCTION extern "C" __declspec(dllexport)
#else
	#define EXPORT_FUNCTION extern "C"
#endif 


