/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// EigenPlugins.h
///
/// Created on: 20 Dec 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include <limits>

inline auto nanClear(float value = 0.0f) const { return (*this == *this).select(*this, value).eval(); } 
inline auto infClear(float value = 0.0f) const { return this->unaryExpr([value](float v) { return std::isfinite(v) ? v : value; }); }
inline auto nanMinClear() const { return this->nanClear((std::numeric_limits<float>::max)()).eval(); }
inline auto nanMaxClear() const { return this->nanClear((std::numeric_limits<float>::min)()).eval(); }
inline auto nanMinCoeff() const { return this->nanMinClear().minCoeff().eval(); }
inline auto nanMask(float value = 0.0f) const { return (*this == *this).select(*this / *this, value); }

inline auto filterPositive(const Array<float, -1, -1, RowMajor>& zero, float value = 0.0f) const { return (*this > zero).select(*this, value); }

inline auto filterNegative(const Array<float, -1, -1, RowMajor>& zero, float value = 0.0f) const { return (*this < zero).select(*this, value); }

inline auto positiveMask(const Array<float, -1, -1, RowMajor>& zero, const Array<float, -1, -1, RowMajor>& one) { return (*this > zero).select(one, 0.0f); }

inline auto negativeMask(const Array<float, -1, -1, RowMajor>& zero, const Array<float, -1, -1, RowMajor>& one) { return (*this < zero).select(one, 0.0f); }

inline auto nonZeroMask(const Array<float, -1, -1, RowMajor>& zero, const Array<float, -1, -1, RowMajor>& one) { return (*this != zero).select(one, 0.0f); }

inline auto nanColMin() const { return this->nanMinClear().colwise().minCoeff().eval(); }
inline auto nanColMax() const { return this->nanMaxClear().colwise().maxCoeff().eval(); }
inline auto colMin(int startIndex, int count) const { return this->colwise().minCoeff().eval(); }
inline auto colMax(int startIndex, int count) const { return this->colwise().minCoeff().eval(); }

//template <typename Type>
//inline auto nanSum() const {
//	Array<Type, -1, -1, RowMajor> result(1, this->cols());
//	auto& sum = result.row(0);
//
//	sum = this->row(0).nanClear();
//
//	for (int i = 1; i < this->rows(); i++) 
//		sum = (sum + this->row(i).nanClear()).eval();
//
//	return sum;
//}

template <typename Type>
inline auto nanCount() const {
	Array<Type, -1, -1, RowMajor> result(1, this->cols());
	auto& count = result.row(0);
	count = this->row(0).nanMask();
	for (int i = 1; i < this->rows(); i++) 
		count = (count + this->row(i).nanMask()).eval();
	return count;
}

template <typename Type>
inline auto nanMean() const {
	Array<Type, -1, -1, RowMajor> result(2, this->cols()); 

	result.row(0) = this->row(0).nanClear();
	result.row(1) = this->row(0).nanMask();

	for (int i = 1; i < this->rows(); i++) {
		result.row(0) += this->row(i).nanClear();
		result.row(1) += this->row(i).nanMask();
	}

	return (result.row(0) / result.row(1)).eval();
}

template <typename Type>
inline auto nanVariance(Array<Type, -1, -1, RowMajor>* mean = nullptr) const {
	Array<Type, -1, -1, RowMajor> result(3, this->cols());
	// auto& sum = result.row(0);
	// auto& count = result.row(1);
	// auto& sumOfSquares = result.row(2);

	result.row(0) = (this->row(0).nanClear()).eval();
	result.row(1) = (this->row(0).nanMask()).eval();
	result.row(2) = (result.row(0) * result.row(0)).eval();

	for (int i = 1; i < this->rows(); i++) {
		auto pt = this->row(i).nanClear();
		result.row(0) = (result.row(0) + pt).eval();
		result.row(1) = (result.row(1) + this->row(i).nanMask()).eval();
		result.row(2) = (result.row(2) + pt.square()).eval();
	}

	auto rcpCount = ((Type)1 / result.row(1)).eval();
	auto sumMean = (result.row(0) * rcpCount).eval();

	if (mean)
        mean->row(0) = (sumMean).eval();

	auto sumOfSquaresMean = (result.row(2) * rcpCount).eval();
	return (sumOfSquaresMean - (sumMean * sumMean).eval()).eval();
}

template <typename Type>
inline auto nanStdDev(Array<Type, -1, -1, RowMajor>* mean = nullptr) const {
    return (nanVariance<Type>(mean).sqrt()).eval();
}

template <typename Type, typename OtherDerived>
inline auto nanSkew(const Array<Type, -1, -1, RowMajor>& mean, const ArrayBase<OtherDerived>& sd) const {
	Array<Type, -1, -1, RowMajor> skew(1, this->cols());
	skew.setConstant(0.0f);

	/// find the sum of the cubed deviations, normalized by the sd. 

	/// we use a recurrence relation to stably update a running value so
	/// there aren't any large sums that can overflow 
	auto rcpSD = (Type)1 / sd;

	for (auto i = 0; i < this->rows(); i++) {
        auto nanMask = this->row(i).nanMask().eval();
		auto x = (((this->row(i).nanClear() - mean).eval() * rcpSD).eval() * nanMask).eval();
		skew = ((skew + (x.cube().eval() - skew).eval()).eval() / (Type)(i + 1)).eval();
	}

	return skew.eval();
}

template <typename Type>
inline auto nanSkew() const {
	Array<Type, -1, -1, RowMajor> mean(1, this->cols());
	auto variance = nanVariance(&mean);
	auto sd = variance.sqrt();
	return nanSkew(mean, sd);
}

template <typename Type, typename OtherDerived>
inline auto nanKurtosis(const Array<Type, -1, -1, RowMajor>& mean, const ArrayBase<OtherDerived>& sd) const {
	Array<Type, -1, -1, RowMajor> kurtosis(1, this->cols());
	kurtosis.setConstant(0.0f);

	/// find the sum of the cubed deviations, normalized by the sd. 

	/// we use a recurrence relation to stably update a running value so
	/// there aren't any large sums that can overflow 
	auto rcpSD = (Type)1 / sd;

	for (auto i = 0; i < this->rows(); i++) {
        auto nanMask = this->row(i).nanMask().eval();
		auto x = (((this->row(i).nanClear() - mean).eval() * rcpSD).eval() * nanMask).eval();
		kurtosis = (kurtosis + (((x.cube().eval() * x).eval() - kurtosis).eval()).eval() / (Type)(i + 1)).eval();
	}

	return kurtosis.eval();
}

template <typename Type>
inline auto nanKurtosis() const {
	Array<Type, -1, -1, RowMajor> mean(1, this->cols());
	auto variance = nanVariance(&mean);
	auto sd = variance.sqrt();
	return nanKurtosis(mean, sd);
}

template <typename Type>
inline auto nanMedianSorted() const {
	auto n = this->rows();
	auto lhs = (n - 1) / 2;
	auto rhs = n / 2;

	if (lhs == rhs)
		return this->row(lhs);

	//Array<Type, -1, -1, RowMajor> result(1, this->cols());
	//result.row(0) = (this->row(lhs) + this->row(rhs));
	return this->row(lhs);
}

//template <typename Type>
//inline auto nanMedianUnsorted() const {
//	return this->ascSort<Type>().nanMedianSorted<Type>();
//}
//
//template <typename Type>
//inline auto ascSort() const {
//	Array<Type, -1, -1, RowMajor> sorted = this->replicate(1, 1); /// (this->rows(), this->cols());
//	Array<Type, -1, -1, RowMajor> one(1, this->cols());
//
//	one.setConstant((Type)1);
//
//	/// Insertion sort
//	for (auto i = 1; i < this->rows(); i++) { 
//		auto x = sorted.row(i).nanMinClear();
//		auto j = i - 1;
//
//		while (j >= 0) {
//			auto curr = sorted.row(j).nanMinClear();
//			auto mask = (curr > x).select(one, 0);
//			auto imask = mask - one;
//			auto maskCount = mask.sum();
//
//			if (maskCount > 0) {
//				sorted.row(j + 1) = sorted.row(j + 1) * mask + sorted(j) * imask;
//				j = j - 1;
//			}
//			else
//				break;
//
//		}
//
//		sorted.row(j + 1) = sorted.row(i);
//		//auto prev = this->row(i - 1).nanMinClear();
//		//auto curr = this->row(i).nanMinClear();
//		//auto currMask = (curr > prev).select(one, 0);
//		//auto prevMask = currMask - one;
//		//sorted.row(i - 1) = prev * currMask + curr * prevMask;
//		//sorted.row(i) = prev * prevMask + curr * currMask;
//	}
//
//	return sorted;
//}
//
