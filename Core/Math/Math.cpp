/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#include "Math.h"

namespace pesa {

	// float Math::mean(const FVec& fvec) {
	// 	float sum = 0.0f;
	// 	size_t count = 0;

	// 	for (float f : fvec) {
	// 		if (!std::isnan(f)) {
	// 			sum += f;
	// 			count++;
	// 		}
	// 	}

	// 	return count ? sum / (float)count : 0.0f;
	// }

	// float Math::addPositive(const FVec& fvec) {
	// 	float sum = 0.0f;

	// 	for (float f : fvec) {
	// 		/// NaNs will return false
	// 		if (f > 0.0f)
	// 			sum += f;
	// 	}

	// 	return sum;
	// }

	// float Math::addNegative(const FVec& fvec) {
	// 	float sum = 0.0f;

	// 	for (float f : fvec) {
	// 		/// NaNs will return false
	// 		if (f < 0.0f)
	// 			sum += f;
	// 	}

	// 	return sum;
	// }

	// void Math::scalePositive(FVec& fvec, float scale) {
	// 	for (float& f : fvec) {
	// 		/// NaNs will return false
	// 		if (f > 0.0f)
	// 			f *= scale;
	// 	}
	// }

	// void Math::scaleNegative(FVec& fvec, float scale) {
	// 	for (float& f : fvec) {
	// 		/// NaNs will return false
	// 		if (f < 0.0f)
	// 			f *= scale;
	// 	}
	// }
}

