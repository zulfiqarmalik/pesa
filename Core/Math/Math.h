/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Math.h
///
/// Created on: 7 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#define USE_EIGEN

#define EIGEN_ARRAYBASE_PLUGIN "Core/Math/E_ArrayPlugins.h"

#include "Eigen/Dense"

#include "Core/CoreDefs.h"
#include <iostream>
#include <string>
#include <sstream>
#include <string>
#include <chrono>
#include <thread>
#include <memory>
#include <cmath>

#include "Core/Lib/Util.h"

namespace pesa {

	struct Math {
		/// The Euler constant
		const long double E       = 2.7182818284590452353602874713526625L;
		/// log_2(e)
		const long double LOG2E   = 1.4426950408889634073599246810018921L;
		/// log_10(e)
		const long double LOG10E  = 0.4342944819032518276511289189166051L;
		/// ln(2)
		const long double LN2     = 0.6931471805599453094172321214581766L;
		/// ln(10)
		const long double LN10    = 2.3025850929940456840179914546843642L;
		/// pi
		const long double PI      = 3.1415926535897932384626433832795029L;
		/// pi/2
		const long double PI_2    = 1.5707963267948966192313216916397514L;
		/// pi/4
		const long double PI_4    = 0.7853981633974483096156608458198757L;
		/// sqrt(2)
		const long double SQRT2   = 1.4142135623730950488016887242096981L;
		/// 1/sqrt(2)
		const long double SQRT1_2 = 0.7071067811865475244008443621048490L;

		///Round a value to its closest integer
		inline double round(double r) {
			return (r > 0.0) ? std::floor(r + 0.5) : std::ceil(r - 0.5);
		}
	};

	// class Core_API Math {
	// public:
	// 	typedef std::vector<float> 	FVec;

	// 	static float 				mean(const FVec& fvec);
	// 	static float 				addPositive(const FVec& fvec);
	// 	static float 				addNegative(const FVec& fvec);
	// 	static void 				scalePositive(FVec& fvec, float scale);
	// 	static void 				scaleNegative(FVec& fvec, float scale);
	// };

#ifdef USE_EIGEN
	typedef Eigen::Array<float, -1, -1, Eigen::RowMajor> EigenRowMatrixf;
	typedef Eigen::Block<EigenRowMatrixf, 1, -1, true> EigenBlockf;

	//////////////////////////////////////////////////////////////////////////
	/// This is pure EVIL. However, this is the beauty of consistent memory 
	/// layouts!
	/// Taken from: http://stackoverflow.com/questions/16925263/create-an-eigen-matrix-from-a-c-array
	//////////////////////////////////////////////////////////////////////////
	template<typename T> struct ShMemMat {
		T* data;
		Eigen::DenseIndex rows, cols;

		Eigen::Array<T, -1, -1, Eigen::RowMajor>* asMatrix() {
			return reinterpret_cast<Eigen::Array<T, -1, -1, Eigen::RowMajor>*>(this);
		}
	};

	template <typename t_type>
	class Matrix {
	private:
		typedef Eigen::Array<t_type, -1, -1, Eigen::RowMajor> TMatrix;
		typedef Matrix<t_type> 	MatrixType;
		TMatrix*				m_mat = nullptr;
		ShMemMat<t_type>*		m_shMat = nullptr;

	public:
		Matrix() {
		}

		Matrix(size_t rows, size_t cols) {
			m_mat = new TMatrix(rows, cols);
		}

		Matrix(char* shMemPtr, size_t rows, size_t cols) {
			this->fromSharedMemory(shMemPtr, rows, cols);
		}

		~Matrix() {
			clear();
		}

		inline void clear() {
			if (!m_shMat)
				delete m_mat;
			else
				delete m_shMat;

			m_mat = nullptr;
			m_shMat = nullptr;
		}

		MatrixType& operator = (const MatrixType& rhs) {
			if (rhs.m_shMat)
				m_shMat = rhs.m_shMat;
			else
				*m_mat = *rhs.m_mat;

			return *this;
		} 

		inline void fromSharedMemory(char* shMemPtr, size_t rows, size_t cols) { 
			clear();
			m_shMat = new ShMemMat<t_type>();
			m_shMat->data = reinterpret_cast<t_type*>(shMemPtr);
			m_shMat->rows = rows;
			m_shMat->cols = cols;
			m_mat = m_shMat->asMatrix();
		}

		inline const t_type& operator()(size_t i, size_t j) const {
			return (*m_mat)((int)i, (int)j);
		}

		inline t_type& operator()(size_t i, size_t j) {
			return (*m_mat)((int)i, (int)j);
		}

		inline const t_type& getValue(size_t i, size_t j) const {
			return (*m_mat)((int)i, (int)j);
		}

		inline t_type& getValue(size_t i, size_t j) {
			return (*m_mat)((int)i, (int)j);
		}

		inline void setMemory(t_type value) { m_mat->setConstant(value); }
		inline size_t rows() const { return m_mat->rows(); }
		inline size_t cols() const { return m_mat->cols(); }
		inline const t_type* data() const { return m_mat->data(); }
		inline t_type* data() { return m_mat->data(); }
		inline TMatrix& mat() { return *m_mat; }
		inline const TMatrix& mat() const { return *m_mat; }
		inline Eigen::Block<Eigen::Array<t_type, -1, -1, Eigen::RowMajor>, 1, -1, true > row(int di) { return mat().row(di); }
		inline const Eigen::Block<const Eigen::Array<t_type, -1, -1, Eigen::RowMajor>, 1, -1, true > row(int di) const { return mat().row(di); }
		inline Eigen::Block<Eigen::Array<t_type, -1, -1, Eigen::RowMajor>, 1, -1, true > operator[] (int di) { return row(di); }
		inline const Eigen::Block<const Eigen::Array<t_type, -1, -1, Eigen::RowMajor>, 1, -1, true > operator[] (int di) const { return row(di); }
	};

#else

	template <typename t_type>
	class Matrix {
	private:
		typedef Matrix<t_type> 				MatrixType;
		typedef std::shared_ptr<t_type> 	t_typePtr;

		t_typePtr 							m_data; 		/// The actual data that was allocated
		size_t								m_rows; 		/// Number of rows in the matrix
		size_t 								m_cols;			/// Number of colums in the matrix

	public:
		Matrix(size_t rows, size_t cols)
			: m_data(nullptr)
			, m_rows(0)
			, m_cols(0) {
			ensureSize(rows, cols, false);
		}

		Matrix(const MatrixType& rhs)
			: m_data(nullptr)
			, m_rows(0)
			, m_cols(0) {
			*this = rhs;
		}

		~Matrix() {
			m_data = nullptr;
			Trace("MATRIX", "\tFREE: %zx%z", m_rows, m_cols);
		}

		MatrixType& ensureSize(size_t rows, size_t cols, bool copyOld = false) {
			if (m_rows >= rows && m_cols >= cols)
				return *this;

			t_typePtr existingData 	= m_data;
			size_t existingRowCount = m_rows;
			size_t existingColCount = m_cols;

			t_type* dataPtr = new t_type [rows * cols];
			m_data = t_typePtr(dataPtr, SharedPtr_ArrayDeleter<t_type>());

			m_rows = rows;
			m_cols = cols;

			Trace("MATRIX", "\tAlloc: %zx%z", m_rows, m_cols);
			return *this;
		}

		MatrixType& operator = (const MatrixType& rhs) {
			m_data = rhs.m_data;
			m_rows = rhs.m_rows;
			m_cols = rhs.m_cols;
			return *this;
		}

		const t_type* operator[](int i) const {
			return m_data.get() + i * m_cols;
		}

		t_type* operator[](int i) {
			return m_data.get() + i * m_cols;
		}

		const t_type& operator()(size_t i, size_t j) const {
			return *(m_data.get() + i * m_cols + j);
		}

		t_type& operator()(size_t i, size_t j) {
			return *(m_data.get() + i * m_cols + j);
		}

		inline size_t rows() const {
			return m_rows;
		}

		inline size_t cols() const {
			return m_cols;
		}

		inline const t_type* data() const {
			return m_data.get();
		}

		inline t_type* data() {
			return m_data.get();
		}
	};

#endif /// USE_EIGEN
}


