/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#include "Stats.h"
#include <algorithm>
#include <functional>
#include <cmath>

namespace pesa {

	/// Compute the arithmetic mean of a dataset using the recurrence relation 
	/// mean_(n) = mean(n-1) + (data[n] - mean(n-1))/(n+1) 
	float Stats::meanRR(const float* data, size_t n) {
		float mean = 0.0f;

		for (size_t i = 0; i < n; i++) {
			if (!std::isnan(data[i]))
				mean += (data[i] - mean) / (float)(i + 1);
		}

		return mean;
	}

	float Stats::mean(const float* data, size_t n) {
		float sum = 0.0f;
		size_t count = 0;

		for (size_t i = 0; i < n; i++) {
			if (!std::isnan(data[i])) {
				sum += data[i];
				count++;
			}
		}

		if (count)
			return sum / (float)count;

		return 0.0f;
	}

	std::vector<float> Stats::sort(const float* input, size_t n) {
		std::vector<float> output(n);
		sort(input, &output[0], n);
		return output;
	}

	void Stats::sort(const float* input, float* output, size_t n) {
		//if (output != input)
		//	memcpy(output, input, n * sizeof(float));

		/// Copy the real numbers only
		size_t on = 0;
		for (size_t i = 0; i < n; i++) {
			if (!std::isnan(input[i]))
				output[on++] = input[i];
		}

		std::sort(output, output + on);

		// the rest were NaNs
		for (size_t i = on; i < n; i++)
			output[i] = std::nanf("0");
	}

	float Stats::medianSorted(const float* sortedData, size_t n) {
		const size_t lhs = (n - 1) / 2;
		const size_t rhs = n / 2;

		if (n == 0)
			return 0.0f;

		if (lhs == rhs)
			return sortedData[lhs];

		return (sortedData[lhs] + sortedData[rhs]) / 2.0f;
	}

	std::vector<float> Stats::copyValid(const float* data, size_t n) {
		std::vector<float> valid;
		valid.reserve(n);

		for (size_t i = 0; i < n; i++) {
			float value = data[i];
			if (!std::isnan(value))
				valid.push_back(value);
		}

		return valid;
	}

	float Stats::medianUnsorted(const float* unsortedData, size_t n) {
		std::vector<float> validData = copyValid(unsortedData, n);
		std::vector<float> sortedData(validData.size());

		if (!validData.size())
			return std::nanf("");

		sort(&validData[0], &sortedData[0], validData.size());
		return medianSorted(&sortedData[0], sortedData.size());
	}

	float Stats::variance(const float* data, size_t n) {
		return variance(data, n, Stats::mean(data, n));
	}

	float Stats::variance(const float* data, size_t n, float mean) {
		float var = 0.0f;
		size_t count = 0;

		// find the sum of the squares 
		for (size_t i = 0; i < n; i++) {
			if (!std::isnan(data[i])) {
				float delta = (data[i] - mean);
				var += (delta * delta);
				count++;
			}
		}

		return var / (float)count;
	}

	float Stats::standardDeviation(const float* data, size_t n) {
		return sqrt(variance(data, n));
	}

	float Stats::standardDeviation(const float* data, size_t n, float mean) {
		return sqrt(variance(data, n, mean));
	}

	float Stats::skew(const float* data, size_t n) {
		return skew(data, n, mean(data, n));
	}

	float Stats::skew(const float* data, size_t n, float mean) {
		return skew(data, n, mean, standardDeviation(data, n, mean));
	}

	float Stats::skew(const float* data, size_t n, float mean, float sd) {
		float skew = 0;

		/// find the sum of the cubed deviations, normalized by the sd. 

		/// we use a recurrence relation to stably update a running value so
		/// there aren't any large sums that can overflow 

		for (size_t i = 0; i < n; i++) {
			if (!std::isnan(data[i])) {
				float x = (data[i] - mean) / sd;
				skew += ((x * x * x) - skew) / (i + 1);
			}
		}

		return skew;
	}

	float Stats::kurtosis(const float* data, size_t n) {
		return kurtosis(data, n, mean(data, n));
	}

	float Stats::kurtosis(const float* data, size_t n, float mean) {
		return kurtosis(data, n, mean, standardDeviation(data, n, mean));
	}

	float Stats::kurtosis(const float* data, size_t n, float mean, float sd) {
		float avg = 0, kurtosis;

		/// find the fourth moment the deviations, normalized by the sd 

		/// we use a recurrence relation to stably update a running value so
		/// there aren't any large sums that can overflow 

		for (size_t i = 0; i < n; i++) {
			if (!std::isnan(data[i])) {
				const float x = (data[i] - mean) / sd;
				avg += ((x * x * x * x) - avg) / (i + 1);
			}
		}

		kurtosis = avg - 3.0f;  /// makes kurtosis zero for a Gaussian 

		return kurtosis;
	}

	float Stats::weightedAbsDeviation(const float* data, size_t n, const float* w) {
		return weightedAbsDeviation(data, n, w, mean(w, n));
	}

	/// Taken from GSL
	float Stats::weightedAbsDeviation(const float* data, size_t n, const float* w, float wmean) {
		float wabsdev = 0;
		float W = 0;

		/* find the sum of the absolute deviations */
		for (size_t i = 0; i < n; i++) {
			float wi = w[i];

			if (wi > 0 && !std::isnan(data[i])) {
				const float delta = fabs(data[i] - wmean);
				W += wi;
				wabsdev += (delta - wabsdev) * (wi / W);
			}
		}

		return wabsdev;
	}

	float Stats::lag1AutoCorrelation(const float* data, size_t n) {
		return lag1AutoCorrelation(data, n, mean(data, n));
	}

	float Stats::lag1AutoCorrelation(const float* data, size_t n, float mean) {
		float r1;
		float q = 0;
		float v = (data[0] - mean) * (data[0] - mean);

		for (size_t i = 1; i < n; i++) {
			const float delta0 = (data[(i - 1)] - mean);
			const float delta1 = (data[i] - mean);
			q += (delta0 * delta1 - q) / (i + 1);
			v += (delta1 * delta1 - v) / (i + 1);
		}

		r1 = q / v;

		return r1;
	}

	float Stats::covariance(const float* data1, const float* data2, size_t n) {
		return covariance(data1, data2, n, mean(data1, n), mean(data2, n));
	}

	float Stats::covariance(const float* data1, const float* data2, size_t n, float mean1, float mean2) {
		float covariance = 0;

		size_t i;

		/* find the sum of the squares */
		for (i = 0; i < n; i++) {
			if (!std::isnan(data1[i]) && !std::isnan(data2[i])) {
				const float delta1 = (data1[i] - mean1);
				const float delta2 = (data2[i] - mean2);
				covariance += (delta1 * delta2 - covariance) / (i + 1);
			}
		}

		return covariance;
	}

	float Stats::quantile(const float* sortedData, size_t n, float f) {
		const float index = f * (n - 1);
		const size_t lhs = (int)index;
		const float delta = index - lhs;

		if (n == 0)
			return 0.0;

		if (lhs == n - 1)
			return sortedData[lhs];
		
		return (1.0f - delta) * sortedData[lhs] + delta * sortedData[(lhs + 1)];
	}

	bool Stats::linearFit(const float* x, const float* y, size_t n, float* c0, float* c1, float* cov_00, float* cov_01, float* cov_11, float* sumsq) {
		float m_x = 0, m_y = 0, m_dx2 = 0, m_dxdy = 0;

		for (size_t i = 0; i < n; i++) {
			m_x += (x[i] - m_x) / (i + 1.0f);
			m_y += (y[i] - m_y) / (i + 1.0f);
		}

		for (size_t i = 0; i < n; i++) {
			const float dx = x[i] - m_x;
			const float dy = y[i] - m_y;

			m_dx2 += (dx * dx - m_dx2) / (i + 1.0f);
			m_dxdy += (dx * dy - m_dxdy) / (i + 1.0f);
		}

		/* In terms of y = a + b x */

		{
			float s2 = 0, d2 = 0;
			float b = m_dxdy / m_dx2;
			float a = m_y - m_x * b;

			*c0 = a;
			*c1 = b;

			/* Compute chi^2 = \sum (y_i - (a + b * x_i))^2 */

			for (size_t i = 0; i < n; i++) {
				const float dx = x[i] - m_x;
				const float dy = y[i] - m_y;
				const float d = dy - b * dx;
				d2 += d * d;
			}

			s2 = d2 / (n - 2.0f);        /* chisq per degree of freedom */

			*cov_00 = s2 * (1.0f / n) * (1.0f + m_x * m_x / m_dx2);
			*cov_11 = s2 * 1.0f / (n * m_dx2);

			*cov_01 = s2 * (-m_x) / (n * m_dx2);

			*sumsq = d2;
		}

		return true;
	}

	//////////////////////////////////////////////////////
	/// Miscellaneous utility functions
	//////////////////////////////////////////////////////
	float Stats::addPositive(const float* data, size_t n) {
		float sum = 0.0f;

		for (size_t i = 0; i < n; i++) {
			float f = data[i];
			/// NaNs will return false
			if (f > 0.0f)
				sum += f;
		}

		return sum;
	}

	float Stats::addNegative(const float* data, size_t n) {
		float sum = 0.0f;

		for (size_t i = 0; i < n; i++) {
			float f = data[i];
			/// NaNs will return false
			if (f < 0.0f)
				sum += f;
		}

		return sum;
	}

	float Stats::absSum(const float* data, size_t n) {
		float sum = 0.0f;

		for (size_t i = 0; i < n; i++) {
			float f = data[i];
			/// NaNs will return false
			if (f > 0.0f)
				sum += f;
			else if (f < 0.0f)
				sum -= f;
		}

		return sum;
	}

	inline float getFirstValidValue(const float* data, size_t n, size_t& index) {
		if (!n)
			return std::nanf("");

		index = 0;

		while (index < n) {
			float value = data[index];

			if (!std::isnan(value))
				return value;

			index++;
		}

		return std::nanf("");
	}

	float Stats::min(const float* data, size_t n) {
		if (!n)
			return std::nanf("");

		size_t index = 0;
		float value = getFirstValidValue(data, n, index);

		if (std::isnan(value))
			return value;

		for (size_t i = index; i < n; i++) 
			value = std::min(value, data[i]);

		return value;
	}

	float Stats::max(const float* data, size_t n) {
		if (!n)
			return std::nanf("");

		size_t index = 0;
		float value = getFirstValidValue(data, n, index);

		if (std::isnan(value))
			return value;

		for (size_t i = index; i < n; i++)
			value = std::max(value, data[i]);

		return value;
	}

	void Stats::scale(float* data, size_t n, float scale) {
		for (size_t i = 0; i < n; i++) {
			float& f = data[i];
			f *= scale;
		}
	}

	void Stats::invScale(float* data, size_t n, float scale) {
		for (size_t i = 0; i < n; i++) {
			float& f = data[i];
			f /= scale;
		}
	}

	void Stats::scalePositive(float* data, size_t n, float scale) {
		for (size_t i = 0; i < n; i++) {
			float& f = data[i];
			/// NaNs will return false
			if (f > 0.0f)
				f *= scale;
		}
	}

	void Stats::scaleNegative(float* data, size_t n, float scale) {
		for (size_t i = 0; i < n; i++) {
			float& f = data[i];
			/// NaNs will return false
			if (f < 0.0f)
				f *= scale;
		}
	}

	void Stats::neutralise(float* data, size_t n) {
		float mean = Stats::mean(data, n);

		for (size_t i = 0; i < n; i++) {
			if (!std::isnan(data[i])) {
				float& f = data[i];
				f -= mean;
			}
		}
	}

	void Stats::normalise(float* data, size_t n) {
		float mean = Stats::mean(data, n);

		for (size_t i = 0; i < n; i++) {
			if (!std::isnan(data[i])) {
				float& f = data[i];
				f -= mean;
			}
		}
	}

	float Stats::iterativeAverage(float point, float currentAverage, size_t n) {
		float fn = (float)n;
		return iterativeAverage(point, currentAverage, fn, 1.0f / fn);
	}

	float Stats::iterativeAverage(float point, float currentAverage, float n, float inverseN) {
		//////////////////////////////////////////////////////////////////////////
		/// Iteratively calculating average (avgLongs and avgShorts for instance)
		/// AVG(a) = a / 1
		/// AVG(a, b) = (AVG(a) * 1 + b) / 2
		/// AVG(a, b, c) = (AVG(a, b) * 2 + c) / 3
		/// AVG(a, b, c, c) = (AVG(a, b, c) * 3 + d) / 4
		/// AVG(a, b, c, ... n) = (AVG(a, b, ... n - 1) * (n - 1) + n) / n
		//////////////////////////////////////////////////////////////////////////
		return (currentAverage * (n - 1.0f) + point) * inverseN;
	}

	size_t Stats::positiveCount(const float* data, size_t n) {
		size_t count = 0;

		for (size_t i = 0; i < n; i++) {
			if (data[i] > 0.0f)
				count++;
		}

		return count;
	}

	size_t Stats::negativeCount(const float* data, size_t n) {
		size_t count = 0;

		for (size_t i = 0; i < n; i++) {
			if (data[i] < 0.0f)
				count++;
		}

		return count;
	}

	Eigen::VectorXf	Stats::neutralise(const Eigen::VectorXf& v0, const Eigen::VectorXf& v1) {
		/// Using single level Gram-Schmidt (https://en.wikipedia.org/wiki/Gram%E2%80%93Schmidt_process)
		/// So basically the new vector is v1 - proj_v1_u0 (projection of v1 onto u0)
		/// where:
		///		u0 = norm(v0)
		///		proj_v1_u0 = (projection of v1 onto u0) = u0 * v1.dot(v0)
		//auto u0 = v0;
		//u0.normalize();
		//return (v1 - (u0 * v1.dot(v0)));
		auto proj_v1_v0 = (v0.dot(v1) / v0.dot(v0)) * v0;
		return v1 - proj_v1_v0;
	}
		
	void Stats::neutralise(EigenRowMatrixf& alpha, const EigenRowMatrixf& factor, int di, float factorPercent /* = 100.0f */, bool useResidual /* = false */) {
		size_t numCols = alpha.cols();
		Eigen::VectorXf v0(numCols), v1(numCols);
		std::vector<size_t> validIndexes;

		for (size_t ii = 0; ii < numCols; ii++) {
			float value = alpha(0, ii);
			float f = factor(di, ii);

			if (!std::isnan(value) && !std::isnan(f)) {
				validIndexes.push_back(ii);
				float scaledFactor = f * factorPercent * 0.01f;
				if (useResidual) 
					v0[ii] = (1.0f - 1.0f / scaledFactor);
				else
					v0[ii] = value * scaledFactor;
				v1[ii] = value;
			}
			else {
				v0[ii] = 0.0f;
				v1[ii] = 0.0f;
				alpha(0, ii) = std::nanf("");
			}
		}

		float mv0 = std::sqrt(v0.dot(v0));
		float mv1 = std::sqrt(v1.dot(v1));

		Eigen::VectorXf v1New = neutralise(v0, v1);
		for (size_t ii : validIndexes)
			alpha(0, ii) = v1New[ii];

		auto odp = v1.dot(v0);
		auto ndp = v1New.dot(v0);
		//ASSERT(std::abs(ndp) <= 0.01f, "Vector is not sufficiently normal. Old DP: " << odp << " - New DP: " << ndp);
	}

	float Stats::largest(float baseValue, const float* data, size_t n) {
		if (!n)
			return baseValue;

		float value = data[0];
		for (size_t i = 1; i < n; i++) {
			if (data[i] > value)
				value = data[i];
		}

		return std::max(baseValue, value);
	}

	float Stats::largest(const float* data, size_t n) {
		if (!n)
			return std::nanf("");
		
		float value = data[0];
		for (size_t i = 1; i < n; i++) {
			if (data[i] > value)
				value = data[i];
		}

		return value;
	}

	float Stats::smallest(float baseValue, const float* data, size_t n) {
		if (!n)
			return baseValue;

		float value = data[0];
		for (size_t i = 1; i < n; i++) {
			if (data[i] < value)
				value = data[i];
		}

		return std::min(baseValue, value);
	}

	float Stats::smallest(const float* data, size_t n) {
		if (!n)
			return std::nanf("");
		
		float value = data[0];
		for (size_t i = 1; i < n; i++) {
			if (data[i] < value)
				value = data[i];
		}

		return value;
	}

}

