/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Stats.h
///
/// Created on: 28 Jun 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Math.h"

namespace pesa {

	struct Core_API Stats {
									/// Compute the arithmetic mean of a dataset using the recurrence relation 
									/// mean_(n) = mean(n-1) + (data[n] - mean(n-1))/(n+1) 
		static float 				meanRR(const float* data, size_t n);

		static float 				mean(const float* data, size_t n);

		static void 				sort(const float* input, float* output, size_t n);
		static std::vector<float>	sort(const float* input, size_t n);

		static float 				medianSorted(const float* sortedData, size_t n);
		static float				medianUnsorted(const float* sortedData, size_t n);

		static float 				variance(const float* data, size_t n);
		static float 				variance(const float* data, size_t n, float mean);

		static float 				standardDeviation(const float* data, size_t n);
		static float 				standardDeviation(const float* data, size_t n, float mean);

		static float 				skew(const float* data, size_t n);
		static float 				skew(const float* data, size_t n, float mean);
		static float 				skew(const float* data, size_t n, float mean, float sd);

		static float 				kurtosis(const float* data, size_t n);
		static float 				kurtosis(const float* data, size_t n, float mean);
		static float 				kurtosis(const float* data, size_t n, float mean, float sd);

		static float 				weightedAbsDeviation(const float* data, size_t n, const float* weights);
		static float 				weightedAbsDeviation(const float* data, size_t n, const float* weights, float wmean);

		static float 				lag1AutoCorrelation(const float* data, size_t n);
		static float 				lag1AutoCorrelation(const float* data, size_t n, float mean);

		static float 				covariance(const float* data1, const float* data2, size_t n);
		static float 				covariance(const float* data1, const float* data2, size_t n, float mean1, float mean2);

		static float 				quantile(const float* sortedData, size_t n, float f);

		static bool 				linearFit(const float* x, const float* y, size_t n, float* c0, float* c1, float* cov_00, float* cov_01, float* cov_11, float* sumsq);

		//////////////////////////////////////////////////////
		/// Miscellaneous utility functions
		//////////////////////////////////////////////////////
		static std::vector<float>	copyValid(const float* data, size_t n);
		static float				min(const float* data, size_t n);
		static float				max(const float* data, size_t n); 
		static float				absSum(const float* data, size_t n);
		static float 				addPositive(const float* data, size_t n);
		static float 				addNegative(const float* data, size_t n);
		static void 				scalePositive(float* data, size_t n, float scale);
		static void 				scaleNegative(float* data, size_t n, float scale);
		static void					scale(float* data, size_t n, float scale);
		static void					invScale(float* data, size_t n, float scale);
		static void 				neutralise(float* data, size_t n);
		static void 				normalise(float* data, size_t n);
		static float				iterativeAverage(float point, float currentAverage, size_t n);
		static float				iterativeAverage(float point, float currentAverage, float n, float inverseN);
		static size_t				positiveCount(const float* data, size_t n);
		static size_t				negativeCount(const float* data, size_t n);

		static float 				largest(float baseValue, const float* data, size_t n);
		static inline float 		largest(float baseValue, const std::vector<float>& data) { return !data.size() ? baseValue : largest(baseValue, &data[0], data.size()); }
		static float 				largest(const float* data, size_t n);
		static inline float 		largest(const std::vector<float>& data) { return !data.size() ? std::nanf("") : largest(&data[0], data.size()); }
		
		static float 				smallest(float baseValue, const float* data, size_t n);
		static inline float 		smallest(float baseValue, const std::vector<float>& data) { return !data.size() ? baseValue : smallest(baseValue, &data[0], data.size()); }
		static float 				smallest(const float* data, size_t n);
		static inline float 		smallest(const std::vector<float>& data) { return !data.size() ? std::nanf("") : smallest(&data[0], data.size()); }
		

		static Eigen::VectorXf		neutralise(const Eigen::VectorXf& v0, const Eigen::VectorXf& v1);
		static void 				neutralise(EigenRowMatrixf& alpha, const EigenRowMatrixf& factor, int di, float factorPercent = 100.0f, bool useResidual = false);
	};
}

