/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// App.cpp
///
/// Created on: 31 Mar 2016
///     Author: zulfiqar
//////////////////////////////////////////////////////

#include "./App.h"
#include "Poco/SplitterChannel.h"
#include "Poco/ConsoleChannel.h"
#include "Poco/SimpleFileChannel.h"
#include "Poco/AutoPtr.h"
#include "Poco/File.h"
#include "Poco/String.h"

#include "Framework/Config/Config.h"
#include "Framework/Helper/ShLib.h"

#include "Framework/App/Simulator/Simulator.h"

#include "Framework/Data/MatrixData.h"
#include "Framework/Data/VarMatrixData.h"
#include "Framework/Framework.h"
#include "Core/IO/BasicStream.h"

#include <stdio.h>
#include <sstream>
#include <iostream>
#include <string>
#include <exception>
#include <inttypes.h>
#include <cstdlib>
#include <signal.h>

#include <regex>

#include "Core/Lib/Profiler.h"

#ifndef __WINDOWS__
/// Taken from: https://gist.github.com/aspyct/3462238

#include <signal.h> // sigaction(), sigsuspend(), sig*()
#include <unistd.h> // alarm()

void doSleep(int seconds);

	void handle_SIGNAL(int signal) {
		const char *signal_name;
		sigset_t pending;

		// Find out which signal we're handling
		switch (signal) {
		case SIGHUP:
			signal_name = "SIGHUP";
			break;
		case SIGUSR1:
			signal_name = "SIGUSR1";
			break;
		case SIGINT:
			printf("Caught SIGINT, exiting now\n");
			exit(0);
		default:
			fprintf(stderr, "Caught wrong signal: %d\n", signal);
			return;
		}

		/*
		* Please note that printf et al. are NOT safe to use in signal handlers.
		* Look for async safe functions.
		*/
		printf("Caught %s, sleeping for ~3 seconds\n"
			"Try sending another SIGHUP / SIGINT / SIGALRM "
			"(or more) meanwhile\n", signal_name);
		/*
		* Indeed, all signals are blocked during this handler
		* But, at least on OSX, if you send 2 other SIGHUP,
		* only one will be delivered: signals are not queued
		* However, if you send HUP, INT, HUP,
		* you'll see that both INT and HUP are queued
		* Even more, on my system, HUP has priority over INT
		*/
		doSleep(3);
		printf("Done sleeping for %s\n", signal_name);

		// So what did you send me while I was asleep?
		sigpending(&pending);
		if (sigismember(&pending, SIGHUP)) {
			printf("A SIGHUP is waiting\n");
		}

		if (sigismember(&pending, SIGUSR1)) {
			printf("A SIGUSR1 is waiting\n");
		}

		printf("Done handling %s\n\n", signal_name);
	}

	void handle_SIGALRM(int signal) {
		if (signal != SIGALRM) {
			fprintf(stderr, "Caught wrong signal: %d\n", signal);
		}

		printf("Got sigalrm, doSleep() will end\n");
	}

	void doSleep(int seconds) {
		struct sigaction sa;
		sigset_t mask;

		sa.sa_handler = &handle_SIGALRM; // Intercept and ignore SIGALRM
		sa.sa_flags = SA_RESETHAND; // Remove the handler after first signal
		sigfillset(&sa.sa_mask);
		sigaction(SIGALRM, &sa, NULL);

		// Get the current signal mask
		sigprocmask(0, NULL, &mask);

		// Unblock SIGALRM
		sigdelset(&mask, SIGALRM);

		// Wait with this mask
		alarm(seconds);
		sigsuspend(&mask);

		printf("sigsuspend() returned\n");
	}

	int setupSignals() {
		struct sigaction sa;

		// Print pid, so that we can send signals from other shells
		printf("My pid is: %d\n", getpid());

		// Setup the sighub handler
		sa.sa_handler = &handle_SIGNAL;

		// Restart the system call, if at all possible
		sa.sa_flags = SA_RESTART;

		// Block every signal during the handler
		sigfillset(&sa.sa_mask);

		// Intercept SIGHUP and SIGINT
		if (sigaction(SIGHUP, &sa, NULL) == -1) {
			perror("Error: cannot handle SIGHUP"); // Should not happen
		}

		if (sigaction(SIGUSR1, &sa, NULL) == -1) {
			perror("Error: cannot handle SIGUSR1"); // Should not happen
		}

		if (sigaction(SIGINT, &sa, NULL) == -1) {
			perror("Error: cannot handle SIGINT"); // Should not happen
		}

		return 0;
	}
#else
#include <windows.h>
BOOL WINAPI consoleHandler(DWORD signal) {

	if (signal == CTRL_C_EVENT) {
		std::cerr << "Ctrl-C handled" << std::endl; // do cleanup
		std::exit(1);
	}

	return TRUE;
}
int setupSignals() { 
	if (!SetConsoleCtrlHandler(consoleHandler, TRUE)) {
		std::cerr << "ERROR: Could not set control handler" << std::endl;
		std::exit(1);
		return 1;
	}

	return 0;
}
#endif /// !__WINDOWS__

namespace pesa {

	using namespace Poco::Util;

	//class logger final : public mongocxx::logger {
	//public:
	//	explicit logger(std::ostream* stream) : _stream(stream) {
	//	}

	//	void operator()(mongocxx::log_level level, mongocxx::stdx::string_view domain,
	//		mongocxx::stdx::string_view message) noexcept override {
	//		if (level >= mongocxx::log_level::k_trace) return;
	//		*_stream << '[' << mongocxx::to_string(level) << '@' << domain << "] " << message << '\n';
	//	}

	//private:
	//	std::ostream* const _stream;
	//};

	using namespace Poco;

	App::App()
		: m_args(*this) {
	}

	App::~App() {
		delete[] m_pyArgv;
	}

	void App::defineOptions(OptionSet& options) {
		Application::defineOptions(options);
		m_args.define(options);
	}

	static const std::string g_passKey = "TAsdg1wyfd0ioarhibhayj2-08jh";

	void App::initAppOptions() {
		AppOptions&	options		= m_appOptions;

		options.updateDataOnly	= m_args.dataOnly();
		options.dumpData		= m_args.dumpData();
		options.dumpFilename	= m_args.logFilename();
		options.limit			= m_args.limit();

		options.dbPath			= m_args.dbPath();
		options.dbName			= m_args.dbName();

		options.logLevel		= (Poco::Message::Priority)m_args.translateLogLevel();
		options.numThreads		= m_args.numThreads();
		options.maxChildProcesses = m_args.maxChildProcesses();

		options.runDiagnostics	= m_args.runDiagnostics();
		options.headerOnly		= m_args.headerOnly();
		options.force			= m_args.force();

		options.simInstanceName	= m_args.simInstanceName();

		options.waitForTimestamp= m_args.waitForTimestamp();
		options.waitInterval	= m_args.waitInterval();

		options.quiet			= m_args.quiet();
		options.jobId			= m_args.jobId();

		options.disableOptimisation = m_args.disableOptimisation();
		options.noDownload		= m_args.noDownload();

		options.days			= m_args.days();

		options.noRunChildStrats= m_args.noRunChildStrats();
		options.forceLocates	= m_args.forceLocates();
		options.runCmds			= m_args.runCmds();
		options.useShMem		= m_args.useShMem();
		options.useComboCheckpoint = m_args.useComboCheckpoint();

		options.ovrStartDate	= m_args.ovrStartDate();
		options.ovrEndDate		= m_args.ovrEndDate();

		options.clampEndDate	= m_args.clampEndDate();

		options.configOverrides = m_args.configOverrides();
        options.runServer       = m_args.runServer();

		options.onlyValid		= m_args.onlyValid();
		options.onlyInvalid		= m_args.onlyInvalid();

		options.ignoreDefault	= m_args.ignoreDefault();
		options.defaultValues	= m_args.defaultValues();

		const auto& updateUuids = m_args.updateUuids();
		if (!updateUuids.empty()) 
			options.uuids		= Util::split(updateUuids, ",");

		const auto& macros		= m_args.macros();

		if (!macros.empty()) {
			FrameworkUtil::parseMappings(macros, &options.userMacros, nullptr, ";");
		}

		/// OK now that we have the username and password data
		std::string username	= m_args.username();
		std::string password	= m_args.password();

		//Poco::Path path(Poco::Path::home());
		//path.setFileName("creds");

		//auto filename			= path.toString();

		//if (!username.empty() && !password.empty()) {
		//	auto writeStream	= io::BasicOutputStream::createBinaryWriter(filename);
		//	auto epassword		= password; ///Util::XOR(password, g_passKey);

		//	writeStream->write("username", username);
		//	writeStream->write("password", epassword);
		//	writeStream			= nullptr;
		//}
		//else {
		//	auto readStream		= io::BasicInputStream::createBinaryReader(filename);
		//	if (readStream) {
		//		std::string epassword;
		//		readStream->read("username", &username);
		//		readStream->read("password", &epassword);
		//		password		= epassword; ///Util::XOR(epassword, g_passKey);
		//		readStream		= nullptr;
		//	}
		//	else {
		//		Warning(m_appOptions.simInstanceName.c_str(), "No username and/or password was given or was stored in the creds file. The simulation might fail if the data needs to be updated!");
		//	}
		//}

		options.username		= username;
		options.password		= password;
	}

	AppOptions App::appOptions() const {
		return m_appOptions;
	}

	int App::runSim(const std::string& configFilename) {
		Simulator sim(m_appOptions);
		m_simulator = &sim; 

		int errCode = sim.run(configFilename);

		m_simulator = nullptr;
		return errCode;
	}

	void App::exit(int code) {
		if (m_simulator) 
			m_simulator->exit(code);
		std::exit(code);
	}

	void App::abort() {
		if (m_simulator) 
			m_simulator->abort();

#ifdef __WINDOWS__
		std::exit(1); /// Stupid Debug dialog is a nuisance. Can't be arsed to teach every dev on how to disable it
#else
		std::abort();
#endif /// __WINDOWS__
	}

	template <typename Type>
	inline void ascSort(Eigen::Array<Type, 3, 3, Eigen::RowMajor>& sorted) {
		Eigen::Array<Type, -1, -1, Eigen::RowMajor> one(1, sorted.cols());

		one.setConstant((Type)1);

		/// Insertion sort
		for (auto i = 1; i < sorted.rows(); i++) {
			auto x = sorted.row(i).replicate(1, 1);
			std::cout << x << std::endl;
			auto j = i - 1;

			while (j >= 0) {
				auto curr = sorted.row(j);
				auto mask = (curr > x).select(one, 0);
				auto imask = one - mask;
				auto maskCount = mask.sum();

				std::cout << "Mask: " << mask << std::endl;
				std::cout << "IMask: " << imask << std::endl;

				if (maskCount > 0) {
					std::cout << "j: " << sorted.row(j) << std::endl;
					std::cout << "j + 1: " << sorted.row(j + 1) << std::endl;
					auto oldValue = sorted.row(j + 1).replicate(1, 1);
					auto v0 = sorted.row(j) * mask.row(0);
					auto v1 = sorted.row(j + 1) * imask.row(0);
					std::cout << "v0: " << v0 << std::endl;
					std::cout << "v1: " << v1 << std::endl;
					std::cout << "v1': " << (oldValue * imask.row(0)) << std::endl;

					auto newJP1 = v0 + v1;
					std::cout << "newJP1: " << newJP1 << std::endl;
					sorted.row(j + 1) = newJP1;
					std::cout << sorted << std::endl;
					j = j - 1;
				}
				else
					break;

			}

			std::cout << x << std::endl;
			sorted.row(j + 1) = x;
		}
	}

	int App::run() {
		try {
			PROFILE_SCOPED();

            Eigen::initParallel();

            setupSignals();

			//Eigen::Array<float, -1, -1, Eigen::RowMajor> mat(9, 9);
			//for (int i = 0; i < 9; i++) {
			//	mat(0, i) = 1;
			//	mat(1, i) = 5;
			//	mat(2, i) = 3;
			//	mat(3, i) = 19;
			//	mat(4, i) = 2;
			//	mat(5, i) = 16;
			//	mat(6, i) = 6;
			//	mat(7, i) = 19;
			//	mat(8, i) = 4;
			//	//mat <<	1, 5, 3,
			//	//		19, 2, 16,
			//	//		6, 19, 4	
			//	//	;
			//}

			//std::cout << "MAT: " << mat << std::endl;
			//std::cout << "STDDEV: " << mat.nanStdDev<float>().eval() << std::endl;

			//ascSort<float>(mat);
			//std::cout << mat << std::endl;

			//auto nr = (mat.row(0) * std::nanf(""));
			//auto zr = (mat.row(0) * 0.0f);
			//std::cout << mat.nanMinClear().colwise().minCoeff() << std::endl;
			//std::cout << mat.nanMaxClear().colwise().maxCoeff() << std::endl;
			//for (size_t ii = 0; ii < mat.cols(); ii++) {
			//	std::cout << "Col wise mean: " << mat.colwise(ii).mean();
			//}

			//std::cout << mat << std::endl;
			//std::cout << tmat << std::endl;

			//float* matData = mat.data();
			//float* tmatData = tmat.data();

			if (m_args.helpRequested()) {
				m_args.showHelp();
				return Application::EXIT_USAGE;
			}

			if (m_args.printVersion()) {
				Info(m_appOptions.simInstanceName.c_str(), "PSim Version: %s", version());
				return Application::EXIT_USAGE;
			}

			initAppOptions();

			/// remove the old log file first ...
			Poco::DateTime now;
			std::string year = Util::cast(now.year());
			std::string month = Util::castPadded(now.month(), 2);
			std::string day = Util::castPadded(now.day(), 2);
			std::string hour = Util::castPadded(now.hour(), 2);
			std::string minute = Util::castPadded(now.minute(), 2);
			std::string second = Util::castPadded(now.second(), 2);

			std::string logFilename = m_args.logFilename();

			logFilename = Poco::replace(logFilename, "$YYYY", year.c_str());
			logFilename = Poco::replace(logFilename, "$MM", month.c_str());
			logFilename = Poco::replace(logFilename, "$DD", day.c_str());
			logFilename = Poco::replace(logFilename, "$HH", hour.c_str());
			logFilename = Poco::replace(logFilename, "$mm", minute.c_str());
			logFilename = Poco::replace(logFilename, "$ss", second.c_str());

			Poco::Path logPath(logFilename);
			Util::ensureDirExists(logPath.makeAbsolute());

			if (!logFilename.empty()) {
				Poco::File file(logFilename);
				if (file.exists())
					file.remove();
			}

			m_appOptions.logFilename = logFilename;
			Info(m_appOptions.simInstanceName.c_str(), "Logging to file: %s", logFilename);

			AutoPtr<ColorConsoleChannel> consoleChannel(new ColorConsoleChannel);
			AutoPtr<SimpleFileChannel> fileChannel(new SimpleFileChannel(logFilename));
			AutoPtr<SplitterChannel> splitterChannel(new SplitterChannel);

			if (!m_args.quiet()) {
				if (m_args.colouredConsole()) {
					consoleChannel->setProperty("enableColors", "true");
					consoleChannel->setProperty("debugColor", "green");
					consoleChannel->setProperty("informationColor", "cyan");
				}
				else 
					consoleChannel->setProperty("enableColors", "false");

				splitterChannel->addChannel(consoleChannel);
			}

			splitterChannel->addChannel(fileChannel);
			Logger::root().setChannel(splitterChannel);

			Info(m_appOptions.simInstanceName.c_str(), "Setting Log Level: %s", m_args.logLevel());
			Logger::root().setLevel(m_args.translateLogLevel());

			Info(m_appOptions.simInstanceName.c_str(), "PSim Version: %s", version());
			if (m_appOptions.waitForTimestamp) 
				Info(m_appOptions.simInstanceName.c_str(), "Wait Interval: %d", m_appOptions.waitInterval);
			Info(m_appOptions.simInstanceName.c_str(), "Preparing the application for run ...");

			Info(m_appOptions.simInstanceName.c_str(), "Config: %s", m_args.config());
			if (!m_args.config().empty())
				runSim(m_args.config());

#ifdef __PROFILER_ENABLED__
			Profiler::dump();
#endif 
		}
		catch (const Poco::Exception& e) {
			Util::reportException(e);
			Util::exit(1);
		}
		catch (const std::exception& e) {
			Util::reportException(e);
			Util::exit(1);
		}
		catch (...) {
			Error(m_appOptions.simInstanceName.c_str(), "Unknown exception!");
			Util::exit(1);
		}

		return Application::EXIT_OK;
	}

	void App::pyInit(std::string appPath, std::vector<std::string>& args) {
		m_appPath = appPath;
		m_pyArgc = 1 + (int)args.size();
		m_pyArgs = args;
		m_pyArgv = new char* [m_pyArgc];

		m_pyArgv[0] = const_cast<char*>(m_appPath.c_str());

		for (size_t i = 0; i < m_pyArgs.size(); i++) {
			m_pyArgv[i + 1] = const_cast<char*>(m_pyArgs[i].c_str());
		}

		Poco::Util::Application::init(m_pyArgc, m_pyArgv);
	}
} /// namespace pesa 
