/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Args.cpp
///
/// Created on: 30 Mar 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "./Args.h"
#include "./App.h"

#include <iostream>

#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Util/HelpFormatter.h"
#include "Poco/String.h"

#include "Framework/Components/ComponentDefs.h"

#define PESA_VALUE_ARG(Long, Short, Help) 	Option(Long, Short, Help) \
												.required(false) \
												.repeatable(false) \
												.argument("value") \
												.callback(OptionCallback<Args>(this, &Args::handleArgument))

#define PESA_BOOL_ARG(Long, Short, Help) 	Option(Long, Short, Help) \
												.required(false) \
												.repeatable(false) \
												.callback(OptionCallback<Args>(this, &Args::handleArgument))

namespace pesa {

	using namespace Poco::Util;

	Args::Args(App& app)
		: m_app(app)
        , m_helpRequested(false)
		, m_logLevel("debug")
		//, m_logFilename("./logs/app_$YYYY$MM$DD_$HH$mm.log")
		, m_logFilename("./logs/app.log")
		, m_dbPath("mongodb://127.0.0.1:27017/")
		, m_dbName("pesa")
		, m_simInstanceName("PSim") {
	}

	Args::~Args() {
	}

	void Args::showHelp() {
		HelpFormatter helpFormatter(m_app.options());
		helpFormatter.setCommand(m_app.commandName());
		helpFormatter.setUsage("OPTIONS");
		helpFormatter.setHeader("Pesa simulator.");
		helpFormatter.format(std::cout);
	}

	void Args::define(OptionSet& options) {
		options.addOption(PESA_BOOL_ARG ("help",		"h",	"Display help information on command line arguments"));
		options.addOption(PESA_BOOL_ARG ("version",		"v",	"Version information"));

		options.addOption(PESA_VALUE_ARG("config", 		"c", 	"Configuration file to load for the simulator"));
		options.addOption(PESA_BOOL_ARG ("clamp", 		"C", 	"Clamp the end date (Defaults to false)"));
		options.addOption(PESA_VALUE_ARG("db", 			"d", 	"The path:name of the database (default = mongo://127.0.0.1:27017/@pesa)"));
		options.addOption(PESA_VALUE_ARG("days", 		"D", 	"Number of days to update the data (default = 5)"));
		options.addOption(PESA_VALUE_ARG("ovrEndDate", 	"E", 	"Override the config endDate (TODAY-1 etc)"));
		options.addOption(PESA_BOOL_ARG ("force",		"F",	"Force certain operations. This will be picked up by bits of modules that support forced behaviour e.g. DataRegistry will run an update etc."));
		options.addOption(PESA_BOOL_ARG ("forceLocates","f",	"Force locates or not."));
		options.addOption(PESA_BOOL_ARG ("diagnostics", "G", 	"Run diagnostics on a particular dataset. Use startdate (-S) and enddate (-E) if you want to run it on a date range"));
		options.addOption(PESA_BOOL_ARG ("headeronly",	"H",	"Perform various operations only on the header. Like diagnostics etc."));
		options.addOption(PESA_BOOL_ARG ("invalid", 	"i", 	"Only dump invalid values"));
		options.addOption(PESA_VALUE_ARG("ignore", 		"I", 	"Ignore certain values while dumping"));
		options.addOption(PESA_VALUE_ARG("threads", 	"j", 	"Number of threads to use in the system (default = 1)"));
		options.addOption(PESA_VALUE_ARG("job", 		"J", 	"The ID of the job to run"));
		options.addOption(PESA_BOOL_ARG ("simpleconsole","k", 	"Use simple/non-coloured console output (default = false)"));
		options.addOption(PESA_BOOL_ARG ("runCmds",		"K",	"Run commands in PyCmdRunner"));
		options.addOption(PESA_VALUE_ARG("logfile", 	"l", 	"Where to dump the log file (default = ./app.log)"));
		options.addOption(PESA_VALUE_ARG("loglevel",	"L", 	"What log level to use. Valid values are: trace, debug, info, error (default = debug)"));
		options.addOption(PESA_BOOL_ARG ("shmem",		"m", 	"Use shared memory across processes"));
		options.addOption(PESA_VALUE_ARG("macros",		"M", 	"Define macros using MACRO1=VALUE1;MACRO2=VALUE2 ..."));
		options.addOption(PESA_VALUE_ARG("override",	"o", 	"Override variable(s) in the config without modifying the config (which kills the checkpoint)"));
		options.addOption(PESA_BOOL_ARG ("logprogress",	"O", 	"Log the progress to the log file instead of using the progress bar"));
		options.addOption(PESA_VALUE_ARG("instname", 	"N", 	"The name of the instance that is used to identify different tasks in the DB. This defaults to PSim"));
		options.addOption(PESA_BOOL_ARG ("noRunStrats",	"n", 	"Whether to run the child strategies or not."));
		options.addOption(PESA_VALUE_ARG("password", 	"p", 	"!WARNING - USE WITH CARE - DO NOT PUT IN CONFIG FILES etc.! The password to go with the username. This can be used for various kinds of authentication e.g. the Data API (THIS IS NEVER LOGGED!)"));
		options.addOption(PESA_VALUE_ARG("dump", 		"P", 	"Dump/Print data to console/log file. Note that string is of format <universe_id>.<data_id>. If no <universe_id> is given then Security Master is used! To dump universe, just give an empty <data_id>."));
		options.addOption(PESA_BOOL_ARG ("queue", 		"q", 	"Only queue the config in the database and not really run it"));
		options.addOption(PESA_BOOL_ARG ("quiet", 		"Q", 	"Quiet mode. The application will ONLY log to the file and not to the console"));
		options.addOption(PESA_BOOL_ARG ("dataonly", 	"R", 	"Update data only"));
		options.addOption(PESA_VALUE_ARG("ovrStartDate","S", 	"Override the config start date (20170101 etc)"));
		options.addOption(PESA_BOOL_ARG ("server", 		"s", 	"Run in server/daemon mode"));
		options.addOption(PESA_VALUE_ARG("configType", 	"T", 	"What is the type of the config. Valid values are: xml, json and mongo (default = xml)"));
		options.addOption(PESA_VALUE_ARG("username", 	"u", 	"The username. This can be used for various kinds of authentication e.g. the Data API (THIS IS NEVER LOGGED!)"));
		options.addOption(PESA_VALUE_ARG("uuids", 		"U", 	"Update only a certain set of uuids (all history or specify a Start and End date using -S and -E). Use comma so separate the ids!"));
		options.addOption(PESA_BOOL_ARG ("valid", 		"v", 	"Only dump valid values"));
		options.addOption(PESA_BOOL_ARG ("combocp", 	"V", 	"Combo checkpoint or not"));
		options.addOption(PESA_VALUE_ARG("waitInterval","W", 	"Wait interval for the TIMESTAMP in milliseconds (this is the duration of the spin-lock interval). Defaults to 0 (NO WAIT!)"));
		options.addOption(PESA_BOOL_ARG ("noDownload", 	"x", 	"Don't download some of the data"));
		options.addOption(PESA_BOOL_ARG ("disableOpt", 	"X", 	"Disable DataRegistry optimisation"));
		options.addOption(PESA_VALUE_ARG("maxCCProcs",	"Z",	"Maximum number of concurrent child processes to run (Defaults to 0 which means unlimited)"));
	}

	int Args::translateLogLevel(const std::string& logLevel) {
		if (logLevel == "trace")
			return Poco::Message::PRIO_TRACE;
		else if (logLevel == "debug")
			return Poco::Message::PRIO_DEBUG;
		else if (logLevel == "info")
			return Poco::Message::PRIO_INFORMATION;
		else if (logLevel == "error")
			return Poco::Message::PRIO_ERROR;

		return Poco::Message::PRIO_DEBUG;
	}

	void Args::handleArgument(const std::string& name, const std::string& value) {
		if (name == "help") 
			m_helpRequested = true;
		else if (name == "version")
			m_printVersion = true;
		else if (name == "config") {
			m_config = value;
			ASSERT(!m_config.empty(), "Cannot have an empty config name!");
		}
		else if (name == "db") {
			auto parts = Util::split(value, "@");
			ASSERT(parts.size() <= 2, "Invalid database URL: " << value);
			m_dbPath = parts[0];
			if (parts.size() > 1)
				m_dbName = parts[1];
		}
		else if (name == "dump") 
			m_dumpData = value;
		else if (name == "loglevel") {
			m_logLevel = value;
			ASSERT(!m_logLevel.empty(), "Cannot have an empty logging level!");
		}
		else if (name == "logfile") {
			m_logFilename = value;
			ASSERT(!m_logFilename.empty(), "Cannot have an empty log filename!");
		}
		else if (name == "dataonly")
			m_dataOnly = true;
		else if (name == "limit")
			m_limit = Util::cast<int>(value);
		else if (name == "username")
			m_username = value;
		else if (name == "password")
			m_password = value;
		else if (name == "simpleconsole")
			m_colouredConsole = false;
		else if (name == "diagnostics")
			m_runDiagnostics = true;
		else if (name == "threads") {
			m_numThreads = Util::cast<int>(value);
			ASSERT(m_numThreads >= 1, "Invalid number of threads specified: " << value);
			m_numThreads = std::min(m_numThreads, AppOptions::s_maxThreads);
		}
		else if (name == "configType") {
			std::string ctype = Poco::toLower(value);
			if (value == "xml")
				m_configType = Config::kXML;
			else if (value == "json")
				m_configType = Config::kJSON;
			else if (value == "mongo")
				m_configType = Config::kMongoDB;
			else 
				ASSERT(false, "Unknown config type specified: " << value);
		}
		else if (name == "force")
			m_force = true;
		else if (name == "clamp")
			m_clampEndDate = true;
		else if (name == "instname")
			m_simInstanceName = value;
		else if (name == "logprogress")
			m_logProgress = true;
		else if (name == "override")
			m_configOverrides = value;
		else if (name == "quiet")
			m_quiet = true;
		else if (name == "job")
			m_jobId = value;
		else if (name == "disableOpt")
			m_disableOptimisation = true;
		else if (name == "noDownload")
			m_noDownload = true;
		else if (name == "days")
			m_days = Util::cast<int>(value);
		else if (name == "waitInterval") {
			m_waitForTimestamp = true;
			m_waitInterval = Util::cast<int>(value);
		}
		else if (name == "macros") 
			m_macros = value;
		else if (name == "forceLocates")
			m_forceLocates = true;
		else if (name == "noRunStrats")
			m_noRunChildStrats = true;
		else if (name == "uuids")
			m_updateUuids = value;
		else if (name == "runCmds")
			m_runCmds = true;
		else if (name == "ovrEndDate")
			m_ovrEndDate = value;
		else if (name == "ovrStartDate")
			m_ovrStartDate = value;
		else if (name == "combocp")
			m_useComboCheckpoint = true;
		else if (name == "valid")
			m_onlyValid = true;
		else if (name == "invalid")
			m_onlyInvalid = true;
		else if (name == "maxCCProcs")
			m_maxChildProcesses = Util::cast<int>(value);
		else if (name == "server")
			m_runServer = true;
		else if (name == "ignore") {
			if (value.empty()) {
				m_ignoreDefault = false;
				m_defaultValues.clear();
			}
			else {
				m_ignoreDefault = true;
				m_defaultValues = Util::split(value, ",");
			}
		}
		else if (name == "shmem") {
#if defined(__LINUX__) || defined(__MACOS__)
			m_useShMem = true;
#else 
			std::cerr << "shmem option is only available in Linux. Ignoring ..." << std::endl;
			m_useShMem = false;
#endif /// __MACOS__
		}
	}
} /// namespace pesa 

#undef PESA_VALUE_ARG
#undef PESA_BOOL_ARG
