/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Args.h
///
/// Created on: 30 Mar 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/FrameworkDefs.h"
#include "Core/Lib/Util.h"

#include <Poco/Util/OptionSet.h>
#include "Framework/Config/Config.h"

namespace pesa {

	class App;

	class Framework_API Args {
	private:
		App& 					m_app;						/// The application instance
		bool 					m_helpRequested = false;	/// Has help been requested
		bool					m_printVersion = false;		/// Print the version information
		bool					m_dataOnly = false;			/// Update data only
		bool					m_colouredConsole = true;	/// Use colours on the console
		std::string 			m_config;					/// Path to the config that we are going to run
		Config::Type			m_configType = Config::kXML;/// What is the type of the config
		std::string 			m_logLevel; 				/// What is the log level
		std::string 			m_logFilename; 				/// The name of the log file
		bool					m_alphaGen = false;			/// Generate configs for the Alpha generation process (these are always written to the DB)

		std::string				m_dbPath;					/// The path of the mongo database
		std::string				m_dbName;					/// The name of the database

		std::string				m_simInstanceName;			/// The name of the simulation instance

		std::string 			m_username; 				/// The username
		std::string 			m_password; 				/// The password

		std::string				m_dumpData;					/// Dump data 
		int						m_limit;					/// Limit
		bool					m_runDiagnostics = false; 	/// Run diagnostics on the dataset specified in dumpData
		bool					m_headerOnly = false; 		/// Perform various operations with the header
		int						m_numThreads = 1;			/// Number of threads to use in the application
		int						m_maxChildProcesses = 0;	/// Maximum number of child processes
		bool 					m_force = false; 			/// Force update of certain operations 
		bool					m_logProgress = false;		/// Whether to log the progress to console instead of showing the progress bar
		bool					m_quiet = false;			/// Tells the application to not log anything to the console
		bool					m_disableOptimisation = false; /// Disable dataset optimisation
		bool					m_noDownload = false;		/// Don't download thirdparty content and denpendencies
		bool					m_waitForTimestamp = false;	/// Wait for TIMESTAMP in the data cache or not
		int						m_waitInterval = 0;			/// The wait interval for the TIMESTAMP
		std::string				m_macros = "";				/// Custom defined macros
		bool					m_forceLocates = false;		/// Force the use of locates on the day
		bool					m_noRunChildStrats = false;	/// Whether to override the config variable of noRunStrategies or not
		bool					m_runCmds = false;			/// Run commands through PyCmdRunner
		bool					m_useShMem = false;			/// Use shared memory or not
		bool					m_useComboCheckpoint = false; /// Whether use the combo checkpoint or not 
		bool					m_clampEndDate = false;		/// Clamp the end date to 
		bool					m_runServer = false;		/// Run in daemon/server mode
		bool					m_onlyValid = false;		/// Only dump valid values
		bool					m_onlyInvalid = false;		/// Only dump invalid values 
		bool					m_ignoreDefault = false;	/// Ignore the default values while dumping a dataset
		StringVec				m_defaultValues;			/// The default valus for a dataset

		int						m_days = 5;					/// Number of days to update the data
		std::string				m_jobId;					/// The id of the job that needs to be run 
		std::string				m_updateUuids;				/// Update a certain set of uuids!

		std::string				m_ovrStartDate;				/// Override start date
		std::string				m_ovrEndDate;				/// Override end date

		std::string				m_configOverrides;			/// Config overrides

	public:
								Args(App& app);
		virtual 				~Args();

		void 					define(Poco::Util::OptionSet& options);
		void 					showHelp();

		void 					handleArgument(const std::string& name, const std::string& value);

		////////////////////////////////////////////////////////////////////////////
		/// Static methods
		////////////////////////////////////////////////////////////////////////////
		static int 				translateLogLevel(const std::string& logLevel);

		////////////////////////////////////////////////////////////////////////////
		/// Inline methods
		////////////////////////////////////////////////////////////////////////////
		inline bool 			helpRequested() const 			{ return m_helpRequested; 		} 
		inline bool				printVersion() const			{ return m_printVersion;		}
		inline std::string 		config() const 					{ return m_config; 				}
		inline std::string 		logLevel() const 				{ return m_logLevel;			}
		inline std::string 		logFilename() const 			{ return m_logFilename;			}
		inline int 				translateLogLevel() const 		{ return Args::translateLogLevel(m_logLevel); }
		inline bool				dataOnly() const				{ return m_dataOnly;			}
		inline std::string		dumpData() const				{ return m_dumpData;			}
		inline int				limit() const 					{ return m_limit; 				}
		inline const std::string& dbPath() const				{ return m_dbPath;				}
		inline const std::string& dbName() const				{ return m_dbName;				}
		inline const std::string& username() const				{ return m_username;			}
		inline const std::string& password() const				{ return m_password;			}
		inline bool				colouredConsole() const			{ return m_colouredConsole;		}
		inline bool				runDiagnostics() const			{ return m_runDiagnostics;		}
		inline Config::Type		configType() const				{ return m_configType;			}
		inline int				numThreads() const				{ return m_numThreads;			}
		inline bool				headerOnly() const				{ return m_headerOnly;			}
		inline bool				force() const					{ return m_force;				}
		inline const std::string& simInstanceName() const		{ return m_simInstanceName;		}
		inline bool				logProgress() const				{ return m_logProgress;			}
		inline bool				quiet() const					{ return m_quiet;				}
		inline const std::string& jobId() const					{ return m_jobId;				}	
		inline bool				disableOptimisation() const		{ return m_disableOptimisation; }
		inline bool				noDownload() const				{ return m_noDownload;			}
		inline int				days() const					{ return m_days;				}
		inline bool				waitForTimestamp() const		{ return m_waitForTimestamp;	}
		inline int				waitInterval() const			{ return m_waitInterval;		}
		inline bool				forceLocates() const			{ return m_forceLocates;		}
		inline bool				noRunChildStrats() const		{ return m_noRunChildStrats;    }
		inline const std::string& macros() const				{ return m_macros;				}
		inline const std::string& updateUuids() const			{ return m_updateUuids;			}
		inline bool				runCmds() const					{ return m_runCmds;				}
		inline std::string		ovrStartDate() const			{ return m_ovrStartDate;		}
		inline std::string		ovrEndDate() const				{ return m_ovrEndDate;			}
		inline bool				useShMem() const				{ return m_useShMem;			}
		inline bool				useComboCheckpoint() const		{ return m_useComboCheckpoint;  }
		inline std::string		configOverrides() const			{ return m_configOverrides;		}
		inline int				maxChildProcesses() const		{ return m_maxChildProcesses;   }
		inline bool				clampEndDate() const			{ return m_clampEndDate;        }
		inline bool				runServer() const				{ return m_runServer;			}
		inline bool				onlyValid() const				{ return m_onlyValid;			}
		inline bool				onlyInvalid() const				{ return m_onlyInvalid;			}
		inline bool				ignoreDefault() const			{ return m_ignoreDefault;		}
		inline const StringVec& defaultValues() const			{ return m_defaultValues;		}
	};

} /// namespace pesa 

