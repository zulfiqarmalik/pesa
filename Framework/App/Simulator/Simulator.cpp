/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// App.cpp
///
/// Created on: 31 Mar 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Simulator.h"
#include "Framework/Components/IComponent.h"
#include "Framework/Components/IAlpha.h"
#include "Framework/Components/IDataLoader.h"
#include "Framework/Components/IUniverse.h"
#include "Framework/Pipeline/IPipelineComponent.h"
#include "Framework/Helper/ShLib.h"
#include "Poco/Logger.h"

#include "Framework/App/App.h"

#include "Core/Lib/Util.h"
#include "Core/Lib/Profiler.h"

#include <cstdlib>
#include <iomanip>

namespace pesa {

	////////////////////////////////////////////////////////////////////////////
	/// Static functions
	////////////////////////////////////////////////////////////////////////////

	IDataRegistry* Simulator::dataRegistry() {
		return m_rt.dataRegistry;
	}

	const Config* Simulator::config() const {
		if (!m_configs.size())
			return nullptr;
		ASSERT(m_configIter < m_configs.size(), "Invalid config iter: " << m_configIter << " - Total size: " << m_configs.size());
		return m_configs[m_configIter].get();
	}

	Config* Simulator::config() {
		if (!m_configs.size())
			return nullptr;
		ASSERT(m_configIter < m_configs.size(), "Invalid config iter: " << m_configIter << " - Total size: " << m_configs.size());
		return m_configs[m_configIter].get();
	}

	const RuntimeData& Simulator::runtimeData() const {
		return m_rt;
	}

	IPipelineComponentPtr Simulator::component(const std::string& id) {
		for (auto pcomp : m_pipeline) {
			auto config = (const ConfigSection&)pcomp->config();
			if (config.id() == id)
				return pcomp;
		}

		return nullptr;
	}

	const AppOptions& Simulator::options() const {
		return m_options;
	}

	////////////////////////////////////////////////////////////////////////////
	/// Member functions
	////////////////////////////////////////////////////////////////////////////
	Simulator::Simulator(const AppOptions& options)
		: m_options(options) {
		m_rt.appOptions = m_options;

		bool logProgress = false;
		App* app = dynamic_cast<App*>(Application::instance());
		m_logProgress = app->args().logProgress();
		m_instName = options.simInstanceName;
	}

	Simulator::~Simulator() {
	}

	void Simulator::initSim() {
		initPipeline(*config(), m_pipeline);
	}

	void Simulator::freeSim() {
		m_rt.simState = kSS_Free;
		m_pipeline.clear();
	}

	void Simulator::initPipeline(const Config& config, IPipelineComponentPtrVec& pipeline) {
		/// Load all the pipeline components
		m_rt.simState = kSS_Init;
		auto modules = config.pipeline().modules();

		for (auto* module : modules) {
			IPipelineComponentPtr pcomp;

			if (module->id() == "DataRegistry") {
				IDataRegistryPtr dataRegistry = helper::ShLib::global().createPipelineComponent<IDataRegistry>(*module, nullptr);
				m_rt.dataRegistry = dataRegistry.get();
				pcomp = dataRegistry;
			}
			else
				pcomp = helper::ShLib::global().createPipelineComponent<IPipelineComponent>(*module, nullptr);

			pcomp->pipelineHandler() = this;

			ASSERT(pcomp, "Unable to create pipeline component: " << module->toString());
			pipeline.push_back(pcomp);
		}
	}

	static void writeChar(std::string& str, int start, int end, int ipercent, unsigned char c) {
		for (int i = start; i < end; i++) {
			if (i == 48) {
				std::ostringstream ss;
				ss << std::setfill('0') << std::setw(2) << ipercent;
				str += ss.str();
			}
			else if (i == 49 || i == 50)
				continue;
			else if (i == 51)
				str += '%';
			else
				str += c;
		}
		std::cout << str;
	}

	void Simulator::prepare(RuntimeData& rt, const IPipelineComponentPtrVec& pipeline) {
		PROFILE_SCOPED();

		rt.simState = kSS_Prepare;
		Info(m_instName.c_str(), "PreExec pipeline. Num components: %z", pipeline.size());
		for (auto pcomp : pipeline)
			pcomp->prepare(rt);
	}

	void Simulator::exec(RuntimeData& rt, const IPipelineComponentPtrVec& pipeline) {
		PROFILE_SCOPED();

		prepare(rt, pipeline);

		m_percent = 0.0f;

		rt.simState = kSS_Running;
		/// If only a dump is requested ...
		if (!m_options.dumpData.empty()) {
			DumpInfo dumpInfo;

			dumpInfo.dataId		= m_options.dumpData;
			dumpInfo.startDate	= (IntDate)config()->defs().startDate();
			dumpInfo.endDate	= (IntDate)config()->defs().endDate();
			dumpInfo.runDiagnostics = m_options.runDiagnostics;

			if (dumpInfo.endDate > rt.dates[rt.diEnd])
				dumpInfo.endDate = rt.dates[rt.diEnd];
			if (dumpInfo.startDate < rt.dates[rt.diStart])
				dumpInfo.startDate = rt.dates[rt.diStart];

			m_rt.dataRegistry->debugDump(m_rt, dumpInfo);
			m_options.updateDataOnly = true;
		}

		if (!m_options.updateDataOnly) {
			Info(m_instName.c_str(), "Exec pipeline. Num components: %z", pipeline.size());
			Poco::Timestamp startTime;

			IntDay diStart = rt.diSimStart();
			IntDay diEnd = rt.diSimEnd();

			for (IntDay di = diStart; di <= diEnd; di++) {
				rt.di = di;

				for (auto pcomp : pipeline) {
					pcomp->tick(rt);

					/// Check whether someone modified di
					ASSERT(rt.di == di, "Pipeline component modified di. Going in was: " << di << " and came out with: " << rt.di);
				}

				float percent = ((float)(rt.di - rt.diStart) / (float)(rt.diEnd - rt.diStart));
				config()->tick(rt, percent);

				/// Not enabled for trace log level!
				if (m_options.logLevel <= Poco::Message::PRIO_DEBUG) {
					int ipercent = (int)(percent * 100.0f);
					int diff = ipercent - (int)(m_percent * 100.0f);

					if (diff > 0) {
						m_percent = percent;

						if (rt.di == rt.diStart)
							Debug(m_instName.c_str(), "Starting Simulation ...");

						if (!m_logProgress) {
							std::string str = "\r[";
							writeChar(str, 0, ipercent, ipercent, 254);
							writeChar(str, ipercent, 100, ipercent, ' ');
							str += "]";
							std::cout << str;
						}
						else {
							//Debug(m_instName.c_str(), "Progress: %0.2hf", m_percent);
						}
					}
				}
			}

			config()->tick(rt, 1.0f);

			std::cout << std::endl;
			float seconds = (float)startTime.elapsed() * 0.001f * 0.001f;
			Info(m_instName.c_str(), "Core simulation took: %0.2hf seconds [%0.2hf minutes]", seconds, seconds / 60.0f);
		}

		finalise(rt, pipeline);

		/// Now here we see whether there was a master for this config. 
		/// If there was one then we need to call it and tell it that 
		/// the config that was queued has finished!
		config()->end(rt, 0);
	}

	void Simulator::finalise(RuntimeData& rt, const IPipelineComponentPtrVec& pipeline) {
		PROFILE_SCOPED();

		rt.simState = kSS_Finalise;
		Info(m_instName.c_str(), "Post pipeline. Num components: %z", pipeline.size());
		for (auto pcomp : pipeline)
			pcomp->finalise(rt);
	}

	float Simulator::getStatus() const {
		return m_percent;
	}

	int Simulator::run() {
		try {
			m_configIter = 0;

			while (m_configIter < m_configs.size()) {
				Info(m_instName.c_str(), "Running Config#%z", m_configIter);

				Poco::Timestamp configStartTime;

				initSim();
				exec(m_rt, m_pipeline);
				freeSim();

				float seconds = (float)configStartTime.elapsed() * 0.001f * 0.001f;
				Info(m_instName.c_str(), "Config took: %0.2hf seconds [%0.2hf minutes]", seconds, seconds / 60.0f);

				m_configIter++;
			}
		}
		catch (const Poco::Exception& e) {
			Util::reportException(e);
            Poco::Debugger::enter();
		}
		catch (const std::exception& e) {
			Util::reportException(e);
            Poco::Debugger::enter();
		}

		return 0;
	}

	void Simulator::queueConfig(ConfigPtr config) {
		config->validate();
		m_configs.push_back(config);
	}

	int Simulator::run(const std::string& configFilename) {
		Debug(m_instName.c_str(), "Loading config: %s ...", configFilename);
		Config* config = new Config();
		Config::parseXml(configFilename, *config);
		Debug(m_instName.c_str(), "Config loaded successfully!");

		queueConfig(ConfigPtr(config));

		return run();
	}

	void Simulator::exit(int errCode) {
		config()->exit(errCode);
	}

	void Simulator::abort() {
		config()->abort();
	}

} /// namespace pesa 
