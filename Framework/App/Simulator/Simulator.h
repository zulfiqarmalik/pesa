/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// App.h
///
/// Created on: 31 Mar 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/FrameworkDefs.h"
#include "Framework/Config/Config.h"
#include "Framework/Pipeline/IDataRegistry.h"
#include "Framework/Pipeline/IPipelineHandler.h"

#include <iostream>
#include <string>

namespace pesa {

	class IComponent;
	class IUniverse;
	class App;

	class Framework_API Simulator : public IPipelineHandler {
	public:
	private:
		typedef std::vector<IComponent*> ComponentPVec;
		typedef std::vector<IPipelineComponentPtr> IPipelineComponentPtrVec;

		ConfigPtrVec				m_configs;					/// The configs that we're going to run 
		size_t						m_configIter;				/// The config iterator
		AppOptions					m_options; 					/// The engine options
		RuntimeData 				m_rt; 						/// Runtime data
		IPipelineComponentPtrVec	m_pipeline;					/// Load the pipeline
		float						m_percent = 0.0f;			/// How much simulation has completed thus far
		bool						m_logProgress = false;		/// Whether to log the progress to the application log file instead of the progress bar
		std::string					m_instName;					/// The name of the PSIM instance

		void 						initSim();
		void 						freeSim();
		void 						initPipeline(const Config& config, IPipelineComponentPtrVec& pipeline);

		void 						exec(RuntimeData& rt, const IPipelineComponentPtrVec& pipeline);
		void 						prepare(RuntimeData& rt, const IPipelineComponentPtrVec& pipeline);
		void 						finalise(RuntimeData& rt, const IPipelineComponentPtrVec& pipeline);

		////////////////////////////////////////////////////////////////////////////
		/// Static functions
		////////////////////////////////////////////////////////////////////////////

	public:
									Simulator(const AppOptions& options);
									~Simulator();

		int 						run();
		int 						run(const std::string& configFilename);
		virtual void				queueConfig(ConfigPtr config);

		void						exit(int errCode);
		void						abort();

		////////////////////////////////////////////////////////////////////////////
		/// IPipelineHandler implementation
		////////////////////////////////////////////////////////////////////////////
		virtual pesa::IDataRegistry* dataRegistry();
		virtual const pesa::Config* config() const;
		pesa::Config* 				config();
		virtual const pesa::RuntimeData& 	runtimeData() const;
		virtual pesa::IPipelineComponentPtr component(const std::string& id);
		virtual const pesa::AppOptions& options() const;
		virtual float				getStatus() const;

		////////////////////////////////////////////////////////////////////////////
		/// Inline functions
		////////////////////////////////////////////////////////////////////////////
		inline const pesa::IDataRegistry* dataRegistry() const	{ return m_rt.dataRegistry; 	}
//		inline const IUniverse* 	universe() const 		{ return m_rt.universe; 		}
	};

} /// namespace pesa 

