/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// ComponentDefs.h
///
/// Created on: 8 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "ComponentDefs.h"
#include "Framework/Data/Data.h"
#include "Framework/Data/Dataset.h"
#include "Framework/Components/IUniverse.h"

#include "Poco/String.h"

#include <algorithm>

namespace pesa {

	IntDay 				Constants::s_invalidDay 	= -1;

	const char* 		Constants::s_defClosePrice 	= "baseData.adjClosePrice";
	const char* 		Constants::s_defOpenPrice 	= "baseData.adjOpenPrice";
	const char* 		Constants::s_defHighPrice 	= "baseData.adjHighPrice";
	const char* 		Constants::s_defLowPrice 	= "baseData.adjLowPrice";
	const char* 		Constants::s_defVolume 		= "baseData.volume";
	const char* 		Constants::s_defMarketCap 	= "baseData.mcap";
	const char*			Constants::s_defBorrowCost	= "cost.borrowRate";
	const char*			Constants::s_defLocates		= "cost.locates";
	const char*			Constants::s_defCurrDayPrice= "snaps.price";

	const size_t		DataDefinition::s_version 	= sizeof(DataDefinition);
	const size_t		Security::s_version 		= sizeof(Security);

	DataDefinition 		DataDefinition::s_null;
	DataDefinition 		DataDefinition::s_int(DataType::kInt, sizeof(int));
	DataDefinition 		DataDefinition::s_long(DataType::kLong, sizeof(long));
	DataDefinition 		DataDefinition::s_uint32_t(DataType::kUInt, sizeof(uint32_t));
	DataDefinition 		DataDefinition::s_ulong_t(DataType::kULong, sizeof(ulong_t));
	DataDefinition 		DataDefinition::s_int64_t(DataType::kInt64, sizeof(int64_t));
	DataDefinition 		DataDefinition::s_float(DataType::kFloat, sizeof(float));
	DataDefinition 		DataDefinition::s_double(DataType::kDouble, sizeof(double));
	DataDefinition 		DataDefinition::s_string(DataType::kString, 0);
	DataDefinition 		DataDefinition::s_istring(DataType::kUInt16, sizeof(uint16_t));
	DataDefinition 		DataDefinition::s_int16_t(DataType::kInt16, sizeof(uint16_t));
	DataDefinition 		DataDefinition::s_uint16_t(DataType::kUInt16, sizeof(uint16_t));
	const Security::Index Security::s_invalidIndex = (Security::Index)(~0);

	std::string DataType::toString(DataType::Type type) {
		switch (type) {
		case DataType::kFloat:			return "float";
		case DataType::kDouble:			return "double";
		case DataType::kInt:			return "int";
		case DataType::kInt64:			return "int64";
		case DataType::kFloatArray:		return "float[]";
		case DataType::kIntArray:		return "int[]";
		case DataType::kInt16:			return "int16";
		case DataType::kUInt16:			return "uint16";
		case DataType::kUInt:			return "uint";
		case DataType::kUIntArray:		return "uint[]";
		case DataType::kLong:			return "long";
		case DataType::kLongArray:		return "long[]";
		case DataType::kULong:			return "ulong";
		case DataType::kULongArray:		return "ulong[]";
		case DataType::kString:			return "string";
		case DataType::kBlob:			return "blob";
		case DataType::kSecurityData:	return "SecurityData";
		case DataType::kFloatConstant:	return "const_float";
		case DataType::kTimestamp:		return "timestamp";
		case DataType::kStringArray:	return "string[]";
		case DataType::kDateTime:		return "datetime";
		case DataType::kVarFloat:		return "var_float";
		case DataType::kVarInt:			return "var_int";
		case DataType::kVarDouble:		return "var_double";
		case DataType::kVarDateTime:	return "var_datetime";
		case DataType::kVarTimestamp:	return "var_timestamp";
		case DataType::kVarString:		return "var_string";
		case DataType::kIndexedString:	return "i_string";
		case DataType::kIndexedStringMetadata: return "i_string_md";
		case DataType::kLongIndexedString:	return "l_string";
		case DataType::kVarLongIndexedString: return "var_l_string";
		case DataType::kLongIndexedStringMetadata: return "l_string_md";
		case DataType::kCustom:			return "custom";

		default:
			ASSERT(false, "Unknown data type: " << type);
		}

		return "";
	}

	DataType::Type DataType::parse(const std::string& stype_) {
		std::string stype = Poco::toLower(stype_);
		if (stype == "float" || stype.find("BigDecimal") == 0)
			return DataType::kFloat;
		else if (stype == "float[]")
			return DataType::kFloatArray;
		else if (stype == "int")
			return DataType::kInt;
		else if (stype == "int[]")
			return DataType::kIntArray;
		else if (stype == "double" || stype == "double[]" || stype == "float64")
			return DataType::kDouble;
		else if (stype == "int64" || stype == "int64[]" || stype == "int64")
			return DataType::kInt64;
		else if (stype == "timestamp" || stype == "datetime64[ns]")
			return DataType::kTimestamp;
		else if (stype == "datetime")
			return DataType::kDateTime;
		else if (stype == "string" || stype == "str" || stype == "String") 
			return DataType::kString;
		else if (stype == "string[]" || stype == "str[]")
			return DataType::kStringArray;
		else if (stype == "security")
			return DataType::kSecurityData;
		else if (stype == "int16")
			return DataType::kInt16;
		else if (stype == "uint16")
			return DataType::kUInt16;
		else if (stype == "uint")
			return DataType::kUInt;
		else if (stype == "uint[]")
			return DataType::kUIntArray;
		else if (stype == "long")
			return DataType::kLong;
		else if (stype == "long[]")
			return DataType::kLongArray;
		else if (stype == "ulong")
			return DataType::kULong;
		else if (stype == "ulong[]")
			return DataType::kULongArray;
		else if (stype == "const_float")
			return DataType::kFloatConstant;
		else if (stype == "var_float")
			return DataType::kVarFloat;
		else if (stype == "var_int")
			return DataType::kVarInt;
		else if (stype == "var_double")
			return DataType::kVarDouble;
		else if (stype == "var_datetime")
			return DataType::kVarDateTime;
		else if (stype == "var_timestamp")
			return DataType::kVarTimestamp;
		else if (stype == "var_string")
			return DataType::kVarString;
		else if (stype == "i_string")
			return DataType::kIndexedString;
		else if (stype == "i_string_md")
			return DataType::kIndexedStringMetadata;
		else if (stype == "l_string")
			return DataType::kLongIndexedString;
		else if (stype == "var_l_string")
			return DataType::kVarLongIndexedString;
		else if (stype == "l_string_md")
			return DataType::kLongIndexedStringMetadata;
		else if (stype == "custom")
			return DataType::kCustom;

		ASSERT(false, "Unsupported/Unknown data type: " << stype);
		return DataType::kUnknown;
	}

	size_t DataType::size(DataType::Type t) {
		switch (t) {
			case kFloat: 			return sizeof(float);
			case kDouble: 			return sizeof(double);
			case kInt: 				return sizeof(int);
			case kInt64: 			return sizeof(int64_t);
			case kFloatArray: 		return sizeof(float);
			case kIntArray: 		return sizeof(int);
			case kUInt: 			return sizeof(unsigned int);
			case kUIntArray: 		return sizeof(unsigned int);
			case kLong: 			return sizeof(long);
			case kLongArray: 		return sizeof(long);
			case kULong: 			return sizeof(unsigned long);
			case kULongArray: 		return sizeof(unsigned long);
			case kSecurityData: 	return sizeof(Security);
			case kFloatConstant: 	return sizeof(float);
			case kTimestamp: 		return sizeof(int64_t);
			case kDateTime: 		return sizeof(int64_t);
			case kInt16:			return sizeof(int16_t);
			case kUInt16:			return sizeof(uint16_t);
			case kIndexedString:	return sizeof(uint16_t);
			case kLongIndexedString:return sizeof(int);

			default:
			case kBlob:
			case kString:
			case kIndexedStringMetadata:
			case kLongIndexedStringMetadata:
			case kStringArray:
			case kVarFloat:
			case kVarDouble:
			case kVarDateTime:
			case kVarTimestamp:
			case kVarInt:
			case kVarString:
			case kVarLongIndexedString:
				return 0;
		}

		return 0;
	}

	//////////////////////////////////////////////////////////////////////////
	Futures::Month Futures::parse(char code) {
		switch (code) {
			case 'F': return Futures::kJanuary;
			case 'G': return Futures::kFebruary;
			case 'H': return Futures::kMarch;
			case 'J': return Futures::kApril;
			case 'K': return Futures::kMay;
			case 'M': return Futures::kJune;
			case 'N': return Futures::kJuly;
			case 'Q': return Futures::kAugust;
			case 'U': return Futures::kSeptember;
			case 'V': return Futures::kOctober;
			case 'X': return Futures::kNovember;
			case 'Z': return Futures::kDecember;
		}

		ASSERT(false, "Invalid/Unknown futures delivery month code: " << code);

		return Futures::kJanuary;
	}

	char Futures::code(Futures::Month month) {
		switch (month) {
			case Futures::kJanuary: return 'F';
			case Futures::kFebruary: return 'G';
			case Futures::kMarch: return 'H';
			case Futures::kApril: return 'J';
			case Futures::kMay: return 'K';
			case Futures::kJune: return 'M';
			case Futures::kJuly: return 'N';
			case Futures::kAugust: return 'Q';
			case Futures::kSeptember: return 'U';
			case Futures::kOctober: return 'V';
			case Futures::kNovember: return 'X';
			case Futures::kDecember: return 'Z';
		}

		ASSERT(false, "Invalid/Unknown futures delivery month: " << month);

		return '\0';
	}

	//////////////////////////////////////////////////////////////////////////
	SecId::Type SecId::parse(const std::string& stype) {
		if (stype == "uuid")
			return SecId::kUuid;
		else if (stype == "cfigi" || stype == "CFIGI")
			return SecId::kCFigi;
		else if (stype == "figi" || stype == "FIGI")
			return SecId::kFigi;
		else if (stype == "isin" || stype == "ISIN")
			return SecId::kISIN;
		else if (stype == "permId" || stype == "permid")
			return SecId::kPermId;
		else if (stype == "instrument")
			return SecId::kInstrument;
		else if (stype == "ticker")
			return SecId::kTicker;

		return SecId::kUuid;
	}

	std::string SecId::toString(Type type) {
		switch (type) {
		case SecId::kUuid:			return "uuid";
		case SecId::kInstrument:	return "instrument";
		case SecId::kTicker:		return "ticker";
		case SecId::kISIN:			return "isin";
		default: break;
		}

		return "uuid";
	}

	//////////////////////////////////////////////////////////////////////////

	std::string DataFrequency::toString(DataFrequency::Type type) {
		switch (type) {
		case kStatic: 			return "static";
		case kDaily: 			return "daily";
		case kWeekly: 			return "weekly";
		case kMonthly: 			return "monthly";
		case kQuarterly: 		return "quarterly";
		case kSemiAnnually: 	return "semi_annually";
		case kAnnually: 		return "annually";
		case kIntraday:			return "intraday";

		default:
			break;
		}

		/// Just use the integer frequency ...
		return Util::cast((int)type);
	}

	DataFrequency::Type DataFrequency::parse(const std::string& stype_) {
		std::string stype = Poco::toLower(stype_);

		/// The most common scenario ...
		if (stype == "daily")
			return kDaily;
		else if (stype == "universal" || stype == "static")
			return kStatic;
		else if (stype == "weekly")
			return kWeekly;
		else if (stype == "monthly")
			return kMonthly;
		else if (stype == "quarterly")
			return kQuarterly;
		else if (stype == "semiAnnually")
			return kSemiAnnually;
		else if (stype == "annually")
			return kAnnually;
		else if (stype == "intraday")
			return kIntraday;

		return kDaily;
	}

	Security::Security(const std::string& uuid) {
		setUuid(uuid);
	}

	Security::Security(const Security& rhs) {
		*this = rhs;
	}

	bool Security::isHolidayInExchange(const RuntimeData& rt, IntDay di) const {
		return rt.isHolidayInExchange(this->exchangeStr(), di);
	}

	std::string Security::toString() const {
		return uuid;
	}

#define MERGE_PROPERTY(prop)	if (this->prop[0] == '\0' && other.prop[0] != '\0') memcpy(this->prop, other.prop, sizeof(this->prop));
#define REPLACE_PROPERTY(prop)	if (other.prop[0] != '\0') memcpy(this->prop, other.prop, sizeof(this->prop));

	void Security::merge(const Security& other) {
		MERGE_PROPERTY(uuid);
		MERGE_PROPERTY(parentUuid);
		MERGE_PROPERTY(name);
		MERGE_PROPERTY(cfigi);
		MERGE_PROPERTY(cfigi);
		MERGE_PROPERTY(permId);
		MERGE_PROPERTY(bbTicker);
		MERGE_PROPERTY(ticker);
		MERGE_PROPERTY(exchange);
		MERGE_PROPERTY(instrument);
		MERGE_PROPERTY(permId);
		MERGE_PROPERTY(issuerPermId);
		MERGE_PROPERTY(quotePermId);
		MERGE_PROPERTY(ric);
		MERGE_PROPERTY(issueClass);
		MERGE_PROPERTY(issueType);
		MERGE_PROPERTY(issueDesc);
		MERGE_PROPERTY(currency);
		MERGE_PROPERTY(countryCode2);
		MERGE_PROPERTY(countryCode3);
		MERGE_PROPERTY(bbSecurityType);
		MERGE_PROPERTY(bbSecurityDesc);

		/// If our main UUID is NOT a CFIGI then we change that
		if (memcmp(uuid, cfigi, sizeof(uuid) != 0 && cfigi[0] != '\0'))
			memcpy(uuid, cfigi, sizeof(uuid));
	}

	void Security::override(const Security& other) {
		/// First of all we do a basic merge
		MERGE_PROPERTY(uuid);
		MERGE_PROPERTY(cfigi);

		/// And then we replace some properties
		REPLACE_PROPERTY(name);
		REPLACE_PROPERTY(cfigi);
		REPLACE_PROPERTY(cfigi);
		REPLACE_PROPERTY(permId);
		REPLACE_PROPERTY(bbTicker);
		REPLACE_PROPERTY(ticker);
		REPLACE_PROPERTY(exchange);
		REPLACE_PROPERTY(instrument);
		REPLACE_PROPERTY(permId);
		REPLACE_PROPERTY(issuerPermId);
		REPLACE_PROPERTY(quotePermId);
		REPLACE_PROPERTY(ric);
		REPLACE_PROPERTY(issueClass);
		REPLACE_PROPERTY(issueType);
		REPLACE_PROPERTY(issueDesc);
		REPLACE_PROPERTY(currency);
		REPLACE_PROPERTY(countryCode2);
		REPLACE_PROPERTY(countryCode3);
		REPLACE_PROPERTY(bbSecurityType);
		REPLACE_PROPERTY(bbSecurityDesc);

		/// If our main UUID is NOT a CFIGI then we change that
		if (memcmp(uuid, cfigi, sizeof(uuid) != 0 && cfigi[0] != '\0'))
			memcpy(uuid, cfigi, sizeof(uuid));
	}

#undef MERGE_PROPERTY
#undef REPLACE_PROPERTY

	std::string Security::debugString() const {
	  //std::ostringstream ss;

		auto ids  	= Poco::format("\nUUID  : %s [CFIGI: %s, FIGI: %s, ISIN: %s, PermId: %s, PerfId: %s]", uuidStr(), cfigiStr(), figiStr(), isinStr(), permIdStr(), perfIdStr());
		auto misc 	= Poco::format("\nTicker: %s [Instrument: %s, Exchange: %s, Name: %s, Index: %u]", tickerStr(), instrumentId(), exchangeStr(), nameStr(), index);

		return (ids + misc);

		//		ss 	<< "\nUUID     : " << uuid << " [CFIGI: NM: " << name << ", Index: " << index << ", Parent: " << parentUuid << "]"
		//	<< "\nTicker   : " << ticker << " [EX: " << exchange << ", INST: " << instrument << "]" 
			//<< "\nISS-CLS  : " << issueClass << "[CC: " << currency << "]"
			//<< "\nISS-TYPE : " << issueType << " [BB: " << bbSecurityType << "]" 
			//<< "\nISS-DESC : " << issueDesc << " [BB: " << bbSecurityDesc << "]"
		//	;

		//return ss.str();
	}

	//////////////////////////////////////////////////////////////////////////
	/// RuntimeData
	//////////////////////////////////////////////////////////////////////////
	template <class PeriodType>
	static inline void checkPeriod(IntDay di, const Poco::DateTime& date, int periodIndex, std::vector<PeriodType>& periods, SizeVecPtr periodMap) {
		if (!periods.size())
			periods.push_back(PeriodType(di, di, date.year(), periodIndex));

		PeriodType& period = periods[periods.size() - 1];

		if (period.index != periodIndex) 
			periods.push_back(PeriodType(di, di, date.year(), periodIndex));
		else 
			period.diEnd = di;

		(*periodMap)[di] = periods.size();
	}

	void RuntimeData::initPeriods() {
		weekIndexMap = std::make_shared<SizeVec>(diEnd + 1);	/// diEnd is inclusive
		quarterIndexMap = std::make_shared<SizeVec>(diEnd + 1); /// diEnd is inclusive

		for (IntDay di = 0; di <= diEnd; di++) {
			Poco::DateTime date = Util::intToDate(dates[di]);

			/// Week number 1 is the week containing January 4. This is in accordance to ISO 8601 (http://pocoproject.org/docs/Poco.DateTime.html#9371)
			int week = date.week();
			int quarter = date.month() / 4;

			checkPeriod(di, date, week, weeks, weekIndexMap);
			checkPeriod(di, date, quarter, quarters, quarterIndexMap);
		}
	}

	bool RuntimeData::isLastDi(IntDay di) const {
		if (di == 0)
			di = this->di;
		if (!isTodayHoliday)
			return di == this->diEnd;
		return di == this->diEnd - 1;
	}

	IntDay RuntimeData::getExactDateIndex(IntDate date) const {
		auto iter = datesIndex.find(date);

		if (iter == datesIndex.end())
			return Constants::s_invalidDay;

		return iter->second;
	}

	IntDay RuntimeData::getDateIndex(IntDate date) const {
		ASSERT(dates.size() > 0, "Empty dates array!");

		auto p = std::equal_range(dates.begin(), dates.end(), date);

		if (p.first == dates.end())
			return Constants::s_invalidDay;
		else if (isTodayHoliday && p.first == dates.end())
			return Constants::s_invalidDay;

		return (IntDay)(p.first - dates.begin());
	}

	bool RuntimeData::isTimeAfterMarketOpen(const Poco::DateTime& utcNow) const {
		Poco::DateTime utcMarketOpen = Util::py_timestampToDateTime((int64_t)utcTradingStartTime[utcTradingStartTime.size() - 1]);

		/// First we match it to the same date ... 
		if (utcNow.day() != utcMarketOpen.day() || utcNow.month() != utcMarketOpen.month() || utcNow.year() != utcMarketOpen.year())
			return false;

		/// Now we compare the time
		return utcNow.timestamp() > utcMarketOpen.timestamp();
	}

	bool RuntimeData::isAfterMarketOpen() const {
		Poco::DateTime utcNow;
		return isTimeAfterMarketOpen(utcNow);
	}

	bool RuntimeData::isBeforeMarketClose() const {
		Poco::DateTime utcNow;
		Poco::DateTime utcMarketOpen = Util::py_timestampToDateTime((int64_t)utcTradingStartTime[utcTradingStartTime.size() - 1]);
		Poco::DateTime utcMarketClose = Util::py_timestampToDateTime((int64_t)utcTradingEndTime[utcTradingEndTime.size() - 1]);

		/// First we match it to the same date ... 
		if ((utcNow.day() >= utcMarketOpen.day() && utcNow.day() <= utcMarketClose.day()) &&
			(utcNow.month() >= utcMarketOpen.month() && utcNow.month() <= utcMarketClose.month()) &&
			(utcNow.year() >= utcMarketOpen.year() && utcNow.year() <= utcMarketClose.year())) {
			return utcNow.timestamp() < utcMarketClose.timestamp();
		}

		return false;
	}

	bool RuntimeData::isInTradingHours() const {
		Poco::Timestamp utcNow;
		Poco::DateTime utcMarketOpen = Util::py_timestampToDateTime((int64_t)utcTradingStartTime[utcTradingStartTime.size() - 1]);
		Poco::DateTime utcMarketClose = Util::py_timestampToDateTime((int64_t)utcTradingEndTime[utcTradingEndTime.size() - 1]);

		return utcNow >= utcMarketOpen.timestamp() && utcNow <= utcMarketClose.timestamp();
	}

	bool RuntimeData::isHolidayInExchange(const std::string& exchange, IntDay di) const {
		auto hiter = this->exchangeHolidays.find(exchange);
		if (hiter == this->exchangeHolidays.end()) {
			//Warning("RT", "Unknown exchange: %s", exchange);
			return false;
		}

		//ASSERT(hiter != this->exchangeHolidays.end(), "Invalid/Un-cached exchange: " << exchange);
		ASSERT(di < dates.size(), "Invalid di: " << di << " [Max: " << dates.size() << "]");
		IntDate date = dates[di];
		auto biter = hiter->second.find(date);
		if (biter == hiter->second.end())
			return false;
		return biter->second;
	}

	std::ostream& operator << (std::ostream& os, const Security& obj) {
		os << obj.toString();
		return os;
	}

	////////////////////////////////////////////////////////////////////////////
	/// DataDefinition
	////////////////////////////////////////////////////////////////////////////
	std::string DataDefinition::toString(const Dataset& ds) const {
		std::ostringstream ss;
		ss << ds.name << "." << name;
		return ss.str();
	}

	DataDefinition& DataDefinition::operator = (const DataDefinition& rhs) {
		this->setName(rhs.name);
		this->setTypeName(rhs.typeName);

		itemType 	= rhs.itemType;
		itemSize 	= rhs.itemSize;
		frequency 	= rhs.frequency;
		flags 		= rhs.flags;

		return *this;
	}

	bool DataDefinition::operator == (const DataDefinition& rhs) const {
		return 	version == rhs.version &&
				(std::string(name) == std::string(rhs.name)) &&
				itemType == rhs.itemType &&
				itemSize == rhs.itemSize &&
				frequency == rhs.frequency &&
				flags == rhs.flags;
	}

	DataDefinition::DataDefinition() {
	}

	DataDefinition::DataDefinition(DataType::Type itype, size_t isize)
		: itemType(itype), itemSize(isize) {
	}

	DataDefinition::DataDefinition(const DataDefinition& rhs) {
		*this = rhs;
	}

	DataDefinition::DataDefinition(std::string name) {
		ASSERT(name.length() <= s_maxName - 1, "Length of data should not exceed: " << s_maxName);

		strncpy(this->name, name.c_str(), sizeof(this->name));
	}

	DataDefinition::DataDefinition(std::string name, DataType::Type itemType, size_t itemSize, UIntCount frequency, unsigned int flags, const char* typeName /* = nullptr */) {
		this->setName(name);

		if (typeName)
			this->setTypeName(typeName);

		this->itemType = itemType;
		this->itemSize = itemSize;
		this->frequency = frequency;
		this->flags = flags;
	}

	Poco::Path DataDefinition::path(IUniverse* universe, const std::string& rootPath, const std::string& dataName) const {
		return path(universe ? universe->name() : "", rootPath, dataName);
		//if (universe)
		//	return Poco::Path(Poco::Path(Poco::Path(Poco::Path(rootPath), universe->name()), dataName), std::string(name));

		//return Poco::Path(Poco::Path(Poco::Path(rootPath), dataName), std::string(name));
	}

	Poco::Path DataDefinition::path(const std::string& universeName, const std::string& rootPath, const std::string& dataName) const {
		if (!universeName.empty())
			return Poco::Path(Poco::Path(Poco::Path(Poco::Path(rootPath), universeName), dataName), std::string(name));

		return Poco::Path(Poco::Path(Poco::Path(rootPath), dataName), std::string(name));
	}

	void DataDefinition::parseDefString(const std::string dataset, std::string& dataName, DataType::Type& type, DataFrequency::Type& frequency) {
		StringVec parts = Util::split(dataset, ".");
		frequency = DataFrequency::kDaily;
		type = DataType::kFloat;
		dataName = dataset;

		ASSERT(parts.size() <= 3, "Invalid dataset name: " << dataset);
		if (parts.size() >= 2) {
			type = DataType::parse(parts[0]);
			dataName = parts[1];

			if (parts.size() == 3)
				frequency = DataFrequency::parse(parts[2]);
		}
	}
}
