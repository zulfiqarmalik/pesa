/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Defs.h
///
/// Created on: 8 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/FrameworkDefs.h"

#include <memory>
#include <vector>
#include <unordered_map>
#include <map>
#include <functional>

#include "Poco/Path.h"
#include "Poco/LocalDateTime.h"
#include "Framework/Config/ConfigSection.h"
#include "Core/Lib/Util.h"
#include "Core/Lib/Application.h"

#ifdef __WINDOWS__
	#pragma warning(disable : 4251) /// class needs to have a dll-interface 
#endif 

namespace pesa {
	class IDataRegistry;
	class IUniverse;
	class IComponent;
	class IPipelineComponent;
	class FloatMatrixData;

	typedef std::shared_ptr<IUniverse> 		IUniversePtr;
	typedef std::shared_ptr<IDataRegistry> 	IDataRegistryPtr;

	typedef IComponent* 					ComponentCreateFunction(const ConfigSection& config);
	typedef IPipelineComponent* 			PipelineComponentCreateFunction(const ConfigSection& config);

	typedef unsigned int 					IntDate;
	typedef int								IntDay;
	typedef unsigned int 					UIntCount;
	typedef unsigned int					IntTime;

	typedef std::map<IntDate, bool> 		IntDateBoolMap;
	typedef std::vector<IntDate> 			IntDateVec;
	typedef std::vector<IntTime> 			IntTimeVec;
	typedef std::vector<Poco::DateTime> 	DateTimeVec;
	typedef std::vector<Poco::LocalDateTime> LocalDateTimeVec;
	typedef std::vector<float> 				FloatVec;
	typedef std::vector<double> 			DoubleVec;
	typedef std::vector<int> 				IntVec;
	typedef std::vector<int64_t> 			Int64Vec;
	typedef std::vector<unsigned int> 		UIntVec;
	typedef std::vector<size_t> 			SizeVec;
	// typedef std::vector<FloatVec> 			AlphasVec;

	typedef std::vector<std::string> 		StringVec;
	typedef std::vector<StringVec>			StringVecVec;
	typedef std::unordered_map<std::string, std::string> StringMap;
	typedef std::shared_ptr<SizeVec>		SizeVecPtr;
	typedef std::unordered_map<std::string, StringVec> StringVecMap;

	//////////////////////////////////////////////////////////////////////////

	struct Framework_API Constants {
		static IntDay 						s_invalidDay;

		static const char*					s_defClosePrice;
		static const char*					s_defOpenPrice;
		static const char*					s_defHighPrice;
		static const char*					s_defLowPrice;
		static const char*					s_defVolume;
		static const char*					s_defMarketCap;
		static const char*					s_defBorrowCost;
		static const char*					s_defLocates;
		static const char*					s_defCurrDayPrice;

		enum Level {
			kLvlNone			= 0,
			kLvlAlpha			= 1,
			kLvlRawPortfolio	= 2,
			kLvlPreOptPortfolio	= 3,

			kLvlFinalPortfolio	= 10,
		};
	};

	////////////////////////////////////////////////////////////////////////////
	/// Defines a tradable security - NEVER CHANGE THIS
	////////////////////////////////////////////////////////////////////////////
	struct RuntimeData;

	struct Framework_API SecId {
		enum Type {
			kUuid				= 0,
			kCFigi				= 1,
			kFigi				= 2,
			kISIN				= 3,
			kPermId				= 4,
			kPerfId				= 5,
			kInstrument			= 6,
			kTicker				= 7,

			kCount,
		};

		static Type parse(const std::string& stype);
		static std::string toString(Type type);
	};

	struct Framework_API Security {
		typedef unsigned int	Index;

		static const size_t 	s_maxSymbol = 32;
		static const size_t 	s_maxName = 128;
		static const size_t 	s_maxUuid = 24;
		static const size_t 	s_maxIssueType = 8;
		static const size_t 	s_maxIssueClass = 32;
		static const size_t		s_maxCcy = 4;
		static const size_t		s_maxCountryCode = 4;

		static const Index 		s_invalidIndex;
		static const size_t 	s_version;

		size_t 					version = s_version; 					/// Version of this data structure
		Index 					index = s_invalidIndex; 				/// The index of security
		char 					uuid[s_maxUuid] = {'\0'};				/// System specific unique id
		char 					parentUuid[s_maxUuid] = { '\0' };		/// The parent UUID. This can be used to identify multiple class of shares listed from a single company
		char 					name[s_maxName] = {'\0'};				/// The name of the security
		SecId::Type				uuidType = SecId::kCFigi;				/// What is the type of uuid used

		char 					bbTicker[s_maxSymbol] = { '\0' }; 		/// The Bloomberg Ticker
		char 					ticker[s_maxSymbol] = {'\0'};			/// The ticker for the security 
		char 					exchange[s_maxSymbol] = {'\0'};			/// The exchange on which it is listed
		char					instrument[s_maxSymbol] = { '\0' };		/// Internal instrument id that is used for various purposes

		char 					cfigi[s_maxUuid] = { '\0' };			/// The composite FIGI. This is generally the UUID as well
		char 					isin[s_maxUuid] = { '\0' };				/// ISIN
		char 					figi[s_maxUuid] = { '\0' };				/// The exchange FIGI (NOT the composite one, which is used as UUID)
		char 					perfId[s_maxUuid] = { '\0' };			/// The MorninStar perfId

		char 					permId[s_maxUuid] = { '\0' };			/// The Reuters permId
		char 					issuerPermId[s_maxUuid] = { '\0' };		/// The PermId issued by issuer
		char 					quotePermId[s_maxUuid] = { '\0' };		/// The PermId of the quote
		char 					ric[s_maxUuid] = { '\0' };				/// The RIC code for the security

		char					issueClass[s_maxIssueClass] = { '\0' };	/// The issue type
		char					issueType[s_maxIssueType] = {'\0'};		/// The issue class
		char					issueDesc[s_maxIssueClass] = { '\0' };	/// The issue description

		char					currency[s_maxCcy] = { '\0' };			/// The currency that this security is traded in
		char					countryCode2[3] = { '\0' };				/// The country code for this stock
		char					countryCode3[4] = { '\0' };				/// The country code for this stock

		char					bbSecurityType[s_maxIssueClass] = { '\0' }; /// The security type from BB
		char					bbSecurityDesc[s_maxIssueClass] = { '\0' }; /// The security desc from BB

		bool 					isInactive = false;						/// Is this non-active now?
		int						ipoDate = 0;							/// What is the date the security was listed
		int						delistingDate = 0;						/// What was the date the security was delisted

								Security() {}
								Security(const std::string& uuid);
								Security(const Security& rhs);

		std::string 			toString() const;
		std::string				debugString() const;
		bool					isHolidayInExchange(const RuntimeData& rt, IntDay di) const;
		void					merge(const Security& other);
		void					override(const Security& other);

		// Security& 				operator = (const Security& rhs);

		inline std::string		cfigiStr() const							{ return std::string(cfigi); 							}
		inline std::string		isinStr() const								{ return std::string(isin); 							}
		inline std::string		figiStr() const								{ return std::string(figi); 							}
		inline std::string		perfIdStr() const							{ return std::string(perfId); 							}
		inline std::string		permIdStr() const							{ return std::string(permId); 							}
		inline std::string		issuerPermIdStr() const						{ return std::string(issuerPermId);						}
		inline std::string		quotePermIdStr() const						{ return std::string(quotePermId);						}
		inline std::string		ricStr() const								{ return std::string(ric);								}
		inline std::string 		symbolStr() const 							{ return strlen(isin) ? std::string(isin) : std::string(ticker); }
		inline std::string 		nameStr() const 							{ return std::string(name); 							}
		inline std::string		tickerStr() const							{ return std::string(ticker);							}
		inline std::string		bbTickerStr() const 						{ return std::string(bbTicker); 						}
		inline bool				isValid() const								{ return index != s_invalidIndex;						}
		inline std::string		uuidStr() const								{ return std::string(uuid);								}
		inline std::string		issueClassStr() const 						{ return std::string(issueClass); 						}
		inline std::string		issueTypeStr() const 						{ return std::string(issueType); 						}
		inline std::string		issueDescStr() const 						{ return std::string(issueDesc); 						}
		inline std::string		parentUuidStr() const 						{ return std::string(parentUuid);						}
		inline std::string		currencyStr() const 						{ return std::string(currency);							}
		inline std::string		countryCode3Str() const 					{ return std::string(countryCode3);						}
		inline std::string		countryCode2Str() const 					{ return std::string(countryCode2);						}
		inline std::string		bbSecurityTypeStr() const					{ return std::string(bbSecurityType);					}
		inline std::string		bbSecurityDescStr() const					{ return std::string(bbSecurityDesc); 					}
		inline std::string		exchangeStr() const							{ return std::string(exchange);							}
		inline std::string		instrumentId() const						{ return std::string(instrument);						}

		inline void				setUuid(const std::string str) 				{ Util::copyString(uuid, str, sizeof(uuid)); 			}
		inline void				setCFIGI(const std::string str) 			{ Util::copyString(cfigi, str, sizeof(cfigi)); 			}
		inline void				setISIN(const std::string str) 				{ Util::copyString(isin, str, sizeof(isin)); 			}
		inline void				setFIGI(const std::string str) 				{ Util::copyString(figi, str, sizeof(figi)); 			}
		inline void				setPerfId(const std::string str) 			{ Util::copyString(perfId, str, sizeof(perfId));		}
		inline void				setPermId(const std::string str) 			{ Util::copyString(permId, str, sizeof(permId));		}
		inline void				setIssuerPermId(const std::string str) 		{ Util::copyString(issuerPermId, str, sizeof(issuerPermId));}
		inline void				setQuotePermId(const std::string str) 		{ Util::copyString(quotePermId, str, sizeof(quotePermId));}
		inline void				setRIC(const std::string str) 				{ Util::copyString(ric, str, sizeof(ric));				}
		inline void				setName(const std::string str) 				{ Util::copyString(name, str, sizeof(name)); 			}
		inline void				setTicker(const std::string str) 			{ Util::copyString(ticker, str, sizeof(ticker)); 		}
		inline void				setBBTicker(const std::string str) 			{ Util::copyString(bbTicker, str, sizeof(bbTicker)); 	}
		inline void				setExchange(const std::string str) 			{ Util::copyString(exchange, str, sizeof(exchange)); 	}
		inline void				setParentUuid(const std::string str)		{ Util::copyString(parentUuid, str, sizeof(parentUuid));}
		inline void				setIssueClass(const std::string str) 		{ Util::copyString(issueClass, str, sizeof(issueClass));}
		inline void				setIssueType(const std::string str) 		{ Util::copyString(issueType, str, sizeof(issueType)); 	}
		inline void				setIssueDesc(const std::string str) 		{ Util::copyString(issueDesc, str, sizeof(issueDesc)); 	}
		inline void				setCurrency(const std::string str) 			{ Util::copyString(currency, str, sizeof(currency)); 	}
		inline void				setCountryCode3(const std::string str) 		{ Util::copyString(countryCode3, str, sizeof(countryCode3));}
		inline void				setCountryCode2(const std::string str) 		{ Util::copyString(countryCode2, str, sizeof(countryCode2));}
		inline void				setBBSecurityType(const std::string str)	{ Util::copyString(bbSecurityType, str, sizeof(bbSecurityType));}
		inline void				setBBSecurityDesc(const std::string str)	{ Util::copyString(bbSecurityDesc, str, sizeof(bbSecurityDesc));}
		inline void				setInstrumentId(const std::string str)		{ Util::copyString(instrument, str, sizeof(instrument)); }

		inline bool				operator == (const Security& rhs) const 	{ return std::string(uuid) == std::string(rhs.uuid); 	}
		inline bool				operator != (const Security& rhs) const 	{ return !(*this == rhs); 								}
	};

	extern std::ostream& 		operator << (std::ostream& os, const Security& obj);

	typedef std::vector<Security> 					SecurityVec;
	typedef std::shared_ptr<SecurityVec> 			SecurityVecPtr;
	typedef std::vector<Security::Index> 			SecurityIndexVec;
	typedef std::shared_ptr<SecurityIndexVec> 		SecurityIndexVecPtr;
	typedef std::shared_ptr<Security> 				SecurityPtr;

	//////////////////////////////////////////////////////////////////////////
	/// Simulation state
	//////////////////////////////////////////////////////////////////////////
	enum SimulationState {
		kSS_Init				= 0,

		kSS_Prepare				= 5,
		kSS_Running				= 6,
		kSS_Finalise			= 7,

		kSS_Free				= 10,
	};

	//////////////////////////////////////////////////////////////////////////
	/// Quarter
	//////////////////////////////////////////////////////////////////////////
	struct Framework_API Period {
		IntDay					diStart = -1;			/// The starting date of the week/quarter
		IntDay					diEnd = -1;				/// The ending date of the week/quarter
		unsigned int			year = 0;				/// The year for the week/quarter
		unsigned int			index = 0;				/// What week/quarter is it (this is 0 index)

								Period() {}
								Period(IntDay diStart_, IntDay diEnd_, unsigned int year_, unsigned int index_)
									: diStart(diStart_), diEnd(diEnd_), year(year_), index(index_) {}
	};

	typedef Period Quarter;
	typedef Period Week;
	typedef std::shared_ptr<Quarter>	QuarterPtr;
	typedef std::vector<Quarter>		QuarterVec;
	typedef std::shared_ptr<Week>		WeekPtr;
	typedef std::vector<Week>			WeekVec;
	typedef std::unordered_map<IntDate, size_t> IntDateMap;

	////////////////////////////////////////////////////////////////////////////
	/// Runtime Data
	////////////////////////////////////////////////////////////////////////////
	struct Framework_API RuntimeData {
		IDataRegistry* 			dataRegistry = nullptr; /// The data registry
		IntDay 					di = Constants::s_invalidDay; /// The day index for which the data is being calculated
		IntDay					diStart = 0;			/// The starting day of the simulation 
		IntDay					diEnd = 0;				/// The ending day of the simulation (always inclusive i.e. iterate from i = diStart; i <= diEnd)
		UIntCount 				backdays; 				/// The number of backdays for the backtest
		IntDate 				startDate; 				/// The start date as specified in the config minus (-) backdays (adjusted for holidays)
		IntDate 				endDate; 				/// The end date as specified in the config (adjusted for holidays)
		IntDate					configStartDate;		/// The actual start date in the config
		IntDate					configEndDate;			/// The actual end date in the config
		IntDate 				date; 					/// The current date
		Poco::Timespan 			timeZone; 				/// The time zone
		IntDateVec				dates; 					/// The window of dates, this is in the range [startDate - backdays, endDate]
		IntVec					holidaysAhead;			/// For each date di we keep how many holidays are ahead of it. This is useful in getting data that 
														/// is released over the weekend/holiday (some slow moving data). The agreement for such data is that we 
														/// move it to the previous trading day since it falls between two trading dates. We really want this 
														/// data to be available on the next trading day and since the system is tied too much with trading
														/// dates. Hence, we move the data over the weekend to the previous date. 

		std::unordered_map<IntDate, IntDay> datesIndex;	/// The dates index for quick lookup
		IntDateVec				datesHistory; 			/// The historical dates from which the cache was oritinally built
		IntDateBoolMap			holidays;				/// Holidays. This covers AT LEAST the same range as dates. Usually its larger
		std::map<std::string, IntDateBoolMap> exchangeHolidays; /// Exchange specific holiday information

		WeekVec					weeks;					/// The weeks within the [diStart, diEnd] range
		QuarterVec				quarters;				/// The quarters within the [diStart, diEnd] range

		bool					isTodayHoliday = false;	/// Whether today is a holiday or not

		SimulationState			simState;				/// What is the state of the simulation. The IPipelineHandler must set this correctly!
		pesa::AppOptions		appOptions;				/// The application options that were passed along

		////////////////////////////////////////////////////////////////////////////
		/// Trading timings in local time as well as UTC
		////////////////////////////////////////////////////////////////////////////
#ifndef __SCRIPT__
		std::vector<int64_t>	utcTradingStartTime;		/// The trading start time in EPOCH MICROSECONDS (UTC)
		std::vector<int64_t>	utcTradingEndTime;			/// The trading end time in EPOCH MICROSECONDS (UTC)

		std::vector<Poco::DateTime>	utcTradingStartTime_DT;	/// The trading start time in Poco::DateTime (UTC - with seconds and microseconds rounded down to 0)
		std::vector<Poco::DateTime>	utcTradingEndTime_DT;	/// The trading end time in Poco::DateTime (UTC - with seconds and microseconds rounded down to 0)

		std::vector<bool>		isDST;						/// Is 'di' a DST time?
#endif 

		//FloatVec				utcTradingStartTime;	/// The trading start time in EPOCH MICROSECONDS (UTC)
		//FloatVec				utcTradingEndTime;		/// The trading end time in EPOCH MICROSECONDS (UTC)

		//DateTimeVec				tradingStartTime; 		/// Trading starting time. This covers the same range as the dates (LOCAL TIME)
		//DateTimeVec				tradingEndTime; 		/// Ending time for the trade. This covers the same range as the dates (LOCAL TIME)
		//DateTimeVec				utcTradingStartTime;	/// Trading starting time. This covers the same range as the dates (LOCAL TIME)
		//DateTimeVec				utcTradingEndTime; 		/// Ending time for the trade. This covers the same range as the dates (LOCAL TIME)
		//LocalDateTimeVec		localTradingStartTime;	/// Trading starting time. This covers the same range as the dates (LOCAL TIME)
		//LocalDateTimeVec		localTradingEndTime; 	/// Ending time for the trade. This covers the same range as the dates (LOCAL TIME)

		//////////////////////////////////////////////////////////////////////////
		/// Period related data
		//////////////////////////////////////////////////////////////////////////
		SizeVecPtr				weekIndexMap;			/// Index map that maps weeks to dates (this is shared by the Data.h class(es))
		SizeVecPtr				quarterIndexMap;		/// Index map that maps quarters to dates (this is shared by the Data.h class(es))

		//////////////////////////////////////////////////////////////////////////
		/// Helper functions
		//////////////////////////////////////////////////////////////////////////

		IntDay 					getDateIndex(IntDate date) const;
		IntDay					getExactDateIndex(IntDate date) const;
		void					initPeriods();
		bool					isLastDi(IntDay di = 0) const;

#ifndef __SCRIPT__
		bool					isTimeAfterMarketOpen(const Poco::DateTime& dt) const;
#endif 

		bool					isAfterMarketOpen() const;
		bool					isBeforeMarketClose() const;
		bool					isInTradingHours() const;

		////////////////////////////////////////////////////////////////////////////
		/// Inline functions: Some helper inline functions
		////////////////////////////////////////////////////////////////////////////
		inline IntDay			diSimStart() const { return diStart; }
		inline IntDay			diSimEnd() const { return !isTodayHoliday ? diEnd : diEnd - 1; }

		bool					isHolidayInExchange(const std::string& exchange, IntDay di) const;

		inline IntDate 			getStartDate() const {
			ASSERT(dates.size() > 0, "Empty dates array!");
			return dates[0];
		}

		inline IntDate 			getEndDate() const {
			ASSERT(dates.size() > 0, "Empty dates array!");
			return dates[dates.size() - 1];
		}

		inline IntDate			getDate(size_t index) const {
			ASSERT(index < dates.size(), "Invalid date index: " << index);
			return dates[index];
		}

		inline bool				isLastDi(IntDay di = 0) {
			if (di == 0)
				di = this->di;
			if (!isTodayHoliday)
				return di == this->diEnd;
			return di == this->diEnd - 1;
		}


		inline std::string		platform() const {
		#ifdef __MACOS__
			return "mac";
		#elif __WINDOWS__
			return "windows";
		#elif __LINUX__
			return "linux";
		#else 
			ASSERT(false, "Unknown platform!");
		#endif /// __MACOS__
		}

		inline void				exit(int exitCode) { Util::exit(exitCode); }
		// inline IntDate 			currentDataDate() const 	{ return dates[this->di]; 	}
		// inline IntDate 			currentData() const 		{ return dates[this->di]; 		}
		// inline IntDate 			dataDate(IntDay di) const 	{ return dates[di]; 		}
		// inline IntDate 			date(IntDay di) const 		{ return dates[di]; 			}
	};

	//////////////////////////////////////////////////////////////////////////
	/// DataType: Different data types that are supported in the system
	/// NEVER CHANGE THE ORDER OF THESE ENUMS ... ADD MORE AT THE END IF NEEDED
	struct Framework_API DataType {
		enum Type {
			kUnknown, 										/// Unknown item type. Its ok because for the most part the DataRegistry is agnostic to the actual type

			kFloat,
			kDouble,
			kInt,
			kInt64,

			kFloatArray,
			kIntArray,

			kInt16,
			kUInt16,

			kUInt,
			kUIntArray,

			kLong,
			kLongArray,

			kULong,
			kULongArray,

			kString,
			kBlob,

			kSecurityData,
			kFloatConstant,									/// Constant float value as FloatMatrixData
			kTimestamp,										/// Time stamp

			kStringArray,

			kDateTime,

			kVarFloat,
			kVarInt,
			kVarDouble,
			kVarDateTime,
			kVarTimestamp,

			kVarString,

			kVarDataEnd, 

			/// START - Add new values here

			kIndexedString,									/// Indexed (int16) string to save space and loading times for string data
			kIndexedStringMetadata,							/// Indexed (int16) string Metadata

			kLongIndexedString,								/// Long (int32) Indexed string to save space and loading times for string data
			kLongIndexedStringMetadata,						/// Long (int32) Indexed string Metadata

			kVarLongIndexedString,							/// Variable length indexed string

			/// END - Add new values here

			kCustom = 0x00FFFFFF							/// Custom data. Could be a struct or a complex data type
		};

		static Type parse(const std::string& stype);
		static size_t size(Type t);
		static std::string toString(Type type);

#ifndef __SCRIPT__ /// No SWIG
		template <typename t_type>
		Type			get()					{ return kUnknown; }
#endif /// __SCRIPT__
	};

	template<> DataType::Type inline DataType::get<float>()            { return kFloat; }
	template<> DataType::Type inline DataType::get<std::string>()      { return kString; }
	template<> DataType::Type inline DataType::get<int>()              { return kUInt; }
	template<> DataType::Type inline DataType::get<unsigned int>()     { return kInt; }
	template<> DataType::Type inline DataType::get<uint16_t>()         { return kUInt16; }
	template<> DataType::Type inline DataType::get<double>()           { return kDouble; }
	template<> DataType::Type inline DataType::get<long>()             { return kLong; }
	template<> DataType::Type inline DataType::get<unsigned long>()    { return kULong; }

	//////////////////////////////////////////////////////////////////////////
	/// DataFrequency: The different frequency intervals that are supproted
	//////////////////////////////////////////////////////////////////////////
	struct Framework_API DataFrequency {
		enum Type {
			kStatic 					= 0,				/// There is just one copy of the data
			kUniversal					= kStatic,			/// Backwards compatibility
			kDaily						= 1,
			kWeekly						= 7,
			kMonthly					= 30,
			kQuarterly					= 365 / 4,
			kSemiAnnually				= 365 / 2,
			kAnnually 					= 365,

			kIntraday					= 999,
		};

		static Type parse(const std::string& stype);
		static std::string toString(Type ftype);
	};

	//////////////////////////////////////////////////////////////////////////
	/// DataFlags: The different data flags that we support
	//////////////////////////////////////////////////////////////////////////
	struct Framework_API DataFlags {
		enum Type {
			kNone 						= 0,				/// Nothing special
			kNeedsHistoryDuringUpdate 	= (1 << 0), 		/// The previous data history needs to be provided during the update
															/// (The IDataLoader is responsible for keeping its own cache for the current update(s))

			kUpdatesInOneGo 			= (1 << 1),			/// Updates all the data in the [startDate, endDate] range in one go!
			kAlwaysLoad 				= (1 << 2),			/// Always load this data
			kAlwaysOverwrite			= (1 << 3),			/// Always overwrite the old data with new one
			kSecurityMaster				= (1 << 4),			/// Data is security master
			kCollectAllBeforeWriting	= (1 << 5),			/// Collect all the data before writing it to disk
			kNoCopyFromPrevVersion		= (1 << 6),			/// Collect all the data before writing it to disk
			kUpdateDuringTradingHours	= (1 << 7),			/// Update the data ONLY during trading hours
			kUpdateAfterMarketOpen		= (1 << 8),			/// Update the data ONLY AFTER market open on the day
			kUpdateBeforeMarketClose	= (1 << 9),			/// Update the data ONLY BEFORE market close on the day

			kPreLoadExisting			= (1 << 10),		/// Pre-load the existing data into a DataPtr and pass it with DataCacheInfo
		};
	};

	//////////////////////////////////////////////////////////////////////////
	/// ContractDelivery: The delivery information about contracts
	//////////////////////////////////////////////////////////////////////////
	struct Framework_API Futures {
		enum Month {
			kJanuary			= 0,
			kFebruary			= 1,
			kMarch				= 2,
			kApril				= 3,
			kMay				= 4,
			kJune				= 5,
			kJuly				= 6,
			kAugust				= 7,
			kSeptember			= 8,
			kOctober			= 9,
			kNovember			= 10,
			kDecember			= 11
		};

		static Month parse(char code);
		static char code(Month month);
	};

	////////////////////////////////////////////////////////////////////////////
	/// DataDefinition: Represents each of the individual data values within a dataset
	////////////////////////////////////////////////////////////////////////////
	struct Dataset;
	class IUniverse;

	struct Framework_API DataDefinition {
		static const size_t 	s_maxName = 256;				/// Maximum number of chars for the name
		static const size_t 	s_version;

		static DataDefinition	s_null;							/// Null data definition
		static DataDefinition	s_int;
		static DataDefinition	s_long;
		static DataDefinition	s_uint32_t;
		static DataDefinition	s_ulong_t;
		static DataDefinition	s_int64_t;
		static DataDefinition	s_float;
		static DataDefinition	s_double;
		static DataDefinition	s_string;
		static DataDefinition	s_istring;
		static DataDefinition	s_int16_t;
		static DataDefinition	s_uint16_t;

		size_t 					version = s_version;			/// The version. Used by DataRegistry for loading correct data
		char 					name[s_maxName] = { '\0' };		/// The name of the data set
		DataType::Type			itemType = DataType::kFloat;	/// What type of item it is. Most are floating point values
		size_t 					itemSize = sizeof(float);		/// What is the size of the item. This MUST always be set correctly
		UIntCount				frequency = (UIntCount)DataFrequency::kDaily; /// The frequency in days of the data
		unsigned int 			flags = DataFlags::kNone;		/// Any special update required
		char					typeName[s_maxName] = { '\0' };	/// The typename for this data


								DataDefinition();
								DataDefinition(const DataDefinition& rhs);
								DataDefinition(std::string name);
								DataDefinition(DataType::Type itemType, size_t itemSize);
								DataDefinition(std::string name, DataType::Type itemType, size_t itemSize, UIntCount frequency, unsigned int flags, const char* typeName = nullptr);

		bool 					operator == (const DataDefinition& rhs) const;
		DataDefinition& 		operator = (const DataDefinition& rhs);
		std::string 			toString(const Dataset& ds) const;

		Poco::Path 				path(IUniverse* universe, const std::string& rootPath, const std::string& dataName) const;
		Poco::Path 				path(const std::string& universeName, const std::string& rootPath, const std::string& dataName) const;

		static void				parseDefString(const std::string def, std::string& dataName, DataType::Type& type, DataFrequency::Type& frequency);

#ifndef __SCRIPT__ /// No SWIG
		template <typename t_type>
		static DataDefinition				getDefault() { return DataDefinition::s_null; }
#endif /// __SCRIPT__

		inline bool				isInt() const {
			switch (itemType) {
				case DataType::kInt: 
				case DataType::kIntArray: 
				case DataType::kInt64: 
				case DataType::kTimestamp: 
				case DataType::kDateTime: 
				case DataType::kUInt: 
				case DataType::kUIntArray: 
				case DataType::kLong: 
				case DataType::kLongArray: 
				case DataType::kULong: 
				case DataType::kULongArray: 
					return true;
				default:
					break;
			}

			return false;
		}

		inline bool				isFloat() const {
			switch (itemType) {
				case DataType::kFloat:
				case DataType::kFloatArray:
				case DataType::kDouble:
				case DataType::kUInt:
				case DataType::kLong:
				case DataType::kULong:
					return true;
				default:
					break;
			}

			return false;
		}

		inline bool				isDaily() const {
			return frequency == (UIntCount)DataFrequency::kDaily;
		}

		inline bool				isWeekly() const {
			return frequency == (UIntCount)DataFrequency::kWeekly;
		}

		inline bool				isQuarterly() const {
			return frequency == (UIntCount)DataFrequency::kQuarterly;
		}

		inline bool				isConstant() const {
			return itemType == DataType::kFloatConstant;
		}

		inline bool				doesUpdateInOneGo() const {
			return (flags & DataFlags::kUpdatesInOneGo) != 0 || isConstant();
		}

		inline bool 			needsHistoryDuringUpdate() const {
			return (flags & DataFlags::kNeedsHistoryDuringUpdate) != 0;
		}

		inline bool 			isStatic() const {
			return frequency == (UIntCount)DataFrequency::kStatic;
		}

		inline bool 			isUniversal() const {
			return frequency == (UIntCount)DataFrequency::kStatic;
		}

		inline bool				isAlwaysOverwrite() const {
			return (flags & DataFlags::kAlwaysOverwrite) != 0 || isConstant(); /// Constant data is always overwritten!
		}

		inline bool				isSecurityMaster() const {
			return (flags & DataFlags::kSecurityMaster) != 0;
		}

		inline bool				collectAllBeforeWriting() const {
			return (flags & DataFlags::kCollectAllBeforeWriting) != 0;
		}

		inline bool				isNoCopyFromPrevVersion() const {
			return (flags & DataFlags::kNoCopyFromPrevVersion) != 0;
		}

		inline bool				isUpdateDuringTradingHours() const {
			return (flags & DataFlags::kUpdateDuringTradingHours) != 0;
		}

		inline bool				isUpdateAfterMarketOpen() const {
			return (flags & DataFlags::kUpdateAfterMarketOpen) != 0;
		}

		inline bool				isUpdateBeforeMarketClose() const {
			return (flags & DataFlags::kUpdateBeforeMarketClose) != 0;
		}

		inline bool				isVariableLengthData() const {
			return itemType >= DataType::kVarFloat && itemType < DataType::kVarDataEnd;
		}

		inline bool				isIntraday() const {
			return frequency == DataFrequency::kIntraday;
		}

		inline void				setName(const std::string& name) {
			ASSERT(name.length() < s_maxName, "Cannot have a name longer than: " << s_maxName << " (" << name << ")");
			strncpy(this->name, name.c_str(), s_maxName - 1);
		}

		inline void				setTypeName(std::string typeName) {
			ASSERT(typeName.length() < s_maxName, "Cannot have a typename longer than: " << s_maxName << " (" << typeName << ")");
			strncpy(this->typeName, typeName.c_str(), s_maxName - 1);
		}
	};


	template <> DataDefinition   inline DataDefinition::getDefault<int>() { return DataDefinition::s_int; }
	template <> DataDefinition   inline DataDefinition::getDefault<unsigned int>() { return DataDefinition::s_uint32_t; }
	template <> DataDefinition   inline DataDefinition::getDefault<float>() { return DataDefinition::s_float; }
	template <> DataDefinition   inline DataDefinition::getDefault<uint16_t>() { return DataDefinition::s_uint16_t; }
	template <> DataDefinition   inline DataDefinition::getDefault<unsigned long>() { return DataDefinition::s_ulong_t; }
	template <> DataDefinition   inline DataDefinition::getDefault<long>() { return DataDefinition::s_long; }
	template <> DataDefinition   inline DataDefinition::getDefault<double>() { return DataDefinition::s_double; }
	template <> DataDefinition   inline DataDefinition::getDefault<std::string>() { return DataDefinition::s_string; }
}

