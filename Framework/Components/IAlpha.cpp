/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// IAlpha.cpp
///
/// Created on: 14 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "IAlpha.h"
#include "Core/Math/Stats.h"
#include "Framework/Data/MatrixData.h"
#include "Framework/Components/IUniverse.h"
#include "Framework/Components/ICombination.h"

namespace pesa {
	IAlpha::IAlpha(const ConfigSection& config, const std::string& logChannel)
		: IComponent(config, !logChannel.empty() ? logChannel : "ALPHA") {
		m_pinfo.delay			= this->config().get<int>("delay", m_pinfo.delay);
		m_pinfo.days 			= this->config().get<int>("days", m_pinfo.days);
		m_pinfo.omitDays		= this->config().get<int>("omitDays", m_pinfo.omitDays);
		m_pinfo.delta 			= this->config().get<int>("delta", m_pinfo.delta);
		m_pinfo.dayOfWeek		= this->config().get<int>("dayOfWeek", m_pinfo.dayOfWeek);
		m_pinfo.weight			= this->config().get<float>("weight", m_pinfo.weight);
		bool reversion			= this->config().getBool("reversion", false);
		m_pinfo.reversion		= reversion ? -1.0f : 1.0f;

		m_pinfo.shortTermDays	= this->config().get<int>("shortTermDays", m_pinfo.shortTermDays);
		m_pinfo.longTermDays	= this->config().get<int>("longTermDays", m_pinfo.longTermDays);

		if (m_pinfo.shortTermDays == m_pinfo.longTermDays) {
			m_pinfo.longTermDays= m_pinfo.days;
			m_pinfo.shortTermDays= (IntDay)std::round((float)m_pinfo.days / 3.0f);
		}

		/// This will initialise the state!
		resetState();

		m_data->delay			= m_pinfo.delay;
		m_data->config			= &this->config();
	}

	IAlpha::~IAlpha() {
	}

	void IAlpha::resetState() {
		m_yesterdayData = m_data;
		m_data = std::make_shared<CalcData>();
	}

	std::string IAlpha::uuid() const {
		return config().get("uuid");
	}

	bool IAlpha::needsCleanState() const {
		return false;
	}

	float IAlpha::bookSize() {
		return parentConfig()->portfolio().bookSize();
	}

	const FloatMatrixData& IAlpha::getFloatArg(size_t index, const std::string& defaultId) {
		if (index < m_dynamicArgs.size())
			return *m_dynamicArgs[index];

		/// otherwise see if an arg parameter was given
		std::string dataId = config().get<std::string>("arg" + Util::cast(index), !defaultId.empty() ? defaultId : Constants::s_defClosePrice);
		const FloatMatrixData* data = getData<FloatMatrixData>(dataId);
		ASSERT(data, "Unable to get data: " << dataId);
		return *data;
	}

	FloatMatrixDataPtr IAlpha::getFloatArgPtr(size_t index, const std::string& defaultId, bool* isDynamic /* = nullptr */) {
		if (index < m_dynamicArgs.size()) {
			if (isDynamic)
				*isDynamic = true;
			return m_dynamicArgs[index];
		}

		if (isDynamic)
			*isDynamic = false;

		/// otherwise see if an arg parameter was given
		std::string dataId = config().get<std::string>("arg" + Util::cast(index), !defaultId.empty() ? defaultId : Constants::s_defClosePrice);
		return getDataPtr<FloatMatrixData>(dataId);
	}

	const FloatMatrixData* IAlpha::matArg(const std::string& name) {
		FloatMatrixDataPtrStrMap::const_iterator iter = m_namedVars.find(name);

		if (iter != m_namedVars.end())
			return iter->second.get();

		return nullptr;
	}

	const FloatMatrixData& IAlpha::getNamedVar(const std::string& name) {
		FloatMatrixDataPtrStrMap::const_iterator iter = m_namedVars.find(name);

		if (iter != m_namedVars.end())
			return *(iter->second);

		std::string value;
		bool didFind = config().get("exponent", value);

		ASSERT(didFind && !value.empty(), "Unable to find data: " << name);

		/// convert it to float and add it to the named variable list
		float fvalue = Util::cast<float>(value);
		return *(addNamedVar(name, std::make_shared<ConstantFloat>(fvalue)));
	}

	FloatMatrixDataPtr IAlpha::addNamedVar(const std::string& name, FloatMatrixDataPtr var) {
		m_namedVars[name] = var;
		return var;
	}


	float IAlpha::firstValidValue(const FloatMatrixData& data, IntDay di, size_t ii, IntDay startOffset /* = 1 */) const {
		JJ_LOOP_O(startOffset) {
			float value = data(di - jj, ii);
			if (!std::isnan(value)) 
				return value;
		}

		return IAlpha::invalid();
	}

	size_t IAlpha::history(FloatVec& history, const FloatMatrixData& data, IntDay di, size_t ii, IntDay days) const {
		history.clear();

		if (!days)
			days = m_pinfo.days;

		JJ_LOOP_D(m_pinfo.omitDays, days, m_pinfo.delta) {
			float backValue = data(di - jj, ii);

			if (!std::isnan(backValue))
				history.push_back(backValue);
		}

		return history.size();
	}

	float IAlpha::mean(const FloatMatrixData& data, IntDay di, size_t ii, IntDay days) const {
		FloatVec history;
		this->history(history, data, di, ii, days);

		if (!history.size())
			return std::nanf("");

		return Stats::mean(&history[0], history.size());
	}

	float IAlpha::median(const FloatMatrixData& data, IntDay di, size_t ii, IntDay days) const {
		FloatVec history;
		this->history(history, data, di, ii, days);

		if (!history.size())
			return std::nanf("");

		return Stats::medianUnsorted(&history[0], history.size());
	}

	float IAlpha::stddev(const FloatMatrixData& data, IntDay di, size_t ii, IntDay days) const {
		FloatVec history;
		this->history(history, data, di, ii, days);

		if (!history.size())
			return std::nanf("");

		return Stats::standardDeviation(&history[0], history.size());
	}

	int IAlpha::upDays(const FloatMatrixData& data, IntDay di, size_t ii, IntDay days) const {
		int numUpDays = 0;

		if (!days)
			days = m_pinfo.days;

		JJ_LOOP_D(m_pinfo.omitDays, days, m_pinfo.delta) {
			float value = data(di - jj, ii);
			float prevValue = data(di - jj - 1, ii);

			if (!std::isnan(value) && !std::isnan(prevValue) &&
				value > prevValue)
				numUpDays++;
		}

		return numUpDays;
	}

	int IAlpha::downDays(const FloatMatrixData& data, IntDay di, size_t ii, IntDay days) const {
		int numDownDays = 0;

		if (!days)
			days = m_pinfo.days;

		JJ_LOOP_D(m_pinfo.omitDays, days, m_pinfo.delta) {
			float value = data(di - jj, ii);
			float prevValue = data(di - jj - 1, ii);

			if (!std::isnan(value) && !std::isnan(prevValue) &&
				value < prevValue)
				numDownDays++;
		}

		return numDownDays;
	}

	float IAlpha::min(const FloatMatrixData& data, IntDay di, size_t ii, IntDay days) const {
		float lowValue = std::nanf("0");

		if (!days)
			days = m_pinfo.days;

		JJ_LOOP_D(m_pinfo.omitDays, days, m_pinfo.delta) {
			float value = data(di - jj, ii);
			if (!std::isnan(value) && (value < lowValue || std::isnan(lowValue)))
				lowValue = value;
		}

		return lowValue;
	}

	float IAlpha::max(const FloatMatrixData& data, IntDay di, size_t ii, IntDay days) const {
		float highValue = std::nanf("0");

		if (!days)
			days = m_pinfo.days;

		JJ_LOOP_D(m_pinfo.omitDays, days, m_pinfo.delta) {
			float value = data(di - jj, ii);
			if (!std::isnan(value) && (value > highValue || std::isnan(highValue)))
				highValue = value;
		}

		return highValue;
	}

	////////////////////////////////////////////////////////////////////////////
	/// ComboResult - Utility class. ICombination results are NOT guaranteed
	////////////////////////////////////////////////////////////////////////////
	//ComboResult::ComboResult(const std::string& id, const ConfigSection& srcConfig)
	//	: IAlpha(srcConfig, "COMBO_RESULT") {
	//	//m_alphaConfig->set("id", id);
	//	//m_alphaConfig->set("uuid", id);
	//	m_dbWriteStats = false;
	//}

	ComboResult::ComboResult(const std::string& id, const ConfigSection& srcConfig)
	 : IAlpha(*(new ConfigSection(const_cast<Config*>(srcConfig.config()), srcConfig, false)), "COMBO_RESULT")
	 , m_alphaConfig(const_cast<ConfigSection*>(&IAlpha::config())) {
		m_alphaConfig->set("id", id);
		m_alphaConfig->set("uuid", id);
		m_dbWriteStats = m_alphaConfig->getBool("dbWriteStats", false);
	}
		
	ComboResult::~ComboResult() {
	}

	void ComboResult::run(const RuntimeData& rt) { 
		ASSERT(false, "Pointless running ComboResult alpha");
	}

	bool IAlpha::isValid(pesa::IntDay di, size_t ii) const { 
		return universe()->isValid(di, ii); 
	}

	//////////////////////////////////////////////////////////////////////////
	DummyAlpha::DummyAlpha(const ConfigSection& config) : IAlpha(config, "DummyAlpha") {
	}

	DummyAlpha::~DummyAlpha() {
	}

	void DummyAlpha::run(const RuntimeData& rt) {
		/// NO-OP
	}
} /// namespace pesa
