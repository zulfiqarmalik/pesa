/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// IAlpha.h
///
/// Created on: 4 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/FrameworkDefs.h"
#include "IComponent.h"
#include "Core/Lib/Util.h"
#include <vector>
#include "Framework/Data/Data.h"
#include "Framework/Data/CalcData.h"
#include "Framework/Pipeline/IDataRegistry.h"

namespace pesa {
	/// Some optional alpha data that may be passed to the alpha and that can 
	/// be used to change the default behaviour of the alpha
	struct Framework_API AlphaProcessInfo {
		float 								weight = 1.0f; 			/// The weight of this alpha
		IntDay								delay = 1;				/// Delay for the alpha
		float								reversion = 1.0f;		/// Whether we are testing reversion or not (1 = NO REVERSION, -1 = REVERSION)...

		IntDay 								days = 5;				/// How many days in the past do we wanna go
		IntDay 								omitDays = 0;			/// How many days (closest to current day i.e. di) should we omit
		IntDay 								delta = 1;				/// How to loop from [di - omitDays, di - days]
		IntDay								dayOfWeek = -1; 		/// Which day to start with. -1 will start from di
																	/// 0 = Monday, 1 = Tuesday ... 6 = Sunday
																	/// So for a value of 0 the loop [di - omitDays, di - days]
																	/// will round di - omitDays and di - days to the nearest
																	/// Monday. Same for all the other days

		//////////////////////////////////////////////////////////////////////////
		/// For alphas that use a combination of short term and long term outlook
		/// on a certain dataset
		//////////////////////////////////////////////////////////////////////////
		IntDay								shortTermDays = 5;		/// The number of days to look for short-term outlook
		IntDay								longTermDays = 5;		/// The number of days to look for long-term outlook
	};

	class Framework_API IAlpha : public IComponent {
	protected:
		pesa::AlphaProcessInfo 				m_pinfo; 				/// The alpha processing information
		pesa::CalcDataPtr					m_data;					/// The data that this alpha is working with (see ComponentDefs.h for more details)
		FloatMatrixDataPtrVec				m_dynamicArgs;			/// The dynamic args that are passed to this alpha
		FloatMatrixDataPtrStrMap			m_namedVars;			/// Named variables that we've been given access to

		bool								m_collectStats = true;	/// Collect the stats for this alpha 
		bool								m_fileWriteStats = true;/// Should we write the stats of this alpha to file
		bool								m_dbWriteStats = true;	/// Should we write the stats of this alpha to the DB

		pesa::CalcDataPtr					m_yesterdayData;		/// Yesterday's data (if available)

	public:
											IAlpha(const ConfigSection& config, const std::string& logChannel);
											~IAlpha();

		bool								isValid(pesa::IntDay di, size_t ii) const;
		const FloatMatrixData&				getFloatArg(size_t index, const std::string& defaultId = Constants::s_defClosePrice);
		pesa::FloatMatrixDataPtr			getFloatArgPtr(size_t index, const std::string& defaultId = Constants::s_defClosePrice, bool* isDynamic = nullptr);
		virtual const pesa::FloatMatrixData& getNamedVar(const std::string& name);

		//////////////////////////////////////////////////////////////////////////
		virtual const pesa::FloatMatrixData* matArg(const std::string& name);

		template <class t_type>
		t_type								arg(const std::string& name, t_type defValue = 0) {
			const auto* var = dynamic_cast<const ConstantFloat*>(matArg(name));
			return var ? (t_type)var->value() : defValue;
		}
		//////////////////////////////////////////////////////////////////////////

		pesa::FloatMatrixDataPtr			addNamedVar(const std::string& name, pesa::FloatMatrixDataPtr var);
		virtual float						bookSize();

		virtual std::string 				uuid() const;
		virtual bool						needsCleanState() const;
		virtual void						resetState();

		////////////////////////////////////////////////////////////////////////////
		/// Inline functions
		////////////////////////////////////////////////////////////////////////////
		inline float& 						alpha(size_t i) 		{ return (*m_data->alpha)(0, i); 			}
		inline const float& 				alpha(size_t i) const 	{ return (*m_data->alpha)(0, i); 			}
		inline float* 						alphaPtr() 				{ return m_data->alpha->fdata(); 			}
		inline const float* 				alphaPtr() const		{ return m_data->alpha->fdata(); 			}
		inline pesa::FloatMatrixData&		alpha() 				{ return *m_data->alpha; 					}
		inline const pesa::FloatMatrixData& remappedAlpha() const	{ return *m_data->remappedAlpha;			}
		inline const pesa::FloatMatrixData& alpha() const 			{ return *m_data->alpha;					}
		inline const IUniverse* 			universe() const 		{ return m_data->universe; 					}
		inline IUniverse*& 					universe() 				{ return m_data->universe; 					}
		inline const pesa::ConfigSection&	config() const 			{ return IComponent::config<ConfigSection>(); }
		inline pesa::AlphaProcessInfo&		pinfo() 				{ return m_pinfo; 							}
		inline const pesa::AlphaProcessInfo& pinfo() const			{ return m_pinfo; 							}
		inline const float& 				weight() const 			{ return m_pinfo.weight; 					}
		inline float& 						weight() 				{ return m_pinfo.weight; 					}
		inline IntDay&						delay() 				{ return m_pinfo.delay; 					}
		inline IntDay						delay() const 			{ return m_pinfo.delay; 					}
		inline pesa::Statistics&			stats()					{ return m_data->stats; 					}
		inline const pesa::Statistics&		stats() const			{ return m_data->stats; 					}

		inline pesa::CalcData& 				data()					{ return *(m_data.get());					}
		inline const pesa::CalcData&		data() const			{ return *(m_data.get());					}

		/// Prefer using IAlpha::state over IAlpha::data. At some point, IAlpha
		//inline pesa::CalcData*				pdata() 				{ return m_data.get();						}
		//inline const pesa::CalcData*		pdata() const			{ return m_data.get(); 						}
		inline pesa::CalcDataPtr&			dataPtr()				{ return m_data;							}
		//inline const pesa::CalcDataPtr&		dataPtr() const			{ return m_data;							}

		//inline const pesa::CalcData*		dataPtr() const			{ return m_data.get();						}
		inline const pesa::CalcData*		yesterdayData() const	{ return m_yesterdayData.get();				}
		inline pesa::CalcDataPtr&			yesterdayDataPtr()		{ return m_yesterdayData;					}
		inline pesa::CalcDataPtr			yesterdayDataPtr() const { return m_yesterdayData;					}
		inline const
			pesa::LifetimeStatistics*		cumStats() const		{ return m_yesterdayData ? m_yesterdayData->cumStats.get() : nullptr; }

		inline pesa::FloatMatrixDataPtrVec& dynamicArgs()			{ return m_dynamicArgs; 					}
		inline const pesa::FloatMatrixDataPtrVec& dynamicArgs() const { return m_dynamicArgs; 					}
		inline float&						reversion()				{ return m_pinfo.reversion;					}
		inline float						reversion() const		{ return m_pinfo.reversion;					}
		inline IntDay						backdays() const		{ return parentConfig()->defs().backdays(); }
		inline pesa::FloatMatrixDataPtrStrMap namedVars()	{ return m_namedVars;						}

		inline bool 						collectStats() const 	{ return m_collectStats;					}
		inline bool 						fileWriteStats() const 	{ return m_fileWriteStats;					}
		inline bool 						dbWriteStats() const 	{ return m_dbWriteStats;					}
		
		inline bool& 						collectStats() 			{ return m_collectStats;					}
		inline bool& 						fileWriteStats() 		{ return m_fileWriteStats;					}
		inline bool& 						dbWriteStats() 			{ return m_dbWriteStats;					}

		inline size_t						getNumArgs() const		{ return m_dynamicArgs.size();				}
		inline std::string					iid() const				{ return config().getString("iid", "");		}
		
		static inline std::string			genId(IntDay di, const char* prefix = "Alphas") { return std::string(prefix) + std::string(".") + Util::cast<IntDay>(di);}
		static inline float					invalid()				{ return std::nanf("0");					} 

		template <class t_type>
		const std::shared_ptr<t_type>		getDataPtr(const std::string& dataId) {
			ASSERT(m_dataRegistry, "Invalid data registry for alpha!");
			DataPtr data = m_dataRegistry->getDataPtr(m_data->universe, dataId);

			if (!data)
				return nullptr;

			return std::dynamic_pointer_cast<t_type>(data);
		}

		template <class t_type>
		const t_type* 						getData(const std::string& dataId) {
			ASSERT(m_dataRegistry, "Invalid data registry for alpha!");
			const pesa::Data* data = m_dataRegistry->getData(m_data->universe, dataId);

			if (!data)
				return nullptr;

			return dynamic_cast<const t_type*>(data);
		}

		//////////////////////////////////////////////////////////////////////////
		/// Some utility functions that can be used by everyone
		//////////////////////////////////////////////////////////////////////////
		float								firstValidValue(const FloatMatrixData& data, IntDay di, size_t ii, IntDay startOffset = 1) const;
		size_t								history(FloatVec& history, const FloatMatrixData& data, IntDay di, size_t ii, IntDay days = 0) const;
		float								mean(const FloatMatrixData& data, IntDay di, size_t ii, IntDay days = 0) const;
		float								median(const FloatMatrixData& data, IntDay di, size_t ii, IntDay days = 0) const;
		float								stddev(const FloatMatrixData& data, IntDay di, size_t ii, IntDay days = 0) const;
		float 								min(const FloatMatrixData& data, IntDay di, size_t ii, IntDay days = 0) const;
		float 								max(const FloatMatrixData& data, IntDay di, size_t ii, IntDay days = 0) const;
		int									upDays(const FloatMatrixData& data, IntDay di, size_t ii, IntDay days = 0) const;
		int 								downDays(const FloatMatrixData& data, IntDay di, size_t ii, IntDay days = 0) const;

		//////////////////////////////////////////////////////////////////////////
		/// SCRIPT/PYTHON - DO NOT USE OTHERWISE
		//////////////////////////////////////////////////////////////////////////
		inline pesa::AlphaProcessInfo*		getPInfo()	{ return &m_pinfo;						}
		inline int							getBackdays()	{ return parentConfig()->defs().backdays(); }
		inline int 							getDelay() 		{ return m_pinfo.delay; 					}
		inline pesa::Data*					dataPtr(const std::string& dataId)	{ return const_cast<Data*>(getData<Data>(dataId)); }
		inline pesa::FloatMatrixData*		floatData(const std::string& dataId)	{ return const_cast<FloatMatrixData*>(getData<FloatMatrixData>(dataId)); }
		inline pesa::DoubleMatrixData*		doubleData(const std::string& dataId) { return const_cast<DoubleMatrixData*>(getData<DoubleMatrixData>(dataId)); }
		inline pesa::IntMatrixData*			intData(const std::string& dataId) { return const_cast<IntMatrixData*>(getData<IntMatrixData>(dataId)); }
		inline pesa::UIntMatrixData*		uintData(const std::string& dataId) { return const_cast<UIntMatrixData*>(getData<UIntMatrixData>(dataId)); }
		inline pesa::LongMatrixData*		longData(const std::string& dataId) { return const_cast<LongMatrixData*>(getData<LongMatrixData>(dataId)); }
		inline pesa::ULongMatrixData*		ulongData(const std::string& dataId) { return const_cast<ULongMatrixData*>(getData<ULongMatrixData>(dataId)); }
		inline pesa::IUniverse* 			getUniverse() { return m_data->universe; }
		//////////////////////////////////////////////////////////////////////////
	};

	typedef std::shared_ptr<IAlpha>			IAlphaPtr;
	typedef std::vector<IAlphaPtr> 			IAlphaPtrVec;

	////////////////////////////////////////////////////////////////////////////
	/// ComboResult - Utility class. ICombination results are NOT guaranteed
	/// to be of this type. Only assume IAlpha result type
	////////////////////////////////////////////////////////////////////////////
	class Framework_API ComboResult : public IAlpha {
		ConfigSection*						m_alphaConfig = nullptr;

	public:
											ComboResult(const std::string& id, const ConfigSection& srcConfig);
											~ComboResult();
		void								run(const RuntimeData& rt);
	};
	typedef std::shared_ptr<ComboResult>	ComboResultPtr;

	//////////////////////////////////////////////////////////////////////////
	/// DummyAlpha
	//////////////////////////////////////////////////////////////////////////
	class Framework_API DummyAlpha : public IAlpha {
	public:
											DummyAlpha(const ConfigSection& config);
											~DummyAlpha();
		void								run(const RuntimeData& rt);
	};
} /// namespace pesa

#define F_ARG_OR(Ind, Nm)					this->getFloatArgPtr(Ind, Nm)

#define F_ARG(index)						this->getFloatArg(index)
#define F_ARG0()							F_ARG(0)
#define F_ARG1()							F_ARG(1)
#define F_ARG2()							F_ARG(2)
#define F_ARG3()							F_ARG(3)
#define F_ARG4()							F_ARG(4)

#define F_ARG0_OR(Nm)						F_ARG_OR(0, Nm)
#define F_ARG1_OR(Nm)						F_ARG_OR(1, Nm)
#define F_ARG2_OR(Nm)						F_ARG_OR(2, Nm)
#define F_ARG3_OR(Nm)						F_ARG_OR(3, Nm)

#define F_ARG_PTR(index)					this->getFloatArgPtr(index)
#define F_ARG0_PTR()						F_ARG_PTR(0)
#define F_ARG1_PTR()						F_ARG_PTR(1)
#define F_ARG2_PTR()						F_ARG_PTR(2)
#define F_ARG3_PTR()						F_ARG_PTR(3)

#define II_LOOP() 							auto& alpha = this->alpha(); for (size_t ii = 0; ii < alpha.cols(); ii++)
#define JJ_LOOP_D(OD, D, DLT)				for (IntDay jj = OD; jj < D && jj < backdays(); jj += std::min(DLT, 1))
#define JJ_LOOP() 							JJ_LOOP_D(m_pinfo.omitDays, m_pinfo.days, m_pinfo.delta)
#define JJ_LOOP_O(M) 						for (IntDay jj = std::max(m_pinfo.omitDays, M); jj < m_pinfo.days && jj < backdays(); jj += m_pinfo.delta)
#define JJ_LOOP_N(CNT) 						for (IntDay jj = m_pinfo.omitDays; jj < CNT && jj < backdays(); jj += m_pinfo.delta)
#define JJ_LOOP_SN(ST, CNT)					for (IntDay jj = ST; jj < CNT && jj < backdays(); jj += m_pinfo.delta)

#define CHK_DATA_ALPHA(DT, AL)				{ ASSERT(AL.cols() <= DT.cols() || dynamic_cast<const pesa::ConstantFloat*>(&AL) != nullptr, "Data and alpha columns do not match. Alpha cols: " << AL.cols() << " - Data cols: " << DT.cols()); }
