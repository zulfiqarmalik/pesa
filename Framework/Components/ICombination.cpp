/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// ICombination.cpp
///
/// Created on: 20 Mar 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "./ICombination.h"
#include "./IAlpha.h"
#include "Framework/Pipeline/IDataRegistry.h"

namespace pesa {

	//CalcDataPtr AlphaResult::getDataPtr() {
	//	if (!m_data && m_alpha)
	//		m_data = m_alpha->dataPtr();
	//	return m_data;
	//}

	//CalcDataPtr& AlphaResult::dataPtr() {
	//	getDataPtr();
	//	return m_data;
	//}

	//CalcData* AlphaResult::data() {
	//	return getDataPtr().get();
	//}

	void AlphaResult::ensureAlloc() {
		ASSERT(childAlphas.size(), "Invalid AlphaResult with nothing in the childAlphas vector!");

		/// get the firt childAlpha
		const auto& calpha = childAlphas[0].alpha->remappedAlpha();
		
		/// allocate the alpha array and resize
		alpha->data().ensureAlphaSize(calpha.rows(), calpha.cols());
	}
	
} /// namespace pesa

