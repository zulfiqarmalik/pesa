/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Combination.h
///
/// Created on: 4 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/FrameworkDefs.h"
#include "Framework/Data/CustomData.h"
#include "Framework/Components/IComponent.h"
#include "Framework/Data/CalcData.h"

namespace pesa {

	class IAlpha;

	/// Hierarchical Alpha results
	struct AlphaResult {
	//private:
		std::shared_ptr<IAlpha>			alpha = nullptr;		/// The alpha that this result represents
		pesa::CalcDataPtr				data = nullptr;		/// The state object of this alpha
		std::vector<pesa::AlphaResult> 	childAlphas;			/// The child alphas of this alpha

		//pesa::CalcDataPtr				getDataPtr();

	//public:

	//	pesa::IAlphaPtr&				alpha() { return m_alpha; }
	//	pesa::IAlphaPtr					alpha() const { return m_alpha; }

	//	pesa::CalcDataPtr&				dataPtr();
	//	pesa::CalcData*					data();

	//	Inline const std::vector<pesa::AlphaResult>& childAlphas() const { return m_childAlphas; }
	//	Inline std::vector<pesa::AlphaResult>& childAlphas() { return m_childAlphas; }

		//////////////////////////////////////////////////////////////////////////
		/// SWIG specific only
		//////////////////////////////////////////////////////////////////////////
		inline IAlpha*					resultAlpha() { return alpha.get(); }
		inline size_t					numChildren() const { return childAlphas.size(); }
		inline AlphaResult&				childAlpha(size_t i) { return childAlphas[i]; } 

		void 							ensureAlloc();

		~AlphaResult() {
		}
	};

	typedef std::shared_ptr<AlphaResult> 		AlphaResultPtr;
	typedef CustomData<AlphaResult> 			AlphaResultData;
	typedef std::shared_ptr<AlphaResultData> 	AlphaResultDataPtr;

	typedef std::vector<pesa::AlphaResult>		AlphaResultVec;

	class ICombination : public IComponent {
	private:
		AlphaResult 				m_alphas; 			/// The alphas that need to be combined

	public:
		ICombination(const ConfigSection& config, const std::string& logChannel)
			: IComponent(config, !logChannel.empty() ? logChannel : "CMB") {
		}

		////////////////////////////////////////////////////////////////////////////
		/// Inline functions
		////////////////////////////////////////////////////////////////////////////
		inline pesa::AlphaResult&		alphas() 		{ return m_alphas; }
		inline const pesa::AlphaResult& alphas() const	{ return m_alphas; }
		inline const ConfigSection& 	config() const 	{ return IComponent::config<ConfigSection>(); }
	};
	
	typedef std::shared_ptr<ICombination> 	ICombinationPtr;
} /// namespace pesa

