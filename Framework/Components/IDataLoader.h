/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// DataLoader.h
///
/// Created on: 4 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/FrameworkDefs.h"
#include "./IComponent.h"
#include "Framework/Data/Data.h"
#include "Framework/Data/Dataset.h"

namespace pesa {

	class IDataRegistry;

	class Framework_API IDataLoader : public IComponent {
	public:
		IDataLoader(const ConfigSection& config, const std::string& logChannel) 
			: IComponent(config, !logChannel.empty() ? logChannel : "DL") {
		}

		/// initDatasets function is called for initialising the datasets that this DataLoader
		/// is supposed to provide
		virtual void 			initDatasets(IDataRegistry& dr, pesa::Datasets& datasets) = 0;
		virtual pesa::DataPtr	create(IDataRegistry& dr, const Dataset& ds, const DataDefinition& def) { return nullptr; }

		/// preDataBuild and postDataBuild are called, in that order, just after the initDatasets
		/// function and just before each of the data items are invalidated. They are called
		/// once for the entire DataLoader and not the individual datasets that the Loader
		/// is providing
		virtual void 			preDataBuild(IDataRegistry& dr, const pesa::DataCacheInfo& dci) {}
		virtual void 			postDataBuild(IDataRegistry& dr, const pesa::DataCacheInfo& dci) {}

		/// preBuild, build and postBuild are called, in that order, for each of the datasets
		/// provided by the DataLoader in the initDatasets function
		virtual void 			preBuild(IDataRegistry& dr, const pesa::DataCacheInfo& dci) {}
		virtual void 			build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) = 0;
		virtual void 			postBuild(IDataRegistry& dr, const pesa::DataCacheInfo& dci) {}

		virtual bool			optimiseForUniverse() { return true; }

		//////////////////////////////////////////////////////////////////////////
		/// IDataLoader
		//////////////////////////////////////////////////////////////////////////
		virtual pesa::DataPtr	dataLoaded(const RuntimeData& rt, IDataRegistry& dr, pesa::DataPtr data, const std::string& cacheId, IUniverse* universe) { return data; }
		virtual void			dataTick(const RuntimeData& rt, IDataRegistry& dr) {}

		////////////////////////////////////////////////////////////////////////////
		/// IComponent implementation
		////////////////////////////////////////////////////////////////////////////
		virtual void 			run(const RuntimeData& rt);

		////////////////////////////////////////////////////////////////////////////
		/// Inline functions
		////////////////////////////////////////////////////////////////////////////
	};

	typedef std::shared_ptr<IDataLoader> IDataLoaderPtr;
	
} /// namespace pesa

