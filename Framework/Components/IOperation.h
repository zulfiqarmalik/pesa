/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Operation.h
///
/// Created on: 4 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/FrameworkDefs.h"
#include "IComponent.h"
#include "Framework/Data/CalcData.h"

namespace pesa {

	class IOperation : public IComponent {
	protected:
		pesa::CalcDataPtr			m_data = nullptr;				/// The data that this alpha is working with (see ComponentDefs.h for more details)
		const pesa::CalcData*		m_yesterdayData = nullptr;		/// The data for yesterday

	public:
		IOperation(const ConfigSection& config, const std::string& logChannel) 
			: IComponent(config, !logChannel.empty() ? logChannel : "OP") {
		}

		////////////////////////////////////////////////////////////////////////////
		/// Inline functions
		////////////////////////////////////////////////////////////////////////////
		inline const float* 				alphaPtr() const 	{ return m_data->alpha->fdata(); 				}
		inline pesa::FloatMatrixData&		alpha() 			{ return *m_data->alpha; 						}
		inline const pesa::FloatMatrixData& alpha() const 		{ return *m_data->alpha; 						}
		inline const ConfigSection&			config() const 		{ return IComponent::config<ConfigSection>(); 	}
		inline pesa::CalcData& 				data() 				{ return *m_data.get();							}
		inline const pesa::CalcData&		data() const		{ return *m_data.get();							}
		inline pesa::CalcData&				alphaData()			{ return *m_data.get();							}
		inline pesa::CalcDataPtr&			dataPtr()			{ return m_data;								}
		inline const pesa::CalcData*&		yesterdayDataRef()	{ return m_yesterdayData;						}
		inline const pesa::CalcData*		yesterdayData()	const { return m_yesterdayData;						}
		inline const pesa::CalcData&		todayData() const	{ return *m_data.get();							}
		inline const pesa::Statistics*		stats() const		{ return m_yesterdayData ? &m_yesterdayData->stats : nullptr; }
		inline const
			pesa::LifetimeStatistics*		cumStats() const	{ return m_yesterdayData ? m_yesterdayData->cumStats.get() : nullptr; }
	};
	
	typedef std::shared_ptr<IOperation> IOperationPtr;
} /// namespace pesa	

