/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// IOptimiser.cpp
///
/// Created on: 12 Aug 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "IOptimiser.h"
#include "Framework/Data/CalcData.h"

namespace pesa {
	IOptimiser::IOptimiser(const ConfigSection& config, const std::string& logChannel) : IComponent(config, !logChannel.empty() ? logChannel : "OPT") {
		const ConfigSection& portConfig = parentConfig()->portfolio();
		const ConfigSection* optConfig = portConfig.getChildSection("Optimise");

		if (optConfig) {
			m_doOptimise = optConfig->getBool("optimise", m_doOptimise);
			m_doVerify = optConfig->getBool("verify", m_doVerify);

			optConfig->getPercent("maxTrade", m_optConfig.maxTrade);
			optConfig->getPercent("maxPos", m_optConfig.maxPos);
			optConfig->getPercent("maxTradeADV", m_optConfig.maxTradeADV);
			optConfig->getPercent("maxTvr", m_optConfig.maxTvr);
			optConfig->get("advDataId", m_optConfig.advDataId);
		}
	}

	SizeVec IOptimiser::verifyMaxTrade(const CalcData& data, float bookSize, float maxTrade) {
		float maxTradeValue = bookSize * maxTrade * 0.01f;
		SizeVec offenders;

		for (size_t ii = 0; ii < data.alphaNotionals.size(); ii++) {
			float alphaValue = data.alphaNotionals[ii];
			if (std::abs(alphaValue) > maxTradeValue)
				offenders.push_back(ii);
		}

		return offenders;
	}

	bool IOptimiser::verifyMaxTvr(const CalcData& data, float bookSize, float maxTvr) {
		if (!data.yesterday)
			return true;

		/// Give it an error margin of around 0.5%
		return data.stats.tvr <= maxTvr + 0.5f;
	}

	bool IOptimiser::verifyOptimisationResults(const RuntimeData& rt, float bookSize, CalcData& data, const IOptimiser::Config& optConfig) {
		data.calculateStats(rt, bookSize);

		if (optConfig.maxTvr > 0.0f && !verifyMaxTvr(data, bookSize, optConfig.maxTvr)) {
			Warning("OPT", "Max turnover input not optimised properly. Max tvr constraint: %0.2hf - Turnover: %0.2hf", optConfig.maxTvr, data.stats.tvr);
			return false;
		}

		if (optConfig.maxTrade > 0.0f) {
			SizeVec maxTradeOffenders = verifyMaxTrade(data, bookSize, optConfig.maxTrade);
			if (maxTradeOffenders.size()) {
				Warning("OPT", "Max turnover input not optimised properly. Max trade constraint: %0.2hf - Number of offenders: %z", optConfig.maxTrade, maxTradeOffenders.size());
				return false;
			}
		}

		return true;
	}

	bool IOptimiser::verifyOptimisationResults(const RuntimeData& rt) {
		ASSERT(m_calcData, "Calc data is not set!");
		ASSERT(m_bookSize > 0.0f, "Invalid book size: " << m_bookSize);
		
		return verifyOptimisationResults(rt, m_bookSize, *m_calcData, m_optConfig);
	}

} /// namespace pesa
