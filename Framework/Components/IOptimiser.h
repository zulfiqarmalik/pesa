/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// IOptimiser.h
///
/// Created on: 4 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/FrameworkDefs.h"
#include "IComponent.h"

namespace pesa {

	struct CalcData;

	////////////////////////////////////////////////////////////////////////////
	/// IOptimiser - The optimiser
	////////////////////////////////////////////////////////////////////////////
	class Framework_API IOptimiser : public IComponent {
	public:
		//////////////////////////////////////////////////////////////////////////
		/// OptConfig - Optimisation configuration
		/// A value of 0.0 means that the parameter will not be used for 
		/// optimisation
		//////////////////////////////////////////////////////////////////////////
		struct Framework_API Config {
			float maxTrade			= 0.0f;					/// Max trade as a percentage of book size. So 0.05 (5%)
															/// means that we do not wish to invest more than 5% of our 
															/// book in any one of the securities
															/// RANGE: [0, 1]

			float maxPos			= 0.0f;					/// Maximum absolute holding position in a stock 
															/// as a percentage of book size
															/// RANGE: [0, 1]

			float maxPosADV			= 0.0f;					/// Max absolute position as a percentage of ADV

			float maxTradeADV		= 0.0f;					/// Max trade in each stock as a percentage of ADV. 
															/// Whether we use ADV5, ADV10, ADV20 or something else
															/// will depend on the next parameter
															/// RANGE: [0, 1]
			std::string advDataId	= "baseData.adv20";		/// The ADV data type to use for maxTradeADV

			float maxTvr			= 0.0f;					/// Max turnover as a percentage of book size
															/// RANGE: [0, 1]
		};

	protected:
		CalcData* 					m_calcData = nullptr;
		Config 						m_optConfig;
		float 						m_bookSize; 
		bool 						m_doOptimise = false;
		bool 						m_doVerify = false;
		bool 						m_didOptimise = true; 	/// Tells whether the last run was successful or not

		static SizeVec				verifyMaxTrade(const CalcData& data, float bookSize, float maxTrade);
		static bool					verifyMaxTvr(const CalcData& data, float bookSize, float maxTvr);

	public:
									IOptimiser(const ConfigSection& config, const std::string& logChannel);
		virtual 					~IOptimiser() {
		}

		static bool					verifyOptimisationResults(const RuntimeData& rt, float bookSize, CalcData& data, const IOptimiser::Config& optConfig);
		bool						verifyOptimisationResults(const RuntimeData& rt);

		////////////////////////////////////////////////////////////////////////////
		/// Inline functions
		////////////////////////////////////////////////////////////////////////////
		inline IOptimiser::Config& 	optConfig()  			{ return m_optConfig; 		}
		inline const IOptimiser::Config& optConfig() const 	{ return m_optConfig; 		}
		inline const pesa::CalcData* calcData() const 		{ return m_calcData; 		}
		inline pesa::CalcData*& 	calcData() 				{ return m_calcData; 		}
		inline float 				bookSize() const 		{ return m_bookSize; 		}
		inline float& 				bookSize()  			{ return m_bookSize; 		}
		inline bool 				didOptimise() const 	{ return m_didOptimise; 	}
		inline bool 				doOptimise() const 		{ return m_doOptimise; 		}
		inline bool 				doVerify() const 		{ return m_doVerify; 		}
		inline pesa::CalcData& 		data() const 			{ return *m_calcData; 		}
	};

	typedef std::shared_ptr<IOptimiser> IOptimiserPtr;

} /// namespace pesa

