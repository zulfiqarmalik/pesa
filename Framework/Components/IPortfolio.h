//////////////////////////////////////////////////////
/// Portfolio.h
///
/// Created on: 4 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#ifndef ENGINE_PORTFOLIO_H_
#define ENGINE_PORTFOLIO_H_

#include "IComponent.h"

namespace pesa {

	class IPortfolio : public IComponent {
	public:
		IPortfolio(const cfg::Portfolio& config, const char* logChannel = nullptr) 
			: IComponent(config, logChannel ? logChannel : "PORT") {
		}

		////////////////////////////////////////////////////////////////////////////
		/// Inline functions
		////////////////////////////////////////////////////////////////////////////
		inline const cfg::Portfolio& config() const 	{ return IComponent::config<cfg::Portfolio>(); }
	};
	
} /// namespace pesa

#endif /// ENGINE_PORTFOLIO_H_
