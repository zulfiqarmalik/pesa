/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// IUniverse.cpp
///
/// Created on: 07 Oct 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "IUniverse.h"
#include "Framework/Data/MatrixData.h"
#include "Framework/Pipeline/IDataRegistry.h"

namespace pesa {
	void IUniverse::remap(IntDay di, FloatMatrixData* srcAlpha, IUniverse* srcUniverse, FloatMatrixData& dstAlpha) {
		IUniverse* dstUniverse = this;

		size_t dstSize = dstUniverse->size(di);
		size_t srcSize = srcUniverse->size(di);
		//ASSERT(srcSize <= dstSize, "Source universe size is larger than the destination universe. Src size: " << srcSize << " - Dst size: " << dstSize);

		size_t srcAlphaRows = srcAlpha->rows();
		size_t srcAlphaCols = srcAlpha->cols();
		//ASSERT(srcAlphaCols <= srcSize && srcAlphaCols <= dstSize, "Source alpha does not match the source universe. Src alpha cols: " << srcAlphaCols << " - Src universe size: " << srcSize);

		/// ensureSize will invalidate the memory as well ...
		dstAlpha.ensureSize(srcAlphaRows, dstSize);

		/// Construct a security map
		std::unordered_map<Security::Index, size_t> dstSecMap;

		for (size_t ii = 0; ii < dstSize; ii++) {
			Security::Index dstSecId = dstUniverse->index(di, ii);
			dstSecMap[dstSecId] = ii;
		}

		for (size_t ii = 0; ii < srcAlphaCols; ii++) {
			if (!srcUniverse->isValid(di, ii))
				continue;

			Security::Index srcSecId = srcUniverse->index(di, ii);
			auto dstIter = dstSecMap.find(srcSecId);

			ASSERT(dstIter != dstSecMap.end(), "Unable to find security: " << srcSecId << " in the dst universe!");

			size_t iiDst = dstIter->second;
			ASSERT(iiDst < dstSize, "Invalid dst universe index: " << iiDst << " - For security index: " << srcSecId);

			for (size_t jj = 0; jj < srcAlphaRows; jj++) {
				float srcValue = (*srcAlpha)((IntDay)jj, ii);
				float& dstValue = dstAlpha((IntDay)jj, iiDst);
				dstValue = srcValue;
			}
		}
	}

	bool IUniverse::isValid(pesa::IntDay di, size_t ii) const {
		auto index = this->index(di, (Security::Index)ii);
		auto sec = this->security(di, (Security::Index)ii);
		return index != pesa::Security::s_invalidIndex;
	}

	bool IUniverse::isRestricted(const RuntimeData& rt, pesa::IntDay di, size_t ii_, 
		std::string restrictedStartTime /* = "restricted.start" */, std::string restrictedEndTime /* = "restricted.end" */) const {
		Security::Index ii = this->index(di, (Security::Index)ii_);
		if (ii == Security::s_invalidIndex)
			return true;

		IDataRegistry& dr = *((const_cast<IUniverse*>(this))->dataRegistry());
		IUniverse* secMaster = dr.getSecurityMaster();
		Security sec = secMaster->security(di, ii);
		IntDate currDate = rt.dates[di];

		const Int64MatrixData* rstart = dr.get<Int64MatrixData>(secMaster, restrictedStartTime, true);
		const Int64MatrixData* rend = dr.get<Int64MatrixData>(secMaster, restrictedEndTime, true);

		ASSERT(rstart, "Unable to load restriction start time data: " << restrictedStartTime);
		ASSERT(rend, "Unable to load restriction end time data: " << restrictedEndTime);

		Poco::DateTime startTime = rstart->getDTValue(di, ii);
		Poco::DateTime endTime = rend->getDTValue(di, ii);

		int64_t istartTime = rstart->getValue(di, ii);
		int64_t iendTime = rend->getValue(di, ii);

		IntDate startDate = Util::dateToInt(startTime);
		IntDate endDate = Util::dateToInt(endTime);

		/// This stock is restricted ...
		if ((istartTime > 0 && startDate <= currDate) && (endDate >= currDate || iendTime <= 0)) {
            ///            std::cout << restrictedStartTime << " - " << currDate << " - " << sec.nameStr() << " - " << sec.bbTickerStr() << " - Start: " << istartTime << " - End: " << iendTime << std::endl;
			return true;
        }

		return false;
	}

	bool IUniverse::isShortRestricted(const RuntimeData& rt, pesa::IntDay di, size_t ii,
		std::string restrictedStartTime /* = "restricted.shorts_start" */, std::string restrictedEndTime /* = "restricted.shorts_end" */) const {
		return isRestricted(rt, di, ii, restrictedStartTime, restrictedEndTime);
	}

	void IUniverse::uuids(pesa::IntDay di, std::vector<std::string>& ids) const { 
		return idsOfType(di, ids, SecId::kUuid);
	}

	void IUniverse::idsOfType(pesa::IntDay di, std::vector<std::string>& ids, SecId::Type idType) const {
		ASSERT(idType < SecId::kCount, "Invalid SecId::Type: " << idType);

		const auto& secs = m_uuids[idType];
		size_t numSecurities = this->size(di);

		if (secs.size() && secs.size() == numSecurities) {
			ids = secs;
			return;
		}
		//auto uuidIter = m_uuids.find(idType);

		//if (uuidIter != m_uuids.end()) {
		//	ids = uuidIter->second;
		//	return;
		//}

		StringVec uuids;
		uuids.resize(numSecurities);

		for (size_t ii = 0; ii < numSecurities; ii++) {
			if (this->isValid(di, ii)) {
				auto sec = this->security(di, (Security::Index)ii);
				std::string id;

				switch (idType) {
				case SecId::kUuid:			id = sec.uuidStr(); break;
				case SecId::kInstrument:	id = sec.instrumentId(); break;
				case SecId::kTicker:		id = sec.tickerStr(); break;
				case SecId::kISIN:			id = sec.isinStr(); break;
				default: break;
				}

				uuids[ii] = id;
			}
		}

		ids = uuids;
		(const_cast<IUniverse*>(this))->m_uuids[idType] = uuids;
	}
} /// namespace pesa

