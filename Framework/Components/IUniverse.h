/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// IUniverse.h
///
/// Created on: 4 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/FrameworkDefs.h"
#include "./IDataLoader.h"
#include "Framework/Data/SecurityData.h"

namespace pesa {

	class UIntMatrixData;

	////////////////////////////////////////////////////////////////////////////
	/// Base universe interface
	////////////////////////////////////////////////////////////////////////////
	class Framework_API IUniverse : public IDataLoader {
	private:
		//typedef std::unordered_map<SecId::Type, StringVec> SecIdCache;
		///SecIdCache				m_uuids;				/// The uuids that we've cached
		StringVec				m_uuids[SecId::kCount];		/// The uuids that we've cached

	public:
		IUniverse(const ConfigSection& config, const std::string& logChannel) 
			: IDataLoader(config, !logChannel.empty() ? logChannel : "UNIV") {
		}

		virtual void 			remap(IntDay di, FloatMatrixData* srcAlpha, IUniverse* srcUniverse, FloatMatrixData& dstAlpha);
		virtual const Security* mapSymbol(const std::string& symbol) const { return nullptr; }
		virtual const Security* mapUuid(const std::string& uuid) const { return nullptr; }

		virtual Security 		security(pesa::IntDay di, Security::Index ii) const = 0;

		/// This function can load a LOT of data. Please call only for DEBUGGING purposes ONLY!
		virtual Security 		securityInfo(pesa::IntDay di, Security::Index ii) { return security(di, ii); }
		virtual size_t 			size(pesa::IntDay di) const = 0;
		virtual void			uuids(pesa::IntDay di, std::vector<std::string>& ids) const;
		virtual void			idsOfType(pesa::IntDay di, std::vector<std::string>& ids, SecId::Type idType) const;

		virtual size_t			alphaIndex(pesa::IntDay di, size_t ii) {
			return ii;
		}

		virtual Security::Index	index(pesa::IntDay di, size_t ii) const {
			return security(di, (Security::Index)ii).index;
		}
		
		virtual std::string 	name() const {
			return config().get("name");
		}

		virtual bool			isMerged() const { return false; }

		virtual bool			isValid(pesa::IntDay di, size_t ii) const;
		virtual bool			isRestricted(const RuntimeData& rt, pesa::IntDay di, size_t ii, 
								std::string restrictedStartTime = "restricted.start", std::string restrictedEndTime = "restricted.end") const;
		virtual bool			isShortRestricted(const RuntimeData& rt, pesa::IntDay di, size_t ii, 
								std::string restrictedStartTime = "restricted.shorts_start", std::string restrictedEndTime = "restricted.shorts_end") const;

		virtual const FloatMatrixData* dailyValidMap() const { return nullptr; }

		virtual const UIntMatrixData* universeData() const { return nullptr; }
	};

	typedef std::shared_ptr<IUniverse> 		IUniversePtr;
	typedef std::vector<IUniverse*>			IUniversePVec;
} /// namespace pesa

