/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Config.cpp
///
/// Created on: 1 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Config.h"
#include "Core/Lib/Util.h"
#include "Core/IO/BasicStream.h"
#include "Core/Lib/Application.h"
#include "Poco/File.h"

#include "Framework/Helper/FrameworkUtil.h"

#include <fstream>
#include <Poco/SAX/Attributes.h>
#include <Poco/SAX/ContentHandler.h>
#include <Poco/SAX/SAXParser.h>
#include <Poco/SAX/InputSource.h>
#include <Poco/Format.h>
#include <Poco/String.h>

namespace pesa {

	Config::Config(const Config& rhs)
		: m_macros(rhs.m_macros)
		, m_defs(rhs.m_defs)
		, m_pipeline(this, rhs.m_pipeline)
		, m_modules(this, rhs.m_modules)
		, m_portfolio(this, rhs.m_portfolio) {
		copyImports(rhs.m_imports);
	}

	Config::Config()
		: m_macros(this)
		, m_defs(this)
		, m_pipeline(this)
		, m_modules(this)
		, m_portfolio(this) {
		m_macros.name() = "Macros";
		m_defs.name() = "Defs";
		m_pipeline.name() = "Pipeline";
		m_modules.name() = "Modules";
		m_portfolio.name() = "Portfolio";
	}

	Config::~Config() {
		for (auto* import : m_imports)
			delete import;
		m_imports.clear();
	}

	Config& Config::operator = (const Config& rhs) {
		m_macros = ConfigSection(this, rhs.m_macros);
		m_defs = ConfigSection(this, rhs.m_defs);
		m_pipeline = ConfigSection(this, rhs.m_pipeline);
		m_modules = ConfigSection(this, rhs.m_modules);
		m_portfolio = ConfigSection(this, rhs.m_portfolio);

		copyImports(rhs.m_imports);

		m_filename = rhs.m_filename;

		return *this;
	}

	void Config::copyImports(const ConfigSectionPVec& imports) {
		m_imports.clear();
		for (const auto* import : imports)
			m_imports.push_back(new ConfigSection(this, *import));
	}

	void Config::tick(const RuntimeData& rt, float percent) {
	}

	void Config::validate() const {
		Trace("CFG", "Validating config ...");

		unsigned int startDate = m_defs.startDate();
		unsigned int endDate = m_defs.endDate();
		unsigned int today = Util::dateToInt(Poco::DateTime());

		if (startDate != 0 && endDate != 0) {
			if (startDate >= endDate)
				throw Poco::Exception(Poco::format("Start date must be LESS than end date. Start Date: %u, End Date: %u", startDate, endDate));

			if (startDate > today)
				throw Poco::Exception(Poco::format("Cannot have start date in the future: %u", startDate));

			if (endDate > today)
				throw Poco::Exception(Poco::format("Cannot have end date in the future: %u", endDate));
		}

		float bookSize = m_portfolio.bookSize();
		if (bookSize <= 0.0f) {
			Warning("CFG", "Invalid book size specified! Is this the intention?");
		}
			//throw Poco::Exception(Poco::format("Invalid book size specified in the portfolio section: %f", bookSize));

		std::unordered_map<std::string, bool> uuids;
		validateUuids(m_portfolio, uuids);

		Trace("CFG", "Validation successful!");
	}

	void Config::validateUuids(const ConfigSection& section, std::unordered_map<std::string, bool>& uuids) const {
		auto uuid = section.getString("uuid", "");

		if (!uuid.empty()) {
			/// Operations get slightly special treatment. This is because they're dummy objects and in a large enough config
			/// we cannot expect everyone to have a needless unique uuid. They can get away with having no uuid at all. 
			/// However, Eido, can sometimes specify a uuid id. In that case we need to ignore it!
			if (section.name() == "Operation") {
				(const_cast<ConfigSection*>(&section))->set("uuid", "");
			}
			else {
				ASSERT(uuids.find(uuid) == uuids.end(), "All UUIDs must be unique. Duplicate found: " << uuid);
				uuids[uuid] = true;
			}
		}

		auto children = section.children();
		for (auto& child : children) 
			validateUuids(*child, uuids);
	}

	const ConfigSection* Config::getModuleFromModuleId(const ConfigSection& section) const {
		std::string moduleId;

		if (section.get("moduleId", moduleId)) {
			return m_modules.find(moduleId);
		}

		return m_modules.getDefaultModule();
	}

	////////////////////////////////////////////////////////////////////////////
	/// Helper static methods
	////////////////////////////////////////////////////////////////////////////
	class XmlContentHandler : public Poco::XML::ContentHandler {
	private:
		typedef std::vector<ConfigSection*> ConfigSectionPVec;
		Config&				m_config;
		ConfigSection* 		m_currSection = nullptr;
		std::string 		m_currElement;
		ConfigSectionPVec 	m_sectionStack;

	public:
		XmlContentHandler(Config& config) : m_config(config) {
		}

		virtual ~XmlContentHandler() {
		}

		inline const Config& config() { return m_config; }

		// ContentHandler overrides, begin.
		void setDocumentLocator(const Poco::XML::Locator* loc) {}
		void startDocument() {}
		void endDocument() {}
		void ignorableWhitespace(const Poco::XML::XMLChar ch[], int, int) {}
		void processingInstruction(const Poco::XML::XMLString&, const Poco::XML::XMLString&) {}
		void startPrefixMapping(const Poco::XML::XMLString&, const Poco::XML::XMLString&) {}
		void endPrefixMapping(const Poco::XML::XMLString&) {}
		void skippedEntity(const Poco::XML::XMLString&) {}

		void characters(const Poco::XML::XMLChar ch[], int start, int length) {
			std::string str(ch + start, length);
			str = Util::trim(str);

			//if (m_currSection && !m_currElement.empty() && !str.empty()) {
			//	m_currSection->inner() = str;
			//}
		}

		void mapAttribs(const Poco::XML::Attributes& attribs, ConfigSection& section, const std::string& name) {
			for (int i = 0; i < attribs.getLength(); i++) {
				std::string key = attribs.getQName(i);
				std::string value = attribs.getValue(key);
				section.map()[key] = value;
			}

			if (m_currSection != &section && m_currSection != nullptr)
				m_sectionStack.push_back(m_currSection);

			/// If a name hasn't been assigned then we just use the one that was passed in ...
			if (section.name().empty())
				section.name() = name;

			m_currSection = &section;
		}

		void endElement(const Poco::XML::XMLString&, const Poco::XML::XMLString&, const Poco::XML::XMLString& name) {
			if (m_currSection)
				m_currSection->name() = name;

			/// if the current section is unknown then we also add that as a lookup property
			if (m_currSection && m_currSection->isUnknown() && !m_currSection->map().size() && m_sectionStack.size()) {
				/// get the parent section
				auto parentSection = m_sectionStack[m_sectionStack.size() - 1];
				//parentSection->map()[m_currSection->name()] = m_currSection->inner();

				Trace("XML", "Adding section as a key to its parent: %s = %s", m_currSection->name(), m_currSection->inner());
			}

			m_currSection = nullptr;
			m_currElement.clear();

			if (m_sectionStack.size()) {
				m_currSection = m_sectionStack[m_sectionStack.size() - 1];
				m_sectionStack.pop_back();
			}
		}

		void startElement(const Poco::XML::XMLString& nsUri, const Poco::XML::XMLString& localName, const Poco::XML::XMLString& name, const Poco::XML::Attributes& attribs) {
			Trace("XML", "Config Element: %s", name);

			m_currElement = name;

			if (name == "Modules" || name == "modules")
				mapAttribs(attribs, m_config.modules(), name);
			else if (name == "Module" || name == "module") {
				Trace("CFG", "Initialising module ...");
				auto* parentSection = dynamic_cast<ConfigSection*>(m_currSection);
				auto* module = new ConfigSection(&m_config);
				parentSection->addChildSection(module);
				module->map()["handler"] = "Module";
				mapAttribs(attribs, *module, name);
			}
			else if (name == "Data" || name == "data" ||
					 name == "Universe" || name == "universe" ||
					 name == "SecurityMaster" || name == "securityMaster") {
				auto* module = new ConfigSection(&m_config);
				m_config.modules().addChildSection(module);
				mapAttribs(attribs, *module, name);
				module->map()["handler"] = "DataLoader";
				module->map()["type"] = Poco::toLower(name);
			}
			else if (name == "Alias" || name == "alias") {
				auto* module = new ConfigSection(&m_config);
				m_config.modules().addChildSection(module);
				mapAttribs(attribs, *module, name);
				module->map()["handler"] = "DataAlias";
				module->map()["type"] = "alias";
			}
			else if (name == "Alphas" || name == "alphas") {
				//auto* alphas = m_currSection->alphasSec();
				//if (!alphas) {
				auto* alphas = new ConfigSection(&m_config);
				m_currSection->addChildSection(alphas);
				//}

				mapAttribs(attribs, *alphas, name);
			}
			else if (name == "Alpha" || name == "alpha") {
				ConfigSection* alpha = new ConfigSection(&m_config);
				ASSERT(m_currSection->name() == "Alphas" || m_currSection->name() == "Portfolio", "Badly formed XML section (Expecting <Alphas> section to contain the Alpha): " << m_currSection->name());
				auto currSection = m_currSection;
				mapAttribs(attribs, *alpha, name);
				currSection->addChildSection(alpha);
			}
			else if (name == "Combination" || name == "combination") {
				ASSERT(false, "Combination sections are deprecated! Please put everything under the <Alphas> section.");
			}
			else if (name == "Portfolio" || name == "portfolio")
				mapAttribs(attribs, m_config.portfolio(), name);
			else if (name == "Operation" || name == "operation") {
				ConfigSection* operation = new ConfigSection(&m_config);
				//ASSERT(m_currSection->name() == "Alphas" || m_currSection->name() == "Alpha", "Badly formed XML section (Expecting <Alphas> OR <Alpha> section to contain the Operation): " << m_currSection->name());
				ASSERT(m_currSection, "Cannot have an operation without a parent section!");
				auto currSection = m_currSection;
				mapAttribs(attribs, *operation, name);
				currSection->addChildSection(operation);
			}
			else if (name == "Defs" || name == "defs")
				mapAttribs(attribs, m_config.defs(), name);
			else if (name == "Macros" || name == "macros")
				mapAttribs(attribs, m_config.macros(), name);
			else if (name == "Pipeline" || name == "pipeline")
				mapAttribs(attribs, m_config.pipeline(), name);
			else if (name == "Import" || name == "import") {
				/// do nothing ... this will be processed in the endElement function ...
				ConfigSection* importSection = new ConfigSection(&m_config);
				mapAttribs(attribs, *importSection, name);
				m_config.imports().push_back(importSection);
			}
			else if (name == "Insert" || name == "insert") {
				/// do nothing ... this will be processed in the endElement function ...
				auto currSection = m_currSection;
				ConfigSection* insertSection = new ConfigSection(&m_config);
				mapAttribs(attribs, *insertSection, name);
				m_config.inserts().push_back(insertSection);

				/// We now put a dummy section
				ConfigSection* insertDummy = new ConfigSection(&m_config);
				insertDummy->name() = "_INSERT_";
				insertDummy->set("id", "_INSERT_");
				insertDummy->set("index", Util::cast(m_config.inserts().size() - 1));

				currSection->addChildSection(insertDummy);
			}
			else {
				if (m_currSection) {
					/// make a new config section
					ConfigSection* section = new ConfigSection(&m_config);
					m_currSection->addChildSection(section);
					section->name() = name;
					section->isUnknown() = true;
					mapAttribs(attribs, *section, name);
				}
				else if (name != "Config" && name != "config") {
					Warning("CFT", "Unknown section: %s", name);
				}
			}
		}
	};

	bool Config::parseJs(const std::string& filename, Config& config, bool isImport) {
		return false;
	}

	bool Config::parseXml(const std::string& filename, Config& config, bool isImport) {
		/// Otherwise we return the newly created config ...
		config.filename() = filename;

		io::BasicInputStreamPtr inputStream = io::BasicInputStream::createFileReader(filename);
		Poco::XML::InputSource source(*inputStream->stream().get());
		Poco::XML::SAXParser parser;

		parser.setFeature(Poco::XML::XMLReader::FEATURE_NAMESPACES, false);
		parser.setFeature(Poco::XML::XMLReader::FEATURE_NAMESPACE_PREFIXES, false);

		XmlContentHandler handler(config);
		parser.setContentHandler(&handler);

		parser.parse(&source);

		config.finalise();

		if (!isImport) {
			config.postLoad();
		}

		return true;
	}

	void Config::postLoad() {
		m_macros.finalise();
		m_defs.finalise();
		m_modules.finalise(true);
		m_portfolio.finalise();
		m_pipeline.finalise();
	}

	bool Config::loadMongoDB(Config& config) {
		return true;
	}

	std::string Config::dbName() const {
		std::string dbName = m_defs.getString("dbName", "");
		return !dbName.empty() ? dbName : Application::instance()->appOptions().dbName;
	}

	bool Config::parse(const std::string& name, Config::Type type, Config& config) {
		switch (type) {
		case Config::kXML:
			return parseXml(name, config);
		case Config::kJSON:
			return parseJs(name, config);
		case Config::kMongoDB:
			return loadMongoDB(config);
		}

		ASSERT(false, "Unknown type of config to parse: " << type);
		return false;
	}

	void Config::finalise() {
		/// first of all we resolve the imports ...
		resolveImports();

		if (m_baseConfigs.size()) {
			ConfigSection macros(m_macros);
			ConfigSection defs(m_defs);
			ConfigSection modules(m_modules);
			ConfigSection portfolio(m_portfolio);
			ConfigSection pipeline(m_pipeline);

			m_macros.clear();
			m_defs.clear();
			m_modules.clear();
			m_portfolio.clear();
			m_pipeline.clear();

			for (const Config& baseConfig : m_baseConfigs) {
				merge(baseConfig);
			}

			m_macros.merge(macros);
			m_defs.merge(defs);
			m_modules.merge(modules);
			m_portfolio.merge(portfolio);
			m_pipeline.merge(pipeline);

			/// See if the dates have been overridden
			AppOptions appOptions = Application::instance()->appOptions();

			if (!m_defs.getString("startDate", "").empty() && !appOptions.ovrStartDate.empty())
				m_defs.set("startDate", appOptions.ovrStartDate);

			if (!m_defs.getString("endDate", "").empty() && !appOptions.ovrEndDate.empty())
				m_defs.set("endDate", appOptions.ovrEndDate);

			if (!appOptions.configOverrides.empty()) {
				std::map<std::string, std::string> mappings;
				FrameworkUtil::parseMappings(appOptions.configOverrides, &mappings, nullptr);

				for (auto iter = mappings.begin(); iter != mappings.end(); iter++) {
					const std::string& sectionId = iter->first;
					const std::string& rhs = iter->second;
					StringVec sectionIds = Util::split(sectionId, ".");
					const ConfigSection* parentConfig = getChildSection(sectionIds, 0, (int)sectionIds.size() - 1);

					if (parentConfig) {
						ConfigSection* sec = const_cast<ConfigSection*>(parentConfig);
						sec->set(sectionIds[sectionIds.size() - 1], rhs);
					}
				}
			}
		}

		/// In the end resolve inserts
		resolveInserts();

		if (m_inserts.size()) {
			doInserts("pipeline");
			doInserts("modules");
		}
	}

	void Config::doInserts(std::string name) {
		ConfigSection* sec = section(name);
		size_t count = sec->children().size();

		for (size_t iter = 0; iter < count; iter++) {
			ConfigSection* childSec = sec->child(iter);

			if (childSec->name() == "_INSERT_") {
				size_t index = (size_t)childSec->getInt("index", -1);
				ASSERT(index < m_inserts.size(), "Invalid insert index: " << index << " - Max: " << m_inserts.size());

				sec->children().erase(sec->children().begin() + iter);

				auto toInsert = m_insertConfigs[index].section(name);
				size_t numAdded = sec->insertChildrenAt(iter, toInsert);

				/// Here we increment the iterator and also the count. 
				/// We do a -1 because we have erased one element!
				iter = (size_t)std::max((int)(iter + numAdded - 1), 0);
				count = (size_t)std::max((int)(count + numAdded - 1), 0);
			}
		}
	}

	ConfigSection* Config::section(std::string name) { 
		Poco::toLowerInPlace(name);

		if (name == "modules")
			return &m_modules;
		else if (name == "portfolio")
			return &m_portfolio;
		else if (name == "macros")
			return &m_macros;
		else if (name == "defs")
			return &m_defs;
		else if (name == "pipeline")
			return &m_pipeline;

		return nullptr;
	}

	const ConfigSection* Config::getChildSection(const std::vector<std::string>& sectionIds, int start /* = 0 */, int end /* = -1 */) const {
		const ConfigSection* parentConfig = nullptr;

		if (end < 0)
			end = (int)sectionIds.size() - 1;

		for (int i = start; i < end; i++) {
			const std::string& sectionName = sectionIds[i];

			if (!parentConfig) {
				if (sectionName == "Portfolio")
					parentConfig = &m_portfolio;
				else if (sectionName == "Defs")
					parentConfig = &m_defs;
				else if (sectionName == "Macros")
					parentConfig = &m_macros;
				else if (sectionName == "Modules")
					parentConfig = &m_modules;
				else if (sectionName == "Pipeline")
					parentConfig = &m_pipeline;
				else {
					ASSERT(false, "Unrecognised section name: " << sectionName);
				}
			}
			else {
				const ConfigSection* childConfig = parentConfig->getChildSection(sectionName);
				if (!childConfig)
					return nullptr;
				parentConfig = childConfig;
			}
		}

		return parentConfig;
	}

	void Config::merge(const Config& baseConfig) {
		m_macros.merge(baseConfig.m_macros);
		m_defs.merge(baseConfig.m_defs);
		m_modules.merge(baseConfig.m_modules);
		m_portfolio.merge(baseConfig.m_portfolio);
		m_pipeline.merge(baseConfig.m_pipeline);
	}

	void Config::loadSubConfig(ConfigSection* importSec, std::vector<Config>& loadedConfigs){
		/// do nothing ... this will be processed in the endElement function ...
		ConfigSection& importSection = *importSec;
		std::string importPathStr = importSection.get("path");
		ASSERT(!importPathStr.empty(), "<Import> directive must have a path inner value!");

		Poco::Path currPath(m_filename);
		auto dir = currPath.makeParent();

		if (importPathStr.find("./") != std::string::npos) {
			dir.append(importPathStr);
			importPathStr = dir.toString();
		}

		Debug("CFG", "Importing Config: %s", importPathStr);
		Config importConfig;

		std::string resolvedImportPathStr = importSec->resolve(importPathStr, nullptr);
		Poco::Path ppath(resolvedImportPathStr);
		auto ext = Poco::toLower(ppath.getExtension());

		if (ext == "xml") {
			if (Config::parseXml(resolvedImportPathStr, importConfig, true))
				loadedConfigs.push_back(importConfig);
		}
		else if (ext == "js") {
			if (Config::parseJs(resolvedImportPathStr, importConfig, true))
				loadedConfigs.push_back(importConfig);
		}
		else 
			ASSERT(false, "Could not deduce the type of the import config: " << resolvedImportPathStr);
	}

	void Config::resolveImports() {
		for (auto importSec : m_imports)
			loadSubConfig(importSec, m_baseConfigs);
	}

	void Config::resolveInserts() {
		for (auto insertSec : m_inserts)
			loadSubConfig(insertSec, m_insertConfigs);
	}

	void Config::exit(int errCode) {
	}

	void Config::abort() {
	}

	void Config::start() {
	}

	void Config::end(const RuntimeData& rt, int errCode) {
	}

	void Config::overwriteModified() {
		ASSERT(false, "The base config does not support modification! This can only be done by a derived config e.g. DBConfig etc.");
	}

	Poco::Timestamp Config::getModifiedTimestamp() const {
		Poco::File configFile(m_filename);
		return configFile.getLastModified();
	}

#ifndef __SCRIPT__
	io::InputStream& Config::read(io::InputStream& is) {
		ConfigSection macros(this);
		ConfigSection defs(this);
		ConfigSection modules(this);
		ConfigSection portfolio(this);
		ConfigSection pipeline(this);

		is.read("macros", &macros);
		is.read("defs", &defs);
		is.read("modules", &modules);
		is.read("portfolio", &portfolio);
		is.read("pipeline", &pipeline);

		/// Merge properties
		m_macros.mergePropertiesRecursively(macros);
		m_defs.mergePropertiesRecursively(defs);
		m_modules.mergePropertiesRecursively(modules);
		m_portfolio.mergePropertiesRecursively(portfolio);
		m_pipeline.mergePropertiesRecursively(pipeline);

		auto& imports = m_imports;
		PESA_READ_OBJ_VEC(is, imports, new ConfigSection(this));

		return is;
	}

	io::OutputStream& Config::write(io::OutputStream& os) {
		os.write("macros", m_macros);
		os.write("defs", m_defs);
		os.write("modules", m_modules);
		os.write("portfolio", m_portfolio);
		os.write("pipeline", m_pipeline);

		auto& imports = m_imports;
		PESA_WRITE_OBJ_VEC(os, imports);

		return os;
	}
#endif /// __SCRIPT__
} /// namespace pesa 
