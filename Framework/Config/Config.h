/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Config.h
///
/// Created on: 1 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/FrameworkDefs.h"
#include "ConfigDefs.h"

#include "Poco/Exception.h"

#include <vector>
#include <memory>

namespace pesa {

	class Config;
	typedef std::shared_ptr<Config> 	ConfigPtr;
	struct RuntimeData;

	////////////////////////////////////////////////////////////////////////////
	/// Config: The config for the simulator
	////////////////////////////////////////////////////////////////////////////
	class Framework_API Config : public io::Streamable {
	public:
		enum Type {
			kXML						= 0,
			kJSON						= 1,
			kMongoDB					= 2,
		};

	protected:
		ConfigSection					m_macros;				/// The macros section
		ConfigSection 					m_defs; 				/// The defs section of the config
		ConfigSection 					m_modules; 				/// The modules defined
		ConfigSection 					m_portfolio; 			/// The final portfolio calculation element
		ConfigSection 					m_pipeline; 			/// The pipeline to execute for the simulator
		std::string 					m_filename; 			/// Optional filename

		ConfigSectionPVec				m_imports;				/// The imports
		std::vector<Config> 			m_baseConfigs;			/// The base configs from which this config is derived from

		ConfigSectionPVec				m_inserts;				/// The inserts
		std::vector<Config> 			m_insertConfigs;		/// The configs that are inserted


		void 							merge(const Config& base);

		void							loadSubConfig(ConfigSection* importSec, std::vector<Config>& loadedConfigs);
		void							resolveImports();
		void							resolveInserts();

		void							validateUuids(const ConfigSection& section, std::unordered_map<std::string, bool>& uuids) const;
		void							copyImports(const ConfigSectionPVec& imports);
		void							doInserts(std::string name);

	public:
										Config(const Config& rhs);
										Config();
		virtual 						~Config();

		Config& 						operator = (const Config& rhs);
		void 							validate() const;
		void 							finalise();
		void							postLoad();
		ConfigSection*					section(std::string name);

		const ConfigSection*			getModuleFromModuleId(const ConfigSection& section) const;
		virtual void					start();
		virtual void					end(const RuntimeData& rt, int errCode);

		virtual void					exit(int errCode);
		virtual void					abort();

		virtual Poco::Timestamp			getModifiedTimestamp() const;

		const ConfigSection*			getChildSection(const std::vector<std::string>& sectionIds, int start = 0, int end = -1) const;

		std::string						dbName() const;

		//////////////////////////////////////////////////////////////////////////
		/// Streamable implementation
		//////////////////////////////////////////////////////////////////////////
#ifndef __SCRIPT__
		virtual io::InputStream&		read(io::InputStream& is);
		virtual io::OutputStream&		write(io::OutputStream& os);
		virtual void					tick(const RuntimeData& rt, float percent);
#endif 

		virtual void					overwriteModified();

		////////////////////////////////////////////////////////////////////////////
		/// Inline functions
		////////////////////////////////////////////////////////////////////////////
		inline ConfigSection& 			defs() 				{ return m_defs;		}
		inline const ConfigSection& 	defs() const		{ return m_defs;		}

		inline ConfigSection& 			modules() 			{ return m_modules;		}
		inline const ConfigSection& 	modules() const		{ return m_modules;		}

		inline ConfigSection& 			portfolio() 		{ return m_portfolio;	}
		inline const ConfigSection& 	portfolio() const	{ return m_portfolio;	}

		inline ConfigSection& 			pipeline() 			{ return m_pipeline;	}
		inline const ConfigSection&		pipeline() const	{ return m_pipeline;	}

		inline ConfigSection& 			macros() 			{ return m_macros;		}
		inline const ConfigSection&		macros() const		{ return m_macros;		}

		inline std::string& 			filename() 			{ return m_filename;	}
		inline std::string 				filename() const	{ return m_filename;	}

		inline std::vector<Config>&		baseConfigs() 		{ return m_baseConfigs; }
		inline ConfigSectionPVec&		imports()			{ return m_imports;		}
		inline ConfigSectionPVec&		inserts()			{ return m_inserts;		}

		////////////////////////////////////////////////////////////////////////////
		/// Helper static methods
		////////////////////////////////////////////////////////////////////////////
		static bool 					parseXml(const std::string& filename, Config& config, bool isImport = false);
		static bool 					parseJs(const std::string& filename, Config& config, bool isImport = false);
		static bool						loadMongoDB(Config& config);
		static bool						parse(const std::string& name, Config::Type type, Config& config);
	};

	typedef std::shared_ptr<Config>		ConfigPtr;
	typedef std::vector<ConfigPtr>		ConfigPtrVec;

} /// namespace pesa 

