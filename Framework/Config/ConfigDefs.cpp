/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// ConfigDefs.cpp
///
/// Created on: 2 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "ConfigDefs.h"
#include "Core/Lib/Util.h"
#include "Core/Lib/Application.h"
#include "Poco/String.h"

#if 0

namespace pesa {
namespace cfg {

	UnknownSection::UnknownSection(Config* config) : ConfigSection(config) {
	}

	UnknownSection::~UnknownSection() {
	}

	////////////////////////////////////////////////////////////////////////////
	/// ConfigDefs: The defs sectin of the config
	////////////////////////////////////////////////////////////////////////////
	ConfigDefs::ConfigDefs(Config* config, const ConfigDefs& rhs)
		: ConfigSection(config, rhs) {
	}

	ConfigDefs::ConfigDefs(Config* config)
		: ConfigSection(config) {
	}

	ConfigDefs::~ConfigDefs() {
	}

	unsigned int ConfigDefs::startDate() const {
		if (m_startDate != 0)
			return m_startDate;

		std::string startDate = get("startDate");
		m_startDate = Util::parseDate(startDate);
		return m_startDate;
	}

	unsigned int ConfigDefs::endDate() const {
		if (m_endDate != 0)
			return m_endDate;

		std::string endDate = get("endDate");
		m_endDate = Util::parseDate(endDate);
		return m_endDate;
	}

	std::string ConfigDefs::resolve(const std::string& str, unsigned int date /* = 0 */, bool onlyOnce /* = false */) const {
		if (date != 0) {
			Poco::DateTime dt = Util::intToDate(date);
			return resolve(str, &dt, onlyOnce);
		}

		return resolve(str, nullptr, onlyOnce);
	}

	std::string ConfigDefs::resolve(const std::string& str, const Poco::DateTime* date /* = nullptr */, bool onlyOnce /* = false */) const {
		std::string inString = str;

		/// Replace .so with .dylib for MacOS
		#ifdef __MACOS__
			const char* ext = ".dylib";
		#elif __WINDOWS__
			const char* ext = ".dll";
		#elif __LINUX__
			const char* ext = ".so";
		#else 
			ASSERT(false, "Unknown platform!");
		#endif /// __MACOS__

		inString = Poco::replace(inString, ".so", ext);
		inString = Poco::replace(inString, ".dll", ext);
		inString = Poco::replace(inString, ".dylib", ext);

		std::string homeDir = Poco::Path::home();
		inString = Poco::replace(inString, "~", homeDir.c_str());
		inString = Poco::replace(inString, "{$HOME}", homeDir.c_str());
		inString = Poco::replace(inString, "{$PWD}", Poco::Path::current().c_str());
		inString = Poco::replace(inString, "{$APP}", Application::instance()->cmdDir().toString().c_str());

		#if defined(__LINUX__) || defined(__MACOS__)
			/// If its a library path
			if (inString.find(ext) != std::string::npos) {
				size_t lastSlash = inString.rfind("/");
				
				if (lastSlash != std::string::npos) {
					/// find out if it already has a /lib
					size_t existingLib = inString.rfind("/lib");
					if (existingLib == std::string::npos || existingLib != lastSlash)
						inString.insert(lastSlash + 1, "lib");
				}
			}
		#endif /// __LINUX__ || __MACOS__

        size_t posStart = inString.find("{$");

        if (posStart != std::string::npos) {
			size_t posEnd = inString.find("}");

			ASSERT(posEnd != std::string::npos, "Missing } in macro expression: " << inString);
			ASSERT(posEnd > posStart, "Invalid } found in macro expansion: " << inString);

			std::string macroName = inString.substr(posStart + 2, posEnd - posStart - 2);
			std::string macroValue = this->get(macroName);

			if (macroName.empty() && date != nullptr) {
				if (macroName == "YYYY")
					macroValue = Util::cast(date->year());
				else if (macroName == "MM")
					macroValue = Util::cast(date->month());
				else if (macroName == "DD")
					macroValue = Util::cast(date->day());
			}

			ASSERT(!macroValue.empty(), "Undefined macro: " << macroName << " in expression: " << inString);
			std::string newString = Poco::replace(inString, "{$" + macroName + "}", macroValue);

			if (onlyOnce)
				return newString;

			return resolve(newString, date, onlyOnce);
		}

		#ifdef __WINDOWS__
			inString = Poco::replace(inString, "/", "\\");
			inString = Poco::replace(inString, "\\\\", "\\");
		#endif

		return inString;
	}

	////////////////////////////////////////////////////////////////////////////
	/// Module: Config for individual module
	////////////////////////////////////////////////////////////////////////////
	Module::Module(Config* config, const Module& rhs) : ConfigSection(config, rhs) {
	}

	Module::Module(Config* config) : ConfigSection(config) {
	}

	Module::~Module() {
	}

	std::string Module::toString(bool detailed /* = false */) const {
		std::ostringstream ss;
		ss << "id = " << id();

		if (!detailed)
			return ss.str();

		for (StrMap::const_iterator iter = m_map.begin(); iter != m_map.end(); iter++) {
			ss << "\n    " << iter->first << " = " << iter->second;
		}

		return ss.str();
	}

	////////////////////////////////////////////////////////////////////////////
	/// Modules: Config for the modules
	////////////////////////////////////////////////////////////////////////////
	Modules::Modules(Config* config) : ConfigSection(config) {
	}

	Modules::Modules(Config* config, const Modules& rhs)
		: ConfigSection(config, rhs) {
		for (auto* module : rhs.m_modules) {
			ConfigSection* m = new ConfigSection(config, *module);
			m_modules.push_back(m);
		}
	}

	Modules::Modules(Config* config, const ModulePVec& modules)
		: ConfigSection(config), m_modules(modules) {
		for (auto* module : m_modules)
			module->config() = config;
	}

	Modules::~Modules() {
		for (auto* module : m_modules)
			delete module;
	}

	std::string Modules::toString(bool detailed) const {
		std::string ret;
		for (auto* module : m_modules)
			ret += module->toString(detailed) + "\n";

		return ret;
	}

	Module* Modules::getDefaultModule() const {
		if (m_defModule)
			return m_defModule;

		for (auto* module : m_modules) {
			if (module->getBool("isDefault") == true) {
				m_defModule = module;
				return module;
			}
		}

		return nullptr;
	}

	Module* Modules::find(const std::string& id) const {
		for (auto* module : m_modules) {
			if (module->id() == id)
				return module;
		}
		return nullptr;
	}

	Module* Modules::findUnique(const std::string& key, const std::string& value) const {
		for (auto* module : m_modules) {
			if (module->getString(key, "") == value)
				return module;
		}
		return nullptr;
	}

	void Modules::merge(const Modules& base) {
		ConfigSection::merge(base);

        size_t start = 0;

        for (auto* baseModule : base.m_modules) {
   //         bool found = false;

			//for (size_t i = start; i < m_modules.size() && !found; i++) {
			//	if (*(m_modules[i]) == *baseModule) 
			//		found = true;
			//}

			///// Since these are coming from the base class, these need to go to the front of the vector ...
   //         if (!found)
			/// We just blindly assume that the modules are unique
			m_modules.insert(m_modules.begin() + start++, new Module(m_config, *baseModule));
		}
	}
	////////////////////////////////////////////////////////////////////////////
	/// Pipeline: Config for the pipeline
	////////////////////////////////////////////////////////////////////////////
	Pipeline::Pipeline(Config* config) : Modules(config) {
	}

	Pipeline::Pipeline(Config* config, const Pipeline& rhs)
		: Modules(config, rhs) {
	}

	Pipeline::Pipeline(Config* config, const ModulePVec& modules)
		: Modules(config, modules) {
	}

	Pipeline::~Pipeline() {
	}

	////////////////////////////////////////////////////////////////////////////
	/// Operation: Operation
	////////////////////////////////////////////////////////////////////////////
	Operation::Operation(Config* config, const Operation& rhs) : ConfigSection(config, rhs) {
	}

	Operation::Operation(Config* config) : ConfigSection(config) {
	}

	Operation::~Operation() {
	}

	std::string Operation::toString() const {
		std::ostringstream ss;
		ss <<
			"Id: " << id() <<
			"ModuleId: " << moduleId()
			;

		return ss.str();
	}

	////////////////////////////////////////////////////////////////////////////
	/// Combination: The way to combine alphas
	////////////////////////////////////////////////////////////////////////////
	Combination::Combination(Config* config, const Combination& rhs) : ConfigSection(config, rhs) {
	}

	Combination::Combination(Config* config) : ConfigSection(config) {
	}

	Combination::~Combination() {
	}

	std::string Combination::toString() const {
		std::ostringstream ss;
		ss <<
			"Id: " << id() <<
			"ModuleId: " << moduleId()
			;

		return ss.str();
	}

	////////////////////////////////////////////////////////////////////////////
	/// Alpha: Config for the alpha
	////////////////////////////////////////////////////////////////////////////
	Alpha::Alpha(Config* config) : ConfigSection(config) {
	}

	Alpha::Alpha(Config* config, const Alpha& rhs) : ConfigSection(config, rhs) {
	}

	Alpha::Alpha(Config* config, const OperationVec& operations) : ConfigSection(config) {
	}

	Alpha::~Alpha() {
	}

	std::string Alpha::toString() const {
		std::ostringstream ss;
		ss <<
			"Id: " << id() <<
			"Universe: " << universeId() <<
			"ModuleId: " << moduleId()
			;

		return ss.str();
	}

	////////////////////////////////////////////////////////////////////////////
	/// Alphas: Config for the alphas
	////////////////////////////////////////////////////////////////////////////
	Alphas::Alphas(Config* config)
		: ConfigSection(config)
		, m_combination(config) {
	}

	Alphas::Alphas(Config* config, const Alphas& rhs) 
		: ConfigSection(config, rhs)
		, m_combination(config) {
	}

	Alphas::Alphas(Config* config, const AlphaVec& alphas, const Combination& combination) 
		: ConfigSection(config)
		, m_combination(config, combination) {
	}

	Alphas::~Alphas() {
	}

	std::string Alphas::toString() const {
		std::ostringstream ss;
		std::string id = this->get<std::string>("id", "<NO-ID>");

		ss << "Id: " << id;

		return ss.str();
	}

	////////////////////////////////////////////////////////////////////////////
	/// Portfolio: Portfolio element
	////////////////////////////////////////////////////////////////////////////
	Portfolio::Portfolio(Config* config)
		: ConfigSection(config)
		, m_alphas(config) {
	}

	Portfolio::Portfolio(Config* config, const Portfolio& rhs) 
		: ConfigSection(config, rhs)
		, m_alphas(config, rhs.m_alphas)
		, m_operations(rhs.m_operations) {
	}

	Portfolio::Portfolio(Config* config, const Alphas& alphas, const OperationVec& operations) 
		: ConfigSection(config)
		, m_alphas(config, alphas)
		, m_operations(operations) {
	}

	Portfolio::~Portfolio() {
	}

	float Portfolio::bookSize() const {
		if (m_bookSize != 0.0f)
			return m_bookSize;

		std::string bookSizeStr = get("bookSize");
		m_bookSize = Util::amount(bookSizeStr);
		return m_bookSize;
	}
} /// namespace cfg 
} /// namespace pesa 

#endif 