/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// ConfigDefs.h
///
/// Created on: 2 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#ifndef CONFIG_CONFIGDEFS_H_
#define CONFIG_CONFIGDEFS_H_

#include "Framework/FrameworkDefs.h"
#include "ConfigSection.h"

#include "Poco/DateTime.h"

#include <string>
#include <vector>

#if 0

namespace pesa {
namespace cfg {

	////////////////////////////////////////////////////////////////////////////
	/// ConfigDefs: The defs sectin of the config
	////////////////////////////////////////////////////////////////////////////
	class Framework_API ConfigDefs : public ConfigSection {
	private:
		mutable unsigned int 		m_startDate = 0;
		mutable unsigned int 		m_endDate = 0;

	public:
									ConfigDefs(Config* config, const ConfigDefs& rhs);
									ConfigDefs(Config* config);
									~ConfigDefs();

		Cfg_Property(std::string, 	dataPath);
		Cfg_Property(std::string, 	dataCachePath);
		Cfg_Property(unsigned int, 	backdays);

		Cfg_Property(std::string, 	market);

		unsigned int				startDate() const;
		unsigned int				endDate() const;

		std::string 				resolve(const std::string& str, unsigned int date, bool onlyOnce = false) const;
		std::string 				resolve(const std::string& str, const Poco::DateTime* date, bool onlyOnce = false) const;
	};

	class Framework_API UnknownSection : public ConfigSection {
	public:
		UnknownSection(Config* config);
		~UnknownSection();
	};

	////////////////////////////////////////////////////////////////////////////
	/// Module: Config for individual module
	////////////////////////////////////////////////////////////////////////////
	class Framework_API Module : public ConfigSection {
	public:
									Module(Config* config, const Module& rhs);
									Module(Config* config);
									~Module();

		virtual std::string 		toString(bool detailed = false) const;

		Cfg_Property(std::string, 	id);
		Cfg_Property(std::string, 	path);
		Cfg_Property(std::string, 	dataPath);
		Cfg_Property(std::string, 	handler);
		Cfg_Property(std::string, 	type);

#ifndef __SCRIPT__
		Cfg_OptProperty(bool,		wrapper, false);
		Cfg_OptProperty(bool,		optimiseForUniverse, false);
#endif 

		inline bool 				isUniverse() const 	{ return type() == "universe";	}
		inline bool 				isSecMaster() const { return type() == "securitymaster";	}
	};
	typedef std::vector<Module> 	ModuleVec;
	typedef std::vector<Module*> 	ModulePVec;

	////////////////////////////////////////////////////////////////////////////
	/// Modules: Config for the modules
	////////////////////////////////////////////////////////////////////////////
	class Framework_API Modules : public ConfigSection {
	private:
		ModulePVec 					m_modules;
		mutable Module* 			m_defModule = nullptr;

	public:
									Modules(Config* config);
									Modules(Config* config, const Modules& rhs);
									Modules(Config* config, const ModulePVec& modules);
									~Modules();

		virtual std::string 		toString(bool detailed = false) const;
		virtual void 				merge(const Modules& base);
		Module* 					find(const std::string& id) const;
		Module* 					findUnique(const std::string& key, const std::string& value) const;
		Module* 					getDefaultModule() const;

		inline const ModulePVec&	modules() const 	{ return m_modules; }
		inline ModulePVec&			modules() 			{ return m_modules; }
	};

	////////////////////////////////////////////////////////////////////////////
	/// Pipeline: Config for the pipeline
	////////////////////////////////////////////////////////////////////////////
	class Framework_API Pipeline : public Modules {
	public:
									Pipeline(Config* config);
									Pipeline(Config* config, const Pipeline& rhs);
									Pipeline(Config* config, const ModulePVec& modules);
									~Pipeline();
	};

	////////////////////////////////////////////////////////////////////////////
	/// Operation: Operation
	////////////////////////////////////////////////////////////////////////////
	class Framework_API Operation : public ConfigSection {
	public:
									Operation(Config* config, const Operation& rhs);
									Operation(Config* config);
									~Operation();

		Cfg_Property(std::string, 	id);

#ifndef __SCRIPT__
		Cfg_OptProperty(std::string,moduleId, "");
#endif

		std::string 				toString() const;
	};
	typedef std::vector<Operation> 	OperationVec;

	////////////////////////////////////////////////////////////////////////////
	/// Combination: The way to combine alphas
	////////////////////////////////////////////////////////////////////////////
	class Framework_API Combination : public ConfigSection {
	public:
									Combination(Config* config, const Combination& rhs);
									Combination(Config* config);
									~Combination();

		Cfg_Property(std::string, 	id);

#ifndef __SCRIPT__
		Cfg_OptProperty(std::string,moduleId, "");
#endif

		std::string 				toString() const;
	};

	////////////////////////////////////////////////////////////////////////////
	/// Alpha: Config for the alpha
	////////////////////////////////////////////////////////////////////////////
	class Framework_API Alpha : public ConfigSection {
	private:
		OperationVec 				m_operations;

	public:
									Alpha(Config* config);
									Alpha(Config* config, const Alpha& rhs);
									Alpha(Config* config, const OperationVec& operations);
									~Alpha();

		Cfg_Property(std::string, 	id);
		Cfg_Property(std::string, 	universeId);

#ifndef __SCRIPT__
		Cfg_OptProperty(std::string,moduleId, "");
#endif
		
#ifndef __SCRIPT__
		Cfg_OptProperty(int,		delay, 1);
		Cfg_OptProperty(float,		weight, 1.0f);
#endif 

		std::string 				toString() const;

		inline OperationVec& 		operations() 		{ return m_operations; 	}
		inline const OperationVec& 	operations() const	{ return m_operations; 	}
	};
	typedef std::vector<Alpha> 		AlphaVec;

	////////////////////////////////////////////////////////////////////////////
	/// Alphas: Config for the alphas
	////////////////////////////////////////////////////////////////////////////
	class Framework_API Alphas : public ConfigSection {
	private:
		AlphaVec 					m_alphas;
		Combination 				m_combination;

	public:
									Alphas(Config* config);
									Alphas(Config* config, const Alphas& rhs);
									Alphas(Config* config, const AlphaVec& alphas, const Combination& combination);
									~Alphas();

		std::string 				toString() const;

		inline Combination& 		combination() 		{ return m_combination; }
		inline const Combination& 	combination() const	{ return m_combination; }
		inline AlphaVec&			alphas() 			{ return m_alphas; 		}
		inline const AlphaVec&		alphas() const 		{ return m_alphas; 		}
	};

	////////////////////////////////////////////////////////////////////////////
	/// Portfolio: Portfolio element
	////////////////////////////////////////////////////////////////////////////
	class Framework_API Portfolio : public ConfigSection {
	private:
		Alphas 						m_alphas;
		OperationVec 				m_operations;
		mutable float 				m_bookSize = 0;

	public:
									Portfolio(Config* config);
									Portfolio(Config* config, const Portfolio& rhs);
									Portfolio(Config* config, const Alphas& alphas, const OperationVec& operations);
									~Portfolio();

		Cfg_Property(std::string, 	id);

#ifndef __SCRIPT__
		Cfg_OptProperty(std::string,moduleId, "");
		Cfg_OptBoolProperty(		debug, false);
#endif

		float 						bookSize() const;

		inline Alphas& 				alphas() 			{ return m_alphas; 		}
		inline const Alphas& 		alphas() const		{ return m_alphas; 		}
		inline OperationVec& 		operations() 		{ return m_operations; 	}
		inline const OperationVec& 	operations() const	{ return m_operations; 	}
	};

} /// namespace cfg 
} /// namespace pesa 

#endif 

#endif /// CONFIG_CONFIGDEFS_H_
