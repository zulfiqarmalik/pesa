/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// ConfigSection.cpp
///
/// Created on: 1 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "ConfigSection.h"
#include "Config.h"
#include <sstream>
#include <type_traits>

#include "Core/Lib/Util.h"
#include "Core/Lib/Application.h"
#include "Poco/String.h"
#include "Poco/Environment.h"

#include "Framework/Helper/FrameworkUtil.h"

namespace pesa {

	ConfigSection::ConfigSection(Config* config)
		: m_config(config) {
	}

	ConfigSection::ConfigSection(const ConfigSection& rhs) {
		*this = rhs;
	}

	ConfigSection::ConfigSection(Config* config, const ConfigSection& rhs, bool copyChildren) 
		: m_config(config) {
		if (copyChildren)
			*this = rhs;
		else {
			m_map = rhs.m_map;
			m_name = rhs.m_name;
			m_inner = rhs.m_inner;
			m_isUnknown = rhs.m_isUnknown;
			m_line = rhs.m_line;
		}

		m_config = config;
	}

	ConfigSection::~ConfigSection() {
		deleteAllChildren();
	}

	void ConfigSection::remove(const std::string& key) {
		auto iter = m_map.find(key);
		if (iter != m_map.end())
			m_map.erase(iter);
	}

	void ConfigSection::deleteAllChildren() {
		for (auto* child : m_children)
			delete child;

		m_children.clear();
	}

	void ConfigSection::deleteAllChildrenOfType(const std::string& type) {
		for (auto iter = m_children.begin(); iter != m_children.end(); iter++) {
			auto* child = *iter;
			if (child->name() == type) {
				iter = m_children.erase(iter);
				delete child;
			}
		}
	}

	bool ConfigSection::deleteChildSection(ConfigSection* childToDelete) {
		auto iter = m_children.begin();
		bool found = false;

		while (iter != m_children.end() && !found) {
			ConfigSection* child = *iter;
			if (child == childToDelete) {
				found = true;
				break;
			}
			iter++;
		}

		if (found && iter != m_children.end()) {
			delete *iter;
			m_children.erase(iter);
		}

		return found;
	}

	bool ConfigSection::deleteChildSection(const std::string& name) {
		auto iter = m_children.begin();
		bool found = false;

		while (iter != m_children.end() && !found) {
			ConfigSection* child = *iter;
			if (child->name() == name) {
				found = true;
				break;
			}
			iter++;
		}

		if (found && iter != m_children.end()) {
			delete *iter;
			m_children.erase(iter);
		}

		return found;
	}

#ifndef __SCRIPT__
	io::InputStream& ConfigSection::read(io::InputStream& is) {
		PESA_READ_VAR(is, m_name);

		StrMap map;
		PESA_READ_VAR(is, map);

		if (!m_map.empty()) {
			for (const auto iter : map)
				m_map[iter.first] = iter.second;
		}
		else
			m_map = std::move(map);

		ConfigSectionPVec children;
		PESA_READ_OBJ_VEC(is, children, new ConfigSection(m_config));

		if (!m_children.empty()) 
			m_children.insert(m_children.end(), children.begin(), children.end());
		else
			m_children = std::move(children);

		return is;
	}

	io::OutputStream& ConfigSection::write(io::OutputStream& os) {
		PESA_WRITE_VAR(os, m_name);

		//if (m_map.size()) {
			auto& map = m_map;
			PESA_WRITE_VAR(os, map);
		//}

		//if (m_children.size()) {
			auto& children = m_children;
			PESA_WRITE_OBJ_VEC(os, children);
		//}

		return os;
	}
#endif /// __SCRIPT__

	std::string ConfigSection::toString(bool detailed /* = false */) const {
		std::ostringstream ss;
		ss << m_name;

		if (!detailed)
			return ss.str();

		for (StrMap::const_iterator iter = m_map.begin(); iter != m_map.end(); iter++) {
			ss << "\n    " << iter->first << " = " << iter->second;
		}

		return ss.str();
	}

	bool ConfigSection::operator != (const ConfigSection& rhs) {
		return !(*this == rhs);
	}
	
	bool ConfigSection::operator == (const ConfigSection& rhs) {
		std::string myId, otherId;

		return 	rhs.get("id", otherId) && get("id", myId) && 
				!myId.empty() && myId == otherId;
	}

	void ConfigSection::clear() {
		m_map.clear();
		deleteAllChildren();
	}

	ConfigSection& ConfigSection::operator = (const ConfigSection& rhs) {
		m_map		= rhs.m_map;
		m_name		= rhs.m_name;
		m_inner		= rhs.m_inner;
		m_isUnknown = rhs.m_isUnknown;
		m_line		= rhs.m_line;

		//ASSERT(m_children.size() < 50, "Invalid state. Should not be copying such a large object recursively!");
		ASSERT(!m_children.size(), "Memory leak!");
		for (ConfigSection* child : rhs.m_children) {
			//ConfigSection* newChildSection = new std::remove_pointer<decltype(child)>::type(m_config, *child);
			ConfigSection* newChildSection = new ConfigSection(m_config, *child);
			addChildSection(newChildSection);
		}

		if (!m_config)
			m_config = rhs.m_config;

		return *this;
	}

	ConfigSection* ConfigSection::operationsSec() {
		return getChildSection("Operations");
	}

	ConfigSection* ConfigSection::alphasSec() {
		return getChildSection("Alphas");
	}

	ConfigSection* ConfigSection::combinationSec() {
		return getChildSection("Combination");
	}

	void ConfigSection::expandDerived() {
		for (auto* child : m_children) {
			std::string value = child->getString("_PARENT_", "");

			if (!value.empty()) {
				/// Get this dummy
				const ConfigSection* parent = this->getParent(value);
				ASSERT(parent, "Unable to find parent: " << value);
				child->makeParent(*parent, true);
			}
		}

		this->deleteAllChildrenOfType("_PARENT_");
	}

	void ConfigSection::resolveAllChildren(const ConfigSection& defs, unsigned int date) {
		/// Resolve the keys first ...
		for (auto iter = m_map.begin(); iter != m_map.end(); iter++) {
			iter->second = defs.resolve(iter->second, date);
		}

		/// Then resolve all the children
		for (auto* child : m_children) {
			child->resolveAllChildren(defs, date);
		}
	}

	void ConfigSection::expandForEach() {
		ConfigSectionPVec forEachChildren;

		for (auto iter = m_children.begin(); iter != m_children.end(); iter++) {
			auto* forEachChild = *iter;
			auto isForEach = forEachChild->getBool("_FOR_EACH_", false);
			if (!isForEach)
				continue;

			/// We need to get the iterator
			std::string iterValue = forEachChild->getRequired("_ITER_");
			std::map<std::string, std::string> mappings;
			FrameworkUtil::parseMappings(iterValue, &mappings, nullptr, "|");

			StringVec keys;
			std::vector<std::vector<std::string> > values;

			for (auto iter = mappings.begin(); iter != mappings.end(); iter++) {
				auto key = iter->first;
				StringVec kvalues = Util::split(iter->second, ",");
				keys.push_back(key);
				values.push_back(kvalues);
			}

			ASSERT(values.size(), "Invalid _FOR_EACH_ section!"); 
			size_t valueIter = 0;
			size_t expectedSize = values[0].size();

			while (valueIter < expectedSize) {
				/// Make a new child that is derived from the _FOR_EACH_ section
				ConfigSection* newChild = new ConfigSection(*forEachChild);

				for (size_t k = 0; k < keys.size(); k++) {
					std::string key = keys[k];
					ASSERT(values[k].size() == expectedSize, "Invalid key: " << key);
					std::string value = values[k][valueIter];
					std::string rkey = "{$" + key + "}";

					for (auto mapIter = newChild->m_map.begin(); mapIter != newChild->m_map.end(); mapIter++) {
						Poco::replaceInPlace(mapIter->second, rkey, value);
					}

					for (auto grandchild : newChild->m_children) {
						for (auto mapIter = grandchild->m_map.begin(); mapIter != grandchild->m_map.end(); mapIter++) {
							Poco::replaceInPlace(mapIter->second, rkey, value);
						}
					}
				}

				//std::string type = forEachChild->getRequired("_type_");
				//newChild->set("type", type);
				//newChild->name() = 

				newChild->set("type", Poco::toLower(forEachChild->name()));
				newChild->remove("_FOR_EACH_");
				//newChild->remove("_PARENT_");
				//newChild->remove("_ITER_");

				/// Add this new child ...
				//this->addChildSection(newChild);
				iter++;
				iter = m_children.insert(iter, newChild);

				valueIter++;
			}

			/// We don't need the for each section anymore
			//this->deleteChildSection(forEachChild);
			forEachChildren.push_back(forEachChild);
		}

		for (auto* forEachChild : forEachChildren)
			deleteChildSection(forEachChild);
	}

	void ConfigSection::expandMacros() {
		const auto& userMacros = Application::instance()->appOptions().userMacros;
		if (!userMacros.size())
			return;

		/// Do it recursively
		for (auto* child : m_children) 
			child->expandMacros();

		for (auto iter = userMacros.begin(); iter != userMacros.end(); iter++) {
			std::string key = "{$" + iter->first + "}";
			std::string value = iter->second;

			for (auto mapIter = m_map.begin(); mapIter != m_map.end(); mapIter++) 
				Poco::replaceInPlace(mapIter->second, key, value);
		}
	}

	void ConfigSection::finalise(bool expandMacros /* = false */) {
		expandDerived();
		expandForEach();

		if (expandMacros == true && Application::instance()->appOptions().userMacros.size()) {
			this->expandMacros();
		}
	}

	const ConfigSection* ConfigSection::getParent(const std::string& dummyId) const {
		for (const auto* child : m_children) {
			if (child->name() == "_PARENT_" && child->getString("parentId", "") == dummyId)
				return child;
		}

		return nullptr;
	}

	void ConfigSection::addChildSection(ConfigSection* child) { 
		/// We need to clear the cache because it might not be valid anymore ...
		m_namedChildrenCache.clear();
		m_children.push_back(child); 
	}

	const ConfigSection* ConfigSection::getChildSection(const std::string& name) const {
		return const_cast<ConfigSection*>(this)->getChildSection(name);
	}

	const ConfigSection* ConfigSection::getChildSectionWithId(const std::string& id) const {
		return const_cast<ConfigSection*>(this)->getChildSection(id);
	}

	const ConfigSection* ConfigSection::getChildSectionWithNameAndId(const std::string& name, const std::string& id) const {
		return getChildSectionWithNameAndProperty(name, "id", id);
	}

	const ConfigSection* ConfigSection::getChildSectionWithNameAndProperty(const std::string& name, const std::string& propertyName, const std::string& propertyValue) const {
		for (size_t i = 0; i < m_children.size(); i++) {
			if (m_children[i]->name() == name) {
				std::string value = m_children[i]->getString(propertyName, "");
				if (!value.empty() && value == propertyValue)
					return m_children[i];
			}
		}

		return nullptr;
	}

	ConfigSection* ConfigSection::getChildSectionWithId(const std::string& id) {
		for (size_t i = 0; i < m_children.size(); i++) {
			if (m_children[i]->id() == id)
				return m_children[i];
		}

		return nullptr;
	}

	ConfigSectionPVec ConfigSection::modules() const {
		return children();
	}

	ConfigSectionPVec ConfigSection::operations() const {
		return getAllChildrenOfType("Operation");
	}

	ConfigSectionPVec ConfigSection::alphas() const {
		return getAllChildrenOfType("Alpha");
	}

	ConfigSectionPVec ConfigSection::getAllChildrenOfField(const std::string& fieldName, const std::string& fieldValue) const {
		auto iter = m_namedChildrenCache.find(fieldName);

		if (iter != m_namedChildrenCache.end())
			return iter->second;

		ConfigSectionPVec result;

		for (auto* child : m_children) {
			if (child->getString(fieldName, "") == fieldValue)
				result.push_back(child);
		}

		m_namedChildrenCache[fieldValue] = result;
		return result;
	}
		
	ConfigSectionPVec ConfigSection::getAllChildrenOfType(const std::string& name) const {
		auto iter = m_namedChildrenCache.find(name);

		if (iter != m_namedChildrenCache.end())
			return iter->second;

		ConfigSectionPVec result;

		for (auto* child : m_children) {
			if (child->name() == name)
				result.push_back(child);
		}

		m_namedChildrenCache[std::string(name)] = result;
		return result;
	}

	ConfigSection* ConfigSection::getChildSection(const std::string& name) {
		for (size_t i = 0; i < m_children.size(); i++) {
			if (m_children[i]->m_name == name)
				return m_children[i];
		}

		return nullptr;
	}

	void ConfigSection::makeParent(const ConfigSection& parent, bool copyChildren /* = false */) {
		for (auto iter = parent.m_map.begin(); iter != parent.m_map.end(); iter++) {
			m_map[iter->first] = iter->second;
		}

		if (copyChildren) {
			for (auto* pchild : parent.m_children) {
				ConfigSection* cchild = new ConfigSection(m_config, *pchild, true);
				m_children.push_back(cchild);
			}
		}
	}

	void ConfigSection::mergePropertiesRecursively(const ConfigSection& derived) {
		for (StrMap::const_iterator iter = derived.m_map.begin(); iter != derived.m_map.end(); iter++) {
			// if the entry does not exist in the derived map then put it in there ...
			if (iter->first != "id") {
				m_map[iter->first] = iter->second;
			}
		}

		for (size_t i = 0; i < derived.m_children.size(); i++) {
			auto* derivedChild = derived.m_children[i];
			auto* thisChild = findByName(derivedChild->name());

			if (thisChild)
				thisChild->mergePropertiesRecursively(*derivedChild);
			else
				m_children.push_back(new ConfigSection(m_config, *derivedChild));
		}
	}

	void ConfigSection::mergeProperties(const ConfigSection& derived, pesa::StringVec* exceptions /* = nullptr */, bool ignoreChildSections /* = false */) {
		for (StrMap::const_iterator iter = derived.m_map.begin(); iter != derived.m_map.end(); iter++) {
			// if the entry does not exist in the derived map then put it in there ...
			if (m_map.find(iter->first) == m_map.end()) {
				if (iter->first == "id" || (exceptions && Util::isInVector(*exceptions, iter->first)))
					continue;
				m_map[iter->first] = iter->second;
			}
		}

		//ConfigSectionPVec existingChildren;

		//// merge all the children now ...
		//if (!ignoreChildSections) {
		//	for (size_t i = 0; i < derived.m_children.size(); i++) {
		//		auto* baseChildSection = derived.m_children[i];
		//		ConfigSection* newChildSection = new ConfigSection(m_config, *baseChildSection);
		//		m_children.push_back(newChildSection);
		//	}
		//}
	}

	void ConfigSection::merge(const ConfigSection& derived, pesa::StringVec* exceptions /* = nullptr */, bool ignoreChildSections, bool noCheckCompatibility) {
		/// Nothing to merge over here 
		if (derived.m_children.empty() && derived.m_map.empty())
			return;

		ASSERT(name() == derived.name() || noCheckCompatibility, "Invalid merge request. Trying to merge: " << name() << " with: " << derived.name());

		for (StrMap::const_iterator iter = derived.m_map.begin(); iter != derived.m_map.end(); iter++) {
			//if (iter->first == "id" || (exceptions && Util::isInVector(*exceptions, iter->first)))
			//	continue;
			m_map[iter->first] = iter->second;
		}

		/// TODO: Fix this!
		//for (StrMap::const_iterator iter = derived.m_map.begin(); iter != derived.m_map.end(); iter++) {
		//	// if the entry does not exist in the derived map then put it in there ...
		//	if (m_map.find(iter->first) == m_map.end()) {
		//		if (iter->first == "id" || (exceptions && Util::isInVector(*exceptions, iter->first)))
		//			continue;
		//		m_map[iter->first] = iter->second;
		//	}
		//}

		ConfigSectionPVec existingChildren;

		//if (reverse) {
		//	existingChildren = m_children;
		//	m_children.clear();
		//}

		// merge all the children now ...
		if (!ignoreChildSections) {
			for (size_t i = 0; i < derived.m_children.size(); i++) {
				auto* derivedChildSection = derived.m_children[i];

				/// If the child section has nothing ... then we don't do anything
				if (!derivedChildSection->m_map.size() && !derivedChildSection->m_children.size())
					continue;

				auto name = derivedChildSection->name();
				ConfigSection* existingChild = nullptr;
				auto id = derivedChildSection->getString("id", "");

				if (!name.empty() && derivedChildSection->name() == "Module" && !id.empty()) {
					existingChild = getChildSectionWithId(id); 

					/// Has to be a module
					if (existingChild && existingChild->name() != "Module")
						existingChild = nullptr;
				}

				if (!existingChild) {
					ConfigSection* newChildSection = new ConfigSection(m_config, *derivedChildSection);
					m_children.push_back(newChildSection);
				}
				else {
					/// merge the derived child section
					existingChild->merge(*derivedChildSection);
				}
			}
		}

		//if (reverse && existingChildren.size()) 
		//	m_children.insert(m_children.end(), existingChildren.begin(), existingChildren.end());
	}

	std::string ConfigSection::getResolved(const std::string& key, unsigned int date /* = 0U */) const {
		std::string value;

		if (get(key, value)) 
			return m_config->defs().resolve(value, date);

		return value;
	}

	unsigned int ConfigSection::startDate() const {
		std::string startDate = getString("startDate", "");
		if (startDate.empty())
			return 0;
		return Util::parseDate(startDate);
	}

	unsigned int ConfigSection::endDate() const {
		std::string endDate = getString("endDate", "TODAY");
		if (endDate.empty())
			return 1;
		return Util::parseDate(endDate);
	}

	std::string ConfigSection::resolve(const std::string& str, unsigned int date /* = 0 */, bool onlyOnce /* = false */) const {
		if (date != 0) {
			Poco::DateTime dt = Util::intToDate(date);
			return resolve(str, &dt, onlyOnce);
		}

		return resolve(str, nullptr, onlyOnce);
	}

#ifndef __SCRIPT__
	std::string ConfigSection::resolve(const std::string& str, const Poco::DateTime* date_ /* = nullptr */, bool onlyOnce /* = false */) const {
		std::string inString = str;

		Poco::DateTime date = date_ ? *date_ : Poco::DateTime();

		/// Replace .so with .dylib for MacOS
#ifdef __MACOS__
		const char* ext = ".dylib";
#elif __WINDOWS__
		const char* ext = ".dll";
#elif __LINUX__
		const char* ext = ".so";
#else 
		ASSERT(false, "Unknown platform!");
#endif /// __MACOS__

		inString = Poco::replace(inString, ".so", ext);
		inString = Poco::replace(inString, ".dll", ext);
		inString = Poco::replace(inString, ".dylib", ext);

		std::string homeDir = Poco::Path::home();
		inString = Poco::replace(inString, "~", homeDir.c_str());
		inString = Poco::replace(inString, "{$HOME}", homeDir.c_str());
		inString = Poco::replace(inString, "{$PWD}", Poco::Path::current().c_str());
		inString = Poco::replace(inString, "{$APP}", Application::instance()->cmdDir().toString().c_str());
		inString = Poco::replace(inString, "{$APP_VER}", Application::instance()->version().c_str());
		inString = Poco::replace(inString, "{$APP_PREV_VER}", Application::instance()->prevVersion().c_str());
		inString = Poco::replace(inString, "{$PORT_UUID}", m_config->portfolio().getString("uuid", "").c_str());
		//inString = Poco::replace(inString, "{$DLL_EXT}", ext);

		// #ifdef __WINDOWS__
// 		Poco::Path configsPath("Y:\\configs");
// #else
// 		Poco::Path configsPath(homeDir + "/drv/configs");
// #endif 
		// configsPath.append(Application::instance()->version());
		// std::string configsPathStr = configsPath.toString(); 
		Poco::Path appPath = Application::instance()->cmdDir();
		appPath.append("Configs");
		auto configsPathStr = appPath.toString();
		inString = Poco::replace(inString, "{$CFG_DIR}", configsPathStr.c_str());

		if (inString.find("./") == 0) {
			Poco::Path configPath(m_config->filename());
			inString = Poco::replace(inString, "./", configPath.makeParent().toString().c_str());
		}

#if defined(__LINUX__) || defined(__MACOS__)
		/// If its a library path
		if (inString.find(ext) != std::string::npos) {
			size_t lastSlash = inString.rfind("/");

			if (lastSlash != std::string::npos) {
				/// find out if it already has a /lib
				size_t existingLib = inString.rfind("/lib");
				if (existingLib == std::string::npos || existingLib != lastSlash)
					inString.insert(lastSlash + 1, "lib");
			}
		}
#endif /// __LINUX__ || __MACOS__

		size_t posStart = inString.find("{$");

		if (posStart != std::string::npos) {
			size_t posEnd = inString.find("}");

			ASSERT(posEnd != std::string::npos, "Missing } in macro expression: " << inString);
			ASSERT(posEnd > posStart, "Invalid } found in macro expansion: " << inString);

			std::string macroName = inString.substr(posStart + 2, posEnd - posStart - 2);
			std::string macroValue;

			if (!macroName.empty()) {
				if (macroName == "YYYY")
					macroValue = Util::cast(date.year());
				else if (macroName == "MM")
				    macroValue = Util::castPadded(date.month(), 2);
				else if (macroName.find("MM-") == 0) {
                    int numMonths = Util::cast<int>(macroName.substr(3));
                    date -= Poco::Timespan(30 * numMonths, 0, 0, 0, 0);
                    macroValue = Util::cast(date.month());
				}
				else if (macroName == "M")
					macroValue = Util::cast(date.month());
				else if (macroName.find("M-") == 0) {
				    int numMonths = Util::cast<int>(macroName.substr(2));
				    date -= Poco::Timespan(30 * numMonths, 0, 0, 0, 0);
                    macroValue = Util::cast(date.month());
				}
				else if (macroName == "DD")
				    macroValue = Util::castPadded(date.day(), 2);
				else if (macroName == "D")
					macroValue = Util::cast(date.day());
				else if (macroName == "hh")
					macroValue = Util::castPadded(date.hour(), 2);
				else if (macroName == "mm")
					macroValue = Util::castPadded(date.minute(), 2);
				else if (macroName == "ss")
					macroValue = Util::castPadded(date.second(), 2);
				else {
					/// Try to find the macro in the defs section
					auto defsValue = m_config->defs().getString(macroName, "");
					if (!defsValue.empty())
						macroValue = defsValue;
					else 
						macroValue = Poco::Environment::get(macroName, "");
				}
			}

			if (macroValue.empty())
				macroValue = getMacro(macroName);

			if (macroValue.empty())
				Warning("CFG", "Empty or undefined macro: %s", macroName);

			std::string newString = Poco::replace(inString, "{$" + macroName + "}", macroValue);

			if (onlyOnce)
				return newString;

			return resolve(newString, &date, onlyOnce);
		}

#ifdef __WINDOWS__
		if (inString.find("://") == std::string::npos) {
			inString = Poco::replace(inString, "/", "\\");
			inString = Poco::replace(inString, "\\\\", "\\");
		}
#endif

		return inString;
	}
#endif 

	const ConfigSection* ConfigSection::getChildSectionAtIndex(size_t index) const {
		ASSERT(index < m_children.size(), "Invalid index: " << index << " - Num children: " << m_children.size());
		return m_children[index];
	}

	std::string ConfigSection::getMacro(const std::string& macroName, bool noThrow /* = false */) const {
		/// User defined macros override the config macros
		const auto& userMacros = Application::instance()->appOptions().userMacros;
		auto iter = userMacros.find(macroName);
		if (iter != userMacros.end())
			return iter->second;

		std::string value;
		bool didFind = this->get(macroName, value);
		if (didFind)
			return value;

		const auto& macros = m_config->macros();
		const auto* macroSec = macros.getChildSection(macroName);

		/// Don't throw an error!
		if (!macroSec && noThrow)
			return "";

		ASSERT(macroSec, "No macro with name: " << macroName);

		didFind = macroSec->get("value", value);
		if (didFind)
			return value;

#ifdef __WINDOWS__
		return macroSec->getRequired("Windows");
#else
		return macroSec->getRequired("Linux");
#endif
	}

	std::string ConfigSection::getCurrency() const {
		auto ccy = getString("currency", "");

		/// If we have a currency section then just use that ...
		if (!ccy.empty())
			return ccy;

		/// After that we try the portfolio section
		ccy = m_config->portfolio().getString("currency", "");
		if (!ccy.empty())
			return ccy;
		
		/// In the end we are going to try out the defs section
		ccy = m_config->defs().getString("currency", "");
		return ccy;
	}

	std::string ConfigSection::getServerUrl(const std::string key_ /* = "serverUrl" */, unsigned int date /* = 0 */) const {
		std::string key = key_;
		if (key.empty())
			key = "serverUrl";

		std::string serverUrl = getResolvedString(key, "", date);
		if (serverUrl.empty())
			return m_config->defs().getResolvedString(key, "", date);

		return serverUrl;
	}

	std::string ConfigSection::getYml(const std::string key /* = "yml" */) const {
		std::string yml = getRequired(key);
		std::string ymlDir = this->getString("ymlDir", ""); 

		if (ymlDir.empty()) 
			ymlDir = m_config->defs().getString("ymlDir", "");

		/// If there is no YML directory in the Defs section!
		if (ymlDir.empty() || (yml.length() > 2 && yml[0] == '\\' && yml[1] == '\\'))
			return yml;

		return ymlDir + "\\" + yml;
	}

	std::string ConfigSection::getKeyword(const std::string key /* = "keyword" */) const {
		std::string keyword = getRequired(key);
		return m_config->defs().resolve(keyword, nullptr);
	}

	float ConfigSection::bookSize() const {
		std::string bookSizeStr = get("bookSize");
		if (!bookSizeStr.empty())
			return Util::amount(bookSizeStr);
		return 0.0f;
	}

	void ConfigSection::getChildStrategies(ConfigSectionPVec& childStrats, bool recursive /* = true */) const {
		auto childStrategyConfigs = this->getAllChildrenOfType("Strategy");

		for (auto i = 0; i < childStrategyConfigs.size(); i++) {
			auto* childStrat = childStrategyConfigs[i];
			childStrats.push_back(childStrat);
		}

		if (recursive) {
			auto childAlphasConfigs = this->getAllChildrenOfType("Alphas");

			for (auto i = 0; i < childAlphasConfigs.size(); i++)
				childAlphasConfigs[i]->getChildStrategies(childStrats, recursive);
		}
	}

	ConfigSection* ConfigSection::findUnique(const std::string& key, const std::string& value) const {
		for (auto* child : m_children) {
			if (child->getString(key, "") == value)
				return child;
		}
		return nullptr;
	}

	ConfigSection* ConfigSection::find(const std::string& id) const {
		for (auto* child : m_children) {
			if (child->id() == id)
				return child;
		}
		return nullptr;
	}

	ConfigSection* ConfigSection::findByName(const std::string& name) const {
		for (auto* child : m_children) {
			if (child->name() == name)
				return child;
		}
		return nullptr;
	}

	ConfigSection* ConfigSection::getDefaultModule() const {
		if (m_defModule)
			return m_defModule;

		for (auto* child : m_children) {
			if (child->getBool("isDefault") == true) {
				m_defModule = child;
				return child;
			}
		}

		return nullptr;
	}

    std::string ConfigSection::getResolvedRequiredString(const std::string& key, unsigned int date /* = 0 */) const {
        std::string value = getRequired(key);
        return m_config->defs().resolve(value, date);
    }

    std::string ConfigSection::getResolvedString(const std::string& key, std::string defValue, unsigned int date /* = 0 */) const {
        std::string value = getString(key, defValue);
        return m_config->defs().resolve(value, date);
    }

	bool ConfigSection::getBool(const std::string& key, bool defValue /*= false*/, bool resolve /*= false*/) const {
		std::string value(resolve ? getResolvedString(key, "") : get(key));

		if (value.empty())
			return defValue;

		return value == "true" ? true : false;
	}

	float ConfigSection::getFloat(const std::string& key, float defValue /*= 0.0f*/, bool resolve /*= false*/) const {
		std::string value(resolve ? getResolvedString(key, "") : get(key));

		if (value.empty())
			return defValue;

		return Util::cast<float>(value);
	}

	int ConfigSection::getInt(const std::string& key, int defValue /*= 0*/, bool resolve /*= false*/) const {
		std::string value(resolve ? getResolvedString(key, "") : get(key));

		if (value.empty())
			return defValue;

		return Util::cast<int>(value);
	}

	ConfigSection& ConfigSection::set(const std::string& key, const std::string& value) {
		m_map[key] = value;
		return *this;
	}

	bool ConfigSection::get(const std::string& key, std::string& value) const {
		const StrMap::const_iterator& iter = m_map.find(key);
		value.clear();

		if (iter == m_map.end())
			return false;

		value = iter->second;
		return true;
	}

	bool ConfigSection::unset(const std::string& key) {
	    auto iter = m_map.find(key);

		if (iter != m_map.end()) {
			m_map.erase(iter);
			return true;
		}

		return false;
	}

	std::string ConfigSection::get(const std::string& key) const {
		std::string value;
		get(key, value);
		return value;
	}

	std::string ConfigSection::getRequired(const std::string& key) const {
		std::string value;
		bool doesExist = get(key, value);
		ASSERT(doesExist, "Required value missing. Expecting key: " << key << " - Config: " << this->toString(true));
		return value;
	}

	bool ConfigSection::getAmount(const std::string& key, float& amount) const {
		std::string value = get(key);
		if (value.empty())
			return false;
		amount = Util::amount(value);
		return true;
	}

	bool ConfigSection::getPercent(const std::string& key, float& percent) const {
		std::string value = get(key);
		if (value.empty())
			return false;
		percent = Util::percent(value);
		return true;
	}

	std::string ConfigSection::getString(const std::string& key, const std::string& defValue) const {
		std::string value = get(key);

		if (value.empty())
			return defValue;

		return value;
	}

	size_t ConfigSection::insertChildrenAt(size_t pos, const ConfigSection* sec) {
		std::vector<ConfigSection*> children(sec->m_children.size());

		for (size_t i = 0; i < sec->m_children.size(); i++)
			children[i] = new ConfigSection(m_config, *(sec->m_children[i]), true);

		/// We will own the pointer from here on ...
		m_children.insert(m_children.begin() + pos, children.begin(), children.end());

		return children.size();
	}


} /// namespace pesa 
