/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// ConfigSection.h
///
/// Created on: 1 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/FrameworkDefs.h"
#include "Core/Lib/Util.h"
#include "Core/IO/Stream.h"

#include <unordered_map>
#include <string>
#include <sstream>
#include <vector>

#ifdef __WINDOWS__
    #pragma warning(disable : 4251) /// class needs to have a dll-interface 
#endif 

namespace pesa {

#define Cfg_Property(type, name) 				type name() const { return get<type>(std::string(#name)); }
#define Cfg_OptProperty(type, name, defValue) 	type name() const { return get<type>(#name, defValue); }
#define Cfg_OptBoolProperty(name, defValue) 	bool name() const { return getBool(#name, defValue); }

	class Config;
	class ConfigSection;
	typedef std::vector<ConfigSection*>			ConfigSectionPVec;
	typedef std::vector<const ConfigSection*>	ConfigSectionCPVec;
	typedef std::unordered_map<std::string,		ConfigSectionPVec> ConfigSectionPVecMap;

	class Framework_API ConfigSection 
#ifndef __SCRIPT__
		: public io::Streamable 
#endif 
	{
	public:
		typedef std::map<std::string, std::string> StrMap;

	protected:

		std::string 				m_name; 				/// The name of the section
		std::string 				m_inner;				/// The inner string
		StrMap						m_map; 					/// The list of all key value pairs
		ConfigSectionPVec 			m_children; 			/// The child elements
		Config* 					m_config = nullptr;		/// Pointer to the parent config
		bool	 					m_isUnknown = false;	/// Is unknown
		int 						m_line = -1;			/// What line number was this section

		/// Dynamically constructed on the fly ...
		mutable
		ConfigSectionPVecMap		m_namedChildrenCache;	/// The named children cache (constructed from getAllChildrenOfType)
		mutable ConfigSection*		m_defModule = nullptr;

	public:
									ConfigSection(Config* config);
									ConfigSection(const ConfigSection& rhs);
									ConfigSection(Config* config, const ConfigSection& rhs, bool copyChildren = true);

		virtual 					~ConfigSection();

		ConfigSection& 				operator = (const ConfigSection& rhs);
		bool 						operator == (const ConfigSection& rhs);
		bool 						operator != (const ConfigSection& rhs);

		//////////////////////////////////////////////////////////////////////////
		/// Streamable implementation
		//////////////////////////////////////////////////////////////////////////
#ifndef __SCRIPT__
		virtual io::InputStream& 	read(io::InputStream& is);
		virtual io::OutputStream& 	write(io::OutputStream& os);
#endif 

		//////////////////////////////////////////////////////////////////////////
		virtual std::string 		toString(bool detailed = false) const;
		virtual void 				merge(const ConfigSection& derived, pesa::StringVec* exceptions = nullptr, bool ignoreChildSections = false, bool noCheckCompatibility = false);
		virtual void 				mergeProperties(const ConfigSection& derived, pesa::StringVec* exceptions = nullptr, bool ignoreChildSections = false);
		virtual void 				mergePropertiesRecursively(const ConfigSection& derived);
		virtual void 				makeParent(const ConfigSection& parent, bool copyChildren = false);
		virtual void				addChildSection(ConfigSection* child);
		void						clear();
		void						expandDerived();
		void						expandForEach();
		void						expandMacros();
		void						finalise(bool expandMacros = false);
		size_t						insertChildrenAt(size_t pos, const ConfigSection* sec);

		bool						deleteChildSection(const std::string& name);
		bool						deleteChildSection(ConfigSection* child);
		void						deleteAllChildren();
		void						deleteAllChildrenOfType(const std::string& type);
		const ConfigSection*		getParent(const std::string& parentId) const;
		ConfigSection* 				getChildSection(const std::string& name);
		const ConfigSection* 		getChildSection(const std::string& name) const;
		ConfigSection*				getChildSectionWithId(const std::string& id);
		const ConfigSection*		getChildSectionWithId(const std::string& id) const;
		const ConfigSection*		getChildSectionWithNameAndId(const std::string& name, const std::string& id) const;
		const ConfigSection*		getChildSectionWithNameAndProperty(const std::string& name, const std::string& propertyName, const std::string& propertyValue) const;
		const ConfigSection*		getChildSectionAtIndex(size_t index) const;
		ConfigSectionPVec			getAllChildrenOfType(const std::string& name) const;
		ConfigSectionPVec			getAllChildrenOfField(const std::string& fieldName, const std::string& fieldValue) const;
		std::string					getMacro(const std::string& macroName, bool noThrow = false) const;

		std::string 				getResolved(const std::string& key, unsigned int date = 0U) const;
		std::string					getYml(const std::string key = "yml") const;
		std::string					getKeyword(const std::string key = "keyword") const;
		std::string					getServerUrl(const std::string key = "serverUrl", unsigned int date = 0) const;
		std::string					getCurrency() const;

		void						resolveAllChildren(const ConfigSection& defs, unsigned int date);

		void						getChildStrategies(ConfigSectionPVec& childStrats, bool recursive = true) const;

        std::string                 getResolvedRequiredString(const std::string& key, unsigned int date = 0) const;
        std::string                 getResolvedString(const std::string& key, std::string defValue, unsigned int date = 0) const;
		bool 						getBool(const std::string& key, bool defValue = false, bool resolve = false) const;
		float 						getFloat(const std::string& key, float defValue = 0.0f, bool resolve = false) const;
		int 						getInt(const std::string& key, int defValue = 0, bool resolve = false) const;
		bool 						get(const std::string& key, std::string& value) const;
		std::string 				get(const std::string& key) const;
		std::string 				getRequired(const std::string& key) const;
		bool 						getAmount(const std::string& key, float& amount) const;
		bool 						getPercent(const std::string& key, float& percent) const;
		std::string 				getString(const std::string& key, const std::string& defValue) const;
		ConfigSection& 				set(const std::string& key, const std::string& value);
		bool						unset(const std::string& key);

		//////////////////////////////////////////////////////////////////////////
		/// Misc. Utilities and accessors
		//////////////////////////////////////////////////////////////////////////
		Cfg_Property(std::string, 	id);
		Cfg_Property(std::string, 	type);

		Cfg_Property(std::string, 	dataPath);
		Cfg_Property(std::string, 	dataCachePath);
		Cfg_Property(unsigned int, 	backdays);

		Cfg_Property(std::string, 	market);
		Cfg_Property(std::string, 	path);
		Cfg_Property(std::string, 	handler);
		Cfg_Property(std::string, 	universeId);

#ifndef __SCRIPT__
		Cfg_OptProperty(bool,		wrapper, false);
		Cfg_OptProperty(bool,		optimiseForUniverse, false);
		Cfg_OptProperty(std::string,moduleId, "");
		Cfg_OptBoolProperty(		debug, false);
		Cfg_OptProperty(int,		delay, 1);
		Cfg_OptProperty(float,		weight, 1.0f);
#endif 

		unsigned int				startDate() const;
		unsigned int				endDate() const;

		std::string 				resolve(const std::string& str, unsigned int date, bool onlyOnce = false) const;
#ifndef __SCRIPT__
		std::string 				resolve(const std::string& str, const Poco::DateTime* date, bool onlyOnce = false) const;
#endif 

		ConfigSectionPVec			modules() const;
		ConfigSectionPVec			operations() const;
		ConfigSectionPVec			alphas() const;

		ConfigSection*				operationsSec();
		ConfigSection*				alphasSec();
		ConfigSection*				combinationSec();

		ConfigSection* 				find(const std::string& moduleId) const;
		ConfigSection* 				findByName(const std::string& name) const;
		ConfigSection* 				findUnique(const std::string& key, const std::string& value) const;
		ConfigSection* 				getDefaultModule() const;
		float 						bookSize() const;
		void						remove(const std::string& key);

		//////////////////////////////////////////////////////////////////////////
		/// Inline Functions
		//////////////////////////////////////////////////////////////////////////

		inline const Config* 		config() const 			{ return m_config;	}
		inline std::string 			name() const			{ return m_name;	}
		inline std::string 			inner() const			{ return m_inner;	}

#ifndef __SCRIPT__
		inline bool& 				isUnknown() 			{ return m_isUnknown; }
		inline int& 				line() 					{ return m_line; 	}
		inline StrMap& 				map()					{ return m_map;		}
		inline const StrMap& 		map() const				{ return m_map;		}
		inline std::string& 		name()					{ return m_name;	}
#endif 
		inline StrMap				dict() const			{ return m_map; 	}
		inline bool 				isUnknown() const 		{ return m_isUnknown; }
		inline bool 				isNull() const 			{ return get("id").empty() ? true : false; }
		inline int  				line() const 			{ return m_line; 	}

		inline bool 				isUniverse() const		{ return type() == "universe"; }
		inline bool 				isSecMaster() const 	{ return type() == "securitymaster"; }

		inline size_t				numChildren() const		{ return m_children.size(); }
		inline ConfigSection*		child(size_t index)		{ 
			ASSERT(index < m_children.size(), "Invalid index: " << index << " - Num children: " << m_children.size()); 
			return m_children[index]; 
		}

		inline ConfigSectionPVec&	children() { return m_children; }
		inline const ConfigSectionPVec& children() const 	{ return m_children; }

		//////////////////////////////////////////////////////////////////////////

		/// Get a value that MUST exist ...
		template <typename t_type>
		t_type get(const std::string& key) const {
			std::string value = get(key);
			ASSERT(!value.empty(), "Expecting Key: " << key);
			t_type ret;
			std::istringstream ss(value);
			ss >> ret;

			return ret;
		}

        template <typename t_type>
		t_type get(const std::string& key, const t_type defValue) const {
			std::string value = get(key);

			if (value.empty())
				return defValue;

			t_type ret;
			std::istringstream ss(value);
			ss >> ret;

			return ret;
		}

		template <typename t_type>
		ConfigSection& set(const std::string& key, const t_type& value) {
			std::ostringstream ss;
			ss << value;
			return set(key, ss.str());
		}
	};

	typedef std::shared_ptr<ConfigSection>		ConfigSectionPtr;
	typedef std::vector<ConfigSectionPtr>		ConfigSectionPtrVec;
} /// namespace pesa 

