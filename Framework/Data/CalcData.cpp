/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// CalcData.cpp
///
/// Created on: 13 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "CalcData.h"
#include "Poco/Format.h"
#include "Core/Math/Stats.h"
#include "Framework/Pipeline/IDataRegistry.h"
#include "Framework/Components/IUniverse.h"
#include "Framework/Components/IOptimiser.h"
#include "Framework/Data/MatrixData.h"
#include "Poco/String.h"

#include "Framework/Helper/ShLib.h"

#include "Core/Lib/Profiler.h"

#define SH_MAT(lhs, rhs, size)		FloatMatrixData lhs(nullptr, 1, 1); \
									lhs.fromSharedMemory(0, (char*)(float*)&rhs[0], 1, size);


namespace pesa {
	static std::string format(const std::string& fmt, const Poco::Any& value1, const Poco::Any& value2,
		const Poco::Any& value3, const Poco::Any& value4, const Poco::Any& value5, const Poco::Any& value6,
		const Poco::Any& value7, const Poco::Any& value8, const Poco::Any& value9, const Poco::Any& value10,
		const Poco::Any& value11, const Poco::Any& value12) {
		std::vector<Poco::Any> args;
		std::string result;

		args.push_back(value1); args.push_back(value2); args.push_back(value3); args.push_back(value4); args.push_back(value5); args.push_back(value6);
		args.push_back(value7); args.push_back(value8); args.push_back(value9); args.push_back(value10); args.push_back(value11);
		args.push_back(value12);

		Poco::format(result, fmt, args);

		return result;
	}

	struct AlphaCache {
		typedef std::unordered_map<std::string, FloatMatrixDataPtrVec> CacheMap;

		static CacheMap			cache;

		inline static
		std::string				key(size_t rows, size_t cols) {
			std::ostringstream ss;
			ss << rows << "x" << cols;
			return ss.str();
		}
		static FloatMatrixDataPtr alloc(size_t rows, size_t cols) {
			auto k = key(rows, cols);
			auto iter = cache.find(k);

			if (iter == cache.end()) {
				auto mat = std::make_shared<FloatMatrixData>(nullptr, rows, cols);
				return mat;
			}

			auto matVec = iter->second;
			auto mat = matVec[matVec.size() - 1];
			matVec.pop_back();
			cache.erase(iter);
			return mat;
		}

		static void				clearBelow(size_t cols) {
			auto iter = cache.begin();

			while (iter != cache.end()) {
				auto matVec = iter->second;
				if (!matVec.size() || matVec[0]->cols() < cols) {
					iter = cache.erase(iter);
				}
				else
					iter++;
			}
		}

		static void				release(FloatMatrixDataPtr mat) {
			if (!mat)
				return;

			size_t rows = mat->rows();
			size_t cols = mat->cols();
			std::string k = key(rows, cols);
			cache[k].push_back(mat);
		}
	};

	AlphaCache::CacheMap AlphaCache::cache;

	////////////////////////////////////////////////////////////////////////////
	/// CalcData
	////////////////////////////////////////////////////////////////////////////
	CalcData::CalcData() {
	}

	CalcData::CalcData(const CalcData& rhs) {
		*this = rhs;
	}

	CalcData::~CalcData() {
		alpha = nullptr;
		yesterday = nullptr;
	}

	void CalcData::ensureAlphaSize(size_t rows, size_t cols) {
		PROFILE_SCOPED();

		if (!alpha) {
			alpha = std::make_shared<FloatMatrixData>(nullptr, rows, cols);
			alpha->zeroMemory();
		}
		else
			alpha->ensureSize(rows, cols);

		remappedAlpha = alpha;
		remappedUniverse = universe;
	}

	void CalcData::resetAlpha() {
		size_t rows			= alpha->rows();
		size_t cols			= alpha->cols();

		/// We wanna create a new one ...
		alpha				= nullptr;
		stats				= Statistics(); /// reset the stats too 

		ensureAlphaSize(rows, cols);
	}

	void CalcData::deepCopy(const CalcData& rhs) {
		/// We do a normal assignment ...
		*this = rhs;

		/// But then make a copy of the alpha
		alpha = nullptr;
		ensureAlphaSize(rhs.alpha->rows(), rhs.alpha->cols());
		alpha->deepCopy(*rhs.alpha.get());
	}

	CalcData& CalcData::operator=(const CalcData& rhs) {
		PROFILE_SCOPED();

		alpha			= rhs.alpha;
		stats			= rhs.stats;
		cumStats		= rhs.cumStats;
		universe		= rhs.universe;
		delay			= rhs.delay;
		di				= rhs.di;
		config			= rhs.config;

		remappedAlpha	= rhs.remappedAlpha;
		remappedUniverse= rhs.remappedUniverse;
		
		/// This needs to be copied explicitly ...
		yesterday		= nullptr;

		rawAlpha		= rhs.rawAlpha;
		alphaNotionals	= rhs.alphaNotionals;
		alphaPnls		= rhs.alphaPnls;
		cumAlphaPnls	= rhs.cumAlphaPnls;
		alphaCosts		= rhs.alphaCosts;
		alphaShares		= rhs.alphaShares;
		alphaReturns	= rhs.alphaReturns;
		alphaTurnover	= rhs.alphaTurnover;

		alphaPrice		= rhs.alphaPrice;
		alphaPrevPrice	= rhs.alphaPrevPrice;

		sortedAlpha		= rhs.sortedAlpha;
		rankedAlpha		= rhs.rankedAlpha;
		alphaSharesCalcPrice = rhs.alphaSharesCalcPrice;

		customData		= rhs.customData;

		alphaOpenPnls	= rhs.alphaOpenPnls;
		alphaHighPnls	= rhs.alphaHighPnls;
		alphaLowPnls	= rhs.alphaLowPnls;
		alphaIntradayPnls = rhs.alphaIntradayPnls;

		return *this;
	}

	void CalcData::finaliseForPNLCalc(const RuntimeData& rt, IntDay di, IntDay delay, float bookSize, FloatMatrixData& alpha, IUniverse* universe, 
									  FloatVec& alphaNotionals_, FloatVec& alphaShares_, FloatVec& alphaSharesCalcPrice_, std::string priceDataId, 
									  std::string currDayPriceDataId) {
		/// if there is no alpha ... then there is no point
		if (!alpha.cols())
			return;

		auto& dr			= *(const_cast<IDataRegistry*>(rt.dataRegistry));
		size_t size			= alpha.cols();

		//float* alphaData	= alpha.fdata();
		auto* closePrice 	= dr.get<FloatMatrixData>(universe, priceDataId);
		//float alphaRange	= Stats::absSum(alphaData, size);

		float alphaRange	= (alpha[0]).nanClear().abs().sum();
		float invAlphaRange = 1.0f / alphaRange;
		alpha[0]			= alpha[0] * invAlphaRange;

		ASSERT(closePrice, "Unable to load price data: " << priceDataId);

		//float longSum = Stats::addPositive(alphaData, size);
		//float shortSum = Stats::addNegative(alphaData, size);

		//if (alphaRange != 0.0f)
		//	Stats::invScale(alphaData, size, alphaRange);

		//float sum = 0.0f;
		//for (auto i = 0; i < size; i++) { 
		//	if (!std::isnan(alphaData[i]))
		//		sum += std::abs(alphaData[i]);
		//}

		ASSERT(alphaShares_.size() == size, "Size mismatch between alphaShares and alpha (This should have been pre-allocated!). Expecting: " << alphaShares_.size() << " - Found: " << size);
		ASSERT(alphaNotionals_.size() == size, "Size mismatch between alphaNotionals and alpha (This should have been pre-allocated!). Expecting: " << alphaNotionals_.size() << " - Found: " << size);

		IntDay diD			= di - 1;

		bool useLivePrice	= di == rt.diEnd && !rt.isTodayHoliday;

		/// By default we are going to use the last day's closing price as the price for 
		/// calculating the PNL. However, if we have a live price and this is the last 
		/// day of the simulation (AND NOT a holiday - in which case we can still use the 
		/// data from yesterday), THEN we use the current price. 
		/// We set the diOffset = di since LIVE prices are only for a single day and they
		/// can ONLY handle di = 0. We don't want to have lots of if conditions in this
		/// code so we do this
		if (useLivePrice) {
			DataInfo dinfo;
			dinfo.noAssertOnMissingDataLoader = true;
			/// Try loading the live price 
			auto livePricePtr = !currDayPriceDataId.empty() ? dr.getDataPtr(universe, currDayPriceDataId, &dinfo) : nullptr;

			if (livePricePtr) {
				auto* livePrice = dynamic_cast<const FloatMatrixData*>(livePricePtr.get());
				closePrice = livePrice;
				if (livePrice->rows() == 1)
					diD = 0;
				else
					diD = di - livePrice->delay();
			}
		}

		SH_MAT(alphaNotionals, alphaNotionals_, size);
		SH_MAT(alphaShares, alphaShares_, size);
		SH_MAT(alphaSharesCalcPrice, alphaSharesCalcPrice_, size);

		memcpy(alphaSharesCalcPrice.fdata(0), closePrice->rowPtr(diD), sizeof(float) * size);

		//alphaSharesCalcPrice[0]	= (*closePrice)[diD];
		alphaNotionals[0]		= (alpha[0] * bookSize).nanClear(); /// * universe->dailyValidMap()->row(di);
		alphaShares[0]			= (alphaNotionals[0] / alphaSharesCalcPrice[0]);

		//FloatVec alphaVec	= alpha.rowArray(0);
		//float bookLong = 0.0f;
		//float bookShort = 0.0f;
		//ASSERT(alphaVec.size() == size, "Size mismatch between alpha matrix and alpha vector. Expecting: " << alphaVec.size() << " - Found: " << size);

		//for (size_t ii = 0; ii < size; ii++) {
		//	float returns	= 0.0f;
		//	float cp0		= (*closePrice)(diD, ii);
		//	float diAlpha	= alphaVec[ii];

		//	ASSERT(universe->isValid(di, ii) || std::isnan(diAlpha), "Security not valid in the universe at: " << di << ", " << ii << ", yet the alpha value is NOT NaN. This must be handled by prior stages");

		//	alphaSharesCalcPrice[ii] = cp0;

		//	if (std::isnan(diAlpha)) {
		//		alphaNotionals[ii] = diAlpha;
		//		alphaShares[ii] = std::nanf("");
		//		continue;
		//	}

		//	float alphaValue= 0.0f;
		//	float alphaShare= 0.0f;

		//	if (universe->isValid(di, ii) && !std::isnan(cp0) && cp0 != 0.0f) {
		//		alphaValue	= diAlpha * bookSize;
		//		alphaShare	= alphaValue / cp0;
		//	}
		//	else 
		//		alphaValue	= std::nanf("");

		//	ASSERT(!std::isnan(alphaShare), "Cannot have NaN in alphaShare. This must be checked properly!");

		//	alphaNotionals[ii]	= !std::isnan(alphaValue) ? alphaValue : 0.0f;

		//	if (alphaNotionals[ii] > 0.0f)
		//		bookLong += alphaNotionals[ii];
		//	else
		//		bookShort += alphaNotionals[ii];

		//	alphaShares[ii]	= alphaShare;
		//}
	}

#define PESA_INIT_FLOAT_VEC(V, SZ)	V.resize(SZ); memset(&V[0], 0, sizeof(float) * V.size());

	void CalcData::finaliseForPNLCalc(const pesa::RuntimeData& rt, float bookSize, std::string priceDataId, std::string currDayPriceDataId) {
		PROFILE_SCOPED();

		size_t rows = alpha->rows();
		size_t cols = alpha->cols();

		this->di = rt.di;

		/// There is no alpha
		if (!rows || !cols) 
			return;

		PESA_INIT_FLOAT_VEC(alphaNotionals, cols);
		PESA_INIT_FLOAT_VEC(alphaShares, cols);
		PESA_INIT_FLOAT_VEC(alphaCosts, cols);
		PESA_INIT_FLOAT_VEC(alphaTurnover, cols);
		PESA_INIT_FLOAT_VEC(alphaSharesCalcPrice, cols);

		rawAlpha = alpha->rowArray(0);

		CalcData::finaliseForPNLCalc(rt, rt.di, delay, bookSize, *alpha, universe, alphaNotionals, alphaShares, alphaSharesCalcPrice, priceDataId, currDayPriceDataId);

		/// Calculate turnover
		alphaTurnover.resize(cols);
		std::fill_n(&alphaTurnover[0], cols, 100.0f);

		if (yesterday) {
			memset(&alphaTurnover[0], 0, sizeof(float) * alphaTurnover.size());

			SH_MAT(alphaNotionals, this->alphaNotionals, yesterday->alphaNotionals.size());
			SH_MAT(alphaPrevNotionals, yesterday->alphaNotionals, yesterday->alphaNotionals.size());
			SH_MAT(alphaTurnover, this->alphaTurnover, yesterday->alphaNotionals.size());

			float invBookSize = 1.0f / bookSize;
			alphaTurnover[0] = ((alphaNotionals[0] - alphaPrevNotionals[0]) * invBookSize).nanClear().infClear();
		}
		else {
			std::fill_n(&alphaTurnover[0], cols, 100.0f);
		}
	}

	void CalcData::calculatePNL(const RuntimeData& rt, IntDay di, IntDay delay, float bookSize, FloatMatrixData& alpha,
								IUniverse* universe, FloatVec& alphaNotionals_, FloatVec& alphaPnls_, FloatVec& alphaOpenPnls_,
								FloatVec& alphaHighPnls_,FloatVec& alphaLowPnls_, std::vector<FloatVec>& alphaIntradayPnls_, FloatVec& cumAlphaPnls_,
								FloatVec& alphaShares_, FloatVec& alphaReturns_, FloatVec& alphaPrice_, FloatVec& alphaPrevPrice_, 
								std::string priceDataId, std::string openPriceDataId, std::string highPriceDataId, std::string lowPriceDataId,
								StringVec* intradayPriceDataId, std::string currDayPriceDataId) {
		size_t size			= alphaNotionals_.size();
		/// if there is no alpha ... then there is no point
		if (!size)
			return;

		auto& dr			= *(const_cast<IDataRegistry*>(rt.dataRegistry));
		auto* closePrice 	= dr.get<FloatMatrixData>(universe, priceDataId);
		auto* currPrice		= closePrice;
		bool useLivePrice	= di == rt.diEnd && !rt.isTodayHoliday;
		IntDay diOffset		= 0;
		FloatMatrixDataPtr	alphaOpenPnls = nullptr;
		FloatMatrixDataPtr	alphaHighPnls = nullptr;
		FloatMatrixDataPtr	alphaLowPnls = nullptr;
		FloatMatrixDataPtrVec alphaIntradayPnls;
		const FloatMatrixData* alphaOpenPrice = nullptr;
		const FloatMatrixData* alphaHighPrice = nullptr;
		const FloatMatrixData* alphaLowPrice = nullptr;
		std::vector<const FloatMatrixData*> alphaIntradayPrice;

		//std::ostringstream ss;

		PESA_INIT_FLOAT_VEC(alphaPnls_, size);
		PESA_INIT_FLOAT_VEC(alphaReturns_, size);
		PESA_INIT_FLOAT_VEC(alphaPrice_, size);
		PESA_INIT_FLOAT_VEC(alphaPrevPrice_, size);

		if (!openPriceDataId.empty()) {
			PESA_INIT_FLOAT_VEC(alphaOpenPnls_, size);
			alphaOpenPnls = std::make_shared<FloatMatrixData>(nullptr, 1, 1);
			alphaOpenPnls->fromSharedMemory(0, (char*)(float*)&alphaOpenPnls_[0], 1, size);
			alphaOpenPrice = dr.get<FloatMatrixData>(universe, openPriceDataId);
		}

		if (!highPriceDataId.empty()) {
			PESA_INIT_FLOAT_VEC(alphaHighPnls_, size);
			alphaHighPnls = std::make_shared<FloatMatrixData>(nullptr, 1, 1);
			alphaHighPnls->fromSharedMemory(0, (char*)(float*)&alphaHighPnls_[0], 1, size);
			alphaHighPrice = dr.get<FloatMatrixData>(universe, highPriceDataId);
		}

		if (!lowPriceDataId.empty()) {
			PESA_INIT_FLOAT_VEC(alphaLowPnls_, size);
			alphaLowPnls = std::make_shared<FloatMatrixData>(nullptr, 1, 1);
			alphaLowPnls->fromSharedMemory(0, (char*)(float*)&alphaLowPnls_[0], 1, size);
			alphaLowPrice = dr.get<FloatMatrixData>(universe, lowPriceDataId);
		}

		//if (intradayPriceDataId && intradayPriceDataId->size()) {
		//	alphaIntradayPnls_.resize(intradayPriceDataId->size());
		//	alphaIntradayPnls.resize(intradayPriceDataId->size());
		//	alphaIntradayPrice.resize(intradayPriceDataId->size());

		//	for (size_t i = 0; i < intradayPriceDataId->size(); i++) {
		//		PESA_INIT_FLOAT_VEC(alphaIntradayPnls_[i], size);
		//		alphaIntradayPnls[i] = std::make_shared<FloatMatrixData>(nullptr, 1, 1);
		//		alphaIntradayPnls[i]->fromSharedMemory(0, (char*)(float*)&alphaIntradayPnls_[i][0], 1, size);
		//		alphaIntradayPrice[i] = dr.get<FloatMatrixData>(universe, (*intradayPriceDataId)[i]);
		//	}
		//}

		SH_MAT(alphaNotionals, alphaNotionals_, size);
		SH_MAT(alphaPnls, alphaPnls_, size);
		SH_MAT(cumAlphaPnls, cumAlphaPnls_, size);
		SH_MAT(alphaShares, alphaShares_, size);
		SH_MAT(alphaReturns, alphaReturns_, size);
		SH_MAT(alphaPrice, alphaPrice_, size);
		SH_MAT(alphaPrevPrice, alphaPrevPrice_, size);

		// ASSERT(alphaNotionals.size() && alphaNotionals.size() == alpha.cols(), "Cannot calculate PNL. Data must be finalised first! Expecting array of size: " << alphaNotionals.size() << " - Found: " << alpha.cols());
		// ASSERT(alphaNotionals.size() == alphaShares.size(), "Shares vector mismatch with the values vector. Values: " << alphaNotionals.size() << " - Shares: " << alphaShares.size());

		/// By default we are going to use the last day's closing price as the price for 
		/// calculating the PNL. However, if we have a live price and this is the last 
		/// day of the simulation (AND NOT a holiday - in which case we can still use the 
		/// data from yesterday), THEN we use the current price. 
		/// We set the diOffset = di since LIVE prices are only for a single day and they
		/// can ONLY handle di = 0. We don't want to have lots of if conditions in this
		/// code so we do this
		if (useLivePrice) {
			DataInfo dinfo;
			dinfo.noAssertOnMissingDataLoader = true;
			/// Try loading the live price 
			auto livePricePtr	= !currDayPriceDataId.empty() ? dr.getDataPtr(universe, currDayPriceDataId, &dinfo) : nullptr;

			if (!livePricePtr)
				return;

			auto* livePrice = dynamic_cast<const FloatMatrixData*>(livePricePtr.get());

			currPrice			= livePrice;

			if (livePrice->rows() == 1)
				diOffset		= di;
			else 
				diOffset		= livePrice->delay();
		}

		ASSERT(closePrice, "Unable to load price data: " << priceDataId);

		IntDay diD				= di - 1;
		//size_t numRows			= closePrice->rows();

		memcpy(alphaPrevPrice.fdata(0), closePrice->rowPtr(diD), sizeof(float) * size);
		memcpy(alphaPrice.fdata(0), currPrice->rowPtr(di - diOffset), sizeof(float) * size);

		//alphaPrevPrice[0]		= (*closePrice)[diD];
		//alphaPrice[0]			= (*currPrice)[di - diOffset];

		/// TODO: Fix this later. Problems happening on Windows with vector optimisations disabled!
		alphaReturns[0]			= (alphaPrice[0].nanClear().infClear() / alphaPrevPrice[0].nanClear().infClear()).nanClear().infClear() - 1.0f;
		alphaPnls[0]			= alphaNotionals[0] * alphaReturns[0];

		if (alphaOpenPnls && alphaOpenPrice && !useLivePrice)
			alphaOpenPnls->row(0) = (alphaNotionals[0] * ((alphaOpenPrice->row(di) / closePrice->row(diD)) - 1.0f)).nanClear();

		if (alphaHighPnls && alphaHighPrice && !useLivePrice)
			alphaHighPnls->row(0) = (alphaNotionals[0] * ((alphaHighPrice->row(di) / closePrice->row(diD)) - 1.0f)).nanClear();

		if (alphaLowPnls && alphaHighPrice && !useLivePrice)
			alphaLowPnls->row(0) = (alphaNotionals[0] * ((alphaLowPrice->row(di) / closePrice->row(diD)) - 1.0f)).nanClear();

		//if (alphaIntradayPnls.size()) {
		//	for (size_t i = 0; i < alphaIntradayPnls.size(); i++) {
		//		alphaIntradayPnls[i]->row(0) = (alphaNotionals[0] * ((alphaIntradayPrice[i]->row(di) / closePrice->row(diD)) - 1.0f)).nanClear();
		//	}
		//}

		alphaPnls[0]			= alphaPnls[0].nanClear();
		cumAlphaPnls[0]			+= alphaPnls[0];

		//for (size_t ii = 0; ii < size; ii++) {
		//	/// If there is no data available on these dates then don't calculate anything ...
		//	//if (numRows <= diD || numRows <= di) {
		//	//	alphaPnls[ii]	= 0.0f;
		//	//	cumAlphaPnls[ii]= 0.0f;
		//	//	alphaReturns[ii]= 0.0f;
		//	//	continue;
		//	//}

		//	float cp0			= (*closePrice)(diD, ii);
		//	float cp1			= (*currPrice)(di - diOffset, ii);

		//	alphaPrevPrice[ii]	= cp0;
		//	alphaPrice[ii]		= cp1;

		//	Security sec0		= universe->security(diD, (Security::Index)ii);
		//	Security sec1		= universe->security(di, (Security::Index)ii);

		//	ASSERT(sec0 == sec1, "Security mismatch between consecutive days! Sec0: " << sec0.debugString() << " - Sec1: " << sec1.debugString());

		//	/// If the previous price is also not there then this is not supposted to be part of the universe
		//	if (invalid::check(cp0) || cp0 == 0.0f)
		//		continue;

		//	/// if we do not have the current close price then zero it out ...
		//	if (invalid::check(cp1))
		//		cp1				= cp0;

		//	float returns		= (cp1 / cp0) - 1.0f;

		//	//ss << cp1 << " (" << alpha[ii] << ", " << alphaNotionals[ii] << ", " << returns << "), ";
		//	float alphaValue	= alphaNotionals[ii];
		//	float alphaPnl		= 0.0f;
		//	float cumAlphaPnl	= cumAlphaPnls[ii];

		//	if (!std::isnan(alphaValue)) {
		//		alphaPnl		= returns * alphaValue;
		//		cumAlphaPnl		+= alphaPnl;
		//	}
		//	else {
		//		alphaValue		= 0.0f;
		//		alphaPnl		= std::nanf("");
		//		returns			= std::nanf("");
		//	}

		//	/// Can only have this if the alphaValue is NaN. If returns or something else is wrong then we have a problem!
		//	//ASSERT(!std::isnan(alphaPnl), "Cannot have NaN in alphaPnl. This must be checked properly!");

		//	/// These should have been allocated to the correct size when the 
		//	/// finaliseForPNLCalc funtion was called and it MUST be called!
		//	alphaPnls[ii]		= alphaPnl;
		//	cumAlphaPnls[ii]	= cumAlphaPnl;
		//	alphaReturns[ii]	= returns;
		//}
	}

	void CalcData::calculatePNL(const pesa::RuntimeData& rt, float bookSize, std::string priceDataId, std::string highPriceDataId, 
		std::string openPriceDataId, std::string lowPriceDataId, StringVec* intradayPriceDataId, std::string currDayPriceDataId) {
		PROFILE_SCOPED();

		/// Nothing to calculate over here ...
		if (!yesterday->alphaNotionals.size() || !this->alphaNotionals.size())
			return;

		ASSERT(yesterday, "Cannot calculate PNL without data from yesterday!");

		if (!yesterday) {
			this->finaliseForPNLCalc(rt, bookSize, priceDataId, currDayPriceDataId);
			return;
		}

		/// Make sure that today's alpha data is at least the same size as yesterday (padded with 0s)
		if (yesterday && yesterday->alphaNotionals.size() > alphaNotionals.size()) {
			//for (size_t i = alphaNotionals.size(); i < yesterday->alphaNotionals.size(); i++) {
			//	alphaNotionals.push_back(std::nanf(""));
			//	alphaShares.push_back(std::nanf(""));
			//	alphaCosts.push_back(std::nanf(""));
			//}
			size_t desiredSize = alphaNotionals.size(); /// yesterday->alphaNotionals.size();
			alphaNotionals.resize(desiredSize);
			alphaShares.resize(desiredSize);
			alphaCosts.resize(desiredSize);

			float nanVal = std::nanf("");
			std::fill_n(&alphaNotionals[0], desiredSize, nanVal);
			std::fill_n(&alphaShares[0], desiredSize, nanVal);
			std::fill_n(&alphaCosts[0], desiredSize, 0.0f);

		}

		PESA_INIT_FLOAT_VEC(cumAlphaPnls, yesterday->alphaNotionals.size());

		/// Yesterday is always guaranteed to be <= to today
		if (yesterday->cumAlphaPnls.size())
			memcpy(&cumAlphaPnls[0], &yesterday->cumAlphaPnls[0], sizeof(float) * yesterday->cumAlphaPnls.size());

		CalcData::calculatePNL(rt, rt.di, delay, bookSize, *(yesterday->alpha), yesterday->universe, yesterday->alphaNotionals,
			this->alphaPnls, this->alphaHighPnls, this->alphaOpenPnls, this->alphaLowPnls, this->alphaIntradayPnls, this->cumAlphaPnls, yesterday->alphaShares, 
			this->alphaReturns, this->alphaPrice, this->alphaPrevPrice, priceDataId, openPriceDataId, highPriceDataId, lowPriceDataId, intradayPriceDataId, 
			currDayPriceDataId);
	}

	struct CustomCmp {
		const FloatVec& alphaNotionals;

		CustomCmp(const FloatVec& alphaNotionals_) : alphaNotionals(alphaNotionals_) {}

		bool operator() (size_t lhs, size_t rhs) {
			float lhsValue = alphaNotionals[lhs];
			float rhsValue = alphaNotionals[rhs];
			return lhsValue > rhsValue;
		}
	};

	void CalcData::calculateStats(const RuntimeData& rt, float bookSize) {
		PROFILE_SCOPED();

		Statistics newStats;

		newStats.date			= rt.dates[rt.di];
		newStats.bookSize		= bookSize;
		newStats.universeSize	= yesterday ? yesterday->universe->size(rt.di) : universe->size(rt.di);

		/// If there is no data for yesterday then no stats can be calculated ...
		if (!yesterday) {
			stats = newStats;
			newStats.finalise();
			return;
		}

		/// Since yesterday's PNL is realised today therefore we use yesterday's
		/// alpha values in today's PNL stats
		size_t size 			= yesterday->alpha->cols();

		size_t todaySize		= this->alpha->cols();
		FloatMatrixData			yesterdayLongMask(nullptr, 1, size);
		FloatMatrixData			yesterdayShortMask(nullptr, 1, size);
		FloatMatrixData			longMask(nullptr, 1, todaySize);
		FloatMatrixData			shortMask(nullptr, 1, todaySize);
		FloatMatrixData			liquidateMask(nullptr, 1, size);
		FloatMatrixData			zero(nullptr, DataDefinition(), 1, size);
		FloatMatrixData			one(nullptr, DataDefinition(), 1, size);
		FloatMatrixData			tzero(nullptr, DataDefinition(), 1, todaySize);
		FloatMatrixData			tone(nullptr, DataDefinition(), 1, todaySize);

		zero.setMemory(0.0f);
		one.setMemory(1.0f);

		tzero.setMemory(0.0f);
		tone.setMemory(1.0f);

		SH_MAT(yesterdayAlphaNotionals, yesterday->alphaNotionals, size);
		SH_MAT(yesterdayAlphaShares, yesterday->alphaShares, size);
		SH_MAT(alphaCosts, this->alphaCosts, size);
		SH_MAT(alphaPnls, this->alphaPnls, size);
		SH_MAT(alphaReturns, this->alphaReturns, size);
		SH_MAT(alphaNotionals, this->alphaNotionals, todaySize);
		SH_MAT(alphaShares, this->alphaShares, todaySize);
		SH_MAT(alphaNotionals_wrt_Yesterday, this->alphaNotionals, size);

		yesterdayLongMask[0]	= (yesterdayAlphaNotionals[0] > zero[0]).select(one.innerMat(), 0.0f).nanClear();
		yesterdayShortMask[0]	= (yesterdayAlphaNotionals[0] < zero[0]).select(one.innerMat(), 0.0f).nanClear();
		longMask[0]				= (alphaNotionals[0] > tzero[0]).select(tone.innerMat(), 0.0f).nanClear();
		shortMask[0]			= (alphaNotionals[0] < tzero[0]).select(tone.innerMat(), 0.0f).nanClear();

		auto ynorm				= (yesterdayAlphaNotionals[0] != zero[0]).select(one.innerMat(), 0.0f);
		auto tnorm				= (alphaNotionals_wrt_Yesterday[0] != zero[0]).select(one.innerMat(), 0.0f);

		liquidateMask[0]		= ((ynorm - tnorm) > zero[0]).select(one.innerMat(), 0.0f);

		newStats.longPnl		= (alphaPnls[0] * yesterdayLongMask[0]).sum();
		//newStats.longReturns	= (alphaReturns[0].nanClear() * yesterdayLongMask[0]).sum();

		newStats.longNotional	= (alphaNotionals[0] * longMask[0]).sum();
		//newStats.longShares 	= (alphaShares[0].nanClear() * longMask[0]).sum();
		newStats.longCount		= (longMask[0].sum());

		newStats.shortPnl		= (alphaPnls[0] * yesterdayShortMask[0]).sum();

		//newStats.shortReturns	= (alphaReturns[0].nanClear() * yesterdayShortMask[0]).sum();

		newStats.shortNotional		= (alphaNotionals[0] * shortMask[0]).sum();
		//newStats.shortShares 	= (alphaShares[0].nanClear() * shortMask[0]).sum();
		newStats.shortCount 	= (shortMask[0].sum());

		newStats.pnl			= newStats.longPnl + newStats.shortPnl;
		newStats.cost			= ((alphaCosts[0] * yesterdayShortMask[0]) + (alphaCosts[0] * yesterdayLongMask[0])).sum();
		newStats.notional 		= yesterdayAlphaNotionals[0].abs().sum();

		newStats.liquidateCount	= (liquidateMask[0].sum());
		newStats.liquidateNotional	= (yesterdayAlphaNotionals[0] * liquidateMask[0]).abs().sum();
		//newStats.numLiquidate	= (yesterdayAlphaShares[0].nanClear() * liquidateMask[0]).abs().sum();

		auto positivePnls		= alphaPnls[0].filterPositive(zero[0]);
		auto positivePnlNotionals = positivePnls.nonZeroMask(zero[0], one[0]) * yesterdayAlphaNotionals[0];

		newStats.pnlWon			= positivePnls.sum();
		newStats.winNotional	= positivePnlNotionals.abs().sum();

		/// We'll use insertion sort to sort the alpha vector 
		//for (size_t ii = 0; ii < size; ii++) {
		//	float alphaValue	= yesterday->alphaNotionals[ii];
		//	float alphaShare	= yesterday->alphaShares[ii];
		//	float alphaCost		= alphaCosts[ii]; /// We use the cost for today!
		//	float alphaPnl		= alphaPnls[ii];
		//	float alphaReturn	= alphaReturns[ii];

		//	if (alphaValue > 0.0f) {
		//		newStats.longPnl += alphaPnl;
		//		newStats.longReturns += alphaReturn;
		//	}
		//	else if (alphaValue <= 0.0f) {
		//		newStats.shortPnl += alphaPnl;
		//		newStats.shortReturns += alphaReturn;
		//	}
		//	else {/// Used to handle cases of NaNs
		//		alphaValue		= 0.0f;
		//		alphaPnl		= std::isnan(alphaPnl) ? 0.0f : alphaPnl; 
		//		alphaShare		= 0.0f;
		//	}

		//	newStats.pnl		+= alphaPnl;
		//	newStats.cost		+= alphaCost;
		//	newStats.totalValue += std::abs(alphaValue);

		//	/// Long side
		//	if (alphaValue > 0.0f) {
		//		newStats.longValue	+= alphaValue;
		//		newStats.longShares	+= alphaShare;
		//		newStats.longCount++;
		//	}
		//	/// Short side
		//	else if (alphaValue < 0.0f) {
		//		newStats.shortValue += alphaValue;
		//		newStats.shortShares+= alphaShare;
		//		newStats.shortCount++;
		//	}

		//	if (yesterday && (alphaNotionals[ii] == 0.0f || std::isnan(alphaNotionals[ii])) && ii < yesterday->alphaNotionals.size() &&
		//		(yesterday->alphaNotionals[ii] != 0.0f && !std::isnan(yesterday->alphaNotionals[ii]))) {
		//		/// So we liquidate this between yesterday and today
		//		newStats.liquidateValue += std::abs(yesterday->alphaNotionals[ii]);
		//		newStats.numLiquidate += std::abs(yesterday->alphaShares[ii]);
		//		newStats.liquidateCount++;
		//	} 
		//}

		newStats.notional = newStats.longNotional + std::abs(newStats.shortNotional);

		newStats.finaliseRelativeToYesterday(this->alphaNotionals, yesterday->alphaNotionals, this->alphaReturns, this->alphaPrevPrice);

		/// Copy them over
		stats = newStats;
	}

	void CalcData::calculateValueFromShares(const RuntimeData& rt, std::string priceDataId, std::string currDayPriceDataId, bool recalcShares) {
		PROFILE_SCOPED();

		auto* price 			= rt.dataRegistry->get<FloatMatrixData>(universe, priceDataId);

		ASSERT(price, "Unable to load data: " << priceDataId);
		ASSERT(this->alphaNotionals.size() == this->alphaShares.size(), "Alpha has not been finalised! Comparing values against shares. Expecting size: " << alphaNotionals.size() << " - Found: " << alphaShares.size());
		ASSERT(alpha->cols() == alphaShares.size(), "Alpha has not been finalised properly! Comparing alpha against shares. Expecting size: " << alpha->cols() << " - Found: " << alphaShares.size());

		IntDay diD				= rt.di - 1;
		size_t size 			= alpha->cols();

		bool useLivePrice = di == rt.diEnd && !rt.isTodayHoliday;

		/// By default we are going to use the last day's closing price as the price for 
		/// calculating the PNL. However, if we have a live price and this is the last 
		/// day of the simulation (AND NOT a holiday - in which case we can still use the 
		/// data from yesterday), THEN we use the current price. 
		/// We set the diOffset = di since LIVE prices are only for a single day and they
		/// can ONLY handle di = 0. We don't want to have lots of if conditions in this
		/// code so we do this
		if (useLivePrice) {
			DataInfo dinfo;
			dinfo.noAssertOnMissingDataLoader = true;
			/// Try loading the live price 
			auto livePricePtr = !currDayPriceDataId.empty() ? rt.dataRegistry->getDataPtr(universe, currDayPriceDataId, &dinfo) : nullptr;

			if (livePricePtr) {
				auto* livePrice = dynamic_cast<const FloatMatrixData*>(livePricePtr.get());
				price = livePrice;
				if (livePrice->rows() == 1)
					diD = 0;
				else
					diD = rt.di - livePrice->delay();
			}
		}

		SH_MAT(alphaNotionals, this->alphaNotionals, size);
		SH_MAT(alphaShares, this->alphaShares, size);

		/// TODO: 
		//alphaShares[0]			= ((alphaNotionals[0] / (*price)[diD]).floor()) * (*price)[diD].nanMask();
		alphaShares[0]			= ((alphaNotionals[0] / (*price)[diD]).floor()) * (*price)[diD].nanMask();
		alphaNotionals[0]		= (alphaShares[0] * (*price)[diD]).nanClear();

		//float bookSizeUsed		= 0.0f;
		//for (size_t ii = 0; ii < size; ii++) {
		//	float alphaShare	= alphaShares[ii];
		//	float cp0			= (*price)(diD, ii);

		//	if (invalid::check(cp0) || invalid::check(alphaNotionals[ii])) {
		//		alphaShares[ii] = std::nanf("");
		//		alphaNotionals[ii] = 0.0f;
		//		continue;
		//	}

		//	if (recalcShares == true) {
		//		alphaShare		= alphaNotionals[ii] / cp0;
		//	}

		//	int numShares		= (int)alphaShare;

		//	/// nothing exists over here
		//	if (std::abs(alphaShare) < 0.00001f) {/// Too small ... EPSILON
		//		alphaShares[ii] = 0;
		//		alphaNotionals[ii] = 0.0f;
		//		continue;
		//	}

		//	float alphaValue	= numShares * cp0;
		//	alphaShares[ii]		= (float)numShares;
		//	alphaNotionals[ii]		= !std::isnan(alphaValue) ? alphaValue : 0.0f;

		//	bookSizeUsed		+= std::abs(alphaValue);
		//}
	}

	void CalcData::factorGenericCost(const RuntimeData& rt, std::string priceDataId, const std::string dataId, bool shortOnly, 
		bool useDeltaCost /* = true */, bool annualised /* = false */, bool useTodayData /* = false */, float scale /* = 1.0f */, 
		float defaultCost /* = std::nanf("") */) {
		const auto* price 		= rt.dataRegistry->get<FloatMatrixData>(universe, priceDataId);
		ASSERT(price, "Unable to load data: " << priceDataId);

		const auto* costData	= rt.dataRegistry->get<FloatMatrixData>(universe, dataId);
		ASSERT(price, "Unable to load data: " << dataId);

		IntDay diD				= rt.di - 1;
		size_t size 			= alpha->cols();
		size_t ysize			= yesterday->alpha->cols();
		IntDay currDi			= !useTodayData ? diD : rt.di;
		float annualFactor		= annualised ? (float)Util::daysInYear(rt.dates[yesterday->di]) : 1.0f;

		FloatMatrixData alphaNotionals(nullptr, 1, size);
		FloatMatrixData yesterdayAlphaNotionals(nullptr, 1, size);
		FloatMatrixData valueDelta(nullptr, 1, size);
		FloatMatrixData costPercent(nullptr, 1, size);
		FloatMatrixData costMask(nullptr, 1, size);
		FloatMatrixData zero(nullptr, DataDefinition(), 1, size);
		zero.setMemory(0.0f);

		FloatMatrixData one(nullptr, DataDefinition(), 1, size);
		one.setMemory(1.0f);

		SH_MAT(alphaPnls, this->alphaPnls, size);
		SH_MAT(cumAlphaPnls, this->cumAlphaPnls, size);
		SH_MAT(alphaCosts, this->alphaCosts, size);

		memcpy(alphaNotionals.fdata(), &this->alphaNotionals[0], sizeof(float) * size);
		memcpy(yesterdayAlphaNotionals.fdata(), &yesterday->alphaNotionals[0], sizeof(float) * size);

		for (IntDay ii = (IntDay)ysize; ii < (IntDay)size; ii++) {
			yesterdayAlphaNotionals(0, ii) = 0.0f;
			alphaNotionals(0, ii) = 0.0f;
			alphaCosts(0, ii) = 0.0f;
		}

		float invAnnualFactor	= 1.0f / annualFactor;

		if (shortOnly)
			costMask[0]			= (alphaNotionals[0] >= zero[0]).select(0.0f, one.innerMat());
		else
			costMask.setMemory(1.0f);

		costMask[0]				= costMask[0] * (*price)[currDi].nanMask();

		if (costData->rows() > 1 && costData->cols() > 1)
			costPercent[0]		= ((*costData)[currDi] * invAnnualFactor).nanClear(!std::isnan(defaultCost) ? defaultCost : 0.0f);
		else {
			/// If it's not a constant
			if (costData->rows() != 1 && costData->cols() != 1)
				costPercent[0]	= costData->row(0) * invAnnualFactor;
			else
				costPercent.setMemory(costData->getValue(0, 0) * invAnnualFactor);
		}

		if (useDeltaCost)
			valueDelta[0]		= (alphaNotionals[0] - yesterdayAlphaNotionals[0]);
		else
			valueDelta[0]		= yesterdayAlphaNotionals[0];

		auto cost				= ((valueDelta[0].abs() * costPercent[0] * costMask[0]) * (0.01f * scale)).nanClear();
		alphaCosts[0]			+= cost;
		alphaPnls[0]			-= cost;
		cumAlphaPnls[0]			-= cost;

		//for (size_t ii = 0; ii < size; ii++) {
		//	float alphaPnl		= alphaPnls[ii];
		//	float costPercent	= (*costData)(currDi, ii) / annualFactor;
		//	float priceValue	= (*price)(currDi, ii);
		//	float prevAlphaNotional= 0.0f;
		//	float alphaValue	= alphaNotionals[ii];

		//	/// There is nothing against this security!
		//	if (std::isnan(costPercent)) {
		//		/// If there is no default cost then do nothing either ...
		//		if (std::isnan(defaultCost))
		//			continue;

		//		costPercent = defaultCost;
		//	}

		//	if (std::isnan(priceValue))
		//		continue;

		//	if (std::isnan(alphaPnl))
		//		continue;

		//	if (yesterday && ii < yesterday->alphaNotionals.size()) {
		//		prevAlphaNotional	= yesterday->alphaNotionals[ii];
		//		if (std::isnan(prevAlphaNotional))
		//			prevAlphaNotional = 0.0f;
		//	}

		//	if (std::isnan(alphaValue))
		//		alphaValue		= 0.0f;

		//	float valueDelta	= useDeltaCost ? alphaValue - prevAlphaNotional : prevAlphaNotional;
		//	float cost			= (std::abs(valueDelta) * costPercent * 0.01f) * scale;

		//	//ASSERT(!std::isnan(alphaValue), "Invalid alpha allocation value (NaN). This must be handled correctly!");

		//	/// If its short only then don't apply to long alphas
		//	if (shortOnly && (*alpha)(0, ii) >= 0.0f)
		//		cost			= 0.0f;

		//	alphaCosts[ii]		+= cost;
		//	alphaPnl			-= cost;
		//	ASSERT(!std::isnan(alphaPnl), "Cannot have a NaN in PNL calculation. This must be sorted out in the previous step(s) - di: " << rt.di << ", diD: " << currDi << ", ii: " << ii);

		//	cumAlphaPnls[ii]	-= cost; // adjust the cumulative PnL as well
		//	alphaPnls[ii]		= alphaPnl;
		//}
	}
	
	void CalcData::factorSlippage(const RuntimeData& rt, std::string priceDataId, const std::string dataId /* = "cost.slippage" */, 
		float defaultCost /* = std::nanf("") */) {
		factorGenericCost(rt, priceDataId, dataId, false, true, false, false, 1.0f, defaultCost);
	}
	
	void CalcData::factorImpact(const RuntimeData& rt, std::string priceDataId, const std::string dataId /* = "" */) {
		if (dataId.empty())
			return;
		factorGenericCost(rt, priceDataId, dataId, false);
	}
	
	void CalcData::factorBorrowCost(const RuntimeData& rt, std::string priceDataId, const std::string dataId /* = "cost.netBorrowCost" */) {
		/// No borrow costs without any data from previous day ...
		if (!yesterday)
			return;

		float scale = 0.01f; /// Since its in basis points

		/// Since we need to include weekends and holidays in borrowing costs as well //////////////////////////////////////////////////////////////////////////
		float daysInBetween = (float)Util::daysInBetween(rt.dates[yesterday->di], rt.dates[this->di]);
		scale *= daysInBetween;

		factorGenericCost(rt, priceDataId, dataId, true, false, true, false, scale);
	}
	
	void CalcData::factorTradingCost(const RuntimeData& rt, std::string priceDataId, const std::string dataId /* = "cost.trade" */) {
		factorGenericCost(rt, priceDataId, dataId, false);
	}

	void CalcData::factorDividendCost(const pesa::RuntimeData& rt, std::string divAmountDataId, float dividendTax) {
		/// Dividend is only calculated on positions held overnight and the Div-EX date is TODAY
		if (!yesterday)
			return;

		auto* divAmounts_		= rt.dataRegistry->floatData(this->universe, divAmountDataId);
		size_t size 			= yesterday->alpha->cols();
		FloatMatrixData			longMask(nullptr, 1, size);
		FloatMatrixData			shortMask(nullptr, 1, size);
		FloatMatrixData			divAmounts(nullptr, 1, size);
		FloatMatrixData			zero(nullptr, DataDefinition(), 1, size);
		FloatMatrixData			one(nullptr, DataDefinition(), 1, size);

		zero.setMemory(0.0f);
		one.setMemory(1.0f);

		SH_MAT(alphaCosts, this->alphaCosts, size);
		SH_MAT(alphaPnls, this->alphaPnls, size);
		SH_MAT(cumAlphaPnls, this->cumAlphaPnls, size);
		SH_MAT(alphaShares, yesterday->alphaShares, size);

		auto ashares			= alphaShares[0].nanClear();
		divAmounts[0]			= divAmounts_->row(rt.di).nanClear();

		longMask[0]				= (ashares > zero[0]).select(one.innerMat(), 0.0f).nanClear();
		shortMask[0]			= (ashares < zero[0]).select(one.innerMat(), 0.0f).nanClear();

		auto longDivProfit		= ((ashares * divAmounts[0] * longMask[0]) * (1.0f - dividendTax)).nanClear(); 
		auto shortDivCost		= ((ashares.abs() * divAmounts[0] * shortMask[0])).nanClear();
		auto diff				= (longDivProfit - shortDivCost);

		/// Individual alpha costs go here ...
		alphaCosts[0]			-= diff; /// Costs are a positive number so long costs will need to be subtracted

		/// Now we add it to alpha as well as the cum alpha pnls
		alphaPnls[0]			+= diff;
		cumAlphaPnls[0]			+= diff;

		//float shortCost			= shortDivCost.sum();
		//float longProfit		= longDivProfit.sum();
		//float totalCost			= (longProfit - shortCost);
		//Debug("PnL", "Dividends: %0.2hf [%0.2hf - %0.2hf]", totalCost, longProfit, shortCost);
	}

	//////////////////////////////////////////////////////////////////////////
	/// Optimiser result verification!
	//////////////////////////////////////////////////////////////////////////
	bool CalcData::optimise(IOptimiser* optimiser, const RuntimeData& rt, float bookSize) {
		PROFILE_SCOPED();

		ASSERT(optimiser, "Invalid optimiser passed!");
		optimiser->calcData() = this;
		optimiser->bookSize() = bookSize;
		optimiser->run(rt);
		return optimiser->didOptimise();
	}

	bool CalcData::optimise(const ConfigSection& portConfig, const RuntimeData& rt, float bookSize) {
		PROFILE_SCOPED();

		const ConfigSection* optSec = portConfig.getChildSection("Optimise");

		if (!optSec)
			return false;

		IOptimiserPtr optimiser = helper::ShLib::global().createComponent<IOptimiser>(*optSec, rt.dataRegistry);

		if (!optimiser) {
			Warning("OPT", "Unable to create optimiser!");
			return false;
		}

		return optimise(optimiser.get(), rt, bookSize);
	}

	//////////////////////////////////////////////////////////////////////////
	/// General utility
	//////////////////////////////////////////////////////////////////////////
	std::string CalcData::hline(size_t count) {
		return Statistics::hline(count ? count : 11);
	}

	std::string CalcData::perStockHeaderString(bool tabular, bool underline) {
		std::string s;

		if (tabular)
			s += "|";

		s =
			Poco::format("%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s",
				std::string("Ticker"),
				std::string("FIGI"),
				std::string("$ Notional"),
				std::string("$ D. P&L"),
				std::string("Cost"),
				std::string("D. TVR"),
				std::string("Num Shrs."),
				std::string("$ O. P&L"),
				std::string("$ H. P&L"),
				std::string("$ L. P&L")
				);

		if (tabular) {
			s += "|\n";
			if (underline)
				s += hline() + "\n";
		}
		else {
			/// We do not put newlines and let the user deal with non-tabular data
			//s += "\n";
			s = Poco::replace(s, "|", ",");
		}

		return s;
	}

	std::string CalcData::toPerStockString(bool tabular, bool header) const {
#define VV(v)		(v.size() && ii < v.size() && !std::isnan(v[ii]) ? v[ii] : 0.0f)

		std::string s;

		if (tabular) {
			s += hline() + "\n";
			if (header)
				s += CalcData::perStockHeaderString(tabular, true);
			s += "|";
		}

		size_t universeSize = universe->size(di);

		for (size_t ii = 0; ii < universeSize; ii++) {
			if (universe->isValid(di, ii)) {
				Security sec = universe->security(di, (Security::Index)ii);
				float prevAlphaNotional = yesterday ? yesterday->alphaNotionals[ii] : 0.0f;
				float prevAlpha = yesterday ? yesterday->rawAlpha[ii] : std::nanf("");
				float lowPnl = VV(alphaLowPnls);
				float highPnl = VV(alphaHighPnls);
				float closePnl = VV(alphaPnls);

				s += Poco::format("%12s|%12s|%12.2hf|%12.2hf|%12.4hf|%12.4hf|%12d|%12.2hf|%12.2hf|%12.2hf",
					sec.tickerStr(), sec.uuidStr(),
					VV(alphaNotionals), closePnl, VV(alphaCosts), 
					(std::abs(VV(alphaNotionals) - prevAlphaNotional) / stats.bookSize) * 100.0f,
					(int)VV(alphaShares), VV(alphaOpenPnls), 
					std::max(std::max(lowPnl, highPnl), closePnl), 
					std::min(std::min(lowPnl, highPnl), closePnl));

				//for (size_t k = 0; k < alphaIntradayPnls.size(); k++) {
				//	s += Poco::format("%12.2hf", VV(alphaIntradayPnls[k]));
				//}

				s += "\n";
			}
		}

		if (tabular) {
			s += "|\n";
			s += hline();
		}
		else {
			/// We do not put newlines and let the user deal with non-tabular data
			//s += "\n";
			s = Poco::replace(s, "|", ",");
		}

		return s;

#undef VV
	}

	std::string CalcData::tickerHeaderString(bool tabular, bool underline) {
		std::string s;

		if (tabular)
			s += "|";

		s =
			format("%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s",
				std::string("Date"),
				std::string("$ BookSize"),
				std::string("$ Cum. P&L"),
				std::string("$ D. P&L"),
				std::string("C.P."),
				std::string("D. TVR"),
				std::string("$ Alloc."),
				std::string("$ P. Alloc"),
				std::string("$ Num Shrs."),
				std::string("AlphaVal"),
				std::string("P.AlphaVal."),
				std::string("Cost")
			);

		if (tabular) {
			s += "|\n";
			if (underline)
				s += hline() + "\n";
		}
		else {
			/// We do not put newlines and let the user deal with non-tabular data
			//s += "\n";
			s = Poco::replace(s, "|", ",");
		}

		return s;
	}

	std::string CalcData::toString(Security::Index ii, bool tabular, bool header) const {
#define VV(v)		(v.size() ? v[ii] : 0.0f)

		std::string s;

		if (tabular) {
			s += hline() + "\n";
			if (header)
				s += CalcData::tickerHeaderString(tabular, true);
			s += "|";
		}

		float prevAlphaNotional = yesterday ? yesterday->alphaNotionals[ii] : 0.0f;
		float prevAlpha = yesterday ? yesterday->rawAlpha[ii] : std::nanf("");

		s += format("%12d|%12.2hf|%12.2hf|%12.2hf|%12.2hf|%12.4hf%%|%12.2hf|%12.2hf|%12d|%12.4hf|%12.4hf|%12.2hf",
			stats.date, stats.bookSize, VV(cumAlphaPnls), VV(alphaPnls), VV(alphaPrice), (std::abs(VV(alphaNotionals) - prevAlphaNotional) / stats.bookSize) * 100.0f,
			VV(alphaNotionals), prevAlphaNotional, (int)VV(alphaShares), rawAlpha[ii],
			prevAlpha, VV(alphaCosts));

		if (tabular) {
			s += "|\n";
			s += hline();
		}
		else {
			/// We do not put newlines and let the user deal with non-tabular data
			//s += "\n";
			s = Poco::replace(s, "|", ",");
		}

		return s;

#undef VV
	}

	io::InputStream& CalcData::read(io::InputStream& is) {
		size_t numRows = 0;
		size_t numCols = 0;

		PESA_READ_VAR(is, numRows);
		PESA_READ_VAR(is, numCols);

		ensureAlphaSize(numRows, numCols);

		for (size_t di = 0; di < numRows; di++) {
			std::vector<float> row(numCols);
			is.read(("alpha-" + Util::cast(di)).c_str(), &row);

			const float* src = &row[0];
			float* dst = alpha->rowPtr((IntDay)di);
			memcpy(dst, src, sizeof(float) * numCols);
		}

		cumStats = std::make_shared<LifetimeStatistics>();

		is.read("cumStats", cumStats.get());
		PESA_READ_VAR(is, stats);
		PESA_READ_VAR(is, rawAlpha);
		PESA_READ_VAR(is, alphaNotionals);
		PESA_READ_VAR(is, alphaPnls);
		PESA_READ_VAR(is, cumAlphaPnls);
		PESA_READ_VAR(is, alphaCosts);
		PESA_READ_VAR(is, alphaShares);
		PESA_READ_VAR(is, alphaReturns);
		PESA_READ_VAR(is, alphaPrice);
		PESA_READ_VAR(is, alphaPrevPrice);
		PESA_READ_VAR(is, sortedAlpha);
		PESA_READ_VAR(is, rankedAlpha);

		PESA_READ_VAR(is, customData);

		PESA_READ_VAR(is, alphaOpenPnls);
		PESA_READ_VAR(is, alphaHighPnls);
		PESA_READ_VAR(is, alphaLowPnls);

		/// Recalculate the stats after loading them from the database ...
		cumStats->recalculate();

		//if (yesterday) 
		//	is.read("yesterday", yesterday);

		return is;
	}

	io::OutputStream& CalcData::write(io::OutputStream& os) {
		size_t numRows = alpha->rows();
		size_t numCols = alpha->cols();

		PESA_WRITE_VAR(os, numRows);
		PESA_WRITE_VAR(os, numCols);

		for (size_t di = 0; di < numRows; di++) {
			std::vector<float> row = alpha->rowArray((IntDay)di);
			os.write(("alpha-" + Util::cast(di)).c_str(), row);
		}

		os.write("cumStats", *(cumStats.get()));
		PESA_WRITE_VAR(os, stats);
		PESA_WRITE_VAR(os, rawAlpha);
		PESA_WRITE_VAR(os, alphaNotionals);
		PESA_WRITE_VAR(os, alphaPnls);
		PESA_WRITE_VAR(os, cumAlphaPnls);
		PESA_WRITE_VAR(os, alphaCosts);
		PESA_WRITE_VAR(os, alphaShares);
		PESA_WRITE_VAR(os, alphaReturns);
		PESA_WRITE_VAR(os, alphaPrice);
		PESA_WRITE_VAR(os, alphaPrevPrice);
		PESA_WRITE_VAR(os, sortedAlpha);
		PESA_WRITE_VAR(os, rankedAlpha);

		PESA_WRITE_VAR(os, customData);

		PESA_WRITE_VAR(os, alphaOpenPnls);
		PESA_WRITE_VAR(os, alphaHighPnls);
		PESA_WRITE_VAR(os, alphaLowPnls);
		//if (yesterday)
		//	os.write("yesterday", *yesterday);

		return os;
	}

	std::string CalcData::getCustomData(const std::string& key) const {
		auto iter = customData.find(key);
		if (iter == customData.end())
			return "";
		return iter->second;
	}

	void CalcData::setCustomData(const std::string& key, const std::string& value) {
		//auto iter = customData.find(key);
		//if (iter == customData.end()) {
		//	Warning("CALC_DATA", "Key already in use: %s [Overwriting existing data. Please ensure you own this key!]", key);
		//}
		customData[key] = value;
	}
} /// namespace pesa 

#undef PESA_INIT_FLOAT_VEC
#undef SH_MAT
