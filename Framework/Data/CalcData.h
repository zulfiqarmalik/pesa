/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// CalcData.h
///
/// Created on: 13 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/Components/ComponentDefs.h"
#include "CustomData.h"
#include "MatrixData.h"
#include <vector>
#include <deque>

#include "Statistics.h"

namespace pesa {
	class IOptimiser;

	struct CalcData;
	typedef std::shared_ptr<CalcData> CalcDataPtr;

	////////////////////////////////////////////////////////////////////////////
	/// CalcData: Represents the calculation being performed on the alpha
	/// operation or portfolio. Essentially the data at each point in 
	/// the simulation engine. This is used by IAlpha for alpha calculation.
	/// IOperation uses this to perform calculation on the data. Since operations
	/// are generic (i.e. don't necessarily have to work with Alphas) 
	/// this data can not only come from alpha but also from the portfolio 
	/// or any other custom component that uses the operation
	////////////////////////////////////////////////////////////////////////////
	struct Framework_API CalcData 
#ifndef __SCRIPT__
		: public io::Streamable
#endif 
	{
		pesa::FloatMatrixDataPtr	alpha;					/// The actual alpha vector
		pesa::Statistics			stats;					/// The statistics for the alpha
		pesa::LifetimeStatisticsPtr	cumStats;				/// The lifetime stats of this alpha
		pesa::IUniverse*			universe = nullptr;		/// The universe that this alpha is working with
		std::shared_ptr<pesa::CalcData> yesterday = nullptr;/// Data from yesterday!
		IntDay						delay = 1;				/// The delay for this alpha
		IntDay						di = 0;					/// The day for which these calculations were done

		const ConfigSection*		config = nullptr;		/// The config associated with this data. Note that this might be a NULL pointer at times!

		/// This is only created when we have multiple <Alpha> sections under an <Alphas> section in the 
		/// config, and they use different universe ids. It then becomes problematic to make do the combo
		/// and the final portfolio! The remappedAlpha is then used to make sure that all the alphas
		/// have the same size. 
		pesa::FloatMatrixDataPtr	remappedAlpha;			/// (alpha) vector remapped to another universe
		pesa::IUniverse*			remappedUniverse = nullptr; /// The universe that this was remapped to

		////////////////////////////////////////////////////////////////////////////
		/// This data is calculated by the system that calculates the PNL
		/// for each of the alpha
		////////////////////////////////////////////////////////////////////////////
		pesa::FloatVec				rawAlpha;				/// The raw alpha vector 
        pesa::FloatVec				alphaNotionals;			/// alpha * <bookSize>
		pesa::FloatVec				alphaPnls;				/// The PNL values for each of the individual alphas
		pesa::FloatVec				cumAlphaPnls;			/// The PNL values for each of the individual alphas
		pesa::FloatVec				alphaCosts;				/// The total cost of alpha accumulated
		pesa::FloatVec				alphaShares;			/// Number of shares to buy. This is floating point but 
															/// when the time comes for execution, these are rounded down
															/// to the nearest integer
		pesa::FloatVec				alphaReturns;			/// Returns of the alpha calculated for the day ((closePrice[di - 1] / closePrice[di]) - 1)
		pesa::FloatVec				alphaPrice;				/// Close Price at di
		pesa::FloatVec				alphaPrevPrice;			/// Close Price at di - 1
		pesa::FloatVec				alphaTurnover;			/// The turnover from previous day to today

		pesa::IntVec				sortedAlpha;			/// Indexes of alpha DESC sorted
		pesa::IntVec				rankedAlpha;			/// The rank of the alpha

		pesa::FloatVec				alphaSharesCalcPrice;	/// Price that was used to calculate alpha shares

		pesa::FloatVec				alphaOpenPnls;			/// The opening PNL for the day
		pesa::FloatVec				alphaHighPnls;			/// The PNL high for the day
		pesa::FloatVec				alphaLowPnls;			/// The PNL low for the day
		std::vector<pesa::FloatVec> alphaIntradayPnls;		/// The intraday PNLs (these are optional)

		//////////////////////////////////////////////////////////////////////////
		/// Additional serializable data that has can be written to and read
		/// from by the different components. Most common scenario being CHECKPOINT
		/// support (custom data). Most common data format is JSON!
		//////////////////////////////////////////////////////////////////////////
		std::map<std::string, std::string> customData;

									CalcData();
									CalcData(const CalcData& rhs);
									~CalcData();

		pesa::CalcData&				operator = (const CalcData& rhs);
		void						deepCopy(const CalcData& rhs);

		//////////////////////////////////////////////////////////////////////////
		/// Internal use from C++ only. Cannot be used from Python/Scripts
		//////////////////////////////////////////////////////////////////////////
#ifndef __SCRIPT__
		io::InputStream&			read(io::InputStream& is);
		io::OutputStream&			write(io::OutputStream& os);
#endif
		//////////////////////////////////////////////////////////////////////////

		/// Static helper function. Pass in an alpha and a universe and it will 
		/// fill out alpha values, pnls, shares and returns
		/// This will calculate the PNL
		static void					calculatePNL(const pesa::RuntimeData& rt, IntDay di, IntDay delay, float bookSize, pesa::FloatMatrixData& alpha, 
									pesa::IUniverse* universe, pesa::FloatVec& alphaValues, pesa::FloatVec& alphaPnls, pesa::FloatVec& alphaHighPnls, 
									pesa::FloatVec& alphaOpenPnls, pesa::FloatVec& alphaLowPnls, std::vector<pesa::FloatVec>& alphaIntradayPnls, pesa::FloatVec& cumAlphaPnls,
									pesa::FloatVec& alphaShares, pesa::FloatVec& alphaReturns, pesa::FloatVec& alphaPrice, pesa::FloatVec& alphaPrevPrice, 
									std::string priceDataId, std::string openPriceDataId, std::string highPriceDataId, std::string lowPriceDataId, pesa::StringVec* intradayPriceDataId,
									std::string currDayPriceDataId);

		void						calculatePNL(const pesa::RuntimeData& rt, float bookSize, std::string priceDataId, std::string openPriceDataId, std::string highPriceDataId,
									std::string lowPriceDataId, pesa::StringVec* intradayPriceDataId, std::string currDayPriceDataId);

		void						ensureAlphaSize(size_t rows, size_t cols);
		void						resetAlpha();

		/// This will prepare and finalise the Alpha data of TODAY so 
		/// that it is ready for PNL calculation TOMORROW
		static void					finaliseForPNLCalc(const pesa::RuntimeData& rt, IntDay di, IntDay delay, float bookSize, pesa::FloatMatrixData& alpha,
									pesa::IUniverse* universe, pesa::FloatVec& alphaValues, pesa::FloatVec& alphaShares, pesa::FloatVec& alphaSharesCalcPrice,
									std::string priceDataId, std::string currDayPriceDataId);

		void						finaliseForPNLCalc(const pesa::RuntimeData& rt, float bookSize, std::string priceDataId, std::string currDayPriceDataId);

		/// Calculates the stats!
		void						calculateStats(const pesa::RuntimeData& rt, float bookSize);

		/// These are some helper functions that allow us to factor in cost and other
		/// calculations for a particular alpha
		void						calculateValueFromShares(const pesa::RuntimeData& rt, std::string priceDataId, std::string currDayPriceDataId, bool recalcShares);
		void						factorGenericCost(const pesa::RuntimeData& rt, std::string priceDataId, std::string dataId, bool shortOnly, 
									bool useDeltaCost = true, bool annualised = false, bool useTodayData = false, float scale = 1.0f, float defaultCost = std::nanf(""));
		void						factorSlippage(const pesa::RuntimeData& rt, std::string priceDataId, std::string dataId = "cost.slippage", float defaultCost = std::nanf(""));
		void						factorImpact(const pesa::RuntimeData& rt, std::string priceDataId, std::string dataId = "");
		void						factorBorrowCost(const pesa::RuntimeData& rt, std::string priceDataId, std::string dataId = "cost.netBorrowCost");
		void						factorTradingCost(const pesa::RuntimeData& rt, std::string priceDataId, std::string dataId = "cost.trade");
		void						factorDividendCost(const pesa::RuntimeData& rt, std::string divAmountDataId, float dividendTax);

		//////////////////////////////////////////////////////////////////////////
		/// Optimisation and verification
		//////////////////////////////////////////////////////////////////////////
		bool						optimise(IOptimiser* optimiser, const RuntimeData& rt, float bookSize);
		bool						optimise(const ConfigSection& portConfig, const RuntimeData& rt, float bookSize);

		//////////////////////////////////////////////////////////////////////////
		/// General utility
		//////////////////////////////////////////////////////////////////////////
		static std::string			hline(size_t count = 0);
		static std::string			tickerHeaderString(bool tabular, bool underline);
		static std::string			perStockHeaderString(bool tabular, bool underline);
		std::string					toPerStockString(bool tabular, bool header) const;
		std::string					toString(Security::Index ii, bool tabular, bool header) const;

		std::string					getCustomData(const std::string& key) const;
		void						setCustomData(const std::string& key, const std::string& value);

		//////////////////////////////////////////////////////////////////////////
		/// Inline functions
		//////////////////////////////////////////////////////////////////////////
		inline float				pnl(size_t ii) { return ii < alphaPnls.size() ? alphaPnls[ii] : 0.0f; }
		inline float				cumPnl(size_t ii) { return ii < cumAlphaPnls.size() ? cumAlphaPnls[ii] : 0.0f; }
		inline float				cost(size_t ii) { return ii < alphaCosts.size() ? alphaCosts[ii] : 0.0f; }
		inline float				shares(size_t ii) { return ii < alphaShares.size() ? alphaShares[ii] : 0.0f; }
		inline float				returns(size_t ii) { return ii < alphaReturns.size() ? alphaReturns[ii] : 0.0f; }
		inline float				price(size_t ii) { return ii < alphaPrice.size() ? alphaPrice[ii] : 0.0f; }
		inline float				prevPrice(size_t ii) { return ii < alphaPrevPrice.size() ? alphaPrevPrice[ii] : 0.0f; }
		inline float				alphaValue(size_t ii) { return ii < alphaNotionals.size() ? alphaNotionals[ii] : 0.0f; }
        inline void                 setAlphaValue(size_t ii, float value) { alphaNotionals[ii] = value; }

		//////////////////////////////////////////////////////////////////////////
		/// !!! ONLY USE IN SCRIPTS - DON'T USE IN C++ !!!
		//////////////////////////////////////////////////////////////////////////
		inline FloatMatrixData*		alphaPtr() { return alpha.get(); }
		inline FloatMatrixData*		remappedAlphaPtr() { return remappedAlpha.get(); }
		//inline float* 				alphaPtr() { return alpha->fdata(); }
        inline float* 				rawAlphaPtr() { return rawAlpha.size() ? &rawAlpha[0] : nullptr; }
        inline float* 				alphaValuesPtr() { return alphaNotionals.size() ? &alphaNotionals[0] : nullptr; }
        inline float* 				alphaPnlsPtr() { return alphaPnls.size() ? &alphaPnls[0] : nullptr; }
        inline float* 				cumAlphaPnlsPtr() { return cumAlphaPnls.size() ? &cumAlphaPnls[0] : nullptr; }
        inline float* 				alphaCostsPtr() { return alphaCosts.size() ? &alphaCosts[0] : nullptr; }
        inline float* 				alphaSharesPtr() { return alphaShares.size() ? &alphaShares[0] : nullptr; }
        inline float* 				alphaReturnsPtr() { return alphaReturns.size() ? &alphaReturns[0] : nullptr; }
        inline float* 				alphaPricePtr() { return alphaPrice.size() ? &alphaPrice[0] : nullptr; }
        inline float* 				alphaPrevPricePtr() { return alphaPrevPrice.size() ? &alphaPrevPrice[0] : nullptr; }
	};
} /// namespace pesa 

