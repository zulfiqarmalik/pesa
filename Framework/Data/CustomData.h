/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// CustomData.h
///
/// Created on: 15 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/FrameworkDefs.h"
#include "Data.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// CustomData - Use this for storing custom data types
	////////////////////////////////////////////////////////////////////////////
	template <typename t_type>
	class CustomData : public Data {
	public:
		typedef std::shared_ptr<t_type> TPtr;

	protected:
		TPtr					m_data = nullptr;		/// The actual data
		size_t					m_dataSize = 0;			/// The total number of items

	public:
		CustomData(IUniverse* universe, TPtr data, size_t dataSize, DataDefinition def)
			: Data(universe, def)
		 	, m_data(data)
			, m_dataSize(dataSize) {
		}

		virtual ~CustomData() {
		}

		virtual void makeInvalid(IntDay di, size_t ii, size_t kk = 0) {
			memset((char*)m_data.get() + (di * 0 + ii * m_def.itemSize), 0, m_def.itemSize);
		}

		virtual bool isValid(IntDay di, size_t ii, size_t kk = 0) const {
			return true;
		}

		virtual const void* data() const {
			return (const void*)m_data.get();
		}

		virtual void* data() {
			return (void*)m_data.get();
		}

		virtual size_t dataSize() const {
			return m_dataSize;
		}

		virtual Data* clone(IUniverse* universe, bool noCopy) {
			return new CustomData<t_type>(universe ? universe : Data::m_universe, m_data, m_dataSize, m_def);
		}

		virtual Data* createInstance() {
			return new CustomData<t_type>(nullptr, nullptr, 0, def());
		}

		virtual void ensureSize(size_t rows, size_t cols, bool copyOld = false) {
			size_t numItemsRequired = rows * cols;
			size_t dataSizeRequired = numItemsRequired * sizeof(t_type);

			if (m_dataSize >= dataSizeRequired)
				return;

			// otherwise we rcreate
			TPtr existingData = m_data;
			m_data = TPtr(new t_type [dataSizeRequired], SharedPtr_ArrayDeleter<t_type>());

			// copy the old data to the new buffer
			if (copyOld && existingData)
				memcpy((void*)m_data.get(), (const void*)existingData.get(), m_dataSize);

			m_dataSize = dataSizeRequired;
		}

		virtual void initMem(void* mem, size_t memSize) const {
			size_t isize = Data::itemSize();

			ASSERT(memSize % isize == 0, "Unaligned memory given to initMem. Expecting total size to be aligned to: " <<
				isize << ", given total size: " << memSize);

			t_type* tmem = reinterpret_cast<t_type*>(mem);
			size_t numItems = memSize / isize;

			t_type value;
			pesa::invalid::get(value);

			std::fill_n(tmem, numItems, value);
		}

		inline const TPtr	dataPtr() const		{ return m_data; }
		inline TPtr			dataPtr()			{ return m_data; }
		 
		const t_type&		operator*() const	{ return *m_data.get(); }
		t_type&				operator*()			{ return *m_data.get(); }

		const t_type*		operator->() const	{ return m_data.get(); }
		t_type*				operator->()		{ return m_data.get(); }
	};

	typedef CustomData<char> 			BinaryData;
	typedef std::shared_ptr<BinaryData>	BinaryDataPtr;
} /// namespace pesa 

