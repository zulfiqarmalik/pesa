/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// CustomVectorData.h
///
/// Created on: 12 May 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/FrameworkDefs.h"
#include "CustomData.h"

namespace pesa {
	typedef std::shared_ptr<char> CharPtr;

	////////////////////////////////////////////////////////////////////////////
	/// CustomVectorData - Since vectored data is very common there is a helpful
	/// class that provides a CustomData implementation for std::vector-ed
	/// type data
	////////////////////////////////////////////////////////////////////////////
	template <typename t_type>
	class CustomVectorData : public CustomData<std::vector<t_type> > {
	protected:
		typedef std::vector<t_type> t_typeVec;
		typedef std::shared_ptr<std::vector<t_type> > TPtr;

	public:
		CustomVectorData(IUniverse* universe, TPtr data, DataDefinition def)
			: CustomData<t_typeVec>(universe, data, data ? data->size() * sizeof(t_type) : 0, def) {
		}

		CustomVectorData(IUniverse* universe, DataDefinition def, size_t size)
			: CustomData<t_typeVec>(universe, nullptr, 0, def) {
			ensureSize(1, size, false);
		}

		virtual ~CustomVectorData() {
		}

		virtual void copyElement(const Data* src_, IntDay diSrc, size_t iiSrc, IntDay diDst, size_t iiDst, size_t kkSrc = 0, size_t kkDst = 0) {
			auto* src = dynamic_cast<const CustomVectorData<t_type>*>(src_);

			ASSERT(src, "Unable to cast source in CustomVectorData");
			ASSERT(diSrc == 0 && diDst == 0, "Cannot have multi-dimensional access to CustomVectorData (its a 1-D vector)");

			vector()->at(iiDst) = src->vector()->at(iiSrc);
		}

		virtual const void* data() const {
			return (const void*)(&(*CustomData<t_typeVec>::m_data)[0]);
		}

		virtual Data* clone(IUniverse* universe, bool noCopy) {
            return new CustomVectorData<t_type>(universe ? universe : Data::m_universe, CustomData<t_typeVec>::m_data, CustomData<t_typeVec>::def());
		}

		virtual Data* createInstance() {
			return new CustomVectorData<t_type>(nullptr, nullptr, CustomData<t_typeVec>::def());
		}

		virtual void* data() {
			return (void*)(&(*CustomData<t_typeVec>::m_data)[0]);
		}

		virtual size_t numItems() const {
			return CustomData<t_typeVec>::m_data->size();
		}

		virtual void ensureSize(size_t rows, size_t cols, bool copyOld = false) {
			size_t numItemsRequired = rows * cols;
			TPtr existingData = CustomData<t_typeVec>::m_data;

			if (existingData && existingData->size() >= numItemsRequired)
				return;

			// otherwise we rcreate
			CustomData<t_typeVec>::m_data = TPtr(new t_typeVec(numItemsRequired));

			// copy the old data to the new buffer
			if (copyOld && existingData)
				memcpy(&(*CustomData<t_typeVec>::m_data)[0], &(*existingData)[0], existingData->size() * sizeof(t_type));

			CustomData<t_typeVec>::m_dataSize = CustomData<t_typeVec>::m_data->size() * sizeof(t_type);
		}

		inline const TPtr vector() const {
			return CustomData<t_typeVec>::m_data;
		}

		inline TPtr vector() {
			return CustomData<t_typeVec>::m_data;
		}

		virtual void initMem(void* mem, size_t memSize) const {
			size_t isize = Data::itemSize();

			ASSERT(memSize % isize == 0, "Unaligned memory given to initMem. Expecting total size to be aligned to: " <<
				isize << ", given total size: " << memSize);

			t_type* tmem = reinterpret_cast<t_type*>(mem);
			size_t numItems = memSize / isize;

			t_type value;
			pesa::invalid::get(value);

			std::fill_n(tmem, numItems, value);
		}
	};

	typedef std::vector<int> 				IntVector;
	typedef std::shared_ptr<IntVector>		IntVectorPtr;
	typedef CustomVectorData<int> 			IntVectorData;
	typedef std::shared_ptr<IntVectorData>	IntVectorDataPtr;
	typedef std::vector<Poco::DateTime>		DateTimeVector;
	typedef std::shared_ptr<DateTimeVector>	DateTimeVectorPtr;
	typedef CustomVectorData<Poco::DateTime> DateTimeVectorData;

	typedef std::vector<Security::Index> 				SecurityIndexVector;
	typedef std::shared_ptr<SecurityIndexVector>		SecurityIndexVectorPtr;
	typedef CustomVectorData<Security::Index> 			SecurityIndexVectorData;
	typedef std::shared_ptr<SecurityIndexVectorData>	SecurityIndexVectorDataPtr;

} /// namespace pesa 

