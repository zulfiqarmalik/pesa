/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Data.cpp
///
/// Created on: 15 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Data.h"
#include "Dataset.h"
#include "Framework/Components/IUniverse.h"
#include <cmath>

#include "MatrixData.h"
#include "StringData.h"

namespace pesa {

	SharedMemory::SharedMemory(const std::string& name, std::size_t size, SharedMemory::AccessMode mode, bool server /* = true */, const void* addrHint /* = nullptr */) :
		_size(size),
		_fd(-1),
		_address(0),
		_access(mode),
		_name("/"),
		_fileMapped(false),
		_server(server) {

#if defined(__LINUX__) || defined(__MACOS__)

		_name.append(name);

		int flags = O_CREAT;
		if (_access == SharedMemory::AM_WRITE)
			flags |= O_RDWR;
		else
			flags |= O_RDONLY;

		// open the shared memory segment
		_fd = ::shm_open(_name.c_str(), flags, S_IRUSR | S_IWUSR);
		if (_fd == -1)
			throw Poco::SystemException("Cannot create shared memory object", _name);

		// now set the correct size for the segment

		if (flags & O_RDWR) {
			if (-1 == ::ftruncate(_fd, size)) {
				::close(_fd);
				_fd = -1;
				::shm_unlink(_name.c_str());
				throw Poco::SystemException("Cannot resize shared memory object", _name);
			}
		}

		map(addrHint);

#endif
	}

	SharedMemory::~SharedMemory() {
#if defined(__LINUX__) || defined(__MACOS__)

		unmap();
		close();

#endif 
	}

	void SharedMemory::map(const void* addrHint) {
#if defined(__LINUX__) || defined(__MACOS__)

		int access = PROT_READ;
		if (_access == SharedMemory::AM_WRITE)
			access |= PROT_WRITE;

		void* addr = ::mmap(const_cast<void*>(addrHint), _size, access, MAP_SHARED, _fd, 0);
		if (addr == MAP_FAILED)
			throw Poco::SystemException("Cannot map file into shared memory", _name);

		_address = static_cast<char*>(addr);

#endif 
	}


	void SharedMemory::unmap() {
#if defined(__LINUX__) || defined(__MACOS__)

		if (_address)
		{
			::munmap(_address, _size);
		}

#endif 
	}


	void SharedMemory::close() {
#if defined(__LINUX__) || defined(__MACOS__)

		if (_fd != -1) {
			::close(_fd);
			_fd = -1;
		}

		if (!_fileMapped && _server) {
			::shm_unlink(_name.c_str());
		}
#endif 
	}

	char* SharedMemory::begin() {
#if defined(__LINUX__) || defined(__MACOS__)
		return _address;
#else
		return nullptr;
#endif 
	}

	//////////////////////////////////////////////////////////////////////////

	namespace invalid {
		void get(float& value) {
			value = std::nanf("0");
		}

		void get(int& value) {
			value = ~0;
		}

		void get(uint16_t& value) {
			value = (uint16_t)(~0);
		}

		void get(Security::Index& value) {
			value = Security::s_invalidIndex;
		}

		bool check(Security::Index value) {
			return value == Security::s_invalidIndex;
		}

		bool check(float value) {
			return std::isnan(value);
		}

		bool check(int value) {
			return value == ~0;
		}

		bool check(uint16_t value) {
			return value == (uint16_t)(~0);
		}
	}

	Data::Data(IUniverse* universe, DataDefinition def)
		: m_universe(universe), m_def(def) {
		m_delay = 0;
	}

	Data::~Data() {
	}

	void Data::initMem(void* mem, size_t memSize) const {
		memset(mem, 0, memSize);
	}

	std::shared_ptr<Data> Data::getMetadata() {
		return nullptr;
	}

	void Data::setMetadata(std::shared_ptr<Data> rhs) {
		/// NO-OP!
	}

	size_t Data::depthAt(IntDay di, size_t ii) const {
		return 1;
	}

	void Data::ensureDepthAt(IntDay di, size_t ii, size_t depth) {
		return;
	}

	bool Data::isVariableDepth() const {
		return false;
	}

	void Data::copyElement(const Data* src, IntDay diSrc, size_t iiSrc, IntDay diDst, size_t iiDst, size_t kkSrc /*= 0*/, size_t kkDst /*= 0*/) {
		const char* srcData = reinterpret_cast<const char*>(src->data());
		char* dstData = reinterpret_cast<char*>(data());
		size_t isize = itemSize();
		ASSERT(isize == src->itemSize(), "Invalid, the data item sizes do not match. Src: " << src->itemSize() << " - Dst: " << isize);

		size_t srcIndex = (diSrc * src->rows() + iiSrc) * src->itemSize();
		size_t dstIndex = (diDst * rows() + iiDst) * isize;

		ASSERT(srcIndex < src->dataSize(), "Source buffer overflow while copying. Src Index: " << srcIndex << " - Src Size: " << src->dataSize());
		ASSERT(dstIndex < dataSize(), "Destination buffer overflow while copyying. Dst Index: " << dstIndex << " - Dst Size: " << dataSize());

		memcpy(dstData + dstIndex, srcData + srcIndex, isize);
	}

	void Data::copyRow(const Data* src, IntDay diSrc, IntDay diDst, size_t kkSrc /* = 0 */, size_t kkDst /* = 0 */) {
		ASSERT(cols() == src->cols(), "Number of columns in source are not the same. this->cols(): " << cols() << " - src->cols(): " << src->cols());

		/// This is the base implementation that copies element by element. Optimised implementations are provided by 
		/// individual implementations
		for (size_t ii = 0; ii < cols(); ii++)
			copyElement(src, diSrc, ii, diDst, ii, kkSrc, kkDst);
	}

	DataPtr Data::extractRow(IntDay di, size_t kk /* = 0 */) {
		DataPtr rowData = DataPtr(createInstance());
		rowData->ensureSize(1, cols(), true);
		rowData->copyRow(this, di, 0, kk, 0);
		return rowData;
	}

	void Data::ensureSizeAndDepth(size_t rows, size_t cols, size_t depth, bool copyOld /* = false */) {
		/// We just ignore the depth here in the default implementation ...
		this->ensureSize(rows, cols, copyOld);
	}

	size_t Data::rows() const {
		return 1;
	}

	size_t Data::cols() const {
		return numItems();
	}

	size_t Data::depth() const {
		return 1;
	}

	size_t Data::numItems() const {
		return dataSize() / m_def.itemSize;
	}

	size_t Data::itemSize() const {
		return m_def.itemSize;
	}

	bool Data::doesCustomReadWrite() const {
		return false;
	}

	io::OutputStream& Data::writeRow(io::OutputStream& out, size_t kk, size_t di) {
		return out;
	}

	io::InputStream& Data::readRow(io::InputStream& in, size_t kk, size_t di) {
		return in;
	}

	io::OutputStream& Data::write(io::OutputStream& out) {
		return out;
	}

	io::InputStream& Data::read(io::InputStream& in) {
		return in;
	}

	Data* Data::optimiseFor(IntDay diStart, IUniverse* srcUniverse, IUniverse* dstUniverse) {
		return nullptr;
	}

	bool Data::isDataValid() const {
		return rows() > 0 && cols() > 0;
	}

	void Data::postMemcpy() {
	}

	std::string Data::getString(pesa::IntDay di, size_t ii, size_t kk) const {
		return "";
	}

	size_t Data::II(pesa::IntDay di, size_t ii) const {
		di = DI(di);
		if (!m_universe)
			return (Security::Index)ii;
		Security::Index iiMapped = m_universe->index(di, ii);
		ASSERT(iiMapped != Security::s_invalidIndex, "Unable to map index: (" << di << ", " << ii << ") - Universe: " << m_universe->name());
		return (size_t)iiMapped;
	}

	IntDay Data::DI(IntDay di) const {
		if (di < 0)
			di = 0;

		if (m_applyDelay && di > 0)
			di -= m_delay;

		if (m_diOffset && di > 0)
			di = di - m_diOffset;

		if (!m_diLookup)
			return di;

		ASSERT(di < m_diLookup->size(), "Invalid index to lookup - di: " << di << " - Max index: " << m_diLookup->size());
		return (IntDay)m_diLookup->at(di);
	}

	void Data::invalidateAll() {
		size_t rows = this->rows();
		size_t cols = this->cols();
		size_t depth = this->depth();

		for (size_t k = 0; k < depth; k++) {
			for (size_t i = 0; i < rows; i++) {
				for (size_t j = 0; j < cols; j++) {
					this->makeInvalid((IntDay)i, j, k);
				}
			}
		}
	}

	FloatMatrixData& Data::asFloat() {
		return *(dynamic_cast<FloatMatrixData*>(this));
	}

	DoubleMatrixData& Data::asDouble() {
		return *(dynamic_cast<DoubleMatrixData*>(this));
	}

	StringData& Data::asString() {
		return *(dynamic_cast<StringData*>(this));
	}

	IntMatrixData& Data::asInt() {
		return *(dynamic_cast<IntMatrixData*>(this));
	}

	Int64MatrixData& Data::asInt64() {
		return *(dynamic_cast<Int64MatrixData*>(this));
	}

	//////////////////////////////////////////////////////////////////////////
	DataFrame::DataFrame(DataPtr data_, size_t dataOffset_, size_t dataSize_)
		: data(data_), dataOffset(dataOffset_), dataSize(dataSize_) {
	}

    DataFrame::~DataFrame() {
      Trace("DataFrame", "DELETING: %z", (data ? data->dataSize() : 0));
    }

	void DataFrame::reset() {
		data = nullptr;
		dataOffset = 0;
		dataSize = 0;
		noDataUpdate = false;
	}

	void DataFrame::setData(Data* data) {
		this->data = DataPtr(data);
	}
}/// namespace pesa 
