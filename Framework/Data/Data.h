/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Data.h
///
/// Created on: 15 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/FrameworkDefs.h"
#include "Framework/Components/ComponentDefs.h"
#include "Core/IO/Stream.h"

#if defined(__LINUX__) || defined(__MACOS__)
	#include <sys/types.h>
	#include <sys/stat.h>
	#include <sys/mman.h>
	#include <fcntl.h>
	#include <unistd.h>
#endif 

namespace pesa {

	class Framework_API SharedMemory {
	public:
		enum AccessMode {
			AM_READ = 0,
			AM_WRITE
		};

	private:
		std::size_t _size = 0;
		int         _fd = 0;
		char*       _address = nullptr;
		SharedMemory::AccessMode _access = SharedMemory::AM_READ;
		std::string _name;
		bool        _fileMapped = false;
		bool        _server = false;

	public:
					SharedMemory(const std::string& name, std::size_t size, SharedMemory::AccessMode mode, bool server = true, const void* addrHint = nullptr);
					~SharedMemory();

		void		map(const void* addrHint);
		void		unmap();
		void		close();
		char*		begin();
	};


	class IDataRegistry;

	namespace invalid {
		extern void Framework_API get(float& value);
		extern void Framework_API get(int& value);
		extern void Framework_API get(uint16_t& value);
		extern void Framework_API get(Security::Index& value);

		extern bool Framework_API check(float value);
		extern bool Framework_API check(int value);
		extern bool Framework_API check(uint16_t value);
		extern bool Framework_API check(Security::Index value);

		template <class t_type>
		void get(t_type& value) {
			memset(&value, 0, sizeof(t_type));
		}

		template <class t_type>
		bool check(const t_type& value) {
			return false;
		}
	}

	//////////////////////////////////////////////////////////////////////////
	/// Dumping and data debugging
	//////////////////////////////////////////////////////////////////////////
	struct DumpInfo {
		std::string				dataId;
		IntDate					startDate = 0;
		IntDate					endDate = 0;
		int 					limit = 0;
		bool					runDiagnostics = false;
	};

	typedef std::shared_ptr<char>	CharPtr;

	//////////////////////////////////////////////////////////////////////////
	/// DataRowHeader - The header for each row of data
	//////////////////////////////////////////////////////////////////////////
	struct Framework_API DataRowHeader {
		IntDate					date = 0;				/// The date of this data 
		size_t					size = 0;				/// The size in bytes of the row
		size_t					cols = 0;				/// The number of columns in the row
		size_t					depth = 1;				/// The depth of this row (defaults to 1)
		size_t					writtenBytes = 0;		/// How many bytes of data was actually written
	};

	typedef std::vector<DataRowHeader> DataRowHeaderVec;

	//////////////////////////////////////////////////////////////////////////
	/// IDataHandle
	//////////////////////////////////////////////////////////////////////////
	class IDataHandle {
	public:
		virtual bool			readRow(IntDate date, void* data, size_t dataLength, size_t offset = 0, DataRowHeader* rhdr = nullptr, bool noSeek = true) = 0;
		virtual bool			readRowHeader(IntDate date, DataRowHeader& rhdr) = 0;
		virtual IntDate			mapDate(IntDay di) = 0;
	};

	typedef std::shared_ptr<IDataHandle> IDataHandlePtr;

	class FloatMatrixData;
	class DoubleMatrixData;
	class StringData;
	class IntMatrixData;
	class Int64MatrixData;

	typedef std::shared_ptr<SharedMemory> SharedMemoryPtr;

	//////////////////////////////////////////////////////////////////////////
	/// Data - The base class for all data types in the DataRegistry
	//////////////////////////////////////////////////////////////////////////
	class Framework_API Data : public io::Streamable {
	protected:
		//typedef 
		//	std::shared_ptr<Data>	DataPtr;
		//typedef 
		//	std::vector<DataPtr>	DataPtrVec;

		/// Each item in the vector is the
		DataDefinition				m_def;					/// The data definition
		IUniverse*					m_universe = nullptr;	/// The universe that this data belongs to
		IntDay						m_diOffset = 0;			/// DI offset
		bool						m_namedArgument = false;/// Is this a named argument (used by ExprVM mostly)
		SizeVecPtr					m_diLookup;				/// di Index lookup
		IntDay						m_delay = 0;			/// The delay of this dataset
		bool						m_applyDelay = false;	/// Apply the delay or not
		DataRowHeaderVec			m_rowInfos;				/// The information against each row for matrix data

		SharedMemoryPtr				m_shmem = nullptr;		/// Shared memory pointer that was created for this data ...

		//DataPtrVec					m_chunks;				/// Chunks of data within this dataset. This is used for container datasets and not used be unique datasets as such

	public:
									Data(IUniverse* universe, DataDefinition def);
		virtual 					~Data();

		virtual void 				makeInvalid(pesa::IntDay di, size_t ii, size_t kk = 0) = 0;
		virtual bool 				isValid(pesa::IntDay di, size_t ii, size_t kk = 0) const = 0;
		virtual bool				isDataValid() const;
		virtual const void* 		data() const = 0;
		virtual void* 				data() = 0;
		virtual size_t 				dataSize() const = 0;
		virtual void 				ensureSize(size_t rows, size_t cols, bool copyOld = false) = 0;
		virtual void 				ensureSizeAndDepth(size_t rows, size_t cols, size_t depth, bool copyOld = false);
		virtual void				ensureDepthAt(IntDay di, size_t ii, size_t depth);
		virtual std::string			getString(pesa::IntDay di, size_t ii, size_t kk) const;
		virtual void				setString(pesa::IntDay di, size_t ii, size_t kk, const std::string& value) {}

		/// Clone the data from the same to the same or another universe
		virtual Data*				clone(IUniverse* universe, bool noCopy) = 0; 
		virtual Data*				createInstance() = 0;

		virtual void 				initMem(void* mem, size_t memSize) const;
		virtual size_t				rows() const;
		virtual size_t 				cols() const;
		virtual size_t				depth() const;
		virtual size_t				depthAt(IntDay di, size_t ii) const;
		virtual size_t				numItems() const;
		virtual size_t 				itemSize() const;

		virtual void				invalidateAll();

		virtual bool				doesSupportSharedMemory() const { return false; }
		virtual void				fromSharedMemory(size_t kk, char* shMemPtr, size_t rows, size_t cols) {}

		virtual IntDay				DI(IntDay di) const;
		virtual size_t 				II(pesa::IntDay di, size_t ii) const;
		virtual void				copyElement(const Data* src, IntDay diSrc, size_t iiSrc, IntDay diDst, size_t iiDst, size_t kkSrc = 0, size_t kkDst = 0);
		virtual void				copyRow(const Data* src, IntDay diSrc, IntDay diDst, size_t kkSrc = 0, size_t kkDst = 0);
		virtual std::shared_ptr<Data> extractRow(IntDay di, size_t kk = 0);

		virtual bool				doesCustomReadWrite() const;

		virtual pesa::io::InputStream& read(pesa::io::InputStream& is);
		virtual pesa::io::OutputStream& write(pesa::io::OutputStream& os);

		virtual pesa::io::InputStream& readRow(pesa::io::InputStream& is, size_t kk, size_t di);
		virtual pesa::io::OutputStream& writeRow(pesa::io::OutputStream& os, size_t kk, size_t di);

		virtual void				setMetadata(std::shared_ptr<Data> metadata);
		virtual std::shared_ptr<Data> getMetadata();

		virtual void				postMemcpy();
		virtual Data*				optimiseFor(IntDay diStart, IUniverse* srcUniverse, IUniverse* dstUniverse);
		virtual bool				isVariableDepth() const;

		template <class t_type>
		const t_type* 				dataPtr() const {
			return reinterpret_cast<t_type*>(data());
		}

		template <class t_type>
		t_type* 					dataPtr() {
			return reinterpret_cast<t_type*>(data());
		}

		//////////////////////////////////////////////////////////////////////////
		/// Debugging related 
		//////////////////////////////////////////////////////////////////////////
		virtual void 				debugDump(const pesa::RuntimeData& rt, const pesa::DumpInfo& dumpInfo) {}

		////////////////////////////////////////////////////////////////////////////
		/// Inline functions
		////////////////////////////////////////////////////////////////////////////
		//inline size_t				numChunks() const { return m_chunks.size(); }
		//inline DataPtr				chunk(size_t i) const { ASSERT(i < m_chunks.size(), "Invalid chunk read index requested: " << i); return m_chunks[i]; }
		//inline DataPtr&				chunk(size_t i) { ASSERT(i < m_chunks.size(), "Invalid chunk write index requested: " << i); return m_chunks[i]; }
		//inline const DataPtrVec&	chunks() const { return m_chunks; }
		//inline DataPtrVec&			chunks() { return m_chunks; }

		inline IntDay&				diOffset() { return m_diOffset; }
		inline IntDay				diOffset() const { return m_diOffset; }
		inline void					clearUniverse() { m_universe = nullptr; }
		inline IUniverse*&			universe() { return m_universe; }
		inline IUniverse*			universe() const { return m_universe; }
		inline const DataDefinition& def() const { return m_def; }
		inline std::string			name() const { return std::string(m_def.name); }
		inline void					setName(const std::string& name) { m_def.setName(name);}
		inline size_t 				II(size_t ii) const { return II(0, ii); }
		inline bool					namedArgument() const { return m_namedArgument; }
		inline bool&				namedArgument() { return m_namedArgument; }
		inline SizeVecPtr			diLookup() const { return m_diLookup; }
		inline SizeVecPtr&			diLookup() { return m_diLookup; }
		inline IntDay&				delay() { return m_delay; }
		inline IntDay				delay() const { return m_delay; }
		inline bool					applyDelay() const { return m_applyDelay; }
		inline bool&				applyDelay() { return m_applyDelay; }
		inline DataRowHeaderVec&	rowInfos() { return m_rowInfos; }
		inline const DataRowHeaderVec& rowInfos() const { return m_rowInfos; }
		inline SharedMemoryPtr		shmemPtr() const { return m_shmem; }
		inline SharedMemoryPtr&		shmemPtr() { return m_shmem; }
		inline size_t				rowSize() const { return this->cols() * this->itemSize(); }

		////////////////////////////////////////////////////////////////////////////
		/// SCRIPT helper functions - DO NOT USE IN C++
		////////////////////////////////////////////////////////////////////////////
		inline Data* script_sptr() {
			return static_cast<Data*>(this);
		}

		////////////////////////////////////////////////////////////////////////////
		/// Static functions
		////////////////////////////////////////////////////////////////////////////
		template <class t_type>
		static inline std::shared_ptr<t_type> cast(std::shared_ptr<Data>& ptr) {
			if (!ptr)
				return nullptr;
			return std::dynamic_pointer_cast<t_type>(ptr);
		}

		//////////////////////////////////////////////////////////////////////////
		/// HELPER
		//////////////////////////////////////////////////////////////////////////
		FloatMatrixData&			asFloat();
		DoubleMatrixData&			asDouble();
		StringData&					asString();
		IntMatrixData&				asInt();
		Int64MatrixData&			asInt64();
	};

	typedef std::shared_ptr<Data>	DataPtr;
	typedef std::vector<DataPtr>	DataPtrVec;

	////////////////////////////////////////////////////////////////////////////
	/// DataFrame: Represents one data frame!
	////////////////////////////////////////////////////////////////////////////
	struct Framework_API DataFrame {
		DataPtr 				data;
		size_t 					dataOffset;
		size_t 					dataSize;
		bool 					noDataUpdate = false;
		//bool					usePrevData = false;
		IntDay					di = Constants::s_invalidDay;

		DataPtr					customMetadata;					/// The custom metadata that's available and built incrementally.
																/// This can be written to on a per frame basis and is available
																/// with incremental updates on each subsequent Data update calls

								DataFrame(DataPtr data_ = nullptr, size_t dataOffset_ = 0, size_t dataSize_ = 0);
                                ~DataFrame();

		void 					reset();
		void					setData(Data* data);
		// void 					set(Data* data, size_t dataOffset, size_t dataSize);
		// void 					copy(Data* data, size_t dataOffset, size_t dataSize);
	};
} /// namespace pesa 

