/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// SecurityData.cpp
///
/// Created on: 15 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Dataset.h"

namespace pesa {

	////////////////////////////////////////////////////////////////////////////
	/// Metadata
	////////////////////////////////////////////////////////////////////////////
	MetadataItem::MetadataItem(const std::string& key, const std::string& value) {
		ASSERT(key.length() < s_maxKey - 1, "Metadata key overflow: " << key << ". Can have a max length of: " << s_maxKey - 1);
		ASSERT(value.length() < s_maxValue - 1, "Metadata value overflow: " << value << ". Can have a max length of: " << s_maxValue - 1);

		strncpy(this->key, key.c_str(), sizeof(this->key));
		strncpy(this->value, value.c_str(), sizeof(this->value));
	}

	Metadata::Metadata() : numItems(0) {
		memset(items, 0, sizeof(items));
	}

	Metadata::Metadata(const Metadata& rhs) {
		*this = rhs;
	}

	Metadata& Metadata::operator = (const Metadata& rhs) {
		memcpy(items, rhs.items, sizeof(items));
		numItems = rhs.numItems;
		return *this;
	}

	Metadata& Metadata::set(const std::string& key, const std::string& value) {
		ASSERT(numItems < s_maxItems, "Metadata overflow! Already reached max items: " << s_maxItems);
		items[numItems++] = MetadataItem(key, value);
		return *this;
	}

	Metadata& Metadata::addItem(const MetadataItem& item) {
		ASSERT(numItems < s_maxItems, "Metadata overflow! Already reached max items: " << s_maxItems);
		items[numItems++] = item;
		return *this;
	}

	int Metadata::getInt(const std::string& key, int defValue /* = 0 */) const {
		std::string value = get(key);

		if (value.empty())
			return defValue;

		return Util::cast<int>(value);
	}

	float Metadata::getFloat(const std::string& key, float defValue /* = 0.0f */) const {
		std::string value = get(key);

		if (value.empty())
			return defValue;

		return Util::cast<float>(value);
	}

	std::string Metadata::get(const std::string& key) const {
		for (const MetadataItem& item : items) {
			if (std::string(item.key) == key)
				return item.value;
		}

		return "";
	}

	void Metadata::clear() {
		numItems = 0;
		memset(items, 0, sizeof(items));
	}

	size_t Metadata::size() const {
		return numItems;
	}

	////////////////////////////////////////////////////////////////////////////
	/// DataCacheInfo
	////////////////////////////////////////////////////////////////////////////
	DataCacheInfo::DataCacheInfo(const RuntimeData& rt_, const Dataset& ds_, const DataValue& dv_, const DataDefinition& def_, 
		IntDay di_, IntDay diStart_, IntDay diEnd_, const ConfigSection& config_, IUniverse* universe_, 
		bool finalise_, void* privateInfo_, IUniverse* secSrcUniverse_)
		: rt(rt_)
		, ds(ds_)
		, dv(dv_)
		, def(def_)
		, di(di_)
		, diStart(diStart_)
		, diEnd(diEnd_)
		, config(config_)
		, universe(universe_)
		, finalise(finalise_)
		, privateInfo(privateInfo_)
		, secSrcUniverse(secSrcUniverse_ ? secSrcUniverse_ : universe_) {
	}

	std::string DataCacheInfo::dataName() const {
		return def.toString(ds);
	}

	////////////////////////////////////////////////////////////////////////////
	/// DataValue
	////////////////////////////////////////////////////////////////////////////
	DataValue::DataValue(const std::string& name, DataType::Type itemType, size_t itemSize, UIntCount frequency, unsigned int flags, const char* typeName /* = nullptr */)
		: definition(name, itemType, itemSize, frequency, flags, typeName) {
		if (!itemSize)
			this->definition.itemSize = DataType::size(itemType);
	}

	DataValue::DataValue(const std::string& name, DataType::Type itemType, size_t itemSize, Callback callback, UIntCount frequency, unsigned int flags, void* customInfo_ /* = nullptr */, const char* typeName /* = nullptr */)
		: definition(name, itemType, itemSize, frequency, flags, typeName)
		, customInfo(customInfo_) {
		if (!itemSize)
			this->definition.itemSize = DataType::size(itemType);
		this->callback = callback;
	}

	DataValue::DataValue(const DataDefinition& definition) {
		this->definition = definition;
		if (!this->definition.itemSize)
			this->definition.itemSize = DataType::size(this->definition.itemType);
	}
	
	std::string DataValue::toString(const Dataset& ds) const {
		return definition.toString(ds);
	}

	////////////////////////////////////////////////////////////////////////////
	/// Dataset
	////////////////////////////////////////////////////////////////////////////
	Dataset::Dataset() {
	}
	
	Dataset::Dataset(const std::string& name) {
		this->name = name;
	}

	Dataset::Dataset(const std::string& name, DataValueVec values) {
		this->name = name;
		this->values = values;
	}

	void Dataset::add(const DataValue& dv) {
		this->values.push_back(dv);
	}

	////////////////////////////////////////////////////////////////////////////
	/// Datasets
	////////////////////////////////////////////////////////////////////////////
	void Datasets::add(const Dataset& ds) {
		this->datasets.push_back(ds);
	}
} /// namespace pesa 
