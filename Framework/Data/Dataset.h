/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Dataset.h
///
/// Created on: 15 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include <memory>
#include <vector>
#include <unordered_map>
#include <map>
#include <functional>

#include "Data.h"

namespace pesa {

	class IDataRegistry;

	////////////////////////////////////////////////////////////////////////////
	/// Metadata: Metadata that the IDataLoader can associate with each of 
	/// the datasets that it has. 
	/// HAS TO BE A FIXED SIZED STRUCT!
	////////////////////////////////////////////////////////////////////////////

	/// Not a NESTED struct inside Metadata because SWIG does not support it yet!
	struct Framework_API MetadataItem {
        static const size_t 	s_maxKey = 8;
        static const size_t 	s_maxValue = 16;

        char 				    key[s_maxKey] = {'\0'};	/// Item key
		char 				    value[s_maxValue] = {'\0'};	/// Item value

								MetadataItem() {}
							    MetadataItem(const std::string& key, const std::string& value);
	};

	struct Framework_API Metadata {
		static const size_t 	s_maxItems = 16;

		MetadataItem			items[s_maxItems];
		size_t 					numItems = 0;

								Metadata();
								Metadata(const Metadata& rhs);

		Metadata& 				operator = (const Metadata& rhs);

		Metadata&				set(const std::string& key, const std::string& value);
		Metadata& 				addItem(const MetadataItem& item);
		std::string 			get(const std::string& key) const;
		int 					getInt(const std::string& key, int defValue = 0) const;
		float					getFloat(const std::string& key, float defValue = 0.0f) const;
		void 					clear();
		size_t 					size() const;

		template <typename T>
		Metadata&				set(const std::string& key, T value) {
			std::string svalue = Util::cast(value);
			return set(key, svalue);
		}
	};

	////////////////////////////////////////////////////////////////////////////
	/// DataCacheInfo: Caching info for a particular Dataset::DataValue
	////////////////////////////////////////////////////////////////////////////
	struct DataValue;
	struct Dataset;
	class Data;

	struct Framework_API DataCacheInfo {
		const RuntimeData& 		rt;				/// The Runtime data information provided by the IPipelineHandler
		const Dataset& 			ds;				/// The parent dataset of which the current definition is a child of
		const DataValue&		dv;				/// The DataValue that is currently being fetched
		const DataDefinition& 	def;			/// The definition of the dataset
		IntDay					di;				/// The current day for which the data is being loaded (this is indexed into rt.dates)
		IntDay 					diStart;		/// The starting day for which the data will be loaded (this is indexed into rt.dates)
		IntDay 					diEnd;			/// The last day for which the data will be loaded (this is indexed into rt.dates)
		const ConfigSection& 	config;			/// The IDataLoader config

		Metadata 				metadata; 		/// Metadata that the IDataLoader can associate with its data. This is saved 
												/// in the data cache and can be retrieved at a latter stage. 
												/// NOTE: This is only picked up after the postBuild function

		IUniverse* 				universe; 		/// The universe for which the data is being created

		bool 					finalise;		/// Is this the final call in the data update loop

		DataFrame				lastFrame;		/// Last good data. This is loaded from the cache on the very first day 

		void* 					privateInfo;	/// Private data from the DataRegistry. Never touch!
		IUniverse*				secSrcUniverse;	/// The universe that is used for getting the source securities to query the data for

		std::shared_ptr<Data>	customMetadata = nullptr;	/// Custom metadata

		Data*					existingData = nullptr;		/// Existing data. This is loaded for datatypes with kPreLoadExisting flag set. 
															/// This is only available in the first loop. The IDataLoader is responsible for 
															/// caching this information, if it's required
		
		DataCacheInfo(const RuntimeData& rt_, const Dataset& ds_, const DataValue& dv, const DataDefinition& def_, 
			IntDay di_, IntDay diStart_, IntDay diEnd_, const ConfigSection& config_, IUniverse* universe, 
			bool finalise_, void* privateInfo_, IUniverse* secSrcUniverse = nullptr);

		std::string 			dataName() const;
	};

	////////////////////////////////////////////////////////////////////////////
	/// DataValue: Represents a partecular kind of data inside a Dataset
	////////////////////////////////////////////////////////////////////////////
	struct Framework_API DataValue {
		typedef std::function<void(IDataRegistry&, const DataCacheInfo&, DataFrame& frame)> Callback;

		DataDefinition			definition;
		Callback 				callback = nullptr;				/// The callback that we have
		void*					customInfo;						/// Any custom info that the user might want to attach

								DataValue(const std::string& name, DataType::Type itemType, size_t itemSize, UIntCount frequency, unsigned int flags, const char* typeName = nullptr);
								DataValue(const std::string& name, DataType::Type itemType, size_t itemSize, Callback callback, UIntCount frequency, unsigned int flags, void* customInfo = nullptr, const char* typeName = nullptr);
								DataValue(const DataDefinition& definition);
								
		std::string 			toString(const Dataset& ds) const;
	};

	typedef std::vector<DataValue> DataValueVec;

	////////////////////////////////////////////////////////////////////////////
	/// Dataset: Represents a dataset with multiple (or just one) value(s) in it
	////////////////////////////////////////////////////////////////////////////
	struct Framework_API Dataset {
		std::string 			name;
		DataValueVec 			values;
		bool					flushDataset = false;

								Dataset();
								Dataset(const std::string& name);
								Dataset(const std::string& name, DataValueVec values);

		void 					add(const DataValue& dv);
	};
	
	/// This wrapper is solely there for SWIG. Cannot get raw vector to get exposed properly 
	/// and get weird issues. This part of SWIG is poorly documented. Even if I could get 
	/// it to work, it may become problem with other vectors in the future. So the best 
	/// way would be to expose vectors as minimal wrappers 
	struct Framework_API Datasets {
		std::vector<Dataset>	datasets;

								~Datasets() {}

		void 					add(const Dataset& ds); 				
		inline size_t 			size() const { return datasets.size(); }
	};
	
} /// namespace pesa 

#define DATA_FUNC(object, function)	std::bind(&function, std::ref(object), std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)
#define THIS_DATA_FUNC(function)	DATA_FUNC(*this, function)

