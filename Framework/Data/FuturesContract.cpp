/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// CustomData.cpp
///
/// Created on: 19 Jun 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "FuturesContract.h"

namespace pesa {
namespace fut {
	io::InputStream& Contract::read(io::InputStream& is) {
		PESA_READ_CVAR(is, name);
		PESA_READ_CVAR(is, issueDate);
		PESA_READ_CVAR(is, expiryDate);
		PESA_READ_CVAR(is, deliveryDate);

		PESA_READ_CVAR(is, dates);
		PESA_READ_CVAR(is, open);
		PESA_READ_CVAR(is, high);
		PESA_READ_CVAR(is, low);
		PESA_READ_CVAR(is, close);
		PESA_READ_CVAR(is, volume);
		PESA_READ_CVAR(is, interest);
		PESA_READ_CVAR(is, totalVolume);
		PESA_READ_CVAR(is, interestAll);
		PESA_READ_CVAR(is, expired);

		return is;
	}

	io::OutputStream& Contract::write(io::OutputStream& os) { 
		PESA_WRITE_CVAR(os, name);
		PESA_WRITE_CVAR(os, issueDate);
		PESA_WRITE_CVAR(os, expiryDate);
		PESA_WRITE_CVAR(os, deliveryDate);

		PESA_WRITE_CVAR(os, dates);
		PESA_WRITE_CVAR(os, open);
		PESA_WRITE_CVAR(os, high);
		PESA_WRITE_CVAR(os, low);
		PESA_WRITE_CVAR(os, close);
		PESA_WRITE_CVAR(os, volume);
		PESA_WRITE_CVAR(os, interest);
		PESA_WRITE_CVAR(os, totalVolume);
		PESA_WRITE_CVAR(os, interestAll);
		PESA_WRITE_CVAR(os, expired);

		return os;
	}

	IntDay Contract::dateIndex(IntDate date) const {
		ASSERT(m_dates.size() > 0, "Empty dates array for contract: " << m_name);

		auto p = std::equal_range(m_dates.begin(), m_dates.end(), (int)date);

		if (p.first == m_dates.end())
			return Constants::s_invalidDay;
		else if (p.first == m_dates.end())
			return Constants::s_invalidDay;

		return (IntDay)(p.first - m_dates.begin());
	}

	IntDate Contract::date(IntDate date) const {
		auto di = dateIndex(date);
		ASSERT(di >= 0 && di < (IntDay)m_dates.size(), "Invalid index returned for date: " << date << " - Contract: " << m_name);
		return m_dates[di];
	}

	float Contract::open(IntDate date) const {
		auto di = dateIndex(date);
		ASSERT(di >= 0 && di < (IntDay)m_dates.size(), "Invalid index returned for date: " << date << " - Contract: " << m_name);
		return m_open[di];
	}

	float Contract::high(IntDate date) const {
		auto di = dateIndex(date);
		ASSERT(di >= 0 && di < (IntDay)m_dates.size(), "Invalid index returned for date: " << date << " - Contract: " << m_name);
		return m_high[di];
	}

	float Contract::low(IntDate date) const {
		auto di = dateIndex(date);
		ASSERT(di >= 0 && di < (IntDay)m_dates.size(), "Invalid index returned for date: " << date << " - Contract: " << m_name);
		return m_low[di];
	}

	float Contract::close(IntDate date) const {
		auto di = dateIndex(date);
		ASSERT(di >= 0 && di < (IntDay)m_dates.size(), "Invalid index returned for date: " << date << " - Contract: " << m_name);
		return m_close[di];
	}

	float Contract::volume(IntDate date) const {
		auto di = dateIndex(date);
		ASSERT(di >= 0 && di < (IntDay)m_dates.size(), "Invalid index returned for date: " << date << " - Contract: " << m_name);
		return m_volume[di];
	}

	float Contract::interest(IntDate date) const {
		auto di = dateIndex(date);
		ASSERT(di >= 0 && di < (IntDay)m_dates.size(), "Invalid index returned for date: " << date << " - Contract: " << m_name);
		return m_interest[di];
	}

	float Contract::totalVolume(IntDate date) const {
		auto di = dateIndex(date);
		ASSERT(di >= 0 && di < (IntDay)m_dates.size(), "Invalid index returned for date: " << date << " - Contract: " << m_name);
		return m_totalVolume[di];
	}

	float Contract::interestAll(IntDate date) const {
		auto di = dateIndex(date);
		ASSERT(di >= 0 && di < (IntDay)m_dates.size(), "Invalid index returned for date: " << date << " - Contract: " << m_name);
		return m_interestAll[di];
	}

	void Contract::makeInvalid() {
		m_name.clear();
	}

	bool Contract::isValid() const {
		return !m_name.empty();
	}

	std::string Contract::toString() const {
		return m_name;
	}

	void Contract::fromString(const std::string& rhs) {
		/// NO-OP
	}

	Poco::DateTime Contract::guessExpiry(const std::string& name) {
		/// Get the last 4 characters of the contract name
		char code = name[name.length() - 1];
		std::string expiryYear = name.substr(name.length() - 5, 4);
		Futures::Month month = Futures::parse(code);
		int year = Util::cast<int>(expiryYear);
		Poco::DateTime expiryDate(year, (int)month + 1, Poco::DateTime::daysOfMonth(year, month + 1));

		return expiryDate;
	}

	Poco::DateTime Contract::getMaxExpiryDate() const {
		return Contract::guessExpiry(m_name);
	}

	void Contract::checkExpired(const Poco::DateTime& today) {
		auto expiryDate = getMaxExpiryDate();
		if (expiryDate < today)
			m_expired = true;
	}

	//////////////////////////////////////////////////////////////////////////
	ContractData::ContractData() 
		: StreamData<Contract>(nullptr, DataDefinition("FuturesContractData", DataType::kCustom, 0, DataFrequency::kDaily, 0)) {
		Data::m_def.setTypeName("FuturesContractData");
	}

	ContractData::ContractData(IUniverse* universe, DataDefinition def)
		: StreamData<Contract>(universe, def) {
		Data::m_def.setTypeName("FuturesContractData");
	}

	Contract& ContractData::contract(IntDay di, size_t ii, size_t kk) {
		return StreamData<Contract>::itemT(di, ii, kk);
	}

	const Contract& ContractData::contract(IntDay di, size_t ii, size_t kk) const {
		return StreamData<Contract>::itemT(di, ii, kk);
	}

	//////////////////////////////////////////////////////////////////////////
	io::InputStream& ActiveContracts::read(io::InputStream& is) {
		size_t size;

		is.read("size", &size);

		if (size) {
			m_contracts.resize(size);

			for (size_t ci = 0; ci < size; ci++)
				is.read(("contract_" + Util::cast(ci)).c_str(), &m_contracts[ci]);
		}

		return is;
	}

	io::OutputStream& ActiveContracts::write(io::OutputStream& os) {
		size_t size = m_contracts.size();
		os.write("size", size);

		for (size_t ci = 0; ci < size; ci++)
			os.write(("contract_" + Util::cast(ci)).c_str(), m_contracts[ci]);

		return os;
	}

	void ActiveContracts::makeInvalid() {
		m_contracts.clear();
	}

	bool ActiveContracts::isValid() const {
		return !!m_contracts.size();
	}

	std::string ActiveContracts::toString() const {
		return "";
	}

	void ActiveContracts::fromString(const std::string& rhs) {
		/// NO-OP
	}

	void ActiveContracts::buildContractsLUT() const {
		if (m_contractsLUT.size() == m_contracts.size())
			return;

		for (size_t ci = 0; ci < m_contracts.size(); ci++) 
			m_contractsLUT[m_contracts[ci].name()] = ci;
	}

	Contract* ActiveContracts::chainContracts(const RuntimeData& rt, int rollDays) const {
		/// The contract chain has already been established
		if (m_chain || !m_contracts.size())
			return m_chain;

		auto rollSpan					= Util::daysSpan(rollDays);
		Contract* prev					= nullptr;
		auto& sortedContracts			= getSortedContracts();

		for (size_t sci = 0; sci < sortedContracts.size(); sci++) {
			size_t ci					= sortedContracts[sci];
			auto* contract				= &m_contracts[ci];
			auto expiryDate				= Util::intToDate(contract->expiryDate());
			auto liquidateDate			= expiryDate - rollSpan;

			contract->liquidateDate()	= Util::dateToInt(liquidateDate);

			if (prev)
				prev->next()			= contract;

			const auto& cdates			= contract->dates(); 
			auto diStart				= rt.getDateIndex(cdates[0]);
			contract->diEnd()			= rt.getDateIndex(cdates[cdates.size() - 1]);

			/// If this is zero when then we need to see exactly which date corresponds to the start of the simulation
			/// because the getDateIndex returns 0 for all dates that are less than the start of the simulation date.
			/// In could be that the contract that we're looking at started before the start of the simulation date
			/// in which case, the first date would return 0
			if (diStart == 0) { 
				IntDay diOffset = 0;
				auto startDate = rt.dates[0];

				while (diOffset < (IntDay)cdates.size() && cdates[diOffset] < startDate)
					diOffset++;

				if (diOffset >= (IntDay)cdates.size()) {
					diStart				= 0;
					contract->diEnd()	= 0;
					contract->diOffset()= 0;
				}
				else
					contract->diOffset()= diOffset;
			}
			
			contract->diStart()			= diStart;
			contract->prev()			= prev;
			prev						= contract;
		}

		m_chain							= &m_contracts[sortedContracts[0]];

		return m_chain;
	}

	const Contract* ActiveContracts::getFrontContract(const RuntimeData& rt, const Contract* iter_, IntDate date_, int rollDays) const {
		Poco::DateTime date				= Util::intToDate(date_);
		const Contract* iter			= iter_ ? iter_ : chainContracts(rt, rollDays);
		int deltaDays					= 999999;
		int prevDeltaDays				= 999999;

		while (iter) {
			auto liquidateDate			= Util::intToDate(iter->liquidateDate());

			if (liquidateDate > date) 
				return iter;

			iter						= iter->next();
		}

		return nullptr;
	}

	const Contract* ActiveContracts::findContract(const std::string& contractName) const {
		buildContractsLUT();

		auto ciIter = m_contractsLUT.find(contractName);

		if (ciIter == m_contractsLUT.end())
			return nullptr;

		auto ci = ciIter->second;
		ASSERT(ci < m_contracts.size(), "Invalid lookup index for contract: " << contractName << " [Index: " << ci << "]");

		return &m_contracts[ci];
	}

	void ActiveContracts::addContract(const Contract& contract) {
		m_contracts.push_back(contract);
	}

	const SizeVec& ActiveContracts::getSortedContracts() const {
		/// If we have constructed the sorted list before then there's no need to do it again ...
		if (m_sortedContracts.size() == m_contracts.size())
			return m_sortedContracts;

		/// Otherwise we just get the sorted contracts list. Sorted based on ascending expiry date
		m_sortedContracts.resize(m_contracts.size());

		for (size_t ci = 0; ci < m_contracts.size(); ci++)
			m_sortedContracts[ci] = ci;

		std::sort(m_sortedContracts.begin(), m_sortedContracts.end(), [&](auto&& lhsIndex, auto&& rhsIndex) -> bool {
			const auto& lhs = m_contracts[lhsIndex];
			const auto& rhs = m_contracts[rhsIndex];
			return lhs.expiryDate() < rhs.expiryDate();
		});

		return m_sortedContracts;
	}

	//////////////////////////////////////////////////////////////////////////
	ActiveContractsData::ActiveContractsData()
		: StreamData<ActiveContracts>(nullptr, DataDefinition(ActiveContractsData::typeName(), DataType::kCustom, 0, DataFrequency::kDaily, 0)) {
		Data::m_def.setTypeName(ActiveContractsData::typeName());
	}

	ActiveContractsData::ActiveContractsData(IUniverse* universe, DataDefinition def)
		: StreamData<ActiveContracts>(universe, def) {
		Data::m_def.setTypeName(ActiveContractsData::typeName());
	}

	ActiveContracts& ActiveContractsData::activeContracts(IntDay di, size_t ii, size_t kk) {
		return StreamData<ActiveContracts>::itemT(di, ii, kk);
	}

	const ActiveContracts& ActiveContractsData::activeContracts(IntDay di, size_t ii, size_t kk) const {
		return StreamData<ActiveContracts>::itemT(di, ii, kk);
	}

} /// namespace fut
} /// namespace pesa 

EXPORT_FUNCTION pesa::Data* createFuturesContractData(pesa::IUniverse* universe, pesa::DataDefinition def, const pesa::ConfigSection& config) {
	return new pesa::fut::ContractData(universe, def);
}

EXPORT_FUNCTION pesa::Data* createFuturesActiveContractsData(pesa::IUniverse* universe, pesa::DataDefinition def, const pesa::ConfigSection& config) {
	return new pesa::fut::ActiveContractsData(universe, def);
}
