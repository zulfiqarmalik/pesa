/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// CustomData.h
///
/// Created on: 19 Jun 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/FrameworkDefs.h"
#include "StreamData.h"

namespace pesa {
namespace fut {
	//////////////////////////////////////////////////////////////////////////
	/// FuturesContract: Defines one Futures contract!
	//////////////////////////////////////////////////////////////////////////
	class Framework_API Contract : public StreamDataItem {
	protected:
		std::string				m_name;					/// The name of the contract
		IntDate					m_issueDate = 0;		/// The date the contract was issued
		IntDate					m_expiryDate = 0;		/// The date the contract will expire
		IntDate					m_deliveryDate = 0;		/// The date the contract is due to be delivered

		//////////////////////////////////////////////////////////////////////////
		/// Daily data for the contract!
		//////////////////////////////////////////////////////////////////////////
		IntVec					m_dates;				/// The trading dates of the contract
		FloatVec				m_open;					/// The open
		FloatVec				m_high;					/// The high
		FloatVec				m_low;					/// The low
		FloatVec				m_close;				/// The close
		FloatVec				m_volume;				/// The volume
		FloatVec				m_interest;				/// The interest
		FloatVec				m_totalVolume;			/// The totalVolume
		FloatVec				m_interestAll;			/// The interestAll

		bool					m_expired = false;		/// Whether the contract has expired or not

		//////////////////////////////////////////////////////////////////////////
		/// TRIANSIENT! Information below IS NOT saved to the stream
		//////////////////////////////////////////////////////////////////////////
		IntDate					m_liquidateDate = 0;	/// Chain only: The date when we're going to liquidate this contract
		Contract*				m_next = nullptr;		/// Chain only: This is the next contract in the chain
		Contract*				m_prev = nullptr;		/// Chain only: This is the previous contract in the chain

		IntDay					m_diOffset = 0;			/// The starting offset in the di array
		IntDay					m_diStart = 0;			/// Index of the first date in the dates array
		IntDay					m_diEnd = 0;			/// Index of the first day in the dates array

		IntDay					dateIndex(IntDate date) const;

	public:
#ifndef __SCRIPT__
		io::InputStream&		read(io::InputStream& is);
		io::OutputStream&		write(io::OutputStream& os);
#endif

		virtual void			makeInvalid();
		virtual bool			isValid() const;
		virtual std::string		toString() const;
		virtual void			fromString(const std::string& rhs);

		Poco::DateTime			getMaxExpiryDate() const;
		void					checkExpired(const Poco::DateTime& today);

		static Poco::DateTime	guessExpiry(const std::string& name);

		IntDate					date(IntDate date) const;
		float					open(IntDate date) const;
		float 					high(IntDate date) const;
		float 					low(IntDate date) const;
		float 					close(IntDate date) const;
		float 					volume(IntDate date) const;
		float 					interest(IntDate date) const;
		float 					totalVolume(IntDate date) const;
		float 					interestAll(IntDate date) const;

		//////////////////////////////////////////////////////////////////////////
		/// Inline functions
		//////////////////////////////////////////////////////////////////////////
		std::string&			name() { return m_name; }
		const std::string&		name() const { return m_name; }

		IntDate&				issueDate() { return m_issueDate; }
		const IntDate&			issueDate() const { return m_issueDate; }

		IntDate&				expiryDate() { return m_expiryDate; }
		const IntDate&			expiryDate() const { return m_expiryDate; }

		IntDate&				deliveryDate() { return m_deliveryDate; }
		const IntDate&			deliveryDate() const { return m_deliveryDate; }

		IntVec&					dates() { return m_dates; }
		const IntVec&			dates() const { return m_dates; }

		FloatVec&				open() { return m_open; }
		const FloatVec&			open() const { return m_open; }

		FloatVec&				high() { return m_high; }
		const FloatVec&			high() const { return m_high; }

		FloatVec&				low() { return m_low; }
		const FloatVec&			low() const { return m_low; }

		FloatVec&				close() { return m_close; }
		const FloatVec&			close() const { return m_close; }

		FloatVec&				volume() { return m_volume; }
		const FloatVec&			volume() const { return m_volume; }

		FloatVec&				interest() { return m_interest; }
		const FloatVec&			interest() const { return m_interest; }

		FloatVec&				totalVolume() { return m_totalVolume; }
		const FloatVec&			totalVolume() const { return m_totalVolume; }

		FloatVec&				interestAll() { return m_interestAll; }
		const FloatVec&			interestAll() const { return m_interestAll; }

		bool&					expired() { return m_expired; }
		const bool&				expired() const { return m_expired; }

		Contract*&				next() { return m_next; }
		const Contract*			next() const { return m_next; }

		Contract*&				prev() { return m_prev; }
		const Contract*			prev() const { return m_prev; }

		IntDate&				liquidateDate() { return m_liquidateDate; }
		IntDate					liquidateDate() const { return m_liquidateDate; }

		IntDay&					diStart() { return m_diStart; }
		IntDay					diStart() const { return m_diStart; }

		IntDay&					diEnd() { return m_diEnd; }
		IntDay					diEnd() const { return m_diEnd; }

		IntDay&					diOffset() { return m_diOffset; }
		IntDay					diOffset() const { return m_diOffset; }
	};

	typedef std::vector<const Contract*> ContractCPVec;
	typedef std::vector<Contract> ContractVec;
	typedef std::shared_ptr<Contract> ContractPtr;

	//////////////////////////////////////////////////////////////////////////
	/// FuturesContractData: FuturesContractData implementation 
	//////////////////////////////////////////////////////////////////////////
	class Framework_API ContractData : public StreamData<Contract> {
	public:
								ContractData();
								ContractData(IUniverse* universe, DataDefinition def);

		Contract&				contract(IntDay di, size_t ii, size_t kk = 0);
		const Contract&			contract(IntDay di, size_t ii, size_t kk = 0) const;

		////////////////////////////////////////////////////////////////////////////
		/// SCRIPT helper functions - DO NOT USE IN C++
		////////////////////////////////////////////////////////////////////////////
		inline Data* script_sptr() {
			return static_cast<Data*>(this);
		}

		inline static ContractData* script_cast(Data* data) {
			return dynamic_cast<ContractData*>(data);
		}
	};

	typedef std::shared_ptr<ContractData>	FuturesContractDataPtr;

	//////////////////////////////////////////////////////////////////////////
	/// ActiveContracts: Defines the active futures contracts
	//////////////////////////////////////////////////////////////////////////
	class Framework_API ActiveContracts : public StreamDataItem {
	protected:
		typedef std::unordered_map<std::string, size_t> ContractMap;

		mutable ContractVec		m_contracts;			/// All the active contracts
		mutable SizeVec			m_sortedContracts;		/// Contracts sorted on expiry date
		mutable ContractMap		m_contractsLUT;			/// Lookup table for the contracts
		mutable Contract*		m_chain = nullptr;		/// Contract chain. This points to the head of the contracts chain

		void					buildContractsLUT() const;
		Contract*				chainContracts(const RuntimeData& rt, int rollDays) const;

	public:
#ifndef __SCRIPT__
		io::InputStream&		read(io::InputStream& is);
		io::OutputStream&		write(io::OutputStream& os);
#endif

		virtual void			makeInvalid();
		virtual bool			isValid() const;
		virtual std::string		toString() const;
		virtual void			fromString(const std::string& rhs);

		const SizeVec&			getSortedContracts() const;
		const Contract*			getFrontContract(const RuntimeData& rt, const Contract* iter, IntDate date, int rollDays) const;
		const Contract*			findContract(const std::string& contractName) const;
		void					addContract(const Contract& contract);

		//////////////////////////////////////////////////////////////////////////
		/// Inline functions
		//////////////////////////////////////////////////////////////////////////
		inline size_t			size() const { return m_contracts.size(); }
		inline const Contract&	contract(size_t ci) const { return m_contracts[ci]; }
		inline Contract&		contract(size_t ci) { return m_contracts[ci]; }
		inline ContractVec&		contracts() { return m_contracts; }
		inline const ContractVec& contracts() const { return m_contracts; }
	};

	typedef std::shared_ptr<ActiveContracts> ActiveContractsPtr;

	//////////////////////////////////////////////////////////////////////////
	/// ActiveContractsData: Data for the ActiveContracts
	//////////////////////////////////////////////////////////////////////////
	class Framework_API ActiveContractsData : public StreamData<ActiveContracts> {
	public:
								ActiveContractsData();
								ActiveContractsData(IUniverse* universe, DataDefinition def);

		ActiveContracts&		activeContracts(IntDay di, size_t ii, size_t kk = 0);
		const ActiveContracts&	activeContracts(IntDay di, size_t ii, size_t kk = 0) const;

		////////////////////////////////////////////////////////////////////////////
		/// SCRIPT helper functions - DO NOT USE IN C++
		////////////////////////////////////////////////////////////////////////////
		inline Data* script_sptr() {
			return static_cast<Data*>(this);
		}

		inline static ActiveContractsData* script_cast(Data* data) {
			return dynamic_cast<ActiveContractsData*>(data);
		}

		static inline std::string typeName() {
			return "FuturesActiveContractsData";
		}
	};

} /// namespace fut
} /// namespace pesa 

