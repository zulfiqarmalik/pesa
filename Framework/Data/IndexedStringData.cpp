/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// IndexedStringData.cpp
///
/// Created on: 18 May 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "./IndexedStringData.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////// 
	/// ShortIndexedStringMetadata: SWIG Wrapper. DO NOT CHANGE!
	//////////////////////////////////////////////////////////////////////////
	ShortIndexedStringMetadata::ShortIndexedStringMetadata(IUniverse* universe, size_t numCols) : IndexedStringMetadata<uint16_t>(universe, numCols) {
	}

	ShortIndexedStringMetadata::ShortIndexedStringMetadata(IUniverse* universe, size_t numRows, size_t numCols) : IndexedStringMetadata<uint16_t>(universe, numRows, numCols) {
	}

	ShortIndexedStringMetadata::ShortIndexedStringMetadata(IUniverse* universe, size_t numCols, DataDefinition def) : IndexedStringMetadata<uint16_t>(universe, numCols, def) {
	}

	ShortIndexedStringMetadata::ShortIndexedStringMetadata(IUniverse* universe, size_t numRows, size_t numCols, DataDefinition def) : IndexedStringMetadata<uint16_t>(universe, numRows, numCols, def) {
	}

	ShortIndexedStringMetadata::ShortIndexedStringMetadata(IUniverse* universe, DataDefinition def) : IndexedStringMetadata<uint16_t>(universe, def) {
	}

	ShortIndexedStringMetadata::ShortIndexedStringMetadata(IUniverse* universe, const StringVec& rhs) : IndexedStringMetadata<uint16_t>(universe, rhs) {
	}

	ShortIndexedStringMetadata::ShortIndexedStringMetadata(IUniverse* universe, const StringVecVecVec& rhs) : IndexedStringMetadata<uint16_t>(universe, rhs) {
	}

	//////////////////////////////////////////////////////////////////////////
	/// LongIndexedStringMetadata: SWIG Wrapper. DO NOT CHANGE!
	//////////////////////////////////////////////////////////////////////////
	LongIndexedStringMetadata::LongIndexedStringMetadata(IUniverse* universe, size_t numCols) : IndexedStringMetadata<int>(universe, numCols) {
	}

	LongIndexedStringMetadata::LongIndexedStringMetadata(IUniverse* universe, size_t numRows, size_t numCols) : IndexedStringMetadata<int>(universe, numRows, numCols) {
	}

	LongIndexedStringMetadata::LongIndexedStringMetadata(IUniverse* universe, size_t numCols, DataDefinition def) : IndexedStringMetadata<int>(universe, numCols, def) {
	}

	LongIndexedStringMetadata::LongIndexedStringMetadata(IUniverse* universe, size_t numRows, size_t numCols, DataDefinition def) : IndexedStringMetadata<int>(universe, numRows, numCols, def) {
	}

	LongIndexedStringMetadata::LongIndexedStringMetadata(IUniverse* universe, DataDefinition def) : IndexedStringMetadata<int>(universe, def) {
	}

	LongIndexedStringMetadata::LongIndexedStringMetadata(IUniverse* universe, const StringVec& rhs) : IndexedStringMetadata<int>(universe, rhs) {
	}

	LongIndexedStringMetadata::LongIndexedStringMetadata(IUniverse* universe, const StringVecVecVec& rhs) : IndexedStringMetadata<int>(universe, rhs) {
	}

	//////////////////////////////////////////////////////////////////////////
	/// ShortIndexedStringData: SWIG Wrapper. DO NOT CHANGE!
	//////////////////////////////////////////////////////////////////////////
	ShortIndexedStringData::ShortIndexedStringData(IUniverse* universe, size_t numCols) : IndexedStringData<uint16_t, UShortMatrixData>(universe, numCols) {
	}

	ShortIndexedStringData::ShortIndexedStringData(IUniverse* universe, size_t numRows, size_t numCols) : IndexedStringData<uint16_t, UShortMatrixData>(universe, numRows, numCols) {
	}

	ShortIndexedStringData::ShortIndexedStringData(IUniverse* universe, size_t numCols, DataDefinition def) : IndexedStringData<uint16_t, UShortMatrixData>(universe, numCols, def) {
	}

	ShortIndexedStringData::ShortIndexedStringData(IUniverse* universe, size_t numRows, size_t numCols, DataDefinition def) : IndexedStringData<uint16_t, UShortMatrixData>(universe, numRows, numCols, def) {
	}

	ShortIndexedStringData::ShortIndexedStringData(IUniverse* universe, DataDefinition def) : IndexedStringData<uint16_t, UShortMatrixData>(universe, def) {
	}

	ShortIndexedStringData::ShortIndexedStringData(IUniverse* universe, const StringVec& rhs) : IndexedStringData<uint16_t, UShortMatrixData>(universe, rhs) {
	}

	ShortIndexedStringData::ShortIndexedStringData(IUniverse* universe, const StringVecVecVec& rhs) : IndexedStringData<uint16_t, UShortMatrixData>(universe, rhs) {
	}

	//ShortIndexedStringData::ShortIndexedStringData(IUniverse* universe, const StringVec& rhs, std::shared_ptr<Data> customMetadata) : IndexedStringData<uint16_t, UShortMatrixData>(universe, rhs, customMetadata) {
	//}

	//////////////////////////////////////////////////////////////////////////
	/// LongIndexedStringData: SWIG Wrapper. DO NOT CHANGE!
	//////////////////////////////////////////////////////////////////////////
	LongIndexedStringData::LongIndexedStringData(IUniverse* universe, size_t numCols) : IndexedStringData<int, IntMatrixData>(universe, numCols) {
	}

	LongIndexedStringData::LongIndexedStringData(IUniverse* universe, size_t numRows, size_t numCols) : IndexedStringData<int, IntMatrixData>(universe, numRows, numCols) {
	}

	LongIndexedStringData::LongIndexedStringData(IUniverse* universe, size_t numCols, DataDefinition def) : IndexedStringData<int, IntMatrixData>(universe, numCols, def) {
	}

	LongIndexedStringData::LongIndexedStringData(IUniverse* universe, size_t numRows, size_t numCols, DataDefinition def) : IndexedStringData<int, IntMatrixData>(universe, numRows, numCols, def) {
	}

	LongIndexedStringData::LongIndexedStringData(IUniverse* universe, DataDefinition def) : IndexedStringData<int, IntMatrixData>(universe, def) {
	}

	LongIndexedStringData::LongIndexedStringData(IUniverse* universe, const StringVec& rhs) : IndexedStringData<int, IntMatrixData>(universe, rhs) {
	}

	LongIndexedStringData::LongIndexedStringData(IUniverse* universe, const StringVecVecVec& rhs) : IndexedStringData<int, IntMatrixData>(universe, rhs) {
	}

	//LongIndexedStringData::LongIndexedStringData(IUniverse* universe, const StringVec& rhs, std::shared_ptr<Data> customMetadata) : IndexedStringData<int, IntMatrixData>(universe, rhs, customMetadata) {
	//}

	//////////////////////////////////////////////////////////////////////////
	/// LongIndexedStringData: SWIG Wrapper. DO NOT CHANGE!
	//////////////////////////////////////////////////////////////////////////
	VarLongIndexedStringData::VarLongIndexedStringData(IUniverse* universe, size_t numCols) : IndexedStringData<int, IntVarMatrixData>(universe, numCols) {
	}

	VarLongIndexedStringData::VarLongIndexedStringData(IUniverse* universe, size_t numRows, size_t numCols) : IndexedStringData<int, IntVarMatrixData>(universe, numRows, numCols) {
	}

	VarLongIndexedStringData::VarLongIndexedStringData(IUniverse* universe, size_t numCols, DataDefinition def) : IndexedStringData<int, IntVarMatrixData>(universe, numCols, def) {
	}

	VarLongIndexedStringData::VarLongIndexedStringData(IUniverse* universe, size_t numRows, size_t numCols, DataDefinition def) : IndexedStringData<int, IntVarMatrixData>(universe, numRows, numCols, def) {
	}

	VarLongIndexedStringData::VarLongIndexedStringData(IUniverse* universe, DataDefinition def) : IndexedStringData<int, IntVarMatrixData>(universe, def) {
	}

	VarLongIndexedStringData::VarLongIndexedStringData(IUniverse* universe, const StringVec& rhs) : IndexedStringData<int, IntVarMatrixData>(universe, rhs) {
	}

	VarLongIndexedStringData::VarLongIndexedStringData(IUniverse* universe, const StringVecVecVec& rhs) : IndexedStringData<int, IntVarMatrixData>(universe, rhs) {
	}

	DataPtr VarLongIndexedStringData::extractRow(IntDay di, size_t kk /* = 0 */) {
		auto ret = std::make_shared<VarLongIndexedStringData>(universe(), cols());
		ret->setMetadata(m_metadata);

		ASSERT(di < this->m_indexes.rows(), "Invalid di passed for extract row: " << di << " - Max row count: " << this->m_indexes.rows());

		ret->m_indexes.copyRow(&m_indexes, di, 0, kk, 0);

		return ret;
	}

	void VarLongIndexedStringData::copyElement(const Data* src_, IntDay diSrc, size_t iiSrc, IntDay diDst, size_t iiDst, size_t kkSrc /*= 0*/, size_t kkDst /*= 0*/) {
		std::string srcValue;
		auto* src = dynamic_cast<const StringData*>(src_);

		if (src) {
			//auto& lhs = m_indexes.item(diDst, iiDst, kkDst);
			//const auto& rhs = m_indexes
			auto depth = src->depthAt(diSrc, iiSrc);

			for (size_t kk = 0; kk < depth; kk++) {
				std::string value = src->getValueAt(diSrc, iiSrc, kk);
				this->setValueAt(diDst, iiDst, kk, value);
			}

			return;
		}
	}


} /// namespace pesa 

