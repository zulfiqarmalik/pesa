/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// IndexedStringData.h
///
/// Created on: 18 May 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "./StringData.h"
#include "./MatrixData.h"
#include "./VarMatrixData.h"
#include "Data.h"
#include "IndexedStringMetadata.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// IndexedStringData - Generic string data, but with optimised for storage
	/// and access. This uses a LUT to store unique values and then a 16-bit
	/// unsigned integer to index them. 
	////////////////////////////////////////////////////////////////////////////
	template <typename t_type, typename t_matrix>
	class IndexedStringData : public StringData {
	protected:
		typedef std::unordered_map<std::string, t_type> DataIndex;
		typedef IndexedStringMetadata<t_type> T_IndexedStringMetadata;
		typedef std::shared_ptr<T_IndexedStringMetadata> IndexedStringMetadataPtr;

		IndexedStringMetadataPtr	m_metadata = nullptr;		/// The metadata
		t_matrix					m_indexes;					/// The indexes of daily data

		void						init(size_t cols, size_t depth = 1, size_t rows = 1);
		void						resetMetadata();

	public:
									IndexedStringData(IUniverse* universe, size_t numCols);
									IndexedStringData(IUniverse* universe, size_t numRows, size_t numCols);
									IndexedStringData(IUniverse* universe, size_t numCols, DataDefinition def);
									IndexedStringData(IUniverse* universe, size_t numRows, size_t numCols, DataDefinition def);
									IndexedStringData(IUniverse* universe, DataDefinition def);
									IndexedStringData(IUniverse* universe, const StringVec& rhs);
									IndexedStringData(IUniverse* universe, const StringVecVecVec& rhs);
									//IndexedStringData(IUniverse* universe, const StringVec& rhs, std::shared_ptr<Data> customMetadata);

		virtual 					~IndexedStringData();

		virtual void				setMetadata(DataPtr metadata);
		virtual DataPtr				getMetadata();
		virtual Data*				clone(IUniverse* universe, bool noCopy);
		virtual Data*				createInstance();
		virtual void 				copyElement(const Data* src, IntDay diSrc, size_t iiSrc, IntDay diDst, size_t iiDst, size_t kkSrc = 0, size_t kkDst = 0);
		virtual void				copyRow(const Data* src, IntDay diSrc, IntDay diDst, size_t kkSrc = 0, size_t kkDst = 0);
		virtual void 				makeInvalid(IntDay di, size_t ii, size_t kk = 0);
		virtual bool 				isValid(IntDay di, size_t ii, size_t kk = 0) const;
		virtual const void* 		data() const;
		virtual void* 				data();
		virtual size_t 				dataSize() const;
		virtual void 				ensureSize(size_t rows, size_t cols, bool copyOld = false);
		virtual void				ensureSizeAndDepth(size_t rows, size_t cols, size_t depth, bool copyOld = false);
		virtual void				ensureDepthAt(IntDay di, size_t ii, size_t depth);
		virtual size_t				depthAt(IntDay di, size_t ii) const;

		virtual std::string			getString(pesa::IntDay di, size_t ii, size_t kk) const;
		virtual bool				doesCustomReadWrite() const;
		virtual void				invalidateAll();

		virtual size_t				rows() const;
		virtual size_t 				cols() const;
		virtual size_t				depth() const;
		virtual size_t				numItems() const;
		virtual size_t 				itemSize() const;

		virtual std::shared_ptr<Data> extractRow(IntDay di, size_t kk = 0);

		virtual void				setString(pesa::IntDay di, size_t ii, size_t kk, const std::string& value) { setValueAt(di, ii, kk, value); }
		virtual void 				setValue(IntDay di, size_t ii, const std::string& value);
		virtual void 				setValueAt(IntDay di, size_t ii, size_t kk, const std::string& value);
		virtual const std::string&	getValue(IntDay di, size_t ii) const;
		virtual const std::string&	getValueAt(IntDay di, size_t ii, size_t kk) const;
		virtual const std::string&	operator[](size_t ii) const;
		virtual bool				isVariableDepth() const;

#ifndef __SCRIPT__
		virtual io::InputStream&	read(pesa::io::InputStream& is) { return m_indexes.read(is); }
		virtual io::OutputStream&	write(pesa::io::OutputStream& os) { return m_indexes.write(os); }
		virtual io::InputStream&	readRow(pesa::io::InputStream& is, size_t kk, size_t di) { return m_indexes.readRow(is, kk, di); }
		virtual io::OutputStream&	writeRow(pesa::io::OutputStream& os, size_t kk, size_t di) { return m_indexes.writeRow(os, kk, di); }
#endif /// __SCRIPT__

		////////////////////////////////////////////////////////////////////////////
		/// SCRIPT helper functions - DO NOT USE IN C++
		////////////////////////////////////////////////////////////////////////////
		Data*						script_sptr() {
			return static_cast<Data*>(this);
		}

		inline static IndexedStringData* script_cast(Data* data) {
			return dynamic_cast<IndexedStringData*>(data);
		}
	};

	//////////////////////////////////////////////////////////////////////////
	/// IndexedStringData
	//////////////////////////////////////////////////////////////////////////
	template <typename t_type, typename t_matrix>
	void IndexedStringData<t_type, t_matrix>::init(size_t cols, size_t depth /* = 1 */, size_t rows /* = 1 */) {
		m_metadata = std::make_shared<IndexedStringMetadata<t_type> >(nullptr, 1, cols);
		m_indexes.ensureSizeAndDepth(rows, cols, depth, false);
		m_indexes.invalidateAll();
	}

	template <typename t_type, typename t_matrix>
	IndexedStringData<t_type, t_matrix>::IndexedStringData(IUniverse* universe, const StringVec& rhs)
		: StringData(universe, 1)
		, m_indexes(nullptr, DataDefinition::getDefault<t_type>(), 1, rhs.size()) {
		m_metadata = std::make_shared<IndexedStringMetadata<t_type> >(nullptr, rhs);
	}

	//template <typename t_type, typename t_matrix>
	//void IndexedStringData<t_type, t_matrix>::init(IUniverse* universe, const StringVec& rhs, std::shared_ptr<Data> customMetadata)
	//	: StringData(universe, 1)
	//	, m_indexes(nullptr, DataDefinition::getDefault<t_type>(), 1, rhs.size()) {
	//	m_metadata = std::make_shared<IndexedStringMetadata<t_type> >(nullptr, rhs);
	//}

	template <typename t_type, typename t_matrix>
	IndexedStringData<t_type, t_matrix>::IndexedStringData(IUniverse* universe, const StringVecVecVec& rhs)
		: StringData(universe, 1)
		, m_indexes(nullptr, DataDefinition::getDefault<t_type>(), 1, rhs.size()) {
		m_metadata = std::make_shared<IndexedStringMetadata<t_type> >(nullptr, rhs);
	}

	template <typename t_type, typename t_matrix>
	IndexedStringData<t_type, t_matrix>::IndexedStringData(IUniverse* universe, size_t numCols)
		: StringData(universe, DataDefinition::s_istring)
		, m_indexes(nullptr, DataDefinition::getDefault<t_type>(), 1, numCols) {
		init(numCols);
	}

	template <typename t_type, typename t_matrix>
	IndexedStringData<t_type, t_matrix>::IndexedStringData(IUniverse* universe, size_t numRows, size_t numCols)
		: StringData(universe, DataDefinition::s_istring)
		, m_indexes(nullptr, DataDefinition::getDefault<t_type>(), numRows, numCols) {
		init(numCols, 1, numRows);
	}

	template <typename t_type, typename t_matrix>
	IndexedStringData<t_type, t_matrix>::IndexedStringData(IUniverse* universe, size_t numCols, DataDefinition def)
		: StringData(universe, def)
		, m_indexes(nullptr, DataDefinition(def.name, def.itemType, DataType::size(def.itemType), def.frequency, def.flags), 1, numCols, false) {
		init(numCols);
	}

	template <typename t_type, typename t_matrix>
	IndexedStringData<t_type, t_matrix>::IndexedStringData(IUniverse* universe, size_t numRows, size_t numCols, DataDefinition def)
		: StringData(universe, DataDefinition(def.name, def.itemType, DataType::size(def.itemType), def.frequency, def.flags))
		, m_indexes((IUniverse*)nullptr, DataDefinition(def.name, def.itemType, DataType::size(def.itemType), def.frequency, def.flags), numRows, numCols, false) {
		init(numCols, 1, numRows);
	}

	template <typename t_type, typename t_matrix>
	IndexedStringData<t_type, t_matrix>::IndexedStringData(IUniverse* universe, DataDefinition def)
		: StringData(universe, def)
		, m_indexes(nullptr, DataDefinition(def.name, def.itemType, DataType::size(def.itemType), def.frequency, def.flags)) {

		DataType::Type dataType = DataType::kIndexedStringMetadata;
		if (sizeof(t_type) > 2)
			dataType = DataType::kLongIndexedStringMetadata;

		m_metadata = std::make_shared<IndexedStringMetadata<t_type> >(nullptr, DataDefinition(std::string(def.name) + ".meta",
			dataType, DataType::size(dataType), DataFrequency::kStatic, 0));
		m_metadata->ensureSize(1, 0, false);
	}

	template <typename t_type, typename t_matrix>
	IndexedStringData<t_type, t_matrix>::~IndexedStringData() {
	}

	template <typename t_type, typename t_matrix>
	size_t IndexedStringData<t_type, t_matrix>::rows() const {
		return m_indexes.rows();
	}

	template <typename t_type, typename t_matrix>
	size_t IndexedStringData<t_type, t_matrix>::cols() const {
		return m_indexes.cols();
	}

	template <typename t_type, typename t_matrix>
	size_t IndexedStringData<t_type, t_matrix>::depth() const {
		return m_indexes.depth();
	}

	template <typename t_type, typename t_matrix>
	size_t IndexedStringData<t_type, t_matrix>::numItems() const {
		return m_indexes.numItems();
	}

	template <typename t_type, typename t_matrix>
	size_t IndexedStringData<t_type, t_matrix>::itemSize() const {
		return m_indexes.itemSize();
	}

	template <typename t_type, typename t_matrix>
	Data* IndexedStringData<t_type, t_matrix>::createInstance() {
		return new IndexedStringData(nullptr, def());
	}

	template <typename t_type, typename t_matrix>
	Data* IndexedStringData<t_type, t_matrix>::clone(IUniverse* universe, bool noCopy) {
		//if (!noCopy)
		//	return new IndexedStringData(universe, m_data);

		IndexedStringData* cl = new IndexedStringData(universe, def());
		cl->ensureSizeAndDepth(rows(), cols(), depth(), false);
		cl->setMetadata(m_metadata);
		return cl;
	}

	template <typename t_type, typename t_matrix>
	void IndexedStringData<t_type, t_matrix>::resetMetadata() {
		m_metadata = nullptr;
	}

	template <typename t_type, typename t_matrix>
	DataPtr IndexedStringData<t_type, t_matrix>::getMetadata() {
		return m_metadata;
	}

	template <typename t_type, typename t_matrix>
	void IndexedStringData<t_type, t_matrix>::setMetadata(DataPtr metadata_) {
		if (!metadata_)
			return;

		auto metadata = std::dynamic_pointer_cast<IndexedStringMetadata<t_type> >(metadata_);

		ASSERT(metadata, "Invalid metadata passed that's not of type IndexedStringMetadata!");

		/// If it's the same pointer then we are already there and need not do anything ...
		if (metadata == m_metadata)
			return;

		if (m_metadata->depth() && m_metadata->rows() && m_metadata->cols()) {
			for (size_t kk = 0; kk < m_indexes.depth(); kk++) {
				for (IntDay di = 0; di < (IntDay)m_indexes.rows(); di++) {
					for (size_t ii = 0; ii < m_indexes.cols(); ii++) {
						auto value = getValueAt(di, ii, kk);
						auto newIndex = metadata->index(value, true);
						m_indexes.item(di, ii, kk) = newIndex;
					}
				}
			}
		}

		resetMetadata();

		m_metadata = metadata;
	}

	template <typename t_type, typename t_matrix>
	void IndexedStringData<t_type, t_matrix>::copyElement(const Data* src_, IntDay diSrc, size_t iiSrc, IntDay diDst, size_t iiDst, size_t kkSrc /*= 0*/, size_t kkDst /*= 0*/) {
		std::string srcValue;

		auto* src = dynamic_cast<const StringData*>(src_);
		std::string value = src->getValueAt(diSrc, iiSrc, kkSrc);
		this->setValue(diDst, iiDst, value);
	}

	template <typename t_type, typename t_matrix>
	void IndexedStringData<t_type, t_matrix>::copyRow(const Data* src, IntDay diSrc, IntDay diDst, size_t kkSrc /* = 0 */, size_t kkDst /* = 0 */) {
		Data::copyRow(src, diSrc, diDst, kkSrc, kkDst);
	}

	template <typename t_type, typename t_matrix>
	DataPtr IndexedStringData<t_type, t_matrix>::extractRow(IntDay di, size_t kk /* = 0 */) {
		auto ret = std::make_shared<IndexedStringData<t_type, t_matrix> >(universe(), cols());
		ret->setMetadata(m_metadata);

		ASSERT(di < this->m_indexes.rows(), "Invalid di passed for extract row: " << di << " - Max row count: " << this->m_indexes.rows());

		ret->m_indexes[0] = this->m_indexes[di];
		return ret;
	}

	template <typename t_type, typename t_matrix>
	void IndexedStringData<t_type, t_matrix>::makeInvalid(IntDay di, size_t ii, size_t kk) {
		m_indexes.makeInvalid(di, ii, kk);
	}

	template <typename t_type, typename t_matrix>
	bool IndexedStringData<t_type, t_matrix>::isValid(IntDay di, size_t ii, size_t kk) const {
		//return m_indexes.isValid(di, ii, kk);
		auto value = getValueAt(di, ii, kk);
		return !value.empty();
	}

	template <typename t_type, typename t_matrix>
	const void* IndexedStringData<t_type, t_matrix>::data() const {
		return m_indexes.data();
	}

	template <typename t_type, typename t_matrix>
	void* IndexedStringData<t_type, t_matrix>::data() {
		return m_indexes.data();
	}

	template <typename t_type, typename t_matrix>
	size_t IndexedStringData<t_type, t_matrix>::dataSize() const {
		return m_indexes.dataSize();
	}

	template <typename t_type, typename t_matrix>
	bool IndexedStringData<t_type, t_matrix>::doesCustomReadWrite() const {
		return m_indexes.doesCustomReadWrite();
	}

	template <typename t_type, typename t_matrix>
	void IndexedStringData<t_type, t_matrix>::invalidateAll() {
		/// Clear out the indexes and the metadata
		m_indexes.invalidateAll();
		m_metadata->ensureSize(1, 0, false);
	}

	template <typename t_type, typename t_matrix>
	void IndexedStringData<t_type, t_matrix>::ensureSize(size_t rows, size_t cols, bool copyOld /* = false */) {
		m_indexes.ensureSize(rows, cols, copyOld);
		m_metadata->ensureSize(1, 0, false);
	}

	template <typename t_type, typename t_matrix>
	void IndexedStringData<t_type, t_matrix>::ensureSizeAndDepth(size_t rows, size_t cols, size_t depth, bool copyOld /* = false */) {
		/// We just ignore the depth
		//ensureSize(rows, cols, copyOld);
		m_indexes.ensureSizeAndDepth(rows, cols, depth, copyOld);
		m_metadata->ensureSize(1, 0, false);
	}

	template <typename t_type, typename t_matrix>
	void IndexedStringData<t_type, t_matrix>::ensureDepthAt(IntDay di, size_t ii, size_t depth) {
		m_indexes.ensureDepthAt(di, ii, depth);
	}

	template <typename t_type, typename t_matrix>
	size_t IndexedStringData<t_type, t_matrix>::depthAt(IntDay di, size_t ii) const {
		return m_indexes.depthAt(di, ii);
	}

	template <typename t_type, typename t_matrix>
	void IndexedStringData<t_type, t_matrix>::setValue(IntDay di, size_t ii, const std::string& value) {
		t_type index = m_metadata->index(value, true);
		m_indexes.item(di, ii, 0) = index;
	}

	template <typename t_type, typename t_matrix>
	void IndexedStringData<t_type, t_matrix>::setValueAt(IntDay di, size_t ii, size_t kk, const std::string& value) {
		t_type index = m_metadata->index(value, true);
		m_indexes.item(di, ii, kk) = index;
	}

	template <typename t_type, typename t_matrix>
	const std::string& IndexedStringData<t_type, t_matrix>::getValue(IntDay di, size_t ii) const {
		return getValueAt(di, ii, 0);
	}

	template <typename t_type, typename t_matrix>
	const std::string& IndexedStringData<t_type, t_matrix>::getValueAt(IntDay di, size_t ii, size_t kk) const {
		t_type index = m_indexes.item(di, ii, kk);

		if (pesa::invalid::check(index))
			return StringData::m_empty;

		return m_metadata->getValueAt(0, index, 0);
	}

	template <typename t_type, typename t_matrix>
	const std::string& IndexedStringData<t_type, t_matrix>::operator [](size_t ii) const {
		return getValue(0, ii);
	}

	template <typename t_type, typename t_matrix>
	bool IndexedStringData<t_type, t_matrix>::isVariableDepth() const {
		return m_indexes.isVariableDepth();
	}

	template <typename t_type, typename t_matrix>
	std::string	IndexedStringData<t_type, t_matrix>::getString(pesa::IntDay di, size_t ii, size_t kk) const {
		return getValueAt(di, ii, kk);
	}
	
	//////////////////////////////////////////////////////////////////////////
	/// ShortIndexedStringData: SWIG Wrapper. DO NOT CHANGE!
	//////////////////////////////////////////////////////////////////////////
	class Framework_API ShortIndexedStringData : public IndexedStringData<uint16_t, UShortMatrixData> {
	public:
		ShortIndexedStringData(IUniverse* universe, size_t numCols);
		ShortIndexedStringData(IUniverse* universe, size_t numRows, size_t numCols);
		ShortIndexedStringData(IUniverse* universe, size_t numCols, DataDefinition def);
		ShortIndexedStringData(IUniverse* universe, size_t numRows, size_t numCols, DataDefinition def);
		ShortIndexedStringData(IUniverse* universe, DataDefinition def);
		ShortIndexedStringData(IUniverse* universe, const StringVec& rhs);
		ShortIndexedStringData(IUniverse* universe, const StringVecVecVec& rhs);
		//ShortIndexedStringData(IUniverse* universe, const StringVec& rhs, std::shared_ptr<Data> customMetadata);

		virtual void 				setValue(IntDay di, size_t ii, const std::string& value) {
			IndexedStringData<uint16_t, UShortMatrixData>::setValue(di, ii, value);
		}

		virtual size_t 				dataSize() const {
			return IndexedStringData<uint16_t, UShortMatrixData>::dataSize();
		}

		Data*						createInstance() {
			return new ShortIndexedStringData(nullptr, def());
		}

		////////////////////////////////////////////////////////////////////////////
		/// SCRIPT helper functions - DO NOT USE IN C++
		////////////////////////////////////////////////////////////////////////////
		Data*						script_sptr() {
			return static_cast<Data*>(this);
		}

		inline static ShortIndexedStringData* script_cast(Data* data) {
			return dynamic_cast<ShortIndexedStringData*>(data);
		}
	};

	typedef std::shared_ptr<ShortIndexedStringData>			ShortIndexedStringDataPtr;

	//////////////////////////////////////////////////////////////////////////
	/// LongIndexedStringData: SWIG Wrapper. DO NOT CHANGE!
	//////////////////////////////////////////////////////////////////////////
	class Framework_API LongIndexedStringData : public IndexedStringData<int, IntMatrixData> {
	public:
		LongIndexedStringData(IUniverse* universe, size_t numCols);
		LongIndexedStringData(IUniverse* universe, size_t numRows, size_t numCols);
		LongIndexedStringData(IUniverse* universe, size_t numCols, DataDefinition def);
		LongIndexedStringData(IUniverse* universe, size_t numRows, size_t numCols, DataDefinition def);
		LongIndexedStringData(IUniverse* universe, DataDefinition def);
		LongIndexedStringData(IUniverse* universe, const StringVec& rhs);
		LongIndexedStringData(IUniverse* universe, const StringVecVecVec& rhs);
		//LongIndexedStringData(IUniverse* universe, const StringVec& rhs, std::shared_ptr<Data> customMetadata);

		virtual void 				setValue(IntDay di, size_t ii, const std::string& value) {
			IndexedStringData<int, IntMatrixData>::setValue(di, ii, value);
		}

		virtual size_t 				dataSize() const {
			return IndexedStringData<int, IntMatrixData>::dataSize();
		}

		Data*						createInstance() {
			return new LongIndexedStringData(nullptr, def());
		}

		////////////////////////////////////////////////////////////////////////////
		/// SCRIPT helper functions - DO NOT USE IN C++
		////////////////////////////////////////////////////////////////////////////
		Data*						script_sptr() {
			return static_cast<Data*>(this);
		}

		inline static LongIndexedStringData* script_cast(Data* data) {
			return dynamic_cast<LongIndexedStringData*>(data);
		}
	};

	typedef std::shared_ptr<LongIndexedStringData>			LongIndexedStringDataPtr;

	//////////////////////////////////////////////////////////////////////////
	/// VarLongIndexedStringData: SWIG Wrapper. DO NOT CHANGE!
	//////////////////////////////////////////////////////////////////////////
	class Framework_API VarLongIndexedStringData : public IndexedStringData<int, IntVarMatrixData> {
	public:
		VarLongIndexedStringData(IUniverse* universe, size_t numCols);
		VarLongIndexedStringData(IUniverse* universe, size_t numRows, size_t numCols);
		VarLongIndexedStringData(IUniverse* universe, size_t numCols, DataDefinition def);
		VarLongIndexedStringData(IUniverse* universe, size_t numRows, size_t numCols, DataDefinition def);
		VarLongIndexedStringData(IUniverse* universe, DataDefinition def);
		VarLongIndexedStringData(IUniverse* universe, const StringVec& rhs);
		VarLongIndexedStringData(IUniverse* universe, const StringVecVecVec& rhs);
		//LongIndexedStringData(IUniverse* universe, const StringVec& rhs, std::shared_ptr<Data> customMetadata);

		virtual void 				setValue(IntDay di, size_t ii, const std::string& value) {
			IndexedStringData<int, IntVarMatrixData>::setValue(di, ii, value);
		}

		virtual size_t 				dataSize() const {
			return IndexedStringData<int, IntVarMatrixData>::dataSize();
		} 
		
		virtual DataPtr				extractRow(IntDay di, size_t kk /* = 0 */);
		void						copyElement(const Data* src_, IntDay diSrc, size_t iiSrc, IntDay diDst, size_t iiDst, size_t kkSrc /*= 0*/, size_t kkDst /*= 0*/);

		virtual Data*				createInstance() {
			return new VarLongIndexedStringData(nullptr, def());
		}

		////////////////////////////////////////////////////////////////////////////
		/// SCRIPT helper functions - DO NOT USE IN C++
		////////////////////////////////////////////////////////////////////////////
		Data*						script_sptr() {
			return static_cast<Data*>(this);
		}

		inline static VarLongIndexedStringData* script_cast(Data* data) {
			return dynamic_cast<VarLongIndexedStringData*>(data);
		}
	};

	typedef std::shared_ptr<VarLongIndexedStringData>		VarLongIndexedStringDataPtr;

} /// namespace pesa 

