/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// IndexedStringData.h
///
/// Created on: 18 May 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "./StringData.h"
#include "./MatrixData.h"
#include "Data.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// IndexedStringMetadataData - Represents the metadata of the 
	/// IndexedString data object
	////////////////////////////////////////////////////////////////////////////
	template <typename t_type>
	class IndexedStringMetadata : public StringData {
	protected:
		typedef std::unordered_map<std::string, t_type> DataIndex;			/// The map of the data
		DataIndex					m_lut;					/// The look-up table
		std::recursive_mutex 		m_dataMutex;			/// Data mutex for thread safety

		void						init(size_t cols, size_t depth = 1, size_t rows = 1);

		virtual void				constructLUT();
		void						copyFrom(const IndexedStringMetadata* rhs);

	public:
									IndexedStringMetadata(IUniverse* universe, size_t numCols);
									IndexedStringMetadata(IUniverse* universe, size_t numRows, size_t numCols);
									IndexedStringMetadata(IUniverse* universe, size_t numCols, DataDefinition def);
									IndexedStringMetadata(IUniverse* universe, size_t numRows, size_t numCols, DataDefinition def);
									IndexedStringMetadata(IUniverse* universe, DataDefinition def);
									IndexedStringMetadata(IUniverse* universe, const StringVec& rhs);
									IndexedStringMetadata(IUniverse* universe, const StringVecVecVec& rhs);

		virtual 					~IndexedStringMetadata();

		t_type						index(const std::string& value, bool add);
		t_type						index(const std::string& value) const;

		virtual Data*				clone(IUniverse* universe, bool noCopy);
		virtual Data*				createInstance();

		virtual void 				ensureSize(size_t rows, size_t cols, bool copyOld = false);
		virtual void				ensureSizeAndDepth(size_t rows, size_t cols, size_t depth, bool copyOld = false);

#ifndef __SCRIPT__
		virtual io::InputStream&	read(pesa::io::InputStream& is);
#endif 

		////////////////////////////////////////////////////////////////////////////
		/// SCRIPT helper functions - DO NOT USE IN C++
		////////////////////////////////////////////////////////////////////////////
		Data*						script_sptr() {
			return static_cast<Data*>(this);
		}

		inline static IndexedStringMetadata* script_cast(Data* data) {
			return dynamic_cast<IndexedStringMetadata*>(data);
		}
	};

	//////////////////////////////////////////////////////////////////////////
	/// IndexedStringMetadata
	//////////////////////////////////////////////////////////////////////////
	template <typename t_type>	
	void IndexedStringMetadata<t_type>::init(size_t cols, size_t depth /* = 1 */, size_t rows /* = 1 */) {
		m_lut.clear();

		m_def.itemType = DataType::kIndexedStringMetadata;
		if (sizeof(t_type) > 2)
			m_def.itemType = DataType::kLongIndexedStringMetadata;
	}

	template <typename t_type>
	IndexedStringMetadata<t_type>::IndexedStringMetadata(IUniverse* universe, const StringVec& rhs)
		: StringData(universe, rhs) {
		constructLUT();

		m_def.itemType = DataType::kIndexedStringMetadata;
		if (sizeof(t_type) > 2)
			m_def.itemType = DataType::kLongIndexedStringMetadata;
	}

	template <typename t_type>
	IndexedStringMetadata<t_type>::IndexedStringMetadata(IUniverse* universe, const StringVecVecVec& rhs)
		: StringData(universe, rhs) {
		constructLUT();

		m_def.itemType = DataType::kIndexedStringMetadata;
		if (sizeof(t_type) > 2)
			m_def.itemType = DataType::kLongIndexedStringMetadata;
	}

	template <typename t_type>
	IndexedStringMetadata<t_type>::IndexedStringMetadata(IUniverse* universe, size_t numCols)
		: StringData(universe, DataDefinition::s_string) {
		init(numCols);
	}

	template <typename t_type>
	IndexedStringMetadata<t_type>::IndexedStringMetadata(IUniverse* universe, size_t numRows, size_t numCols)
		: StringData(universe, DataDefinition::s_string) {
		init(numCols, 1, numRows);
	}

	template <typename t_type>
	IndexedStringMetadata<t_type>::IndexedStringMetadata(IUniverse* universe, size_t numCols, DataDefinition def)
		: StringData(universe, def) {
		init(numCols);
	}

	template <typename t_type>
	IndexedStringMetadata<t_type>::IndexedStringMetadata(IUniverse* universe, size_t numRows, size_t numCols, DataDefinition def)
		: StringData(universe, def) {
		init(numCols, 1, numRows);
	}

	template <typename t_type>
	IndexedStringMetadata<t_type>::IndexedStringMetadata(IUniverse* universe, DataDefinition def)
		: StringData(universe, def) {
		m_def.itemType = def.itemType;
	}

	template <typename t_type>
	IndexedStringMetadata<t_type>::~IndexedStringMetadata() {
	}

	template <typename t_type>
	void IndexedStringMetadata<t_type>::constructLUT() {
		m_lut.clear();

		ASSERT(this->depth() == 1 && this->rows() == 1, "IndexedStringMetadata cannot have more than 1 row and 1 depth!");

		for (size_t ii = 0; ii < this->cols(); ii++) {
			const std::string& value = this->getValue(0, ii);
			m_lut[value] = (t_type)ii;
			//this->index(value, true);
		}
	}

	template <typename t_type>
	void IndexedStringMetadata<t_type>::ensureSize(size_t rows, size_t cols, bool copyOld /* = false */) {
		StringData::ensureSize(1, 0);
	}

	template <typename t_type>
	void IndexedStringMetadata<t_type>::ensureSizeAndDepth(size_t rows, size_t cols, size_t depth, bool copyOld /* = false */) {
		StringData::ensureSizeAndDepth(1, 0, 1);
	}

	template <typename t_type>
	void IndexedStringMetadata<t_type>::copyFrom(const IndexedStringMetadata* rhs) {
		StringData::m_data = rhs->m_data;
		m_lut = rhs->m_lut;
	}

	template <typename t_type>
	t_type IndexedStringMetadata<t_type>::index(const std::string& value, bool add) {
		auto iter = m_lut.find(value);
		t_type size = (t_type)m_lut.size();

		if (iter == m_lut.end()) {
			if (!add || value.empty()) {
				t_type invalid;
				pesa::invalid::get(invalid);
				return invalid;
			}

			/// This needs to be thread-safe
			{
				AUTO_REC_LOCK(m_dataMutex);
				if (!m_data.size()) {
					StringData::ensureSizeAt(0, 1, 0, false);
				}

				size_t size = m_data[0][0].size();
				t_type usize = (t_type)size;

				ASSERT(size < std::numeric_limits<t_type>::max(), "Size overflow with indexed string. Max allowed: " << std::numeric_limits<t_type>::max() << " - Current size: " << size);
				m_data[0][0].push_back(value);
				m_lut[value] = usize;
				return usize;
			}
		}

		return iter->second;
	}

	template <typename t_type>
	t_type IndexedStringMetadata<t_type>::index(const std::string& value) const {
		auto iter = m_lut.find(value);
		t_type invalid;

		if (iter == m_lut.end()) {
			pesa::invalid::get(invalid);
			return invalid;
		}

		return iter->second;
	}

	template <typename t_type>
	Data* IndexedStringMetadata<t_type>::clone(IUniverse* universe, bool noCopy) {
		if (!noCopy)
			return new IndexedStringMetadata(universe, m_data);

		IndexedStringMetadata* cl = new IndexedStringMetadata(universe, def());
		cl->ensureSizeAndDepth(rows(), cols(), depth(), false);
		return cl;
	}

	template <typename t_type>
	Data* IndexedStringMetadata<t_type>::createInstance() {
		return new IndexedStringMetadata(nullptr, def());
	}

	template <typename t_type>
	io::InputStream& IndexedStringMetadata<t_type>::read(pesa::io::InputStream& is) {
		StringData::read(is);
		constructLUT();
		return is;
	}

	////////////////////////////////////////////////////////////////////////// 
	/// ShortIndexedStringMetadata: SWIG Wrapper. DO NOT CHANGE!
	//////////////////////////////////////////////////////////////////////////
	class Framework_API ShortIndexedStringMetadata : public IndexedStringMetadata<uint16_t> {
	public:
		ShortIndexedStringMetadata(IUniverse* universe, size_t numCols);
		ShortIndexedStringMetadata(IUniverse* universe, size_t numRows, size_t numCols);
		ShortIndexedStringMetadata(IUniverse* universe, size_t numCols, DataDefinition def);
		ShortIndexedStringMetadata(IUniverse* universe, size_t numRows, size_t numCols, DataDefinition def);
		ShortIndexedStringMetadata(IUniverse* universe, DataDefinition def);
		ShortIndexedStringMetadata(IUniverse* universe, const StringVec& rhs);
		ShortIndexedStringMetadata(IUniverse* universe, const StringVecVecVec& rhs);

		Data* createInstance() {
			return new ShortIndexedStringMetadata(nullptr, def());
		}
	};

	typedef std::shared_ptr<ShortIndexedStringMetadata>	ShortIndexedStringMetadataPtr;

	//////////////////////////////////////////////////////////////////////////
	/// LongIndexedStringMetadata: SWIG Wrapper. DO NOT CHANGE!
	//////////////////////////////////////////////////////////////////////////
	class Framework_API LongIndexedStringMetadata : public IndexedStringMetadata<int> {
	public:
		LongIndexedStringMetadata(IUniverse* universe, size_t numCols);
		LongIndexedStringMetadata(IUniverse* universe, size_t numRows, size_t numCols);
		LongIndexedStringMetadata(IUniverse* universe, size_t numCols, DataDefinition def);
		LongIndexedStringMetadata(IUniverse* universe, size_t numRows, size_t numCols, DataDefinition def);
		LongIndexedStringMetadata(IUniverse* universe, DataDefinition def);
		LongIndexedStringMetadata(IUniverse* universe, const StringVec& rhs);
		LongIndexedStringMetadata(IUniverse* universe, const StringVecVecVec& rhs);

		Data* createInstance() {
			return new LongIndexedStringMetadata(nullptr, def());
		}
	};

	typedef std::shared_ptr<LongIndexedStringMetadata>	LongIndexedStringMetadataPtr;
} /// namespace pesa 

