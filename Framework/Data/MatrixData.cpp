/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// MatrixData.cpp
///
/// Created on: 15 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "MatrixData.h"

#include "Poco/DateTimeFormat.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Int64MatrixData
	////////////////////////////////////////////////////////////////////////////
	Poco::DateTime Int64MatrixData::getDTValue(IntDay di, size_t ii) const {
		return Poco::DateTime(getTSValue(di, ii));
	}

	void Int64MatrixData::setDTValue(IntDay di, size_t ii, const Poco::DateTime& dt) {
		setTSValue(di, ii, dt.timestamp());
	}

	Poco::Timestamp	Int64MatrixData::getTSValue(IntDay di, size_t ii) const {
		/// This is in keeping with pandas/BamData convention of nanoseconds instead of the microseconds
		int64_t value = getValue(di, ii) / 1000;
		return Poco::Timestamp(value);
	}

	void Int64MatrixData::setTSValue(IntDay di, size_t ii, const Poco::Timestamp& ts) {
		/// Change to nanoseconds (pandas/BamData convention)
		int64_t value = ts.epochMicroseconds() * 1000;
		setValue(di, ii, value);
	}

	float Int64MatrixData::getPyValue(IntDay di, size_t ii) const {
		/// This is in keeping with pandas/BamData convention of nanoseconds instead of the microseconds
		int64_t value = getValue(di, ii);
		return (float)value;
	}

	void Int64MatrixData::setPyValue(IntDay di, size_t ii, float value) {
		setValue(di, ii, (int64_t)value);
	} 

	void Int64MatrixData::setString(pesa::IntDay di, size_t ii, size_t kk, const std::string& value) {
		if (value.empty()) {
			(*this)(di, ii, kk) = 0;
			return;
		}

		/// If it's all numbers then we just treat this as a number
		if (std::all_of(value.begin(), value.end(), ::isdigit)) 
			(*this)(di, ii, kk) = Util::cast<uint64_t>(value);
		else {
			auto date = Util::parseFormattedDate(Poco::DateTimeFormat::ISO8601_FORMAT, value);
			setDTValue(di, ii, date);
		}

	}

	////////////////////////////////////////////////////////////////////////////
	/// ConstantFloat
	////////////////////////////////////////////////////////////////////////////
	ConstantFloat::ConstantFloat(float value, DataDefinition def) 
		: FloatMatrixData(nullptr, std::make_shared<MatrixData<float>::TMatrix>(1, 1), def, false) {
		(*MatrixData<float>::m_data[0])(0, 0) = value;
	}

	ConstantFloat::ConstantFloat(float value)
		: FloatMatrixData(nullptr, std::make_shared<MatrixData<float>::TMatrix>(1, 1), DataDefinition::s_float, false) {
		(*MatrixData<float>::m_data[0])(0, 0) = value;
	}

	void ConstantFloat::ensureCols(size_t di, size_t numCols) {
		auto* data = MatrixData<float>::m_data[0].get();
		if (data->cols() >= numCols)
			return;  

		/// Otherwise we allocated
		float value = (*this)(0, 0);

		ensureSizeAt(0, 1, numCols, false);
		std::fill_n(fdata(0), (1 * numCols), value);
	}

	float ConstantFloat::operator() (IntDay, size_t) const {
		return (*MatrixData<float>::m_data[0])(0, 0);
	}

	float& ConstantFloat::operator() (IntDay, size_t) {
		return (*MatrixData<float>::m_data[0])(0, 0);
	}

	void ConstantFloat::setValue(IntDay, size_t, float) {
		throw Poco::Exception("Cannot set value on a constant float value!");
	}

	float& ConstantFloat::value() {
		return FloatMatrixData::operator()(0, 0);
	}

	float ConstantFloat::value() const {
		return FloatMatrixData::operator()(0, 0);
	}

	Data* ConstantFloat::clone(IUniverse*, bool) {
		return new ConstantFloat(value());
	}

	////////////////////////////////////////////////////////////////////////////
	/// FloatVector
	////////////////////////////////////////////////////////////////////////////
	// FloatVector::FloatVector(size_t size)
	// 	: FloatMatrixData(DataDefinition(), 1, size) {
	// }

	// FloatVector::FloatVector(const DataDefinition& def, size_t size)
	// 	: FloatMatrixData(def, 1, size) {
	// }

	// float FloatVector::operator() (size_t ii) const {
	// 	return (*MatrixData<float>::m_data)(0, ii);
	// }

	// float& FloatVector::operator() (size_t ii) {
	// 	return (*MatrixData<float>::m_data)(0, (int)ii);
	// }

	// float FloatVector::getValue(size_t ii) const {
	// 	return (*this)(ii);
	// }

	// void FloatVector::setValue(size_t ii, float value) {
	// 	(*this)(ii) = value;
	// }

	////////////////////////////////////////////////////////////////////////////
	/// UW_FloatMatrixData
	////////////////////////////////////////////////////////////////////////////
	// UW_FloatMatrixData::UW_FloatMatrixData(IUniverse* universe, FloatMatrixDataPtr mat)
	// 	: FloatMatrixData(DataDefinition(), 0, 0, mat->isCircular())
	// 	, m_universe(universe), m_mat(mat) {}

	// float UW_FloatMatrixData::operator() (IntDay di_, size_t ii_) const { 
	// 	return (*m_mat.get())(m_universe, di_, ii_); 
	// }

	// float& UW_FloatMatrixData::operator() (IntDay di_, size_t ii_) {
	// 	ASSERT(false, "Cannot call a non-const function on a constant internal matrix");
	// 	if (di_ > rows())
	// 		di_ = (IntDay)rows() - 1;
	// 	return (*const_cast<FloatMatrixData*>(m_mat.get()))(di_, ii_);
	// }

	// float UW_FloatMatrixData::operator() (IUniverse* universe, IntDay di, size_t ii) const {
	// 	if (di > rows())
	// 		di = (IntDay)rows() - 1;
	// 	return (*m_mat)(universe, di, ii);
	// }

	// float UW_FloatMatrixData::operator() (pesa::IAlpha* alpha, const RuntimeData& rt, IntDay di, size_t ii) const {
	// 	if (di > rows())
	// 		di = (IntDay)rows() - 1;
	// 	return (*m_mat)(alpha, rt, di, ii);
	// }

	// size_t UW_FloatMatrixData::rows() const { 
	// 	return m_mat->rows(); 
	// }

	// size_t UW_FloatMatrixData::cols() const { 
	// 	return m_mat->cols(); 
	// }

	// const float* UW_FloatMatrixData::fdata() const { 
	// 	return m_mat->fdata(); 
	// }

	// float* UW_FloatMatrixData::fdata() {
	// 	return m_mat->fdata();
	// }

	// const void* UW_FloatMatrixData::data() const { 
	// 	return m_mat->data(); 
	// }

	// size_t UW_FloatMatrixData::dataSize() const { 
	// 	return m_mat->dataSize(); 
	// }

	// FloatMatrixDataPtr UW_FloatMatrixData::mat() {
	// 	return m_mat;
	// }

	// float UW_FloatMatrixData::getValue(IntDay di, size_t ii) const { 
	// 	return (*this)(di, ii); 
	// }

	// void UW_FloatMatrixData::invalidateMemory() {
	// 	m_mat->invalidateMemory();
	// }

	// void UW_FloatMatrixData::zeroMemory() { 
	// 	m_mat->zeroMemory(); 
	// }

	// void UW_FloatMatrixData::ensureSize(size_t rows, size_t cols, bool copyOld /* = false */) { 
	// 	m_mat->ensureSize(rows, cols, copyOld); 
	// }

	// UW_FloatMatrixData::operator bool() const {
	// 	return m_mat ? true : false;
	// }

	// UW_FloatMatrixData& UW_FloatMatrixData::operator = (const FloatMatrixData& rhs) {
	// 	*m_mat = rhs;
	// 	return *this;
	// }

	// UW_FloatMatrixData& UW_FloatMatrixData::operator = (const UW_FloatMatrixData& rhs) {
	// 	m_universe = rhs.m_universe;
	// 	*m_mat = *rhs.m_mat;
	// 	return *this;
	// }

	// ////////////////////////////////////////////////////////////////////////////
	// /// AlphaVector
	// ////////////////////////////////////////////////////////////////////////////
	// AlphaVector::AlphaVector(IUniverse* universe, FloatMatrixDataPtr mat, size_t indexCols)
	// 	: UW_FloatMatrixData(universe, mat) 
	// 	, m_indexCols(indexCols) {
	// }

	// AlphaVector::AlphaVector(IUniverse* universe, size_t rows, size_t cols, size_t indexCols)
	// 	: UW_FloatMatrixData(universe, rows && cols ? std::make_shared<FloatMatrixData>(DataDefinition(), 1, cols, false) : nullptr) 
	// 	, m_indexCols(indexCols) {
	// }

	// float AlphaVector::operator() (IntDay di_, size_t ii_) const {
	// 	return (*this)(m_universe, di_, ii_);
	// }

	// float& AlphaVector::operator() (IntDay di_, size_t ii_) {
	// 	return (*this)(m_universe, di_, ii_);
	// }

	// size_t AlphaVector::mapIndex(IUniverse* universe, IntDay di, size_t ii) const {
	// 	if (!universe)
	// 		universe = m_universe;

	// 	return universe->alphaIndex(di, ii);
	// }

	// float AlphaVector::operator() (IUniverse* universe, IntDay di, size_t ii_) const {
	// 	size_t ii = mapIndex(universe, di, ii_);

	// 	ASSERT(ii < m_mat->cols(), "Invalid index returned. Original ii: " << ii_ << " - mapped ii: " << ii << " - di: " << di << " - Universe: " << universe->name() << " - Max Cols: " << m_mat->cols());
	// 	return (*m_mat)(0, ii);
	// }

	// float& AlphaVector::operator() (IUniverse* universe, IntDay di, size_t ii_) {
	// 	size_t ii = mapIndex(universe, di, ii_);

	// 	if (ii >= m_mat->cols()) {
	// 		size_t maxAlphaSize = universe->maxAlphaSize(di);
	// 		m_mat->ensureSize(m_mat->rows(), maxAlphaSize, true);
	// 	}

	// 	ASSERT(ii < m_mat->cols(), "Invalid index returned. Original ii: " << ii_ << " - mapped ii: " << ii << " - di: " << di << " - Universe: " << universe->name() << " - Max Cols: " << m_mat->cols());
	// 	float& value = (*m_mat)(0, ii);
	// 	return value;
	// }

	// size_t AlphaVector::cols() const {
	// 	return m_indexCols;
	// }

	// size_t AlphaVector::matCols() const {
	// 	return m_mat->cols();
	// }

	// float AlphaVector::at(IntDay di, size_t ii) const {
	// 	return (*m_mat)(di, ii);
	// }

	// float& AlphaVector::at(IntDay di, size_t ii) {
	// 	return (*m_mat)(di, ii);
	// }

	// FloatVec AlphaVector::toVector(IntDay di, bool full) const {
	// 	size_t size = !full ? cols() : matCols();
	// 	FloatVec vec(size);

	// 	if (!full) {
	// 		for (size_t ii = 0; ii < size; ii++) 
	// 			vec[ii] = (*this)(di, ii);
	// 	}
	// 	else {
	// 		for (size_t ii = 0; ii < size; ii++)
	// 			vec[ii] = (*m_mat)(di, ii);
	// 	}

	// 	return vec;
	// }
} /// namespace pesa 
