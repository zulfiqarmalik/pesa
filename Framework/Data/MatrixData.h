/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// MatrixData.h
///
/// Created on: 15 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/FrameworkDefs.h"
#include "Data.h"
#include "Core/Math/Math.h"
#include "Framework/Components/IUniverse.h"

#include <memory>
#include <cmath>
#include <type_traits>

namespace pesa {

	class IAlpha;

	////////////////////////////////////////////////////////////////////////////
	/// Matrix data
	////////////////////////////////////////////////////////////////////////////
	template <typename t_type, const int ROWS = -1, const int COLS = -1>
	class MatrixData : public Data {
	public:
		typedef pesa::Matrix<t_type>				TMatrix;
		typedef std::shared_ptr<TMatrix> 			TMatrixPtr;

		typedef std::vector<TMatrixPtr>				TMatrixPtrVec;
		typedef std::shared_ptr<MatrixData<t_type> > MatrixDataTypePtr;
		typedef std::vector<MatrixDataTypePtr>		MatrixDataTypePtrVec;

	protected:
		TMatrixPtrVec			m_data;					/// The data (its a DEPTHxROWSxCOLS matrix). 
														/// The vector is the DEPTH and then the DEPTH with 
														/// ROWSxCOLS matrices stacked on top of each other
#ifndef __SCRIPT__
		mutable 
		std::mutex*				m_transposeMutex = new std::mutex();/// Since the transpose is calculated at runtime (outside the safety of the DataRegistry).
																	/// We need to ensure thread safety.
		mutable 
#endif
		MatrixDataTypePtrVec	m_dataTranspose;		/// The transpose of the data above. Note that 
		bool 					m_isCircular = false; 	/// Is it a circular array

	public:
		MatrixData() 
			: Data(nullptr, DataDefinition()) 
			, m_data(1)
			, m_isCircular(false) {
		}

		MatrixData(IUniverse* universe, TMatrixPtr data, DataDefinition def, bool isCircular = false)
			: Data(universe, def)
			, m_data(1)
			, m_isCircular(isCircular) {
			m_data[0] = data;
		}

		MatrixData(IUniverse* universe, DataDefinition def, size_t rows, size_t cols, bool isCircular = false)
			: Data(universe, def)
			, m_data(1)
			, m_isCircular(isCircular) {
			m_data[0] = nullptr;
			if (rows && cols)
				ensureSize(rows, cols, false);
		}

		MatrixData(IUniverse* universe, const std::vector<t_type>& vector) 
			: Data(universe, DataDefinition()) 
			, m_data(1) {
			if (vector.size()) {
				ensureSize(1, vector.size(), false);
				memcpy(fdata(), &vector[0], sizeof(t_type) * vector.size());
			}
		}

		MatrixData(IUniverse* universe, DataDefinition def)
			: Data(universe, def)
			, m_data(1) {
		}

		explicit MatrixData(IUniverse* universe, const MatrixData<t_type>& rhs)
			: Data(universe, rhs.def()) {
			auto m = m_transposeMutex;
			*this = rhs;
			m_transposeMutex = m;
		}

		virtual ~MatrixData() {
			Trace("MATRIX", "Deleting matrix");
			m_data.clear();
			m_dataTranspose.clear();
			delete m_transposeMutex;
			m_transposeMutex = nullptr;
		}

		inline bool isAllocated() const { return m_data[0] ? true : false; }

		virtual size_t dataSize() const {
			return rows() * cols() * depth() * sizeof(t_type);
		}

		inline bool isCircular() const {
 			return m_isCircular;
		}

		virtual MatrixData<t_type>* createTranspose() const { return new MatrixData<t_type>(nullptr, this->def()); }

		virtual const MatrixData<t_type>* transpose(size_t kk = 0) const {
			t_type invalidValue;
			pesa::invalid::get(invalidValue);
			return this->nanTranspose(invalidValue, kk);
		}

		virtual const MatrixData<t_type>* nanTranspose(t_type invalidValue, size_t kk = 0) const {
			if (m_dataTranspose.size() > kk && m_dataTranspose[kk])
				return m_dataTranspose[kk].get();

			AUTO_LOCK(*m_transposeMutex);

			/// We need to check this again after the lock to ensure that no race-condition 
			/// has happened in the block above the mutex lock! The block above is for 
			/// performance reasons since once we have created the transpose matrix
			/// we don't want to have a needless mutex lock to access it
			if (m_dataTranspose.size() > kk && m_dataTranspose[kk])
				return m_dataTranspose[kk].get();

			if (m_dataTranspose.size() <= kk)
				m_dataTranspose.resize(kk + 1);

			size_t srcNumCols = this->colsForTranspose();
			size_t srcNumRows = this->rowsForTranspose();

			//MatrixData<t_type>* dst = dynamic_cast<MatrixData<t_type>*>((const_cast<MatrixData<t_type>*>(this))->clone(m_universe, true));
			MatrixData<t_type>* dst = this->createTranspose();
			dst->ensureSizeAndDepth(srcNumCols, srcNumRows, 1);

			for (auto ri = 0; ri < srcNumRows; ri++) {
				for (auto ci = 0; ci < srcNumCols; ci++) {
					t_type rhs = (t_type)(*this)(ri, ci, kk);
					if (!pesa::invalid::check(rhs))
						(*dst)(ci, ri) = rhs;
					else
						(*dst)(ci, ri) = (t_type)invalidValue;
				}
			}

			m_dataTranspose[kk] = std::shared_ptr<MatrixData<t_type> >(dst);
			return dst;
		}

		//virtual MatrixData<t_type>& operator = (const MatrixData& rhs) {
		//	// ensureSize(rhs.rows(), rhs.cols());

		//	// //m_data = TMatrixPtr(new TMatrix(rhs.rows(), rhs.cols()));
		//	// m_def = rhs.m_def;
		//	// m_isCircular = rhs.m_isCircular;

		//	// //memcpy(fdata(), rhs.fdata(), dataSize());

		//	// for (int i = 0; i < (int)rhs.rows(); i++) {
		//	// 	for (int j = 0; j < (int)rhs.cols(); j++) {
		//	// 		(*m_data)(i, j) = rhs(i, j);
		//	// 	}
		//	// }
		//	m_data = rhs.m_data;
		//	m_isCircular = rhs.m_isCircular;

		//	return *this;
		//}

		const t_type* rowPtr(IntDay di) const {
			const t_type* data = this->fdata();
			return data + DI(di) * this->cols();
		}

		t_type* rowPtr(IntDay di) {
			t_type* data = this->fdata();
			return data + DI(di) * this->cols();
		}

		std::vector<t_type> rowArray(IntDay di, bool adjustForUniverse = false) const {
			if (!adjustForUniverse) {
				size_t size = cols();
				std::vector<t_type> vec(size);

				for (size_t ii = 0; ii < size; ii++)
					vec[ii] = (*this)(di, ii);

				return vec;
			}

			const auto* universe = this->universe();
			ASSERT(universe, "Cannot create universe specific data with no universe!");
			auto size = universe->size(di);
			std::vector<t_type> vec(size);

			for (size_t ii = 0; ii < size; ii++)
				vec[ii] = (*this)(di, ii);

			return vec;
		}

		std::vector<t_type> colArray(size_t ii, size_t maxDays = 0) const {
			size_t size = rows();
			size_t start = 0;
			
			if (maxDays != 0 && maxDays < size) 
				start = size - maxDays;

			std::vector<t_type> vec(size - start);

			for (size_t di = start; di < size; di++)
				vec[di - start] = (*this)((IntDay)di, ii);

			return vec;
		}

		virtual t_type* fdata(size_t index = 0) {
			return m_data[index]->data();
		}

		virtual const t_type* fdata(size_t index = 0) const {
			return m_data[index]->data();
		}

		TMatrixPtrVec& innerData() const {
			return const_cast<TMatrixPtrVec&>(m_data);
		}

		TMatrixPtrVec& innerData() {
			return m_data;
		}

		virtual const void* data() const {
			return reinterpret_cast<const void*>(m_data[0]->data());
		}

		virtual void* data() {
			return reinterpret_cast<void*>(m_data[0]->data());
		}

		virtual void copyRow(const Data* src, IntDay diSrc, IntDay diDst, size_t kkSrc = 0, size_t kkDst = 0) {
			ASSERT(diSrc < src->rows(), "Invalid source data row passed to copyRow: " << diSrc << " - Row Count: " << src->rows());
			ASSERT(diDst < rows(), "Invalid destination data row passed to copyRow: " << diDst << " - Row Count: " << rows());
			ASSERT(cols() == src->cols(), "Number of columns in source are not the same. this->cols(): " << cols() << " - src->cols(): " << src->cols());

			const MatrixData<t_type>* rhs = dynamic_cast<const MatrixData<t_type>*>(src);
			this->innerMat(kkDst).middleRows(diDst, 1) = rhs->innerMat(kkSrc).middleRows(diSrc, 1);
		}

		virtual void copyElement(const Data* src_, IntDay diSrc, size_t iiSrc, IntDay diDst, size_t iiDst, size_t kkSrc = 0, size_t kkDst = 0) {
			auto* src = dynamic_cast<const MatrixData<t_type>*>(src_);
			ASSERT(src, "Unable to cast source into a MatrixData. Data: " << std::string(def().name));
			const auto& srcValue = (*src)(diSrc, iiSrc, kkSrc);
			(*this)(diDst, iiDst, kkDst) = srcValue;
		}

		virtual t_type operator() (IntDay di_, size_t ii_) const {
			return (*this)(di_, ii_, 0);
		}

		virtual t_type& operator() (IntDay di_, size_t ii_) {
			return (*this)(di_, ii_, 0);
		}

		const t_type item(IntDay di, size_t ii, size_t kk) const {
			return (*this)(di, ii, kk);
		}

		t_type& item(IntDay di, size_t ii, size_t kk) {
			return (*this)(di, ii, kk);
		}

		virtual t_type operator() (IntDay di_, size_t ii_, size_t kk) const {
			int di = (int)DI(di_);
			int ii = (int)II(di, ii_);

			ASSERT(ii >= 0 && ii < cols(), "Invalid ii: " << ii << " - Col Count: " << cols() << " - Data: " << std::string(def().name));

			if (m_isCircular)
				return (*m_data[kk])(di % m_data[kk]->rows(), ii);

			ASSERT(di >= 0 && di < rows(), "Invalid di: " << di << " - Row Count: " << rows() << " - Data: " << std::string(def().name));
			return m_data[kk]->getValue(di, ii);
		}

		virtual t_type& operator() (IntDay di_, size_t ii_, size_t kk) {
			int di = (int)DI(di_);
			int ii = (int)II(di, ii_);

			ASSERT(kk < m_data.size(), "Invalid kk: " << kk << " - Max depth: " << depth() << " - Data: " << std::string(def().name));
			ASSERT(ii >= 0 && ii < cols(), "Invalid ii: " << ii << " - Col Count: " << cols() << " - Data: " << std::string(def().name));

			if (m_isCircular)
				return (*m_data[kk])(di % m_data[kk]->rows(), ii);

			ASSERT(di >= 0 && di < rows(), "Invalid di: " << di << " - Row Count: " << rows() << " - Data: " << std::string(def().name));
			return m_data[kk]->getValue(di, ii);
		}

		inline IntDay DI(IntDay di) const {
			di = Data::DI(di);
			return !isCircular() ? di : (di % m_data[0]->rows());
		}

		virtual size_t rows() const {
			return m_data.size() && m_data[0] ? (size_t)(m_data[0]->rows() + m_diOffset) : 0;
		}

		virtual size_t cols() const {
			return m_data.size() && m_data[0] ? (size_t)m_data[0]->cols() : 0;
		}

		virtual size_t rowsForTranspose() const {
			return rows();
		}

		virtual size_t colsForTranspose() const {
			return cols();
		}

		virtual size_t depth() const {
			return m_data.size();
		}

		virtual void makeInvalid(pesa::IntDay di, size_t ii) {
			makeInvalid(di, ii, 0);
		}

		virtual void makeAlphaInvalid(size_t ii) {
			makeInvalid(0, ii, 0);
		}

		virtual bool isValid(pesa::IntDay di, size_t ii) const {
			return isValid(di, ii, 0);
		}

		virtual void makeInvalid(pesa::IntDay di, size_t ii, size_t kk) {
			t_type value;
			pesa::invalid::get(value);
			(*m_data[kk])(DI(di), II(di, ii)) = value;
		}

		virtual bool isValid(pesa::IntDay di, size_t ii, size_t kk) const {
			t_type value = (*m_data[kk])(DI(di), II(di, ii));
			return !pesa::invalid::check(value);
		}

		void setMemory(t_type value) {
			for (size_t kk = 0; kk < m_data.size(); kk++) {
				std::fill_n(fdata(kk), rows() * cols(), value);
				//innerMat(kk).setConstant(value);
			}
		}

		virtual void zeroMemory() {
			for (size_t kk = 0; kk < m_data.size(); kk++)
				memset(fdata(kk), 0, rows() * cols() * sizeof(t_type));
		}

		virtual void invalidateMemory() {
			t_type value;
			pesa::invalid::get(value);
			setMemory(value);
		}

		virtual void forceSize(size_t rows, size_t cols) {
			auto* mptr = new TMatrix(rows, cols);
			m_data[0] = TMatrixPtr(mptr);
		}

		virtual void ensureSizeAndDepth(size_t rows, size_t cols, size_t depth, bool copyOld = false) {
			/// If the depth is 1 then we just return the normal ensureSize
			if (depth == 1) {
				ensureSize(rows, cols, copyOld);
				return;
			}

			/// Otherwise we need to resize the array
			size_t existingSize = m_data.size();
			m_data.resize(depth);

			/// If the depth is less than the existing size of the vector then we don't need to do anything 
			/// more than just resizing the vector. The vector will be shrunk and the 
			if (depth < existingSize) 
				return;

			m_data.resize(depth);

			t_type value;
			pesa::invalid::get(value);

			/// OK here we'll need to increase the size of the vector
			for (size_t kk = 0; kk < depth; kk++) {
				ensureSizeAt(kk, rows, cols, copyOld);
			}
		}

		virtual void ensureSizeAt(size_t kk, size_t rows, size_t cols, bool copyOld = false) {
			if (kk > m_data.size())
				m_data.resize(kk + 1);

			if (m_data[kk] && rows == this->rows() && cols == this->cols())
				return;

			// Trace("MATRIX", "Allocating matrix data of size: %z (%z x %z)", rows * cols * sizeof(t_type), rows, cols);

			TMatrixPtr existingData = m_data[kk];
			t_type value;
			pesa::invalid::get(value);
			auto* mptr = new TMatrix(rows, cols);
			m_data[kk] = TMatrixPtr(mptr);

			if (existingData && copyOld) {
				Trace("MATRIX", "Copying old data: %zx%z", existingData->rows(), existingData->cols());

				size_t existingDataSize = existingData->rows() * existingData->cols();
				t_type* existingDataPtr = existingData->data();
				size_t existingPtrIncr = existingData->cols();
				t_type* newDataPtr = fdata(kk);
				size_t newPtrIncr = this->cols();

				/// We just blindly invalidate the data
				std::fill_n(newDataPtr, rows * cols, value);

				if (existingDataPtr && existingPtrIncr) {
					/// Copy the first bit of the memory 
					for (size_t e_di = 0; e_di < std::min(existingData->rows(), this->rows()); e_di++) {
						memcpy(newDataPtr, existingDataPtr, sizeof(t_type) * existingData->cols());
						newDataPtr += newPtrIncr;
						existingDataPtr += existingPtrIncr;
					}
				}
			}
			else {
				std::fill_n(reinterpret_cast<t_type*>(m_data[kk]->data()), rows * cols, value);
			}
		}

		virtual void ensureCols(size_t di, size_t numCols) {
			/// NO-OP
			return;
		}

		virtual void ensureSize(size_t rows, size_t cols, bool copyOld = false) {
			ensureSizeAt(0, rows, cols, copyOld);
		}

		virtual void initMem(void* mem, size_t memSize) const {
			ASSERT(memSize % sizeof(t_type) == 0, "Unaligned memory given to initMem. Expecting total size to be aligned to: " << 
				sizeof(t_type) << ", given total size: " << memSize);

			t_type* tmem = reinterpret_cast<t_type*>(mem);
			size_t numItems = memSize / sizeof(t_type);

			t_type value;
			pesa::invalid::get(value);
			std::fill_n(tmem, numItems, value);
		}

		std::vector<t_type> validRow(size_t di, std::vector<size_t>& validIndexes, size_t kk = 0) const {
			std::vector<t_type> row;
			size_t numCols = cols();
			for (size_t ii = 0; ii < numCols; ii++) {
				if (isValid((IntDay)di, ii, kk)) {
					validIndexes.push_back(ii);
					row.push_back((*this)((IntDay)di, ii, kk));
				}
			}

			return row;
		}

		Data* createInstance() {
			return new MatrixData<t_type>(nullptr, def());
		}

		virtual Data* clone(IUniverse* universe, bool noCopy) {
			if (!noCopy)
				return new MatrixData<t_type>(universe, *this);
			return new MatrixData<t_type>(universe, def(), rows(), cols());
		}

		void deepCopy(const MatrixData<t_type>& rhs) {
			auto rows = rhs.rows();
			auto cols = rhs.cols();
			ensureSize(rows, cols, false);
			memcpy(fdata(0), rhs.fdata(0), rows * cols * sizeof(t_type));
		}

		virtual Data* optimiseFor(IntDay diStart, IUniverse* srcUniverse, IUniverse* dstUniverse) {
			if (diStart >= this->rows())
				return nullptr;

			ASSERT(srcUniverse && dstUniverse, "Invalid data passed for cross-universe data optimiser!");

			Trace("DR", "Optimise data: %s => %s", srcUniverse->name(), dstUniverse->name());

			size_t maxCols = 0;
			size_t maxRows = this->rows() - diStart;

			for (IntDay di = diStart; di < (IntDay)this->rows(); di++) {
				size_t numCols = dstUniverse->size(di);

				if (numCols > maxCols)
					maxCols = numCols;
			}

			ASSERT(maxCols > 0, "Invalid maxCols for universe: " << dstUniverse->name() << " - Data: " << std::string(def().name));

			auto* dstData = dynamic_cast<MatrixData<t_type>*>(this->clone(dstUniverse, true));
			dstData->clearUniverse();
			dstData->forceSize(maxRows, maxCols);

			ASSERT(!Data::m_universe, "Optimsing dataset requires the source dataset to be from the Security Master. Current universe: " << Data::m_universe->name() << " - Data: " << std::string(def().name));

//#ifdef __WINDOWS__
//			MatrixData<t_type>* dstData = new std::remove_pointer<decltype(this)>::type(Data::m_universe, m_def, maxRows, maxCols, m_isCircular);
//#elif __LINUX__
//			/// TODO: Handle this properly!
//			MatrixData<t_type>* dstData = nullptr; 
//#endif

			for (IntDay di = diStart; di < diStart + (IntDay)maxRows; di++) {
				for (Security::Index ii = 0; ii < (Security::Index)maxCols; ii++) {
					Security sec = dstUniverse->security(di, ii);
					Security::Index iiSrc = sec.index;

					if (iiSrc != Security::s_invalidIndex && iiSrc < this->cols()) {
						t_type srcValue = (*this)(di, iiSrc);
						(*dstData)(di - diStart, ii) = srcValue;
					}
					else
						dstData->makeInvalid(di - diStart, ii);
					//ASSERT(iiSrc != Security::s_invalidIndex, "Unable to get security from universe " << dstUniverse->name() << " for (" << di << ", " << ii << ")");
				}
			}

			return dstData;
		}

		virtual void flush(IntDay di) const {
			/// TO BE DERIVED BY THE CHILD CLASSES SO THAT RESULTS CAN BE FETCHED IN ONE GO!
		}

		virtual void flushRange(IntDay diStart, IntDay count) const {
			for (IntDay di = diStart; di < diStart + count; di++)
				flush(di);
		}

		static inline t_type getInvalid() {
			t_type value;
			pesa::invalid::get(value);
			return value;
		}

		inline pesa::Matrix<t_type>& mat(size_t kk = 0) { 
			ASSERT(kk < m_data.size(), "Invalid kk: " << kk << " [Max: " << m_data.size() << "]");
			return *(m_data[kk].get());
		}

		inline const pesa::Matrix<t_type>& mat(size_t kk = 0) const {
			ASSERT(kk < m_data.size(), "Invalid kk: " << kk << " [Max: " << m_data.size() << "]");
			return *(m_data[kk].get());
		}

		inline const Eigen::Array<t_type, -1, -1, Eigen::RowMajor>& innerMat(size_t kk = 0) const { return mat(kk).mat(); }
		inline Eigen::Array<t_type, -1, -1, Eigen::RowMajor>& innerMat(size_t kk = 0) { return mat(kk).mat(); }

		virtual Eigen::Block<Eigen::Array<t_type, -1, -1, Eigen::RowMajor>, 1, -1, true > row(IntDay di, size_t kk = 0) { return mat(kk).mat().row(DI(di)); }
		virtual const Eigen::Block<const Eigen::Array<t_type, -1, -1, Eigen::RowMajor>, 1, -1, true > row(IntDay di, size_t kk = 0) const { flush(DI(di)); return mat(kk).mat().row(DI(di)); }

		inline Eigen::Block<Eigen::Array<t_type, -1, -1, Eigen::RowMajor>, 1, -1, true > operator[] (IntDay di) { return row(di, 0); }
		inline const Eigen::Block<const Eigen::Array<t_type, -1, -1, Eigen::RowMajor>, 1, -1, true > operator[] (IntDay di) const { return row(di, 0); }

		virtual const Eigen::Block<const Eigen::Array<t_type, -1, -1, Eigen::RowMajor>, -1, -1, true> middleRows(IntDay di, IntDay count) const {
			flushRange(di, count);
			di = DI(di);
			ASSERT(di + count <= rows(), "Invalid middler rows with di: " << di << " - Count: " << count);
			return this->innerMat().middleRows(di, count);
		}

		virtual bool doesSupportSharedMemory() const { return true; }
		virtual void fromSharedMemory(size_t kk, char* shMemPtr, size_t rows, size_t cols) {
			// ensureSizeAndDepth(kk + 1, 1, 1, false);
			// m_data[kk]->fromSharedMemory(shMemPtr, rows, cols);
			if (m_data.size() < kk)
				m_data.resize(kk + 1);

			m_data[kk] = TMatrixPtr(new TMatrix(shMemPtr, rows, cols));
		}

		//TMatrix sum(IntDay diStart, IntDay diEnd, IntDay delta = 1, IntDay* cnt = nullptr) const {
		//	TMatrix result(1, this->cols());
		//	result.setMemory(0.0f);

		//	IntDay count = 0;

		//	for (IntDay di = diStart; di <= diEnd; di += delta) { 
		//		result[0] += (*this)[di];
		//		count++;
		//	}

		//	if (cnt)
		//		*cnt = count;

		//	return result;
		//}

		//TMatrix mean(IntDay diStart, IntDay diEnd, IntDay delta = 1) const { 
		//	IntDay count = 0;
		//	auto result = this->sum(diStart, diEnd, delta, &count);
		//	float invCount = 1.0f / (float)count;
		//	result[0] = result[0] * invCount;
		//	return result;
		//}

		inline const t_type& getRawValue(int di, int ii) const {
			return (*m_data[0])(di, ii);
		}

		////////////////////////////////////////////////////////////////////////////
		/// SWIG - DO NOT USE IN C++
		////////////////////////////////////////////////////////////////////////////
		//#ifdef __SCRIPT__
		inline t_type getValue(int di, int ii) const {
			return (*this)(di, ii);
		}

		inline void setValue(int di, int ii, const t_type& value) {
			(*this)(di, ii) = value;
		}

		inline Data* script_sptr() {
			return static_cast<Data*>(this);
		}
		//#endif 
	};

#define SCRIPT_STUB(Name, Type) \
	public: \
								Name(IUniverse* universe, MatrixData<Type>::TMatrixPtr data, DataDefinition def, bool isCircular = false)  \
									: MatrixData<Type>(universe, data, def, isCircular) {} \
								Name(IUniverse* universe, DataDefinition def, size_t rows, size_t cols, bool isCircular = false) \
									: MatrixData<Type>(universe, def, rows, cols, isCircular) {} \
								Name(IUniverse* universe, size_t rows, size_t cols, bool isCircular = false) \
									: MatrixData<Type>(universe, DataDefinition::s_##Type, rows, cols, isCircular) {} \
								Name(IUniverse* universe, const std::vector<Type>& vector) \
									: MatrixData<Type>(universe, vector) {} \
								Name(IUniverse* universe, DataDefinition def) \
									: MatrixData<Type>(universe, def) {} \
								explicit Name(IUniverse* universe, const Name& rhs) \
									: MatrixData<Type>(universe, rhs) {} \
	using 						MatrixData::dataSize; \
	using 						MatrixData::rows; \
	using 						MatrixData::cols; \
	using 						MatrixData::makeInvalid; \
	using 						MatrixData::makeAlphaInvalid; \
	using 						MatrixData::isValid; \
	using 						MatrixData::ensureSize; \
	virtual Data*				createInstance() { return new Name(nullptr, Data::m_def, 0, 0); } \
	Data*						clone(IUniverse* universe, bool noCopy) { return !noCopy ? new Name(universe, *this) : new Name(universe, def(), rows(), cols()); } \
	std::string					getString(pesa::IntDay di, size_t ii, size_t kk) const { return Util::cast<Type>(getValue(di, ii)); } \
	inline Type					getValue(IntDay di, size_t ii) const { return (*this)(di, ii); } \
	inline void					setValue(IntDay di, size_t ii, Type value) { (*this)(di, ii) = value; } \
	inline Data*				script_sptr() { return static_cast<Data*>(this); } \
	inline std::vector<Type>	rowArray(IntDay di, bool adjustForUniverse = false) { return MatrixData<Type>::rowArray(di, adjustForUniverse); } \
	inline std::vector<Type>	colArray(size_t ii, size_t maxDays = 0) { return MatrixData<Type>::colArray(ii, maxDays); } \
	inline const Type*			rowPtr(IntDay di) const { return MatrixData<Type>::rowPtr(di); } \
	inline Type*				rowPtr(IntDay di) { return MatrixData<Type>::rowPtr(di); } \
	inline static Name*			script_cast(Data* dptr) { return dynamic_cast<Name*>(dptr); } \
	virtual void				invalidateMemory() { MatrixData<Type>::invalidateMemory(); } \
	virtual const Name*			transpose(size_t kk = 0) const { return dynamic_cast<const Name*>(MatrixData<Type>::transpose(kk)); } \
	virtual const Name*			nanTranspose(Type invalidValue, size_t kk = 0) const { return dynamic_cast<const Name*>(MatrixData<Type>::nanTranspose(invalidValue, kk)); }

	////////////////////////////////////////////////////////////////////////////
	/// IntMatrixData
	////////////////////////////////////////////////////////////////////////////
	class Framework_API IntMatrixData : public MatrixData<int> {
		SCRIPT_STUB(IntMatrixData, int);

		virtual void setString(pesa::IntDay di, size_t ii, size_t kk, const std::string& value) { 
			if (!value.empty())
				(*this)(di, ii, kk) = Util::cast<int>(value);
			else 
				this->makeInvalid(di, ii, kk); 
		}
	};
	typedef std::shared_ptr<IntMatrixData> 		IntMatrixDataPtr;

	////////////////////////////////////////////////////////////////////////////
	/// UShortMatrixData - 16-bit unsigned inteder
	////////////////////////////////////////////////////////////////////////////
	class Framework_API UShortMatrixData : public MatrixData<uint16_t> {
		SCRIPT_STUB(UShortMatrixData, uint16_t);

		virtual void setString(pesa::IntDay di, size_t ii, size_t kk, const std::string& value) { 
			if (!value.empty())
				(*this)(di, ii, kk) = Util::cast<uint16_t>(value);
			else 
				this->makeInvalid(di, ii, kk); 
		}
	};
	typedef std::shared_ptr<UShortMatrixData> 	UShortMatrixDataPtr;

	////////////////////////////////////////////////////////////////////////////
	/// UShortMatrixData - 16-bit unsigned inteder
	////////////////////////////////////////////////////////////////////////////
	class Framework_API ShortMatrixData : public MatrixData<int16_t> {
		SCRIPT_STUB(ShortMatrixData, int16_t);

		virtual void setString(pesa::IntDay di, size_t ii, size_t kk, const std::string& value) { 
			if (!value.empty())
				(*this)(di, ii, kk) = Util::cast<int16_t>(value);
			else 
				this->makeInvalid(di, ii, kk); 
		}
	};
	typedef std::shared_ptr<ShortMatrixData> 	ShortMatrixDataPtr;

	////////////////////////////////////////////////////////////////////////////
	/// Int64MatrixData
	////////////////////////////////////////////////////////////////////////////
	class Framework_API Int64MatrixData : public MatrixData<int64_t> {
		SCRIPT_STUB(Int64MatrixData, int64_t);

		Poco::DateTime							getDTValue(IntDay di, size_t ii) const;
		void									setDTValue(IntDay di, size_t ii, const Poco::DateTime& dt);

		Poco::Timestamp							getTSValue(IntDay di, size_t ii) const;
		void									setTSValue(IntDay di, size_t ii, const Poco::Timestamp& ts);

		float									getPyValue(IntDay di, size_t ii) const;
		void									setPyValue(IntDay di, size_t ii, float ts);

		virtual void							setString(pesa::IntDay di, size_t ii, size_t kk, const std::string& value);
	};
	typedef std::shared_ptr<Int64MatrixData> 	Int64MatrixDataPtr;

	////////////////////////////////////////////////////////////////////////////
	/// UIntMatrixData
	////////////////////////////////////////////////////////////////////////////
	class Framework_API UIntMatrixData : public MatrixData<uint32_t> {
		SCRIPT_STUB(UIntMatrixData, uint32_t);

		virtual void setString(pesa::IntDay di, size_t ii, size_t kk, const std::string& value) { 
			if (!value.empty())
				(*this)(di, ii, kk) = Util::cast<uint32_t>(value);
			else 
				this->makeInvalid(di, ii, kk); 
		}
		inline void setIntValue(IntDay di, size_t ii, int value) { (*this)(di, ii) = (uint32_t)value; } 
	};
	typedef std::shared_ptr<UIntMatrixData>		UIntMatrixDataPtr;

	////////////////////////////////////////////////////////////////////////////
	/// LongMatrixData
	////////////////////////////////////////////////////////////////////////////
	class Framework_API LongMatrixData : public MatrixData<long> {
		SCRIPT_STUB(LongMatrixData, long);

		virtual void setString(pesa::IntDay di, size_t ii, size_t kk, const std::string& value) { 
			if (!value.empty())
				(*this)(di, ii, kk) = Util::cast<long>(value);
			else 
				this->makeInvalid(di, ii, kk); 
		}
	};
	typedef std::shared_ptr<LongMatrixData> 	LongMatrixDataPtr;

	////////////////////////////////////////////////////////////////////////////
	/// ULongMatrixData
	////////////////////////////////////////////////////////////////////////////
	class Framework_API ULongMatrixData : public MatrixData<ulong_t> {
		SCRIPT_STUB(ULongMatrixData, ulong_t);

		virtual void setString(pesa::IntDay di, size_t ii, size_t kk, const std::string& value) { 
			if (!value.empty())
				(*this)(di, ii, kk) = Util::cast<ulong_t>(value);
			else 
				this->makeInvalid(di, ii, kk); 
		}
	};
	typedef std::shared_ptr<ULongMatrixData>	ULongMatrixDataPtr;

	////////////////////////////////////////////////////////////////////////////
	/// DoubleMatrixData
	////////////////////////////////////////////////////////////////////////////
	class Framework_API DoubleMatrixData : public MatrixData<double> {
	public:
		SCRIPT_STUB(DoubleMatrixData, double);

		virtual void setString(pesa::IntDay di, size_t ii, size_t kk, const std::string& value) { 
			if (!value.empty())
				(*this)(di, ii, kk) = Util::cast<double>(value);
			else 
				this->makeInvalid(di, ii, kk); 
		}
	};

	typedef std::shared_ptr<DoubleMatrixData> 	DoubleMatrixDataPtr;
	////////////////////////////////////////////////////////////////////////////
	/// FloatMatrixData
	////////////////////////////////////////////////////////////////////////////
	class Framework_API FloatMatrixData : public MatrixData<float> {
	public:
		SCRIPT_STUB(FloatMatrixData, float);

		virtual MatrixData<float>* createTranspose() const { return new FloatMatrixData(nullptr, this->def()); }

		virtual void setString(pesa::IntDay di, size_t ii, size_t kk, const std::string& value) { 
			if (!value.empty())
				(*this)(di, ii, kk) = Util::cast<float>(value);
			else 
				this->makeInvalid(di, ii, kk); 
		}
	};

	typedef std::shared_ptr<FloatMatrixData> 	FloatMatrixDataPtr;
	typedef std::vector<FloatMatrixDataPtr>		FloatMatrixDataPtrVec;
	typedef std::unordered_map<std::string, FloatMatrixDataPtr> FloatMatrixDataPtrStrMap;

	////////////////////////////////////////////////////////////////////////////
	/// ConstantFloat
	////////////////////////////////////////////////////////////////////////////
	class Framework_API ConstantFloat : public FloatMatrixData {
	public:
					ConstantFloat(float value, DataDefinition def);
					ConstantFloat(float value);

		float 		operator() (IntDay, size_t) const;
		float& 		operator() (IntDay, size_t);
		void 		setValue(IntDay, size_t, float);
		float& 		value();
		float		value() const;
		Data*		clone(IUniverse* universe, bool noCopy);
		Data*		createInstance() { return new ConstantFloat(0.0f); }
		void		ensureCols(size_t di, size_t numCols);
		virtual IntDay DI(IntDay di) const { return 0; }
		//virtual Eigen::Block<Eigen::Array<float, -1, -1, Eigen::RowMajor>, 1, -1, true > row(IntDay, size_t kk = 0) { return MatrixData<float>::row(0, 0); }
		//virtual const Eigen::Block<const Eigen::Array<float, -1, -1, Eigen::RowMajor>, 1, -1, true > row(IntDay, size_t kk = 0) const { return MatrixData<float>::row(0, 0); }
	};
	typedef std::shared_ptr<ConstantFloat> 		ConstantFloatPtr;
} /// namespace pesa 

#undef SCRIPT_STUB

