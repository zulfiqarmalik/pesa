/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// MatrixDataWindow.h
///
/// Created on: 12 Sep 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/FrameworkDefs.h"
#include "MatrixData.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// MatrixDataWindow - Generic circular queue for efficient caching and lazy
	/// loading of large datasets. Wraps around the Data class and implements
	/// the same interface. Can be used seamlessly with any dataset
	////////////////////////////////////////////////////////////////////////////
	template <typename t_type, typename t_matrix>
	class Framework_API MatrixDataWindow : public t_matrix {
	protected:
		typedef std::shared_ptr<t_matrix> MatrixBasePtr;

		struct Row {
			MatrixBasePtr 		data;					/// The actual data
			IntDay 				di = Constants::s_invalidDay; /// The di index of the data
			IntDate				date = 0;				/// The date on this di
		};

		typedef std::vector<Row> RowVec;

		mutable RowVec			m_rowData; 				/// The circular vector data (it is always AT LEAST "backdays" + s_preCacheSize long)
		IDataHandlePtr			m_hData;				/// The data handle
		size_t					m_backdays = 0;			/// The number of backdays
		size_t					m_maxRows = 0;			/// Max number of rows

	public:
		MatrixDataWindow(IUniverse* universe, IDataHandlePtr hData, const DataDefinition& def, size_t backdays, size_t maxRows)
			: t_matrix(universe, def)
			, m_hData(hData)
			, m_backdays(backdays)
			, m_maxRows(maxRows) {
			ASSERT(backdays, "Invalid number of backdays specified!");
		}

		virtual ~MatrixDataWindow() {
		}

		virtual size_t dataSize() const {
			return 0;
		}

		virtual const void* data() const {
			return nullptr;
		}

		virtual void* data() {
			return nullptr;
		}

		void cacheDay(Row& r) const {
			if (!r.data)
				r.data = MatrixBasePtr(new t_matrix(const_cast<IUniverse*>(Data::m_universe), Data::m_def));

			DataRowHeader rhdr;
			m_hData->readRowHeader(r.date, rhdr);
			ASSERT(rhdr.date == r.date, "Invalid header read. Expecting date: " << r.date << " - Found: " << rhdr.date);
			size_t numCols = rhdr.size / Data::itemSize();
			r.data->ensureSize(1, numCols);
			m_hData->readRow(r.date, r.data->data(), r.data->dataSize());
		}

		void buildInitialCache(IntDay diStart) const {
			IntDay diCacheStart = std::max(diStart - (int)(m_backdays), 0);
			m_rowData.resize(m_backdays);
			IntDay size = (IntDay)m_rowData.size();

			for (IntDay index = 0; index <= size && index + diCacheStart < m_maxRows; index++) {
				IntDay di = index + diCacheStart;
				IntDay dstIndex = di % size;
				Row& r = m_rowData[dstIndex];

				r.date = m_hData->mapDate(di);
				r.di = di;
				cacheDay(r);
			}
		}

		void updateCache(IntDay orgDi) const {
			if (!m_rowData.size()) {
				buildInitialCache(orgDi);
				return;
			}
				
			/// Correct the index first ...
			IntDay di = orgDi % (int)m_rowData.size();
			IntDate date = m_hData->mapDate(orgDi);

			/// Then see what we have at this point
			Row& r = m_rowData[di];

			/// If we already have some data and the di matches the day that we're actually looking for 
			/// the data then we're good
			if (r.data && r.date && r.date == date)
				return;

			/// Otherwise we cache some data ...
			/// TODO: For every cache miss, we cache s_preCacheSize number of items
			r.date = date;
			r.di = orgDi;
			cacheDay(r);

		}

		virtual t_type operator() (IntDay di, size_t ii) const {
			return (*this)(di, ii, 0);
		}

		virtual t_type& operator() (IntDay di, size_t ii) {
			return (*this)(di, ii, 0);
		}

		virtual t_type operator() (IntDay di, size_t ii, size_t kk) const {
			updateCache(di);
			IntDay rindex = di % (IntDay)m_rowData.size();
			return (*m_rowData[rindex].data.get())(0, ii, kk);
		}

		virtual t_type& operator() (IntDay di, size_t ii, size_t kk) {
			updateCache(di);
			IntDay rindex = di % (IntDay)m_rowData.size();
			return (*m_rowData[rindex].data.get())(0, ii, kk);
		}

		virtual size_t rows() const {
			return m_maxRows;
		}

		virtual size_t cols() const {
			return 0;
		}

		virtual size_t depth() const {
			return 1;
		}

		virtual bool isValid(pesa::IntDay di, size_t ii) const {
			return isValid(di, ii, 0);
		}

		virtual bool isValid(pesa::IntDay di, size_t ii, size_t kk) const {
			updateCache(di);
			IntDay rindex = di % (IntDay)m_rowData.size();
			MatrixBasePtr data = m_rowData[rindex].data;
			return data->isValid(0, ii, kk);
		}

		virtual void ensureSizeAndDepth(size_t rows, size_t cols, size_t depth, bool copyOld = false) {
		}

		virtual void ensureSizeAt(size_t kk, size_t rows, size_t cols, bool copyOld = false) {
		}

		virtual void ensureSize(size_t rows, size_t cols, bool copyOld = false) {
		}

		virtual Data* clone(IUniverse* universe, bool noCopy) {
			return new MatrixDataWindow<t_type, t_matrix>(*this);
		}

		virtual Data* createInstance() {
			return nullptr;
		}

		////////////////////////////////////////////////////////////////////////////
		/// SCRIPT - DO NOT USE IN C++
		////////////////////////////////////////////////////////////////////////////
		#ifdef __SCRIPT__
			inline const t_type& getValue(int di, int ii) const {
				return (*this)(di, ii);
			}

			inline void setValue(int di, int ii, const t_type& value) const {
				(*this)(di, ii) = value;
			}

			inline Data* script_sptr() {
				return static_cast<Data*>(this);
			}
		#endif 
	};

	////////////////////////////////////////////////////////////////////////////
	class Framework_API FloatMatrixDataWindow : public MatrixDataWindow<float, FloatMatrixData> {
	public:
		FloatMatrixDataWindow(IUniverse* universe, IDataHandlePtr hData, const DataDefinition& def, size_t backdays, size_t numRows)
			: MatrixDataWindow(universe, hData, def, backdays, numRows) {}
	};
	typedef std::shared_ptr<FloatMatrixDataWindow> 		FloatMatrixDataWindowPtr;

	////////////////////////////////////////////////////////////////////////////
	class Framework_API DoubleMatrixDataWindow : public MatrixDataWindow<double, DoubleMatrixData> {
	public:
		DoubleMatrixDataWindow(IUniverse* universe, IDataHandlePtr hData, const DataDefinition& def, size_t backdays, size_t numRows)
			: MatrixDataWindow(universe, hData, def, backdays, numRows) {}
	};
	typedef std::shared_ptr<DoubleMatrixDataWindow> 	DoubleMatrixDataWindowPtr;

	////////////////////////////////////////////////////////////////////////////
	class Framework_API IntMatrixDataWindow : public MatrixDataWindow<int, IntMatrixData>	 {
	public:
		IntMatrixDataWindow(IUniverse* universe, IDataHandlePtr hData, const DataDefinition& def, size_t backdays, size_t numRows)
			: MatrixDataWindow(universe, hData, def, backdays, numRows) {}
	};
	typedef std::shared_ptr<IntMatrixDataWindow> 		IntMatrixDataWindowPtr;

	////////////////////////////////////////////////////////////////////////////
	class Framework_API UIntMatrixDataWindow : public MatrixDataWindow<uint32_t, UIntMatrixData> {
	public:
		UIntMatrixDataWindow(IUniverse* universe, IDataHandlePtr hData, const DataDefinition& def, size_t backdays, size_t numRows)
			: MatrixDataWindow(universe, hData, def, backdays, numRows) {}
	};
	typedef std::shared_ptr<UIntMatrixDataWindow> 		UIntMatrixDataWindowPtr;

	////////////////////////////////////////////////////////////////////////////
	class Framework_API LongMatrixDataWindow : public MatrixDataWindow<long, LongMatrixData>	 {
	public:
		LongMatrixDataWindow(IUniverse* universe, IDataHandlePtr hData, const DataDefinition& def, size_t backdays, size_t numRows)
			: MatrixDataWindow(universe, hData, def, backdays, numRows) {}
	};
	typedef std::shared_ptr<LongMatrixDataWindow> 		LongMatrixDataWindowPtr;

	////////////////////////////////////////////////////////////////////////////
	class Framework_API ULongMatrixDataWindow : public MatrixDataWindow<unsigned long, ULongMatrixData> {
	public:
		ULongMatrixDataWindow(IUniverse* universe, IDataHandlePtr hData, const DataDefinition& def, size_t backdays, size_t numRows)
			: MatrixDataWindow(universe, hData, def, backdays, numRows) {}
	};
	typedef std::shared_ptr<ULongMatrixDataWindow> 		ULongMatrixDataWindowPtr;

	////////////////////////////////////////////////////////////////////////////
	class Framework_API Int64MatrixDataWindow : public MatrixDataWindow<int64_t, Int64MatrixData> {
	public:
		Int64MatrixDataWindow(IUniverse* universe, IDataHandlePtr hData, const DataDefinition& def, size_t backdays, size_t numRows)
			: MatrixDataWindow(universe, hData, def, backdays, numRows) {}
	};
	typedef std::shared_ptr<Int64MatrixDataWindow> 		Int64MatrixDataWindowPtr;
} /// namespace pesa 

