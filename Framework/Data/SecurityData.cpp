/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// SecurityData.cpp
///
/// Created on: 14 May 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "SecurityData.h"

namespace pesa {
	SecurityData::SecurityData(IUniverse* universe, TPtr data, DataDefinition def)
		: CustomVectorData<Security>(universe, data, def) {
		Trace("DR", "Creating SecurityData buffer!");
	}

	SecurityData::SecurityData(IUniverse* universe, std::vector<Security>& data, DataDefinition def)
		: CustomVectorData<Security>(universe, TPtr(new std::vector<Security>(data)), def) {
		Trace("DR", "Creating SecurityData from raw vector!");
	}

	SecurityData::~SecurityData() {
		Trace("DR", "Deleteing SecurityData buffer!");
	}
} /// namespace pesa 
