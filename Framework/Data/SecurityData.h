/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// SecurityData.h
///
/// Created on: 14 May 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/FrameworkDefs.h"
#include "CustomVectorData.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// CustomVectorData - Since vectored data is very common there is a helpful
	/// class that provides a CustomData implementation for std::vector-ed
	/// type data
	////////////////////////////////////////////////////////////////////////////
	class Framework_API SecurityData : public CustomVectorData<Security> {
	public:
		typedef std::vector<Security> t_typeVec;
		typedef std::shared_ptr<std::vector<Security> > TPtr;

	public:
								SecurityData(IUniverse* universe, TPtr data, DataDefinition def);
								SecurityData(IUniverse* universe, std::vector<Security>& data, DataDefinition def);
		virtual 				~SecurityData();

		virtual Data*			clone(IUniverse* universe, bool noCopy) {
			return new SecurityData(universe ? universe : Data::m_universe, CustomVectorData<Security>::m_data, def());
		}

		virtual Data*			createInstance() {
			return new SecurityData(nullptr, nullptr, def());
		}

		inline const Security&	security(size_t index) const {
			index = II(index);
			ASSERT(index < CustomVectorData<Security>::m_data->size(), "Invalid index: " << index << " - Max size: " << CustomVectorData<Security>::m_data->size());
			return CustomVectorData<Security>::m_data->at(index);
		}

		inline Security 		security(size_t index) {
			index = II(index);
			ASSERT(index < CustomVectorData<Security>::m_data->size(), "Invalid index: " << index << " - Max size: " << CustomVectorData<Security>::m_data->size());
			return CustomVectorData<Security>::m_data->at(index);
		}

		inline size_t 			numSecurities() const {
			return numItems();
		}

		////////////////////////////////////////////////////////////////////////////
		/// SCRIPT helper functions - DO NOT USE IN C++
		////////////////////////////////////////////////////////////////////////////
		inline std::vector<Security>& script_vector() {
			return *CustomVectorData<Security>::m_data.get();
		}

		inline Data* script_sptr() {
			return static_cast<Data*>(this);
		}

		inline static SecurityData* script_cast(Data* data) {
			return dynamic_cast<SecurityData*>(data);
		}
	};

	typedef std::shared_ptr<SecurityData> SecurityDataPtr;
} /// namespace pesa 

