﻿/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Statistics.cpp
///
/// Created on: 12 Aug 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Statistics.h"
#include "Poco/Format.h"
#include "Core/Math/Stats.h"
#include "Framework/Pipeline/IDataRegistry.h"
#include "Framework/Components/IUniverse.h"
#include "Framework/Components/IOptimiser.h"
#include "Framework/Data/MatrixData.h"
#include "Poco/String.h"

#include "Framework/Helper/ShLib.h"

#include "Core/Lib/Profiler.h"

namespace pesa {

	DataDefinition Statistics::s_dataDef("statistics", DataType::kCustom, sizeof(Statistics), DataFrequency::kDaily, 0);
	const float Statistics::s_daysInYear = 250.0f;
    const int LifetimeStatistics::s_version = 1;

	//////////////////////////////////////////////////////////////////////////
	/// CustomStatistics
	//////////////////////////////////////////////////////////////////////////
	void CustomStatistics::add(std::vector<const CustomStatistics*> prev) {
	} 

	////////////////////////////////////////////////////////////////////////////
	/// Statistics
	////////////////////////////////////////////////////////////////////////////
	Statistics::~Statistics() {
	}

	void Statistics::finalise() {
		PROFILE_SCOPED();

		returns 		= pnl / bookSize;
		winReturns		= winNotional > 0.0f ? pnlWon / winNotional : 0.0f;
		marginBps 		= 0.0f;
		heldNotional 	= 0.0f;
		holdingPnl 		= 0.0f;
		tradingPnl 		= 0.0f;

		//if (longPnl != 0.0f)
		//	longPnlPct 	= (longPnl / pnl) * 100.0f;
		//if (shortPnl != 0.0f)
		//	shortPnlPct = (shortPnl / pnl) * 100.0f;

		tradedNotional 	= longNotional + std::abs(shortNotional);
	}

	void Statistics::finaliseRelativeToYesterday(const pesa::FloatVec& currAlphaValues, const pesa::FloatVec& prevAlphaValues, 
		const pesa::FloatVec& currReturns, const pesa::FloatVec& alphaPrevPrice) {
		PROFILE_SCOPED();

		/// Just finalise it normally first 
		finalise();

		/// If the previous stats are not valid then we just return after doing a normal finalise
		if (!prevAlphaValues.size() || !currAlphaValues.size()) {
			tradedNotional 	= bookSize;
			heldNotional 	= 0.0f;
			tvr 			= 100.0f;
			return;
		}

		tradedNotional 		= 0.0f;
		tradedShares 		= 0.0f;
		tradingPnl 			= 0.0f;
		heldNotional 		= 0.0f;
		holdingPnl 			= 0.0f;

		ASSERT(prevAlphaValues.size() == currReturns.size(), "Yesterday -> Today size mismatch. Yesterday alpha size: " << prevAlphaValues.size() << " - Yesterday's returns (calculated today): " << currReturns.size());

		for (size_t i = 0; i < prevAlphaValues.size(); i++) {
			float alphaValue = currAlphaValues[i];
			if (std::isnan(alphaValue))
				alphaValue 	= 0.0f;

			float prevAlphaValue = prevAlphaValues[i];
			if (std::isnan(prevAlphaValue))
				prevAlphaValue = 0.0f;

			float valueChange = alphaValue - prevAlphaValue;
			float prevPrice = alphaPrevPrice[i];
			float shareChange = 0.0f;

			if (prevPrice != 0.0f && !std::isnan(prevPrice))
				shareChange = std::abs(valueChange / prevPrice);

			tradedShares 	+= shareChange;
			tradingPnl      += !std::isnan(currReturns[i]) ? valueChange * currReturns[i] : 0.0f;
			//tradingPnl 		+= valueChange * currReturns[i];
			tradedNotional 	+= std::abs(valueChange);
		}

		/// The new day's data may be bigger than the old one (it can NEVER be smaller)!
		for (size_t i = prevAlphaValues.size(); i < currAlphaValues.size(); i++) {
			float alphaValue = currAlphaValues[i];
			float prevPrice = alphaPrevPrice[i];

			if (!std::isnan(alphaValue)) {
				tradedNotional += std::abs(alphaValue); 
				if (!std::isnan(prevPrice))
					tradedShares += alphaValue / prevPrice;
			}
		}

		heldNotional 			= notional - tradedNotional;
		ASSERT(!std::isnan(heldNotional), "Invalid held value!");

		holdingPnl 			= pnl - tradingPnl;

		tvr 				= CumStatistics::calcTvr(tradedNotional, bookSize);
		ASSERT(!std::isnan(tvr), "Invalid turnover: " << tvr);

		marginBps 			= CumStatistics::calcMarginBps(pnl, tradedNotional);
		marginCps 			= CumStatistics::calcMarginCps(pnl, tradedShares);
		returns				= (pnl * 10000.0f) / bookSize; /// To BPS
	}

	io::OutputStream& Statistics::writeMinimal(io::OutputStream& os) {
		PESA_WRITE_VAR(os, date);
		PESA_WRITE_VAR(os, pnl);
		PESA_WRITE_VAR(os, tvr);
		PESA_WRITE_VAR(os, longPnl);
		PESA_WRITE_VAR(os, shortPnl);
		PESA_WRITE_VAR(os, longCount);
		PESA_WRITE_VAR(os, shortCount);
		PESA_WRITE_VAR(os, returns);
		PESA_WRITE_VAR(os, marginBps);
		PESA_WRITE_VAR(os, marginCps);

		return os;
	}

	std::string Statistics::hline(size_t count) {
		static const char* line = "-------------";
		const size_t numItems = count ? count : 17;
		std::string s;

		for (size_t i = 0; i < numItems; i++)
			s += line;

		s += "-";

		return s;
	}

	static std::string format(const std::string& fmt, const Poco::Any& value1, const Poco::Any& value2,
		const Poco::Any& value3, const Poco::Any& value4, const Poco::Any& value5, const Poco::Any& value6,
		const Poco::Any& value7, const Poco::Any& value8, const Poco::Any& value9, const Poco::Any& value10,
		const Poco::Any& value11, const Poco::Any& value12, const Poco::Any& value13, const Poco::Any& value14,
		const Poco::Any& value15, const Poco::Any& value16, const Poco::Any& value17, const Poco::Any& value18,
		const Poco::Any& value19) {
		std::vector<Poco::Any> args;
		std::string result;

		args.push_back(value1); args.push_back(value2); args.push_back(value3); args.push_back(value4); args.push_back(value5); args.push_back(value6);
		args.push_back(value7); args.push_back(value8); args.push_back(value9); args.push_back(value10); args.push_back(value11);
		args.push_back(value12); args.push_back(value13); args.push_back(value14); args.push_back(value15); args.push_back(value16);
		args.push_back(value17); args.push_back(value18); args.push_back(value19); 

		Poco::format(result, fmt, args);

		return result;
	}

	static std::string format(const std::string& fmt, const Poco::Any& value1, const Poco::Any& value2,
		const Poco::Any& value3, const Poco::Any& value4, const Poco::Any& value5, const Poco::Any& value6,
		const Poco::Any& value7, const Poco::Any& value8, const Poco::Any& value9, const Poco::Any& value10,
		const Poco::Any& value11, const Poco::Any& value12, const Poco::Any& value13, const Poco::Any& value14,
		const Poco::Any& value15, const Poco::Any& value16, const Poco::Any& value17, const Poco::Any& value18, 
		const Poco::Any& value19, const Poco::Any& value20, const Poco::Any& value21, const Poco::Any& value22) {
		std::vector<Poco::Any> args;
		std::string result;

		args.push_back(value1); args.push_back(value2); args.push_back(value3); args.push_back(value4); args.push_back(value5); args.push_back(value6);
		args.push_back(value7); args.push_back(value8); args.push_back(value9); args.push_back(value10); args.push_back(value11);
		args.push_back(value12); args.push_back(value13); args.push_back(value14); args.push_back(value15); args.push_back(value16);
		args.push_back(value17); args.push_back(value18); args.push_back(value19); args.push_back(value20); args.push_back(value21); 
		args.push_back(value22); 

		Poco::format(result, fmt, args);

		return result;
	}

	std::string Statistics::headerString(bool tabular, bool underline) {
		std::string s;

		if (tabular)
			s += "|";

		s =
			format("%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s",
				std::string("Date"),
				std::string("$ BookSize"),
				std::string("$ Cum. P&L"),
				std::string("$ D. P&L"),
				std::string("D. RET(bps)"),
				std::string("D. TVR %"),
				std::string("Mrgn BPS"),
				std::string("Mrgn CPS"),
				std::string("$ Traded"),
				std::string("$ Held"),
				std::string("$ Long"),
				std::string("$ Short"),
				std::string("$ Liquidate"),
				std::string("Long Shrs."),
				std::string("Short Shrs."),
				std::string("Liq. Shrs."),
				std::string("Num Long"),
				std::string("Num Short"),
				std::string("Num Liq."),
				std::string("Cost"),
				std::string("$ L. P&L"),
				std::string("$ S. P&L")
			);

		if (tabular) {
			s += "|\n";
			if (underline)
				s += hline() + "\n";
		}
		else {
			/// We do not put newlines and let the user deal with non-tabular data
			//s += "\n";
			s = Poco::replace(s, "|", ",");
		}

		return s;
	}

	std::string Statistics::toString(bool tabular, bool header, const LifetimeStatistics* ltStats) const {
		std::string s;

		if (tabular) {
			s += hline() + "\n";
			if (header)
				s += headerString(tabular, true);
			s += "|";
		}

		s += format("%12d|%12.2hf|%12.2hf|%12.2hf|%12.2hf|%12.2hf|%12.2hf|%12.2hf|%12.2hf|%12.2hf|%12.2hf|%12.2hf|%12.2hf|%12d|%12d|%12d|%12.2hf|%12.2hf|%12.2hf",
			date, bookSize, ltStats ? ltStats->pnl : 0.0f, pnl, returns, tvr, marginBps, marginCps, tradedNotional, heldNotional, longNotional, shortNotional, liquidateNotional,
			(int)longCount, (int)shortCount, (int)liquidateCount, cost, longPnl, shortPnl);

		if (tabular) {
			s += "|\n";
			s += hline();
		}
		else {
			/// We do not put newlines and let the user deal with non-tabular data
			//s += "\n";
			s = Poco::replace(s, "|", ",");
		}

		return s;
	}

	io::InputStream& Statistics::read(io::InputStream& is) {
		PESA_READ_VAR(is, date);
		PESA_READ_VAR(is, pnl);
		PESA_READ_VAR(is, tvr);
		PESA_READ_VAR(is, longPnl);
		PESA_READ_VAR(is, shortPnl);

		PESA_READ_VAR(is, bookSize);
		//PESA_READ_VAR(is, totalValue);

		PESA_READ_VAR(is, tradedNotional);
		PESA_READ_VAR(is, tradedShares);
		PESA_READ_VAR(is, tradingPnl);

		//PESA_READ_VAR(is, heldValue);
		//PESA_READ_VAR(is, holdingPnl);

		PESA_READ_VAR(is, shortNotional);
		PESA_READ_VAR(is, longNotional);
		PESA_READ_VAR(is, liquidateNotional);

		//PESA_READ_VAR(is, shortShares);
		//PESA_READ_VAR(is, longShares);
		//PESA_READ_VAR(is, numLiquidate);

		//PESA_READ_VAR(is, cost);
		//PESA_READ_VAR(is, universeSize);
		
		PESA_READ_VAR(is, longCount);
		PESA_READ_VAR(is, shortCount);
		PESA_READ_VAR(is, liquidateCount);

		PESA_READ_VAR(is, returns);
		PESA_READ_VAR(is, cost);

		PESA_READ_VAR(is, marginBps);
		//PESA_READ_VAR(is, marginCps);

		/// Calculate things that we can calculate from the existing data from the DB
		//longPnlPct		= (longPnl / pnl) * 100.0f;
		//shortPnlPct		= (shortPnl / pnl) * 100.0f;
		notional		= longNotional + std::abs(shortNotional);
		heldNotional	= notional - tradedNotional;
		holdingPnl		= pnl - tradingPnl;
		//marginBps		= CumStatistics::calcMarginBps(pnl, tradedNotional);
		marginCps		= CumStatistics::calcMarginCps(pnl, tradedShares);

		return is;
	}

	io::OutputStream& Statistics::write(io::OutputStream& os) {
		PESA_WRITE_VAR(os, date);
		PESA_WRITE_VAR(os, pnl);
		PESA_WRITE_VAR(os, tvr);
		PESA_WRITE_VAR(os, longPnl);
		PESA_WRITE_VAR(os, shortPnl);

		PESA_WRITE_VAR(os, bookSize);

		PESA_WRITE_VAR(os, tradedNotional);
		PESA_WRITE_VAR(os, tradedShares);
		PESA_WRITE_VAR(os, tradingPnl);

		PESA_WRITE_VAR(os, shortNotional);
		PESA_WRITE_VAR(os, longNotional);
		PESA_WRITE_VAR(os, liquidateNotional);

		//PESA_WRITE_VAR(os, shortShares);
		//PESA_WRITE_VAR(os, longShares);
		//PESA_WRITE_VAR(os, numLiquidate);

		PESA_WRITE_VAR(os, longCount);
		PESA_WRITE_VAR(os, shortCount);
		PESA_WRITE_VAR(os, liquidateCount);

		PESA_WRITE_VAR(os, returns);
		PESA_WRITE_VAR(os, cost);

		PESA_WRITE_VAR(os, marginBps);

		return os;
	}

	//std::string Statistics::toSimpleString() const {
	//	//return format("%12u, %12.2hf, %12.2hf, %12.2hf, %12.2hf, %12.2hf, %12.2hf, %12.2hf, %12.2hf, %12.6hf, %12.2hf",
	//	//	date, bookSize, pnl, tradedValue, heldValue, longValue, shortValue, numLong, numShort, tvr, cost);
	//	return toString(true, false);
	//}

	//////////////////////////////////////////////////////////////////////////
	/// CumStatistics 
	//////////////////////////////////////////////////////////////////////////
	void CumStatistics::calculate(pesa::CumStatistics* src, size_t start, size_t end) {
		/// we just have the current point in the list ... no point calculating stats ...
		//if (!size())
		//	return;

		CumStatistics::calculate(src, *this, start, end);
	}

	void CumStatistics::calculate(pesa::CumStatistics* src, pesa::CumStatistics& curr, size_t start, size_t end) {
		PROFILE_SCOPED();

		size_t count = src->size();

		ASSERT(end >= start, "Invalid start/end. Start: " << start << " - End: " << end);
		ASSERT(start < count && end <= count, "Start/end out of bounds. Array size: " << count << " - Start: " << start << " - End: " << end);

		for (size_t i = start; i < end; i++) {
			CumStatistics prev = curr;
			((Statistics&)curr) = *(src->at(i));
			add(curr, prev, i + 1);
		}
	}

	void CumStatistics::calculateHigherStats() {
		pnlSkew		= 0.0f;
		pnlKurtosis = 0.0f;
		rsquared	= 0.0f;
		size_t len	= this->size();

		/// Find the correlation coefficient using the equation below
		/// 
		/// r = [n(Σxy) - (Σx)(Σy)] / [sqrt([nΣx² - (Σx)²][nΣy² - (Σy)²])]
		/// Where:
		///		x: This is the pnl vector. Essentially a vector of stats[i].pnl value
		///     y: The number of days. Essentially a vector of [1, 2, 3, ... n] where n = stats.size()
		///     n: The length of the vector = stats.size()

		/// Note that Σy = n(a1 + an) / 2 since y is an arithmetic progression (https://en.wikipedia.org/wiki/Arithmetic_progression)
		/// Note that Σy = pnl (this is already accumulated)
		/// While we can calculate the sum of square of y using equation given at: http://pballew.blogspot.co.uk/2012/12/my-formula-for-series-of-squares-of.html
		/// we DON'T really need to since we'll be iterating the array of pnl's anyway ...

		/// We will use a single iteration to calculate everything ...
		float n = (float)len;
		float y_Sum = n * (1.0f + n) * 0.5f;
		float x_Sum = pnl; 
		float y_SumSq = 0.0f;
		float x_SumSq = 0.0f;
		float xy_Sum = 0.0f;

		for (size_t i = 0; i < len; i++) {
			const auto* si	= this->at(i);
			float yi		= (float)(i + 1);
			float xi		= si->pnl;

			y_SumSq			+= (yi * yi);
			x_SumSq			+= (xi * xi);
			xy_Sum			+= (xi * yi);

			float xiN		= (xi - pnlMean) / pnlStdDev;
			pnlSkew			+= ((xiN * xiN * xiN) - pnlSkew) / yi;
			pnlKurtosis		+= ((xiN * xiN * xiN * xiN) - pnlKurtosis) / yi;
		}

		pnlKurtosis			-= 3.0f;

		float numer			= (n * xy_Sum) - (x_Sum * y_Sum);
		float denom			= ((n * x_SumSq) - (x_Sum * x_Sum)) * ((n * y_SumSq) - (y_Sum * y_Sum));

		if (denom) {
			/// Calculate correlation coefficient ... then square it!
			rsquared		= numer / std::sqrt(denom);
			rsquared		*= rsquared;
		}
	}

	std::string CumStatistics::detailedHeaderString(bool tabular, bool underline) {
		std::string s;

		if (tabular)
			s += "|";

		s =
			format("%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s|%12s",
				std::string("Date"),
				std::string("$ BookSize"),
				std::string("Num Long"),
				std::string("Num Short"),
				std::string("$ P&L"),
				std::string("IR"),
				std::string("Returns"),
				std::string("TVR"),
				std::string("Max. DD"),
				std::string("Max. DH"),
				std::string("HitRatio"),
				std::string("Mrgn BPS"),
				std::string("Mrgn CPS"),
				std::string("MinRET(bps)"),
				std::string("MaxRET(bps)"),
				std::string("$ Traded"),
				std::string("$ Held"),
				std::string("$ Long"),
				std::string("$ Short"),
				std::string("$ Liquidate"),
				std::string("Num Liq."),
				std::string("Cost")
			);

		if (tabular) {
			s += "|\n";
			if (underline)
				s += hline(24) + "\n";
		}
		else {
			/// We do not put newlines and let the user deal with non-tabular data
			//s += "\n";
			s = Poco::replace(s, "|", ",");
		}

		return s;
	}

	std::string CumStatistics::detailedString(bool tabular, bool header, float dayPnl) const {
		std::string s;

		if (tabular) {
			s += hline(24) + "\n";
			if (header)
				s += headerString(tabular, true);
			s += "|";
		}

		if (std::abs(dayPnl) < 0.0001f)
			dayPnl = pnl;

		s += format("%12d|%12.2hf|%12d|%12d|%12.2hf|%12.4hf|%12.2hf|%12.2hf|%12.2hf|%12d|%12.2hf|%12.2hf|%12.2hf|%12.2hf|%12.2hf|%12.2hf|%12.2hf|%12.2hf|%12.2hf|%12.2hf|%12d|%12.2hf",
			date, bookSize, (int)longCount, (int)shortCount, pnl, ir, returns, tvr, -maxDD, maxDH, hitRatio, marginBps, marginCps, minReturns, maxReturns,
			tradedNotional, heldNotional, longNotional, shortNotional, liquidateNotional, (int)liquidateCount, cost);

		if (tabular) {
			s += "|\n";
			s += hline(24);
		}
		else {
			/// We do not put newlines and let the user deal with non-tabular data
			//s += "\n";
			s = Poco::replace(s, "|", ",");
		}

		return s;
	}

	void CumStatistics::add(CumStatistics& curr, const CumStatistics& prev, size_t totalSize) {
		PROFILE_SCOPED();

		float fsize			= (float)totalSize;
		float invSize		= 1.0f / fsize;	/// Might accumulate some rounding errors but shouldn't be too much of a problem (since max size shouldn't go above a few hundred)

		if (curr.bookSize < 0.0001f)
			curr.bookSize	= prev.bookSize;

		if (!curr.date)
			curr.date		= prev.date;

		curr.minReturns		= std::min(curr.returns, curr.minReturns);
		curr.maxReturns		= std::max(curr.returns, curr.maxReturns);

		auto pointPnl		= curr.pnl;
		curr.numDays 		= prev.numDays + 1;
		curr.numWon 		= prev.numWon + (curr.pnl >= 0.0f ? 1 : 0);
		size_t numDownDays	= curr.numDays - curr.numWon;

		curr.hitRatio 		= (float)curr.numWon / (float)curr.numDays;
		curr.pnl 			+= prev.pnl;
		curr.squaredPnl		= prev.squaredPnl + (pointPnl * pointPnl);
		curr.squaredNegPnl	= prev.squaredNegPnl + (pointPnl < 0.0f ? pointPnl * pointPnl : 0.0f);
		curr.cost 			+= prev.cost;
		curr.pnlMean 		= curr.pnl * invSize;			/// pnl / stats.size()
		curr.returns 		= ((curr.pnl * 100.0f) / curr.bookSize) * s_daysInYear * invSize; /// Annualise it!
		curr.pnlWon			+= prev.pnlWon;
		curr.winNotional	+= prev.winNotional;
		curr.winReturns		= curr.winNotional > 0.0f ? ((curr.pnlWon * 100.0f) / curr.winNotional) * s_daysInYear * invSize : 0.0f; /// Annualise it!

		curr.pnlNegMean		= numDownDays > 0 ? Stats::iterativeAverage(std::abs(curr.pnl - curr.pnlWon), prev.pnlNegMean, (float)numDownDays, 1.0f / (float)numDownDays) : 0.0f;

		curr.longPnl		+= prev.longPnl;
		curr.shortPnl		+= prev.shortPnl;
		//curr.longPnlPct		= (curr.longPnl / curr.pnl) * 100.0f;
		//curr.shortPnlPct	= (curr.shortPnl / curr.pnl) * 100.0f;

		//float delta 		= (pointPnl - curr.pnlMean);
		float squaredPnlMean= curr.squaredPnl * invSize;
		float pnlVariance 	= (squaredPnlMean - (curr.pnlMean * curr.pnlMean));

		float squaredNegPnlMean = numDownDays > 0 ? curr.squaredNegPnl / (float)numDownDays : 0.0f;
		float negPnlVariance = (squaredPnlMean - (curr.pnlNegMean * curr.pnlNegMean));

		curr.pnlNegStdDev	= sqrt(negPnlVariance);

		//curr.pnlVariance 	*= invSize;						/// pnlVariance / stats.size() 
		curr.pnlStdDev 		= sqrt(pnlVariance);
		curr.ir 			= curr.pnlStdDev != 0.0f ? curr.pnlMean / curr.pnlStdDev : 0.0f;
		curr.volatility 	= ((curr.pnlStdDev * 100.0f) / curr.bookSize) * sqrt(invSize * s_daysInYear); /// Annualise it!

		//curr.longShares 	= Stats::iterativeAverage(curr.longShares, prev.longShares, fsize, invSize);
		//curr.shortShares 	= Stats::iterativeAverage(curr.shortShares, prev.shortShares, fsize, invSize);
		curr.longNotional 		= Stats::iterativeAverage(curr.longNotional, prev.longNotional, fsize, invSize);
		curr.liquidateNotional = Stats::iterativeAverage(curr.liquidateNotional, prev.liquidateNotional, fsize, invSize);
		curr.shortNotional 	= Stats::iterativeAverage(curr.shortNotional, prev.shortNotional, fsize, invSize);
		curr.notional 		= curr.longNotional + std::abs(curr.shortNotional);
		curr.heldNotional 	= curr.notional - Stats::iterativeAverage(curr.tradedNotional, prev.tradedNotional, fsize, invSize);
		curr.tradedNotional += prev.tradedNotional;
		curr.tradedShares	+= prev.tradedShares;
		curr.tradingPnl 	+= prev.tradingPnl;
		curr.longCount		= Stats::iterativeAverage(curr.longCount, prev.longCount, fsize, invSize);
		curr.shortCount		= Stats::iterativeAverage(curr.shortCount, prev.shortCount, fsize, invSize);
		curr.liquidateCount = Stats::iterativeAverage(curr.liquidateCount, prev.liquidateCount, fsize, invSize);
		curr.holdingPnl 	= curr.pnl - curr.tradingPnl;
		curr.tvr			= calcTvr(curr.tradedNotional, curr.bookSize * totalSize);
		curr.maxTvr 		= std::max(curr.tvr, prev.tvr);
		curr.marginBps		= calcMarginBps(curr.pnl, curr.tradedNotional);
		curr.marginCps		= calcMarginCps(curr.pnl, curr.tradedShares);

		ASSERT(!std::isnan(curr.notional), "Invalid total value!");
		ASSERT(!std::isnan(curr.heldNotional), "Invalid held value!");

		/// Most common scenario ... we're belowe the max high that was achieved
		if (curr.pnl < prev.ddMaxPnl) {
			curr.ddMaxPnl 	= prev.ddMaxPnl;
			curr.dd 		= CumStatistics::calcDD(curr.ddMaxPnl - curr.pnl, curr.bookSize);
			curr.dh 		= prev.dh + 1;

			if (curr.pnl < prev.ddMinPnl) 
				curr.ddMinPnl = curr.pnl;
			else
				curr.ddMinPnl = prev.ddMinPnl;
		}

		/// We have a new high!
		else {
			curr.ddMaxPnl 	= curr.pnl;
			curr.dh 		= 0;
			curr.dd 		= 0.0f;
			curr.ddMinPnl 	= prev.ddMinPnl;
			curr.maxDH 		= 0;
		}

		if (curr.pnl > prev.ddMinPnl) {
			curr.du			= CumStatistics::calcDD(std::abs(curr.pnl - curr.ddMinPnl), curr.bookSize);
			curr.duh		= prev.duh + 1;
		}
		else {
			curr.du			= 0.0f;
			curr.duh		= 0;
		}

		curr.maxDD			= std::max(curr.dd, prev.maxDD);
		curr.maxDH			= std::max(curr.dh, prev.maxDH);

		curr.maxDU			= std::max(curr.du, prev.maxDU);
		curr.maxDUH			= std::max(curr.duh, prev.maxDUH);

		curr.sortinoRatio	= curr.pnlNegStdDev > 0.0f ? curr.pnlMean / curr.pnlNegStdDev : 0.0f;
		curr.calmar			= curr.maxDD > 0.0f ? curr.returns / curr.maxDD : 0.0f;

		curr.riskHistBeta	= Stats::iterativeAverage(curr.riskHistBeta, prev.riskHistBeta, fsize, invSize);
		curr.riskPredBeta	= Stats::iterativeAverage(curr.riskPredBeta, prev.riskPredBeta, fsize, invSize);
		curr.factorBeta		= Stats::iterativeAverage(curr.factorBeta, prev.factorBeta, fsize, invSize);
		curr.factorMomentum	= Stats::iterativeAverage(curr.factorMomentum, prev.factorMomentum, fsize, invSize);
		curr.factorValue	= Stats::iterativeAverage(curr.factorValue, prev.factorValue, fsize, invSize);
		curr.factorSize		= Stats::iterativeAverage(curr.factorSize, prev.factorSize, fsize, invSize);
		curr.factorGrowth	= Stats::iterativeAverage(curr.factorGrowth, prev.factorGrowth, fsize, invSize);
		curr.factorLeverage	= Stats::iterativeAverage(curr.factorLeverage, prev.factorLeverage, fsize, invSize);
		curr.factorVolatility= Stats::iterativeAverage(curr.factorVolatility, prev.factorVolatility, fsize, invSize);
		curr.factorMarketSens= Stats::iterativeAverage(curr.factorMarketSens, prev.factorMarketSens, fsize, invSize);
	}

	float CumStatistics::calcDD(float dd, float bookSize) {
		return ((dd * 100.0f)) / bookSize;
	}

	float CumStatistics::calcTvr(float tradedValue, float bookSize) {
		return ((tradedValue * 100.0f) / bookSize); /// multiply first to minimise rounding errors
	}

	float CumStatistics::calcMarginBps(float pnl, float tradedValue) {
		if (tradedValue == 0.0f || std::isnan(pnl) || std::isnan(tradedValue))
			return 0.0f; 

		return (pnl / tradedValue) * 1.0e4f;
	}

	float CumStatistics::calcMarginCps(float pnl, float tradedShares) {
		if (tradedShares == 0.0f || std::isnan(pnl) || std::isnan(tradedShares))
			return 0.0f;

		return (pnl / tradedShares) * 100.0f;
	}

	void CumStatistics::finalise() {
		PROFILE_SCOPED();

		Statistics::finalise();

		pnlMean 			= pnl;
		pnlStdDev 			= 0.0f;
		ir 					= 0.0f;
		maxDD 				= 0.0f;
		dd 					= 0.0f;
		maxDU 				= 0.0f;
		du 					= 0.0f;
		volatility 			= 0.0f;

		dh 					= 0;
		maxDH 				= 0;
		duh 				= 0;
		maxDUH 				= 0;

		numWon 				= pnl > 0.0f ? 1 : 0;
		numDays 			= 1;
		hitRatio 			= (float)numWon / (float)numDays;
		tvr 				= CumStatistics::calcTvr(tradedNotional, bookSize);
		ASSERT(!std::isnan(tvr), "Invalid turnover: " << tvr);
		maxTvr 				= tvr;
	}

	io::InputStream& CumStatistics::read(io::InputStream& is) {
		Statistics::read(is);

		PESA_READ_VAR(is, numDays);
		PESA_READ_VAR(is, numWon);
		PESA_READ_VAR(is, ddMaxPnl);
		PESA_READ_VAR(is, ddMinPnl);
		PESA_READ_VAR(is, ir);
		PESA_READ_VAR(is, hitRatio);
		PESA_READ_VAR(is, maxTvr);

		PESA_READ_VAR(is, maxDD);
		PESA_READ_VAR(is, dd);
		PESA_READ_VAR(is, dh);
		PESA_READ_VAR(is, maxDH);

		PESA_READ_VAR(is, maxDU);
		PESA_READ_VAR(is, du);
		PESA_READ_VAR(is, duh);
		PESA_READ_VAR(is, maxDUH);

		PESA_READ_VAR(is, volatility);
		PESA_READ_VAR(is, pnlMean);
		PESA_READ_VAR(is, pnlStdDev);
		PESA_READ_VAR(is, squaredPnl);

		PESA_READ_VAR(is, pnlSkew);
		PESA_READ_VAR(is, pnlKurtosis);
		PESA_READ_VAR(is, rsquared);

		PESA_READ_VAR(is, pnlNegMean);
		PESA_READ_VAR(is, squaredNegPnl);
		PESA_READ_VAR(is, pnlNegStdDev);

		PESA_READ_VAR(is, sortinoRatio);
		PESA_READ_VAR(is, calmar);

		PESA_READ_VAR(is, minReturns);
		PESA_READ_VAR(is, maxReturns);

		return is;
	}

	io::OutputStream& CumStatistics::write(io::OutputStream& os) {
		Statistics::write(os); 

		PESA_WRITE_VAR(os, numDays);
		PESA_WRITE_VAR(os, numWon);
		PESA_WRITE_VAR(os, ddMaxPnl);
		PESA_WRITE_VAR(os, ddMinPnl);
		PESA_WRITE_VAR(os, ir);
		PESA_WRITE_VAR(os, hitRatio);
		PESA_WRITE_VAR(os, maxTvr);

		PESA_WRITE_VAR(os, maxDD);
		PESA_WRITE_VAR(os, dd);
		PESA_WRITE_VAR(os, dh);
		PESA_WRITE_VAR(os, maxDH);

		PESA_WRITE_VAR(os, maxDU);
		PESA_WRITE_VAR(os, du);
		PESA_WRITE_VAR(os, duh);
		PESA_WRITE_VAR(os, maxDUH);

		PESA_WRITE_VAR(os, volatility);
		PESA_WRITE_VAR(os, pnlMean);
		PESA_WRITE_VAR(os, pnlStdDev);
		PESA_WRITE_VAR(os, squaredPnl);

		PESA_WRITE_VAR(os, pnlSkew);
		PESA_WRITE_VAR(os, pnlKurtosis);
		PESA_WRITE_VAR(os, rsquared);

		PESA_WRITE_VAR(os, pnlNegMean);
		PESA_WRITE_VAR(os, squaredNegPnl);
		PESA_WRITE_VAR(os, pnlNegStdDev);

		PESA_WRITE_VAR(os, sortinoRatio);
		PESA_WRITE_VAR(os, calmar);

		PESA_WRITE_VAR(os, minReturns);
		PESA_WRITE_VAR(os, maxReturns);

		return os;
	}

	io::OutputStream& CumStatistics::writeMinimal(io::OutputStream& os) {
		Statistics::writeMinimal(os);

		PESA_WRITE_VAR(os, volatility);
		PESA_WRITE_VAR(os, ir);
		PESA_WRITE_VAR(os, hitRatio);
		PESA_WRITE_VAR(os, maxTvr);

		PESA_WRITE_VAR(os, maxDD);
		PESA_WRITE_VAR(os, maxDH);

		PESA_WRITE_VAR(os, maxDU);
		PESA_WRITE_VAR(os, maxDUH);

		return os;
	}

	////////////////////////////////////////////////////////////////////////////
	/// RangeStatistics - A list of stats
	////////////////////////////////////////////////////////////////////////////
	RangeStatistics::RangeStatistics(size_t maxLimit_ /* = 0 */)
		: maxLimit(maxLimit_) {
	}

	void RangeStatistics::clear() {
		vec.clear();
	}

	size_t RangeStatistics::add(const Statistics& stats, bool doUpdate /* = true */) {
		PROFILE_SCOPED();

		vec.push_back(stats);

		CumStatistics prevStats = *this;
		((Statistics&)*this) = stats;

		/// If we've gone past our max limit then delete the front of the vector
		if (maxLimit > 0 && vec.size() > maxLimit) {
			vec.erase(vec.begin());
			ASSERT(maxLimit == 0 || vec.size() <= maxLimit, "Max limit overflow. Max Limit: " << maxLimit << " - Current Size: " << vec.size());
			/// We need to recalculate the net stats ...
			if (doUpdate)
				calculate(0, vec.size());
		}
		else if (doUpdate)
			CumStatistics::add(*this, prevStats, vec.size());

		recalculateDates();

		return vec.size() - 1;
	}

	int RangeStatistics::findStats(const Statistics& stats, int start) const {
		PROFILE_SCOPED();

		/// if its an update to an existing stats then we just update it ...
		for (size_t i = 0; i < vec.size(); i++) {
			const Statistics& existingStats = vec[i];

			if (existingStats.date == stats.date) {
				return (int)i;
			}
		}

		return -1;
	}

	size_t RangeStatistics::addDelta(const Statistics& stats, int start, bool* addedNew) {
		PROFILE_SCOPED();

		int index = findStats(stats, start);

		if (index >= 0) {
			if (addedNew)
				*addedNew = false;

			vec[index] = stats;
			return (size_t)index;
		}

		if (addedNew)
			*addedNew = true;

		/// ... otherwise we add normally
		add(stats, false);
		return vec.size() - 1;
	}

	void RangeStatistics::calculate(size_t start, size_t end) {
		PROFILE_SCOPED();

		ASSERT(end > start, "Invalid start/end. Start: " << start << " - End: " << end);
		ASSERT(start < vec.size() && end <= vec.size(), "Start/end out of bounds. Array size: " << vec.size() << " - Start: " << start << " - End: " << end);

		/// Copy the first point from the start of the array
		CumStatistics newStats;
		((Statistics&)newStats) = vec[start];

		newStats.calculate(this, start + 1, end);
		((CumStatistics&)*this) = newStats;
	}

	void RangeStatistics::recalculate() {
		if (!vec.size())
			return;

		RangeStatistics::calculate(0, vec.size());
		RangeStatistics::calculateHigherStats();
	}

	const Statistics& RangeStatistics::operator[](size_t i) const {
		ASSERT(i < vec.size(), "Array index out of bounds. Size: " << vec.size() << " - Index: " << i);
		return vec[i];
	}

	Statistics& RangeStatistics::operator[](size_t i) {
		ASSERT(i < vec.size(), "Array index out of bounds. Size: " << vec.size() << " - Index: " << i);
		return vec[i];
	}

	size_t RangeStatistics::memSize() const {
		return vec.size() * sizeof(Statistics) + sizeof(RangeStatistics);
	}

	size_t RangeStatistics::memSize(const std::vector<RangeStatistics>& rsVec) {
		size_t size = 0;

		for (const auto& rs : rsVec)
			size += rs.memSize();

		return size;
	}

	io::InputStream& RangeStatistics::read(io::InputStream& is) {
		PESA_READ_VAR(is, startDate);
		PESA_READ_VAR(is, endDate);

		CumStatistics::read(is);

		PESA_READ_VAR(is, maxLimit);
		PESA_READ_OBJ_VEC(is, vec, Statistics());

		return is;
	}

	io::OutputStream& RangeStatistics::write(io::OutputStream& os) {
		PESA_WRITE_VAR(os, startDate);
		PESA_WRITE_VAR(os, endDate);

		CumStatistics::write(os);

		PESA_WRITE_VAR(os, maxLimit);
		PESA_WRITE_OBJ_VEC(os, vec);

		return os;
	}

	io::OutputStream& RangeStatistics::writeDelta(io::OutputStream& os, const IntVec& indexesUpdated, const IntVec& indexesAdded) {
		recalculateDates();
		PESA_WRITE_VAR(os, startDate);
		PESA_WRITE_VAR(os, endDate);

		/// Recalculate and write the stats ...
		recalculate();
		CumStatistics::write(os);

		PESA_WRITE_VAR(os, maxLimit);

		/// Now we update the indexes that have been update
		for (auto index : indexesUpdated) {
			auto& s = vec[index];
			os.write(("vec." + Util::cast(index)).c_str(), s);
		}

		for (size_t i = 0; i < indexesAdded.size(); i++) {
			auto& s = vec[indexesAdded[i]];
			os.write(("vec." + Util::cast(indexesAdded[i])).c_str(), s);
		}

		return os;
	}

	const Statistics* RangeStatistics::at(size_t i) const {
		ASSERT(i < vec.size(), "Invalid index requested: " << i << " [Max: " << vec.size() << "]");
		const Statistics& s = vec.at(i);
		return &s;
	}

	size_t RangeStatistics::size() const {
		return vec.size();
	}

	void RangeStatistics::recalculateDates() {
		if (!vec.size())
			return;
		startDate = vec[0].date;
		endDate = vec[vec.size() - 1].date;
	} 

	//////////////////////////////////////////////////////////////////////////
	/// IndexedRangeStatistics
	//////////////////////////////////////////////////////////////////////////
	IndexedRangeStatistics::IndexedRangeStatistics(size_t maxLimit_ /* = 0 */)
		: maxLimit(maxLimit_) {
	}

	IndexedRangeStatistics::IndexedRangeStatistics(LifetimeStatistics* parent_, size_t maxLimit_ /* = 0 */)
		: parent(parent_)
		, maxLimit(maxLimit_) {
	}

	void IndexedRangeStatistics::clear() {
		indexes.clear();
	}

	size_t IndexedRangeStatistics::size() const {
		return indexes.size();
	}

	const Statistics* IndexedRangeStatistics::at(size_t i) const {
		ASSERT(parent, "No parent to indexed range!");
		ASSERT(i < indexes.size(), "Invalid index: " << i << " [Max: " << indexes.size() << "]");

		return parent->at(indexes[i]);
	}

	void IndexedRangeStatistics::resolveIndexes(StatisticsVec& vec, size_t start) {
		ASSERT(parent, "No parent to indexed range!");

		vec.resize(indexes.size());

		for (size_t i = start; i < indexes.size(); i++)
			vec[i] = *(parent->at(indexes[i]));
	}

	void IndexedRangeStatistics::add(const Statistics& stats, size_t index_, bool doUpdate /* = true */) {
		PROFILE_SCOPED();

		int index = (int)index_;
		indexes.push_back(index);

		/// We are not going to recalculate anything ...
		if (!doUpdate)
			return;

		CumStatistics prevStats = *this;
		((Statistics&)*this) = stats;

		/// If we've gone past our max limit then delete the front of the vector
		if (maxLimit > 0 && indexes.size() > maxLimit) {
			indexes.erase(indexes.begin());
			ASSERT(maxLimit == 0 || indexes.size() <= maxLimit, "Max limit overflow. Max Limit: " << maxLimit << " - Current Size: " << indexes.size());
			/// We need to recalculate the net stats ...
			if (doUpdate)
				calculate(0, indexes.size());
		}
		else if (doUpdate)
			CumStatistics::add(*this, prevStats, indexes.size());

		recalculateDates();
	}

	void IndexedRangeStatistics::recalculateDates() {
		if (!indexes.size())
			return;

		startDate = (*this)[0].date;
		endDate = (*this)[indexes.size() - 1].date;
	}

	void IndexedRangeStatistics::add(size_t index, bool doUpdate /* = true */) {
		add(parent->vec[index], index, doUpdate);
	}

	void IndexedRangeStatistics::calculate(size_t start, size_t end) {
		PROFILE_SCOPED();

		ASSERT(end > start, "Invalid start/end. Start: " << start << " - End: " << end);
		ASSERT(start < indexes.size() && end <= indexes.size(), "Start/end out of bounds. Array size: " << indexes.size() << " - Start: " << start << " - End: " << end);

		CumStatistics newStats;
		((Statistics&)newStats) = *(at(0));

		/// Only do this if we have enough items in the array 
		if (indexes.size() > 1)
			newStats.calculate(this, 1, end - start);

		((CumStatistics&)*this) = newStats;
	}

	void IndexedRangeStatistics::recalculate() {
		if (!indexes.size())
			return;

		IndexedRangeStatistics::calculate(0, indexes.size());
		recalculateDates();
		calculateHigherStats();
	}

	const Statistics& IndexedRangeStatistics::operator[](size_t i) const {
		ASSERT(i < indexes.size(), "Array index out of bounds. Size: " << indexes.size() << " - Index: " << i);
		return parent[i];
	}

	const Statistics& IndexedRangeStatistics::operator[](size_t i) {
		return *(this->at(i));
	}

	size_t IndexedRangeStatistics::memSize() const {
		return indexes.size() * sizeof(int) + sizeof(RangeStatistics);
	}

	size_t IndexedRangeStatistics::memSize(const std::vector<IndexedRangeStatistics>& rsVec) {
		size_t size = 0;

		for (const auto& rs : rsVec)
			size += rs.memSize();

		return size;
	}

	io::InputStream& IndexedRangeStatistics::read(io::InputStream& is) {
		PESA_READ_VAR(is, startDate);
		PESA_READ_VAR(is, endDate);

		CumStatistics::read(is);

		PESA_READ_VAR(is, maxLimit);
		PESA_READ_VAR(is, indexes);

		return is;
	}

	io::OutputStream& IndexedRangeStatistics::write(io::OutputStream& os) {
		PESA_WRITE_VAR(os, startDate);
		PESA_WRITE_VAR(os, endDate);

		CumStatistics::write(os);

		PESA_WRITE_VAR(os, maxLimit);
		PESA_WRITE_VAR(os, indexes);

		return os;
	}

	IndexedRangeStatistics IndexedRangeStatistics::calculateDelta(io::OutputStream& os, const IndexedRangeStatistics& dstats, 
		IntDate refStartDate /* = 0 */, IntDate refEndDate /* = 0 */) { 
		return calculateDelta(os, dstats.indexes, dstats.maxLimit, refStartDate, refEndDate);
	}

	IndexedRangeStatistics IndexedRangeStatistics::calculateDelta(io::OutputStream& os, const IntVec& dstatsIndexes, size_t maxLimit, 
		IntDate refStartDate, IntDate refEndDate, bool isDirectIndex) {
		ASSERT(maxLimit == maxLimit || refStartDate, "maxLimit does not match. Expecting: " << maxLimit << " - Found: " << maxLimit);

		IntVec updateIndexes;
		IntVec newIndexes;
		std::vector<size_t> updateLocations;
		size_t iter = 0;
		bool justAdd = false;

		for (size_t i = 0; i < dstatsIndexes.size(); i++) {
			bool found = false;
			int dstatsIndex = dstatsIndexes[i];

			if (!isDirectIndex) {
				if (parent->indexMap.find(dstatsIndexes[i]) == parent->indexMap.end())
					continue;

				dstatsIndex = parent->indexMap[dstatsIndexes[i]];
			}

			/// If a ref date is given, then we ensure that we never have anything behind this in our statistics
			if (refStartDate != 0) {
				const Statistics& stats = *(parent->at(dstatsIndex));
				if (stats.date < refStartDate)
					continue;
				else if (refEndDate != 0 && refEndDate >= refStartDate && stats.date > refEndDate)
					continue;
			}
			else if (refEndDate != 0) {
				const Statistics& stats = *(parent->at(dstatsIndex));
				if (stats.date > refEndDate)
					continue;
			}

			ASSERT(dstatsIndex >= startIndex(), "How can a new version of the stats start BEFORE the older version? Older version start index: " << startIndex() << " - Current index: " << dstatsIndex);
			
			if (justAdd) {
				newIndexes.push_back(dstatsIndex);
			}
			else {
				while (dstatsIndex <= endIndex() && iter < indexes.size() && !found) {
					/// If we find it in the old vector then we need to update it in that location
					if (indexes[iter] == dstatsIndex) {
						found = true;
						updateIndexes.push_back(dstatsIndex);
						updateLocations.push_back(iter);
						break;
					}

					iter++;
				}

				/// If nothing was found then all the indexes after this are new
				if (!found) {
					justAdd = true;
					newIndexes.push_back(dstatsIndex);
				}
			}
		}

		/// Nothing has changed really ...
		if (!updateIndexes.size() && !newIndexes.size())
			return IndexedRangeStatistics();

		for (size_t i = 0; i < updateIndexes.size(); i++) {
			indexes[updateLocations[i]] = updateIndexes[i];
		}

		for (int newIndex : newIndexes)
			add(newIndex, false);

		/// Now we recalculate the whole thing
		recalculate();

		IndexedRangeStatistics delta(*this);
		delta.indexes.clear();
		delta.indexes = indexes;
		delta.recalculateDates();

		return delta;

		///// Then we write it to the stream 
		//Statistics::write(os);

		//PESA_WRITE_VAR(os, maxLimit);

		///// We just write the new indexes. The updated indexes are already there in the stream ...
		//os.write(Statistics::fullName(name, "indexes"), newIndexes);
		////PESA_WRITE_VAR(os, indexes);

		//return os;
	}

	////////////////////////////////////////////////////////////////////////////
	/// AnnualStatistics
	////////////////////////////////////////////////////////////////////////////
	AnnualStatistics::AnnualStatistics() {
	}

	AnnualStatistics::AnnualStatistics(LifetimeStatistics* parent_, int year_)
		: IndexedRangeStatistics(parent_)
		, year(year_)
		, parent(parent_) {
	}

	IndexedRangeStatistics* AnnualStatistics::ensureSize(IndexedRangeStatisticsVec& rs, size_t index, size_t maxLimit /* = 0 */) {
		//ASSERT(index <= rs.size(), "New item must be (at most) the last entry in the vector. Expecting: " << rs.size() << " - Found: " << index);

		if (index < rs.size())
			return &rs[index];

		rs.reserve(index + 1);
		while (rs.size() < index + 1)
			rs.push_back(IndexedRangeStatistics(parent, maxLimit));

		return &rs[index];
	}

	void AnnualStatistics::add(const Statistics& rhs, size_t index, bool doUpdate /* = true */) {
		PROFILE_SCOPED();

		Poco::DateTime date = Util::intToDate(rhs.date);
		ASSERT(year == date.year(), "Error trying to add statistics for year: " << date.year() << " into year: " << year);

		/// This is essentially the daily stats
		IndexedRangeStatistics::add(rhs, index, doUpdate);

		/// Week number 1 is the week containing January 4. This is in accordance to ISO 8601 (http://pocoproject.org/docs/Poco.DateTime.html#9371)
		//ensureSize(weekly, date.week(), 7)->add(rhs, index);
		ensureSize(monthly, date.month() - 1, 30)->add(rhs, index, doUpdate); /// Months are 1 based while our array indev is 0 based (hence the -1)
		ensureSize(quarterly, (date.month() - 1) / 3, 90)->add(rhs, index, doUpdate);
		//ensureSize(semiAnnual, date.month() / 6, 180)->add(rhs, index);
	}

	void AnnualStatistics::calculateHigherStats() {
		IndexedRangeStatistics::calculateHigherStats();

		for (auto& m : monthly)
			m.calculateHigherStats();

		for (auto& q : quarterly)
			q.calculateHigherStats();
	}

	void AnnualStatistics::recalculate() {
		IndexedRangeStatistics::recalculate();

		for (auto& m : monthly)
			m.recalculate();

		for (auto& q : quarterly)
			q.recalculate();
	}

	size_t AnnualStatistics::memSize() const {
		size_t size = IndexedRangeStatistics::memSize();

		//size += IndexedRangeStatistics::memSize(weekly);
		size += IndexedRangeStatistics::memSize(monthly);
		size += IndexedRangeStatistics::memSize(quarterly);
		//size += IndexedRangeStatistics::memSize(semiAnnual);

		return size;
	}

	io::InputStream& AnnualStatistics::read(io::InputStream& is) {
		IndexedRangeStatistics::read(is);

		PESA_READ_VAR(is, year);
		ASSERT(year, "Invalid year for annual statistics: " << year);

		//PESA_READ_OBJ_VEC(is, weekly, IndexedRangeStatistics(parent, 7));
		PESA_READ_OBJ_VEC(is, monthly, IndexedRangeStatistics(parent, 30));
		PESA_READ_OBJ_VEC(is, quarterly, IndexedRangeStatistics(parent, 90));
		//PESA_READ_OBJ_VEC(is, semiAnnual, IndexedRangeStatistics(parent, 180));

		return is;
	}

	io::OutputStream& AnnualStatistics::write(io::OutputStream& os) {
		if (parent->deltaStats) {
			AnnualStatisticsMap::const_iterator iter = parent->deltaStats->annual.find(year);
			if (iter != parent->deltaStats->annual.end())
				return writeDelta(os, iter->second);
		}

		ASSERT(year, "Invalid year for annual statistics: " << year);
		IndexedRangeStatistics::write(os);

		PESA_WRITE_VAR(os, year);

		//PESA_WRITE_OBJ_VEC(os, weekly);
		PESA_WRITE_OBJ_VEC(os, monthly);
		PESA_WRITE_OBJ_VEC(os, quarterly);
		//PESA_WRITE_OBJ_VEC(os, semiAnnual);

		return os;
	}

	bool AnnualStatistics::configureDelta(io::OutputStream& os, IndexedRangeStatisticsVec& statsVec, const IndexedRangeStatisticsVec& dstatsVec, 
		IndexedRangeStatisticsMergeInfoVec& toMerge, io::StreamablePVec& toAdd) {
		if (!dstatsVec.size())
			return false;

		size_t minStart = statsVec.size();
		size_t minDStatsStart = 0;
		const IndexedRangeStatistics* pdstatsStart = nullptr;

		for (size_t i = 0; i < dstatsVec.size() && !pdstatsStart; i++) {
			minDStatsStart = i;
			if (dstatsVec[i].date != 0) 
				pdstatsStart = &dstatsVec[i];
		}

		if (!pdstatsStart)
			return false;

		const IndexedRangeStatistics& dstatsStart = *pdstatsStart;
		size_t numExistingStats = statsVec.size();

		for (size_t i = 0; i < numExistingStats; i++) {
			IndexedRangeStatistics& stats = statsVec[i];
			if (stats.startDate && stats.endDate &&
				stats.startDate <= dstatsStart.startDate && 
				stats.endDate >= dstatsStart.startDate) {
				minStart = i;
				break;
			}
		}

		size_t numDStatsHandled = 0;

		for (size_t i = minStart; i < numExistingStats; i++) {
			IndexedRangeStatistics& stats = statsVec[i];
			const IndexedRangeStatistics& dstats = dstatsVec[i - minStart + minDStatsStart];

			if (!dstats.indexes.size())
				continue;

			/// We can now merge these stats together ...
			IndexedRangeStatisticsMergeInfo mergeInfo;
			mergeInfo.dstats = &dstats;
			mergeInfo.index = i;
			toMerge.push_back(mergeInfo);
			numDStatsHandled++;
		}

		size_t numExtra = dstatsVec.size() - (minDStatsStart + numDStatsHandled);

		/// If there is nothing extra ...
		if (!numExtra)
			return true;

		/// make sure the array is the correct size ...
		//statsVec.resize(statsVec.size() + numExtra);
		for (size_t i = minDStatsStart + numDStatsHandled; i < dstatsVec.size(); i++) {
			const IndexedRangeStatistics& stats = dstatsVec[i];
			//statsVec[numExistingStats + i - numDStatsHandled] = stats;
			toAdd.push_back(const_cast<IndexedRangeStatistics*>(&stats));
		}

		return true;
	}

#define WRITE_ANNUAL(Name)		{\
									IndexedRangeStatisticsMergeInfoVec Name##Merge; \
									io::StreamablePVec Name##Add; \
									std::string fullName = "annual." + Util::cast(year); \
									AnnualStatistics::configureDelta(os, Name, dstats.Name, Name##Merge, Name##Add); \
									for (size_t i = 0; i < Name##Merge.size(); i++) { \
										IndexedRangeStatisticsMergeInfo& mergeInfo = Name##Merge[i]; \
										auto& stats = Name[mergeInfo.index]; \
										auto delta = stats.calculateDelta(os, *mergeInfo.dstats); \
										os.write(Statistics::fullName(fullName, std::string(#Name) + "." + Util::cast(mergeInfo.index)).c_str(), delta); \
									} \
									size_t weeklyCount = Name.size(); \
									for (size_t i = 0; i < Name##Add.size(); i++) { \
										auto* stats = Name##Add[i]; \
										os.write(Statistics::fullName(fullName, std::string(#Name) + "." + Util::cast(weeklyCount + i)).c_str(), *stats); \
									} \
								}


	io::OutputStream& AnnualStatistics::writeDelta(io::OutputStream& os, const AnnualStatistics& dstats) {
		/// If nothing has changed overall, then individuals haven't changed either ...
		auto delta = IndexedRangeStatistics::calculateDelta(os, dstats);
		delta.write(os);

		//WRITE_ANNUAL(weekly);
		WRITE_ANNUAL(monthly);
		WRITE_ANNUAL(quarterly);
		//WRITE_ANNUAL(semiAnnual);

		return os;
	}

#undef WRITE_ANNUAL

	////////////////////////////////////////////////////////////////////////////
	/// LifetimeStatistics
	////////////////////////////////////////////////////////////////////////////
	LifetimeStatistics::LifetimeStatistics()
		: annual(this)
		, D5(this, 5)
		, D10(this, 10)
		, D21(this, 21)
		, D63(this, 55)
		, D120(this, 120)
		, D250(this, 250)
		, inSample(this)
		, outSample(this)
		, publishSample(this) {

		/// Set creation date to today ...
		creationDate = Util::dateToInt(Poco::DateTime());
	}

	void LifetimeStatistics::recalculate() {
		//RangeStatistics::recalculate();
		/// call the finalise function so that all the children can be recalculated as well
		finalise();
	}

	size_t LifetimeStatistics::memSize() const {
		size_t size = RangeStatistics::memSize();

		for (AnnualStatisticsMap::const_iterator iter = annual.begin(); iter != annual.end(); iter++)
			size += iter->second.memSize();

		size += D5.memSize();
		size += D10.memSize();
		size += D21.memSize();
		size += D63.memSize();
		size += D120.memSize();
		size += D250.memSize();
		size += outSample.memSize();
		size += publishSample.memSize();
		size += inSample.memSize();

		return size;
	}

	void LifetimeStatistics::add(const Statistics& rhs) {
		PROFILE_SCOPED();

		Poco::DateTime date = Util::intToDate(rhs.date);

		/// first of all we add it to our array
		RangeStatistics::add(rhs);
		size_t index = RangeStatistics::size() - 1;

		Poco::DateTime sdate = Util::intToDate(startDate);

		ASSERT(date.year() >= sdate.year(), "Invalid statistic passed. Start date is: " << startDate << " and adding for date: " << rhs.date);

		bool doUpdate = runningWindowCalculations;

		AnnualStatistics* astats = nullptr;
		int year = date.year();
		AnnualStatisticsMap::iterator iter = annual.find(year);

		if (iter != annual.end())
			astats = &iter->second;
		else {
			annual[year] = AnnualStatistics(this, year);
			astats = &annual.find(year)->second;
		}

		//float minPnl = astats->minPnl;
		//float maxPnl = astats->maxPnl;
		astats->add(rhs, index, doUpdate);

		//ASSERT(minPnl >= astats->minPnl, "Invalid");
		//ASSERT(maxPnl <= astats->maxPnl, "Invalid");

		if (runningWindowCalculations) {
			D5.add(rhs, index);
			D10.add(rhs, index);
			D21.add(rhs, index);
			D63.add(rhs, index);
			D120.add(rhs, index);
			D250.add(rhs, index);

			if (rhs.date >= creationDate && creationDate != 0) {
				outSample.add(rhs, index);
				if (rhs.date >= publishDate && publishDate != 0)
					publishSample.add(rhs, index);
			}
			else
				inSample.add(rhs, index);
		}
	}

	void LifetimeStatistics::calculateHigherStats() {
		RangeStatistics::calculateHigherStats();

		std::vector<IndexedRangeStatistics*> allRStats = {
			&D5, &D10, &D21, &D63, &D120, &D250
		};

		for (auto iter = annual.begin(); iter != annual.end(); iter++)
			allRStats.push_back(&iter->second);

		for (IndexedRangeStatistics* rstats : allRStats)
			rstats->calculateHigherStats();
	}

	void LifetimeStatistics::finalise() {
		if (runningWindowCalculations) {
			/// We still wanna calculate higher level statistics ...
			calculateHigherStats();
			return;
		}

		std::vector<IndexedRangeStatistics*> allRStats = {
			&D5, &D10, &D21, &D63, &D120, &D250
		};

		IntVec aindexes(allRStats.size());

		for (size_t i = 0; i < aindexes.size(); i++) {
			aindexes[i] = (int)allRStats[i]->maxLimit;
			allRStats[i]->indexes.clear();
			allRStats[i]->indexes.resize(allRStats[i]->maxLimit);
		}

		inSample.indexes.clear();
		outSample.indexes.clear();
		publishSample.indexes.clear();

		int numStats = (int)this->vec.size();
		size_t numRemaining = allRStats.size();

		for (int i = 0; i < numStats; i++) {
			numRemaining = allRStats.size();

			const auto& stats = this->vec[i];

			if (stats.date >= creationDate && creationDate != 0) {
				outSample.add(stats, i, false);
				if (stats.date >= publishDate && publishDate != 0)
					publishSample.add(stats, i, false);
			}
			else
				inSample.add(stats, i, false);

			for (size_t j = 0; j < allRStats.size(); j++) {
				IndexedRangeStatistics* rstats = allRStats[j];
				int& aindex = aindexes[j];

				/// If the current index takes it past the end then we need to add it
				if (aindex > 0 && i + aindex >= numStats) {
					int indexToAddAt = aindex - (numStats - i);
					ASSERT(indexToAddAt >= 0 && indexToAddAt < (int)rstats->indexes.size(), "Going past the bounds: " << indexToAddAt << " - total size: " << rstats->indexes.size());
					rstats->indexes[indexToAddAt] = (size_t)i;
				}
			}
		}

		/// Recalculate everything ...
		inSample.recalculate();
		outSample.recalculate();
		publishSample.recalculate();

		for (size_t j = 0; j < allRStats.size(); j++) {
			allRStats[j]->recalculate();
		}

		for (auto iter = annual.begin(); iter != annual.end(); iter++)
			iter->second.recalculate();

		RangeStatistics::recalculate();
	}

	io::InputStream& LTAnnual::read(io::InputStream& is) {
		int startYear = Util::intToDate(parent->startDate).year();
		int endYear = Util::intToDate(parent->endDate).year();

		for (int year = startYear; year <= endYear; year++) {
			AnnualStatistics astats(parent, year);
			is.read(Util::cast(year).c_str(), &astats);
			(*this)[year] = astats;
		}

		return is;
	}

	io::OutputStream& LTAnnual::write(io::OutputStream& os) {
		for (AnnualStatisticsMap::iterator iter = AnnualStatisticsMap::begin(); iter != AnnualStatisticsMap::end(); iter++) {
			AnnualStatistics& astats = iter->second;
			os.write(Util::cast(astats.year).c_str(), astats);
		}

		return os;
	}

	io::InputStream& LifetimeStatistics::read(io::InputStream& is) {
		PESA_READ_VAR(is, version);

		if (version != s_version)
			throw Poco::IllegalStateException(Poco::format("Version mismatch. Expecting: %d - Found: %d", s_version, version));

		RangeStatistics::read(is);

		PESA_READ_VAR(is, creationDate);
		PESA_READ_VAR(is, publishDate);

		PESA_READ_VAR(is, universeId);
		ASSERT(!universeId.empty(), "Invalid universeId loaded!");

		PESA_READ_VAR(is, market);
		ASSERT(!market.empty(), "Invalid market loaded!");  

		PESA_READ_VAR(is, delay);

		PESA_READ_VAR(is, type);
		ASSERT(!type.empty(), "Invalid type loaded!");

		PESA_READ_VAR(is, level);
		ASSERT(!level.empty(), "Invalid level loaded!");

		is.read("tags", &tags);

		if (!is.isDB()) {
			//pnlOutput.clear();
			//is.read("pnlOutput", &pnlOutput);

			/// Read other statistics
			annual.read(is);
			D5.read(is);
			D10.read(is);
			D21.read(is);
			D63.read(is);
			D120.read(is);
			D250.read(is);
			inSample.read(is);
			outSample.read(is);
			publishSample.read(is);
		}

		return is;
	}

	io::OutputStream& LifetimeStatistics::write(io::OutputStream& os) {
		if (deltaStats)
			return writeDelta(os, *deltaStats);

		PESA_WRITE_VAR(os, version);

		RangeStatistics::write(os);

		PESA_WRITE_VAR(os, creationDate);
		PESA_WRITE_VAR(os, publishDate);

		ASSERT(!universeId.empty(), "Invalid universeId!");
		PESA_WRITE_VAR(os, universeId);

		ASSERT(!market.empty(), "Invalid universeId!");
		PESA_WRITE_VAR(os, market); 
		PESA_WRITE_VAR(os, delay);

		ASSERT(!type.empty(), "Invalid statistics type!");
		PESA_WRITE_VAR(os, type);

		ASSERT(!level.empty(), "Invalid statistics level!");
		PESA_WRITE_VAR(os, level);

		PESA_WRITE_VAR(os, tags);

		if (!os.isDB()) {
			//os.write("pnlOutput", pnlOutput);

			/// Write other statistics
			annual.write(os);
			D5.write(os);
			D10.write(os);
			D21.write(os);
			D63.write(os);
			D120.write(os);
			D250.write(os);
			inSample.write(os);
			outSample.write(os);
			publishSample.write(os);
		}

		return os;
	}

	void LifetimeStatistics::merge(const LifetimeStatistics& dstats) {
		if (!dstats.vec.size())
			return;

		std::string oldUniverseId = this->universeId;
		std::string newUniverseId = dstats.universeId;

		ASSERT(oldUniverseId == newUniverseId, "Universe id mismatch. Expecting: " << oldUniverseId << " - Found: " << newUniverseId);
		ASSERT(startDate <= dstats.startDate, "Cannot have delta stats with an earlier start date. DB Stats: " << startDate << " - Delta Stats: " << dstats.startDate);
		ASSERT(endDate <= dstats.endDate, "Cannot have delta stats with an earlier end date. DB Stats: " << endDate << " - Delta Stats: " << dstats.endDate);

		indexesUpdated.clear();
		indexesAdded.clear();
		indexChangeStart = findStats(dstats.vec[0]);

		ASSERT(indexChangeStart >= 0, "Invalid input stats where nothing overlaps with the existing stats in the database!");

		int index = indexChangeStart;
		indexChangeEnd = indexChangeStart;
		size_t numHandled = 0;

		/// TODO: This is a bit inefficent and we need to make it a bit more efficient!

		/// The first day has 0 so we don't use that ...
		for (size_t i = 1; i < dstats.vec.size() && index < (int)vec.size(); i++) {
			const Statistics& stats = dstats.vec[i];
			bool addedNew = false;
			int addedIndex = (int)RangeStatistics::addDelta(stats, index, &addedNew);
			numHandled++;

			if (addedIndex >= 0)
				index = addedIndex;

			if (index > indexChangeEnd)
				indexChangeEnd = index;

			indexMap[(int)i] = index;

			if (addedNew)
				indexesAdded.push_back(index);
			else
				indexesUpdated.push_back(index);
		}
	}

	io::OutputStream& LifetimeStatistics::writeDelta(io::OutputStream& os, const LifetimeStatistics& dstats) {
		if (!dstats.vec.size())
			return os;

		merge(dstats);

		PESA_WRITE_VAR(os, version);
		RangeStatistics::writeDelta(os, indexesUpdated, indexesAdded);

		//PESA_WRITE_VAR(os, annual);
		//for (AnnualStatisticsMap::iterator iter = annual.begin(); iter != annual.end(); iter++) {
		//	AnnualStatistics& astats = iter->second;
		//	os.write(("annual." + Util::cast(astats.year)).c_str(), astats);
		//}

		///// First of all we handle the updated ones ...
		//outSample.calculateDelta(os, indexesUpdated, outSample.maxLimit, creationDate, 0, true);
		///// Then we handle the added ones ...
		//outSample.calculateDelta(os, indexesAdded, outSample.maxLimit, creationDate, 0, true);

		///// First of all we handle the updated ones ...
		//inSample.calculateDelta(os, indexesUpdated, outSample.maxLimit, 0, creationDate, true);
		///// Then we handle the added ones ...
		//inSample.calculateDelta(os, indexesAdded, outSample.maxLimit, 0, creationDate, true);

//#define WRITE_DX(Name)	auto Name##Delta = Name.calculateDelta(os, dstats.Name); os.write(#Name, Name##Delta)
//
//		WRITE_DX(D5);
//		WRITE_DX(D10);
//		WRITE_DX(D21);
//		WRITE_DX(D55);
//		WRITE_DX(D250);
//
//#undef WRITE_DX

		return os;
	}

	const CumStatistics* LifetimeStatistics::childStats(const std::string& name) const {
		if (name == "IS" || name == "inSample")
			return &inSample;
		else if (name == "OS" || name == "outSample")
			return &outSample;
		else if (name == "PS" || name == "publishSample")
			return &publishSample;
		else if (name.empty() || name == "LT" || name == "lifetime")
			return this;

		return nullptr;
	}


}
