/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Statistics.h
///
/// Created on: 12 Aug 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/Components/ComponentDefs.h"
#include "MatrixData.h"
#include <vector>

#ifndef __SCRIPT__
#include "Core/IO/Stream.h"
#endif

namespace pesa {
	struct LifetimeStatistics;

	struct CumStatistics;

	//////////////////////////////////////////////////////////////////////////
	/// CustomStatistics - This is the class that the users must override
	/// to create custom Statistics
	//////////////////////////////////////////////////////////////////////////
	struct Framework_API CustomStatistics
#ifndef __SCRIPT__
		: public io::Streamable
#endif 
	{
		virtual ~CustomStatistics() {}
		/// Create a new instance of this CustomStatistics object
		virtual CustomStatistics* clone() = 0;

		/// Add one CustomStatistics to another (This is used for accumulation). 
		/// When this funciton is called this/self = current CustomStatistics
		/// while the previous one is passed as an argument along with the total
		/// number of accumulated Statistics
		virtual void			add(const CustomStatistics& prev, size_t totalSize) = 0;
		virtual void			add(std::vector<const CustomStatistics*> prev);
	};

	typedef std::map<std::string, CustomStatistics*> CustomStatisticsMap;

	////////////////////////////////////////////////////////////////////////////
	/// Statistics - Strategy or Alpha Statistics
	////////////////////////////////////////////////////////////////////////////
	struct Framework_API Statistics 
#ifndef __SCRIPT__
		: public io::Streamable
#endif 
	{
		static DataDefinition	s_dataDef;
		static const float		s_daysInYear;

		////////////////////////////////////////////////////////////////////////////
		/// Core data that should be output by the Statistics generation system
		////////////////////////////////////////////////////////////////////////////
		int 					date = 0; 					/// The date for this point (0 means invalid date)
		float 					pnl = 0.0f; 				/// The PNL at this point (or accumulated at this point)
		float 					tvr = 0.0f; 				/// What was the turnover
		float					longPnl = 0.0f;				/// The PNL from the long side
		float					shortPnl = 0.0f;			/// The PNL from the short side
		//float					longPnlPct = 0.0f;			/// The PNL from the long side as a percent of the total PNL
		//float					shortPnlPct = 0.0f;			/// The PNL from the short side as a percent of the total PNL 
		//float					longReturns = 0.0f;			/// The returns from the long side
		//float					shortReturns = 0.0f;		/// The returns from the short side
		float					bookSize = 0.0f;			/// What was the total booksize
		float 					notional = 0.0f; 			/// Total notional in Dollars. Should ideally be the same as bookSize
		float 					tradedNotional = 0.0f; 		/// The total value (in Dollars) that was traded on the day
		float 					tradedShares = 0.0f; 		/// The total number of shares delta from yesterday to today 
		float					tradingPnl = 0.0f;			/// PNL of traded notional
		float 					heldNotional = 0.0f; 		/// The total value (in Dollars) that was held on the day
		float					holdingPnl = 0.0f;			/// PNL of held notional
		float 					shortNotional = 0.0f; 		/// The total short value (in Dollars)
		float 					longNotional = 0.0f; 		/// The total long value (in Dollars)
		float 					liquidateNotional = 0.0f; 	/// The total liquidated value (in Dollars)
		//float 					shortShares = 0.0f;			/// Total number of short shares
		//float 					longShares = 0.0f; 			/// Total number of long shares
		//float 					numLiquidate = 0.0f;		/// Total number of shares we liquidated
		float					cost = 0.0f;				/// Total cost

		size_t					universeSize = 0; 			/// What was the size of the universe for this point
		float					longCount = 0.0f;			/// How many have we long'ed
		float					shortCount = 0.0f;			/// How many have we short'ed
		float					liquidateCount = 0.0f;		/// How many did we liquidate from yesterday to today

		float					returns = 0.0f;				/// The annulised returns from this alpha as a percentage of booksize (pnl / bookSize)
		float					winReturns = 0.0f;			/// The returns of the winning trades i.e. pnlWon / wonBookSize 

		float					marginBps = 0.0f;			/// The margin in basis points
		float					marginCps = 0.0f;			/// The margin in Cents per share 

		float					pnlWon = 0.0f;				/// PNL of the trades where we were on the correct side
		float					winNotional = 0.0f;			/// The notional that was allocated towards the winning trades

		//////////////////////////////////////////////////////////////////////////
		/// Risk exposures on the day 
		//////////////////////////////////////////////////////////////////////////
		float					riskHistBeta = 0.0f;		/// What is the historical beta exposure
		float					riskPredBeta = 0.0f;		/// What is the predicted beta exposure
		float					factorBeta = 0.0;			/// What is the market beta exposure
		float					factorMomentum = 0.0f;		/// What is the momentum exposure
		float					factorValue = 0.0f;			/// What is the value exposure
		float					factorSize = 0.0f;			/// What is the size exposure
		float					factorGrowth = 0.0f;		/// What is the growth exposure
		float					factorLeverage = 0.0f;		/// What is the leverage exposure
		float					factorVolatility = 0.0f;	/// What is the volatility exposure 
		float					factorMarketSens = 0.0f; 	/// What is the Market Sensitivity exposure

		//////////////////////////////////////////////////////////////////////////
		/// CustomStatistics
		//////////////////////////////////////////////////////////////////////////
		//CustomStatisticsMap		customStats;				/// The custom statistics that the user wants to create

								//Statistics();
								~Statistics();

		//////////////////////////////////////////////////////////////////////////
#ifndef __SCRIPT__
		io::InputStream&		read(io::InputStream& is);
		io::OutputStream&		write(io::OutputStream& os);
		 
		/// Over here we write the stats that make it easy to search the database without 
		/// fetching the entire record from the DB. This is purely an efficency thing 
		/// that is required to speed up the searches in the MongoDB and might not be 
		/// required 
		virtual
		io::OutputStream&		writeMinimal(io::OutputStream& os);
#endif

		virtual void			finalise();
		void					finaliseRelativeToYesterday(const pesa::FloatVec& currAlphaValues, const pesa::FloatVec& prevAlphaValues, const pesa::FloatVec& currReturns,
								const pesa::FloatVec& alphaPrevPrice);

		//////////////////////////////////////////////////////////////////////////
		std::string				toString(bool tabular, bool header, const LifetimeStatistics* ltStats) const;
		static std::string 		headerString(bool tabular, bool underline);
		static std::string		hline(size_t count = 0);

		//////////////////////////////////////////////////////////////////////////
		inline bool				isValid() const { return date != 0; }
		static inline std::string fullName(const std::string& prefix, const std::string& name) {
			return !prefix.empty() ? prefix + "." + name : name;
		}
	};

	typedef std::shared_ptr<Statistics>		StatisticsPtr;
	typedef std::vector<Statistics>			StatisticsVec;

	////////////////////////////////////////////////////////////////////////////
	/// CumStatistics - Stats that have been accumulated
	////////////////////////////////////////////////////////////////////////////
	struct Framework_API CumStatistics : public Statistics {
		int 					numDays = 1; 				/// How many days have we traded (for single points this = 1)
		int 					numWon = 0; 				/// How many stocks made the correct decision (numLoss = numDays - numWon)
		float					ddMaxPnl = 0.0f;			/// What is the max PNL we reached 
		float					ddMinPnl = 0.0f;			/// What is the minimum PNL we reached
		float 					ir = 0.0f;					/// Information Ratio (for single points this = 0s)
		float 					hitRatio = 0.0f; 			/// The hit ratio (numWon / total)
		float					maxTvr = 0.0f;				/// What was the maximum turnover

		/// Drawdown related
		float 					maxDD = 0.0f; 				/// Maximum drawdown as a percentage of booksize
		float 					dd = 0.0f; 					/// What is the current drawdown as a percentage of booksize
		int 					dh = 0; 					/// Days since high
		int 					maxDH = 0; 					/// Maximum days since high was achieved

		/// DrawUP (Inv DD) related (this is for reversion alphas)
		float 					maxDU = 0.0f; 				/// Maximum drawdown as a percentage of booksize
		float 					du = 0.0f; 					/// What is the current drawdown as a percentage of booksize
		int 					duh = 0; 					/// Days since high
		int 					maxDUH = 0; 				/// Maximum days since high was achieved

		/// Volatility
		float 					volatility = 0.0f; 			/// Volatility of the PNL (pnl standard deviation percent)

		/// High level STATS
		float 					pnlMean = 0.0f; 			/// PNL Mean (for single points this = pnl)
		float					squaredPnl = 0.0f;			/// The sum of squared PNLs (used fos stddev calculation)
		float 					pnlStdDev = 0.0f; 			/// PNL Standard Deviation (for single points this is 0)

		float					pnlSkew = 0.0f;				/// The skewness of the PNL
		float					pnlKurtosis = 0.0f;			/// The kurtosis of the PNL
		float					rsquared = 0.0f;			/// Regression R-Squared 

		float					pnlNegMean = 0.0f;			/// Mean of the negative pnls
		float					squaredNegPnl = 0.0f;		/// Sum of squares of only negative pnl
		float					pnlNegStdDev = 0.0f;		/// Standard deviation of 

		float					sortinoRatio = 0.0f;		/// The sortino ratio = mean(pnl) / std(returns[returns < 0]) = pnlMean / pnlNegStdDev
		float					calmar = 0.0f;				/// calmar = annualised returns / maxDD = returns / maxDD

		float					minReturns = 0.0f;			/// The minimum returns in this window
		float					maxReturns = 0.0f;			/// The maximum returns in this window

		/// Add THIS point to a an array of existing stats
		/// is equal to = (this + pt)
		void					calculate(pesa::CumStatistics* src, size_t start, size_t end);
		void					calculate(pesa::CumStatistics* src, pesa::CumStatistics& curr, size_t start, size_t end);
		void					calculateHigherStats();

		static void				add(pesa::CumStatistics& curr, const CumStatistics& prev, size_t totalSize);

		void					finalise();
		static std::string 		detailedHeaderString(bool tabular, bool underline);
		std::string				detailedString(bool tabular, bool header, float dayPnl = 0.0f) const;

		virtual size_t			size() const { return 0; }
		virtual const Statistics* at(size_t i) const { return nullptr; };

#ifndef __SCRIPT__
		io::InputStream&		read(io::InputStream& is);
		io::OutputStream&		write(io::OutputStream& os);
		 
		/// Over here we write the stats that make it easy to search the database without 
		/// fetching the entire record from the DB. This is purely an efficency thing 
		/// that is required to speed up the searches in the MongoDB and might not be 
		/// required 
		virtual
		io::OutputStream&		writeMinimal(io::OutputStream& os);
#endif

		static float			calcTvr(float tradedValue, float bookSize);
		static float			calcMarginBps(float pnl, float tradedValue);
		static float			calcMarginCps(float pnl, float tradedShares);
		static float			calcDD(float dd, float bookSize);
	};

	////////////////////////////////////////////////////////////////////////////
	/// RangeStatistics - A list of stats
	////////////////////////////////////////////////////////////////////////////
	struct Framework_API RangeStatistics : public CumStatistics {
		StatisticsVec			vec;						/// The vector that contains the data
		size_t					maxLimit = 0;				/// The maximum limit (0 means unlimited)
		int 					startDate = 0;				/// The starting date
		int						endDate = 0;				/// The ending date

		bool					needsUpdate = false;		/// Does this need an update 

								RangeStatistics(size_t maxLimit = 0);

		size_t					add(const Statistics& stats, bool doUpdate = true);
		size_t					addDelta(const Statistics& stats, int start = 0, bool* addedNew = nullptr);
		int						findStats(const Statistics& stats, int start = 0) const;
		
		void					clear();

		const Statistics&		operator[](size_t i) const;
		Statistics&				operator[](size_t i);
		inline size_t			count() const { return vec.size(); }

		void					calculate(size_t start, size_t end);
		virtual void			recalculate();
		void					recalculateDates();

		size_t					memSize() const;
		static size_t			memSize(const std::vector<RangeStatistics>& rsVec);

		virtual size_t			size() const;
		virtual const Statistics* at(size_t i) const;

#ifndef __SCRIPT__
		io::InputStream&		read(io::InputStream& is);
		io::OutputStream&		write(io::OutputStream& os);
		io::OutputStream&		writeDelta(io::OutputStream& os, const IntVec& indexesUpdated, const IntVec& indexesAdded);
#endif
	};

	typedef std::vector<RangeStatistics> RangeStatisticsVec;

	////////////////////////////////////////////////////////////////////////////
	/// IndexedRangeStatistics - Similar to RangeStatistics but this time
	/// it doesn't keep a copy of the stats, but instead uses indexes
	/// that link it to an array of stats kept in the parent
	////////////////////////////////////////////////////////////////////////////
	struct Framework_API IndexedRangeStatistics : public CumStatistics {
		LifetimeStatistics*		parent = nullptr;			/// The parent object
		IntVec					indexes;					/// The index vector
		size_t					maxLimit = 0;				/// The maximum limit (0 means unlimited)
		int 					startDate = 0;				/// The starting date
		int						endDate = 0;				/// The ending date

								IndexedRangeStatistics(size_t maxLimit = 0);
								IndexedRangeStatistics(LifetimeStatistics* parent, size_t maxLimit = 0);

		void					add(const Statistics& stats, size_t index, bool doUpdate = true);
		void					add(size_t index, bool doUpdate = true);
		void					clear();
		void					resolveIndexes(StatisticsVec& vec, size_t start = 0);

		const Statistics&		operator[](size_t i) const;
		const Statistics&		operator[](size_t i);
		inline size_t			count() const { return indexes.size(); }

		void					calculate(size_t start, size_t end);
		virtual void			recalculate();
		void					recalculateDates();

		size_t					memSize() const;
		static size_t			memSize(const std::vector<IndexedRangeStatistics>& rsVec);

		virtual size_t			size() const;
		virtual const Statistics* at(size_t i) const;

#ifndef __SCRIPT__
		io::InputStream&		read(io::InputStream& is);
		io::OutputStream&		write(io::OutputStream& os);
		IndexedRangeStatistics	calculateDelta(io::OutputStream& os, const IndexedRangeStatistics& dstats, IntDate refStartDate = 0, IntDate refEndDate = 0);
		IndexedRangeStatistics	calculateDelta(io::OutputStream& os, const IntVec& dstatsIndexes, size_t maxLimit, IntDate refStartDate, IntDate refEndDate, bool isDirectIndex = false);
#endif
		
		inline int				startIndex() const { return indexes.size() ? indexes[0] : -1; }
		inline int				endIndex() const { return indexes.size() ? indexes[indexes.size() - 1] : -1; }
	};

	typedef std::vector<IndexedRangeStatistics> IndexedRangeStatisticsVec;
	typedef std::vector<IndexedRangeStatistics*> IndexedRangeStatisticsPVec;

	struct IndexedRangeStatisticsMergeInfo {
		const
		IndexedRangeStatistics*	dstats = nullptr;			/// The delta stats
		size_t					index = 0;					/// What is the index
	};
	typedef std::vector<IndexedRangeStatisticsMergeInfo> IndexedRangeStatisticsMergeInfoVec;

	////////////////////////////////////////////////////////////////////////////
	/// AnnualStatistics - Annual statistics of alpha or strategy
	////////////////////////////////////////////////////////////////////////////
	struct Framework_API AnnualStatistics : public IndexedRangeStatistics {
		int 					year = 0; 					/// What the year was
		//IndexedRangeStatisticsVec weekly;					/// The weekly points within that year
		IndexedRangeStatisticsVec monthly; 					/// Stats for the months within the system (0 = Jan, 1 = Feb ... 11 = Dec)
		IndexedRangeStatisticsVec quarterly; 				/// Quarterly stats (0 = Jan-Mar, 1 = Apr-Jun, 2 = Jul-Sep, 3 = Oct-Dec)
		//IndexedRangeStatisticsVec semiAnnual; 				/// Semi annual (0 = Jan-Jun, 1 = Jul-Dec)

		LifetimeStatistics*		parent = nullptr;			/// The parent object

								AnnualStatistics();
								AnnualStatistics(LifetimeStatistics* parent, int year);

		IndexedRangeStatistics* ensureSize(IndexedRangeStatisticsVec& rs, size_t index, size_t maxLimit = 0);
		void					add(const Statistics& rhs, size_t index, bool doUpdate = true);

		size_t					memSize() const;
		virtual void			recalculate();
		virtual void			calculateHigherStats();

#ifndef __SCRIPT__
		io::InputStream&		read(io::InputStream& is);
		io::OutputStream&		write(io::OutputStream& os);
		io::OutputStream&		writeDelta(io::OutputStream& os, const AnnualStatistics& dstats);
		static bool				configureDelta(io::OutputStream& os, IndexedRangeStatisticsVec& statsVec, const IndexedRangeStatisticsVec& dstatsVec, 
									IndexedRangeStatisticsMergeInfoVec& toMerge, io::StreamablePVec& toAdd);
#endif
	};

	typedef std::vector<AnnualStatistics> AnnualStatisticsVec;
	typedef std::map<int, AnnualStatistics> AnnualStatisticsMap;

	struct Framework_API LTAnnual : public AnnualStatisticsMap
#ifndef __SCRIPT__
		, public io::Streamable
#endif
	{
		LifetimeStatistics*		parent;

#ifndef __SCRIPT__
		io::InputStream&		read(io::InputStream& is);
		io::OutputStream&		write(io::OutputStream& os);
#endif

								LTAnnual(LifetimeStatistics* parent_) : parent(parent_) {}
	};

	////////////////////////////////////////////////////////////////////////////
	/// LifetimeStatistics - Lifetime statistics of alpha or strategy
	////////////////////////////////////////////////////////////////////////////
	struct Framework_API LifetimeStatistics : public RangeStatistics {
		typedef std::unordered_map<int, int> IndexMap;
		static const int		s_version;				    /// The current version

		int						version = s_version;		/// What is the version of these 

		LTAnnual				annual;
		IndexedRangeStatistics	D5;							/// 5 day stats rolling
		IndexedRangeStatistics	D10;						/// 10 day stats rolling
		IndexedRangeStatistics	D21;						/// 21 day stats rolling
		IndexedRangeStatistics	D63;						/// 63 day stats rolling
		IndexedRangeStatistics	D120;						/// 120 day stats rolling
		IndexedRangeStatistics	D250;						/// 250 day stats rolling

		int						creationDate = 0;			/// The date that this alpha was created
		int						publishDate = 0;			/// The date that this alpha was published. This will start the paper trading 

		IndexedRangeStatistics	inSample;					/// The in sample statistics
		IndexedRangeStatistics	outSample;					/// The out sample statistics
		IndexedRangeStatistics	publishSample;				/// The publish/paper trading sample statistics

		std::string				universeId;					/// What was the name of the universe that this belongs to
		int						delay = 1;					/// What is the delay
		int						maxTime = 0;				/// The max time
		std::string				market;						/// What is the market that this belongs to
		std::string				type = "na";				/// Defaults to an AUTO (which means nothing assigned)
		std::string				level = "alpha";			/// The level of the stats e.g. alpha, portfolio etc. 

		std::unordered_map<std::string, std::string> tags;	/// More properties that can be attached with the stats

		//////////////////////////////////////////////////////////////////////////
		/// This data is not written directly to the database
		//////////////////////////////////////////////////////////////////////////

		bool					runningWindowCalculations = false; /// Calculate D5, D10 etc. as the strategy is run ...
		std::string				uuid;						/// The uuid of the stats
		std::vector<int>		indexesUpdated;				/// What indexes have been updated
		std::vector<int>		indexesAdded;				/// The new indexes that have been added
		IndexMap				indexMap;					/// The new mappings for the indexese of the deltaStats
		int						indexChangeStart = 0;		/// What is the starting change of the index
		int						indexChangeEnd = 0;			/// What is the ending change of the index
		LifetimeStatistics*		deltaStats = nullptr;		/// The new stats delta

		StringVec				pnlOutput;					/// This is for checkpoint purposes!

								LifetimeStatistics();

		void					add(const Statistics& rhs);

		size_t					memSize() const;
		void					finalise();
		void					merge(const LifetimeStatistics& dstats);
		virtual void			recalculate();
		virtual void			calculateHigherStats();

#ifndef __SCRIPT__
		io::InputStream&		read(io::InputStream& is);
		io::OutputStream&		write(io::OutputStream& os);
		io::OutputStream&		writeDelta(io::OutputStream& os, const LifetimeStatistics& dstats);
#endif
		const CumStatistics*	childStats(const std::string& name) const;

		inline int				mapIndex(int index) const {
			if (index < 0)
				return index;

			IndexMap::const_iterator iter = indexMap.find(index); //(indexMap.find(index) != indexMap.end(), "Unable to find index in parent map: " << index);
			if (iter == indexMap.end())
				return index;

			return iter->second;
		}
	};


	typedef std::shared_ptr<LifetimeStatistics> LifetimeStatisticsPtr;
	typedef CustomData<LifetimeStatistics> 		LifetimeStatisticsData;
	typedef std::shared_ptr<LifetimeStatisticsData> LifetimeStatisticsDataPtr;
} /// namespace pesa 

