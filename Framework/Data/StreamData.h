/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// StreamData.h
///
/// Created on: 19 Jun 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/FrameworkDefs.h"
#include "Data.h"

namespace pesa {
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	class StreamDataItem
#ifndef __SCRIPT__
		: public io::Streamable
#endif 
	{
	public:
		virtual void				makeInvalid() = 0;
		virtual bool				isValid() const = 0;
		virtual std::string			toString() const = 0;
		virtual void				fromString(const std::string& rhs) = 0;
	};

	////////////////////////////////////////////////////////////////////////////
	/// StreamData - Use this for storing custom data types
	////////////////////////////////////////////////////////////////////////////
	template <typename t_type>
	class StreamData : public Data {
	public:
		typedef std::vector<t_type>			TTypeVec;
		typedef std::vector<TTypeVec>		TTypeVecVec;
		typedef std::vector<TTypeVecVec>	TTypeVecVecVec;

	protected:
		TTypeVecVecVec				m_data;		/// The actual data

		void						init(size_t rows, size_t cols, size_t depth = 1);
		void						ensureSizeAt(size_t kk, size_t rows, size_t cols, bool copyOld /* = false */);

	public:
									StreamData(IUniverse* universe, DataDefinition def);
									StreamData(IUniverse* universe, const TTypeVecVecVec& data, DataDefinition def);
		virtual						~StreamData();

		StreamDataItem&				item(IntDay di_, size_t ii_, size_t kk = 0);
		const StreamDataItem&		item(IntDay di, size_t ii, size_t kk = 0) const;

		t_type&						itemT(IntDay di_, size_t ii_, size_t kk = 0);
		const t_type&				itemT(IntDay di, size_t ii, size_t kk = 0) const;

		StreamDataItem&				operator() (IntDay di_, size_t ii_, size_t kk = 0);
		const StreamDataItem&		operator() (IntDay di, size_t ii, size_t kk = 0) const;

		virtual Data*				clone(IUniverse* universe, bool noCopy);
		virtual Data*				createInstance();
		virtual void 				copyElement(const Data* src, IntDay diSrc, size_t iiSrc, IntDay diDst, size_t iiDst, size_t kkSrc = 0, size_t kkDst = 0);
		virtual void				copyRow(const Data* src, IntDay diSrc, IntDay diDst, size_t kkSrc = 0, size_t kkDst = 0);
		virtual void 				makeInvalid(IntDay di, size_t ii, size_t kk = 0);
		virtual bool 				isValid(IntDay di, size_t ii, size_t kk = 0) const;
		virtual const void* 		data() const;
		virtual void* 				data();
		virtual size_t 				dataSize() const;
		virtual void 				ensureSize(size_t rows, size_t cols, bool copyOld = false);
		virtual void				ensureSizeAndDepth(size_t rows, size_t cols, size_t depth, bool copyOld = false);

		virtual std::string			getString(IntDay di, size_t ii, size_t kk) const;
		virtual bool				doesCustomReadWrite() const;

		virtual size_t				rows() const;
		virtual size_t 				cols() const;
		virtual size_t				depth() const;
		virtual size_t 				itemSize() const;

		virtual std::shared_ptr<Data> extractRow(IntDay di, size_t kk = 0);

		virtual void				setString(IntDay di, size_t ii, size_t kk, const std::string& value);
		virtual void 				setValue(IntDay di, size_t ii, const t_type& value);
		virtual const t_type&		getValue(IntDay di, size_t ii) const;
		virtual const t_type&		getValueAt(IntDay di, size_t ii, size_t kk) const;

#ifndef __SCRIPT__
		virtual io::InputStream&	read(io::InputStream& is);
		virtual io::OutputStream&	write(io::OutputStream& os);
		virtual io::InputStream&	readRow(io::InputStream& is, size_t kk, size_t di);
		virtual io::OutputStream&	writeRow(io::OutputStream& os, size_t kk, size_t di);
#endif 

	};

	//////////////////////////////////////////////////////////////////////////

	template <typename t_type>
	StreamData<t_type>::StreamData(IUniverse* universe, DataDefinition def)
		: Data(universe, def) {
	}

	template <typename t_type>
	StreamData<t_type>::StreamData(IUniverse* universe, const TTypeVecVecVec& data, DataDefinition def) 
		: Data(universe, def)
		, m_data(data) {
	}

	template <typename t_type>
	StreamData<t_type>::~StreamData() {
	}

	template <typename t_type>
	size_t StreamData<t_type>::dataSize() const {
		return 0;
	}

	template <typename t_type>
	void StreamData<t_type>::init(size_t rows, size_t cols, size_t depth /* = 1 */) {
		m_data.resize(depth);
		for (size_t kk = 0; kk < depth; kk++) {
			m_data[kk].resize(rows);
			for (size_t di = 0; di < rows; di++)
				m_data[kk][di].resize(cols);
		}
	}

	template <typename t_type>
	t_type& StreamData<t_type>::itemT(IntDay di_, size_t ii_, size_t kk) {
		IntDay di = (IntDay)DI(di_);
		int ii = (int)II(di, ii_);

		ASSERT(kk >= 0 && kk < depth(), "Invalid kk: " << kk << " - Depth: " << depth() << " - Data: " << std::string(def().name));
		ASSERT(ii >= 0 && ii < cols(), "Invalid ii: " << ii << " - Col Count: " << cols() << " - Data: " << std::string(def().name));
		ASSERT(di >= 0 && di < rows(), "Invalid di: " << di << " - Row Count: " << rows() << " - Data: " << std::string(def().name));

		return m_data[kk][di][ii];
	}

	template <typename t_type>
	const t_type& StreamData<t_type>::itemT(IntDay di_, size_t ii_, size_t kk) const {
		IntDay di = (IntDay)DI(di_);
		int ii = (int)II(di, ii_);

		ASSERT(kk >= 0 && kk < depth(), "Invalid kk: " << kk << " - Depth: " << depth() << " - Data: " << std::string(def().name));
		ASSERT(ii >= 0 && ii < cols(), "Invalid ii: " << ii << " - Col Count: " << cols() << " - Data: " << std::string(def().name));
		ASSERT(di >= 0 && di < rows(), "Invalid di: " << di << " - Row Count: " << rows() << " - Data: " << std::string(def().name));

		return m_data[kk][di][ii];
	}

	template <typename t_type>
	StreamDataItem& StreamData<t_type>::item(IntDay di_, size_t ii_, size_t kk) {
		return dynamic_cast<StreamDataItem&>(itemT(di_, ii_, kk));
	}

	template <typename t_type>
	const StreamDataItem& StreamData<t_type>::item(IntDay di_, size_t ii_, size_t kk) const {
		return dynamic_cast<const StreamDataItem&>(itemT(di_, ii_, kk));
	}

	template <typename t_type>
	StreamDataItem& StreamData<t_type>::operator() (IntDay di_, size_t ii_, size_t kk) {
		return item(di_, ii_, kk);
	}

	template <typename t_type>
	const StreamDataItem& StreamData<t_type>::operator() (IntDay di_, size_t ii_, size_t kk) const {
		return item(di_, ii_, kk);
	}

	template <typename t_type>
	void StreamData<t_type>::makeInvalid(IntDay di, size_t ii, size_t kk) {
		item(di, ii, kk).makeInvalid();
	}

	template <typename t_type>
	bool StreamData<t_type>::isValid(IntDay di, size_t ii, size_t kk) const {
		return item(di, ii, kk).isValid();
	}

	template <typename t_type>
	Data* StreamData<t_type>::clone(IUniverse* universe, bool noCopy) {
		return nullptr;
	}

	template <typename t_type>
	Data* StreamData<t_type>::createInstance() {
		return new StreamData<t_type>(universe(), def());
	}

	template <typename t_type>
	void StreamData<t_type>::copyElement(const Data* src_, IntDay diSrc, size_t iiSrc, IntDay diDst, size_t iiDst, size_t kkSrc /* = 0 */, size_t kkDst /* = 0 */) {
		const StreamData* src = dynamic_cast<const StreamData<t_type>*>(src_);
		ASSERT(src, "Invalid copyElement call. The source data must be of the same type as StreamData!");
		item(diDst, iiDst, kkDst) = src->item(diSrc, iiSrc, kkSrc);
	}

	template <typename t_type>
	void StreamData<t_type>::copyRow(const Data* src_, IntDay diSrc, IntDay diDst, size_t kkSrc /* = 0 */, size_t kkDst /* = 0 */) {
		const StreamData* src = dynamic_cast<const StreamData<t_type>*>(src_);
		ASSERT(src, "Invalid copyRow call. The source data must be of the same type as StreamData!");

		ensureSize(diDst + 1, src->cols(), true);

		for (size_t ii = 0; ii < src->cols(); ii++)
			item(diDst, ii, kkDst) = src->item(diSrc, ii, kkSrc);
	}

	template <typename t_type>
	void StreamData<t_type>::ensureSize(size_t rows, size_t cols, bool copyOld /* = false */) {
		ensureSizeAt(0, rows, cols, copyOld);
	}

	template <typename t_type>
	void StreamData<t_type>::ensureSizeAndDepth(size_t rows, size_t cols, size_t depth, bool copyOld /* = false */) {
		if (m_data.size() >= depth)
			return;
		m_data.resize(depth);

		/// Make sure everything is the same size
		for (size_t kk = 0; kk < depth; kk++)
			ensureSizeAt(kk, rows, cols, copyOld);
	}

	template <typename t_type>
	void StreamData<t_type>::ensureSizeAt(size_t kk, size_t rows, size_t cols, bool copyOld) {
		if (kk >= m_data.size())
			m_data.resize(kk + 1);

		m_data[kk].resize(rows);
		if (cols > 0) {
			for (size_t di = 0; di < rows; di++)
				m_data[kk][di].resize(cols);
		}
	}

	template <typename t_type>
	std::string StreamData<t_type>::getString(IntDay di, size_t ii, size_t kk) const {
		return item(di, ii, kk).toString();
	}

	template <typename t_type>
	bool StreamData<t_type>::doesCustomReadWrite() const {
		return true;
	}

	template <typename t_type>
	void* StreamData<t_type>::data() {
		return nullptr;
	}

	template <typename t_type>
	const void* StreamData<t_type>::data() const {
		return nullptr;
	}

	template <typename t_type>
	size_t StreamData<t_type>::rows() const {
		return m_data[0].size();
	}

	template <typename t_type>
	size_t StreamData<t_type>::cols() const {
		return m_data[0][m_data[0].size() - 1].size();
	}

	template <typename t_type>
	size_t StreamData<t_type>::depth() const {
		return m_data.size();
	}

	template <typename t_type>
	size_t StreamData<t_type>::itemSize() const {
		return sizeof(t_type);
	}

	template <typename t_type>
	std::shared_ptr<Data> StreamData<t_type>::extractRow(IntDay di, size_t kk) {
		StreamData<t_type>* dst = new StreamData<t_type>(universe(), def());
		dst->ensureSize(1, this->cols());
		dst->copyRow(this, di, 0, kk, 0);
		return std::shared_ptr<StreamData>(dst);
	}

	template <typename t_type>
	void StreamData<t_type>::setString(IntDay di, size_t ii, size_t kk, const std::string& value) {
		item(di, ii, kk).fromString(value);
	}

	template <typename t_type>
	void StreamData<t_type>::setValue(IntDay di, size_t ii, const t_type& value) {
		itemT(di, ii, 0) = value;
	}
	
	template <typename t_type>
	const t_type& StreamData<t_type>::getValue(IntDay di, size_t ii) const {
		return itemT(di, ii, 0);
	}

	template <typename t_type>
	const t_type& StreamData<t_type>::getValueAt(IntDay di, size_t ii, size_t kk) const {
		return itemT(di, ii, kk);
	}

#ifndef __SCRIPT__
	template <typename t_type>
	io::InputStream& StreamData<t_type>::read(io::InputStream& in) {
		size_t rows = 1, depth = 1, cols = 0;

		in.read("rows", &rows);
		in.read("cols", &cols);
		in.read("depth", &depth);

		init(rows, cols, depth);

		for (size_t kk = 0; kk < depth; kk++) {
			for (size_t di = 0; di < rows; di++) {
				readRow(in, kk, di);
			}
		}

		return in;
	}

	template <typename t_type>
	io::InputStream& StreamData<t_type>::readRow(io::InputStream& is, size_t kk, size_t di) {
		size_t cols = this->cols();
		//is.read("cols", &cols);
		if (di >= m_data[kk].size()) {
			m_data[kk].resize(di + 1);
			m_data[kk][di].resize(cols);
		}

		if (cols) {
			m_data[kk][di].resize(cols);

			for (size_t ii = 0; ii < cols; ii++)
				is.read("", &m_data[kk][di][ii]);
		}

		return is;
	}

	template <typename t_type>
	io::OutputStream& StreamData<t_type>::writeRow(io::OutputStream& os, size_t kk, size_t di) {
		size_t cols = m_data[kk][di].size();
		ASSERT(cols == this->cols(), "Invalid column size at index: " << kk << ", " << di << ". Expecting: " << this->cols() << " - Found: " << cols);
		//os.write("", m_data[kk][di]);
 
		for (size_t ii = 0; ii < cols; ii++) 
			os.write("", m_data[kk][di][ii]);

		return os;
	}

	template <typename t_type>
	io::OutputStream& StreamData<t_type>::write(io::OutputStream& os) {
		size_t rows = this->rows();
		size_t cols = this->cols();
		size_t depth = this->depth();

		os.write("rows", rows);
		os.write("cols", cols);
		os.write("depth", depth);

		for (size_t kk = 0; kk < depth; kk++) {
			for (size_t di = 0; di < rows; di++) {
				writeRow(os, kk, di);
			}
		}

		return os;
	}

#endif /// __SCRIPT__

} /// namespace pesa 

