/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// StringData.cpp
///
/// Created on: 18 Aug 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "StringData.h"

namespace pesa {
	void StringData::init(size_t cols, size_t depth /* = 1 */, size_t rows /* = 1 */) {
		m_data.resize(depth);
		for (size_t kk = 0; kk < depth; kk++) {
			m_data[kk].resize(rows);
			for (size_t di = 0; di < rows; di++)
				m_data[kk][di].resize(cols);
		}
	}

	StringData::StringData(IUniverse* universe, size_t numCols)
		: Data(universe, DataDefinition::s_string) {
		init(numCols);
	}

	StringData::StringData(IUniverse* universe, size_t numRows, size_t numCols)
		: Data(universe, DataDefinition::s_string) {
		init(numCols, 1, numRows);
	}

	StringData::StringData(IUniverse* universe, size_t numCols, DataDefinition def)
		: Data(universe, def) {
		init(numCols);
	}

	StringData::StringData(IUniverse* universe, size_t numRows, size_t numCols, DataDefinition def)
		: Data(universe, def) {
		init(numCols, 1, numRows);
	}

	//StringData::StringData(IUniverse* universe, CharPtr data, size_t dataSize, DataDefinition def)
	//	: Data(universe, def)
	//	, m_dataPtr(data)
	//	, m_dataPtrSize(dataSize) {
	//}

	StringData::StringData(IUniverse* universe, const StringVec& rhs)
		: Data(universe, DataDefinition::s_string) {
		init(rhs.size());
		m_data[0][0] = rhs;
	}

	StringData::StringData(IUniverse* universe, const StringVecVecVec& rhs)
		: Data(universe, DataDefinition::s_string)
		, m_data(rhs) {
	}

	StringData::StringData(IUniverse* universe, DataDefinition def)
		: Data(universe, def) {
	}

	StringData::~StringData() {
	}

	Data* StringData::createInstance() {
		return new StringData(nullptr, def());
	}

	Data* StringData::clone(IUniverse* universe, bool noCopy) {
		if (!noCopy)
			return new StringData(universe, m_data);

		StringData* cl = new StringData(universe, def());
		cl->ensureSizeAndDepth(rows(), cols(), depth(), false);
		return cl;
	}

	void StringData::copyElement(const Data* src_, IntDay diSrc, size_t iiSrc, IntDay diDst, size_t iiDst, size_t kkSrc /*= 0*/, size_t kkDst /*= 0*/) {
		diDst = DI(diDst);
		iiDst = II(iiDst);

		std::string srcValue;

		if (src_) {
			auto* src = dynamic_cast<const StringData*>(src_);
			srcValue = src->getValueAt(diSrc, iiSrc, kkSrc);
		}

        if (!m_csvSep.empty() && srcValue.empty())
            srcValue = "<null>";

		if (m_csvSep.empty() || m_data[kkDst][diDst][iiDst].empty())
			m_data[kkDst][diDst][iiDst] = srcValue;
		else 
			m_data[kkDst][diDst][iiDst] += m_csvSep + srcValue;
	}

	void StringData::copyRow(const Data* src, IntDay diSrc, IntDay diDst, size_t kkSrc /* = 0 */, size_t kkDst /* = 0 */) {
		ASSERT(cols() == src->cols(), "Number of columns in source are not the same. this->cols(): " << cols() << " - src->cols(): " << src->cols());
		const StringData* rhs = dynamic_cast<const StringData*>(src);
		m_data[kkDst][diDst] = rhs->m_data[kkSrc][diSrc];
	}

	void StringData::makeInvalid(IntDay di, size_t ii, size_t kk) {
		di = DI(di);
		ii = II(ii);

		ASSERT(di < m_data[0].size(), "Invalid di: " << di << " - Total size: " << m_data[0].size());
		ASSERT(ii < m_data[0][di].size(), "Invalid ii: " << ii << " - Total size: " << m_data[0][di].size());

		m_data[kk][di][ii] = "";
	}

	bool StringData::isValid(IntDay di, size_t ii, size_t kk) const {
		di = DI(di);
		ii = II(ii);

		ASSERT(di < m_data[kk].size(), "Invalid di: " << di << " - Total size: " << m_data[kk].size());
		ASSERT(ii < m_data[kk][di].size(), "Invalid ii: " << ii << " - Total size: " << m_data[kk][di].size());

		return !m_data[kk][di][ii].empty();
	}

	const void* StringData::data() const {
		return nullptr;
	}

	void* StringData::data() {
		return nullptr;
	}

	size_t StringData::dataSize() const {
		if (m_dataPtrSize)
			return m_dataPtrSize;

		m_dataPtrSize = 0;
		for (size_t kk = 0; kk < m_data.size(); kk++) {
			size_t numRows = m_data[kk].size();
			for (size_t di = 0; di < numRows; di++) {
				m_dataPtrSize += m_data[kk][di].size(); /// for the null characters
				size_t numCols = m_data[kk][di].size();

				for (size_t ii = 0; ii < numCols; ii++)
					m_dataPtrSize += m_data[kk][di][ii].length();
			}
		}

		return m_dataPtrSize;
	} 

	bool StringData::doesCustomReadWrite() const {
		return true;
	}

	io::InputStream& StringData::read(io::InputStream& in) {
		size_t rows = 1, depth = 1, cols = 0;

		in.read("rows", &rows);
		in.read("cols", &cols);
		in.read("depth", &depth);

		init(cols, depth, rows);

		for (size_t kk = 0; kk < depth; kk++) {
			for (size_t di = 0; di < rows; di++) {
				readRow(in, kk, di);
			}
		}

		return in;
	}

	io::InputStream& StringData::readRow(pesa::io::InputStream& is, size_t kk, size_t di) {
		size_t cols = this->cols();

		if (di >= m_data[kk].size()) {
			m_data[kk].resize(di + 1);
			m_data[kk][di].resize(cols);
		}

		if (cols) {
			is.read("", &m_data[kk][di]);
		}
		return is;
	}

	io::OutputStream& StringData::writeRow(pesa::io::OutputStream& os, size_t kk, size_t di) {
		size_t cols = m_data[kk][di].size();
		ASSERT(cols == this->cols(), "Invalid column size at index: " << kk << ", " << di << ". Expecting: " << this->cols() << " - Found: " << cols);
		//os.write("cols", cols);
		os.write("", m_data[kk][di]);
		return os;
	}

	io::OutputStream& StringData::write(io::OutputStream& os) {
		size_t rows = this->rows();
		size_t cols = this->cols();
		size_t depth = this->depth();

		os.write("rows", rows);
		os.write("cols", cols);
		os.write("depth", depth);

		for (size_t kk = 0; kk < depth; kk++) {
			for (size_t di = 0; di < rows; di++) {
				writeRow(os, kk, di);
			}
		}

		return os;
	}

	size_t StringData::rows() const {
		ASSERT(m_data.size(), "StringData internal buffer is empty!");
		return m_data[0].size();
	}

	size_t StringData::cols() const {
		ASSERT(m_data.size(), "StringData internal buffer is empty!");
		return m_data[0][m_data[0].size() - 1].size();
	}

	size_t StringData::depth() const {
		return m_data.size();
	}

	size_t StringData::itemSize() const {
		return 0;
	}

	void StringData::ensureSizeAt(size_t kk, size_t rows, size_t cols, bool copyOld /* = false */) {
		if (kk >= m_data.size())
		 	m_data.resize(kk + 1);

		m_data[kk].resize(rows);

		if (cols > 0) {
			for (size_t di = 0; di < rows; di++)
				m_data[kk][di].resize(cols);
		}
		m_dataPtr = nullptr;
		m_dataPtrSize = 0;
	}

	void StringData::ensureSize(size_t rows, size_t cols, bool copyOld /* = false */) {
		ensureSizeAt(0, rows, cols, copyOld);
	}

	void StringData::ensureSizeAndDepth(size_t rows, size_t cols, size_t depth, bool copyOld /* = false */) {
		if (m_data.size() < depth)
			m_data.resize(depth);

		/// Make sure everything is the same size
		for (size_t kk = 0; kk < depth; kk++) 
			ensureSizeAt(kk, rows, cols, copyOld);
	}

	void StringData::setValue(IntDay di, size_t ii, const std::string& value) {
		di = DI(di);
		ii = II(ii);

		ASSERT(di < m_data[0].size(), "Invalid di: " << di << " - Total size: " << m_data[0].size() << " - Data: " << Data::name());
		ASSERT(ii < m_data[0][di].size(), "Invalid ii: " << ii << " - Total size: " << m_data[0][di].size() << " - Data: " << Data::name());

		m_data[0][di][ii] = value;
		 
		/// invalidate the thing if its been created before 
		m_dataPtr = nullptr;
		m_dataPtrSize = 0;
	}

	const std::string& StringData::getValue(IntDay di, size_t ii) const {
		return getValueAt(di, ii, 0);
	}

	const std::string& StringData::getValueAt(IntDay di, size_t ii, size_t kk) const {
		ASSERT(kk < m_data.size(), "Invalid depth: " << kk << " - Max depth: " << m_data.size());

		di = DI(di);
		ii = II(ii);

		//if (di >= m_data[0].size())
		//	di = (IntDay)(m_data[0].size() - 1);

		ASSERT(di < m_data[0].size(), "Invalid di: " << di << " - Total size: " << m_data[0].size() << " - Data: " << Data::name());

		if (ii >= m_data[0][di].size())
			return m_empty;
		//ASSERT(ii < m_data[0][di].size(), "Invalid ii: " << ii << " - Total size: " << m_data[0][di].size());

		return m_data[kk][di][ii];
	}

	const std::string& StringData::operator [](size_t ii) const {
		return getValue(0, ii);
	}

	std::string	StringData::getString(pesa::IntDay di, size_t ii, size_t kk) const {
		return getValueAt(di, ii, kk);
	}
} /// namespace pesa 
