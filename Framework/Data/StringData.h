/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// StringData.h
///
/// Created on: 18 Aug 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/FrameworkDefs.h"
#include "Data.h"

namespace pesa {
	typedef std::vector<StringVecVec> StringVecVecVec;	/// 3D matrixof strings

	////////////////////////////////////////////////////////////////////////////
	/// StringData - Generic, variable length strings. The number of columns 
	/// i.e. entries in the array are dynamic BUT the number of rows 
	/// is FIXED to 1
	////////////////////////////////////////////////////////////////////////////
	class Framework_API StringData : public Data {
	protected:
		mutable CharPtr				m_dataPtr = nullptr; 		/// The data pointer 
		mutable size_t				m_dataPtrSize = 0;			/// The size of the data pointer
		StringVecVecVec				m_data;						/// The actual string data
		std::string					m_empty;					/// Empty string
		std::string					m_csvSep = "";				/// CSV separator

		void						init(size_t cols, size_t depth = 1, size_t rows = 1);
		void						ensureSizeAt(size_t kk, size_t rows, size_t cols, bool copyOld = false);

	public:
									StringData(IUniverse* universe, size_t numCols);
									StringData(IUniverse* universe, size_t numRows, size_t numCols);
									StringData(IUniverse* universe, size_t numCols, DataDefinition def);
									StringData(IUniverse* universe, size_t numRows, size_t numCols, DataDefinition def);
									//StringData(IUniverse* universe, CharPtr data, size_t dataSize, DataDefinition def);
									StringData(IUniverse* universe, const StringVec& rhs);
									StringData(IUniverse* universe, const StringVecVecVec& rhs);
									StringData(IUniverse* universe, DataDefinition def);

		virtual 					~StringData();

		virtual Data*				clone(IUniverse* universe, bool noCopy);
		virtual Data*				createInstance();
		virtual void 				copyElement(const Data* src, IntDay diSrc, size_t iiSrc, IntDay diDst, size_t iiDst, size_t kkSrc = 0, size_t kkDst = 0);
		virtual void				copyRow(const Data* src, IntDay diSrc, IntDay diDst, size_t kkSrc = 0, size_t kkDst = 0);
		virtual void 				makeInvalid(IntDay di, size_t ii, size_t kk = 0);
		virtual bool 				isValid(IntDay di, size_t ii, size_t kk = 0) const;
		virtual const void* 		data() const;
		virtual void* 				data();
		virtual size_t 				dataSize() const;
		virtual void 				ensureSize(size_t rows, size_t cols, bool copyOld = false);
		virtual void				ensureSizeAndDepth(size_t rows, size_t cols, size_t depth, bool copyOld = false);
		virtual size_t				rows() const;
		virtual size_t 				cols() const;
		virtual size_t				depth() const;
		virtual size_t 				itemSize() const;

		virtual std::string			getString(pesa::IntDay di, size_t ii, size_t kk) const;
		virtual bool				doesCustomReadWrite() const;

#ifndef __SCRIPT__
		virtual io::InputStream&	read(pesa::io::InputStream& is);
		virtual io::OutputStream&	write(pesa::io::OutputStream& os);
		virtual io::InputStream&	readRow(pesa::io::InputStream& is, size_t kk, size_t di);
		virtual io::OutputStream&	writeRow(pesa::io::OutputStream& os, size_t kk, size_t di);
#endif 

		virtual void				setString(pesa::IntDay di, size_t ii, size_t kk, const std::string& value) { setValue(di, ii, value); }
		virtual void 				setValue(IntDay di, size_t ii, const std::string& value);
		virtual const std::string&	getValue(IntDay di, size_t ii) const;
		virtual const std::string&	getValueAt(IntDay di, size_t ii, size_t kk) const;
		virtual const std::string&	operator[](size_t ii) const;

		//////////////////////////////////////////////////////////////////////////
		/// Inline functions
		//////////////////////////////////////////////////////////////////////////
		inline const std::vector<std::string>& vector(size_t di, size_t kk) const { return m_data[kk][di]; }
		inline const std::vector<std::string>& vector(size_t di) const { return m_data[0][di]; }
		inline const std::vector<std::string>& vector() const { return m_data[0][0]; }
		inline const std::string*	sptr(size_t di = 0, size_t kk = 0) const { return &m_data[kk][di][0]; }
		inline std::string&			csvSep() { return m_csvSep; }
		inline std::string			csvSep() const { return m_csvSep; }

		//inline const std::string& operator(IntDay di, size_t ii) const { return getValue(di, ii); }
		//inline std::string&	operator(IntDay di, size_t ii) { return getValue(di, ii); }
		//inline const std::string& operator(IntDay di, size_t ii, size_t kk) const { return getValue(di, ii); }
		//inline std::string&	operator(IntDay di, size_t ii, size_t kk) { return getValue(di, ii); }

		////////////////////////////////////////////////////////////////////////////
		/// SCRIPT helper functions - DO NOT USE IN C++
		////////////////////////////////////////////////////////////////////////////
		Data*					script_sptr() {
			return static_cast<Data*>(this);
		}

		inline static StringData* script_cast(Data* data) {
			return dynamic_cast<StringData*>(data);
		}
	};

	typedef std::shared_ptr<StringData> StringDataPtr;
} /// namespace pesa 

