/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// VarMatrixData.h
///
/// Created on: 17 Oct 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "MatrixData.h"
#include <vector>
#include "Poco/DateTimeFormat.h"

namespace pesa {

	////////////////////////////////////////////////////////////////////////////
	/// Matrix data
	////////////////////////////////////////////////////////////////////////////
	template <typename t_type>
	class VarMatrixData : public MatrixData<std::vector<t_type> > {
		typedef std::vector<t_type> TypeVec;
	public:
		using 			MatrixData<std::vector<t_type> >::dataSize;
		using 			MatrixData<std::vector<t_type> >::rows;
		using 			MatrixData<std::vector<t_type> >::cols;
        using           MatrixData<std::vector<t_type> >::depth;
		using 			MatrixData<std::vector<t_type> >::makeInvalid;
		using 			MatrixData<std::vector<t_type> >::isValid;
		using 			MatrixData<std::vector<t_type> >::ensureSize;

		VarMatrixData(IUniverse* universe, DataDefinition def, size_t rows, size_t cols, bool isCircular = false)
			: MatrixData<TypeVec>(universe, def, rows, cols, isCircular) {
		}

		VarMatrixData(IUniverse* universe, DataDefinition def)
			: MatrixData<TypeVec>(universe, def) {
		}

		explicit VarMatrixData(IUniverse* universe, const VarMatrixData<t_type>& rhs)
			: MatrixData<TypeVec>(universe, rhs) {
			*this = rhs;
		}

		virtual ~VarMatrixData() {
		}

		const TypeVec& item(IntDay di_, size_t ii_) const {
			int di = (int)this->DI(di_);
			int ii = (int)this->II(di, ii_);
			size_t numRows = MatrixData<TypeVec>::m_data[0]->rows();
			return (*MatrixData<TypeVec>::m_data[0])(di, ii);
		}

		virtual void makeInvalid(pesa::IntDay di, size_t ii, size_t kk) {
			t_type value;
			pesa::invalid::get(value);
            this->item(di, ii, kk) = value;
		}

		TypeVec& item(IntDay di_, size_t ii_) {
			int di = (int)this->DI(di_);
			int ii = (int)this->II(di, ii_);

			size_t numRows = MatrixData<TypeVec>::m_data[0]->rows();
			return (*MatrixData<TypeVec>::m_data[0])(di, ii);
		}

		const t_type item(IntDay di, size_t ii, size_t kk) const {
			const TypeVec& vec = item(di, ii);

			if (kk >= vec.size()) {
				t_type value; 
				pesa::invalid::get(value);
				return value;
			}

			return vec[kk];
		}

		t_type& item(IntDay di, size_t ii, size_t kk) {
			TypeVec& vec = item(di, ii);

			if (kk >= vec.size())
				vec.resize(kk + 1);

			//ASSERT(kk < vec.size(), "Invalid kk: " << kk << " - Vector size: " << vec.size());
			return vec[kk];
		}

		void ensureDepthAt(IntDay di, size_t ii, size_t depth) {
			TypeVec& vec = item(di, ii);

			if (vec.size() < depth)
				vec.resize(depth);
		}

		virtual size_t depthAt(IntDay di, size_t ii) const { return count(di, ii); }

		size_t count(IntDay di_, size_t ii_) const {
			const auto& vec = item(di_, ii_);
			return vec.size();
		}

		virtual bool isVariableDepth() const {
			return true;
		}

		virtual void invalidateAll() {
			for (IntDay di = 0; di < rows(); di++) {
				for (size_t ii = 0; ii < cols(); ii++) {
					TypeVec& vec = item(di, ii);
					vec.clear();
				}
			}
		}
		//virtual t_type operator() (IntDay di_, size_t ii_, size_t kk) const {
		//	int di = (int)this->DI(di_);
		//	int ii = (int)this->II(di, ii_);
		//	size_t numRows = MatrixData<TypeVec>::m_data[0]->rows();

		//	const TypeVec& vec = (*MatrixData<TypeVec>::m_data[0])(!m_isCircular ? di : di % numRows, ii);
		//	ASSERT(kk < vec.size(), "Invalid kk: " << kk << " - Vec: " << vec.size());

		//	return vec.at(kk);
		//}

		//virtual t_type& operator() (IntDay di_, size_t ii_, size_t kk) {
		//	int di = (int)this->DI(di_);
		//	int ii = (int)this->II(di, ii_);
		//	size_t numRows = MatrixData<TypeVec>::m_data[0]->rows();

		//	const TypeVec& vec = (*MatrixData<TypeVec>::m_data[0])(!m_isCircular ? di : di % numRows, ii);
		//	ASSERT(kk < vec.size(), "Invalid kk: " << kk << " - Vec: " << vec.size());

		//	return vec.at(kk);
		//}

		virtual void copyRow(const Data* src, IntDay diSrc, IntDay diDst, size_t kkSrc = 0, size_t kkDst = 0) {
			const VarMatrixData<t_type>* rhs = dynamic_cast<const VarMatrixData<t_type>*>(src);
			size_t numCols = cols();

			for (size_t ii = 0; ii < numCols; ii++) {
				const TypeVec& vsrc = (*rhs)(diSrc, ii, kkSrc);
				TypeVec& vdst = (*this)(diDst, ii, kkDst);
				vdst = vsrc;
			}
		}

		virtual Data* clone(IUniverse* universe, bool noCopy) {
			if (!noCopy)
				return new VarMatrixData<t_type>(universe, *this);
			return new VarMatrixData<t_type>(universe, this->def(), this->rows(), this->cols());
		}

		virtual void setString(pesa::IntDay di, size_t ii, size_t kk, const std::string& value) {
		}

		virtual void ensureSizeAt(size_t kk, size_t rows, size_t cols, bool copyOld = false) {
            if (MatrixData<TypeVec>::m_data[kk] && rows == this->rows() && cols == this->cols())
				return;

            /// Resize over here!
            pesa::Matrix<TypeVec>* existingData = (MatrixData<TypeVec>::m_data[kk]).get();
            pesa::Matrix<TypeVec>* newData = new pesa::Matrix<TypeVec>(rows, cols); ///(MatrixData<TypeVec>::m_data[kk]).get();
//			MatrixData<TypeVec>::m_data[kk] = std::make_shared<pesa::Matrix<TypeVec> >(rows, cols);

            if (copyOld && existingData) {
                for (size_t di = 0; di < existingData->rows(); di++) {
                    for (size_t ii = 0; ii < existingData->cols(); ii++) {
                        (*newData)(di, ii) = (*existingData)(di, ii);
                    }
                }
            }

            MatrixData<TypeVec>::m_data[kk] = std::shared_ptr<pesa::Matrix<TypeVec> >(newData);
		}

		virtual void copyElement(const Data* src_, IntDay diSrc, size_t iiSrc, IntDay diDst, size_t iiDst, size_t kkSrc = 0, size_t kkDst = 0) {
			auto* src = dynamic_cast<const MatrixData<t_type>*>(src_);
			if (src) {
//				TypeVec& dstVec = this->item(diDst, iiDst, kkDst);
                TypeVec& dstVec = this->item(diDst, iiDst);
                ASSERT(dstVec.size() > kkDst, "Invalid kkDst: " << kkDst << " without calling ensureDepthAt first. Current depth: " << dstVec.size());
				t_type srcValue = (*src)(diSrc, iiSrc, kkSrc);
				dstVec[kkDst] = srcValue;

				/// add one more value to the vector ...
//				dstVec.resize(dstVec.size() + 1);
//				dstVec[dstVec.size() - 1] = srcValue;
				return;
			}

			auto* vsrc = dynamic_cast<const VarMatrixData<t_type>*>(src_);
			ASSERT(vsrc, "Unable to cast source into a MatrixData or VarMatrixData. TypeInfo: " << typeid(*src_).name());
			TypeVec& dstVec = (*this)(diDst, iiDst, kkDst);
			const TypeVec& srcVec = (*vsrc)(diSrc, iiSrc, kkSrc);

			//if (srcVec.size())
			dstVec = srcVec;
			//else {
			//	TypeVec& dstVec = this->item(diDst, iiDst);
			//	const TypeVec& srcVec = vsrc->item(diSrc, iiSrc);
			//	dstVec = srcVec;
			//}
		}

		virtual bool doesCustomReadWrite() const {
			return true;
		}

		virtual pesa::io::InputStream& readRow(pesa::io::InputStream& is, size_t kk, size_t di_) {
			IntDay di = this->DI((IntDay)di_);
			size_t cols = MatrixData<TypeVec>::m_data[kk]->cols();
			size_t rows = MatrixData<TypeVec>::m_data[kk]->rows();
			is.read("cols", &cols);

			ASSERT(cols, "Invalid number of columns!");
			ensureSizeAt(kk, rows, cols, true);

			for (size_t ii = 0; ii < cols; ii++) {
				TypeVec& vec = item(di, ii); ///(*MatrixData<TypeVec>::m_data[kk])(di, ii);
				size_t vecSize = 0;
				is.read("vecSize", &vecSize);
				vec.resize(vecSize);
				is.read("iiVec", &vec);
			}

			return is;
		}

		virtual pesa::io::OutputStream& writeRow(pesa::io::OutputStream& os, size_t kk, size_t di_) {
			IntDay di = this->DI((IntDay)di_);
			size_t cols = MatrixData<TypeVec>::m_data[kk]->cols();
			os.write("cols", cols);

			for (size_t ii = 0; ii < cols; ii++) {
				const TypeVec& vec = item(di, ii); /// (*MatrixData<TypeVec>::m_data[kk])(di, ii);
				size_t vecSize = vec.size();
				os.write("vecSize", vecSize);
				os.write("iiVec", vec);
			}

			return os;
		}

		virtual size_t dataSize() const {
			size_t numRows = rows();
			size_t numCols = cols();
			size_t numDepth = depth();
			size_t size = 0;

			for (size_t kk = 0; kk < numDepth; kk++) {
				for (size_t di = 0; di < numRows; di++) {
					for (size_t ii = 0; ii < numCols; ii++) {
						const TypeVec& vec = (*MatrixData<TypeVec>::m_data[kk])(di, ii);
						size += vec.size() * sizeof(t_type);
					}
				}
			}

			return size;
		}

	};

#define SCRIPT_STUB(Name, Type) \
					Name(IUniverse* universe, DataDefinition def, size_t rows, size_t cols, bool isCircular = false) \
						: VarMatrixData<Type>(universe, def, rows, cols, isCircular) {} \
					Name(IUniverse* universe, DataDefinition def) \
						: VarMatrixData<Type>(universe, def) {} \
					explicit Name(IUniverse* universe, const Name& rhs) \
						: VarMatrixData<Type>(universe, rhs) {} \
	using 			VarMatrixData::dataSize; \
	using 			VarMatrixData::rows; \
	using 			VarMatrixData::cols; \
	using 			VarMatrixData::makeInvalid; \
	using 			VarMatrixData::isValid; \
	using 			VarMatrixData::ensureSize; \
	Data*			clone(IUniverse* universe, bool noCopy) { return !noCopy ? new Name(universe, *this) : new Name(universe, this->def(), this->rows(), this->cols()); } \
	Data*			createInstance() { return new Name(nullptr, def()); } \
	inline size_t	getSize(IntDay di, size_t ii) const { return (*this)(di, ii).size(); } \
	std::string		getString(pesa::IntDay di, size_t ii, size_t kk) const { return Util::cast<Type>(getValueKK(di, ii, kk)); } \
	inline Type		getValueKK(IntDay di, size_t ii, size_t kk) const { \
		const std::vector<Type> vec = (*this)(di, ii); \
		if (kk >= vec.size()) { \
			Type value; \
			pesa::invalid::get(value); \
			return value; \
		} \
		return vec[kk]; \
	} \
	inline Type		getValue(IntDay di, size_t ii) const { return getValueKK(di, ii, 0); } \
	inline Data*	script_sptr() { return static_cast<Data*>(this); } \
	inline static Name* script_cast(Data* dptr) { return dynamic_cast<Name*>(dptr); } \

	////////////////////////////////////////////////////////////////////////////
	/// IntVarMatrixData
	////////////////////////////////////////////////////////////////////////////
	class Framework_API IntVarMatrixData : public VarMatrixData<int> {
	public:
		SCRIPT_STUB(IntVarMatrixData, int);

		virtual void setString(pesa::IntDay di, size_t ii, size_t kk, const std::string& value) {
			if (!value.empty())
				this->item(di, ii, kk) = Util::cast<int>(value);
			else
				this->makeInvalid(di, ii, kk);
		}
	};
	typedef std::shared_ptr<IntVarMatrixData> 		IntVarMatrixDataPtr;

	////////////////////////////////////////////////////////////////////////////
	/// FloatVarMatrixData
	////////////////////////////////////////////////////////////////////////////
	class Framework_API FloatVarMatrixData : public VarMatrixData<float> {
	public:
		SCRIPT_STUB(FloatVarMatrixData, float);

		virtual void setString(pesa::IntDay di, size_t ii, size_t kk, const std::string& value) {
			if (!value.empty())
				this->item(di, ii, kk) = Util::cast<float>(value);
			else
				this->makeInvalid(di, ii, kk);
		}
	};

	typedef std::shared_ptr<FloatVarMatrixData> 	FloatVarMatrixDataPtr;
	typedef std::vector<FloatVarMatrixDataPtr>		FloatVarMatrixDataPtrVec;
	typedef std::unordered_map<std::string, FloatVarMatrixDataPtr> FloatVarMatrixDataPtrStrMap;

	 ////////////////////////////////////////////////////////////////////////////
	 /// Int64VarMatrixData
	 ////////////////////////////////////////////////////////////////////////////
	 class Framework_API Int64VarMatrixData : public VarMatrixData<int64_t> {
	 public:
	 	SCRIPT_STUB(Int64VarMatrixData, int64_t);

	 	//Poco::DateTime							getDTValue(IntDay di, size_t ii) const;
	 	//void									setDTValue(IntDay di, size_t ii, const Poco::DateTime& dt);

	 	//Poco::Timestamp							getTSValue(IntDay di, size_t ii) const;
	 	//void									setTSValue(IntDay di, size_t ii, const Poco::Timestamp& ts);

		virtual void setString(pesa::IntDay di, size_t ii, size_t kk, const std::string& value) {
			//if (!value.empty())
			//	this->item(di, ii, kk) = Util::cast<int64_t>(value);
			//else
			//	this->makeInvalid(di, ii, kk);
			if (value.empty()) {
				this->item(di, ii, kk) = 0;
				return;
			}

			/// If it's all numbers then we just treat this as a number
			if (std::all_of(value.begin(), value.end(), ::isdigit))
				this->item(di, ii, kk) = Util::cast<uint64_t>(value);
			else {
				auto date = Util::parseFormattedDate(Poco::DateTimeFormat::ISO8601_FORMAT, value);
				setDTValue(di, ii, kk, date);
			}
		}

		Poco::DateTime							getDTValue(IntDay di, size_t ii, size_t kk = 0) const {
			return Poco::DateTime(getTSValue(di, ii, kk));
		}

		void									setDTValue(IntDay di, size_t ii, size_t kk, const Poco::DateTime& dt) {
			setTSValue(di, ii, kk, dt.timestamp());
		}

		Poco::Timestamp							getTSValue(IntDay di, size_t ii, size_t kk = 0) const {
			int64_t value = getValueKK(di, ii, kk) / 1000;
			return Poco::Timestamp(value);
		}

		void									setTSValue(IntDay di, size_t ii, size_t kk, const Poco::Timestamp& ts) {
			/// Change to nanoseconds (pandas/BamData convention)
			int64_t value = ts.epochMicroseconds() * 1000;
			this->item(di, ii, kk) =  value;
		}

		float									getPyValue(IntDay di, size_t ii) const {
			/// This is in keeping with pandas/BamData convention of nanoseconds instead of the microseconds
			int64_t value = getValue(di, ii);
			return (float)value;
		}

		float									getPyValueKK(IntDay di, size_t ii, size_t kk) const {
			/// This is in keeping with pandas/BamData convention of nanoseconds instead of the microseconds
			int64_t value = getValueKK(di, ii, kk);
			return (float)value;
		}

		//void									setPyValue(IntDay di, size_t ii, float ts);
	 };
	 typedef std::shared_ptr<Int64VarMatrixData> Int64VarMatrixDataPtr;

	////////////////////////////////////////////////////////////////////////////
	/// DoubleVarMatrixData
	////////////////////////////////////////////////////////////////////////////
	class Framework_API DoubleVarMatrixData : public VarMatrixData<double> {
	public:
		SCRIPT_STUB(DoubleVarMatrixData, double);

		virtual void setString(pesa::IntDay di, size_t ii, size_t kk, const std::string& value) {
			if (!value.empty())
				this->item(di, ii, kk) = Util::cast<double>(value);
			else
				this->makeInvalid(di, ii, kk);
		}
	};

	typedef std::shared_ptr<DoubleVarMatrixData> 	DoubleVarMatrixDataPtr;

	////////////////////////////////////////////////////////////////////////////
	/// UIntVarMatrixData
	////////////////////////////////////////////////////////////////////////////
	//class Framework_API UIntVarMatrixData : public VarMatrixData<uint32_t> {
	//	SCRIPT_STUB(UIntVarMatrixData, uint32_t);
	//	inline void setIntValue(IntDay di, size_t ii, int value) { (*this)(di, ii) = (uint32_t)value; } 
	//};
	//typedef std::shared_ptr<UIntVarMatrixData>		UIntVarMatrixDataPtr;

	////////////////////////////////////////////////////////////////////////////
	/// LongVarMatrixData
	////////////////////////////////////////////////////////////////////////////
	//class Framework_API LongVarMatrixData : public VarMatrixData<long> {
	//	SCRIPT_STUB(LongVarMatrixData, long);
	//};
	//typedef std::shared_ptr<LongVarMatrixData> 	LongVarMatrixDataPtr;

	////////////////////////////////////////////////////////////////////////////
	/// ULongVarMatrixData
	////////////////////////////////////////////////////////////////////////////
	//class Framework_API ULongVarMatrixData : public VarMatrixData<ulong_t> {
	//	SCRIPT_STUB(ULongVarMatrixData, ulong_t);
	//};
	//typedef std::shared_ptr<ULongVarMatrixData>	ULongVarMatrixDataPtr;

} /// namespace pesa 


#undef SCRIPT_STUB
