/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Framework.h
///
/// Created on: 4 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/Components/IComponent.h"
#include "Framework/Components/IOptimiser.h"
#include "Framework/Components/IAlpha.h"
#include "Framework/Components/ICombination.h"
#include "Framework/Components/IDataLoader.h"
#include "Framework/Components/IOperation.h"
#include "Framework/Components/IUniverse.h"

#include "Framework/Pipeline/IPipelineComponent.h"
#include "Framework/Pipeline/IPipelineHandler.h"
#include "Framework/Pipeline/IDataRegistry.h"

#include "Framework/Data/Data.h"
#include "Framework/Data/Dataset.h"
#include "Framework/Data/CustomData.h"
#include "Framework/Data/StringData.h"
#include "Framework/Data/IndexedStringData.h"
#include "Framework/Data/MatrixData.h"
#include "Framework/Data/MatrixDataWindow.h"

#include "Framework/Helper/ShLib.h"
#include "Framework/Helper/BasicUniverseBuilder.h"
#include "Framework/Helper/FrameworkUtil.h"

namespace pesa {

} /// namespace pesa

