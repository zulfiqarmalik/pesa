/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// BasicUniverseBuilder.cpp
///
/// Created on: 18 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "./FileDataLoader.h"
#include "./BasicUniverseBuilder.h"
#include "Poco/String.h"
#include <iterator>

namespace pesa {

namespace helper {

	BasicUniverseBuilder::BasicUniverseBuilder(const ConfigSection& config, const std::string& logChannel)
		: IUniverse(config, logChannel) {
		CTrace("Created BasicUniverseBuilder: %s", config.toString(true));
	}

	BasicUniverseBuilder::~BasicUniverseBuilder() {
		CTrace("Deleting BasicUniverseBuilder!");
	}

	std::string BasicUniverseBuilder::makeUuid(const std::string& symbol, const std::string& name) {
		return symbol + "[" + name + "]";
	}

	Security::Index BasicUniverseBuilder::find(const std::string& uuid) const {
		auto iter = m_securities.find(uuid);

		if (iter == m_securities.end())
			return Security::s_invalidIndex;

		size_t lookupIndex = iter->second;
		return m_securitiesList[lookupIndex].index;
	}

	void BasicUniverseBuilder::addExisting(const Security& sec) {
		ASSERT(sec.index != Security::s_invalidIndex, "Invalid index for security: " << sec.toString());

		auto uuid = sec.uuidStr();

		auto* existingSec = mapUuidOnly(uuid);

		if (existingSec) {
			CError("Error while adding new security: %s", sec.debugString());
			CError("Security already exists: %s", existingSec->debugString());
			ASSERT(false, "Fatal assertion!");
			/// TODO: Temporary while I ask Manos to fix the DB so that this never happens!
			return;
		}
		
		// ASSERT(m_securities.find(sec.uuid) == m_securities.end(), "Security already exists: " << sec.toString());

		// if (sec.symbolStr() == "02079K107")
		// 	ASSERT(false, "Test!");

		m_securitiesList.push_back(sec);
		mapSecurityUuid(sec, m_securitiesList.size() - 1);
		mapSecuritySymbol(sec, m_securitiesList.size() - 1);
	}

	Security::Index BasicUniverseBuilder::add(const Security& sec) {
		Security newSec = sec;
		newSec.index = (Security::Index)m_securitiesList.size();

		m_securitiesList.push_back(newSec);
		mapSecuritySymbol(sec, m_securitiesList.size() - 1);

		return newSec.index;
	}

	void BasicUniverseBuilder::mapSecuritySymbol(const Security& sec, size_t lookupIndex) {
		//m_symSecurities[uuid] = newSec;
		m_securities[sec.uuidStr()]			= lookupIndex;

		auto ticker = sec.tickerStr();

		m_securities[ticker] = lookupIndex;

		if (!sec.bbTickerStr().empty())
			m_securities[sec.bbTickerStr()]	= lookupIndex;
		m_securities[sec.isinStr()]			= lookupIndex;

		std::string instrumentId			= sec.instrumentId();

		if (!instrumentId.empty())
			m_securities[instrumentId]		= lookupIndex;

		std::string cfigi					= sec.cfigiStr();
		if (!cfigi.empty())
			m_securities[cfigi]				= lookupIndex;

		std::string isin					= sec.isinStr();
		if (!isin.empty())
			m_securities[isin]				= lookupIndex;

		std::string figi					= sec.figiStr();
		if (!figi.empty())
			m_securities[figi]				= lookupIndex;

		std::string permId					= sec.permIdStr();
		if (!permId.empty())
			m_securities[permId]			= lookupIndex;

		std::string perfId					= sec.perfIdStr();
		if (!perfId.empty())
			m_securities[perfId]			= lookupIndex;

		std::string ric						= sec.ricStr();
		if (!ric.empty())
			m_securities[ric]				= lookupIndex;
	}

	void BasicUniverseBuilder::mapSecurityUuid(const pesa::Security &sec, size_t lookupIndex){
		//m_symSecurities[uuid] = newSec;
		m_securitiesUuidLUT[sec.uuidStr()]	= lookupIndex;
	}

	Security BasicUniverseBuilder::get(const std::string& uuid) const {
		auto iter = m_securities.find(uuid);

		if (iter == m_securities.end())
			return Security();

		size_t lookupIndex = iter->second;
		return m_securitiesList[lookupIndex];
	}

	Security::Index BasicUniverseBuilder::universeDataIndex(IntDay di, size_t ai) const {
		const auto* data = universeData();
		ASSERT(data, "Subuniverses must override universeData function to give the universe data to the BasicUniverseBuilder!");
		Security::Index ii = (Security::Index)(*data)(di, (Security::Index)ai);
		return ii;
		//auto ii = (Security::Index)(data->getRawValue(di, (Security::Index)ai));

		//if (ii == Security::s_invalidIndex)
		//	return ii;

		//auto* secMaster = const_cast<BasicUniverseBuilder*>(this)->dataRegistry()->getSecurityMaster();
		//ASSERT(secMaster, "Unable to load security master from the data registry!");

		//return secMaster->index(di, ii);
	}

	void BasicUniverseBuilder::calculateMaxPITSize() {
		const DataRowHeaderVec& rowInfos = m_dmap->rowInfos();
		if (!rowInfos.size())
			return;

		m_maxPITSize.resize(rowInfos.size());
		size_t diSize = rowInfos[0].cols;
		m_maxPITSize[0] = diSize;

		for (size_t di = 1; di < rowInfos.size(); di++) {
			diSize = std::max(diSize, rowInfos[di].cols);
			m_maxPITSize[di] = diSize;
		}
	}

	size_t BasicUniverseBuilder::size(IntDay di) const {
		//const RuntimeData& rt = dataRegistry()->pipelineHandler()->runtimeData();
		//IntDay diEnd = rt.diEnd;

		//if (!m_maxPITSize.size())
		//	(const_cast<BasicUniverseBuilder*>(this))->calculateMaxPITSize();

		//if (diEnd < (IntDay)m_maxPITSize.size() && diEnd >= 0) {
		//	return m_maxPITSize[diEnd];
		//}

		getAlphaIndexMap(di);
		//return m_daysHandled[di];
		return m_aiMap.size();
	}

	Security BasicUniverseBuilder::security(IntDay, Security::Index ii) const {
		ASSERT(ii >= 0 && ii < (Security::Index)m_securitiesList.size(), "Invalid security index requested: " << ii << " - Max size: " << m_securitiesList.size());
		return m_securitiesList[ii];
	}

	SecurityVec& BasicUniverseBuilder::toArray() {
		// If we already have something that
		return m_securitiesList;

		//if (m_securitiesList && m_securitiesList->size() == m_securities.size())
		//	return m_securitiesList;

		//m_securitiesList = SecurityVecPtr(new SecurityVec(m_securities.size()));

		//for (SecurityMap::const_iterator iter = m_securities.begin(); iter != m_securities.end(); iter++) {
		//	Security sec = iter->second;
		//	ASSERT(sec.index != Security::s_invalidIndex, "Invalid index for security: " << sec.toString());
		//	(*m_securitiesList)[sec.index] = sec;
		//}

		//return m_securitiesList;
	}

	void BasicUniverseBuilder::addParsedFile(const std::string& filename) {
		m_parsedFiles[filename] = true;
	}

	bool BasicUniverseBuilder::hasFileBeenUsed(const std::string& filename) const {
		FileMap::const_iterator iter = m_parsedFiles.find(filename);

		if (iter == m_parsedFiles.end())
			return false;

		return iter->second;
	}

	std::string BasicUniverseBuilder::id() const {
		return "MainUniverse";
	}

	DataPtr BasicUniverseBuilder::create(IDataRegistry& dr, const Dataset& ds, const DataDefinition& def) {
		if (ds.name == "Universe") {
			if (std::string(def.name) == "allSecurities") {
				CTrace("Creating buffer for: %s", def.toString(ds));
				return SecurityDataPtr(new SecurityData(this, nullptr, def));
			}
		}

		return nullptr;
	}

	void BasicUniverseBuilder::initDatasets(IDataRegistry& dr, Datasets& datasets) {
	}

	void BasicUniverseBuilder::exec(RuntimeData& rt) {
		CDebug("BasicUniverseBuilder::exec!");
		run(rt);
	}

	const Security* BasicUniverseBuilder::mapSymbol(const std::string& symbol) const {
		return mapUuid(symbol);
	}

	const Security* BasicUniverseBuilder::mapUuid(const std::string& uuid) const {
		if (uuid.empty())
			return nullptr;

		auto iter = m_securities.find(uuid);

		if (iter == m_securities.end())
			return nullptr;

		size_t lookupIndex = iter->second;
		return &m_securitiesList[lookupIndex];
	}

	const Security* BasicUniverseBuilder::mapUuidOnly(const std::string &uuid) const {
		if (uuid.empty())
			return nullptr;

		auto iter = m_securitiesUuidLUT.find(uuid);

		if (iter == m_securitiesUuidLUT.end())
			return nullptr;

		size_t lookupIndex = iter->second;
		return &m_securitiesList[lookupIndex];
	}

	void BasicUniverseBuilder::buildSecurities(SecurityVecPtr securities) {

		for (size_t i = 0; i < securities->size(); i++) {
			Security security = securities->at(i);
			ASSERT(security.index != Security::s_invalidIndex, "Invalid index for security: " << security.toString() << " - Index: " << security.index << " (i = " << i << ")");
//			CTrace("Security-%z: %u (%s)", i, security.index, std::string(security.uuid));
			addExisting(security);
		}

		/// Cache to an array for quicker access ...
		toArray();

		CTrace("Num Items: %z, Num Loaded: %z", securities->size(), m_securities.size());
	}

	Security::Index	BasicUniverseBuilder::index(IntDay di, size_t ii) const {
		AlphaReverseIndexMap& raiMap = getAlphaReverseIndexMap(di);
		return raiMap[ii];
	}

	void BasicUniverseBuilder::preBuild(IDataRegistry& dr, const DataCacheInfo& dci) {
	}

	void BasicUniverseBuilder::preDataBuild(IDataRegistry& dr, const DataCacheInfo& dci) {
	}

	void BasicUniverseBuilder::postBuild(IDataRegistry& dr, const pesa::DataCacheInfo& dci) {
		if (!m_didBuild)
			return;

		CDebug("Reloading the daily and index maps!");

		/// We wanna uncache it from the DataRegistry
		std::string name = this->name();
		std::string imapName = name + ".IMap";
		std::string dmapName = name + ".DMap";

		dr.clearData(imapName);
		dr.clearData(dmapName);

		m_didLoadMaps = false;
		loadMaps(dr, dci);
	}

	void BasicUniverseBuilder::loadMaps(IDataRegistry& dr, const pesa::DataCacheInfo& dci) {
		if (m_didLoadMaps)
			return;

		std::string name = this->name();
		const UIntMatrixData* imap = dr.get<UIntMatrixData>(nullptr, name + ".IMap", true);

		DataInfo dinfo;
		dinfo.noCacheUpdate = true;
		dinfo.rowHeaders = true;

		const FloatMatrixData* dmap = dr.get<FloatMatrixData>(nullptr, name + ".DMap", &dinfo);

		/// If there is no data then we need to build it from the universe ...
		if (!imap || !dmap) {
			CDebug("No IMap or DMap indexes found!. Constructing from universe data ...");
			const UIntMatrixData* universeData = dr.get<UIntMatrixData>(nullptr, name + ".Universe", true);
			if (universeData) {
				auto rows = universeData->rows();
				auto cols = universeData->cols();

				for (auto di = 0; di < rows; di++) {
					for (auto ii = 0; ii < cols; ii++) {
						Security::Index secId = (*universeData)(di, ii);
						addToAlphaIndexMap((IntDay)di, secId);
					}
				}
			}
		}
		else {
			BasicUniverseBuilder::constructAlphaIndexMap(dci.rt, imap, dmap);
		}

		m_didLoadMaps = true;
	} 

	BasicUniverseBuilder::AlphaReverseIndexMap& BasicUniverseBuilder::getAlphaReverseIndexMap(IntDay di) const {
		AUTO_REC_LOCK(m_stateMutex);
		getAlphaIndexMap(di);
		return m_raiMap;
	}

	void BasicUniverseBuilder::build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		if (!m_didLoadMaps)
			loadMaps(dr, dci);

		m_didBuild = true;
	}

	void BasicUniverseBuilder::buildIMap(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		auto& raiMap = BasicUniverseBuilder::m_raiMap;
		ASSERT(raiMap.size(), "Invalid Reverse alpha index lookup map!");
		UIntMatrixData* data = new UIntMatrixData(dci.universe, 1, raiMap.size());

		for (auto iter = raiMap.begin(); iter != raiMap.end(); iter++) {
			auto index = iter->first;
			auto secId = iter->second;
			(*data)(0, index) = secId;
		}

		frame.data = DataPtr(data);
		frame.dataSize = data->dataSize();
		frame.dataOffset = 0;
	}

	void BasicUniverseBuilder::buildDMap(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		auto& dailyIndexMap = BasicUniverseBuilder::m_dailyIndexMap;
		std::string name = this->name();
		const UIntMatrixData* universeData = dr.get<UIntMatrixData>(nullptr, name + ".Universe", true);
		const UIntMatrixData* imap = dr.get<UIntMatrixData>(nullptr, name + ".IMap", true);
		auto cols = universeData->cols();
		auto numIndexes = BasicUniverseBuilder::m_raiMap.size();
		ASSERT(numIndexes >= cols, "Invalid state. Overall universe indexes: " << numIndexes << " - less than num securities: " << cols << " - in the universe on di = " <<
			dci.di << " [" << dci.rt.dates[dci.di] << "]");
		FloatMatrixData* data = new FloatMatrixData(dci.universe, 1, numIndexes);
		data->setMemory(std::nanf(""));

		for (auto uii = 0; uii < cols; uii++) {
			auto secId = (Security::Index)(*universeData)(dci.di, uii);
			if (secId == Security::s_invalidIndex)
				continue;
			auto aiIter = BasicUniverseBuilder::m_aiMap.find(secId);

			ASSERT(aiIter != BasicUniverseBuilder::m_aiMap.end(), "Unable to find secId: " << secId << " in alpha index map!");
			auto ii = aiIter->second;
			(*data)(0, ii) = 1.0f;
		}

		frame.data = DataPtr(data);
		frame.dataSize = data->dataSize();
		frame.dataOffset = 0;
	}

	void BasicUniverseBuilder::buildAlphaIndexMap(const RuntimeData& rt) const {
		CDebug("Precaching initial securities index ...");
		if (m_aiMap.size())
			return;

		m_buildingAlphaIndexMap = true;

		/// Build the alpha index map for all the days ...
		for (IntDay di = 0; di < (IntDay)rt.diSimEnd(); di++)
			getAlphaIndexMap(di);

		m_buildingAlphaIndexMap = true;

		CDebug("... initial index DONE!");
	}

	BasicUniverseBuilder::AlphaIndexMap& BasicUniverseBuilder::getAlphaIndexMap(IntDay di) const {
		AUTO_REC_LOCK(m_stateMutex);

		if (m_daysHandled.find(di) != m_daysHandled.end())
			return m_aiMap;

		if (!m_aiMap.size() && !m_buildingAlphaIndexMap) {
			buildAlphaIndexMap(dataRegistry()->pipelineHandler()->runtimeData());
		}

		/// ok ... now just iterate through the universe
		const auto* data = universeData();
		size_t diSize = data->cols();
		auto* secMaster = const_cast<BasicUniverseBuilder*>(this)->dataRegistry()->getSecurityMaster();
		size_t maxOffset = diSize;

		for (size_t ii = 0; ii < diSize; ii++) {
			Security::Index index = universeDataIndex(di, ii);

			/// Once we encounter an invalid security that's the end of the list for us ...
			if (index == Security::s_invalidIndex)
				continue;

			//ASSERT(index != Security::s_invalidIndex, "Invalid security at universe index (di, ii): (" << di << ", " << ii << ")");

			/// TODO: Fix issue in case index is s_invalidIndex (or out of bounds otherwise!)
			Security sec = secMaster->security(di, index);

			ASSERT(sec.index == index, "Index does not match with security master. Expected: " << index << " - Found: " << sec.index);

			AlphaIndexMap::const_iterator iter = m_aiMap.find(index);

			/// if we cannot find the security in there then we add a new one
			if (iter == m_aiMap.end()) {
				size_t size = m_aiMap.size();
				m_aiMap[index] = size;
				m_raiMap[size] = index;
				maxOffset = m_aiMap.size();
			}
			else {
				size_t offset = std::distance<AlphaIndexMap::const_iterator>(m_aiMap.begin(), iter);
				if (offset > maxOffset)
					maxOffset = offset;
			}

			m_dailyIndexMap[(size_t)di][(size_t)index] = true;
		}

		m_daysHandled[di] = m_aiMap.size();

		return m_aiMap;
	}

	void BasicUniverseBuilder::addToAlphaIndexMap(IntDay di, Security::Index secId) {
		AlphaIndexMap::iterator iter = m_aiMap.find(secId);
		if (iter == m_aiMap.end()) {
			size_t size = m_aiMap.size();
			m_aiMap[secId] = size;
			m_raiMap[size] = secId;
			m_dailyIndexMap[(size_t)di][(size_t)secId] = true;
		}
	}

	size_t BasicUniverseBuilder::maxAlphaSize(IntDay di) {
		AUTO_REC_LOCK(m_stateMutex);

		AlphaIndexMap& aiMap = getAlphaIndexMap(di);
		return m_daysHandled[di];
	}

	size_t BasicUniverseBuilder::alphaIndex(pesa::IntDay di, size_t ii) {
		AUTO_REC_LOCK(m_stateMutex);

		//AlphaIndexMap& aiMap	= getAlphaIndexMap(di);
		//Security::Index index	= universeDataIndex(di, ii);
		//AlphaIndexMap::iterator iter = aiMap.find(index);

		//ASSERT(iter != aiMap.end(), "Invalid security map. Could not find security at index (di, ii): (" << di << ", " << ii << "). Security details: " << security(di, (Security::Index)ii).debugString());

		//return iter->second;

		auto& aiMap = getAlphaIndexMap(di);
		auto iter = aiMap.find((Security::Index)ii);
		ASSERT(iter != aiMap.end(), "Universe: " << name() << " - Could not reverse map security at index (di, ii): (" << di << ", " << ii << ")");
		return iter->second;
	}

	bool BasicUniverseBuilder::isValid(pesa::IntDay di, size_t ii) const {
		AUTO_REC_LOCK(m_stateMutex);

		if (!m_dmap) {
			Security::Index index = this->index(di, ii);
			DailyIndexLookup& daily = m_dailyIndexMap[(size_t)di];
			bool isPresent = daily.find((size_t)index) != daily.end();

			return isPresent;
		}

		return m_dmap->isValid(di, ii);
	}

	void BasicUniverseBuilder::constructAlphaIndexMap(const RuntimeData& rt, const UIntMatrixData* imap, const FloatMatrixData* dmap) {
		m_aiMap.clear();
		m_raiMap.clear();
		m_daysHandled.clear();
		m_dailyIndexMap.clear();

		auto cols = imap->cols();
		auto dcols = dmap->cols();

		ASSERT(cols == dcols, "Number of columes in daily map: " << dcols << " - do not match with the number of columes in index map: " << cols);

		for (size_t ii = 0; ii < cols; ii++) { 
			Security::Index secId = (*imap)(0, ii);
			m_aiMap[secId] = ii;
			m_raiMap[ii] = secId;
		}

		/// Mark all the days as handled
		for (auto di = 0; di < rt.dates.size(); di++)
			m_daysHandled[di] = m_aiMap.size();

		m_dmap = dmap;
	}
} /// namespace helper

} /// namespace pesa 
