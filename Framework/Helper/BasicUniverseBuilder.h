/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// BasicUniverseBuilder.h
///
/// Created on: 18 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/Components/IUniverse.h"
#include "Framework/FrameworkDefs.h"
#include <future>

namespace pesa {

	class UIntMatrixData;

namespace helper {

	class Framework_API BasicUniverseBuilder : public IUniverse {
	protected:
		typedef std::map<std::string, Security> 	SecurityMap;
		typedef std::map<std::string, size_t>		SecurityIndexLookup;
		typedef std::map<std::string, bool> 		FileMap;
		typedef std::map<Security::Index, size_t>	AlphaIndexMap;
		typedef std::map<size_t, Security::Index>	AlphaReverseIndexMap;
		typedef std::map<IntDay, size_t>			AlphaIntDayMap;
		typedef std::map<size_t, bool>				DailyIndexLookup;
		typedef std::map<size_t, DailyIndexLookup>	GlobalIndexLookup;

		SecurityVec				m_securitiesList; 		/// The securities within the system
		SecurityIndexLookup		m_securities;			/// The securities that we currently have
		SecurityIndexLookup  	m_securitiesUuidLUT;	/// The securities lookup table but ONLY on the UUID
		FileMap					m_parsedFiles;			/// Files that we have already parsed ...

		SizeVec					m_maxPITSize;			/// The max size of the universe at that point in time

		mutable bool			m_buildingAlphaIndexMap = false;

		mutable AlphaIntDayMap	m_daysHandled;
		mutable
		AlphaIndexMap			m_aiMap;				/// Alpha index map

		mutable
		AlphaReverseIndexMap	m_raiMap;				/// Reverse alpha index map

		mutable 
		GlobalIndexLookup		m_dailyIndexMap;		/// Daily index lookup map

		const FloatMatrixData*	m_dmap = nullptr;		/// The daily index lookup map (read from the cache)

		mutable
		std::recursive_mutex	m_stateMutex;			/// State stuff multi-thread safe. The rest is stateless anyway ...

		bool					m_didLoadMaps = false;		/// Have we loaded the index and daily maps or not
		bool					m_didBuild = false;			/// Did we make any change to the universe

		static std::string 		makeUuid(const std::string& symbol, const std::string& name);

		void					calculateMaxPITSize();

		void 					buildSecurities(SecurityVecPtr securities);
		void 					addExisting(const Security& sec);
		AlphaIndexMap&			getAlphaIndexMap(IntDay di) const;
		AlphaReverseIndexMap&	getAlphaReverseIndexMap(IntDay di) const;

		void					addToAlphaIndexMap(IntDay di, Security::Index index);
		void					constructAlphaIndexMap(const RuntimeData& rt, const UIntMatrixData* imap, const FloatMatrixData* dmap);

		Security::Index			universeDataIndex(IntDay di, size_t ii) const;
		void					mapSecuritySymbol(const Security& sec, size_t lookupIndex);
		void					mapSecurityUuid(const Security& sec, size_t lookupIndex);

	public:
								BasicUniverseBuilder(const ConfigSection& config, const std::string& logChannel);
		virtual 				~BasicUniverseBuilder();

		Security 				get(const std::string& uuid) const;
		Security::Index			find(const std::string& uuid) const;
		Security::Index			add(const Security& sec);
		SecurityVec& 			toArray();
		void 					addParsedFile(const std::string& filename);
		bool 					hasFileBeenUsed(const std::string& filename) const;

		virtual void 			buildIMap(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void 			buildDMap(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void 			loadMaps(IDataRegistry& dr, const pesa::DataCacheInfo& dci);


		////////////////////////////////////////////////////////////////////////////
		/// IComponent overrides
		////////////////////////////////////////////////////////////////////////////
		virtual std::string 	id() const;
		virtual void 			exec(RuntimeData& rt);

		////////////////////////////////////////////////////////////////////////////
		/// IPipelineComponent overrides
		////////////////////////////////////////////////////////////////////////////

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual DataPtr 		create(IDataRegistry& dr, const Dataset& ds, const DataDefinition& def);
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			preBuild(IDataRegistry& dr, const DataCacheInfo& dci);
		virtual void 			preDataBuild(IDataRegistry& dr, const DataCacheInfo& dci);
		virtual void 			postBuild(IDataRegistry& dr, const pesa::DataCacheInfo& dci);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void			buildAlphaIndexMap(const RuntimeData& rt) const;

		////////////////////////////////////////////////////////////////////////////
		/// IUniverse overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			combine(FloatMatrixData& thisAlpha, IUniversePtr other, FloatMatrixData& otherAlpha) {}
		virtual Security::Index	index(IntDay di, size_t ii) const;
		virtual Security 		security(IntDay di, Security::Index ii) const;
		virtual size_t 			size(IntDay di) const;
		virtual size_t			alphaIndex(pesa::IntDay di, size_t ii);
		virtual size_t			maxAlphaSize(IntDay di);
		virtual bool			isValid(pesa::IntDay di, size_t ii) const;
		virtual const FloatMatrixData* dailyValidMap() const { return m_dmap; }

		virtual const Security* mapSymbol(const std::string& ticker) const;
		virtual const Security* mapUuid(const std::string& uuid) const;
		virtual const Security* mapUuidOnly(const std::string& uuid) const ;
	};

} /// namespace helper

} /// namespace pesa 

