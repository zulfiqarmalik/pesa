/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Email.cpp
///
/// Created on: 11 Jan 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Poco/Net/SMTPClientSession.h"
#include "Poco/Net/MailMessage.h"
#include "Poco/Net/MailRecipient.h"
#include "Poco/Net/PartSource.h"
#include "Poco/Net/StringPartSource.h"

#include "Poco/File.h"
#include "Poco/FileStream.h"

#include "Email.h"

#include "Core/Lib/Util.h"

namespace pesa {
namespace helper {

	using namespace Poco::Net;

	Email::Email(const std::string& host, int port /* = 25 */)
		: m_session(new SMTPClientSession(host, port))
		, m_msg(new MailMessage()) {
		m_session->login();
		//if (!m_session->startTLS())
		//	throw std::runtime_error("Unable to start TLS session with secure SMTP server: " + host);
	}

	Email::~Email() {
		delete m_msg;
		delete m_session;
	}

	StringVec Email::parseRecipients(const std::string& recipients) {
		return Util::split(recipients, ";");
	}

	Email& Email::setSender(const std::string& sender) {
		m_msg->setSender(sender);
		return *this;
	}

	Email& Email::addRecipients(const std::string& recipientsStr) {
		auto recipients = Email::parseRecipients(recipientsStr);
		ASSERT(recipients.size(), "Invalid recipients: " << recipientsStr);

		for (const auto& address : recipients) {
			MailRecipient recipient(MailRecipient::PRIMARY_RECIPIENT, address);
			m_msg->addRecipient(recipient);
		}

		return *this;
	}

	Email& Email::setSubject(const std::string& subject) {
		m_msg->setSubject(subject);
		return *this;
	}

	Email& Email::addMessage(const std::string& msg) {
		m_msg->addContent(new Poco::Net::StringPartSource(msg));
		return *this;
	}

	Email& Email::addAttachmentGeneric(const std::string& name_, const std::string& filename, const std::string& type, bool asInline, int encoding) {
		std::string name = name_;

		if (name.empty()) {
			Poco::Path path(filename);
			name = path.getFileName();
		}

		std::string content = Util::readEntireTextFile(filename);

		/// If there is no content, then don't attach anything 
		if (content.empty())
			return *this;

		PartSource* part = m_msg->createPartStore(content, type, name);
		if (asInline) 
			part->headers().add("Content-ID", "<image>");

		m_msg->addPart(name, part, !asInline ? MailMessage::CONTENT_ATTACHMENT : MailMessage::CONTENT_INLINE, (MailMessage::ContentTransferEncoding)encoding);

		return *this;
	}

	Email& Email::addTextAttachment(const std::string& name, const std::string& filename) {
		Info("EMAIL", "Attaching file: %s", filename);
		return addAttachmentGeneric(name, filename, "data", false, MailMessage::ENCODING_8BIT);
	}

	Email& Email::addImageAttachment(const std::string& name, const std::string& filename, bool asInline) {
		Info("EMAIL", "Attaching image: %s [Inline = %s]", filename, asInline ? std::string("TRUE") : std::string("FALSE"));
		return addAttachmentGeneric(name, filename, "image/png", asInline, MailMessage::ENCODING_BASE64);
		//auto* image = m_msg->createPartStore(filename, "image/png");
		//image->headers().add("Content-ID", "<image>");
		//m_msg->addPart("", image, MailMessage::CONTENT_INLINE, MailMessage::ENCODING_BASE64);
		//return *this;
	}

	Email& Email::addPDFAttachment(const std::string& name, const std::string& filename) {
		Info("EMAIL", "Attaching pdf: %s", filename);
		return addAttachmentGeneric(name, filename, "application/pdf", false, MailMessage::ENCODING_BASE64);
	}

	Email& Email::addHtmlMessage(const std::string& htmlMessage) {
		m_msg->addPart("html", new StringPartSource(htmlMessage, "text/html"), MailMessage::CONTENT_INLINE, MailMessage::ENCODING_BASE64);
		return *this;
	}

	void Email::send() {
		m_session->sendMessage(*m_msg);
	}
} /// namespace helper 
} /// namespace pesa 
