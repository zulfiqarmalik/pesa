/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Email.h
///
/// Created on: 11 Jan 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/FrameworkDefs.h"
#include "Framework/Components/ComponentDefs.h"

namespace Poco {
namespace Net {
	class SMTPClientSession;
	class MailMessage;
}
}

namespace pesa {
namespace helper {

	class Framework_API Email {
	private:
		Poco::Net::SMTPClientSession* m_session = nullptr;		/// The SMTP client session
		Poco::Net::MailMessage*	m_msg = nullptr;				/// The mail message

		static StringVec		parseRecipients(const std::string& recipients);

		Email&					addAttachmentGeneric(const std::string& name, const std::string& filename, const std::string& type, bool asInline, int encoding);

	public:
								Email(const std::string& host, int port = 25);
		virtual 				~Email();

		Email&					setSender(const std::string& sender);
		Email&					addRecipients(const std::string& recipients);
		Email&					setSubject(const std::string& subject);
		Email&					addMessage(const std::string& msg);
		Email&					addHtmlMessage(const std::string& htmlMessage);
		Email&					addTextAttachment(const std::string& name, const std::string& filename);
		Email&					addImageAttachment(const std::string& name, const std::string& filename, bool asInline);
		Email&					addPDFAttachment(const std::string& name, const std::string& filename);

		void					send();

		//////////////////////////////////////////////////////////////////////////
		/// Inline functions
		//////////////////////////////////////////////////////////////////////////
		inline Poco::Net::SMTPClientSession& session() { return *m_session; }
	};

} /// namespace helper 
} /// namespace pesa 

