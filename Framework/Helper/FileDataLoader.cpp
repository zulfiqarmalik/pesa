/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// FileDataLoader.cpp
///
/// Created on: 7 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "FileDataLoader.h"

#include <fstream>

#include "Poco/StringTokenizer.h"
#include "Poco/String.h"
#include "Poco/Zip/ZipArchive.h"
#include "Poco/Zip/ZipStream.h"
#include "Poco/StreamCopier.h"

namespace pesa {
namespace helper {

	FileDataLoader::FileDataLoader(const char* separator /* = "," */, bool isRegex /* = false */, bool hasHeader /* = true */, const char* commentString /* = nullptr */, bool isZip /* = false */)
		: m_separator(separator)
		, m_useRegex(isRegex)
		, m_hasHeader(hasHeader)
		, m_isZip(isZip) {
		if (commentString)
			m_commentString = commentString;
	}

	FileDataLoader::FileDataLoader(const std::string& path, const char* separator /* = "," */, bool isRegex /* = false */, bool hasHeader /* = true */, const char* commentString /* = nullptr */, bool isZip /* = false */)
		: FileDataLoader(separator, isRegex, hasHeader, commentString, isZip) {
		if (commentString)
			m_commentString = commentString;

		openFile(path);

		if (m_hasHeader)
			readHeader();
	}

	void FileDataLoader::openFile(const std::string& path) {
		if (!m_isZip) {
			m_path = path;
		}
		else {
			auto parts = Util::split(path, "@");
			ASSERT(parts.size() == 2, "Invalid zip filename: " << path << ". Zip file paths must follow the convention: <file path.zip>@<name inside the archive>");

			std::string zipFilename = parts[0];
			std::string name = parts[1];

			std::ifstream inp(zipFilename, std::ios::binary);
			ASSERT(inp, "Unable to open zip file: " << zipFilename);

			using namespace Poco::Zip;
			ZipArchive arch(inp);
			ZipArchive::FileHeaders::const_iterator it = arch.findHeader(name);
			ASSERT(it != arch.headerEnd(), "Unable to find file: " << name << " inside zip archive: " << zipFilename);

			/// Create a temporary file to copy the data
			m_tmp = std::make_unique<Poco::TemporaryFile>();
			m_path = m_tmp->path();

			ZipInputStream zipin(inp, it->second);
			std::ofstream out(m_path, std::ios::binary);
			Poco::StreamCopier::copyStream(zipin, out);
		}

		openPath();
	}

	void FileDataLoader::openPath() {
		/// Now we open the file that contains the contents ...
		Poco::FileInputStream* fis = new Poco::FileInputStream(m_path);
		ASSERT(fis->good(), "Unable to load file: " << m_path);
		m_stream = istreamPtr(fis);
	}

	FileDataLoader::~FileDataLoader() {
		m_stream = nullptr;
		if (m_tmp) {
			m_tmp = nullptr;
			Util::deleteFile(m_path);
		}
	}

	void FileDataLoader::loadBuffer(const std::string& buffer) {
		m_stream = istreamPtr(new std::istringstream(buffer));

		if (m_hasHeader)
			readHeader();
	}

	void FileDataLoader::readHeader() {
		std::string header = readLine();
		ASSERT(!header.empty(), "Unable to read the header!");
		m_header = !m_useRegex ? Util::split(header, m_separator) : Util::regexSplit(header, m_separator);

		for (size_t i = 0; i < m_header.size(); i++) {
			auto& h = m_header[i];
			m_headerMap[h] = (int)i;
			m_headerMap[Poco::toLowerInPlace(h)] = (int)i;
		}
	}

	size_t FileDataLoader::lineCount() {
		if (m_lineCount)
			return m_lineCount;

		auto currPos = m_stream->tellg();

		while (!m_stream->eof()) {
			std::string lineStr = readLine();

			if (!lineStr.empty())
				m_lineCount++;
		}

		m_stream = nullptr;
		openPath();

		m_stream->seekg(currPos, m_stream->beg);

		return m_lineCount;
	}

	std::string FileDataLoader::readLine() {
		static const size_t maxLine = 8192;
		char line[maxLine];
		m_stream->getline(line, maxLine);

		std::string lineStr(line);

		/// If we have a valid comment string as input and the string starts with that same comment 
		/// then we ignore it ...
		if (!m_commentString.empty() && lineStr.find(m_commentString) == 0)
			return std::string();

		return lineStr;
	}

	StringVec FileDataLoader::peek(std::string* lineStr /* = nullptr */) {
		if (m_peekLine.size()) {
			if (lineStr) 
				*lineStr = m_peekStr;
			return m_peekLine;
		}

		StringVec values;
		values.reserve(m_header.size());

		std::string line;

		while (!m_stream->eof() && line.empty()) {
			line = readLine();
			line = Util::trim(line);
		}

		if (lineStr)
			*lineStr = line;

		if (line.empty())
			return values;

		m_peekLine = std::move(!m_useRegex ? Util::split(line, m_separator) : Util::regexSplit(line, m_separator));
		m_peekStr = line;

		return m_peekLine;
	}

	void FileDataLoader::consume() {
		m_peekLine.clear();
		m_peekStr = "";
	}

	StringVec FileDataLoader::read(std::string* lineStr /* = nullptr */) {
		auto values = peek(lineStr);
		consume();
		return std::move(values);
	}


} /// namespace helper 
} /// namespace pesa 
