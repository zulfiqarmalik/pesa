/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// FileDataLoader.h
///
/// Created on: 7 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/FrameworkDefs.h"
#include "Framework/Components/IDataLoader.h"
#include "Poco/FileStream.h"
#include "Poco/TemporaryFile.h"

namespace pesa {
namespace helper {

	typedef std::shared_ptr<std::istream> istreamPtr;

	class Framework_API FileDataLoader {
	private:
		typedef std::unordered_map<std::string, int> IndexMap;

		StringVec				m_header;			/// The header
		IndexMap				m_headerMap;		/// Header map indices
		std::string 			m_path; 			/// The path to the file
		std::string				m_separator;		/// The separator
		bool 					m_useRegex = false; /// Use regex
		bool 					m_hasHeader; 		/// Has it got a header or not
		istreamPtr				m_stream = nullptr;	/// The input stream
		std::string				m_commentString;	/// What is the comment string
		size_t					m_lineCount = 0;	/// The total number of lines in the file

		StringVec				m_peekLine; 
		std::string				m_peekStr;

		/// Zip file specific
		bool					m_isZip = false;	/// Whether the supplied file is a zip file or not
		std::unique_ptr<Poco::TemporaryFile> m_tmp = nullptr; /// Temporary file with the extracted content (for zip files only)

		void					openPath();
		void					openFile(const std::string& path);

	public:
								FileDataLoader(const char* separator = ",", bool isRegex = false, bool hasHeader = true, const char* commentString = nullptr, bool isZip = false);
								FileDataLoader(const std::string& path, const char* separator = ",", bool isRegex = false, bool hasHeader = true, const char* commentString = nullptr, bool isZip = false);
		virtual 				~FileDataLoader();

		void 					readHeader();
		std::string 			readLine();
		void 					loadBuffer(const std::string& buffer);
		StringVec 				read(std::string* lineStr = nullptr);
		StringVec 				peek(std::string* lineStr = nullptr);
		void					consume();
		size_t					lineCount();

		inline bool				eof() {
			return m_stream->eof();
		}

		inline istreamPtr		stream() {
			return m_stream;
		}

		inline const StringVec&	header() const {
			return m_header;
		}

		inline int headerIndex(const std::string& header) const {
			auto iter = m_headerMap.find(header);
			if (iter == m_headerMap.end())
				return -1;
			return iter->second;
		}
	};

	typedef std::shared_ptr<FileDataLoader> FileDataLoaderPtr;

} /// namespace helper 
} /// namespace pesa 

