/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// FrameworkUtil.cpp
///
/// Created on: 12 Jul 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "FrameworkUtil.h"

namespace pesa {
	void FrameworkUtil::parseMultipleMappings(const std::string& str, StringVecMap& currencies, StringMap* countryCcy /* = nullptr */, const char* sep /* = "|" */, const char* ccySep /* = ":" */, const char* countrySep /* = "," */) {
		auto parts = Util::split(str, std::string(sep));

		for (const auto& part : parts) {
			auto mappings = Util::split(part, std::string(ccySep));
			ASSERT(mappings.size() == 2, "Invalid mapping: " << part);

			auto currency = mappings[0];
			auto countries = Util::split(mappings[1], std::string(countrySep));

			currencies[currency] = countries;

			if (countryCcy) {
				for (const auto& country : countries)
					(*countryCcy)[country] = currency;
			}
		}
	}

	void FrameworkUtil::parseExchanges(const std::string& exchangesStr, StringVec& exchanges, StringVec& countries) {
		auto parsedExchanges = Util::split(exchangesStr, "|");

		for (size_t i = 0; i < parsedExchanges.size(); i++) {
			const std::string& exchange = parsedExchanges[i];
			auto parts = Util::split(exchange, ":");
			if (parts.size() == 2) {
				auto sexchanges = Util::split(parts[1], ",");
				for (auto& sexch : sexchanges) {
					countries.push_back(parts[0]);
					exchanges.push_back(sexch);
				}
			}
			else {
				exchanges.push_back(exchange);
				countries.push_back("");
			}
		}
	}

	void FrameworkUtil::parseExchangeIds(const std::string& str, StringMap& exchangeIds) {
		pesa::StringVecMap countries;
		StringMap countryExchanges;

		auto parts = Util::split(str, "|");

		for (const auto& part : parts) {
			auto mappings = Util::split(part, "=");
			ASSERT(mappings.size() == 2, "Invalid mapping: " << part);

			auto exchangeId = mappings[1];
			auto exchangeName = Util::split(mappings[0], ":")[1];

			exchangeIds[exchangeName] = exchangeId;
		}

		//parseMultipleMappings(str, countries, &countryExchanges);

		//for (const auto& iter : countries) {
		//	auto exchangesVec = iter.second;

		//	for (const auto& exchangeStr : exchangesVec) {
		//		auto parts = Util::split(exchangeStr, "=");
		//		ASSERT(parts.size() == 2, "Invalid exchange id definition: " << exchangeStr);
		//		exchangeIds[parts[0]] = parts[1];
		//	}
		//}
	}

	void FrameworkUtil::parseBBExchangeSuffixes(const std::string& str, StringMap& exchangeIdToBB, StringMap& bbExchangeToId) {
		auto parts = Util::split(str, ",");

		for (const auto& part : parts) {
			auto mappings = Util::split(part, "=");
			ASSERT(mappings.size() == 2, "Invalid BB exchange quote: " << part << ". Excepted to be of format <EXCHANGE_ID>:<BB_EXCHANGE_SUFFIX>");
			exchangeIdToBB[mappings[0]] = mappings[1];
			bbExchangeToId[mappings[1]] = mappings[0];
		}
	}

	void FrameworkUtil::parseCurrencyQuoteStyles(const std::string& str, StringMap& pairToCurrency, StringMap& currencyToPair) {
		auto parts = Util::split(str, ",");

		for (const auto& part : parts) {
			auto mappings = Util::split(part, ":");
			ASSERT(mappings.size() == 2, "Invalid currency quote style: " << part << ". Excepted to be of format <CUR>:<CURRENCY_PAIR>");

			std::string currency = mappings[0];
			std::string currencyPair = mappings[1];

			pairToCurrency[currencyPair] = currency;
			currencyToPair[currency] = currencyPair;
		}
	}

	void FrameworkUtil::parseExchangeIds_Detailed(const std::string& str, StringVec& exchangeIds, StringMap& exchangeNames, StringMap& exchangeCountries) {
		pesa::StringVecMap countries;
		StringMap countryExchanges;

		auto parts = Util::split(str, "|");

		for (const auto& part : parts) {
			auto mappings = Util::split(part, "=");
			ASSERT(mappings.size() == 2, "Invalid mapping: " << part);

			auto exchangeId = mappings[1];
			auto exchangeNameParts = Util::split(mappings[0], ":");

			ASSERT(exchangeNameParts.size() == 2, "Invalid mapping: " << part);
			auto country = exchangeNameParts[0];
			auto exchangeName = exchangeNameParts[1];
			 
			exchangeCountries[exchangeId] = country;
			exchangeNames[exchangeId] = exchangeName;
			exchangeIds.push_back(exchangeId);
		}
	} 

	void FrameworkUtil::parseMappings(const std::string& str, std::map<std::string, std::string>* fmappings, std::map<std::string, std::string>* rmappings /* = nullptr */,
		const char* sep /* "," */, const char* mapSep /* "=" */) {
		auto parts = Util::split(str, std::string(sep));

		for (const auto& part : parts) {
			auto mappings = Util::split(part, std::string(mapSep));
			ASSERT(mappings.size() == 2, "Invalid mapping: " << part);

			auto lhs = mappings[0];
			auto rhs = mappings[1];

			(*fmappings)[lhs] = rhs;
			if (rmappings)
				(*rmappings)[rhs] = lhs;
		}
	}

	void FrameworkUtil::parseMappings(const std::string& str, std::map<std::string, std::vector<std::string>>* fmappings, std::map<std::string, std::string>* rmappings /* = nullptr */,
		const char* sep /* "," */, const char* mapSep /* "=" */, const char* submapSep /* = "|" */) {
		std::string mapListSep = submapSep;
		auto parts = Util::split(str, std::string(sep));

		for (const auto& part : parts) {
			auto mappings = Util::split(part, std::string(mapSep));
			ASSERT(mappings.size() == 2, "Invalid mapping: " << part);
			auto lhs = mappings[0];
			auto rhsParts = Util::split(mappings[1], mapListSep);

			for (auto rhs : rhsParts) {
				(*fmappings)[lhs].push_back(rhs);
				if (rmappings)
					(*rmappings)[rhs] = lhs;
			}
		}
	}


} /// namespace pesa 
