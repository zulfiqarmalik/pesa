/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// FrameworkUtil.h
///
/// Created on: 12 Jul 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/FrameworkDefs.h"
#include "Core/Lib/Util.h"
#include "Core/IO/Stream.h"

#include "Framework/Components/ComponentDefs.h"

#include "Poco/Path.h"
#include "Poco/LocalDateTime.h"

#include <unordered_map>
#include <string>
#include <sstream>
#include <vector>

namespace pesa {
	//////////////////////////////////////////////////////////////////////////
	/// FrameworkUtil: Static helper functions
	//////////////////////////////////////////////////////////////////////////
	class Framework_API FrameworkUtil {
	public:
		static void					parseMultipleMappings(const std::string& str, pesa::StringVecMap& currencies, StringMap* countryCcy = nullptr, const char* sep = "|", const char* ccySep = ":", const char* countrySep = ",");
		static void					parseExchanges(const std::string& exchangesStr, StringVec& exchanges, StringVec& countries);
		static void					parseExchangeIds(const std::string& str, StringMap& exchangeIds);
		static void					parseExchangeIds_Detailed(const std::string& str, StringVec& exchangeIds, StringMap& exchangeNames, StringMap& exchangeCountries);

		static void					parseMappings(const std::string& str, std::map<std::string, std::string>* mappings, std::map<std::string, std::string>* rmappings = nullptr,
			const char* sep = ",", const char* mapSep = "=");

		static void					parseMappings(const std::string& str, std::map<std::string, std::vector<std::string> >* mappings, std::map<std::string, std::string>* rmappings = nullptr,
			const char* sep = ",", const char* mapSep = "=", const char* submapSep = "|");

		static void					parseCurrencyQuoteStyles(const std::string& str, StringMap& pairToCurrency, StringMap& currencyToPair);

		static void 				parseBBExchangeSuffixes(const std::string& str, StringMap& exchangeIdToBB, StringMap& bbExchangeToId);

		//////////////////////////////////////////////////////////////////////////
		/// Inline functions
		//////////////////////////////////////////////////////////////////////////
		static Inline void			parseCurrencies(const std::string& str, pesa::StringVecMap& currencies, StringMap* countryCcy = nullptr) {
			return FrameworkUtil::parseMultipleMappings(str, currencies, countryCcy);
		}

		static Inline void			parseExchanges(const std::string& str, pesa::StringVecMap& countries, StringMap* exchangeCountry = nullptr) {
			FrameworkUtil::parseMultipleMappings(str, countries, exchangeCountry);
		}
	};
} /// namespace pesa 
