/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// MergedUniverse.cpp
///
/// Created on: 07 Oct 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "MergedUniverse.h"
#include "Framework/Pipeline/IDataRegistry.h"
#include "Framework/Data/MatrixData.h"

namespace pesa {
namespace helper {
	size_t MergedUniverse::merge(UIntVec& result, IntDay di, const RuntimeData& rt, std::vector<IUniverse*>& universes) {
		std::map<uint32_t, size_t> uniqueSecurities;
		UIntVec diUniverse;

		for (IUniverse* universe : universes) {
			std::string name = universe->name();
			const UIntMatrixData* universeData = universe->universeData();
			size_t cols = universeData->cols();

			for (size_t ii = 0; ii < cols; ii++) {
				uint32_t secId = (*universeData)(di, ii);

				if (secId != Security::s_invalidIndex) {
					auto iter = uniqueSecurities.find(secId);
					if (iter != uniqueSecurities.end()) {
						iter->second++;
					}
					else {
						result.push_back(secId);
						uniqueSecurities[secId] = 1;
					}
				}
				//else
				//	result.push_back(secId); /// We need to match the universe size
			}
		}

		return result.size();
	}

	UIntMatrixData* MergedUniverse::merge(const RuntimeData& rt, std::vector<IUniverse*>& universes) {
		size_t diMax = 0;
		size_t maxRows = rt.dates.size();

		for (IUniverse* universe : universes)
			maxRows = std::min(maxRows, universe->universeData()->rows());

		std::vector<UIntVec> dataVec(maxRows);

		for (IntDay di = 0; di < (IntDay)maxRows; di++) {
			UIntVec& diData = dataVec.at(di);
			size_t diCount = MergedUniverse::merge(diData, di, rt, universes);

			if (diCount > diMax)
				diMax = diCount;
		}

		/// Now we create a matrix 
		UIntMatrixData* data = new UIntMatrixData(nullptr, maxRows, diMax, false);
		size_t rows = data->rows();
		size_t cols = data->cols();

		for (size_t di = 0; di < rows; di++) {
			size_t diCount = dataVec[di].size();

			for (size_t ii = 0; ii < diCount; ii++)
				(*data)((IntDay)di, ii) = dataVec[di][ii];

			/// fill the rest of the data with invalid ...
			for (size_t ii = diCount; ii < cols; ii++)
				(*data)((IntDay)di, ii) = Security::s_invalidIndex;
		}

		return data;
	}

	std::string MergedUniverse::id() const {
		return m_name;
	}

	std::string MergedUniverse::name() const {
		return m_name;
	}

	std::string MergedUniverse::makeName(std::vector<IUniverse*>& universes) {
		StringVec names(universes.size());
		for (size_t i = 0; i < universes.size(); i++)
			names[i] = universes[i]->name();
		std::sort(names.begin(), names.end());
		return Util::combine(names, "|");
	}

	//////////////////////////////////////////////////////////////////////////
	MergedUniverse::MergedUniverse(const RuntimeData& rt, std::vector<IUniverse*>& universes)
		: BasicUniverseBuilder(universes[0]->config(), "MERGED_UNIVERSE") {
		m_name = MergedUniverse::makeName(universes);
		m_data = rt.dataRegistry->get<UIntMatrixData>(nullptr, m_name, true, nullptr, true);

		if (!m_data) {
			UIntMatrixData* mergedData = MergedUniverse::merge(rt, universes);
			rt.dataRegistry->setData(m_name, mergedData);
			m_data = rt.dataRegistry->get<UIntMatrixData>(nullptr, m_name, true, nullptr, true);
			ASSERT(m_data, "Why is this not in the cache: " << m_name);
		}
	}

	MergedUniverse::~MergedUniverse() {
	}

	Security MergedUniverse::security(IntDay di, Security::Index ai) const {
		/// Get it from sec master which does not have a concept of di (but we pass it nonetheless)
		Security::Index ii = index(di, ai);

		/// Return an invalid security
		if (ii == Security::s_invalidIndex)
			return Security();

		auto* secMaster = const_cast<MergedUniverse*>(this)->dataRegistry()->getSecurityMaster();
		ASSERT(secMaster, "Unable to load security master from the data registry!");

		return secMaster->security(di, ii);
	}

	const UIntMatrixData* MergedUniverse::universeData() const {
		return m_data;
	}
} /// namespace helper
} /// namespace pesa

