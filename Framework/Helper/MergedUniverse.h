/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// MergedUniverse.h
///
/// Created on: 07 Oct 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "BasicUniverseBuilder.h"

namespace pesa {
namespace helper {

	//////////////////////////////////////////////////////////////////////////
	/// MergedUniverse
	//////////////////////////////////////////////////////////////////////////
	class Framework_API MergedUniverse : public BasicUniverseBuilder {
	protected:
		std::string				m_name;					/// The name of the universe
		const UIntMatrixData*	m_data;					/// The merged universe ids

	public:
								MergedUniverse(const RuntimeData& rt, std::vector<IUniverse*>& universes);
								~MergedUniverse();

		////////////////////////////////////////////////////////////////////////////
		/// IUniverse overrides
		////////////////////////////////////////////////////////////////////////////
		virtual Security 		security(IntDay di, Security::Index ii) const;
		virtual const UIntMatrixData* universeData() const;
		virtual void 			build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {}

		virtual std::string 	id() const;
		virtual std::string 	name() const;
		virtual bool			isMerged() const { return true; }

		//////////////////////////////////////////////////////////////////////////
		/// Static functions
		//////////////////////////////////////////////////////////////////////////
		static std::string		makeName(std::vector<IUniverse*>& universes);
		static UIntMatrixData*	merge(const RuntimeData& rt, std::vector<IUniverse*>& universes);
		static size_t			merge(UIntVec& result, IntDay di, const RuntimeData& rt, std::vector<IUniverse*>& universes);
	};

	typedef std::shared_ptr<MergedUniverse> MergedUniversePtr;

} /// namespace helper
} /// namespace pesa

