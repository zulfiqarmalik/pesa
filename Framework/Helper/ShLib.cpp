/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// SharedObjectCache.cpp
///
/// Created on: 7 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "ShLib.h"
#include "Core/Lib/Profiler.h"

namespace pesa {
namespace helper {

	ShLib ShLib::s_global;

	ShLib::ShLib() {
	}

	ShLib::~ShLib() {
	}

	ShLib& ShLib::global() {
		return s_global;
	}

	void ShLib::lock() {
		m_libsMutex.lock();
	}

	void ShLib::unlock() {
		m_libsMutex.unlock();
	}

	SharedLibraryPtr ShLib::find(const std::string& path) {
		AUTO_REC_LOCK(m_libsMutex);

		SharedLibraryPtrMap::iterator iter = m_libs.find(path);

		if (iter == m_libs.end())
			return nullptr;

		return iter->second;
	}

	SharedLibraryPtr ShLib::load(const std::string& path) {
		PROFILE_SCOPED();

		AUTO_REC_LOCK(m_libsMutex);

		/// Try to find it from the cache first ...
		SharedLibraryPtr lib = find(path);

		if (lib != nullptr)
			return lib;

		Trace("SHLIB", "Opening shared library at path: %s", path);

		/// If not found then try to load it ...
		lib = std::make_shared<Poco::SharedLibrary>(path, Poco::SharedLibrary::SHLIB_GLOBAL);
        //		m_libs[path] = lib;

		return lib;
	}

	std::string ShLib::getPath(const ConfigSection& config) {
		PROFILE_SCOPED();

		std::string path = config.get("path");

		if (!path.empty())
			return path;

		std::string moduleId = config.get("moduleId");

		/// If a moduleId is specified then we load from there first 
		if (!moduleId.empty()) {
			ConfigSection* module = config.config()->modules().find(moduleId);

			if (!module) {
				std::string defaultPath = Poco::format("{$APP}/%s.dll", moduleId);
				Warning("SHLIB", "Module section not defined for moduleId: %s. Trying to search in the default path: %s", moduleId, defaultPath);
				return defaultPath;
			}

			path = module->get("path");
			ASSERT(!path.empty(), "Unable to find path in module: " << moduleId);

			return path;
		}
		
		path = config.config()->defs().get("defaultPath");
		ASSERT(!path.empty(), "No path specified in module: " << config.get("id") << ". A path attribute must be specified either via a moduleId (linking to a <Module></Module> or a <defaultPath> element must be given under <Defs> section in order to load a shared library!");

		return path;
	}

	DataPtr ShLib::createData(IDataRegistry* dr, IUniverse* universe, DataDefinition def, const ConfigSection& config) {
		auto* createFPtr = createFuncPtr<DataCreateFunction>(config, dr, nullptr);

		ASSERT(createFPtr, "Unable to find the correct symbol. The symbol should be of the signature: pesa::Data* create<id>(IUniverse* u, DataDefinition def, const ConfigSection& cfg)");
		pesa::Data* data = createFPtr(universe, def, config);
		return pesa::DataPtr(data);
	}
} /// namespace helper
} /// namespace pesa 

