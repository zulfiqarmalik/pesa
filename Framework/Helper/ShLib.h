/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// SharedObjectCache.h
///
/// Created on: 7 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/FrameworkDefs.h"
#include "Poco/SharedLibrary.h"
#include <memory>
#include <vector>
#include <unordered_map>
#include "Framework/Components/IComponent.h"
#include "../Data/Data.h"

namespace pesa {
namespace helper {

	class ShLib;

	typedef std::shared_ptr<Poco::SharedLibrary> SharedLibraryPtr;
	typedef std::shared_ptr<ShLib> ShLibPtr;

	typedef Data* 				DataCreateFunction(IUniverse* universe, DataDefinition def, const ConfigSection& config);

	class Framework_API ShLib {
	private:
		typedef std::unordered_map<std::string, SharedLibraryPtr> SharedLibraryPtrMap;
		typedef std::unordered_map<std::string, ComponentCreateFunction*> ComponentCreateFunctionMap;
		typedef std::unordered_map<std::string, PipelineComponentCreateFunction*> PipelineComponentCreateFunctionMap;

		static ShLib			s_global;		/// Global cache
		SharedLibraryPtrMap 	m_libs;			/// Shared library cache
		std::recursive_mutex	m_libsMutex;	/// Make it thread safe 

	public:

								ShLib();
		virtual 				~ShLib();

		SharedLibraryPtr		find(const std::string& path);
		SharedLibraryPtr		load(const std::string& path);
		std::string 			getPath(const ConfigSection& config);
		void					lock();
		void					unlock();
		DataPtr					createData(IDataRegistry* dr, IUniverse* universe, DataDefinition def, const ConfigSection& config);

		template <typename CreateFunc>
		CreateFunc*				createFuncPtr(const ConfigSection& config, IDataRegistry* dr = nullptr, std::unordered_map<std::string, CreateFunc*>* funcMap = nullptr) {
			const std::string& id = config.get("id");
			ASSERT(!id.empty(), "No id given for config section at line: ");

			std::string symbolName = "create" + id;
			std::string path = getPath(config);
			path = config.config()->defs().resolve(path, 0U);

			std::string funcId = path + "-" + symbolName;
			CreateFunc* createFPtr = nullptr;
			if (funcMap) {
				auto iter = funcMap->find(funcId);
				if (iter != funcMap->end())
					createFPtr = iter->second;
			}

			if (!createFPtr) {
				SharedLibraryPtr lib = load(path);

				ASSERT(lib, "Unable to load library: " << path);

				// ASSERT(lib->hasSymbol(symbolName), "Unable to find symbol: " << symbolName << " in library: " << path << " [Config = " << config.config()->filename() << "]");

				Trace("SHLIB", "Loading symbol: %s", symbolName);

				void* funcPtr = lib->getSymbol(symbolName);
				ASSERT(funcPtr, "Unable to find symbol: " << symbolName << " in library: " << path << " [Config = " << config.config()->filename() << "]");
				createFPtr = reinterpret_cast<CreateFunc*>(funcPtr);

				if (funcMap) {
					(*funcMap)[funcId] = createFPtr;
				}
			}

			return createFPtr;
		}

		template <typename RetClass, typename CreateFunc>
		RetClass*				createGeneric(const ConfigSection& config, IDataRegistry* dr = nullptr, std::unordered_map<std::string, CreateFunc*>* funcMap = nullptr) {
			CreateFunc* createFPtr = createFuncPtr<CreateFunc>(config, dr, funcMap);

			ASSERT(createFPtr, "Unable to find the correct symbol. The symbol should be of the signature: IComponent* create<id>(const ConfigSection& cfg)");
			RetClass* component = dynamic_cast<RetClass*>(createFPtr(config));

			if (dr)
				component->dataRegistry() = dr;

			return component;
		}

		template <class T>
		std::shared_ptr<T>		createComponent(const ConfigSection& config, IDataRegistry* dr = nullptr) {
			IComponent* cptr = createGeneric<T, ComponentCreateFunction>(config, dr, nullptr);
			return std::shared_ptr<T>(dynamic_cast<T*>(cptr));
		}

		template <class T>
		std::shared_ptr<T>		createPipelineComponent(const ConfigSection& config, IDataRegistry* dr = nullptr) {
			IPipelineComponent* cptr = createGeneric<T, PipelineComponentCreateFunction>(config, dr, nullptr);
			return std::shared_ptr<T>(dynamic_cast<T*>(cptr));
		}

		static ShLib& 			global();
	};

} /// namespace helper
} /// namespace pesa 

