/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// TurtleReader.cpp
///
/// Created on: 02 Jul 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "TurtleReader.h"

#include <fstream>

#include "Poco/StringTokenizer.h"
#include "Poco/String.h"
#include "Poco/Zip/ZipArchive.h"
#include "Poco/Zip/ZipStream.h"
#include "Poco/StreamCopier.h"

namespace pesa {
namespace helper {
	void BinTree::add(const char* str, unsigned char key) {
		BinNode* node = add(str, 0, root);
		node->key = key;
	}

	BinNode* BinTree::add(const char* str, size_t index /* = 0 */, BinNode* node) {
		char c = str[index];
		if (c == '\0')
			return node;

		if (!node->children[c]) 
			node->children[c] = new BinNode();

		return add(str, index + 1, node->children[c]);
	}

	BinNode* BinTree::find(const char* str) {
		BinNode* node = root;

		while (node && *str != '\0')
			node = node->children[*str];

		return node;
	}

	//////////////////////////////////////////////////////////////////////////

	size_t TurtleReader::s_maxBufferLength = 75 * 1024 * 1024;		/// Max size of the buffer. This is a guidance and not an exact size. 
																	/// Actual sizes are rounded up to a little bit above this size.

	TurtleReader::TurtleReader(bool isZip /* = false */)
		: m_isZip(isZip) 
		, m_buffer(nullptr) 
		, m_length(0) {
			m_createNode = std::bind(&TurtleReader::createNode, std::ref(*this), std::placeholders::_1);
	}

	TurtleReader::TurtleReader(const std::string& path, bool isZip /* = false */)
		: TurtleReader(isZip) {
		openFile(path);
		readHeader();
	}

	TurtleReader::TurtleReader(char* buffer, size_t length)
		: TurtleReader(false) {
		m_buffer = buffer;
		m_length = length;
	}

	TurtleReader::~TurtleReader() {
		for (char* buffer : m_buffers)
			delete[] buffer;
		m_buffers.clear();
	}

	void TurtleReader::createNode(CNode** node) {
		*node = new CNode();
	}

	void TurtleReader::openFile(const std::string& path) {
		if (!m_isZip) {
			m_path = path;
		}
		else {
			auto parts = Util::split(path, "@");
			ASSERT(parts.size() == 2, "Invalid zip filename: " << path << ". Zip file paths must follow the convention: <file path.zip>@<name inside the archive>");

			std::string zipFilename = parts[0];
			std::string name = parts[1];

			std::ifstream inp(zipFilename, std::ios::binary);
			ASSERT(inp, "Unable to open zip file: " << zipFilename);

			using namespace Poco::Zip;
			ZipArchive arch(inp);
			ZipArchive::FileHeaders::const_iterator it = arch.findHeader(name);
			ASSERT(it != arch.headerEnd(), "Unable to find file: " << name << " inside zip archive: " << zipFilename);

			/// Create a temporary file to copy the data
			m_tmp = std::make_unique<Poco::TemporaryFile>();
			m_path = m_tmp->path();

			ZipInputStream zipin(inp, it->second);
			std::ofstream out(m_path, std::ios::binary);
			Poco::StreamCopier::copyStream(zipin, out);
		}

		openPath();
	}

	void TurtleReader::openPath() {
		/// Now we open the file that contains the contents ...
		Poco::FileInputStream* fis = new Poco::FileInputStream(m_path);
		ASSERT(fis->good(), "Unable to load file: " << m_path);
		m_stream = istreamPtr(fis);
	}

	void TurtleReader::loadBuffer(const std::string& buffer) {
		m_stream = istreamPtr(new std::istringstream(buffer));
		readHeader();
	}

	void TurtleReader::readHeader() {
		m_header = readLine();
	}

	size_t TurtleReader::lineCount() {
		return m_lineCount;
	}

	size_t TurtleReader::nodeCount() {
		return m_nodeCount;
	}

	std::string TurtleReader::readLine() {
		static const size_t maxLine = 8192;
		char line[maxLine];
		m_stream->getline(line, maxLine);

		std::string lineStr(line);

		/// If we have a valid comment string as input and the string starts with that same comment 
		/// then we ignore it ...
		if (!m_commentString.empty() && lineStr.find(m_commentString) == 0)
			return std::string();

		return Util::trim(lineStr);
	}

	void TurtleReader::simplifyLHS(std::string& str) {
		auto parts = Util::split(str, ":");

		if (parts.size() == 2)
			str = parts[1];
	}

	void TurtleReader::simplifyRHS(std::string& str) {
		auto parts = Util::split(str, "^^");
		str = parts[0];

		simplifyId(str);
	}

	void TurtleReader::simplifyId(std::string& str) {
		static const std::string pattern = "<https://permid.org/";

		auto pos = str.find(pattern);

		if (pos != std::string::npos) {
			pos += pattern.length();
			auto posStart = pos;

			while (pos < str.length() && str[pos] != '>')
				pos++;

			str = str.substr(posStart, pos - posStart);
		}
	}

	bool TurtleReader::eof() {
		return m_stream->eof();
	}

	static inline char* readBufferLine(char** iter, char* iterEnd) {
		char* str = *iter;

		while (**iter != '\n' && *iter < iterEnd)
			(*iter)++;

		if (*iter >= iterEnd)
			return nullptr;

		/// Put a NULL terminator here
		(**iter) = '\0';
		(*iter)++; /// Increment once more so that we go past the NULL character

		return str;
	}

	static inline char* readId(char** iter, char* iterEnd, uint64_t& permId) {
		char* str = *iter;

		while (**iter != '\n' && *iter < iterEnd) {
			if (**iter == '/')
				str = (*iter + 1);
			else if (**iter == '1' && *(*iter + 1) == '-') {
				permId = 0;
				(*iter)++;
				str = (*iter + 1);
			}
			else if (**iter == '>')
				**iter = '\0';
			else {
				permId = permId * 10 + (**iter - '0');
			}
			(*iter)++;
		}

		if (*iter >= iterEnd)
			return nullptr;

		/// Put a NULL terminator here
		(**iter) = '\0';
		(*iter)++; /// Increment once more so that we go past the NULL character

		return str;
	}

	static inline void ignoreWhitespace(char** iter, char* iterEnd) {
		char* str = *iter;

		while (*iter < iterEnd && (**iter == ' ' || **iter == '\t' || **iter == '\n' || **iter == '\r'))
			(*iter)++;
	}

	static inline char* readLHS(char** iter, char* iterEnd, unsigned char& lhsId, BinTree* tree) {
		ignoreWhitespace(iter, iterEnd);

		char* str = *iter;
		BinNode* biter = tree ? tree->root : nullptr;

		while (*iter < iterEnd && **iter != ' ' && **iter != '\t' && **iter != '\n') {
			if (**iter == ':') {
				**iter = '\0';
				str = (*iter + 1);
				biter = tree ? tree->root : nullptr;
			}
			else if (biter) 
				biter = biter->children[**iter];

			(*iter)++;
		}

		if (*iter >= iterEnd)
			return nullptr;

		if (biter) {
			ASSERT(biter->key >= 0, "Invalid key for string: " << str);
			lhsId = biter->key;
		}

		**iter = '\0';
		(*iter)++; 

		return str;
	}

	static inline char* readRHS(char** iter, char* iterEnd, bool& nodeEnded) {
		ignoreWhitespace(iter, iterEnd);

		int quoteCount = **iter != '\"' ? 0 : 1;
		char* str = (*iter + quoteCount);
		bool isLink = !quoteCount && **iter == '<';

		while (**iter != '\n' && *iter < iterEnd) {
			if (quoteCount && (**iter == '\"'/* || **iter == ' '*/) )
				**iter = '\0';
			else if (isLink) {
				if (**iter == '/') 
					str = (*iter + 1);
				else if (**iter == '1' && *(*iter + 1) == '-')
					str = (*iter + 2);
				else if (**iter == '>')
					**iter = '\0';
			}

			(*iter)++;
		}

		if (*iter >= iterEnd)
			return nullptr;

		if (*(*iter - 1) == '.')
			nodeEnded = true;

		*(*iter - 1) = '\0';

		/// Put a NULL terminator here
		(**iter) = '\0';
		(*iter)++; /// Increment once more so that we go past the NULL character

		return str;
	}

	void TurtleReader::run() {
		readAll();
	}

	void TurtleReader::readAll() {
		char* iter = m_buffer;
		char* iterEnd = m_buffer + m_length;

		while (iter < iterEnd) {
			while (*iter == '@')
				readBufferLine(&iter, iterEnd);

			/// Find the first non-null character
			while (*iter == ' ' || *iter == '\n')
				iter++;

			if (iter >= iterEnd)
				break;

			uint64_t permId = 0;
			char* id = readId(&iter, iterEnd, permId);

			if (!id || iter >= iterEnd)
				break;

			ignoreWhitespace(&iter, iterEnd);

			CNode* node = nullptr;
			m_createNode(&node);

			bool nodeEnded = false;

			node->id = permId;
			node->emitValue(0, "_id", id);

			ASSERT(iter + strlen(id) < iterEnd, "Invalid string!");

			while (!nodeEnded && iter < iterEnd) {
				if (iter >= iterEnd)
					break;

				unsigned char lhsId = -1;
				char* lhs = readLHS(&iter, iterEnd, lhsId, m_tree);

				if (!lhs || iter >= iterEnd)
					break;

				char* rhs = readRHS(&iter, iterEnd, nodeEnded);

				if (!rhs)
					break;

				if (lhsId >= 0)
					node->emitValue(lhsId, lhs, rhs);
					//node->values[lhs] = rhs;

				ignoreWhitespace(&iter, iterEnd);
			}

			ASSERT(iter <= iterEnd, "Buffer overflow!");

			m_nodes.push_back(node);
		}

		ASSERT(iter <= iterEnd, "Buffer overflow!");
	}

	void TurtleReader::readAllBuffered(char* buffer, size_t bufferLength, size_t maxThreads, std::vector<std::shared_ptr<TurtleReader> >& readers, std::vector<Poco::Thread*>& readerThreads){
		readers.resize(maxThreads);

		size_t startPos = 0;
		size_t maxChunk = bufferLength / maxThreads;

		for (size_t i = 0; i < readers.size() && startPos < bufferLength; i++) {
			size_t endPos = std::min(startPos + maxChunk, bufferLength);

			while (endPos < bufferLength && (buffer[endPos] != '\n' || buffer[endPos - 1] != '\n' || buffer[endPos - 2] != '.'))
				endPos++;

			endPos = std::min(endPos, bufferLength);

			Trace("Turtle", "Buffer range: [%z, %z)", startPos, endPos);

			TurtleReaderPtr reader = TurtleReaderPtr(new TurtleReader(buffer + startPos, endPos - startPos + 1));
			reader->createNodeCallback() = m_createNode;
			reader->tree() = m_tree;

			readers[i] = reader;

			startPos = endPos + 1;

			auto* readerThread = new Poco::Thread();
			readerThread->start(*reader.get());
			readerThreads.push_back(readerThread);

			//readerThreads.push_back(std::thread([&]() {
			//	reader->readAll();
			//}));
		}
	}

	bool TurtleReader::cacheBuffer(char** buffer, size_t* length, size_t& startPos, size_t& endPos, size_t totalLength) {
		/// If there is already a buffer then we 
		if (*buffer) {
			m_buffers.push_back(*buffer);
			//delete[] *buffer;
		}

		*buffer = nullptr;

		if (eof() || endPos >= totalLength)
			return false;

		if (totalLength - startPos < s_maxBufferLength) {
			endPos = totalLength;
		}
		else {
			endPos = std::min(startPos + s_maxBufferLength, totalLength);
		
			m_stream->seekg(endPos, m_stream->beg);

			/// keep on reading until we've encountered an empty line
			auto line = readLine();

			while (!line.empty() && !eof())
				line = readLine();

			endPos = m_stream->tellg();

			if (eof())
				endPos = totalLength;
		}

		endPos = std::min(endPos, totalLength);

		if (endPos <= startPos)
			return false;

		ASSERT(*length < totalLength, "Invalid length: " << *length << " - Total length: " << totalLength);

		*length = endPos - startPos;
		*buffer = new char [*length];

		m_stream->seekg(startPos, m_stream->beg);
		m_stream->read(*buffer, *length);

		/// Move the start pos
		startPos = endPos;

		return true;
	}

	TurtleReader::FileDetailsPtr TurtleReader::readAll(size_t maxThreads) {
		size_t startPos = m_stream->tellg();
		size_t totalLength = Util::getStreamSize(*m_stream.get()) - startPos;
		size_t maxChunk = s_maxBufferLength;
		size_t endPos = startPos;

		static const size_t maxBuffers = 2;
		char* buffers[maxBuffers];
		size_t bufferLengths[maxBuffers];
		size_t bi = 0;
		FileDetailsPtr details = std::make_shared<FileDetails>();

		memset(buffers, 0, sizeof(char*) * maxBuffers);
		memset(bufferLengths, 0, sizeof(size_t) * maxBuffers);

		bool hasMore = cacheBuffer(&buffers[bi], &bufferLengths[bi], startPos, endPos, totalLength);
		size_t batchId = 0;

		while (buffers[bi] && bufferLengths[bi]) {
			++batchId; 

			TurtleReaderPtrVec readers;
			std::vector<Poco::Thread*> readerThreads;

			/// Deal with the current buffer
			readAllBuffered(buffers[bi], bufferLengths[bi], maxThreads, readers, readerThreads);

			/// Move onto the next buffer
			bi = (bi + 1) % maxBuffers;

			/// Cache the next bit of the file
			hasMore = cacheBuffer(&buffers[bi], &bufferLengths[bi], startPos, endPos, totalLength);

			/// Then we join all the threads
			for (auto* readerThread : readerThreads) {
				readerThread->join();
				delete readerThread;
			}

			readerThreads.clear();

			/// Just add the values from all the readers
			size_t totalNodeCount = 0;

			for (auto reader : readers) {
				if (reader)
					totalNodeCount += reader->m_nodes.size();
			}

			Debug("Turtle", "Consolodating reading threads. Batch: %z (Total: %z)", batchId, totalNodeCount);

			if (totalNodeCount) {
				size_t citer = details->values.size();
				details->values.resize(citer + totalNodeCount);

				for (auto reader : readers) {
					if (reader) {
						//for (auto node : reader->m_nodes) {
						memcpy(&details->values[citer], &reader->m_nodes[0], sizeof(CNodeP) * reader->m_nodes.size());
						citer += reader->m_nodes.size();
						//}
					}
				}
			}

			readers.clear();
		}

		//std::vector<std::thread> readerThreads;

		//char* buffer = new char [totalLength];

		//m_stream->read(buffer, totalLength);

		//for (size_t i = 0; i < readers.size() && startPos < totalLength; i++) {
		//	size_t endPos = std::min(startPos + maxChunk, totalLength);
		//	m_stream->seekg(endPos, m_stream->beg);

		//	/// keep on reading until we've encountered an empty line
		//	auto line = readLine();

		//	while (!line.empty() && !eof())
		//		line = readLine();

		//	endPos = m_stream->tellg();

		//	if (eof())
		//		endPos = totalLength;

		//	TurtleReaderPtr reader = TurtleReaderPtr(new TurtleReader(buffer + startPos, endPos - startPos));
		//	readers[i] = reader;

		//	readerThreads.push_back(std::thread([&]() {
		//		reader->readAll();
		//	}));

		//	startPos = endPos;
		//}

		///// We close the stream, since this will just be managing the threads now
		//m_stream = nullptr;

		///// Then we join all the threads
		//for (auto& readerThread : readerThreads)
		//	readerThread.join();

		//readerThreads.clear();

		//delete[] buffer;

		//FileDetailsPtr details = std::make_shared<FileDetails>();

		/////// Just add the values from all the readers
		////for (auto reader : readers) {
		////	if (reader) {
		////		for (auto& value : reader->m_details->values)
		////			details->add(value);
		////	}
		////}

		//readers.clear();

		return details;
	}

	bool TurtleReader::read(TurtleReader::Node& node) {
		std::string line;

		while (!eof() && line.empty()) {
			line = readLine();

			if (line.length()) {
				node.id = line; 

				if (m_simplify)
					simplifyId(node.id);

				bool nodeEnded = false;

				line = "";

				while (!nodeEnded && !eof()) {
					line = readLine();

					if (!line.empty()) {
						auto dotPos = line.find('.');
						nodeEnded = line[line.length() - 1] == '.';

						size_t iter = 0;

						while (line[iter] != ' ' && line[iter] != '\t' && iter < line.length())
							iter++;

						std::string lhs, rhs;

						if (iter < line.length()) {
							lhs = line.substr(0, iter);
							rhs = line.substr(iter, line.length() - iter - 1); 
						}
						else {
							lhs = line;
							line = readLine();
							rhs = line.substr(0, line.length() - 1);
						}

						Util::trim(rhs);
						Poco::removeInPlace(rhs, '\"');

						if (m_simplify) {
							simplifyLHS(lhs);
							simplifyRHS(rhs);
						}

						node.values[lhs] = rhs;
					}
				}

				m_nodeCount++;

				return true;
			}
		}

		return false;
	}
} /// namespace helper 
} /// namespace pesa 
