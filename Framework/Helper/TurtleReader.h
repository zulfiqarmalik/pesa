/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// TurtleReader.h
///
/// Created on: 02 Jul 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/FrameworkDefs.h"
#include "Framework/Components/IDataLoader.h"
#include "Poco/FileStream.h"
#include "Poco/TemporaryFile.h"

//#include "sparsepp/spp.h"

namespace pesa {
namespace helper {

	struct Framework_API BinNode {
		static const int s_max = 128;

		unsigned char key = -1;
		BinNode*	children[s_max];

		BinNode() {
			memset(children, 0, sizeof(BinNode*) * s_max);
		}

		~BinNode() {
			for (auto* c : children) 
				delete c;
		}
	};

	struct Framework_API BinTree {
		BinNode*	root = new BinNode();

		BinNode*	add(const char* str, size_t index, BinNode* node);
		void		add(const char* str, unsigned char key);

		BinNode*	find(const char* str);

		~BinTree() {
			delete root;
		}
	};

	class Framework_API TurtleReader : public Poco::Runnable {
	public:
		//////////////////////////////////////////////////////////////////////////
		struct Node {
			std::string			id;						/// The ID of the node
			StringMap			values;					/// The values

			std::string			value(std::string key) const {
				auto iter		= values.find(key);
				return iter != values.end() ? iter->second : "";
			} 

			inline std::string	operator[](std::string key) const {
				return value(key);
			}

			Node& operator = (const Node& rhs) {
				id = rhs.id;

				for (auto iter = rhs.values.begin(); iter != rhs.values.end(); iter++)
					values[iter->first] = iter->second;

				return *this;
			}
		};

		typedef std::vector<Node> NodeVec;
		typedef std::shared_ptr<Node> NodePtr;
		typedef std::vector<NodePtr> NodePtrVec;
		
		struct CNodeValue {
			unsigned char		lhsId = -1;
			const char*			lhs = nullptr;
			char*				rhs = nullptr;
		};

		typedef std::vector<CNodeValue> CNodeValueVec;

		/// Compact node!
		struct CNode {
			static const unsigned char s_max = 32;
			uint64_t			id = 0;
			CNodeValueVec		values;
			unsigned char		lut[s_max];

			CNode() {
				std::fill_n(lut, s_max, 255);
			}

			~CNode() {
			}

			CNode(const CNode& rhs) {
				*this = rhs;
			}

			//CNode& operator = (const CNode& rhs) {
			//	id = rhs.id;

			//	//for (auto iter = rhs.values.begin(); iter != rhs.values.end(); iter++)
			//	//	values[iter->first] = iter->second;

			//	return *this;
			//}

			Inline const char* value(unsigned char id) {
				if (id < s_max && lut[id] < 255 && lut[id] < (unsigned char)values.size())
					return values[lut[id]].rhs;
				return nullptr;
			}

			Inline std::string valueStr(unsigned char id) {
				auto* v = value(id);
				return v ? std::string(v) : std::string("");
			}

			Inline uint64_t valueId(unsigned char id) { 
				auto* idStr = value(id);

				if (!idStr)
					return 0;

				return CNode::toId<uint64_t>(idStr);
			}

			Inline std::string idStr() {
				return std::string(values[0].rhs);
			}

			Inline const char* operator[](unsigned char id) {
				return value(id);
			}

			virtual void emitValue(unsigned char lhsId, const char* lhs, char* rhs) {
				values.push_back({ lhsId, lhs, rhs });
				if (lhsId >= 0 && lhsId < s_max)
					lut[lhsId] = (unsigned char)values.size() - 1;
			}

			template <class T>
			static Inline T toId(const char* idStr) {
				const char* iter = idStr;
				T id = 0;

				while (*iter != '\0') {
					id = id * 10 + (*iter - '0');
					iter++;
				}

				return id;
			}
		};

		typedef std::function<void(CNode** node)> CreateNode;

		typedef std::vector<CNode> CNodeVec;
		typedef std::shared_ptr<CNode> CNodePtr;
		typedef std::vector<CNodePtr> CNodePtrVec;
		typedef CNode* CNodeP;
		typedef std::vector<CNodeP> CNodePVec;

		//////////////////////////////////////////////////////////////////////////
		struct FileDetails {
			typedef std::unordered_map<uint64_t, TurtleReader::CNodeP> NodeLUT;

			CNodePVec				values;				/// The list of values
			NodeLUT					lut;				/// The ID lookup table   

			TurtleReader*			reader = nullptr;	/// The reader. This is assigned and maintained by the caller

			~FileDetails() {
				for (auto* value : values)
					delete value;
				values.clear();
			} 

			Inline CNodeP			get(uint64_t id) {
				auto iter = lut.find(id);
				return iter != lut.end() ? iter->second : nullptr;
			}

			Inline CNodeP			operator[](uint64_t id) {
				return get(id);
			}

			//int						findId(const std::string& id) {
			//	auto iter = nodeLUT.find(id);
			//	return iter != nodeLUT.end() ? (int)iter->second : -1;
			//}

			Inline void				add(TurtleReader::CNodeP value) {
				values.push_back(value);
				//lut[value->id] = values.size() - 1;
			}

			//void					add(TurtleReader::CNode& cnode) {
			//	Node node;
			//	size_t index		= values.size();
			//	values.push_back(node);

			//	values[index].id				= cnode.id;
			//	
			//	for (const auto& value : cnode.values) {
			//		values[index].values[value.first] = value.second;
			//	}
			//}
		};

		typedef std::shared_ptr<FileDetails> FileDetailsPtr;

		//////////////////////////////////////////////////////////////////////////

	private:
		typedef std::shared_ptr<std::istream> istreamPtr;
		typedef std::unordered_map<std::string, int> IndexMap;

		static size_t			s_maxBufferLength;

		std::string 			m_path; 				/// The path to the file
		std::string				m_header;				/// The header string
		istreamPtr				m_stream = nullptr;		/// The input stream
		std::string				m_commentString = "@";	/// What is the comment string
		size_t					m_lineCount = 0;		/// The total number of lines in the file
		size_t					m_nodeCount = 0;		/// The total number of nodes we've read
		bool					m_simplify = true;		/// Simplify the labels

		char*					m_buffer = nullptr;		/// The start of the buffer
		size_t					m_length = 0;			/// The length of the buffer

		CNodePVec				m_nodes;				/// Nodes
		CreateNode				m_createNode = nullptr;	/// Callback for creating node

		BinTree*				m_tree = nullptr;		/// Tree for indexing strings to ids

		std::vector<char*>		m_buffers;				/// All the buffers that we've created. This is for the top level

		/// Zip file specific
		bool					m_isZip = false;	/// Whether the supplied file is a zip file or not
		std::unique_ptr<Poco::TemporaryFile> m_tmp = nullptr; /// Temporary file with the extracted content (for zip files only)

		void					openPath();
		void					openFile(const std::string& path);

		void					simplifyLHS(std::string& str);
		void					simplifyRHS(std::string& str);
		void					simplifyId(std::string& str);

		void					readAll();
		void					readAllBuffered(char* buffer, size_t bufferLength, size_t maxThreads, std::vector<std::shared_ptr<TurtleReader> >& readers, std::vector<Poco::Thread*>& readerThreads);
		bool					cacheBuffer(char** buffer, size_t* length, size_t& startPos, size_t& endPos, size_t totalLength);

		bool					eof();

								TurtleReader(char* buffer, size_t length);

	public:
								TurtleReader(bool isZip = false);
								TurtleReader(const std::string& path, bool isZip = false);
		virtual 				~TurtleReader();

		void 					readHeader();
		std::string 			readLine();
		FileDetailsPtr			readAll(size_t maxThreads);
		void 					loadBuffer(const std::string& buffer);
		bool					read(TurtleReader::Node& node);
		size_t					lineCount();
		size_t					nodeCount();

		virtual void			run();

		void					createNode(CNode** node);

		inline istreamPtr		stream() { return m_stream; }
		inline bool&			simplify() { return m_simplify; }
		inline bool				simplify() const { return m_simplify; }
		inline CreateNode&		createNodeCallback() { return m_createNode; }
		inline BinTree*&		tree() { return m_tree; }
	};

	typedef std::shared_ptr<TurtleReader>	TurtleReaderPtr;
	typedef std::vector<TurtleReaderPtr>	TurtleReaderPtrVec;

} /// namespace helper 
} /// namespace pesa 

