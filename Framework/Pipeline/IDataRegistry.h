/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// DataRegistry.h
///
/// Created on: 4 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#ifndef ENGINE_DATAREGISTRY_H_
#define ENGINE_DATAREGISTRY_H_

#include "Framework/FrameworkDefs.h"
#include "Framework/Pipeline/IPipelineComponent.h"
#include "Framework/Components/IDataLoader.h"
#include "Core/Math/Math.h"
#include "Framework/Data/Data.h"
#include "Framework/Data/StringData.h"
#include "Framework/Data/Dataset.h"
#include "Framework/Data/MatrixData.h"
#include "Framework/Data/VarMatrixData.h"

namespace pesa {
	struct AlphaResult;

	struct DataInfo {
		bool						noCacheUpdate = false;			/// Do not update the cache if it needs updating for this particular dataset
		pesa::Metadata*				metadata = nullptr;				/// Optional metadata for the dataset (null means no interest in metadata)
		bool						onlyCheckCached = false;		/// Only check datasets that have already been cached
		bool						loadEntire = false;				/// Load the entire dataset instead of just loading the range:[diStart - backdays, diEnd]
		bool						noOptimise = false;				/// Do not optimise the data at all!
		bool						noAssertOnMissingDataLoader = false; /// Do not assert if the user requests a data that does not have a corresponding IDataLoader. Just return nullptr
		bool						dataWindow = false;				/// Use the data window
		bool						rowHeaders = false;				/// Whether to save the row headers or not

		bool						useShMem = false;				/// Use shared memory or not 
		size_t						shmemAddress = 0;				/// The address to the sharedmemory where the data is immediately available!
	};

	////////////////////////////////////////////////////////////////////////////
	/// IDataRegistry: The main data registry. Responsible for caching of
	/// all data in a homogenous format defined by each individual
	/// IDataLoader implementations.
	////////////////////////////////////////////////////////////////////////////
	class IDataRegistry : public IPipelineComponent {
	public:
		IDataRegistry(const ConfigSection& config, const std::string& logChannel)
			: IPipelineComponent(config, !logChannel.empty() ? logChannel : "DR") {
		}

		virtual void 				runForData(const pesa::RuntimeData& rt, pesa::IDataLoaderPtr dl) = 0;
		virtual pesa::IDataLoader*  getDataLoader(const std::string& dataId) = 0;
		virtual pesa::Metadata 		getMetadata(IUniverse* universe, const std::string& dataId) = 0;
		virtual pesa::DataPtr		getDataPtr(IUniverse* universe, const std::string& dataId, DataInfo* dinfo) = 0;
		virtual pesa::DataPtrVec	getAllDataPtr(IUniverse* universe, const std::string& dataId, DataInfo* dinfo) = 0;
		virtual void*				createDataCheckpoint() { return nullptr; }
		virtual void				deleteDataCheckpoint(void* checkpoint) {}

		/// DO NOT DELETE THE POINTER AFTER setData. DataRegistry will own the pointer
		virtual pesa::DataPtr		setData(const std::string& dataId, pesa::Data* data) = 0;
		virtual void 				clearData(const std::string& dataId) = 0;

		virtual pesa::IUniverse* 	getUniverse(const std::string& id, bool noOptimise = false) = 0;
		virtual void 				debugDump(const pesa::RuntimeData& rt, const pesa::DumpInfo& dumpInfo) = 0;

		virtual pesa::DataPtr		createDataBuffer(pesa::IUniverse* universe, pesa::IDataLoaderPtr dl, const pesa::Dataset& ds, const pesa::DataDefinition& def, pesa::DataPtr metadata) = 0;
		virtual pesa::DataPtr		createDataBuffer(pesa::IUniverse* universe, const DataDefinition& def) = 0;

		virtual std::vector<pesa::IUniverse*> getActiveUniverses() { return std::vector<pesa::IUniverse*>(); }

		pesa::AlphaResult*			alphaResultData(const std::string& dataId);

		virtual pesa::IUniverse* 	getSecurityMaster() {
			return getUniverse("Universe.SecMaster");
		}

		virtual pesa::DataPtr		getDataPtr(IUniverse* universe, const std::string& dataId, bool noCacheUpdate = false, pesa::Metadata* metadata = nullptr, bool onlyCheckCached = false) {
			DataInfo dinfo;
			dinfo.noCacheUpdate = noCacheUpdate;
			dinfo.metadata = metadata;
			dinfo.onlyCheckCached = onlyCheckCached;
			return this->getDataPtr(universe, dataId, &dinfo);
		}

		virtual const pesa::Data* 	getData(IUniverse* universe, const std::string& dataId, bool noCacheUpdate = false, pesa::Metadata* metadata = nullptr, bool onlyCheckCached = false) {
			DataInfo dinfo;
			dinfo.noCacheUpdate = noCacheUpdate;
			dinfo.metadata = metadata;
			dinfo.onlyCheckCached = onlyCheckCached;
			DataPtr data = getDataPtr(universe, dataId, &dinfo);
			return data ? data.get() : nullptr;
		}

		template <class t_type>
		const t_type* 		get(IUniverse* universe, const std::string& dataId, bool noCacheUpdate = false, pesa::Metadata* metadata = nullptr, bool onlyCheckCached = false) {
			DataInfo dinfo;
			dinfo.noCacheUpdate = noCacheUpdate;
			dinfo.metadata = metadata;
			dinfo.onlyCheckCached = onlyCheckCached;
			DataPtr data = this->getDataPtr(universe, dataId, &dinfo);

			if (!data)
				return nullptr;

			return dynamic_cast<const t_type*>(data.get());
		}

		template <class t_type>
		const t_type* 		get(IUniverse* universe, const std::string& dataId, DataInfo* dinfo) {
			DataPtr data = this->getDataPtr(universe, dataId, dinfo);

			if (!data)
				return nullptr;

			return dynamic_cast<const t_type*>(data.get());
		}

		//////////////////////////////////////////////////////////////////////////
		/// SCRIPT/PYTHON - DO NOT USE OTHERWISE
		//////////////////////////////////////////////////////////////////////////
		template <typename T>
		inline std::vector<T*> genericAllData(IUniverse* universe, const std::string& dataId, DataInfo* dinfo) {
			DataPtrVec data = getAllDataPtr(universe, dataId, dinfo);
			std::vector<T*> ret(data.size());
			for (size_t i = 0; i < data.size(); i++) {
				auto* fd = dynamic_cast<T*>(data[i].get());
				ret[i] = fd;
			}

			return ret;
		}

		inline pesa::Data*				dataPtr(IUniverse* u, const std::string& dataId, bool noCacheUpdate = false, pesa::Metadata* metadata = nullptr, bool onlyCheckCached = false) {
			return const_cast<Data*>(get<Data>(u, dataId, noCacheUpdate, metadata, onlyCheckCached));
		}

		inline pesa::FloatMatrixData* 	floatData(IUniverse* u, const std::string& dataId, bool noCacheUpdate = false, pesa::Metadata* metadata = nullptr, bool onlyCheckCached = false) { 
			return const_cast<FloatMatrixData*>(get<FloatMatrixData>(u, dataId, noCacheUpdate, metadata, onlyCheckCached));
		}

		inline std::vector<pesa::FloatMatrixData*> floatDataAll(IUniverse* u, const std::string& dataId, DataInfo* dinfo = nullptr) {
			return genericAllData<FloatMatrixData>(u, dataId, dinfo);
		}

		inline pesa::StringData* 		stringData(IUniverse* u, const std::string& dataId, bool noCacheUpdate = false, pesa::Metadata* metadata = nullptr, bool onlyCheckCached = false) {
			return const_cast<StringData*>(get<StringData>(u, dataId, noCacheUpdate, metadata, onlyCheckCached));
		}

		inline std::vector<pesa::StringData*> stringDataAll(IUniverse* u, const std::string& dataId, DataInfo* dinfo = nullptr) {
			return genericAllData<StringData>(u, dataId, dinfo);
		}

		inline pesa::FloatVarMatrixData* floatVarData(IUniverse* u, const std::string& dataId, bool noCacheUpdate = false, pesa::Metadata* metadata = nullptr, bool onlyCheckCached = false) {
			return const_cast<FloatVarMatrixData*>(get<FloatVarMatrixData>(u, dataId, noCacheUpdate, metadata, onlyCheckCached));
		}

		inline std::vector<pesa::FloatVarMatrixData*> floatVarDataAll(IUniverse* u, const std::string& dataId, DataInfo* dinfo = nullptr) {
			return genericAllData<FloatVarMatrixData>(u, dataId, dinfo);
		}

		inline pesa::DoubleVarMatrixData* doubleVarData(IUniverse* u, const std::string& dataId, bool noCacheUpdate = false, pesa::Metadata* metadata = nullptr, bool onlyCheckCached = false) {
			return const_cast<DoubleVarMatrixData*>(get<DoubleVarMatrixData>(u, dataId, noCacheUpdate, metadata, onlyCheckCached));
		}

		inline std::vector<pesa::DoubleVarMatrixData*> doubleVarDataAll(IUniverse* u, const std::string& dataId, DataInfo* dinfo = nullptr) {
			return genericAllData<DoubleVarMatrixData>(u, dataId, dinfo);
		}

		inline pesa::IntVarMatrixData* 	intVarData(IUniverse* u, const std::string& dataId, bool noCacheUpdate = false, pesa::Metadata* metadata = nullptr, bool onlyCheckCached = false) {
			return const_cast<IntVarMatrixData*>(get<IntVarMatrixData>(u, dataId, noCacheUpdate, metadata, onlyCheckCached));
		}

		inline std::vector<pesa::IntVarMatrixData*> intVarDataAll(IUniverse* u, const std::string& dataId, DataInfo* dinfo = nullptr) {
			return genericAllData<IntVarMatrixData>(u, dataId, dinfo);
		}

		inline pesa::Int64VarMatrixData* 	int64VarData(IUniverse* u, const std::string& dataId, bool noCacheUpdate = false, pesa::Metadata* metadata = nullptr, bool onlyCheckCached = false) {
			return const_cast<Int64VarMatrixData*>(get<Int64VarMatrixData>(u, dataId, noCacheUpdate, metadata, onlyCheckCached));
		}

		inline std::vector<pesa::Int64VarMatrixData*> int64VarDataAll(IUniverse* u, const std::string& dataId, DataInfo* dinfo = nullptr) {
			return genericAllData<Int64VarMatrixData>(u, dataId, dinfo);
		}

		inline pesa::DoubleMatrixData* 	doubleData(IUniverse* u, const std::string& dataId, bool noCacheUpdate = false, pesa::Metadata* metadata = nullptr, bool onlyCheckCached = false) {
			return const_cast<DoubleMatrixData*>(get<DoubleMatrixData>(u, dataId, noCacheUpdate, metadata, onlyCheckCached));
		}

		inline std::vector<pesa::DoubleMatrixData*> doubleDataAll(IUniverse* u, const std::string& dataId, DataInfo* dinfo = nullptr) {
			return genericAllData<DoubleMatrixData>(u, dataId, dinfo);
		}

		inline pesa::IntMatrixData* 	intData(IUniverse* u, const std::string& dataId, bool noCacheUpdate = false, pesa::Metadata* metadata = nullptr, bool onlyCheckCached = false) {
			return const_cast<IntMatrixData*>(get<IntMatrixData>(u, dataId, noCacheUpdate, metadata, onlyCheckCached));
		}

		inline std::vector<pesa::IntMatrixData*> intDataAll(IUniverse* u, const std::string& dataId, DataInfo* dinfo = nullptr) {
			return genericAllData<IntMatrixData>(u, dataId, dinfo);
		}

		inline pesa::UIntMatrixData* 	uintData(IUniverse* u, const std::string& dataId, bool noCacheUpdate = false, pesa::Metadata* metadata = nullptr, bool onlyCheckCached = false) {
			return const_cast<UIntMatrixData*>(get<UIntMatrixData>(u, dataId, noCacheUpdate, metadata, onlyCheckCached));
		}

		inline std::vector<pesa::UIntMatrixData*> uintDataAll(IUniverse* u, const std::string& dataId, DataInfo* dinfo = nullptr) {
			return genericAllData<UIntMatrixData>(u, dataId, dinfo);
		}

		inline pesa::LongMatrixData* 	longData(IUniverse* u, const std::string& dataId, bool noCacheUpdate = false, pesa::Metadata* metadata = nullptr, bool onlyCheckCached = false) {
			return const_cast<LongMatrixData*>(get<LongMatrixData>(u, dataId, noCacheUpdate, metadata, onlyCheckCached));
		}

		inline std::vector<pesa::LongMatrixData*> longDataAll(IUniverse* u, const std::string& dataId, DataInfo* dinfo = nullptr) {
			return genericAllData<LongMatrixData>(u, dataId, dinfo);
		}

		inline pesa::ULongMatrixData* 	ulongData(IUniverse* u, const std::string& dataId, bool noCacheUpdate = false, pesa::Metadata* metadata = nullptr, bool onlyCheckCached = false) {
			return const_cast<ULongMatrixData*>(get<ULongMatrixData>(u, dataId, noCacheUpdate, metadata, onlyCheckCached));
		}

		inline std::vector<pesa::ULongMatrixData*> ulongDataAll(IUniverse* u, const std::string& dataId, DataInfo* dinfo = nullptr) {
			return genericAllData<ULongMatrixData>(u, dataId, dinfo);
		}

		inline pesa::Int64MatrixData* 	int64Data(IUniverse* u, const std::string& dataId, bool noCacheUpdate = false, pesa::Metadata* metadata = nullptr, bool onlyCheckCached = false) {
			return const_cast<Int64MatrixData*>(get<Int64MatrixData>(u, dataId, noCacheUpdate, metadata, onlyCheckCached));
		}

		inline pesa::Int64MatrixData* 	int64Data(IUniverse* u, const std::string& dataId, DataInfo* dinfo) {
			return const_cast<Int64MatrixData*>(get<Int64MatrixData>(u, dataId, dinfo));
		}

		inline std::vector<pesa::Int64MatrixData*> int64DataAll(IUniverse* u, const std::string& dataId, DataInfo* dinfo = nullptr) {
			return genericAllData<Int64MatrixData>(u, dataId, dinfo);
		}

		inline pesa::ShortMatrixData* 	int16Data(IUniverse* u, const std::string& dataId, bool noCacheUpdate = false, pesa::Metadata* metadata = nullptr, bool onlyCheckCached = false) {
			return const_cast<ShortMatrixData*>(get<ShortMatrixData>(u, dataId, noCacheUpdate, metadata, onlyCheckCached));
		}

		inline pesa::UShortMatrixData* 	uint16Data(IUniverse* u, const std::string& dataId, bool noCacheUpdate = false, pesa::Metadata* metadata = nullptr, bool onlyCheckCached = false) {
			return const_cast<UShortMatrixData*>(get<UShortMatrixData>(u, dataId, noCacheUpdate, metadata, onlyCheckCached));
		}

		//////////////////////////////////////////////////////////////////////////

		////////////////////////////////////////////////////////////////////////////
		/// Inline functions
		////////////////////////////////////////////////////////////////////////////
	};

	typedef std::shared_ptr<IDataRegistry> IDataRegistryPtr;

} /// namespace pesa

#endif /// ENGINE_DATAREGISTRY_H_
