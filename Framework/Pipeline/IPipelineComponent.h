/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// IComponent.h
///
/// Created on: 4 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#ifndef ENGINE_PIPELINE_COMPONENT_H_
#define ENGINE_PIPELINE_COMPONENT_H_

#include "Framework/FrameworkDefs.h"
#include "Framework/Components/ComponentDefs.h"
#include "Framework/Config/Config.h"

namespace pesa {

	class IPipelineHandler;
	class IDataRegistry;

	////////////////////////////////////////////////////////////////////////////
	/// IPipelineComponent - All backtest components must derive from it
	////////////////////////////////////////////////////////////////////////////
	class IPipelineComponent {
	protected:
		IPipelineHandler* 			m_pipelineHandler = nullptr;	/// The pipeline handler
		const ConfigSection& 		m_config;						/// The config section for this component
		IDataRegistry* 				m_dataRegistry = nullptr;		/// The data registry
		std::string 				m_logChannel;

	public:
		IPipelineComponent(const ConfigSection& config, const std::string& logChannel)
			: m_config(config)
			, m_logChannel(logChannel) {
		}

		virtual ~IPipelineComponent() {
		}

		virtual void 				prepare(RuntimeData	& rt) {}
		virtual void 				tick(const RuntimeData& rt) = 0;
		virtual void 				finalise(RuntimeData& rt) {}

		////////////////////////////////////////////////////////////////////////////
		/// Inline functions
		////////////////////////////////////////////////////////////////////////////
		IPipelineHandler*& 			pipelineHandler() 			{ return m_pipelineHandler; }
		const IPipelineHandler* 	pipelineHandler() const 	{ return m_pipelineHandler; }

		inline const Config* 		parentConfig() const		{ return m_config.config(); }
		inline const ConfigSection& config() const 				{ return m_config; 		 }

		template <typename t_type>
		inline const t_type& 		config() const 				{ return *(reinterpret_cast<const t_type*>(&m_config)); }
		inline const IDataRegistry*	dataRegistry() const		{ return m_dataRegistry; }
		inline IDataRegistry*&		dataRegistry()				{ return m_dataRegistry; }

		inline void					setLogChannel(const std::string& logChannel) { m_logChannel = logChannel; }
		inline std::string 			getLogChannel() { return m_logChannel; }

		//		inline const RuntimeData& 	runtimeData() const			{ return m_pipelineHandler->runtimeData(); }

		template <typename... Args>
		inline void trace(const std::string& message, Args... args) const {
			if (Poco::Logger::root().getLevel() < Poco::Message::PRIO_TRACE)
				return;
			Poco::Logger::root().trace(Util::tsstr() + "[" + m_logChannel + "] " + message, args...);
		}

		template <typename... Args>
		inline void debug(const std::string& message, Args... args) const {
			if (Poco::Logger::root().getLevel() < Poco::Message::PRIO_DEBUG)
				return;
			Poco::Logger::root().debug(Util::tsstr() + "[" + m_logChannel + "] " + message, args...);
		}

		template <typename... Args>
		inline void info(const std::string& message, Args... args) const {
			Poco::Logger::root().information(Util::tsstr() + "[" + m_logChannel + "] " + message, args...);
		}

		template <typename... Args>
		inline void warning(const std::string& message, Args... args) const {
			Poco::Logger::root().warning(Util::tsstr() + "[" + m_logChannel + "] " + message, args...);
		}

		template <typename... Args>
		inline void error(const std::string& message, Args... args) const {
			Poco::Logger::root().error(Util::tsstr() + "[" + m_logChannel + "] " + message, args...);
		}

		template <typename... Args>
		inline void critical(const std::string& message, Args... args) const {
			Poco::Logger::root().critical(Util::tsstr() + "[" + m_logChannel + "] " + message, args...);
		}
	};

	typedef std::shared_ptr<IPipelineComponent> IPipelineComponentPtr;
	
#define PTrace 		IPipelineComponent::trace
#define PDebug 		IPipelineComponent::debug
#define PInfo 		IPipelineComponent::info
#define PWarning 	IPipelineComponent::warning
#define PError 		IPipelineComponent::error
#define PCritical	IPipelineComponent::critical

} /// namespace pesa

#endif /// ENGINE_PIPELINE_COMPONENT_H_
