/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// IComponent.h
///
/// Created on: 4 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/FrameworkDefs.h"
#include "IDataRegistry.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// IPipelineHandler - Handles and executes the pipeline
	////////////////////////////////////////////////////////////////////////////
	class IPipelineHandler {
	public:
		virtual const pesa::Config*			config() const = 0;
		virtual const pesa::RuntimeData&	runtimeData() const = 0;
		virtual pesa::IPipelineComponentPtr component(const std::string& id) = 0;
		virtual pesa::IDataRegistry*		dataRegistry() = 0;

		virtual float						getStatus() const = 0;

#ifndef __SCRIPT__
		virtual void						queueConfig(ConfigPtr config) = 0;
#endif /// __SCRIPT__
	};
} /// namespace pesa

