/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Stream.cpp
///
/// Created on: 2 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "MongoClient.h"
#include "Core/Lib/Util.h"

namespace pesa {
namespace io {
	using namespace bsoncxx;

	//////////////////////////////////////////////////////////////////////////
	/// MongoClient
	//////////////////////////////////////////////////////////////////////////
	bool MongoClient::s_isAvailable = false;
	bool MongoClient::s_availabilityChecked = false;
	mongocxx::instance MongoClient::s_instMongo;
	std::mutex MongoClient::s_mutex;

	MongoClient::~MongoClient() {
		delete m_clMongo;
	}

	MongoClient::MongoClient(const char* dbName, const char* dbPath) {
		if (dbName)
			m_dbName = dbName;
		if (dbPath)
			m_dbPath = dbPath;

		init();
	}

	void MongoClient::init() {
		AUTO_LOCK(m_mutex);

		/// The client has already been initialised
		if (m_clMongo)
			return;

		AppOptions appOptions = Application::instance()->appOptions();

		//m_instMongo = new mongocxx::instance{};
		auto dbPath = appOptions.dbPath;

		if (!m_dbPath.empty())
			dbPath = m_dbPath;
		
		/// TODO: Get the URI path from the appOptions
		std::string suri = !appOptions.dbPath.empty() ? appOptions.dbPath : mongocxx::uri::k_default_uri;
		const auto uri = mongocxx::uri{ suri };
		mongocxx::options::client clientOptions;

		if (uri.ssl()) {
			mongocxx::options::ssl sslOptions;
			// NOTE: To test SSL, you may need to set options. The following
			// would enable certificates for Homebrew OpenSSL on OS X.
			// options.ca_file("/usr/local/etc/openssl/cert.pem");
			// ssl_options.ca_file("/usr/local/etc/openssl/cert.pem");
			clientOptions.ssl_opts(sslOptions);
		}

		m_clMongo = new MCxxClient{ uri, clientOptions };
	}

	MCxxClient& MongoClient::cl() {
		return *m_clMongo;
	}

	std::string MongoClient::defaultDBName() {
		return "pesa";
	}

	bool MongoClient::checkAvailability() {
		auto& cl = this->cl();

		if (!cl)
			return false;

		if (m_availabilityChecked)
			return m_isAvailable;

		m_availabilityChecked = true;
		m_isAvailable = false;

		try {
			bsoncxx::builder::stream::document ping;
			ping << "ping" << 1;
			auto db = cl[defaultDBName()];
			auto name = db.name(); 
			auto result = db.run_command(ping.view());

			if (result.view()["ok"].get_double() != 1)
				return false;

			m_isAvailable = true;
		}
		catch (std::exception&) {
			return false;
		}

		return m_isAvailable;
	}

	bool MongoClient::isAvailable() {
		AUTO_LOCK(s_mutex);

		if (!s_availabilityChecked) {
			MongoClient cl;
			s_isAvailable = cl.checkAvailability();
			s_availabilityChecked = true;
			return s_isAvailable;
		}

		return s_isAvailable;
	}

	MongoDatabase MongoClient::db(const char* dbName /* = nullptr */) {
		if (!dbName && !m_dbName.empty())
			dbName = m_dbName.c_str();

		return (*m_clMongo)[resolveDBName(dbName)];
	}

	MongoCollection MongoClient::coll(const std::string& collName, const char* dbName /* = nullptr */) {
		if (!dbName && !m_dbName.empty())
			dbName = m_dbName.c_str();

		auto db = this->db(dbName);
		return db[collName];
	}
} /// namespace io
} /// namespace pesa 
