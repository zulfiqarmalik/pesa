/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// MongoClient.h
///
/// Created on: 19 Apr 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Core/IO/Stream.h"
#include "Helper/HelperDefs.h"
#include "Core/Lib/Application.h"

#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/json.hpp>
#include <bsoncxx/stdx/make_unique.hpp>
#include <bsoncxx/types.hpp>
#include <bsoncxx/builder/basic/array.hpp>
#include <bsoncxx/builder/basic/document.hpp>
#include <bsoncxx/builder/basic/kvp.hpp>
#include <bsoncxx/document/value.hpp>
#include <bsoncxx/document/view.hpp>
#include <bsoncxx/stdx/string_view.hpp>

#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
#include <mongocxx/logger.hpp>
#include <mongocxx/options/client.hpp>
#include <mongocxx/uri.hpp>

#include <boost/optional/optional.hpp>

namespace pesa {
namespace io {

	using bsoncxx::builder::stream::document;
	using bsoncxx::builder::stream::open_document;
	using bsoncxx::builder::stream::close_document;
	using bsoncxx::builder::stream::open_array;
	using bsoncxx::builder::stream::close_array;
	using bsoncxx::builder::stream::array_context;
	using bsoncxx::builder::stream::finalize;
	using bsoncxx::builder::concatenate;
	using bsoncxx::builder::basic::kvp;
	using bsoncxx::builder::basic::sub_array;
	using bsoncxx::builder::basic::sub_document;
	using bsoncxx::builder::basic::sub_array;

    typedef mongocxx::stdx::optional<bsoncxx::document::value> BsonOptDocumentValue;
	typedef bsoncxx::document::value			BsonValue;
	typedef bsoncxx::builder::stream::document	BsonDocumentBuilder;
	typedef mongocxx::client					MCxxClient;
	typedef mongocxx::database					MongoDatabase;
	typedef mongocxx::collection				MongoCollection;
	typedef bsoncxx::document::view				BsonDocumentView;
	typedef bsoncxx::document::element			BsonDocumentElement;
	typedef std::vector<BsonDocumentElement> 	BsonDocumentElementVec;
	typedef bsoncxx::document::value 			BsonDocumentValue;
	typedef bsoncxx::oid  						ObjectId;

	//////////////////////////////////////////////////////////////////////////
	/// MongoClient - Simple wrapper around mongo client
	//////////////////////////////////////////////////////////////////////////
	class Helper_API MongoClient {
	private:
		static bool				s_isAvailable; 
		static bool				s_availabilityChecked;
		static std::mutex 		s_mutex;				/// Data mutex

		static mongocxx::instance s_instMongo;			/// The Mongo instance

		MCxxClient*				m_clMongo = nullptr;	/// The underlying mongo client object
		bool					m_isAvailable = false;	/// Check whether the client is available or not
		bool					m_availabilityChecked = false; /// Whether the availability of the mongo db server has been checked or not
		std::mutex 				m_mutex;				/// Data mutex
		std::string				m_dbName;				/// The DB Name override
		std::string				m_dbPath;				/// The URL of the DB
		void					init();

		bool					checkAvailability();

	public:
								MongoClient(const char* dbName = nullptr, const char* dbPath = nullptr);			/// No instantiating
								~MongoClient();			/// No instantiating

		MCxxClient&				cl();
		MongoDatabase			db(const char* dbName = nullptr);
		MongoCollection			coll(const std::string& collName, const char* dbName = nullptr);

		static bool				isAvailable();
		static std::string		defaultDBName();

		//////////////////////////////////////////////////////////////////////////
		/// Static functions
		//////////////////////////////////////////////////////////////////////////

		//////////////////////////////////////////////////////////////////////////
		/// Inline functions
		//////////////////////////////////////////////////////////////////////////
		static inline std::string resolveDBName(const char* dname) {
			std::string appDBName = Application::instance()->appOptions().dbName;
			if (!dname)
				return appDBName;

			std::string dbName = dname;

			if (dbName == appDBName || dbName.empty())
				return appDBName;

			std::string prefix = appDBName + "_";

			/// If the string already starts with the app db name then don't do anything about it 
			if (dbName.find(prefix) == 0)
				return dbName;

			/// Otherwise we concatename the new name with the app DB name. This is the convention of the entire system
			return prefix + dbName;
		}
	}; 

	typedef std::shared_ptr<MongoClient> MongoClientPtr;
} /// namespace io
} /// namespace pesa 

