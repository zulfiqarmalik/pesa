/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// MongoFileStream.cpp
///
/// Created on: 19 Apr 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "./MongoFileStream.h"
#include "./MongoStream.h"
#include <sstream>
#include <fstream>

#include <bsoncxx/json.hpp>
#include <bsoncxx/stdx/make_unique.hpp>
#include <mongocxx/client.hpp>
#include <mongocxx/gridfs/bucket.hpp>
#include <mongocxx/instance.hpp>
#include <mongocxx/uri.hpp>

#include "Poco/Base64Encoder.h"
#include "Poco/Base64Decoder.h"
#include "Poco/DeflatingStream.h"
#include "Poco/BinaryWriter.h"
#include "Poco/BinaryReader.h"
#include "Poco/FileStream.h"

namespace pesa {
namespace io {
	using namespace bsoncxx;

	MongoFileSettings::MongoFileSettings(const std::string& id, const char* collName, const char* dbName /* = nullptr */) {
		this->dbName	= MongoClient::resolveDBName(dbName);
		this->collName	= collName;
		this->id		= id;
	}

	MongoFileSettings::MongoFileSettings(const std::string& id, const char* collName, const char* dbName, bool useGridFS)
		: MongoFileSettings(id, collName, dbName) {
		this->useGridFS = useGridFS;
	}

	//////////////////////////////////////////////////////////////////////////
	/// MongoFileInputStream
	//////////////////////////////////////////////////////////////////////////
	MongoFileInputStream::MongoFileInputStream(const std::string& id, const char* collName, bool useGridFS, const char* dbName /* = nullptr */)
		: BasicInputStream()
		, m_settings(id, collName, dbName, useGridFS) {
		init();
	}

	MongoFileInputStream::MongoFileInputStream(const std::string& id, const char* collName, const char* dbName /* = nullptr */)
		: MongoFileInputStream(id, collName, false, dbName) {
	}

	MongoFileInputStream::MongoFileInputStream(const MongoFileSettings& settings)
		: BasicInputStream()
		, m_settings(settings) {
		init();
	}

	MongoFileInputStream::~MongoFileInputStream() {
	}

	void MongoFileInputStream::init() {
		if (m_settings.useGridFS)
			initFromGridFS();
		else
			initFromColl();
	}

	void MongoFileInputStream::initFromColl() {
		try {
			InputMongoStream mstream(m_settings.collName, m_settings.id, m_settings.dbName.c_str());

			if (!mstream.isValid()) {
				m_fileLength = 0;
				return;
			}

			uint8_t* data = nullptr;
			mstream.read("data", &data, m_fileLength);

			if (!m_fileLength) {
				delete[] data;
				return;
			}

			const auto& interFilename = m_settings.interFile.path();
			auto os = std::make_shared<Poco::FileOutputStream>(interFilename, std::ios::out | std::ios::trunc | std::ios::binary);

			os->write((const char*)data, m_fileLength);
			os->flush();
			os = nullptr;
			delete[] data;

			m_stream = std::make_shared<Poco::FileInputStream>(interFilename, std::ios::in | std::ios::binary);
		}
		catch (const std::exception&) {
			m_fileLength = 0;
		}
	}
	
	void MongoFileInputStream::initFromGridFS() {
		try {
			const auto& interFilename = m_settings.interFile.path();

			MongoClient* cl = nullptr;
			MongoClientPtr scl = nullptr;

			if (m_settings.mongoCl)
				cl = m_settings.mongoCl;
			else {
				scl = std::make_shared<MongoClient>(m_settings.dbName.c_str());
				cl = scl.get();
			}

			bsoncxx::types::value dbId(types::b_utf8{ m_settings.id });
			auto coll		= cl->cl()[m_settings.filename()];
			auto bucket		= coll.gridfs_bucket();
			auto downloader = bucket.open_download_stream(dbId);
			auto fileLength = downloader.file_length();
			auto bufferSize = std::min(fileLength, static_cast<std::int64_t>(downloader.chunk_size()));
			auto buffer		= std::make_unique<std::uint8_t[]>(static_cast<std::size_t>(bufferSize));
			auto os			= std::make_shared<Poco::FileOutputStream>(interFilename, std::ios::out | std::ios::trunc | std::ios::binary);
			size_t lengthRead = 0;
			size_t numRead	= 0;

			do {
				lengthRead = downloader.read(buffer.get(), static_cast<std::size_t>(bufferSize));
				if (lengthRead)
					os->write((const char*)buffer.get(), lengthRead);
				numRead += static_cast<std::int32_t>(lengthRead);
			} while (lengthRead);

			os->flush();
			os = nullptr;

			m_stream = std::make_shared<Poco::FileInputStream>(interFilename, std::ios::in | std::ios::binary);
			m_fileLength = numRead;
		}
		catch (const std::exception&) {
			m_fileLength = 0;
		}
	}

	void MongoFileInputStream::flush() {
	}

	//////////////////////////////////////////////////////////////////////////
	/// MongoFileOutputStream
	//////////////////////////////////////////////////////////////////////////
	MongoFileOutputStream::MongoFileOutputStream(const std::string& id, const char* collName, bool useGridFS, const char* dbName /* = nullptr */)
		: BasicOutputStream()
		, m_settings(id, collName, dbName, useGridFS) {
		init();
	}

	MongoFileOutputStream::MongoFileOutputStream(const std::string& id, const char* collName, const char* dbName /* = nullptr */)
		: MongoFileOutputStream(id, collName, false, dbName) {
	}

	MongoFileOutputStream::MongoFileOutputStream(const MongoFileSettings& settings)
		: BasicOutputStream()
		, m_settings(settings) {
		init();
	}

	MongoFileOutputStream::~MongoFileOutputStream() {
	}

	void MongoFileOutputStream::init() {
		const auto& interFilename = m_settings.interFile.path();
		m_stream = std::make_shared<Poco::FileOutputStream>(interFilename, std::ios::out | std::ios::trunc | std::ios::binary);
	}

	void MongoFileOutputStream::flushColl() {
		const auto& interFilename = m_settings.interFile.path();

		/// Close the stream
		m_stream->flush();
		m_stream = nullptr;

		MongoClient* cl = nullptr;
		MongoClientPtr scl = nullptr;

		if (m_settings.mongoCl)
			cl = m_settings.mongoCl;
		else {
			scl = std::make_shared<MongoClient>(m_settings.dbName.c_str());
			cl = scl.get();
		}

		size_t fileLength = 0;
		char* fileContents = Util::readEntireFile(m_settings.interFile.path(), fileLength);

		if (!fileContents || !fileLength)
			return;

		io::OutputMongoStream os(m_settings.collName, m_settings.id, nullptr, m_settings.dbName.c_str());

		os.isUpdate() = false;
		os.replace() = true;

		os.write("data", (const void*)fileContents, fileLength);

		os.flush();
	}

	void MongoFileOutputStream::flushGridFS() {
		const auto& interFilename = m_settings.interFile.path();

		/// Close the stream
		m_stream->flush();
		m_stream = nullptr;

		MongoClient* cl = nullptr;
		MongoClientPtr scl = nullptr;

		if (m_settings.mongoCl)
			cl = m_settings.mongoCl;
		else {
			scl = std::make_shared<MongoClient>(m_settings.dbName.c_str());
			cl = scl.get();
		}

		bsoncxx::types::value dbId(types::b_utf8{ m_settings.id });
		auto coll			= cl->cl()[m_settings.filename()];
		auto bucket			= coll.gridfs_bucket();
		auto uploader		= bucket.open_upload_stream_with_id(dbId, m_settings.id);
		size_t fileLength	= 0;
		char* fileContents	= Util::readEntireFile(m_settings.interFile.path(), fileLength);

		/// Before we write out ... we wanna delete the existing file ... this is because GridFS doesn't really support update operation
		auto cursor = bucket.find(document{} << "_id" << dbId << finalize);
		if (cursor.begin() != cursor.end())
			bucket.delete_file(dbId);

		uploader.write((const uint8_t*)fileContents, fileLength);
		uploader.close();
	}

	void MongoFileOutputStream::flush() {
		if (m_settings.useGridFS)
			flushGridFS();
		else
			flushColl();
	}
} /// namespace io 
} /// namespace pesa 
