/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// MongoFileStream.h
///
/// Created on: 19 Apr 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "../HelperDefs.h"
#include "./MongoClient.h"
#include "Core/IO/BasicStream.h"
#include  "Poco/TemporaryFile.h"

namespace pesa {
namespace io {

	struct MongoFileSettings {
		std::string				dbName;				/// The name of the DB that this file belongs to 
		std::string				collName;			/// The name of the collection that we're going to use
		std::string				id;					/// The actual fileid
		Poco::TemporaryFile		interFile;			/// The intermediate file that we use for reading
		bool					useGridFS = true;	/// Write binary data in the stream 
		MongoClient*			mongoCl = nullptr;	/// The mongo client

								MongoFileSettings(const std::string& id, const char* collName, const char* dbName);
								MongoFileSettings(const std::string& id, const char* collName, const char* dbName, bool useGridFS);

		inline std::string		filename() const {
			return dbName + "_" + collName;
		}
	};

	////////////////////////////////////////////////////////////////////////////
	/// MongoFileInputSteam: Mongo GridFS InputStream
	////////////////////////////////////////////////////////////////////////////
	class Helper_API MongoFileInputStream : public BasicInputStream {
	private:
		MongoFileSettings		m_settings;			/// The settings 
		size_t					m_fileLength = 0;	/// The length of the file

		void					init();
		void					initFromGridFS();
		void					initFromColl();

	public:
								MongoFileInputStream(const std::string& id, const char* collName, const char* dbName = nullptr);
								MongoFileInputStream(const std::string& id, const char* collName, bool useGridFS, const char* dbName = nullptr);
								MongoFileInputStream(const MongoFileSettings& settings);
		virtual 				~MongoFileInputStream();

		virtual void 			flush();

		//////////////////////////////////////////////////////////////////////////
		/// Inline functions
		//////////////////////////////////////////////////////////////////////////
		bool					isValid() const { return m_fileLength > 0; }
		inline MongoFileSettings& settings() { return m_settings; }
	};

	////////////////////////////////////////////////////////////////////////////
	/// MongoFileOutputStream: Mongo GridFS OutputStream
	////////////////////////////////////////////////////////////////////////////
	class Helper_API MongoFileOutputStream : public BasicOutputStream {
	private:
		MongoFileSettings		m_settings;			/// The settings 

		void					init();
		void					flushGridFS();
		void					flushColl();

	public:
								MongoFileOutputStream(const std::string& id, const char* collName, const char* dbName = nullptr);
								MongoFileOutputStream(const std::string& id, const char* collName, bool useGridFS, const char* dbName = nullptr);
								MongoFileOutputStream(const MongoFileSettings& settings);
		virtual 				~MongoFileOutputStream();

		virtual void 			flush();

		//////////////////////////////////////////////////////////////////////////
		/// Inline functions
		//////////////////////////////////////////////////////////////////////////
		inline MongoClient*&	mongoCl() { return m_settings.mongoCl; }
		inline MongoFileSettings& settings() { return m_settings; }
	};


} /// namespace io 
} /// namespace pesa 

