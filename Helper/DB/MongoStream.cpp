/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Stream.cpp
///
/// Created on: 2 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "MongoStream.h"
#include "Core/Lib/Util.h"

namespace pesa {
namespace io {
	using namespace bsoncxx;
	////////////////////////////////////////////////////////////////////////////
	/// InputMongoStream: Input MongoStreams
	////////////////////////////////////////////////////////////////////////////
	InputMongoStream::InputMongoStream(BsonDocumentView view)
		: m_view(view)
		, m_isValid(true) {
	}

	InputMongoStream::InputMongoStream(const std::string& collName, const std::string& _id, const char* dbName) {
		MongoClient mongoCl(dbName);
		initFromID(mongoCl, collName, "_id", _id);
	}

	InputMongoStream::InputMongoStream(const std::string& collName, const ObjectId& _id, const char* dbName) {
		MongoClient mongoCl(dbName);
		initFromID(mongoCl, collName, "_id", _id);
	}

	InputMongoStream::InputMongoStream(MongoClient& mongoCl, const std::string& collName, const std::string& _id) {
		initFromID(mongoCl, collName, "_id", _id);
	}

	InputMongoStream::InputMongoStream(MongoClient& mongoCl, const std::string& collName, const ObjectId& _id) {
		initFromID(mongoCl, collName, "_id", _id);
	}

	InputMongoStream::InputMongoStream(const std::string& json) {
		/// Load from JSON
		bsoncxx::from_json(json);
	}

	InputMongoStream::InputMongoStream(BsonOptDocumentValue result) {
		m_result = result;

		if (m_result) {
			m_view = *m_result;
			m_isValid = true;
		}
	}

	InputMongoStream::~InputMongoStream() {
		ASSERT(m_eltStack.size() == 0, "Invalid view stack size. You did not traverse the document correctly. Expecting stack size to be 0, actual size: " << m_eltStack.size());
		m_eltStack.clear();
	}

	bool InputMongoStream::isValid() const {
		return m_isValid;
	}

	InputMongoStream& InputMongoStream::read(const char* name, uint8_t** data, size_t& size) {
		auto elt = this->elt(name);

		if (elt) {
			auto binData = elt.get_binary();
			size = binData.size;
			*data = new uint8_t[size];
			memcpy((void*)(*data), (const void*)binData.bytes, size * sizeof(uint8_t));
		}

		return *this;
	}

	InputStream& InputMongoStream::read(const char* name, size_t* value) {
		auto elt = this->elt(name);
		if (elt)
			*value = (size_t)elt.get_int64().value;
		return *this;
	}

	InputStream& InputMongoStream::read(const char* name, unsigned int* value) {
		auto elt = this->elt(name);
		if (elt)
			*value = (unsigned int)elt.get_int64().value;
		return *this;
	}

	InputStream& InputMongoStream::read(const char* name, int* value) {
		auto elt = this->elt(name);
		if (elt)
			*value = elt.get_int32().value;
		return *this;
	}

	InputStream& InputMongoStream::read(const char* name, int64_t* value) {
		auto elt = this->elt(name);
		if (elt)
			*value = (int64_t)elt.get_int64().value;
		return *this;
	}

	void InputMongoStream::read(const char* name, ObjectId* oid) {
		auto elt = this->elt(name);
		if (elt)
			*oid = elt.get_oid().value;
	}

	InputStream& InputMongoStream::read(const char* name, float* value) {
		auto elt = this->elt(name);
		if (elt)
			*value = (float)elt.get_double().value;
		return *this;
	}

	InputStream& InputMongoStream::read(const char* name, double* value) {
		auto elt = this->elt(name);
		if (elt)
			*value = elt.get_double().value;
		return *this;
	}

	InputStream& InputMongoStream::read(const char* name, std::string* value) {
		auto elt = this->elt(name);
		if (elt)
			*value = elt.get_utf8().value.to_string();
		return *this;
	}

	InputStream& InputMongoStream::read(const char* name, bool* value) {
		auto elt = this->elt(name);
		if (elt)
			*value = elt.get_bool().value;
		return *this;
	}

	InputStream& InputMongoStream::read(const char* name, Poco::Timestamp* value) {
		int64_t milliseconds = 0;
		auto elt = this->elt(name);
		if (elt) 
			milliseconds = elt.get_date().value.count();
		*value = Poco::Timestamp(milliseconds * 1000);
		return *this;
	}

	InputStream& InputMongoStream::read(const char* name, Streamable* value) {
		if (pushElt(name)) {
			value->read(*this);
			popElt();
		}
		return *this;
	}

	size_t InputMongoStream::size(const char* name) {
		auto elt = this->elt(name);

		if (elt) {
			auto arr = elt.get_array().value;
			size_t count = 0;
			for (auto iter = arr.cbegin(); iter != arr.cend(); iter++)
				count++;
			return count;
		}

		return 0;
	}

	InputStream& InputMongoStream::read(const char* name, std::vector<int>* values) {
		auto elt = this->elt(name);

		if (elt) {
			auto arr = elt.get_array().value;
			if (!values->size())
				values->resize(size(name));

			for (size_t i = 0; i < values->size(); i++)
				(*values)[i] = arr[(uint32_t)i].get_int32().value;
		}

		return *this;
	}

	InputStream& InputMongoStream::read(const char* name, std::vector<float>* values) {
		auto elt = this->elt(name);

		if (elt) {
			auto arr = elt.get_array().value;
			if (!values->size())
				values->resize(size(name));

			for (size_t i = 0; i < values->size(); i++)
				(*values)[i] = (float)arr[(uint32_t)i].get_double().value;
		}

		return *this;
	}

	InputStream& InputMongoStream::read(const char* name, std::vector<std::string>* values) {
		auto elt = this->elt(name);

		if (elt) {
			auto arr = elt.get_array().value;
			if (!values->size())
				values->resize(size(name));

			for (size_t i = 0; i < values->size(); i++)
				(*values)[i] = arr[(uint32_t)i].get_utf8().value.to_string();
		}

		return *this;
	}

	InputStream& InputMongoStream::read(const char* name, std::vector<Streamable*>* values) {
		auto elt = pushElt(name);

		if (elt) {
			auto arr = elt.get_array().value;
			if (!values->size())
				values->resize(size(name));

			BsonDocumentView view = m_view;
			BsonDocumentElement elt = m_elt;
			m_elt = BsonDocumentElement();

			for (size_t i = 0; i < values->size(); i++) {
				auto arrElt = arr[(uint32_t)i];
				m_view = arrElt.get_document().value;
				//pushElt(arrElt.get_document().value); // BsonDocumentElement(arrElt.raw(), arrElt.length(), arrElt.offset()));
				values->at(i)->read(*this);
				//popElt();
			}

			m_view = view;
			m_elt = elt;

			popElt();
		}

		return *this;
	}

	BsonDocumentElement InputMongoStream::pushElt(BsonDocumentElement elt) {
		m_eltStack.push_back(m_elt);
		m_eltNameStack.push_back("<anon>");
		m_elt = elt;
		return m_elt;
	}

	BsonDocumentElement InputMongoStream::pushElt(const char* name) {
		BsonDocumentElement newElt;

		if (!m_elt)
			newElt = m_view[std::string(name)];
		else
			newElt = m_elt[std::string(name)];

		if (!newElt)
			return newElt;

		m_eltStack.push_back(m_elt);
		m_eltNameStack.push_back(name);

		m_elt = newElt;
		//ASSERT(m_elt, "No sub-element: " << name);

		return m_elt;
	}

	void InputMongoStream::popElt() {
		ASSERT(m_eltStack.size(), "Invalid popViwe call. Nothing in the stack!");
		m_elt = m_eltStack[m_eltStack.size() - 1];
		m_eltStack.pop_back();
		m_eltNameStack.pop_back();
	}

	void InputMongoStream::flush() {
	}

	////////////////////////////////////////////////////////////////////////////
	/// OutputMongoStream: Output MongoStreams
	////////////////////////////////////////////////////////////////////////////
	OutputMongoStream::OutputMongoStream(const std::string& collName, const char* dbName)
		: m_dbName(MongoClient::resolveDBName(dbName))
		, m_collName(collName) {
	}

	OutputMongoStream::OutputMongoStream(const std::string& collName, const std::string& _id, const char* prefix, const char* dbName)
		: m_dbName(MongoClient::resolveDBName(dbName))
		, m_collName(collName) 
		, m_prefix(prefix ? prefix : "")
		, m_isUpdate(true) {
		m_queryBuilder << "_id" << _id;
	}
	
	OutputMongoStream::OutputMongoStream(const std::string& collName, ObjectId& _id, const char* prefix, const char* dbName)
		: m_dbName(MongoClient::resolveDBName(dbName))
		, m_collName(collName) 
		, m_prefix(prefix ? prefix : "")
		, m_isUpdate(true) {
		m_queryBuilder << "_id" << _id;
	}

	OutputMongoStream::OutputMongoStream()
		: m_isUpdate(false) {
	}
	
	OutputMongoStream::~OutputMongoStream() {
		//ASSERT(m_prefixStack.size() == 0, "Invalid use of OutputMongoStream. Expecting prefix stack to be empty. Num elements: " << m_prefixStack.size());

		for (auto iter : m_updateMap)
			delete iter.second;
		m_updateMap.clear();
	}

	OutputStream& OutputMongoStream::write(const char* name, const size_t& value) {
		return writeGeneric<int64_t, types::b_int64>(name, (int64_t)value);
	}

	OutputStream& OutputMongoStream::write(const char* name, const bool& value) {
		return writeGeneric<bool, types::b_bool>(name, value);
	}

	OutputStream& OutputMongoStream::write(const char* name, const int& value) {
		return writeGeneric<int, types::b_int32>(name, value);
	}

	OutputStream& OutputMongoStream::write(const char* name, const unsigned int& value) {
		return writeGeneric<int64_t, types::b_int64>(name, (int64_t)value);
	}

	OutputStream& OutputMongoStream::write(const char* name, const int64_t& value) {
		return writeGeneric<int64_t, types::b_int64>(name, value);
	}

	OutputStream& OutputMongoStream::write(const char* name, io::ObjectId& oid) {
		if (!m_isUpdate)
			m_document << fullName(name) << oid;
		else
			updateDoc("$set") << fullName(name) << oid;
		return *this;
	}

	OutputStream& OutputMongoStream::write(const char* name, const void* buffer, size_t length) {
		types::b_binary binData;
		binData.size = (uint32_t)length;
		binData.bytes = (uint8_t*)buffer;

		document() << fullName(name) << binData;
		return *this;
	}

	OutputMongoStream& OutputMongoStream::pushBinaryData(const char* name, const void* buffer, size_t length) {
		//ASSERT(m_isUpdate, "Cannot use push methods unless updating the document!");

		pushPrefix(name);
		updateDoc("$push") << 
			m_prefix << open_document << 
				"$each" << open_array <<
					[&](array_context<> arr) {
						types::b_binary binData;
						binData.size = (uint32_t)length;
						binData.bytes = (uint8_t*)buffer;
						arr << binData;
					} 
				<< close_array
			<< close_document;
		popPrefix();

		return *this;
	}

	OutputStream& OutputMongoStream::write(const char* name, const float& value) {
		return writeGeneric<double, types::b_double>(name, (double)value);
	}

	OutputStream& OutputMongoStream::write(const char* name, const double& value) {
		return writeGeneric<double, types::b_double>(name, value);
	}

	OutputStream& OutputMongoStream::write(const char* name, const std::string& value) {
		return writeGeneric<std::string, types::b_utf8>(name, value);
	}

	OutputStream& OutputMongoStream::write(const char* name, Poco::Timestamp& value) {
		int64_t microseconds = value.epochMicroseconds();
		int64_t milliseconds = microseconds / 1000;
		return writeGeneric<std::chrono::milliseconds, types::b_date>(name, std::chrono::milliseconds(milliseconds));
	}

	OutputStream& OutputMongoStream::write(const char* name, const std::vector<int>& values) {
		return writeArray(name, values);
	}
	
	OutputStream& OutputMongoStream::write(const char* name, const std::vector<float>& values) {
		return writeArray(name, values);
	}
	
	OutputStream& OutputMongoStream::write(const char* name, const std::vector<std::string>& values) {
		return writeArray(name, values);
	}
	
	OutputStream& OutputMongoStream::write(const char* name, Streamable& value) {
		if (!m_isUpdate) {
			std::string sname = fullName(name);
			/// If we already have a "." in the name then this has been resolved by the caller

			StringVec prefixStack = m_prefixStack;
			std::string prefix = m_prefix;

			if (sname.find(".") != std::string::npos) {
				m_prefixStack.clear();
				m_prefix = "";
			}
			m_document << sname << open_document;
			value.write(*this);
			m_document << close_document;

			m_prefixStack = prefixStack;
			m_prefix = prefix;
		}
		else {
			//m_document << "$set" << open_document << 
			//	fullName(name) << open_document;
			//		OutputMongoStream os(m_collName, m_dbName.c_str());
			//		os.isUpdate() = m_isUpdate;
			//		value.write(os);
			//		m_document << concatenate(os.document().view());
			//	m_document << close_document;
			//m_document << close_document;

			pushPrefix(name);
			OutputMongoStream os(m_collName, m_dbName.c_str());
			os.isUpdate() = false;
			/// Only for the top level (deeper levels are referring to their parent!)
			if (m_prefixStack.size() == 1)
				os.setPrefixStack(m_prefixStack);
			value.write(os);
			updateDoc("$set") << concatenate(os.finalise().view());
			popPrefix();
		}
		return *this;
	}

	OutputStream& OutputMongoStream::write(const char* name, const std::vector<Streamable*>& values) {
		if (!m_isUpdate) {
			m_document << fullName(name) << open_array <<
				[&](array_context<> arr) {
					for (size_t i = 0; i < values.size(); i++) {
						OutputMongoStream os(m_collName, m_dbName.c_str());
						os.isUpdate() = m_isUpdate;
						values[i]->write(os);
						arr << concatenate(os.document().view());
					}
				} 
			<< close_array;
		}
		else {
			pushPrefix(name);
			updateDoc("$push") << 
				m_prefix << open_document << 
					"$each" << open_array <<
						/// We wanna write the child Streamable objects just normally ...
						[&](array_context<> arr) {
							for (size_t i = 0; i < values.size(); i++) {
								OutputMongoStream os(m_collName, m_dbName.c_str());
								os.isUpdate() = false;
								values[i]->write(os);
								arr << concatenate(os.document().view());
							}
						}
					<< close_array
				<< close_document;
			popPrefix();
		}
		
		return *this;
	}

	const std::string& OutputMongoStream::pushPrefix(const char* name) {
		m_prefixStack.push_back(std::string(name));
		m_prefix = Util::combine(m_prefixStack, ".");
		return m_prefix;
	}

	const std::string& OutputMongoStream::popPrefix() {
		m_prefixStack.pop_back();
		if (m_prefixStack.size())
			m_prefix = Util::combine(m_prefixStack, ".");
		else
			m_prefix = "";
		return m_prefix;
	}

	BsonDocumentBuilder& OutputMongoStream::finalise() {
		for (auto iter : m_updateMap) {
			(*iter.second) << close_document;
			m_document << concatenate(iter.second->view());
		}

		return m_document;
	}

	std::string OutputMongoStream::toJson() const {
		return bsoncxx::to_json(m_document);
	}
	
	void OutputMongoStream::flush() {
		MongoClientPtr localMongoCl;

		MongoClient* mongoCl = m_mongoCl;

		if (!mongoCl) {
			localMongoCl = std::make_shared<MongoClient>(m_dbName.c_str());
			mongoCl = localMongoCl.get();
		}

		auto coll = mongoCl->coll(m_collName);

		finalise();

		//Info("DB", bsoncxx::to_json(m_document));

		/// Occasionally we'll need to replace the documents (This overides the m_isUpdate flag) ...
		if (m_replace) {
			mongocxx::options::update opts;
			opts.upsert(true); 

			auto result = coll.replace_one(m_queryBuilder.view(), m_document.view(), opts);

			if (!result) {
				Info("DB", "Unable to replace document into the database!");
				return;
			}

			return;
		}
		/// This is the most common case where most of the time we'll be updatin the existing records in the DB
		else if (m_isUpdate) {
			mongocxx::options::update opts;
			opts.upsert(true);
		
			//Info("DB", bsoncxx::to_json(m_document));
			auto result = coll.update_one(m_queryBuilder.view(), m_document.view(), opts);

			if (!result) {
				Info("DB", "Unable to insert document into the database!");
				return;
			}

			return;
		}

		/// Lastly the insertion bit ...
		auto result = coll.insert_one(m_document.view());

		if (!result) {
			Info("DB", "Unable to insert document into the database!");
			return;
		}

		m_insertedId = result->inserted_id().get_oid().value;
	}

	std::string OutputMongoStream::insertedIdStr() const {
		return m_insertedId.to_string();
	}
} /// namespace io
} /// namespace pesa 
