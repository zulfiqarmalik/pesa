/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// MongoStream.h
///
/// Created on: 14 Aug 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Core/IO/Stream.h"
#include "Helper/HelperDefs.h"
#include "Core/Lib/Application.h"

#include "./MongoClient.h"

namespace pesa {
namespace io {
	////////////////////////////////////////////////////////////////////////////
	/// InputMongoStream: Input MongoStreams
	////////////////////////////////////////////////////////////////////////////
	class Helper_API InputMongoStream : public InputStream {
	public:
        BsonOptDocumentValue	m_result;			/// The search result
		BsonDocumentView		m_view;				/// The document view
		BsonDocumentElement 	m_elt; 				/// The current element that we're traversing
		BsonDocumentElementVec	m_eltStack;			/// The view stack that we use to traverse the entire document
		std::vector<std::string> m_eltNameStack;	/// The name stack ... for debugging purposes
		bool 					m_isValid = false; 	/// Is this a valid stream

		BsonDocumentElement		pushElt(BsonDocumentElement elt);
		BsonDocumentElement		pushElt(const char* name);
		void					popElt();

		inline BsonDocumentElement elt(const char* name) { 
			if (m_elt) 
				return m_elt[std::string(name)];

			return m_view[std::string(name)]; 
		}

		template <class T>
		void 					initFromID(const char* dbName, const std::string& collName, T& _id) {
			MongoClient mongoClient(dbName);
			return initFromID(mongoClient, dbName, collName, _id);
		}

		template <class T>
		void 					initFromID(MongoClient& mongoClient, const std::string& collName, const std::string& fieldName, T& fieldValue) {
			auto db = mongoClient.db();
			BsonDocumentBuilder query;
			query << fieldName << fieldValue;
			m_result = db[collName].find_one(query.view());

			if (m_result) {
				m_view = *m_result;
				m_isValid = true;
			}
		}

	public:
								InputMongoStream(BsonDocumentView view);
								InputMongoStream(const std::string& collName, const ObjectId& _id, const char* dbName = nullptr);
								InputMongoStream(const std::string& collName, const std::string& _id, const char* dbName = nullptr);
								InputMongoStream(MongoClient& mongoCl, const std::string& collName, const ObjectId& _id);
								InputMongoStream(MongoClient& mongoCl, const std::string& collName, const std::string& _id);
								InputMongoStream(const std::string& json);
								InputMongoStream(BsonOptDocumentValue result);

								template <class T>
								InputMongoStream(MongoClient& mongoCl, const std::string& collName, const std::string& findField, T& findFieldValue) {
									initFromID(mongoCl, collName, findField, findFieldValue);
								}

		virtual 				~InputMongoStream();

		virtual InputStream& 	read(const char* name, size_t* value);
		virtual InputStream& 	read(const char* name, unsigned int* value);
		virtual InputStream& 	read(const char* name, bool* value);
		virtual InputStream& 	read(const char* name, int* value);
		virtual InputStream& 	read(const char* name, int64_t* value);
		virtual InputStream& 	read(const char* name, float* value);
		virtual InputStream& 	read(const char* name, double* value);
		virtual InputStream& 	read(const char* name, std::string* value);
		virtual InputStream& 	read(const char* name, Streamable* object);
		virtual InputStream& 	read(const char* name, Poco::Timestamp* value);

		void					read(const char* name, ObjectId* oid);
		InputMongoStream&		read(const char* name, uint8_t** data, size_t& size);

		virtual size_t			size(const char* name);
		virtual InputStream&	read(const char* name, std::vector<int>* values);
		virtual InputStream&	read(const char* name, std::vector<float>* values);
		virtual InputStream&	read(const char* name, std::vector<std::string>* values);
		virtual InputStream&	read(const char* name, std::vector<Streamable*>* values);

		virtual InputStream& 	read(const char* name, StrStr_UnMap* map) { return readGeneric(name, map); }
		virtual InputStream&	read(const char* name, StrStr_Map* map) { return readGeneric(name, map); }
		virtual InputStream& 	read(const char* name, StrInt_UnMap* map) { return readGeneric(name, map); }
		virtual InputStream&	read(const char* name, StrInt_Map* map) { return readGeneric(name, map); }
		virtual InputStream& 	read(const char* name, StrFloat_UnMap* map) { return readGeneric(name, map); }
		virtual InputStream&	read(const char* name, StrFloat_Map* map) { return readGeneric(name, map); }

		virtual bool			doesSupportNativeHashtables() const { return true; }
		virtual bool			isDB() const { return true; }

		template <typename MapType> 
		InputStream& 			readGeneric(const char* name, MapType* map) {
			map->clear();

			auto elt = pushElt(name);

			if (!elt)
				return *this;

			auto docElt = elt.get_document();
			auto doc = elt.get_document().value;

			for (auto iter = doc.begin(); iter != doc.end(); iter++) {
				std::string key = iter->key().to_string();
				typename MapType::mapped_type value;
				read(key.c_str(), &value);
				(*map)[key] = value;
			}

			popElt();

			return *this;
		}

		virtual void 			flush();

		bool 					isValid() const;

		inline BsonDocumentView view() { return m_view; }
	};

	////////////////////////////////////////////////////////////////////////////
	/// OutputMongoStream: Output MongoStreams
	////////////////////////////////////////////////////////////////////////////
	class Helper_API OutputMongoStream : public OutputStream {
	protected:
		typedef std::map<std::string, BsonDocumentBuilder*> StrBsonDocumentBuilderPMap;

		std::string				m_dbName;			/// The name of the db
		std::string 			m_collName; 		/// The name of the collection
		std::string				m_prefix;			/// The entire prefix
		std::string				m_id;				/// The id that we're gonna use
		std::vector<std::string> m_prefixStack;		/// The prefix stack
		BsonDocumentBuilder		m_document;			/// The document
		BsonDocumentBuilder		m_queryBuilder;		/// If we're running a query
		bool					m_isUpdate = false; /// It this an update query or not
		bool					m_replace = false;	/// Should we replace the existing document?
		io::ObjectId			m_insertedId;		/// The inserted id
		MongoClient*			m_mongoCl = nullptr;/// The mongo client


		StrBsonDocumentBuilderPMap m_updateMap;		/// To keep track of updates

		const std::string&		pushPrefix(const char* name);
		const std::string&		popPrefix();

		BsonDocumentBuilder&	finalise();

		inline std::string		fullName(const char* n) {
			if (m_prefix.empty())
				return std::string(n);
			return m_prefix + "." + std::string(n);
		}

		inline BsonDocumentBuilder& updateDoc(const std::string& key) {
			auto iter = m_updateMap.find(key);
			if (iter != m_updateMap.end())
				return *iter->second;
			BsonDocumentBuilder* doc = new BsonDocumentBuilder{};
			*doc << key << open_document;
			m_updateMap[key] = doc;
			return *doc;
		}

	public:
		template <typename t_src, typename t_mongo>
		OutputMongoStream& writeGeneric(const char* name, const t_src& value) {
			if (!m_isUpdate)
				m_document << fullName(name) << t_mongo{ value };
			else
				updateDoc("$set") << fullName(name) << t_mongo{ value };
			return *this;
		}

		template <typename T>
		OutputMongoStream& writeArray(const char* name, const std::vector<T>& values, size_t si = 0, size_t ei = 0) {
			if (!ei || ei < si)
				ei = values.size();

			if (!m_isUpdate) {
				m_document << fullName(name) << open_array <<
					[&](array_context<> arr) {
						for (size_t i = si; i < ei; i++)
							arr << values[i];
					} 
				<< close_array;
			}
			else {
				pushPrefix(name);
				updateDoc("$push") << 
					m_prefix << open_document << 
						"$each" << open_array <<
							[&](array_context<> arr) {
								for (size_t i = 0; i < ei; i++)
									arr << values[i];
							} 
						<< close_array
					<< close_document;
				popPrefix();
			}
		
			return *this;
		}

		template <typename T>
		OutputMongoStream& push(const char* name, T value) { 
			ASSERT(m_isUpdate, "Cannot use push unless updating the document!");
			std::vector<T> arr = { value };
			return writeArray(name, arr);
		}

		template <typename T>
		OutputMongoStream& replaceArray(const char* name, const std::vector<T>& values) {
			ASSERT(m_isUpdate, "replaceArray function is only valid while updating the document!");
			pushPrefix(name);
			updateDoc("$set") << 
				m_prefix << open_array <<
					[&](array_context<> arr) {
						for (size_t i = 0; i < values.size(); i++)
							arr << values[i];
					} 
				<< close_array;
			popPrefix();
		
			return *this;
		}

	public:
								OutputMongoStream(const std::string& collName, const char* dbName = nullptr);
								OutputMongoStream(const std::string& collName, const std::string& _id, const char* prefix = nullptr, const char* dbName = nullptr);
								OutputMongoStream(const std::string& collName, ObjectId& _id, const char* prefix = nullptr, const char* dbName = nullptr);
								OutputMongoStream();
		virtual 				~OutputMongoStream();

		virtual OutputStream& 	write(const char* name, const size_t& value);
		virtual OutputStream& 	write(const char* name, const unsigned int& value);
		virtual OutputStream&	write(const char* name, const bool& value);
		virtual OutputStream&	write(const char* name, const int& value);
		virtual OutputStream&	write(const char* name, const int64_t& value);
		virtual OutputStream&	write(const char* name, const float& value);
		virtual OutputStream&	write(const char* name, const double& value);
		virtual OutputStream&	write(const char* name, const std::string& value);
		virtual OutputStream&	write(const char* name, Streamable& value);
		virtual OutputStream&	write(const char* name, Poco::Timestamp& value);

		virtual OutputStream&	write(const char* name, const std::vector<int>& values);
		virtual OutputStream&	write(const char* name, const std::vector<float>& values);
		virtual OutputStream&	write(const char* name, const std::vector<std::string>& values);
		virtual OutputStream&	write(const char* name, const std::vector<Streamable*>& values);

		virtual OutputStream&	write(const char* name, io::ObjectId& oid);
		virtual OutputStream&	write(const char* name, const void* buffer, size_t length);
		OutputMongoStream&		pushBinaryData(const char* name, const void* buffer, size_t length);

		//////////////////////////////////////////////////////////////////////////
		/// Writing different kinds of std::map<KEY, VALUE> variants
		//////////////////////////////////////////////////////////////////////////
		virtual OutputStream& 	write(const char* name, const StrStr_UnMap& map) { return writeMapGeneric(name, map); }
		virtual OutputStream&	write(const char* name, const StrStr_Map& map) { return writeMapGeneric(name, map); }
		virtual OutputStream& 	write(const char* name, const StrInt_UnMap& map) { return writeMapGeneric(name, map); }
		virtual OutputStream&	write(const char* name, const StrInt_Map& map) { return writeMapGeneric(name, map); }
		virtual OutputStream& 	write(const char* name, const StrFloat_UnMap& map) { return writeMapGeneric(name, map); }
		virtual OutputStream&	write(const char* name, const StrFloat_Map& map) { return writeMapGeneric(name, map); }

		virtual bool			doesSupportNativeHashtables() const { return true; }
		virtual bool			isDB() const { return true; }

		std::string				insertedIdStr() const;

		template <typename MapType>
		OutputStream& 			writeMapGeneric(const char* name, const MapType& map) {
			m_document << std::string(name) << open_document;
			for (auto iter = map.begin(); iter != map.end(); iter++)
				write(iter->first.c_str(), iter->second);
			m_document << close_document;
			return *this;
		}

		//////////////////////////////////////////////////////////////////////////
		/// Flush the contents of the stream to Disk/DB
		//////////////////////////////////////////////////////////////////////////
		virtual void 			flush();
		std::string				toJson() const;

		//////////////////////////////////////////////////////////////////////////
		/// Inline functions
		//////////////////////////////////////////////////////////////////////////
		inline BsonDocumentBuilder&	document()	{ return m_document; }
		inline bool&			replace()		{ return m_replace; }
		inline bool				replace() const { return m_replace; }
		inline bool&			isUpdate()		{ return m_isUpdate; }
		inline bool				isUpdate() const{ return m_isUpdate; }
		inline io::ObjectId		insertedId()	{ return m_insertedId; }
		inline std::string&		prefix()		{ return m_prefix; }
		inline MongoClient*&	mongoCl()		{ return m_mongoCl; }
		inline void				setPrefixStack(const StringVec& prefixStack) { 
			m_prefixStack = prefixStack; 
			m_prefix = Util::combine(m_prefixStack, ".");
		}
	};

	typedef std::shared_ptr<InputMongoStream>	InputMongoStreamPtr;
	typedef std::shared_ptr<OutputMongoStream>	OutputMongoStreamPtr;

} /// namespace io
} /// namespace pesa 

