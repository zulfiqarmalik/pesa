### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#!python

import os
import sys
import traceback
import time

from pdaemon_settings import Settings
import datetime as DT
import multiprocessing

def sendEmail(recipients, subject, body, sender = "pdaemon@quanvolve.com"):
    # TODO: Disable temporarily
    return
    try:

        import smtplib
        from os.path import basename
        from email.mime.application import MIMEApplication
        from email.mime.multipart import MIMEMultipart
        from email.mime.text import MIMEText
        from email.utils import COMMASPACE, formatdate

        recipients_     = recipients.split(";")

        msg             = MIMEMultipart()
        msg['From']     = sender
        msg['To']       = ", ".join(recipients_)
        msg['Date']     = formatdate(localtime = True)
        msg['Subject']  = "PDaemon - " + subject

        body            = '<font face="Courier New, Courier, monospace" size="9">' + body + '</font>'
        msg.attach(MIMEText(body, "html"))

        smtp = smtplib.SMTP("qvsmtp")
        smtp.sendmail(sender, recipients_, msg.as_string())

    except Exception as e:
        self.log.error("Unable to send email. Ignoring ...")


# Add all the subdirectories to the search path ...
class PDaemon:
    def __init__(self):
        self.settings           = Settings(self)
        self.processes          = []
        self.numProcesses       = 0
        self.mongoClient        = None
        self.db                 = None
        self.jobsColl           = None
        self.daemonColl         = None
        self.id                 = None
        self.psimVersion        = "1.0.0"
        self.maintenanceMode    = False
        self.numRunningProcesses= 0
        self.lastJob            = None
        self.dbProcesses        = {}

    def run(self):
        try:
            self.settings.parseArguments()
            self.settings.initLogging()
            
            self.minPriority    = int(self.settings.options.minPriority)
            self.maxPriority    = int(self.settings.options.maxPriority)
            self.runOnce        = bool(self.settings.options.runOnce)
            self.maxProcesses   = int(self.settings.options.maxProcesses)

            self.log            = self.settings.log
            self.id             = self.settings.options.id
            self.numCpus        = multiprocessing.cpu_count()
            self.maxPriority    = int(self.settings.options.maxPriority)
            
            self.log.info("Number of CPUs: %d" % (self.numCpus))
            
            # set the number of processes ...
            maxProcesses        = 4 #self.maxProcesses if self.maxProcesses > 0 else int(self.numCpus * 4)
            for i in range(maxProcesses):
                self.processes.append(None)
                
            self.numProcesses = len(self.processes)
                
            self.log.info("Number of processes to spawn: %d" % (self.numProcesses))

            self.connectDB()
            self.updateStatus(doUpsert = True, resetRestart = True)

            # Now we just loop infinitely and spawn processes
            while True:
                try:
                    numRunningProcesses = self.checkProcesses()
                    self.checkDBProcesses()

                    if self.runOnce and numRunningProcesses == 0 and self.lastJob is None:
                        self.log.info("No more jobs and the daemon is set to run once. Breaking the loop ...")
                        break
                    
                    # Update the status for THIS server
                    self.updateStatus(doUpsert = False, resetRestart = False)
                    
                    # Update the latest version for the PSim
                    self.checkStatus()

                except Exception as e:
                    self.log.exception(traceback.format_exc())
                    self.log.exception(e)

            # Once we are out of the main loop, we just wait for all the processes to finish!
            self.log.info("Checking ALL processes before EXIT")

            numRunningProcesses = self.checkProcesses()
            while numRunningProcesses:
                numRunningProcesses = self.checkProcesses()

            self.log.info("EXITING the pdaemon ...")

            # if nothing is given then we assume master
            # if (self.settings.options.master is False and self.settings.options.slave is False) or self.settings.options.master is True:
            #   return self.runMaster()

            # return self.runSlave()
        except Exception as e:
            self.log.exception(e)
            print(e)

    def checkProcesses(self):
        self.numRunningProcesses = 0
        for i in range(self.numProcesses):
            self.checkProcess(i)
                
            if self.processes[i] is not None:
                self.numRunningProcesses += 1

        return self.numRunningProcesses
            
    def connectDB(self):
        from pymongo import MongoClient
        
        if self.db is not None:
            return
            
        dbPath = self.settings.options.dbPath
        self.log.info("Initialising the mongo client at path: %s" % (dbPath))
        self.mongoClient = MongoClient(dbPath)
        if self.mongoClient is None:
            self.log.error("Unable to connect to mongo: %s" % (dbPath))
            sys.exit(1)
        
        self.log.info("Mongo client initialised. Connecting to DB: %s" % (self.settings.options.dbName))
        
        self.db = self.mongoClient[self.settings.options.dbName]
        if self.db is None:
            self.log.error("Unable to connecto to database: %s" % (self.settings.options.dbName))
            sys.exit(1)
        
        self.log.info("Database initialised!")
        self.jobsColl = self.db["Jobs"]
        self.daemonColl = self.db["PDaemon"]
        self.statusColl = self.db["Status"]
        
        # make an entry into the database for this IP address ...
        self.updateStatus(doUpsert = True)
        
    def updateStatus(self, doUpsert = False, resetRestart = False):
        setInfo = { 
            "lastUpdate": DT.datetime.utcnow(),
            "numProcesses": self.numProcesses,
            "numRunningProcesses": self.numRunningProcesses
        }

        if resetRestart:
            setInfo["restart"] = False

        self.daemonColl.update_one({ 
            "_id": self.id,
        }, {
            "$set": setInfo
        }, upsert = doUpsert)
        
    def checkStatus(self):
        doc = self.statusColl.find_one({
            "_id": "global"
        })
        
        # If there is nothing in the global status then we just return
        if doc is None:
            return
            
        # OR there is no version setting in psimVersion
        if "psimVersion" in doc:
            # Update the PSIM version
            self.psimVersion = doc["psimVersion"] 
            
        if "maintenanceMode" in doc:
            self.maintenanceMode = doc["maintenanceMode"]

        # Now we want to see if this particular server has been put into maintenance mode
        myDoc = self.daemonColl.find({
            "_id": self.id
        })

        # Global maintenance mode overrides the local one 
        if "maintenanceMode" in myDoc and self.maintenanceMode is False:
            self.maintenanceMode = doc["maintenanceMode"]

        # If there is a global request for termination
        if "terminate" in doc and doc["terminate"] is True:
            self.log.info("Global termination request ...")
            self.terminate()    

        # If there is a local request for termination then do the same
        if "terminate" in myDoc and myDoc["terminate"] is True: 
            self.log.info("Termination request received for THIS server ...")
            self.terminate()
            
        if "exit" in doc and doc["exit"] is True:    
            self.log.info("Global EXIT request ...")
            self.exit()
                
        # If there is a local request for termination then do the same
        if "exit" in myDoc and myDoc["exit"] is True: 
            self.log.info("EXIT request received for THIS server ...")
            self.exit()

        if "restart" in myDoc and myDoc["restart"] is True:
            self.log.info("RESTARTING PDaemon ...")
            self.restartDaemon()

    def terminate(self, exitCode = 1):
        self.log.info("TERMINATING: %d" % (exitCode))   
        sys.exit(exitCode)

    def getHighPriorityJob(self, index):
        if self.maxPriority <= 70 or self.minPriority >= 70:
            return

        return self.jobsColl.find_one_and_update({
            "$or": [{
                "isFinished": False,
                "handlingServer": "",
                "priority": { 
                    "$gte": 75
                }
            }, {
                "isFinished": False,
                "serverTickTime": {
                    "$lte": DT.datetime.utcnow() - DT.timedelta(minutes = 60)
                },
                "numTries": {
                    "$lt": 3
                },
                "priority": { 
                    "$gte": 75
                }
            }]
        }, {
            "$set": {
                "handlingServer": self.id + "-" + str(index),
                "handlingServerIndex": index,
                "serverTickTime": DT.datetime.utcnow()
            }
        }, sort = [('priority', -1)])

    def getNormalPriorityJob(self, index):
        if self.minPriority:
            return self.jobsColl.find_one_and_update({
                "$or": [{
                    "isFinished": False,
                    "handlingServer": "", 
                    "$and": [{
                        "priority": { "$gte": self.minPriority }
                    }, {
                        "priority": { "$lte": self.maxPriority }
                    }]
                }, {
                    "isFinished": False,
                    "serverTickTime": {
                        "$lte": DT.datetime.utcnow() - DT.timedelta(minutes = 60)
                    },
                    "numTries": {
                        "$lt": 3
                    },
                    "priority": { 
                        "$lte": 100
                    }
                }]
            }, {
                "$set": {
                    "handlingServer": self.id + "-" + str(index),
                    "handlingServerIndex": index,
                    "serverTickTime": DT.datetime.utcnow()
                }
            }) #, sort = [('priority', -1)])
        else:
            return self.jobsColl.find_one_and_update({
                "$or": [{
                    "isFinished": False,
                    "handlingServer": "",
                    "priority": { 
                        "$lte": self.maxPriority
                    }
                }, {
                    "isFinished": False,
                    "serverTickTime": {
                        "$lte": DT.datetime.utcnow() - DT.timedelta(minutes = 60)
                    },
                    "numTries": {
                        "$lt": 3
                    },
                    "priority": { 
                        "$lte": 100
                    }
                }]
            }, {
                "$set": {
                    "handlingServer": self.id + "-" + str(index),
                    "handlingServerIndex": index,
                    "serverTickTime": DT.datetime.utcnow()
                }
            }) #, sort = [('priority', -1)])

    def spawnProcess(self, index):
        try:
            # if we are in maintenance mode then we just don't spawn another process
            if self.maintenanceMode: 
                return None

            self.log.info("[PROC-%d] - Trying to get next job at index: %d" % (index, index))

            # First of all we try to find a high priority job
            nextJob = self.getHighPriorityJob(index)

            # If there is no high priority job then we just move to check whether we have any new jobs at all ...
            if nextJob is None:
                nextJob = self.getNormalPriorityJob(index)

            # What was the last job that we got
            self.lastJob = nextJob

            # If there are no jobs to be processed then we just return ...
            if nextJob is None:
                return None
                
            jobIdStr = str(nextJob["_id"])
            self.log.info("[PROC-%d] - Next job id: %s [Priority: %d]" % (index, jobIdStr, int(nextJob["priority"])))

            import subprocess
            
            opt = self.settings.options
            exeName = "psim"
            if sys.platform == "win32" or sys.platform == "win64":
                exeName += ".exe"

            psimVersion = self.psimVersion
            if "psimVersion" in nextJob:
                psimVersion = nextJob["psimVersion"]
                
            psimPath = opt.path
            
            # If this is just a directory then we need to construct the full filename from it ...
            if os.path.isdir(psimPath) or not psimPath:
                psimPath = os.path.join(opt.path, psimVersion)
            else:
                exeName = os.path.basename(psimPath)
                psimPath = os.path.dirname(psimPath) # get rid of the filename and just have the directory
                
            configPath = opt.config
            # If there is no config path given then we just try to load db_job.xml from the PSIM directory
            if not configPath:
                configPath =  os.path.join(psimPath, "Configs", "Misc", "db_job.xml")
                
            psimLogFilename = str.format("app-%s.log" % (jobIdStr))
            # psimLogFilename = os.path.join(self.settings.psimLogDir, psimLogFilename)

            import tempfile
            psimLogFilename = os.path.join(tempfile.gettempdir(), psimLogFilename)
            
            exePath = os.path.join(psimPath, exeName)

            cmd = [
                exePath, "-c", configPath, "-d", "\"" + opt.dbPath + "@" + opt.dbName + "\"", "-l", psimLogFilename, 
                "-N", "PSIM_Daemon-" + self.id + "-" + str(index), "-O", "-Q", "-J", jobIdStr, "-C"
            ]
            
            self.log.info("[PROC-%d] - Running command: (%s) @ %s" % (index, psimPath, str(cmd)))
            devnull = open(os.devnull, 'w')
            process = None
            
            try:
                process = subprocess.Popen(cmd, cwd = psimPath, stdout = devnull, stderr = devnull, stdin = devnull)
            except Exception as e:
                self.log.error(traceback.format_exc())
                process = None
            
            if process is None:
                self.log.info("[PROC-%d] - Unable to start the process: %s" % (index, cmd))
                return None
            
            self.processes[index] = { 
                "_id": nextJob["_id"],
                "process": process,
                "index": index,
                "logFilename": psimLogFilename,
                "stratId": nextJob["stratId"] if "stratId" in nextJob else "",
                "email": nextJob["email"] if "email" in nextJob else "",
                "emailSuccess": bool(nextJob["emailSuccess"]) if "emailSuccess" in nextJob else False,
                "emailFailure": bool(nextJob["emailFailure"]) if "emailFailure" in nextJob else False
            }
            
            return process
            
        except Exception as e:
            self.log.error(traceback.format_exc())
            print(traceback.format_exc())
            
        return None
        
    def checkProcessReturnCode(self, processInfo, index):
        process = processInfo["process"]

        if process.poll() is None:
            return 0
            
        if index >= 0:
            # Make sure we mark the slot as empty as the first thing
            self.processes[index] = None
                
        returnCode = process.returncode
        self.log.debug("[PROC-%d] - Return code: %d [_id = %s]" % (index, returnCode, str(processInfo["_id"])))

        # read the entire logFilename
        logFilename = processInfo["logFilename"]
        logContents = ""

        # we only write the log file once we have a failure ...
        if process.returncode != 0:
            try:
                if os.path.exists(logFilename):
                    with open(logFilename, 'r') as fileHandle:
                        logContents = fileHandle.read()
            except Exception as e:
                self.log.exception(e)
                logContents = "Could not read log file: " + logFilename + " - Process returned: " + str(process.returncode)
        else:
            self.log.debug("[PROC-%d] - Removing log file: %s" % (index, logFilename))
            try:
                os.remove(logFilename)
            except Exception as e:
                self.log.exception(e)
        
        self.jobsColl.find_one_and_update({
                "_id": processInfo["_id"]
            }, {
                "$set": {
                    "isFinished": True,
                    "exitStatus": process.returncode,
                    "log": logContents
                }
            })

        if processInfo["email"]:
            processName = processInfo["stratId"] if processInfo["stratId"] else str(processInfo["_id"])
            if process.returncode != 0 and processInfo["emailFailure"] is True:
                sendEmail(recipients = processInfo["email"], subject = "FAILED - " + processName + ": " + str(process.returncode), body = logContents)
            elif process.returncode == 0 and processInfo["emailSuccess"] is True:
                sendEmail(recipients = processInfo["email"], subject = "SUCCESS - " + processName, body = "SUCCESS")

        if index >= 0:
            # OK, so we have an empty slot now ... spawn a new process
            return self.spawnProcess(index)

        return -1
        
    def checkProcess(self, index, isExiting = False):
        processInfo = self.processes[index]
        
        # If there is no process running then we just try to spawn a new process
        if processInfo is None:
            return self.spawnProcess(index)
            
        process = processInfo["process"]
            
        # Otherwise we see what the state of this process is ...
        # if process.poll() is None:
        #     # The process is still running ... We cannot use this slot, unless the process is writing to the 
        #     # database. We detect this by seeing if the percent complete has gone past 97% and then we 
        #     # put this process in the DB
        #     if isExiting is False:
        #         jobDoc = self.jobsColl.find_one({"_id": processInfo["_id"]})

        #         if jobDoc is not None and "percent" in jobDoc:
        #             percent = jobDoc["percent"]

        #             if percent >= 0.97:
        #                 self.log.debug("Percent completion gone past 0.97 percent. Freeing up the slot: %s" % (processInfo["_id"]))
        #                 self.processes[index] = None
        #                 self.dbProcesses[processInfo["_id"]] = processInfo

        #                 # immediately spawn a new process
        #                 return self.spawnProcess(index)

        #     return 0
            
        return self.checkProcessReturnCode(processInfo, index)

    def checkDBProcesses(self):
        jobsIdsToRemove = []

        for jobId, processInfo in self.dbProcesses.items():
            # see whether this process has finished
            retCode = self.checkProcessReturnCode(processInfo, processInfo["index"])

            # this process has finished now ... remove it from the dbProcesses list
            if retCode < 0:
                jobsIdsToRemove.append(jobId)

        for jobIdToRemove in jobsIdsToRemove:
            del self.dbProcesses[jobIdToRemove]

    def waitForProcesses(self, isExiting = True):
        self.log.debug("Waiting for processes to finish ...")

        while self.numRunningProcesses > 0:
            self.numRunningProcesses = 0
            for i in range(self.numProcesses):
                self.checkProcess(i, isExiting = isExiting)
                
                if self.processes[i] is not None:
                    self.numRunningProcesses += 1

            time.sleep(5)
        
    def restartDaemon():
        """Restarts the current program, with file objects and descriptors
           cleanup
        """
        self.waitForProcesses(isExiting = True)

        try:
            p = psutil.Process(os.getpid())
            for handler in p.get_open_files() + p.connections():
                os.close(handler.fd)
        except Exception as e:
            self.log.exception(e)

        python = sys.executable
        os.execl(python, python, *sys.argv)

    def exit(self):
        self.waitForProcesses(isExiting = True)
        self.log.debug("EXIT!")
        
        sys.exit(0)

    def runTest(self):
        import pickle
        from m_pingpong import Ping, Pong
        p = Ping()
        s = pickle.dumps(p)
        print(s)
        po = pickle.loads(s)
        print(po)


daemon = PDaemon()

def terminate(signal, frame):
    daemon.terminate()
    sys.exit(0)
    
import signal
signal.signal(signal.SIGINT, terminate)

daemon.run()
