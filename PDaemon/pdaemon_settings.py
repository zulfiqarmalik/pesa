### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys
import logging
from optparse import OptionParser
from logging.handlers import RotatingFileHandler

# from log_util import RainbowLoggingHandler

def getLocalIPAddress():
    import socket
    return socket.gethostbyname(socket.gethostname())

class Settings:
    def __init__(self, app):
        self.app        = app
        self.basePath   = os.path.realpath(__file__)
        self.options    = None
        self.args       = None
        self.argv       = sys.argv
        self.platform   = "linux"
        self.baseDir    = os.path.join(os.path.expanduser('~'), "drv")
        self.logDir     = "./"
        
        if sys.platform == "win32" or sys.platform == "win64":
            self.platform= "windows"
            self.baseDir = "Y:\\"
        
    def initLogging(self):
        # setup `logging` module
        logger = logging.getLogger(self.options.id)

        # console logging
        formatter = logging.Formatter("[%(asctime)s:%(name)s]: %(message)s ")  # same as default

        # # setup `RainbowLoggingHandler`
        self.psimLogDir = os.path.join(self.baseDir, "pdaemon", "psim_logs") 
        self.logDir = os.path.join(self.baseDir, "pdaemon", "logs") 
        self.logFilename = os.path.join(self.logDir, "pdaemon-" + self.options.id + ".log")

        # file logging 
        fileHandler = RotatingFileHandler(self.logFilename, mode = "w", maxBytes=5 * 1024 * 1024, backupCount=2, encoding=None, delay=0)
        fileHandler.setFormatter(formatter)

        # if bool(self.options.consoleLogging) is True:
        #     consoleHandler = RainbowLoggingHandler(sys.stderr, color_funcName=('black', 'yellow', True))
        #     consoleHandler.setFormatter(formatter)
        #     logger.addHandler(consoleHandler)
            
        logger.addHandler(fileHandler) 

        logger.setLevel(logging.INFO)

        self.log = logger

        if self.options.logLevel is "debug":
            self.log.setLevel(logging.DEBUG)
        elif self.options.logLevel is "info":
            self.log.setLevel(logging.INFO)
        elif self.options.logLevel is "warn":
            self.log.setLevel(logging.WARNING)
        else:
            raise RuntimeError("Invalid logging option: " + logger.logLevel)

    def parseArguments(self):
        parser = OptionParser()

        parser.add_option("-c", "--config", dest = "config", default = "", help = "What is the path to the PSim config that we will run", metavar = "FILE")
        parser.add_option("-C", "--consoleLogging", dest = "consoleLogging", action="store_true", default = False, help = "Log to console or not")
        # parser.add_option("-p", "--mip", dest = "minPriority", default = 40, help = "Minimum priorities that we're supposed to handle")
        # parser.add_option("-x", "--mxp", dest = "maxPriority", default = 60, help = "Maximum priorities that we're supposed to handle")

        #parser.add_option("-d", "--dbPath", dest = "dbPath", default = "mongodb://10.240.1.146:27018,10.240.1.242:27018/?replicaSet=psim", help = "The path to the database server")
        parser.add_option("-d", "--dbPath", dest = "dbPath", default = "mongodb://localhost:27017", help = "The path to the database server")
        parser.add_option("-D", "--dbName", dest = "dbName", default = "pesa", help = "The name of the database to connect to")

        parser.add_option("-I", "--id", dest = "id", default = "", help = "What is the ID of the application. This is helpful in logging")
        parser.add_option("-L", "--logLevel", dest = "logLevel", default = "debug", help = "What log level to set. Choose from [debug, info, warn]")

        parser.add_option("-j", "--maxProcesses", dest = "maxProcesses", default = 0, help = "The max number of processes to run. If this is 0 then pdaemon will use: maxProcesses = numCPUs * 4")

        parser.add_option("-O", "--runOnce", dest = "runOnce", action="store_true", default = False, help = "Just one iteration of the pdaemon loop")
        parser.add_option("-p", "--minPriority", dest = "minPriority", default = 0, help = "What is the minimum priority level this can handle!")
        parser.add_option("-P", "--maxPriority", dest = "maxPriority", default = 70, help = "What is the maximum priority level this can handle!")

        psimVersion = "1.0.0"
        baseDir = "/home/zmalik/drv/psim/"
        platformName = "linux"
        exeName = "psim"

        if sys.platform == "win32" or sys.platform == "win64" or sys.platform == "windows":
            baseDir = "Y:\\psim"
            platformName = "windows"
            exeName = "psim.exe"

        psimPath = os.path.join(baseDir, "psim" + psimVersion, platformName, exeName)

        # psimPath = "/home/zmalik/tdrv/pdaemon_sim/linux/psim"
        # if sys.platform == "win32" or sys.platform == "win64" or sys.platform == "windows":
        #     psimPath = "Y:\\psim\\windows\\psim.exe"

        parser.add_option("-F", "--path", dest = "path", default = psimPath, help = "What is the path to the PSim executable")

        (self.options, self.args) = parser.parse_args()
        
        self.options.id = getLocalIPAddress() + self.options.id

    def nodb(self):
        return self.options.nodb

