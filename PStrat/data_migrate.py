### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#!python

import os
import sys
import traceback
import time

import datetime as DT
import multiprocessing

from pymongo import MongoClient

collName = "StatsSearch"
dbPath = "mongodb://10.240.1.70:27018/?replicaSet=psim"
dbName = "pesa"

mongoClient = MongoClient(dbPath)
if mongoClient is None:
    sys.exit(1)

dbStats = mongoClient[dbName]
if dbStats is None:
    sys.exit(1)

coll = dbStats[collName]
assert coll, "Unable to find collection: %s [DB = %s, Universe = %s, Market = %s]" % (collName, dbName, universeId, market)

stypes = [
    "LT",
    "2012", "2012_QT01", "2012_QT02", "2012_QT03", "2012_QT04", 
    "2013", "2013_QT01", "2013_QT02", "2013_QT03", "2013_QT04", 
    "2014", "2014_QT01", "2014_QT02", "2014_QT03", "2014_QT04", 
    "2015", "2015_QT01", "2015_QT02", "2015_QT03", "2015_QT04", 
    "2016", "2016_QT01", "2016_QT02", "2016_QT03", "2016_QT04", 
    "2017" 
]

for stype in stypes:
    try:
        print("SType: %s" % (stype))
        cursor = dbStats.find({ "stype": stype })

        print("%s count: %d" % (cursor.count()))

        icount = 0
        for doc in cursor:
            icount += 1
            stype = doc["stype"]
            uuid = doc["uuid"]
            targetColl = "STATS_" + stype 

            icoll = dbStats[targetColl]
            doc["_id"] = doc["uuid"]
            del doc["uuid"]
            icoll.insert_one(doc)
            print("[%d] Inserting: %s => %s" % (icount, uuid, targetColl))

    except Exception as e:
        self.log.exception(e)
        print(e)
