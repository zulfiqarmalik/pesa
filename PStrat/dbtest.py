### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#!python

import os
import sys
import traceback
import time

import datetime as DT
import multiprocessing

from pstrat_settings import Settings
from collections import OrderedDict

import xml.etree.ElementTree as ET
from xml.etree.ElementTree import Comment
from xml.dom import minidom


from pymongo import MongoClient

universeId = "Top1000"
market = "US"
collName = "StatsSearch"
# dbPath = "mongodb://10.240.1.70:27018/?replicaSet=psim"
dbPath = "mongodb://localhost:27017/"
dbName = "pesa"
invalidThreshold = 5
skip = 0
limit = 1

query1 = {'ir': {'$gt': 0.1}, 'stype': '2015'}
query2 = {'LT.ir': {'$gt': 0.3}}
squery = { "$or": [ query1, query2 ] }

mongoClient = MongoClient(dbPath)
if mongoClient is None:
    sys.exit(1)

dbStats = mongoClient[dbName]
if dbStats is None:
    sys.exit(1)

coll = dbStats[collName]
assert coll, "Unable to find collection: %s [DB = %s, Universe = %s, Market = %s]" % (collName, dbName, universeId, market)

pipeline = []

pipeline.append({ "$match": query1 })
pipeline.append({ 
    "$lookup": {
        "from": "LT_StatsSearch",
        "localField": "uuid",
        "foreignField": "uuid",
        "as": "LT"
    } 
})
pipeline.append({
    "$unwind": "$ir"
})
# pipeline.append({
#     "$match": query2
# })
# pipeline.append({ 
#     "$project": {
#         "_id": "$uuid"
#     } 
# })

print("[DB = %s, Coll = %s]: %s" % (dbName, collName, str(pipeline)))

# now we setup an aggregation pipeline
cursor = coll.aggregate(pipeline)
count = 0

import json
if cursor:
    for doc in cursor:
        print(json.dumps(doc, indent = 4))
        count += 1

print("Count: %d" % (count))

