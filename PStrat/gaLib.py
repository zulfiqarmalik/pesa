### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import numpy as np
import sys
import os
import pickle

class Pnl:
    def __init__(self, pnl, name):
        self.pnl = pnl
        self.name = name

def readPnl(fname):
    pnl = []
    maxsize = 1000
    with open(fname, 'r') as infile:
        lines = infile.readlines()
        lines = lines[1:]
        c = 0
        for line in lines:
            l = line.split(',')
            if len(l) < 4:
                break
            date = int(l[0].strip())
            if (date // 10000 == 2017):
                continue
            p = float(l[3].strip())
            pnl.append(p)
    return Pnl(pnl, fname)

def cumPnl(p):
    days = 21
    l = [ sum(p.pnl[(days * i) : (days * i + days)]) for i in range(len(p.pnl) // days) ]
    return Pnl(l, p.name)

def maxcorr(p, cluster):
    return max(np.abs(np.corrcoef(p.pnl, c.pnl)[0][1]) for c in cluster)

def ir(x):
    return np.mean(x) / np.std(x)

def cluster(pnls, CORR):
    #return pnls 
    clusters = []
    clusters.append([pnls[0]])
    for i in range(1, len(pnls)):
        #print('Iter', i, 'out of', len(pnls))
        #print(len(pnls[i].pnl))
        #print(pnls[i].pnl)
        cluster_this = None
        drop_clusters = set()
        for j in range(len(clusters)):
            cluster = clusters[j]
            corr = maxcorr(pnls[i], cluster)
            if corr > CORR:
                if cluster_this is None: #assign
                    cluster_this = cluster
                    cluster_this.append(pnls[i])
                else: #merge
                    cluster_this += cluster
                    drop_clusters.add(j)
        if cluster_this is None:
            #print('New cluster:', i)
            clusters.append([pnls[i]])
        elif drop_clusters:
            #print('Dropping', drop_clusters)
            #print([ len(c) for c in clusters ])
            clusters = [ clusters[j] for j in range(len(clusters)) if j not in drop_clusters ]
            #print([ len(c) for c in clusters ])
        else:
            #print('Appended to existing cluster')
            pass
    lengths = sorted([ len(c) for c in clusters ])
    print('Number of clusters: {}, size distribution: {}'.format(len(lengths), lengths))
    res = []
    for c in clusters:
        x = sorted(c, key = lambda x : np.abs(ir(x.pnl)))
        res.append(x[-1])
        #print(ir(x[-1].pnl))
    return res

def weights(idxSet, signs):
    w = np.zeros(len(signs))
    for idx in idxSet:
        w[idx] = signs[idx]
    return w

def eval(idxSet, signs, fmat):
    p = np.dot(weights(idxSet, signs), fmat)
#    print(np.std(p))
#    return ir(p) - 1e-8 * np.std(p)
    return ir(p) + ir(p[-504:]) + ir(p[-252:])
#    l = 1.0
#    return (1 - l) * ir(p) + l * ir(p[-252:])

def evalVolat(idxSet, signs, fmat):
    return np.std(np.dot(weights(idxSet, signs), fmat))

def randSubset(m, inputIdx):
    return list(np.random.choice(inputIdx, m, replace = False))

def generatePop(signs, nonzeroW, popSize):
    res = []
    size = len(signs)
    rng = list(range(size))
    return [ randSubset(nonzeroW, rng) for i in range(popSize) ]

def cross(nonzeroW, idxSet0, idxSet1):
    return randSubset(nonzeroW, list(set(idxSet0 + idxSet1)))

def splitIsOs(pnl, isMask):
    s = set(isMask)
    isP = []
    osP = []
    for i, p in enumerate(pnl.pnl):
        if i in isMask:
            isP.append(p)
        else:
            osP.append(p)
    return Pnl(isP, pnl.name), Pnl(osP, pnl.name)

def filter(splitPnls, splitSubPnls):
    res = []
    for i, (isPnl, osPnl) in enumerate(splitPnls):
        (isSubPnl, osSubPnl) = splitSubPnls[i]
        print(ir(isPnl.pnl), ir(isSubPnl.pnl))
        if ir(isPnl.pnl) < 0.05:
            continue
        if ir(isSubPnl.pnl) < 0.05:
            continue
        res.append((isPnl, osPnl))
    print('Filtered: {} alphas'.format(len(res)))
    return res

def runGA(pnls, subpnls, isMask, corrCut, iterNum, nonzeroW, popSize):
    splitPnls = [splitIsOs(pnl, isMask) for pnl in pnls]
    splitSubPnls = [ splitIsOs(pnl, isMask) for pnl in subpnls ]
    splitPnls = filter(splitPnls, splitSubPnls)
    isPnls, osPnls = [ s[0] for s in splitPnls ], [ s[1] for s in splitPnls ]

    cumPnls = [ cumPnl(pnl) for pnl in isPnls ]
    print('Clustering {} alphas.. corrCut = {}'.format(len(cumPnls), corrCut))
    goodNames = set([ c.name for c in cluster(cumPnls, corrCut) ])

    fpnls = [ p for p in isPnls if p.name in goodNames ]
    osFpnls = [ p for p in osPnls if p.name in goodNames ]
    leaveTopPerc = 25
    if len(fpnls) < nonzeroW:
        print("Warning: len(fpnls) < nonzeroW")
        nonzeroW = len(fpnls)
        
    #generate initial population
    signs = [ np.sign(np.sum(p.pnl)) for p in fpnls ]
    pop = generatePop(signs, nonzeroW, popSize)

    fmat = np.array([ p.pnl for p in fpnls])
    fOSmat = np.array([ p.pnl for p in osFpnls])
    for iter in range(iterNum):
        #filter population
        evals = [ eval(idx, signs, fmat) for idx in pop ]
        cut = np.percentile(evals, 100 - leaveTopPerc)
#       evalsVolat = [ evalVolat(idx, signs, fmat) for idx in pop ]
#       volPercentile = 10
#       cutVolat = np.percentile(evalsVolat, 100 - volPercentile)
        
        #evaluate OS
        osEvals = [ eval(idx, signs, fOSmat) for idx in pop ]
        #print(sorted(osEval))
        maxIsIdx = evals.index(np.max(evals))
        print('GA iter', iter, '- IS median:', np.median(evals), '- OS median:', np.median(osEvals), '- OS eval max:', osEvals[maxIsIdx])
        
        #filter
        pop = [ idx for ir, idx in zip(evals, pop) if ir >= cut ]
        #pop = [ idx for volat, idx in zip(evalsVolat, pop) if volat >= cutVolat ]
        #do crossover
        while len(pop) < popSize:
            i0 = np.random.randint(len(pop))
            i1 = np.random.randint(len(pop))
            if i0 != i1:
                pop.append(cross(nonzeroW, pop[i0], pop[i1]))

    evals = [ eval(idx, signs, fmat) for idx in pop ]

    cut = sorted(evals)[-100]
    print(cut)
    print(evals)
    goodIdx = set([ i for i, ir in enumerate(evals) if ir >= cut ])
    print(goodIdx)
    weights = np.zeros(len(osFpnls))
    for i, p in enumerate(pop):
        if i in goodIdx:
            for idx in p:
                weights[idx] += 1
    weightDict = { p.name : weights[i] for i, p in enumerate(osFpnls) }
    return weightDict

def getPnls():
    with open('../fastpnl/names/euQSGbasic.txt') as ifl:
        lines = ifl.readlines()
        pnls = [ readPnl(l.strip()) for l in lines ]
        pnls = [ p for p in pnls if np.nanmean(p.pnl) ]
    return pnls

def parseQuery(query):
    qs = query.split('.')
    res = []
    for i in range(1, len(qs)):
        if qs[i].startswith('volatility') or qs[i].startswith('ir'):
            qss = qs[i - 1].split('_')
            try:
                year = int(qss[1])
                res += [ 100 * year + qnum for qnum in range(1, 5) ]
            except:
                pass
    return res

# pnls = getPnls()
# assert len(set([len(p.pnl) for p in pnls ])) == 1
# pnlSize = len(pnls[0].pnl)
# for s in range(100):
#     np.random.seed(seed = s)
#     isMask = getIsMask(pnlSize)
#     runGA(pnls, isMask)

#query = ' LT.tvr < 30.0 and _2015_QT01.ir > 0.05 && _2016_QT02.ir > 0.05 '
#print(parseQuery(query))
