### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#!python

import os
import sys
import json
import traceback
import time
import numpy as np

import datetime as DT
import multiprocessing

from pstrat_settings import Settings
from collections import OrderedDict

import xml.etree.ElementTree as ET
from xml.etree.ElementTree import Comment
from xml.dom import minidom

from pstrat import PStrat
import sutil

import gaLib

# Add all the subdirectories to the search path ...
class GASearch:
    def __init__(self, settings = None, skip = 0, limit = 1000):

        if settings is None:
            self.settings       = Settings(self)
            self.settings.parseArguments()
            self.settings.initLogging()
        else:
            self.settings       = settings

        opt                     = self.settings.options
        self.log                = self.settings.log
        self.universeId         = opt.universeId
        self.query = opt.query
        self.params = opt.GA
        self.usedAlphas         = {}

    def init(self):
        self.queryQuarters = gaLib.parseQuery(self.query)
        params = json.loads(self.params)
        print(params)
        self.paramQuarters = params["quarters"]
        print(self.queryQuarters)
        print(self.paramQuarters)
        assert len(self.queryQuarters) == 8
#        assert len(self.paramQuarters) == 4
        self.quarters = self.queryQuarters + self.paramQuarters
#        assert len(self.quarters) == 12

        self.pstrat = PStrat(settings = self.settings, ignoreAlphaIds = self.usedAlphas, outputFilename = "", getAlphaStats = False, genStrategy = False, noReversion=False)
        self.pstrat.maxQueryIter = 20
   
    def run(self):
        strategy = self.pstrat.run()
#        self.log.info("Number of alphas: %d" % (len(strategy["alphaIds"])))
#        print(strategy["alphaStats"][0]["vec"][100]["pnl"])
        return strategy

def process(strategy, pstrat):
    # startDate = 20130201

# startDate = 20130301

    # endDate = 20160101

    res = []
    subres = []
    dates = set()
    statsColl = pstrat.dbStats['Stats']
    alphaDocs = strategy['alphaDocs']
    name2alphaDoc = {}
    qcount = 0

    print("[GA] Processing num alphas: %d" % (len(alphaDocs)))


    for alphaDoc in alphaDocs:
        alphaId = alphaDoc['_id']

#    print(alphaDoc["reversion"])

        if 'factset.' in alphaId:
            continue

        print("[GA] Processing: %s" % (alphaId))

        stat = statsColl.find_one({'_id': alphaId})
        alphaIdSubuniv = alphaId.replace('US_Top1000_', 'US_Top500_')
        statSub = statsColl.find_one({'_id': alphaIdSubuniv})

    # print(statSub)

        if statSub is None:
            continue

        # if stat['startDate'] != startDate:
        #     continue
        # if stat['endDate'] < endDate:
        #     continue
        # if statSub['startDate'] != startDate:
        #     continue
        if statSub['endDate'] < stat["endDate"]:
            continue

#    print(stat.get("volatility"))

        vec = stat['vec']
        if not vec:
            continue

        subvec = statSub['vec']
        if not subvec:
            continue

        name = stat['_id']
        assert name == alphaId
        name2alphaDoc[name] = alphaDoc
        pnl = []
        subpnl = []

        for i in range(len(vec)):
            date = vec[i]['date']
            dates.add(date)
            if date >= stat['endDate']:
                break

            p = vec[i]['pnl']
            pnl.append(p)
            subpnl.append(subvec[i]['pnl'])

 #   print(np.sign(np.sum(pnl)))

        if alphaDoc['reversion'] == 'true':
            pnl = [-p for p in pnl]
            subpnl = [-p for p in subpnl]

        res.append(gaLib.Pnl(pnl, name))
        subres.append(gaLib.Pnl(subpnl, name))

    dates = sorted(dates)
    print('Processed: {} alphas'.format(len(res)))
    res = sorted(res, key=lambda x: x.name)
    subres = sorted(subres, key=lambda x: x.name)
    return (res, dates, name2alphaDoc, subres)


def quarter(date):
    year = date // 10000
    month = date % 10000 // 100
    q = 1 + (month - 1) // 3
    return 100 * year + q


def getQuarterIsMask(quarters, dates):
    print('getQuarterIsMask', quarters)
    res = []
    qset = set(quarters)

    for (i, d) in enumerate(dates):
        if quarter(d) in quarters:
            res.append(i)

    return res


if __name__ == '__main__':
    gaSearch = GASearch()
    gaSearch.init()
    gaSearch.pstrat.loadRefConfig(gaSearch.universeId)

  # exit()

    strategy = gaSearch.run()
    (pnls, dates, name2alphaDoc, subpnls) = process(strategy, gaSearch.pstrat)
    isMask = getQuarterIsMask(gaSearch.quarters, dates)
    weightDict = gaLib.runGA(pnls, subpnls, isMask, corrCut = 0.7, iterNum = 1600, nonzeroW = 44, popSize = 1000)

#  weightDict = gaLib.runGA(pnls, subpnls, isMask, corrCut = 0.8, iterNum = 10000, nonzeroW = 100, popSize = 1000)
#  weightDict = gaLib.runGA(pnls, subpnls, isMask, corrCut = 0.99, iterNum = 10, nonzeroW = 10, popSize = 100)
#  reversion = -1

    for (name, weight) in weightDict.items():
        if weight == 0.0:
            continue

        alphaDoc = name2alphaDoc[name]
        alphaXml = gaSearch.pstrat.docToXml(gaSearch.pstrat.alphasElt, strategy, alphaDoc)

#    alphaXml.attrib["weight"] = str(reversion * weight)

        alphaXml.attrib['weight'] = str(weight)
        alphaXml.attrib['reversion'] = alphaDoc['reversion']

    xmlString = ET.tostring(gaSearch.pstrat.refXml, 'utf-8')
    reparsed = minidom.parseString(xmlString)
    xmlPrettyString = '\n'.join([line for line in reparsed.toprettyxml(indent=' ' * 4).split('\n') if line.strip()])

    with open('ga_strat.xml', 'w') as file:
        file.write(xmlPrettyString)
