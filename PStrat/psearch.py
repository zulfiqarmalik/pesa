### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#!python

import os
import sys
import traceback
import time

from pstrat_settings import Settings
import datetime as DT
import ast

# Add all the subdirectories to the search path ...
class PSearch:
    def __init__(self, settings, isMainQuery = False):
        self.settings           = settings
        self.query              = { "collections": {} }
        self.collectionsUsed    = {}
        self.log                = None
        self.isMainQuery        = isMainQuery

        if self.settings:
            self.log            = self.settings.log

    def addCollectionField(self, coll, field):
        if coll not in self.collectionsUsed:
            self.collectionsUsed[coll] = {}

        collDict = self.collectionsUsed[coll]
        if field in collDict:
            collDict[field] += 1 
        else:
            collDict[field] = 1

    def evalNode(self, node):
        if isinstance(node, ast.Compare):
            return self.evalComparison(node)
        elif isinstance(node, ast.BoolOp):
            return self.evalBoolOpNode(node)
        elif isinstance(node, ast.Num):
            return self.evalNum(node)
        elif isinstance(node, ast.Attribute):
            return self.evalAttribute(node)
        elif isinstance(node, ast.UnaryOp):
            return self.evalUnaryOp(node)
        return self.evalRelationalOp(node)
        
    def evalUnaryOp(self, node):
        mul = 1.0
        if isinstance(node.op, ast.USub):
            mul = -1.0
        return self.evalNode(node.operand) * mul

    def evalNum(self, node):
        return node.n

    def evalName(self, node):
        return node.id

    def evalAttribute(self, node):
        collection = self.evalName(node.value)
        variable = node.attr
        # self.addCollectionField(collection, variable)
        if collection:
            collection = collection.lstrip('_')
            if collection not in self.query["collections"]:
                self.query["collections"][collection] = 1
            else:
                self.query["collections"][collection] += 1

        return collection + "." + variable if not self.isMainQuery else variable, collection

    def evalRelationalOp(self, node):
        if isinstance(node, ast.GtE):
            return "$gte"
        elif isinstance(node, ast.Gt):
            return "$gt"
        elif isinstance(node, ast.LtE):
            return "$lte"
        elif isinstance(node, ast.Lt):
            return "$lt"
        elif isinstance(node, ast.Eq):
            return "$eq"
        elif isinstance(node, ast.NotEq):
            return "$ne"

        assert False, "Unknown operator: %s" % (str(node))

    def evalBoolOp(self, node):
        if isinstance(node, ast.And):
            return "$and"
        elif isinstance(node, ast.Or):
            return "$or"
        assert False, "Unsupported operation: %s" % (str(node))

    def evalBoolOpNode(self, node):
        qvalues = []
        for value in node.values:
            qvalue = self.evalNode(value)
            qvalues.append(qvalue)

        op = self.evalBoolOp(node.op)
        query = {}
        query[op] = qvalues 
        return query

    def evalComparison(self, node):
        lhs = ""
        collection = ""
        if isinstance(node.left, ast.Attribute):
            lhs, collection = self.evalAttribute(node.left)
        elif isinstance(node.left, ast.Name):
            lhs = self.evalName(node.left)

        assert lhs, "Unable to evaluate comparison properly!"
        assert len(node.ops) == 1, "Do not support more than one relational operator!"
        assert len(node.comparators) == 1, "Do not support more than one relational operator!"

        op = self.evalRelationalOp(node.ops[0])
        rhs = self.evalNode(node.comparators[0])
        query = {}
        query[lhs] = {}

        if collection:
            collection = collection.lstrip('_')
            self.query["collections"][collection] = True  
            # query["stype"] = stype
        query[lhs][op] = rhs

        return query

    def parseString(self, qstr):
        try:
            tree = ast.parse(qstr, mode = 'eval')
            self.query["search"] = self.evalNode(tree.body)
            # print(self.query)
            return self.query
        except Exception as e:
            print(e)
            
        return None

if __name__ == '__main__':
    ps = PSearch(None)
    ps.parseString("LT.ir >= 0.1 and (LT.tvr < 25.0 or _2016.maxDD != 10.0)")
