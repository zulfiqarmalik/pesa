### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#!python

import os
import sys
import traceback
import time

import datetime as DT
import multiprocessing

from pstrat_settings import Settings
from collections import OrderedDict

import xml.etree.ElementTree as ET
from xml.etree.ElementTree import Comment
from xml.dom import minidom
import re

def dateToInt(date):
    if date is None:
        date = DT.datetime.utcnow()
    return int(str(date.year).zfill(4) + str(date.month).zfill(2) + str(date.day).zfill(2))

def formatUuid(uuid):
    import re
    return re.sub('[^a-zA-Z0-9\n_]', '-', uuid)

# Add all the subdirectories to the search path ...
class PStrat:
    def __init__(self, settings = None, limit = None, strategy = None, ignoreAlphaIds = {}, queryStr = None, comment = None, outputFilename = None, getAlphaStats = False, genStrategy = True, noReversion = False):

        if settings is None:
            self.settings       = Settings(self)
            self.settings.parseArguments()
            self.settings.initLogging()
        else:
            self.settings       = settings

        opt                     = self.settings.options
        self.maxSTCorrDays      = 63
        self.log                = self.settings.log
        self.noReversion        = noReversion
        self.mongoClient        = None
        self.dbPath             = opt.dbPath
        self.dbName             = opt.dbName
        self.dbStatsName        = opt.dbName
        self.excludeDatasets    = [ed.strip() for ed in opt.excludeDatasets.split(',')]
        self.outputFilename     = outputFilename if outputFilename is not None else opt.output
        self.db                 = None
        self.dbStats            = None
        self.genStrategy        = genStrategy
        self.universeIds        = opt.universeId.split(",")
        self.market             = opt.market
        self.refConfig          = opt.config
        self.invalidThreshold   = opt.invalidThreshold
        self.searchBatchSize    = int(opt.searchBatchSize)
        self.limit              = limit if limit else int(opt.limit)
        self.sort               = opt.sort
        self.skip               = int(opt.skip)
        self.sortOrder          = 1
        self.batchId            = opt.batchId
        self.refXml             = None
        self.portfolioElt       = None
        self.alphasElt          = None
        self.stCorr             = float(opt.shortTermCorr)
        self.ltCorr             = float(opt.longTermCorr)
        self.stCorrAlpha        = float(opt.shortTermAlphaCorr)
        self.ltCorrAlpha        = float(opt.longTermAlphaCorr)
        self.longShortRatioTol  = float(opt.longShortRatioTolerance)
        self.longShortSumTol    = float(opt.longShortSumTolerance)
        self.author             = opt.author
        self.strategy           = strategy
        self.ignoreAlphaIds     = ignoreAlphaIds
        self.comment            = comment
        self.maxQueryIter       = 20
        self.getAlphaStats      = getAlphaStats

        self.queryStr           = queryStr if queryStr else opt.query
        queries                 = self.queryStr.split("&&")

        assert len(queries) <= 2, "Invalid query: %s (Can only have one && in the query)" % (self.queryStr)

        from psearch import PSearch
        import sutil

        self.query              = {
            "queryStr": self.queryStr,
            "skip": 0,
            "limit": self.searchBatchSize,
            "canSearch": True,
            "joins": [],
            "forwardQuery": {
                "main": None,
                "secondary": None
            },
            "reversionQuery": {
                "main": None,
                "secondary": None
            }
        }

        result = PSearch(self.settings, isMainQuery = True).parseString(queries[0])
        mainCollName = None
        for collName in result["collections"].keys():
            mainCollName = collName

        self.query["mainCollName"] = "STATS_" + mainCollName

        joinsLookup = {}
#        joinsLookup[mainCollName] = True

        self.query["forwardQuery"]["main"] = result["search"]

        if self.noReversion is False:
            self.query["reversionQuery"]["main"] = sutil.invertQuery(self.query["forwardQuery"]["main"])
        else:
            self.query["reversionQuery"] = None

        if len(queries) > 1:
            result = PSearch(self.settings, isMainQuery = False).parseString(queries[1].strip())
            collections = result["collections"]
            self.query["forwardQuery"]["secondary"] = result["search"]

            if self.query["reversionQuery"] and self.noReversion is False:
                self.query["reversionQuery"]["secondary"] = sutil.invertQuery(self.query["forwardQuery"]["secondary"])

            for collName in collections:
                if collName not in joinsLookup:
                    joinsLookup[collName] = True
                    self.query["joins"].append(collName)

#        print("Joins lookup: %s" % (str(joinsLookup)))

        # print("Forward: %s" % (str(forwardQuery)))
        # print("Reversion: %s" % (str(reversionQuery)))

        self.connectDB()

    def loadRefConfig(self, universeId, refString = None):
#        if self.genStrategy is False:
#            return 

        if not refString:
            refConfigFilename       = self.refConfig
            refConfigFilename       = refConfigFilename.replace("universeId", universeId.lower())

            self.refXml             = ET.parse(self.refConfig).getroot()

        else:
            self.refXml             = ET.fromstring(refString)

        # get to the portfolio and alphas element
        self.portfolioElt       = self.refXml.find("Portfolio")
        assert self.portfolioElt is not None, "Unable to load the portfolio element!"

        from bson import ObjectId
        iid = ObjectId()
        self.portfolioElt.attrib["iid"] = str(iid)
        self.portfolioElt.attrib["creationDate"] = str(dateToInt(None))

        defsElt            = self.refXml.find("Defs")
        defsElt.attrib["dbName"] = self.dbName

        self.alphasElt          = self.portfolioElt.find("Alphas")
        assert self.alphasElt is not None, "Unable to load the alphas element!"

    def run(self):
        try:
            for universeId in self.universeIds:
                stopSearch = False
                strategy = self.strategy

                if self.strategy is None:
                    strategy = {
                        "alphaIds": {},
                        "alphaDocs": [],
                        "points": None,
                        "pointDateIndexes": {},
                        "accumPnls": None,
                        "stCorr": self.stCorr,
                        "ltCorr": self.ltCorr,
                        "limit": self.limit, 
                        "alphaStats": [],
                        "xml": ""
                    }

                numIter = 0
                existingLength = len(strategy["alphaDocs"])

                while (len(strategy["alphaDocs"]) - existingLength) < self.limit and stopSearch is False:
                    self.runSearch(universeId = universeId, market = self.market, collName = self.query["mainCollName"], qdetails = self.query, strategy = strategy)
                    numIter += 1
                    
                    if self.maxQueryIter > 0 and numIter > self.maxQueryIter:
                        break

                    # If we can't search anymore then stop the top level while loop
                    if self.query["canSearch"] is False:
                        stopSearch = True

                self.log.info("Alpha count: %d" % (len(strategy["alphaDocs"])))
                    
                # load the ref config again ...
                if self.genStrategy:
                    self.loadRefConfig(universeId, strategy["xml"] if "xml" in strategy and strategy["xml"] else None)

                    if len(strategy["alphaDocs"]) == 0:
                        self.log.info("No search results. Not writing the config file!")
                        continue

                    maxCreationDate = None

                    for alphaDoc in strategy["alphaDocs"]:
                        alphaXml = self.docToXml(self.alphasElt, strategy, alphaDoc)
                        alphaXml.attrib["reversion"] = alphaDoc["reversion"]

                    # self.alphasElt.append(alphaXml)

                    # then push the output to it ...
                    outputFilename = self.outputFilename + "_" + universeId + ".xml" if self.outputFilename else ""
                    if outputFilename:
                        self.log.info("Writing to file: %s" % (outputFilename))

                    comment = self.comment

                    if comment is None:

                        comment = str.format(''' 

Copyright hkhan team, All rights reverved! Please read the terms and conditions.

Created by    : %s
Creation Date : %s
Name          : %s 
Universe      : %s
Query         : %s

                        ''' % (self.author, DT.datetime.utcnow().strftime("%a %b %d %H:%M:%S %Z %Y"), self.outputFilename, universeId, self.queryStr))

                    # file.write("\n\n<!--\nCopyright hkhan, All rights reverved!\n")
                    # file.write("Created by: Zulfiqar Malik\n")
                    # file.write("Creation Date: " + DT.datetime.utcnow().strftime("%a %b %d %H:%M:%S %Z %Y") + "\n")
                    # file.write("Name: " + self.outputFilename + "\n")
                    # file.write("Universe: " + universeId + "\n")
                    # file.write("Query: " + self.queryStr + "\n")
                    # file.write("-->\n\n")

                    self.refXml.insert(0, Comment(comment))

                    strategy["refXml"] = self.refXml

                    # If we have a valid output filename then write to it ...
                    if outputFilename:
                        file = open(outputFilename, "w")

                        # file.write("<?xml version="1.0" ?>\n\n<!--\nCopyright hkhan, All rights reverved!\n")
                        # file.write("Created by: Zulfiqar Malik\n")
                        # file.write("Creation Date: " + DT.datetime.utcnow().strftime("%a %b %d %H:%M:%S %Z %Y") + "\n")
                        # file.write("Name: " + self.outputFilename + "\n")
                        # file.write("Universe: " + universeId + "\n")
                        # file.write("Query: " + self.queryStr + "\n")
                        # file.write("-->\n\n")

                        xmlString = ET.tostring(self.refXml, 'utf-8')
                        reparsed = minidom.parseString(xmlString)
                        xmlPrettyString = '\n'.join([line for line in reparsed.toprettyxml(indent=' ' * 4).split('\n') if line.strip()])
                        
                        file.write(xmlPrettyString)
                        file.close()

                        if "accumPnls" in strategy and strategy["accumPnls"]:
                            pnlFilename = self.outputFilename + "_" + universeId + ".pnl"
                            self.log.info("Writing PNL file: %s", pnlFilename)
                            file = open(pnlFilename, "w")
                            points = strategy["points"]
                            file.write("%12s,%12s,%12s,%12s\n" % ("Date", "$ BookSize", "$ Cum. P&L", "$ D. P&L"))
                            cumPnl = 0.0

                            for i in range(len(points)):
                                pt = points[i]
                                date = pt["date"]
                                bookSize = pt["bookSize"]
                                pnl = strategy["accumPnls"][i]
                                cumPnl += pnl
                                file.write("%12d,%12.2f,%12.2f,%12.2f\n" % (date, bookSize, cumPnl, pnl))

                            file.close()

                # ... Otherwise just return the strategy object ...
                return strategy

        except Exception as e:
            self.log.exception(e)
            print(e)
            sys.exit(1)

    def runQuery(self, universeId, market, collName, query, skip, limit, strategy, joins = None, isReversion = False):

        # return ["US_Top100_Value(baseData.adjClosePrice)_OpNormalise"]
        coll = self.dbStats[collName]
        assert coll, "Unable to find collection: %s [DB = %s, Universe = %s, Market = %s]" % (collName, self.dbStatsName, universeId, market)

        pipeline = [
            { "$match": { "universeId": universeId } },
#            { "$match": { "numInvalidPnl": { "$lte": self.invalidThreshold } } },
        ]

        pipeline.append({ "$match": query["main"] })

        if self.batchId:
            pipeline.append({ "$match": { "tags.gbatchId": { "$eq": self.batchId } } })

        print("Joins: %s" % (str(joins)))

        # We do all the joins first 
        if joins is not None and len(joins):
            for join in joins:
                pipeline.append({
                    "$lookup": {
                        "from": "STATS_" + join,
                        "localField": "_id",
                        "foreignField": "_id",
                        "as": join
                    }
                })

        # now we append the secondary queries
        if query["secondary"]:
            pipeline.append({ "$match": query["secondary"] })

        if skip or self.skip:
            pipeline.append({ "$skip": int(skip) + self.skip })

        if limit:
            pipeline.append({ "$limit": int(limit) })

        if self.sort:
            import ast
            sort = ast.literal_eval(self.sort)
            pipeline.append({ "$sort": sort })

        self.log.info("[DB = %s, Coll = %s]: %s" % (self.dbStatsName, collName, str(query["main"])))
        print("Pipeline: %s" % (str(pipeline)))

        cursor = coll.aggregate(pipeline)

        # now we setup an aggregation pipeline
        if strategy["stCorr"] < 1.0 or strategy["ltCorr"] < 1.0:
            return self.addToStrategy_CheckCorr(universeId, strategy, cursor, isReversion = isReversion)

        return self.addToStrategy(universeId, strategy, cursor, isReversion = isReversion)

    def checkLongShortTolerances(self, universeId, alphaId):
        if self.longShortRatioTol < 0.001 and self.longShortSumTol < 0.001:
            return True

        statsColl = self.dbStats["Stats"]
        assert statsColl, "Unable to load Stats collection!"

        # now we load the alpha stats
        alphaStats = statsColl.find_one({
            "_id": alphaId
        })

        if not alphaStats:
            self.log.debug("Alpha does not have full stats yet: %s" % (alphaId))
            return False

        assert alphaStats, "Unable to load alpha stats: %s" % (str(alphaId))

        if self.longShortRatioTol > 0.0:
            longCount = alphaStats["longCount"]
            shortCount = alphaStats["shortCount"]
            ratio = 1.0 - ((longCount - shortCount) / (longCount + shortCount))
            if ratio < self.longShortRatioTol:
                self.log.debug("L/S Ratio: %0.2f [%0.2f] - %s" % (ratio, self.longShortRatioTol, alphaId))
                return False

        if self.longShortSumTol > 0.0:
            import re
            universeCount = float(re.sub('[^0-9]','', universeId))
            longShortSum = alphaStats["longCount"] + alphaStats["shortCount"]
            ratio = longShortSum / universeCount
            if ratio < self.longShortSumTol:
                self.log.debug("L/S Sum: %0.2f [%0.2f / %0.2f - %0.2f] - %s" % (ratio, longShortSum, universeCount, self.longShortSumTol, alphaId))
                return False

        return True

    def isExcluded(self, alphaId):
        for ed in self.excludeDatasets:
            if ed.strip() and ed in alphaId:
                self.log.debug("Excluding alpha: %s - Exclude rule: %s" % (alphaId, ed))
                return True

        return False

    def addToStrategy(self, universeId, strategy, searchResults, isReversion):
        totalCount = 0

        for alphaInfo in searchResults:
            totalCount += 1
            alphaId = alphaInfo["_id"]

            if self.isExcluded(alphaId):
                continue

            if self.checkLongShortTolerances(universeId, alphaId) is False:
                continue

            alphaDoc = self.loadAlpha(alphaId)

            # Only add if the document has been loaded successfully
            if alphaDoc:
                alphaDoc["reversion"] = "true" if isReversion else "false"

                if self.getAlphaStats:
                    statsColl = self.dbStats["Stats"]
                    assert statsColl, "Unable to load Stats collection!"

                    # now we load the alpha stats
                    alphaStats = statsColl.find_one({ "_id": alphaId })

                    if not alphaStats:
                        self.log.debug("Alpha does not have full stats yet: %s" % (alphaId))
                        continue

                if alphaId not in self.ignoreAlphaIds:
                    if alphaId not in strategy["alphaIds"] and len(strategy["alphaDocs"]) < self.limit:
                        strategy["alphaIds"][alphaId] = True
                        strategy["alphaDocs"].append(alphaDoc)

                        if self.getAlphaStats is True:
                            strategy["alphaStats"].append(alphaStats)

        return totalCount

    def checkCorrelation(self, strategy, alphaId):
        strategyPoints = strategy["points"]

        # load this alpha
        statsColl = self.dbStats["Stats"]
        assert statsColl, "Unable to load Stats collection!"

        # now we load the alpha stats
        alphaStats = statsColl.find_one({
            "_id": alphaId
        })

        if not alphaStats:
            self.log.debug("Alpha does not have full stats yet: %s" % (alphaId))
            return False

        assert alphaStats, "Unable to load alpha stats: %s" % (str(alphaId))

        # If we don't have anything then just ride on with the existing alpha 
        # and just use that as a starting point
        if not strategyPoints or len(strategyPoints) is 0:
            strategy["points"] = alphaStats["vec"]
            strategy["accumPnls"] = []
            alphaStats = {
                "pnls": [],
                "dates": []
            }

            for i in range(len(strategy["points"])):
                pt = strategy["points"][i]
                idate = int(pt["date"])
                strategy["pointDateIndexes"][idate] = i
                alphaStats["pnls"].append(pt["pnl"])
                alphaStats["dates"].append(idate)
                strategy["accumPnls"].append(pt["pnl"])
                strategy["bookSize"] = pt["bookSize"]

            strategy["alphaStats"].append(alphaStats)

            return True

        # Now we check the short term and long term correlation
        alphaPoints = alphaStats["vec"]
        minIndex = -1
        maxIndex = -1

        alphaStats = {
            "pnls": [],
            "dates": []
        }

        alphaPnls = []
        strategyPnls = []

        if "vec" not in alphaStats:
            return True

        alphaPoints = alphaStats["vec"]
        stratBookSize = strategy["bookSize"]
        assert stratBookSize, "Invalid strategy with no book size!"

        for alphaPt in alphaPoints:
            idate = int(alphaPt["date"])
            if idate in strategy["pointDateIndexes"]:
                index = strategy["pointDateIndexes"][idate]
                alphaPnl = alphaPt["pnl"]
                alphaStats["pnls"].append(alphaPnl)
                alphaStats["dates"].append(idate)
                alphaBookSize = alphaPt["bookSize"]
                alphaPnl = (alphaPnl * stratBookSize) / alphaBookSize
                alphaPnls.append(alphaPnl)
                strategyPnls.append(strategy["accumPnls"][index])

                if minIndex < 0 or index < minIndex:
                    minIndex = index
                elif maxIndex < 0 or index > maxIndex:
                    maxIndex = index

        if maxIndex - minIndex < self.maxSTCorrDays:
            return False

        import numpy as np
        stCorr = np.corrcoef(alphaPnls[-self.maxSTCorrDays:], strategyPnls[-self.maxSTCorrDays:])[0][1]
        ltCorr = np.corrcoef(alphaPnls, strategyPnls[minIndex:maxIndex + 1])[0][1]

        if stCorr > strategy["stCorr"] or ltCorr > strategy["ltCorr"]:
#            self.log.debug("IGNORING: STCorr: %0.2f [%0.2f] - LTCorr: %0.2f [%0.2f] - Alpha: %s" % (stCorr, strategy["stCorr"], ltCorr, strategy["ltCorr"], alphaId))
            return False

        # add the PNL of the alpha to the strategy
        for i in range(minIndex, maxIndex + 1):
            strategy["accumPnls"][index] += alphaPnls[i - minIndex]

        return True

    def addToStrategy_CheckCorr(self, universeId, strategy, searchResults, isReversion):
        totalCount = 0

        for alphaInfo in searchResults:
            totalCount += 1
            alphaId = alphaInfo["_id"]

            if self.isExcluded(alphaId):
                continue

            if self.checkLongShortTolerances(universeId, alphaId) is False:
                continue

            alphaDoc = self.loadAlpha(alphaId)
            if alphaDoc is None:
                continue
            assert alphaDoc, "Unable to load alpha doc: %s" % (alphaId)

            alphaDoc["reversion"] = "true" if isReversion and alphaInfo["ir"] < 0.0 else "false"

            # If we have already checked this alpha before then there is no need to worry about it ...
            if alphaId in strategy["alphaIds"]:
                continue

            if alphaId in self.ignoreAlphaIds:
                if self.ignoreAlphaIds[alphaId] >= 5:
                    continue

            strategy["alphaIds"][alphaId] = True

            # Now we check the correlation of this strategy
            if self.checkCorrelation(strategy, alphaId):
                strategy["alphaDocs"].append(alphaDoc)
                if len(strategy["alphaDocs"]) >= self.limit:
                    break

        return totalCount


    def runSearch(self, universeId, market, collName, qdetails, strategy):
        try:
            alphas          = []
            uniqueAlphas    = {}
            forwardQuery    = qdetails["forwardQuery"]
            reversionQuery  = qdetails["reversionQuery"]
            skip            = qdetails["skip"]
            limit           = qdetails["limit"]
            joins           = qdetails["joins"]

#            print("Forward query: %s" % (str(forwardQuery)))
#            print("Reversion query: %s" % (str(reversionQuery)))

            numExistingAlphas = len(strategy["alphaDocs"])
            nresultCount = self.runQuery(universeId = universeId, market = market, collName = collName, query = forwardQuery, skip = skip, limit = limit, strategy = strategy, joins = joins, isReversion = False)
            numNormalAlphas = len(strategy["alphaDocs"]) - numExistingAlphas

            numExistingAlphas = len(strategy["alphaDocs"])
            if reversionQuery:
                rresultCount = self.runQuery(universeId = universeId, market = market, collName = collName, query = reversionQuery, skip = skip, limit = limit, strategy = strategy, joins = joins, isReversion = True)
                numReversionAlphas = len(strategy["alphaDocs"]) - numExistingAlphas
            else:
                rresultCount = 0
                numReversionAlphas = 0

            self.log.info("Num normal alphas: %d [n-count: %d]" % (numNormalAlphas, nresultCount))
            self.log.info("Num reversion alphas: %d [r-count: %d]" % (numReversionAlphas, rresultCount))

            # next time we'll search the next batch of results
            qdetails["skip"] += limit

            if limit and (nresultCount < limit and rresultCount < limit):
                self.log.info("Limit reached: nc: %d - rc: %d [limit: %d]" % (nresultCount, rresultCount, limit))
                qdetails["canSearch"] = False

            return alphas
                
        except Exception as e:
            self.log.exception(e)
            print(e)
            sys.exit(1)

        return []

    def loadAlpha(self, alphaId):
        # self.log.info("Loading alpha id: %s" % (alphaId))

        alphasColl = self.db["AlphaConfigs"]
        assert alphasColl, "Unable to load AlphasConfig from DB: %s" % (self.dbName)

        # Now we load the record
        alphaDoc = alphasColl.find_one({ "_id": alphaId })
        # assert alphaDoc, "Unable to load document: %s [DB = %s]" % (alphaId, self.dbName)

        return alphaDoc

    def docToXml(self, parentXml, strategy, doc):
        name = doc["m_name"]
        if not name:
            name = "Alpha"

        attribs = {}

        if "map" in doc: 
            for key in doc["map"]:
                value = doc["map"][key]
                attribs[key] = value

        if "_id" in doc:
            attribs["dbId"] = doc["_id"]
            attribs["uuid"] = str(doc["iid"])

        if "universeId" in doc:
            attribs["universeId"] = doc["universeId"]

        if "market" in doc:
            attribs["market"] = doc["market"]

        if "creationDate" in doc:
            attribs["creationDate"] = str(dateToInt(doc["creationDate"]))

        xml = ET.SubElement(parentXml, name, attrib = attribs)

        if "children" in doc and len(doc["children"]): 
            for child in doc["children"]:
                self.docToXml(xml, strategy, child)

        return xml

    def connectDB(self):
        from pymongo import MongoClient
        
        if self.db is not None:
            return
            
        self.mongoClient = MongoClient(self.dbPath)
        if self.mongoClient is None:
            sys.exit(1)
        
        self.db = self.mongoClient[self.dbName]
        if self.db is None:
            sys.exit(1)
        
        self.dbStats = self.mongoClient[self.dbStatsName]
        if self.dbStats is None:
            sys.exit(1)
        

if __name__ == '__main__':
    strat = PStrat()
    strat.run()
