### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#!python

import os
import sys
import traceback
import time

import datetime as DT
import multiprocessing

from pstrat_settings import Settings
from collections import OrderedDict

import xml.etree.ElementTree as ET
from xml.etree.ElementTree import Comment
from xml.dom import minidom

from pstrat import PStrat
import sutil

from random import randint

# Add all the subdirectories to the search path ...
class PStratMulti:
    def __init__(self, settings = None):

        if settings is None:
            self.settings       = Settings(self)
            self.settings.parseArguments()
            self.settings.initLogging()
        else:
            self.settings       = settings

        opt                     = self.settings.options
        self.log                = self.settings.log
        self.basePath           = os.path.dirname(__file__)
        self.queries            = sutil.loadNewLineFile(os.path.join(self.basePath, "queries.txt"))
        # self.variations         = sutil.loadNewLineFile(os.path.join(self.basePath, "variations_rhs.txt"))
        self.universeId         = opt.universeId

        self.baseQueries        = opt.query.split("|") if opt.query else []
        self.baseQueryIndex     = 0

        self.maxAlphasPerQuery  = max(int(opt.maxAlphasPerQuery), 5)

        self.maxStrats          = int(opt.maxStrats)
        self.alphaLimit         = int(opt.limit)

        self.prefix             = opt.prefix
        self.pathTarget         = opt.pathTarget

        self.usedAlphas         = {}
        self.strategies         = []

        self.startIndex         = int(opt.startIndex)

    def makeRandomQuery(self):
        numBaseQueries          = len(self.baseQueries)
        queryId                 = randint(0, len(self.queries) - 1)

        if numBaseQueries:
            baseQuery           = self.baseQueries[self.baseQueryIndex]
            self.baseQueryIndex += 1
            if self.baseQueryIndex >= numBaseQueries:
                self.baseQueryIndex = 0

            return baseQuery + " && " + self.queries[queryId]

        return self.queries[queryId]
        # variationId1            = randint(0, len(self.variations) - 1)

        # queryId2                = randint(0, len(self.queries) - 1)
        # variationId2            = randint(0, len(self.variations) - 1)

        # query1                  = self.queries[queryId1].replace("$$", self.variations[variationId1])
        # query2                  = self.queries[queryId2].replace("$$", self.variations[variationId2])

#        return query1 + " && " + query2
        # return "LT.ir > 0.025 and LT.tvr < 30.0 && _2014.ir > 0.025 and _2015.ir > 0.025 and _2016.ir > 0.025" #+ query1
 

    def run(self):
        maxIterWithoutAlpha     = 5

        for i in range(self.maxStrats):
            strategy            = None
            numAlphas           = 0
            numIterWithoutAlpha = 0
            queriesStr          = ""

            while True:
                queryStr        = self.makeRandomQuery()
                self.log.debug("Running query: %s" % (queryStr))

                pstrat          = PStrat(settings = self.settings, limit = self.maxAlphasPerQuery, strategy = strategy, ignoreAlphaIds = self.usedAlphas, 
                                         queryStr = queryStr, comment = "", outputFilename = "")

                pstrat.maxQueryIter = 20
                strategy        = pstrat.run()

                if strategy is None:
                    continue

                preAlphaCount   = numAlphas
                numAlphas       = len(strategy["alphaIds"])

                if preAlphaCount == numAlphas:
                    self.log.info("No alphas for query: %s", queryStr)
                    numIterWithoutAlpha += 1
                else:
                    if queriesStr:
                        queriesStr += ", "

                    numIterWithoutAlpha = 0
                    queriesStr += str.format("%s (%d)" % (queryStr, numAlphas - preAlphaCount))
                    strategy["queriesStr"] = queriesStr

                    for alphaId in strategy["alphaIds"]:
                        if alphaId not in self.usedAlphas:
                            self.usedAlphas[alphaId] = 1
                        else:
                            self.usedAlphas[alphaId] += 1
                            
                    # self.usedAlphas.update(strategy["alphaIds"])

                if numIterWithoutAlpha >= maxIterWithoutAlpha:
                    self.log.info("Max iterations without alpha breached: %d [Max = %d]" % (numIterWithoutAlpha, maxIterWithoutAlpha))
                    break
                elif len(strategy["alphaDocs"]) > self.alphaLimit:
                    self.log.info("Alpha limit reached: %d [Max = %d]" % (len(strategy["alphaDocs"]), self.alphaLimit))
                    break

            if strategy and numAlphas:
                self.strategies.append(strategy)

                filename = "./" + self.universeId + "_" + str(i + self.startIndex) + ".xml"

                if self.pathTarget and self.prefix:
                    dirname = os.path.join(self.pathTarget, self.prefix + "_" + str(i + self.startIndex).zfill(2))
                    try:
                        os.mkdir(dirname)
                    except Exception as e:
                        pass

                    filename = os.path.join(dirname, "config.xml")

                file = open(filename, "w")

                self.log.info("Total alphas: %d" % (len(strategy["alphaDocs"])))

                comment = str.format('''

Copyright hkhan team, All rights reverved! Please read the terms and conditions.

Created by    : Multi-Strat [Zulfiqar Malik]
Creation Date : %s
Universe      : %s
Query         : %s

                ''' % (DT.datetime.utcnow().strftime("%a %b %d %H:%M:%S %Z %Y"), self.universeId, strategy["queriesStr"]))

                strategy["refXml"].insert(0, Comment(comment))

                xmlString = ET.tostring(strategy["refXml"], 'utf-8')
                reparsed = minidom.parseString(xmlString)
                xmlStr = '\n'.join([line for line in reparsed.toprettyxml(indent=' ' * 4).split('\n') if line.strip()])
                file.write(xmlStr)
                file.close()

if __name__ == '__main__':
    strat = PStratMulti()
    strat.run()
