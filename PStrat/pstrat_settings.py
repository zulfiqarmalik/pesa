### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys
import logging
from optparse import OptionParser
# from log_util import RainbowLoggingHandler

def getLocalIPAddress():
    import socket
    return socket.gethostbyname(socket.gethostname())

class Settings:
    def __init__(self, app):
        self.app        = app
        self.basePath   = os.path.realpath(__file__)
        self.options    = None
        self.args       = None
        self.argv       = sys.argv
        self.platform   = "linux"
        self.baseDir    = os.path.join(os.path.expanduser('~'), "drv")
        self.logDir     = "./"
        
        if sys.platform == "win32" or sys.platform == "win64":
            self.platform= "windows"
            self.baseDir = "Y:\\"
        
    def initLogging(self):
        # setup `logging` module
        logger = logging.getLogger("pstrat")

        # console logging
        formatter = logging.Formatter("[%(asctime)s:%(name)s]: %(message)s ")  # same as default

        self.logDir = self.baseDir
        self.logFilename = os.path.join(self.logDir, "pstrat.log")

        # file logging 
        fileHandler = logging.FileHandler(self.logFilename, "w")
        fileHandler.setFormatter(formatter) 

        consoleHandler = logging.StreamHandler()
        consoleHandler.setFormatter(formatter)

        logger.addHandler(consoleHandler)
        logger.addHandler(fileHandler) 

        logger.setLevel(logging.INFO)

        self.log = logger

        if self.options.logLevel is "debug":
            self.log.setLevel(logging.DEBUG)
        elif self.options.logLevel is "info":
            self.log.setLevel(logging.INFO)
        elif self.options.logLevel is "warn":
            self.log.setLevel(logging.WARNING)
        else:
            raise RuntimeError("Invalid logging option: " + logger.logLevel)

    def parseArguments(self):
        parser = OptionParser()

        parser.add_option("-a", "--shortTermAlphaCorr", dest = "shortTermAlphaCorr", default = 1.0, help = "Short term correlation threshold of the selected alpha against ALL the other alphas in the strategy so far")
        parser.add_option("-A", "--longTermAlphaCorr", dest = "longTermAlphaCorr", default = 1.0, help = "Short term correlation threshold of the selected alpha against ALL the other alphas in the strategy so far")

        parser.add_option("-B", "--searchBatchSize", dest = "searchBatchSize", default = 100, help = "The search batch size")
        parser.add_option("-b", "--batchId", dest = "batchId", default = None, help = "The gbatchId to get the data for")
        parser.add_option("-c", "--config", dest = "config", default = "../Configs/US/StratMaker/us_prod.xml", help = "Path to the base config used for construction of the strategy", metavar = "FILE")
        parser.add_option("-d", "--dbPath", dest = "dbPath", default = "mongodb://10.240.1.70:27018/?replicaSet=psim", help = "The path to the database server")
        parser.add_option("-D", "--dbName", dest = "dbName", default = "pesa", help = "The name of the database to connect to")
        parser.add_option("-E", "--exclude", dest = "excludeDatasets", default = "", help = "The datasets to exclude")

        parser.add_option("-G", "--GA", dest = "GA", default = "", help = "GA parameter string")

        parser.add_option("-I", "--invalidThreshold", dest = "invalidThreshold", default = 5, help = "Invalid pnl count threshold. Anything above this will be ignored ...")
        parser.add_option("-i", "--startIndex", dest = "startIndex", default = 0, help = "Multi strat - the starting index")

        parser.add_option("-k", "--shortTermCorr", dest = "shortTermCorr", default = 1.0, help = "Short term correlation threshold. This is the correlation of selected alpha against the running STRATEGY")
        parser.add_option("-K", "--longTermCorr", dest = "longTermCorr", default = 1.0, help = "Long term correlation threshold. This is the correlation of selected alpha against the running STRATEGY")

        parser.add_option("-l", "--limit", dest = "limit", default = 0, help = "Limit the number of alphas")
        parser.add_option("-L", "--logLevel", dest = "logLevel", default = "debug", help = "What log level to set. Choose from [debug, info, warn]")

        parser.add_option("-M", "--market", dest = "market", default = "US", help = "The market for which we're making this strategy")
        parser.add_option("-m", "--maxStrats", dest = "maxStrats", default = 500, help = "How many max strategies to make (pstrat_multi only!)")
        
        parser.add_option("-O", "--output", dest = "output", default = "./strategy", help = "Path to the generated strategy", metavar = "FILE")
        
        parser.add_option("-p", "--prefix", dest = "prefix", default = None, help = "Multi Strat: The prefix for the strategy names")
        parser.add_option("-P", "--pathTarget", dest = "pathTarget", default = None, help = "Multi Strat: The target where to create directories")

        parser.add_option("-q", "--maxAlphasPerQuery", dest = "maxAlphasPerQuery", default = 10, help = "Multi Strat: Max alphas pery query")
        parser.add_option("-Q", "--query", dest = "query", default = "ir > 0.075", help = "Forward query")
        parser.add_option("-R", "--reversion", dest = "reversionQuery", default = "", help = "Reversion query")
        parser.add_option("-s", "--sort", dest = "sort", default = None, help = "What to sort by")
        parser.add_option("-S", "--skip", dest = "skip", default = 0, help = "How many results to skip initially (at the very least these many will be skipped)")

        parser.add_option("-t", "--longShortRatioTolerance", dest = "longShortRatioTolerance", default = 0.0, help = "The ratio of long to short tolerance (longCount / shortCount). This is ALWAYS done on the LIFETIME Stats object")
        parser.add_option("-T", "--longShortSumTolerance", dest = "longShortSumTolerance", default = 0.0, help = "The sum of longs and shorts should be within this much tolerance of the universe size. The universe size is deduced from the name of the universe. This is ALWAYS done on the LIFETIME Stats object")
        
        parser.add_option("-U", "--universe", dest = "universeId", default = "Top100", help = "The universe for which we're making this strategy")
        parser.add_option("-Z", "--author", dest = "author", default = "Zulfiqar Malik", help = "The author of this particular config")

        (self.options, self.args) = parser.parse_args()
        

