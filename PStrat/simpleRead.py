### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
plt.ioff()
import numpy as np
import sys

def ir(x):
    return np.mean(x) / np.std(x)

def fformat(y):
    return '{:.5}'.format(y)

def oldtstat(cump):
    n = len(cump)
    a = sum(i * x for i, x in enumerate(cump)) / n
    a /= (n - 1) * (2 * n - 1) / 6
    nom = np.sum( (cump[i] - a * i) ** 2 for i in range(n) ) / (n - 1)
    denom = (n - 1) * n * (2 * n - 1) / 6
    s = np.sqrt(nom / denom)
    return a / s;

def tstat(cump):
    n = len(cump)
    x = list(range(n))
    y = cump
    covs = np.cov(x, y)
    a = covs[0][1] / covs[0][0]
    b = np.mean(y) - a * np.mean(x)
    nom = np.sum( (y[i] - a * x[i] - b) ** 2 for i in range(n) ) / (n - 2)
    denom = covs[0][0] * n
    s = np.sqrt(nom / denom)
    return a / s;

class PnlStats(object):
    def __init__(self, cumpnl, pnl, bps, tvr):
        self.cumpnl = cumpnl
        self.pnl = pnl
        self.bps = bps
        self.tvr = tvr
    def print(self):
        print( 'SHARPE:', fformat(ir(self.pnl) * np.sqrt(252)), end = '   ')
        print( 'RET:', fformat(np.mean(self.bps) * 252 / 100), end = '   ')
        freq = 63
        c = [ self.cumpnl[freq * i] for i in range(len(self.cumpnl) // freq) ]
        print( 'TSTAT:', fformat(tstat(c)), end = '   ')
        print( 'TVR:', fformat(np.mean(self.tvr)), end = '   ')
        print( 'MARGIN:', fformat(100 * np.mean(self.bps) / np.mean(self.tvr)), end = '   ')
        withcost = np.array(self.bps) - 2 * np.array(self.tvr) / 100
        print( 'SHARPE2BPS:', fformat(ir(withcost) * np.sqrt(252)), end = '   ')
        print('') 
    def plot(self):
        plt.figure(figsize = (4, 2))
        plt.plot(range(0, len(self.cumpnl)), self.cumpnl)
        plt.savefig('PNL.png')
        
def read_pnl(fname):
    cumpnl = []
    pnl = []
    bps = []
    tvr = []
    with open(fname, 'r') as infile:
        lines = infile.readlines()
        lines = lines[1:]	
        for line in lines:
            l = line.split(',')
            if len(l) < 4:
                break
            date = int(l[0].strip())
            if (date // 10000 == 2017):
                continue
            cumpnl.append(float(l[2].strip()))
            pnl.append(float(l[3].strip()))
            bps.append(float(l[4].strip()))
            tvr.append(float(l[5].strip()))
    return PnlStats(cumpnl, pnl, bps, tvr)

name = sys.argv[1]
s = read_pnl(name)
s.print()       
s.plot()
