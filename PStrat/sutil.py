### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import sys
import datetime as DT
import ctypes
import numpy as np

def ivf_none(value):
    return value

def ivf_sub(value):
    return 1.0 - value

def ivf_negate(value):
    return value * -1.0

def ivf_div(value):
    return 1.0 / value

def invertKeyValue(key, value, invertValueFunc):
    if key == "$lt":
        return {"$gt": invertValueFunc(value)}
    elif key == "$lte":
        return {"$gte": invertValueFunc(value)}
    elif key == "$gt":
        return {"$lt": invertValueFunc(value)}
    elif key == "$gte":
        return {"$lte": invertValueFunc(value)}
    d = {}
    d[key] = invertValueFunc(value)
    return d

def invertItem(orgKey, key, value):
    if orgKey == "ir" or orgKey == "marginBps" or orgKey == "marginCps":
        return orgKey, invertKeyValue(key, value, ivf_negate)
    elif orgKey == "hitRatio":
        return orgKey, invertKeyValue(key, value, ivf_sub)
    elif orgKey == "maxDD":
        return "maxDU", invertKeyValue(key, value, ivf_negate)
    elif orgKey == "maxDH":
        return "maxDUH", invertKeyValue(key, value, ivf_none)
    elif orgKey == "tvr" or orgKey == "maxTvr":
        d = {}
        d[key] = value
        return orgKey, d

    if orgKey:
        d = {}
        d[key] = value
        return orgKey, d

    return key, value

def invertList(ilist, rlist):
    for item in ilist:
        if isinstance(item, list):
            ritem = invertList(item, [])
            rlist.append(ritem)
        elif isinstance(item, dict):
            ritem = invertDict(item, "", {})
            rlist.append(ritem)
        else:
            rlist.append(item)

    return rlist

def invertDict(idict, orgKey, rdict):
    for key, value in idict.items():
        if isinstance(value, list):
            rvalue = invertList(value, [])
            rdict[key] = rvalue
        elif isinstance(value, dict):
            rkey, rvalue = invertDict(value, key, rdict)
            # if rkey is None:
            #     rdict = rvalue
            # else:
            #     rdict[rkey] = rvalue
        else:
            parts = orgKey.split(".")
            rkey, rvalue = invertItem(parts[-1], key, value)
            parts[-1] = rkey
            rkey = ".".join(parts)
            rdict[rkey] = rvalue

    if orgKey:
        return None, rdict
    return rdict

def invertQuery(query):
    return invertDict(query, "", {})

def loadNewLineFile(filename, ignoreEmpty = True, comment = "#"):
    lines = []
    file = open(filename, "r")

    rlines = file.readlines()

    for line in rlines:
        line = line.strip()
        if comment and line.startswith(comment):
            continue

        if not line and ignoreEmpty:
            continue

        lines.append(line)

    return lines



