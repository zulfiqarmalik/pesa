//////////////////////////////////////////////////////
/// App.cpp
///
/// Created on: 31 Mar 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "App.h"
#include "Poco/SplitterChannel.h"
#include "Poco/ConsoleChannel.h"
#include "Poco/SimpleFileChannel.h"
#include "Poco/AutoPtr.h"
#include "Poco/File.h"

#include "Lang/Lexer.h"

#include "Lib/NamedVariable.h"
#include "Config/Config.h"
#include "Helper/ShLib.h"

#include "Simulator/Simulator.h"

#include "Data/MatrixData.h"

#include <stdio.h>
#include <sstream>
#include <iostream>
#include <string>
#include <exception>
#include <inttypes.h>
#include <cstdlib>
#include <signal.h>

namespace pesa {
	using namespace Poco;

	App::App()
		: m_args(*this) {
	}

	App::~App() {
	}

	void App::defineOptions(OptionSet& options) {
		Application::defineOptions(options);
		m_args.define(options);
	}

	int App::runSim(const std::string& configFilename) {
		Simulator sim({});
		return sim.run(configFilename);
	}

	int App::run() {
		try {
			// std::string str = "func(p0, p1) / func2(p2, p3);";
			// Lexer lex;
			// lex.loadMemory(str.c_str(), str.length(), "test", 0);
			// Token tok;
			// int numTokens = 0;

			// while (lex.readToken(&tok) != 0) {
			// 	std::cout << "Token-" << numTokens++ << ": " << tok.type() << " (" << tok.subType() << ")" << " = " << tok << std::endl;
			// }

			// return 0;

			if (m_args.helpRequested()) {
				m_args.showHelp();
				return Application::EXIT_OK;
			}

//			NAMED_FLOAT(myVar) = 10.0f;
//
//			std::cout << "Name: " << myVar.name() << std::endl;
//			std::cout << "Value: " << myVar.value() << std::endl;

			Info("APP", "Preparing the application for run ...");

			/// remove the old log file first ...
			const std::string& logFilename = m_args.logFilename();

			if (!logFilename.empty()) {
				Poco::File file(logFilename);
				if (file.exists())
					file.remove();
			}

			AutoPtr<ConsoleChannel> consoleChannel(new ConsoleChannel);
			AutoPtr<SimpleFileChannel> fileChannel(new SimpleFileChannel(m_args.logFilename()));
			AutoPtr<SplitterChannel> splitterChannel(new SplitterChannel);

			splitterChannel->addChannel(consoleChannel);
			splitterChannel->addChannel(fileChannel);
			Logger::root().setChannel(splitterChannel);

			Info("APP", "Setting Log Level: %s", m_args.logLevel());
			Logger::root().setLevel(m_args.translateLogLevel());

			if (!m_args.config().empty())
				return runSim(m_args.config());

//			auto parts = Util::regexSplit("\"MDVN\",\"Medivation, Inc.\",\"47.09\",\"$7.73B\",\"n/a\",\"Health Care\",\"Major Pharmaceuticals\",\"http://www.nasdaq.com/symbol/mdvn\"", "\"([^\"]*)\"");
//			for (auto part : parts)
//				std::cerr << part << std::endl;

//			Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> fmat(3, 2);
//			fmat(0, 0) = 1;
//			fmat(0, 1) = 2;
//			fmat(2, 1) = 3;
//
//			std::cout << "Rows: " << fmat.rows() << std::endl;
//			std::cout << "Cols: " << fmat.cols() << std::endl;
//			std::cout << "Data: " << fmat(2, 1) << std::endl;

		}
		catch (const Poco::Exception& e) {
			Util::reportException(e);
		}
		catch (const std::exception& e) {
			Util::reportException(e);
		}
		catch (...) {
			Error("APP", "Unknown exception!");
		}

		return Application::EXIT_OK;
	}

} /// namespace pesa 
