//////////////////////////////////////////////////////
/// App.h
///
/// Created on: 31 Mar 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#ifndef PESA_APP_H_
#define PESA_APP_H_

#include <vector>
#include "Lib/Util.h"
#include "Lib/Application.h"
#include "Args.h"

namespace pesa {

	using namespace std;

	class App : public pesa::Application {
	private:
		Args 						m_args;

		int 						runSim(const std::string& configFilename);

	public:
									App();
		virtual 					~App();

		////////////////////////////////////////////////////////////////////////////
		/// Overridden methods
		////////////////////////////////////////////////////////////////////////////
		virtual void 				defineOptions(OptionSet& options);
		virtual int 				run();
	};

} /// namespace pesa 

#endif /// PESA_APP_H_
