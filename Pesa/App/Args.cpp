//////////////////////////////////////////////////////
/// Args.cpp
///
/// Created on: 30 Mar 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Args.h"
#include "App.h"

#include <iostream>

#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Util/HelpFormatter.h"

namespace pesa {

	Args::Args(App& app)
		: m_app(app)
		, m_logLevel("debug")
		, m_logFilename("app.log") {
	}

	Args::~Args() {
	}

	void Args::showHelp() {
		HelpFormatter helpFormatter(m_app.options());
		helpFormatter.setCommand(m_app.commandName());
		helpFormatter.setUsage("OPTIONS");
		helpFormatter.setHeader("Pesa simulator.");
		helpFormatter.format(std::cout);
	}

	void Args::define(OptionSet& options) {
		options.addOption(
			Option("help", "h", "Display help information on command line arguments")
				.required(false)
				.repeatable(false)
				.callback(OptionCallback<Args>(this, &Args::handleHelp)));

		options.addOption(
			Option("config", "c", "Configuration file to load for the simulator")
				.required(false)
				.repeatable(false)
				.argument("value")
				.callback(OptionCallback<Args>(this, &Args::handleConfig)));

		options.addOption(
			Option("loglevel", "L", "What log level to use. Valid values are: trace, debug, info, error. Default is debug")
				.required(false)
				.repeatable(false)
				.argument("value")
				.callback(OptionCallback<Args>(this, &Args::handleLogLevel)));

		options.addOption(
			Option("logfile", "F", "Where to dump the log file. Default is app.log")
				.required(false)
				.repeatable(false)
				.argument("value")
				.callback(OptionCallback<Args>(this, &Args::handleLogFilename)));

//		options.addOption(
//			Option("bind", "b", "bind option value to test.property")
//				.required(false)
//				.repeatable(false)
//				.argument("value")
//				.binding("test.property"));

	}

	int Args::translateLogLevel(const std::string& logLevel) {
		if (logLevel == "trace")
			return Poco::Message::PRIO_TRACE;
		else if (logLevel == "debug")
			return Poco::Message::PRIO_DEBUG;
		else if (logLevel == "info")
			return Poco::Message::PRIO_INFORMATION;
		else if (logLevel == "error")
			return Poco::Message::PRIO_ERROR;

		return Poco::Message::PRIO_DEBUG;
	}

	void Args::handleHelp(const std::string&, const std::string&) {
		m_helpRequested = true;
	}

	void Args::handleConfig(const std::string&, const std::string& value) {
		m_config = value;
		ASSERT(!m_config.empty(), "Cannot have an empty config name!");
	}

	void Args::handleLogLevel(const std::string&, const std::string& value) {
		m_logLevel = value;
		ASSERT(!m_logLevel.empty(), "Cannot have an empty logging level!");
	}

	void Args::handleLogFilename(const std::string&, const std::string& value) {
		m_logFilename = value;
		ASSERT(!m_logFilename.empty(), "Cannot have an empty log filename!");
	}

} /// namespace pesa 
