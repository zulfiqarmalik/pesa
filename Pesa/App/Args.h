//////////////////////////////////////////////////////
/// Args.h
///
/// Created on: 30 Mar 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#ifndef PESA_ARGS_H_
#define PESA_ARGS_H_

#include <Poco/Util/OptionSet.h>
#include "Util.h"

namespace pesa {

	class App;
	using namespace Poco::Util;

	class Args {
	private:
		App& 					m_app;				/// The application instance
		bool 					m_helpRequested;	/// Has help been requested
		std::string 			m_config;			/// Path to the config that we are going to run
		std::string 			m_logLevel; 		/// What is the log level
		std::string 			m_logFilename; 		/// The name of the log file

		static int 				translateLogLevel(const std::string& logLevel);

	public:
								Args(App& app);
		virtual 				~Args();

		void 					define(OptionSet& options);
		void 					process();
		void 					showHelp();

		void 					handleHelp(const std::string& name, const std::string& value);
		void 					handleConfig(const std::string& name, const std::string& value);
		void 					handleLogLevel(const std::string& name, const std::string& value);
		void 					handleLogFilename(const std::string& name, const std::string& value);

		////////////////////////////////////////////////////////////////////////////
		/// Inline methods
		////////////////////////////////////////////////////////////////////////////
		inline bool 			helpRequested() const 			{ return m_helpRequested; 		}
		inline std::string 		config() const 					{ return m_config; 				}
		inline std::string 		logLevel() const 				{ return m_logLevel;			}
		inline std::string 		logFilename() const 			{ return m_logFilename;			}
		inline int 				translateLogLevel() const 		{ return Args::translateLogLevel(m_logLevel); }
	};

} /// namespace pesa 

#endif /// PESA_ARGS_H_
