//////////////////////////////////////////////////////
/// App.cpp
///
/// Created on: 31 Mar 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Simulator.h"
#include "Components/IComponent.h"
#include "Components/IAlpha.h"
#include "Components/IDataLoader.h"
#include "Components/IUniverse.h"
#include "Pipeline/IPipelineComponent.h"

#include "Lib/Util.h"
#include "Helper/ShLib.h"

namespace pesa {

	////////////////////////////////////////////////////////////////////////////
	/// Static functions
	////////////////////////////////////////////////////////////////////////////

	IDataRegistry* Simulator::dataRegistry() {
		return m_rt.dataRegistry;
	}

	const Config* Simulator::config() const {
		return &m_config;
	}

	const RuntimeData& Simulator::runtimeData() const {
		return m_rt;
	}

	IPipelineComponentPtr Simulator::component(const std::string& id) {
		for (auto pcomp : m_pipeline) {
			auto config = (const cfg::Module&)pcomp->config();
			if (config.id() == id)
				return pcomp;
		}

		return nullptr;
	}

	////////////////////////////////////////////////////////////////////////////
	/// Member functions
	////////////////////////////////////////////////////////////////////////////
	Simulator::Simulator(Simulator::Options options)
		: m_options(options) {
	}

	Simulator::~Simulator() {
	}

	void Simulator::init() {
		initPipeline(m_config, m_pipeline);
	}

	void Simulator::free() {
		m_pipeline.clear();
	}

	void Simulator::initPipeline(const Config& config, IPipelineComponentPtrVec& pipeline) {
		/// Load all the pipeline components
		Info("SIM", "%s", m_config.pipeline().toString(true));

		auto& modules = config.pipeline().modules();

		for (auto* module : modules) {
			IPipelineComponentPtr pcomp;

			if (module->id() == "DataRegistry") {
				IDataRegistryPtr dataRegistry = helper::ShLib::global().createPipelineComponent<IDataRegistry>(*module, nullptr);
				m_rt.dataRegistry = dataRegistry.get();
				pcomp = dataRegistry;
			}
			else
				pcomp = helper::ShLib::global().createPipelineComponent<IPipelineComponent>(*module, nullptr);

			pcomp->pipelineHandler() = this;

			ASSERT(pcomp, "Unable to create pipeline component: " << module->toString());
			pipeline.push_back(pcomp);
		}
	}

	void Simulator::exec(RuntimeData& rt, const IPipelineComponentPtrVec& pipeline) {
		Info("SIM", "Executing pipeline. Num components: %z", pipeline.size());

		for (auto pcomp : pipeline)
			pcomp->exec(rt);
	}

	int Simulator::run(const Config& config) {
		try {
			init();
			exec(m_rt, m_pipeline);
			free();
		}
		catch (const Poco::Exception& e) {
			Util::reportException(e);
		}
		catch (const std::exception& e) {
			Util::reportException(e);
		}

		return 0;
	}

	int Simulator::run(const std::string& configFilename) {
		Debug("SIM", "Loading config: %s ...", configFilename);
		Config::parseXml(configFilename, m_config);
		Debug("SIM", "Config loaded successfully!");

		m_config.validate();

		return run(m_config);
	}

} /// namespace pesa 
