//////////////////////////////////////////////////////
/// App.h
///
/// Created on: 31 Mar 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#ifndef PESA_SIMULATOR_H_
#define PESA_SIMULATOR_H_

#include <iostream>
#include <string>
#include "Config/Config.h"
#include "Pipeline/IDataRegistry.h"
#include "Pipeline/IPipelineHandler.h"

namespace pesa {

	class IComponent;
	class IUniverse;

	class Simulator : public IPipelineHandler {
	public:
		struct Options {
			bool 					noSimRun = false;
		};

	private:
		typedef std::vector<IComponent*> ComponentPVec;
		typedef std::vector<IPipelineComponentPtr> IPipelineComponentPtrVec;

		Config 						m_config; 					/// The engine config
		Simulator::Options 			m_options; 					/// The engine options
		RuntimeData 				m_rt; 						/// Runtime data
		IPipelineComponentPtrVec	m_pipeline;					/// Load the pipeline

		void 						init();
		void 						free();
		void 						initPipeline(const Config& config, IPipelineComponentPtrVec& pipeline);

		void 						exec(RuntimeData& rt, const IPipelineComponentPtrVec& pipeline);

		////////////////////////////////////////////////////////////////////////////
		/// Static functions
		////////////////////////////////////////////////////////////////////////////

	public:
									Simulator(Simulator::Options options);
									~Simulator();

		int 						run(const Config& config);
		int 						run(const std::string& configFilename);

		////////////////////////////////////////////////////////////////////////////
		/// IPipelineHandler implementation
		////////////////////////////////////////////////////////////////////////////
		virtual IDataRegistry* 		dataRegistry();
		virtual const Config* 		config() const;
		virtual const RuntimeData& 	runtimeData() const;
		virtual IPipelineComponentPtr component(const std::string& id);

		////////////////////////////////////////////////////////////////////////////
		/// Inline functions
		////////////////////////////////////////////////////////////////////////////
		inline const IDataRegistry* dataRegistry() const	{ return m_rt.dataRegistry; 	}
//		inline const IUniverse* 	universe() const 		{ return m_rt.universe; 		}
	};

} /// namespace pesa 

#endif /// PESA_SIMULATOR_H_
