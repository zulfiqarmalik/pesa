/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// CSI_FuturesContracts.cpp
///
/// Created on: 18 Jun 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Framework/Components/IDataLoader.h"

#include "Framework/Data/FuturesContract.h"
#include "Framework/Helper/FileDataLoader.h"

#include "Poco/String.h"
#include "Poco/Glob.h"

namespace pesa {
namespace fut {
	////////////////////////////////////////////////////////////////////////////
	/// Gets all the futures contracts
	////////////////////////////////////////////////////////////////////////////
	class CSI_FuturesContracts : public IDataLoader {
	private:
		typedef std::unordered_map<std::string, Contract> ContractMap;

		ContractMap				m_existingContracts;		/// Contracts that already exist and don't need to be re-loaded

		void 					buildContracts(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		void					buildContractsFor(IDataRegistry& dr, const DataCacheInfo& dci, ActiveContractsData* allContracts, Security::Index ii);
		void					loadContract(const RuntimeData& rt, const std::string& filename, Contract& contract, bool isZip);

	public:
								CSI_FuturesContracts(const ConfigSection& config);
		virtual 				~CSI_FuturesContracts();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			preDataBuild(IDataRegistry& dr, const pesa::DataCacheInfo& dci);
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
	};

	CSI_FuturesContracts::CSI_FuturesContracts(const ConfigSection& config) 
		: IDataLoader(config, "CSI_FuturesContracts") {
	}

	CSI_FuturesContracts::~CSI_FuturesContracts() {
		CTrace("Deleting CSI_FuturesContracts!");
	}

	void CSI_FuturesContracts::initDatasets(IDataRegistry& dr, Datasets& dss) {
		auto datasetName = config().getRequired("datasetName");
		Dataset ds(datasetName);

		ds.add(DataValue("allContracts", DataType::kCustom, 0, THIS_DATA_FUNC(CSI_FuturesContracts::buildContracts), DataFrequency::kStatic, 
			DataFlags::kUpdatesInOneGo | DataFlags::kAlwaysOverwrite, nullptr, ActiveContractsData::typeName().c_str()));

		dss.add(ds);
	}

	void CSI_FuturesContracts::preDataBuild(IDataRegistry& dr, const pesa::DataCacheInfo& dci) {
		auto datasetName = config().getRequired("datasetName");
		DataInfo dinfo;
		dinfo.noCacheUpdate = true;
		auto* allContracts = dr.get<ActiveContractsData>(dr.getSecurityMaster(), datasetName + ".allContracts", &dinfo);

		if (!allContracts)
			return;

		auto cols = allContracts->cols();
		
		for (size_t ii = 0; ii < cols; ii++) {
			const auto& activeContracts = allContracts->activeContracts(0, ii);
			auto numContracts = activeContracts.size();

			for (size_t ci = 0; ci < numContracts; ci++) {
				const auto& contract = activeContracts.contract(ci);
				if (contract.expired())
					m_existingContracts[contract.name()] = contract;
			}
		}
	}

	void CSI_FuturesContracts::loadContract(const RuntimeData& rt, const std::string& filename, Contract& contract, bool isZip) {
		auto sep = config().getString("separator", ",");
		pesa::helper::FileDataLoader reader(filename, sep.c_str(), false, false, nullptr, isZip);

		std::string line;
		auto values = reader.read(&line);
		size_t numLines = 1;

		CDebug("Handling: %s", filename);

		while (values.size()) {
			numLines++;
			ASSERT(values.size() == 9, "Invalid entry at line: " << numLines << " - " << line);
			auto date = Util::cast<IntDate>(values[0]);
			auto diDate = rt.getExactDateIndex(date);

			if (date < contract.issueDate() || contract.issueDate() == 0)
				contract.issueDate() = date;

			if (date > contract.expiryDate() || contract.expiryDate() == 0)
				contract.expiryDate() = date;

			if (date > contract.deliveryDate() || contract.deliveryDate() == 0)
				contract.deliveryDate() = date;

			/// TODO: This needs to be done properly, since we'll be excluding the dates that the future is being traded on!
			if (diDate != Constants::s_invalidDay && date >= rt.dates[0] && date < rt.dates[rt.dates.size() - 1]) {
				contract.dates().push_back(date);
				contract.open().push_back(Util::cast<float>(values[1]));
				contract.high().push_back(Util::cast<float>(values[2]));
				contract.low().push_back(Util::cast<float>(values[3]));
				contract.close().push_back(Util::cast<float>(values[4]));
				contract.volume().push_back(Util::cast<float>(values[5]));
				contract.interest().push_back(Util::cast<float>(values[6]));
				contract.totalVolume().push_back(Util::cast<float>(values[7]));
				contract.interestAll().push_back(Util::cast<float>(values[8]));
			}

			values = reader.read(&line);
		}
	}

	void CSI_FuturesContracts::buildContractsFor(IDataRegistry& dr, const DataCacheInfo& dci, ActiveContractsData* allContracts, Security::Index ii) {
		const auto* secMaster = dr.getSecurityMaster();
		auto baseDir = config().getResolvedRequiredString("baseDir");
		auto filePattern = config().getResolvedRequiredString("filePattern");

		auto sec = secMaster->security(0, ii);
		auto ticker = sec.tickerStr();
		Poco::Path searchPath(baseDir);

		searchPath.append(ticker);
		searchPath.append(filePattern);

		auto searchString = searchPath.toString();
		std::set<std::string> files;
		Poco::Glob::glob(searchString, files);

		CDebug("[%d] %s contracts: %z", (int)ii, sec.tickerStr(), files.size());

		Poco::DateTime simEndDate = Util::intToDate(dci.rt.dates[dci.rt.diSimEnd() - 20]);
		Poco::DateTime simStartDate = Util::intToDate(dci.rt.dates[0]);

		for (const auto& file : files) {
			Poco::Path p(file);
			auto filename = p.getFileName();
			auto parts = Util::split(filename, ".");
			auto contractName = parts[0];
			bool isZip = parts[parts.size() - 1] == "zip";
			auto path = file;

			if (contractName.find("Spot") != std::string::npos || contractName.find("$") != std::string::npos)
				continue;

			if (isZip) 
				path += "@" + Util::combine(parts, ".", 0, parts.size() - 1);

			Poco::DateTime expiryDate = Contract::guessExpiry(contractName);

			if (expiryDate < simStartDate)
				continue;

			auto ciIter = m_existingContracts.find(contractName);

			if (ciIter != m_existingContracts.end()) {
				auto& contract = ciIter->second;
				if (contract.expired()) {
					allContracts->activeContracts(0, ii).addContract(contract);
					continue;
				}
			}

			Contract contract;

			contract.name() = contractName;
			loadContract(dci.rt, path, contract, isZip);

			if (contract.dates().size() && contract.expiryDate() != 0) {
				contract.checkExpired(simEndDate);
				allContracts->activeContracts(0, ii).addContract(contract);
			}
		}
	}

	void CSI_FuturesContracts::buildContracts(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		const auto* secMaster = dr.getSecurityMaster();
		auto numSecurities = (Security::Index)secMaster->size(0);
		auto* allContracts = new ActiveContractsData();

		allContracts->ensureSize(1, numSecurities);

		for (Security::Index ii = 0; ii < numSecurities; ii++) {
			buildContractsFor(dr, dci, allContracts, ii);
		}

		CInfo("Number of securities handled: %d", (int)numSecurities);

		frame.data = DataPtr(allContracts);
		frame.dataSize = 1;
		frame.noDataUpdate = false;
		frame.dataOffset = 0;
	}

	void CSI_FuturesContracts::build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
	}
} /// namespace fut
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createCSI_FuturesContracts(const pesa::ConfigSection& config) {
	return new pesa::fut::CSI_FuturesContracts((const pesa::ConfigSection&)config);
}
