/// *** BEGIN LICENSE ***
/// Copyright (C) QuanVolve Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// CSI_SecMaster.cpp
///
/// Created on: 18 Jun 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Poco/Timestamp.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// CSI Futures Data Security Master
	////////////////////////////////////////////////////////////////////////////
	class CSI_SecMaster : public IDataLoader {
	private:
		typedef std::unordered_map<IntDate, UIntVec>    DailySecMap;
        typedef std::unordered_map<std::string, size_t> SymbolLookupMap;
        typedef std::map<std::string, Security::Index>  SecurityLookup;
        typedef std::unordered_map<std::string, bool>   ExchangeBoolMap;

		SecurityLookup			m_securityLookup;		/// Which securities do we already have? 
		SecurityVec				m_allSecurities;		/// All the securities within the system

		void 					buildSecurities(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities);
		void 					buildDetails(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities);

	public:
								CSI_SecMaster(const ConfigSection& config);
		virtual 				~CSI_SecMaster();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			buildSecurityMaster(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);

		virtual void 			preDataBuild(IDataRegistry& dr, const DataCacheInfo& dci);
		virtual void 			postDataBuild(IDataRegistry& dr, const DataCacheInfo& dci);
	};

	CSI_SecMaster::CSI_SecMaster(const ConfigSection& config) : IDataLoader(config, "CSI_SecMaster") {
	}

	CSI_SecMaster::~CSI_SecMaster() {
		CTrace("Deleting CSI_SecMaster");
	}

	void CSI_SecMaster::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		std::string name = config().getRequired("name");
		std::string prefix = config().getString("prefix", "");

		datasets.add(Dataset(name, {
			DataValue(prefix + "Master", DataType::kSecurityData, Security::s_version, THIS_DATA_FUNC(CSI_SecMaster::buildSecurityMaster), 
				DataFrequency::kStatic, DataFlags::kAlwaysLoad | DataFlags::kUpdatesInOneGo | DataFlags::kSecurityMaster),
		}));
	}

	void CSI_SecMaster::postDataBuild(IDataRegistry& dr, const DataCacheInfo& dci) {
		/// We do not need this data anymore ...
		m_securityLookup.clear();
		m_allSecurities.clear();
	}

	void CSI_SecMaster::preDataBuild(IDataRegistry& dr, const DataCacheInfo& dci) {
		std::string name = config().getRequired("name");
		std::string prefix = config().getString("prefix", "");

		const SecurityData* secData = dr.get<SecurityData>(nullptr, name + "." + prefix + "Master", true);

		if (secData) {
			size_t numSecurities = secData->numSecurities();
			m_allSecurities.resize(numSecurities);
			CInfo("Existing number of securities: %z", numSecurities);

			for (size_t i = 0; i < numSecurities; i++) {
				const Security& sec = secData->security(i);
				m_securityLookup[sec.uuidStr()] = sec.index;
				m_allSecurities[i] = sec;
			}
		}
	}

	void CSI_SecMaster::build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
	}

	void CSI_SecMaster::buildSecurities(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities) {
		DataInfo dinfo;
		dinfo.noCacheUpdate				= true;
		dinfo.noAssertOnMissingDataLoader = true;

		const auto* csiId				= dr.stringData(nullptr, "preprocess.csiId", (DataInfo*)&dinfo);
		const auto* symbol				= dr.stringData(nullptr, "preprocess.symbol", (DataInfo*)&dinfo);
		const auto* exchange			= dr.stringData(nullptr, "preprocess.exchange", (DataInfo*)&dinfo);
		const auto* futuresCategory		= dr.stringData(nullptr, "preprocess.futuresCategory", (DataInfo*)&dinfo);
		const auto* name				= dr.stringData(nullptr, "preprocess.name", (DataInfo*)&dinfo);
		const auto* exchSymbol			= dr.stringData(nullptr, "preprocess.exchSymbol", (DataInfo*)&dinfo);
		const auto* units				= dr.stringData(nullptr, "preprocess.units", (DataInfo*)&dinfo);
		const auto* contractSize		= dr.stringData(nullptr, "preprocess.contractSize", (DataInfo*)&dinfo);
		const auto* pointValue			= dr.stringData(nullptr, "preprocess.pointValue", (DataInfo*)&dinfo);
		const auto* activeMonths		= dr.stringData(nullptr, "preprocess.activeMonths", (DataInfo*)&dinfo);
		const auto* currency			= dr.stringData(nullptr, "preprocess.currency", (DataInfo*)&dinfo);
		const auto* charSymbol			= dr.stringData(nullptr, "preprocess.charSymbol", (DataInfo*)&dinfo);
		const auto* marketType			= dr.stringData(nullptr, "preprocess.marketType", (DataInfo*)&dinfo);
		const auto* metastockShortName	= dr.stringData(nullptr, "preprocess.metastockShortName", (DataInfo*)&dinfo);
		const auto* tradingHours_Local	= dr.stringData(nullptr, "preprocess.tradingHours_Local", (DataInfo*)&dinfo);

		bool ignoreMissingUuid			= config().getBool("ignoreMissingUuid", false);
		size_t count					= csiId->cols();

		for (size_t ii = 0; ii < count; ii++) {
			auto uuid					= csiId->getValue(0, ii); 
			ASSERT(!uuid.empty(), "Invalid/Empty UUID for security at index: " << ii);

			auto iter					= m_securityLookup.find(uuid);
			if (iter != m_securityLookup.end())
				continue;

			Security sec;

			sec.setUuid(uuid);
			sec.setInstrumentId(exchange->getValue(0, ii) + "." + exchSymbol->getValue(0, ii));
			sec.setTicker(symbol->getValue(0, ii));
			sec.setBBTicker(symbol->getValue(0, ii)); 
			sec.setName(name->getValue(0, ii)); 
			sec.setExchange(exchange->getValue(0, ii));
			sec.setCurrency(currency->getValue(0, ii));
			//sec.setCountryCode2("FUT");

			newSecurities.push_back(sec);

			m_securityLookup[uuid] = (unsigned int)(m_allSecurities.size() + ii);
		}
	}

    void CSI_SecMaster::buildSecurityMaster(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
        SecurityVec newSecurities;
        buildSecurities(dr, dci, newSecurities);

        CDebug("Number of new securities found: %z", newSecurities.size());

        size_t numExisting = m_allSecurities.size();
        size_t numNew = newSecurities.size();
        size_t numFiltered = numNew;

        /// Assign the correct indexes to the securities
        for (auto& sec : newSecurities) {
            sec.index = (Security::Index)m_allSecurities.size();
            m_allSecurities.push_back(sec);
        }

        try {
            CDebug("Number of new securities: %z - After filteration: %z [Existing: %z, DataSize: %z]", numNew, numFiltered, numExisting, m_allSecurities.size() * sizeof(Security));

            /// otherwise we create a new data for these
            frame.data = std::make_shared<SecurityData>(dci.universe, m_allSecurities, dci.def);
            frame.dataOffset = 0;
            frame.dataSize = frame.data->dataSize();
            frame.noDataUpdate = false;
        }
        catch (const std::exception& e) {
            Util::reportException(e);
            Util::exit(1);
        }
    }

    void CSI_SecMaster::buildDetails(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities) {
        CDebug("Getting detailed information about the securities!");
    }
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createCSI_SecMaster(const pesa::ConfigSection& config) {
	return new pesa::CSI_SecMaster((const pesa::ConfigSection&)config);
}

