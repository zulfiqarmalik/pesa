/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// FuturesDaily.cpp
///
/// Created on: 20 Jun 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Framework/Components/IDataLoader.h"
#include "Framework/Data/FuturesContract.h"
#include "Framework/Helper/FileDataLoader.h"
#include "Framework/Data/IndexedStringData.h"

#include "Poco/String.h"
#include "Poco/Glob.h"

namespace pesa {
namespace fut {
	////////////////////////////////////////////////////////////////////////////
	/// Gets all the futures contracts
	////////////////////////////////////////////////////////////////////////////
	class FuturesDaily : public IDataLoader {
	private:
		virtual void 			buildPrices(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);

	public:
								FuturesDaily(const ConfigSection& config);
		virtual 				~FuturesDaily();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
	};

	FuturesDaily::FuturesDaily(const ConfigSection& config) 
		: IDataLoader(config, "FuturesDaily") {
	}

	FuturesDaily::~FuturesDaily() {
		CTrace("Deleting FuturesDaily!");
	}

	void FuturesDaily::initDatasets(IDataRegistry& dr, Datasets& dss) {
		auto datasetName = config().getRequired("datasetName");

		dss.add(Dataset(config().getRequired("datasetName"), {
			DataValue("openPrice", DataType::kFloat, 0, THIS_DATA_FUNC(FuturesDaily::buildPrices), DataFrequency::kDaily, 0),
			DataValue("closePrice", DataType::kFloat, 0, THIS_DATA_FUNC(FuturesDaily::buildPrices), DataFrequency::kDaily, 0),
			DataValue("highPrice", DataType::kFloat, 0, THIS_DATA_FUNC(FuturesDaily::buildPrices), DataFrequency::kDaily, 0),
			DataValue("lowPrice", DataType::kFloat, 0, THIS_DATA_FUNC(FuturesDaily::buildPrices), DataFrequency::kDaily, 0),
			DataValue("volume", DataType::kFloat, 0, THIS_DATA_FUNC(FuturesDaily::buildPrices), DataFrequency::kDaily, 0),
			DataValue("interest", DataType::kFloat, 0, THIS_DATA_FUNC(FuturesDaily::buildPrices), DataFrequency::kDaily, 0),
			DataValue("totalVolume", DataType::kFloat, 0, THIS_DATA_FUNC(FuturesDaily::buildPrices), DataFrequency::kDaily, 0),
			DataValue("interestAll", DataType::kFloat, 0, THIS_DATA_FUNC(FuturesDaily::buildPrices), DataFrequency::kDaily, 0),
		}));
	}

	void FuturesDaily::buildPrices(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		auto date				= dci.rt.dates[dci.di]; 
		auto* data				= new FloatMatrixData(nullptr, dci.def);

		auto* secMaster			= dr.getSecurityMaster();
		auto numSecurities		= (Security::Index)secMaster->size(0);

		auto allContractsId		= config().getString("allContracts", "secData.allContracts");
		auto* allContracts		= dr.get<ActiveContractsData>(secMaster, allContractsId, nullptr);
		ASSERT(allContracts, "Unable to load allContracts from: " << allContractsId);

		auto frontContractId	= config().getString("frontContract", "baseData.frontContract");
		auto* frontContract		= dr.stringData(secMaster, frontContractId);

		data->ensureSize(1, numSecurities);
		data->invalidateAll();

		std::string dataName	= dci.def.name;
		size_t numValid			= 0;

		for (auto ii = 0; ii < numSecurities; ii++) {
			float value;
			auto sec			= secMaster->security(0, ii);
			auto ctName			= frontContract->getValue(dci.di, ii);

			if (ctName.empty())
				continue;

			const auto* ct		= allContracts->activeContracts(0, ii).findContract(ctName);

			if (!ct) 
				continue;

			if (dataName == "openPrice")
				value 			= ct->open(date);
			else if (dataName == "closePrice")
				value 			= ct->close(date);
			else if (dataName == "highPrice")
				value 			= ct->high(date);
			else if (dataName == "lowPrice")
				value 			= ct->low(date);
			else if (dataName == "volume")
				value 			= ct->volume(date);
			else if (dataName == "interest")
				value 			= ct->interest(date);
			else if (dataName == "totalVolume")
				value 			= ct->totalVolume(date);
			else if (dataName == "interestAll")
				value 			= ct->interestAll(date);
			else {
				ASSERT(false, "Unsupported data name: " << dataName);
			}

			if (!std::isnan(value))
				numValid++;

			(*data)(0, ii)		= value;
		}

		frame.data				= DataPtr(data);
		frame.dataSize			= data->dataSize();
		frame.dataOffset		= 0;
		frame.noDataUpdate		= false;

		CDebug("[%u]-[%s] - Count: %z", date, dataName, numValid);
	}

	void FuturesDaily::build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
	}
} /// namespace fut
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createFuturesDaily(const pesa::ConfigSection& config) {
	return new pesa::fut::FuturesDaily((const pesa::ConfigSection&)config);
}
