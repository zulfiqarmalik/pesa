/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// FuturesFrontContract.cpp
///
/// Created on: 20 Jun 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Framework/Components/IDataLoader.h"
#include "Framework/Data/FuturesContract.h"
#include "Framework/Helper/FileDataLoader.h"
#include "Framework/Data/IndexedStringData.h"

#include "Poco/String.h"
#include "Poco/Glob.h"

namespace pesa {
namespace fut {
	////////////////////////////////////////////////////////////////////////////
	/// Gets all the futures contracts
	////////////////////////////////////////////////////////////////////////////
	class FuturesFrontContract : public IDataLoader {
	private:
		int						m_rollDays = 2;			/// Roll days
		ContractCPVec			m_frontContracts;		/// The current contracts
		ContractCPVec			m_lastContracts;		/// The last contracts

		virtual void 			buildDailyContracts(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame); 
		virtual void 			buildDates(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);

	public:
								FuturesFrontContract(const ConfigSection& config);
		virtual 				~FuturesFrontContract();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
	};

	FuturesFrontContract::FuturesFrontContract(const ConfigSection& config) 
		: IDataLoader(config, "FuturesFrontContract") {
		m_rollDays			= config.getInt("rollDays", m_rollDays);
	}

	FuturesFrontContract::~FuturesFrontContract() {
		CTrace("Deleting FuturesFrontContract!");
	}

	void FuturesFrontContract::initDatasets(IDataRegistry& dr, Datasets& dss) {
		dss.add(Dataset(config().getRequired("datasetName"), {
			DataValue("frontContract", DataType::kIndexedString, 0, THIS_DATA_FUNC(FuturesFrontContract::buildDailyContracts), DataFrequency::kDaily, 0),
			DataValue("issueDate", DataType::kInt, 0, THIS_DATA_FUNC(FuturesFrontContract::buildDates), DataFrequency::kDaily, 0),
			DataValue("expiryDate", DataType::kInt, 0, THIS_DATA_FUNC(FuturesFrontContract::buildDates), DataFrequency::kDaily, 0),
			DataValue("spotDate", DataType::kInt, 0, THIS_DATA_FUNC(FuturesFrontContract::buildDates), DataFrequency::kDaily, 0),
		}));
	}

	void FuturesFrontContract::buildDates(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		auto date = dci.rt.dates[dci.di];
		auto* data = new IntMatrixData(nullptr, dci.def);

		auto* secMaster = dr.getSecurityMaster();
		auto numSecurities = (Security::Index)secMaster->size(0);

		data->ensureSize(1, numSecurities);
		data->invalidateAll();

		ASSERT(m_frontContracts.size(), "Front contracts have not been cached. This should have been done in FuturesDaily::buildDailyContracts!");

		std::string dataName = dci.def.name;
		size_t numValid = 0;

		for (auto ii = 0; ii < numSecurities; ii++) {
			const auto* ct = m_frontContracts[ii];

			if (!ct)
				continue;

			numValid++;

			if (dataName == "issueDate")
				(*data)(0, ii) = ct->issueDate();
			else if (dataName == "expiryDate")
				(*data)(0, ii) = ct->expiryDate();
			else if (dataName == "spotDate")
				(*data)(0, ii) = ct->liquidateDate();
		}

		frame.data = DataPtr(data);
		frame.dataSize = data->dataSize();
		frame.dataOffset = 0;
		frame.noDataUpdate = false;

		CDebug("[%u]-[%s] - Count: %z", date, dataName, numValid);
	}

	void FuturesFrontContract::buildDailyContracts(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		auto allContractsId		= config().getString("allContracts", "secData.allContracts");
		auto* secMaster			= dr.getSecurityMaster();
		auto* allContracts		= dr.get<ActiveContractsData>(secMaster, allContractsId, nullptr);

		ASSERT(allContracts, "Unable to load allContracts from: " << allContractsId);

		auto numSecurities		= (Security::Index)secMaster->size(0);

		if (!m_frontContracts.size()) {
			m_frontContracts.resize(numSecurities);
			memset(&m_frontContracts[0], 0, sizeof(const Contract*) * m_frontContracts.size());

			m_lastContracts.resize(numSecurities);
			memset(&m_lastContracts[0], 0, sizeof(const Contract*) * m_lastContracts.size());
		}

		auto date				= dci.rt.dates[dci.di];
		auto* data				= new ShortIndexedStringData(nullptr, dci.def);
		std::string dataName	= dci.def.name;

		data->ensureSize(1, numSecurities);
		data->setMetadata(dci.customMetadata);

		/// Now we actually try to construct the front contract for all the securities in the universe
		for (auto ii = 0; ii < numSecurities; ii++) {
			auto sec			= secMaster->security(0, ii);
			auto& activeContracts = allContracts->activeContracts(0, ii);
			const auto* contract= activeContracts.getFrontContract(dci.rt, m_frontContracts[ii], date, m_rollDays);

			if (contract) {
				if (m_lastContracts[ii] != m_frontContracts[ii])
					m_lastContracts[ii] = m_frontContracts[ii];

				m_frontContracts[ii]= contract;
			}
			else {
				CTrace("[%u]-[%s]: No Contract!", date, sec.tickerStr());
			}

			if (dataName == "frontContract" && m_frontContracts[ii])
				data->setValue(0, ii, m_frontContracts[ii]->name());
			else if (dataName == "lastContract" && m_lastContracts[ii])
				data->setValue(0, ii, m_lastContracts[ii]->name());
			else
				data->setValue(0, ii, "");
		}

		frame.dataOffset		= 0;
		frame.dataSize			= data->dataSize();
		frame.data				= DataPtr(data);
		frame.noDataUpdate		= false;
	}

	void FuturesFrontContract::build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
	}
} /// namespace fut
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createFuturesFrontContract(const pesa::ConfigSection& config) {
	return new pesa::fut::FuturesFrontContract((const pesa::ConfigSection&)config);
}
