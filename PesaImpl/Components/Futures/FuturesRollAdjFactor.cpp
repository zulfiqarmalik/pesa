/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// FuturesRollAdjFactor.cpp
///
/// Created on: 21 Jun 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Framework/Components/IDataLoader.h"
#include "Framework/Data/FuturesContract.h"
#include "Framework/Helper/FileDataLoader.h"
#include "Framework/Data/IndexedStringData.h"

#include "Poco/String.h"
#include "Poco/Glob.h"

namespace pesa {
namespace fut {
	////////////////////////////////////////////////////////////////////////////
	/// FuturesRollAdjFactor: Roll adjustment factor
	////////////////////////////////////////////////////////////////////////////
	class FuturesRollAdjFactor : public IDataLoader {
	private:
		FloatMatrixDataPtr		m_dailyFactors = nullptr;	/// The daily PRICE factors

		void					buildDailyFactors(IDataRegistry& dr, const DataCacheInfo& dci);

	public:
								FuturesRollAdjFactor(const ConfigSection& config);
		virtual 				~FuturesRollAdjFactor();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
	};

	FuturesRollAdjFactor::FuturesRollAdjFactor(const ConfigSection& config) 
		: IDataLoader(config, "FuturesRollAdjFactor") {
	}

	FuturesRollAdjFactor::~FuturesRollAdjFactor() {
		CTrace("Deleting FuturesRollAdjFactor!");
	}

	void FuturesRollAdjFactor::initDatasets(IDataRegistry& dr, Datasets& dss) {
		auto datasetName = config().getRequired("datasetName");
		dss.add(Dataset(datasetName, {
			DataValue("f_rollFactor", DataType::kFloat, 0, nullptr, DataFrequency::kDaily, DataFlags::kCollectAllBeforeWriting),
		}));
	}

	void FuturesRollAdjFactor::buildDailyFactors(IDataRegistry& dr, const DataCacheInfo& dci) {
		/// Daily factors have already been calculated
		if (m_dailyFactors)
			return; 

		auto* secMaster			= dr.getSecurityMaster();
		auto numSecurities		= secMaster->size(0);

		auto priceDataId		= config().getString("priceDataId", "baseData.closePrice");
		auto* prices			= dr.floatData(secMaster, priceDataId);

		auto frontContractId	= config().getString("frontContractId", "baseData.frontContract");
		auto* frontContract		= dr.stringData(dr.getSecurityMaster(), frontContractId);

		ASSERT(frontContract, "Unable to load front contract source data: " << frontContractId);

		m_dailyFactors			= std::make_shared<FloatMatrixData>(nullptr, dci.def);
		m_dailyFactors->ensureSize(dci.diEnd - dci.diStart + 1, numSecurities, false);

		m_dailyFactors->setMemory(1.0f);

		/// diEnd - 1 because we're looking from the current to the next contract and on the last day
		/// there won't be any roll adjustment factor kicking in
		for (auto di = dci.diStart; di <= dci.diEnd - 1; di++) {
			for (auto ii = 0; ii < numSecurities; ii++) {
				auto currContract	= frontContract->getValue(di, ii);
				auto nextContract	= frontContract->getValue(di + 1, ii);

				/// A change in contract is happening
				if (currContract != nextContract) {
					auto currPrice	= (*prices)(di, ii);
					auto nextPrice	= (*prices)(di + 1, ii);
					auto factor		= nextPrice / currPrice;

					(*m_dailyFactors)(di, ii) = factor;
				}
			}
		}
	}

	void FuturesRollAdjFactor::build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		buildDailyFactors(dr, dci);

		auto data		= m_dailyFactors->extractRow(dci.di - dci.diStart);

		frame.data		= data;
		frame.dataSize	= data->dataSize();
		frame.dataOffset= 0;
		frame.noDataUpdate = false;

		CDebug("[%u]: %s", dci.rt.dates[dci.di], std::string(dci.def.name));
	}
} /// namespace fut
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createFuturesRollAdjFactor(const pesa::ConfigSection& config) {
	return new pesa::fut::FuturesRollAdjFactor((const pesa::ConfigSection&)config);
}
