/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// AdjustedPrices.cpp
///
/// Created on: 15 Nov 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Poco/String.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// AdjustedPrices data
	////////////////////////////////////////////////////////////////////////////
	class AdjustedPrices : public IDataLoader {
	private:
		typedef std::unordered_map<std::string, double> DoubleMap;
		std::string				m_datasetName;				/// The name of this dataset
		StringVec 				m_datasets; 				/// What datasets are we supposed to load
		StringMap				m_mappings;					/// Mappings from the original name to our custom name
		StringMap				m_rmappings;				/// Reverse mappings
		std::string				m_ccyDataPrefix = "";		/// The prefix of the currency data
		std::string				m_ccyDataSuffix = "";		/// The suffix of the currency data
		std::string				m_adjFactorId = "";			/// The adjustment factor
		const FloatMatrixData*	m_adjFactor = nullptr;		/// The adjustment factor matrix
		bool					m_alwaysOverwrite = false;	/// Always overwrite the data or NOT
		bool					m_useFullQuote = false;		/// Use the full quote to get the currency information
		StringVec				m_excludeCcyFactors;		/// Which datasets are excluded from the ccy factors

		std::unordered_map<std::string, size_t> m_instrumentIndexLUT; /// Instrument index lookup table
		std::string				m_ccyInstrumentId;			/// The data used for currency identifier
		std::string				m_ccyDataId;				/// The currency

		std::string				m_missingCurrencyDataId;	/// Missing currency data id

		StringMap				m_ccyQuoteStyles;			/// The quote styles of the currencies
		DoubleMap				m_ccyFactors;				/// Additional currency factors e.g. GBP which is quoted in pence
		IntDay					m_ccyDelay = 0;				/// Delay for the currency data
		bool					m_applyCcyFactors = false;	/// Whether to apply the currency factors or not 

		double					getCcyRate(IDataRegistry& dr, const pesa::DataCacheInfo& dci, const std::string& ccy, IntDay diCcy, const std::string& ccyQuoteStyle);

	public:
								AdjustedPrices(const ConfigSection& config);
		virtual 				~AdjustedPrices();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);
		double					getCcyFactor(IDataRegistry& dr, const DataCacheInfo& dci, const Security& sec);
		double					getAdjFactor(IDataRegistry& dr, const DataCacheInfo& dci, const Security& sec);
		std::string				getQuoteStyle(const std::string& ccy);
	};

	AdjustedPrices::AdjustedPrices(const ConfigSection& config) : IDataLoader(config, "AdjustedPrices") {
		m_ccyDataPrefix			= config.getString("ccyDataPrefix", m_ccyDataPrefix);
		m_ccyDataSuffix			= config.getString("ccyDataSuffix", m_ccyDataSuffix);
		m_adjFactorId			= config.getString("adjFactor", m_adjFactorId);
		m_datasetName			= config.getRequired("datasetName");
		m_missingCurrencyDataId = config.getString("missingCurrencyDataId", m_missingCurrencyDataId);
		m_alwaysOverwrite		= config.getBool("alwaysOverwrite", m_alwaysOverwrite);
		m_ccyDelay				= config.get<IntDay>("ccyDelay", 0);
		m_ccyInstrumentId		= config.getString("ccyInstrument", "");
		m_ccyDataId				= config.getString("ccyData", "");
		m_useFullQuote			= config.getBool("useFullQuote", m_useFullQuote);

		std::string datasets	= config.getString("datasets", "");

		if (!datasets.empty())
			m_datasets = Util::split(datasets, ",");

		std::string mappings = config.get("mappings");

		if (!mappings.empty()) {
			StringVec mappingsVec = Util::split(mappings, ",");
			for (auto& mapping : mappingsVec) {
				StringVec parts = Util::split(mapping, "=");
				ASSERT(parts.size() == 2, "Invalid mapping: " << mapping);
				m_mappings[parts[0]] = parts[1];
				m_rmappings[parts[1]] = parts[0];
			}
		}

		if (!m_ccyDataPrefix.empty() || !m_ccyInstrumentId.empty()) {
			auto quoteStylesStr = config.getResolvedRequiredString("currencyQuoteStyles");
			auto quoteStyles = Util::split(quoteStylesStr, ",");

			for (auto quoteStyle : quoteStyles) {
				auto parts = Util::split(quoteStyle, ":");
				ASSERT(parts.size() == 2, "Invalid quote style: " << quoteStyle << " - Expecting in form: CC1:CC1CC2 or CC1:CC2CC1");
				m_ccyQuoteStyles[parts[0]] = parts[1];
			}

			m_applyCcyFactors = config.getBool("applyCcyFactors", m_applyCcyFactors);

			if (m_applyCcyFactors) {
				auto ccyFactorsStr = config.getResolvedRequiredString("currencyFactors");
				auto ccyFactors = Util::split(ccyFactorsStr, ",");

				for (auto ccyFactor : ccyFactors) {
					auto parts = Util::split(ccyFactor, ":");
					ASSERT(parts.size() == 2, "Invalid currency factor: " << ccyFactor << " - Expecting in form: CCY:0.01 or some other floating point value");
					m_ccyFactors[parts[0]] = Util::cast<double>(parts[1]);
				}

				auto excludeCcyFactors = config.getString("excludeCcyFactors", "");
				if (!excludeCcyFactors.empty())
					m_excludeCcyFactors = Util::split(excludeCcyFactors, ",");
			}
		}
	}

	AdjustedPrices::~AdjustedPrices() {
		CTrace("Deleting AdjustedPrices!");
	}

	void AdjustedPrices::initDatasets(IDataRegistry& dr, Datasets& dss) {
		Dataset ds(m_datasetName);
		StringVec dataRequired;

		bool afterMarketOpen = config().getBool("afterMarketOpen", false);

		for (std::string dataset : m_datasets) {
			DataFrequency::Type frequency;
			DataType::Type type;
			std::string dataName;
			bool addToRequiredList = true;

			DataDefinition::parseDefString(dataset, dataName, type, frequency);

			std::string orgDataName = dataName;
			auto mappedIter = m_mappings.find(dataName);
			if (mappedIter != m_mappings.end())
				dataName = mappedIter->second;  

			unsigned int dataFlags = 0; 
			if (m_alwaysOverwrite)
				dataFlags |= DataFlags::kAlwaysOverwrite;

			if (frequency == DataFrequency::kStatic)
				dataFlags |= DataFlags::kUpdatesInOneGo;

			if (afterMarketOpen)
				dataFlags |= DataFlags::kUpdateAfterMarketOpen;

			DataValue dv(dataName, type, DataType::size(type), nullptr, frequency, dataFlags);
			ds.add(dv);
		}

		dss.add(ds);
	}

	std::string AdjustedPrices::getQuoteStyle(const std::string& ccy) {
		auto quoteStyleIter = m_ccyQuoteStyles.find(ccy);
		std::string quoteStyle;

		/// If there is no style specified for this particular currency, then we just use the XXX one
		if (quoteStyleIter == m_ccyQuoteStyles.end()) {
			quoteStyleIter = m_ccyQuoteStyles.find("XXX");
			ASSERT(quoteStyleIter != m_ccyQuoteStyles.end(), "Unable to find a suitable quote style for currency: " << ccy << ". Either specify an explicit one for this currency or have a generic one of the form XXX:XXXUSD or XXX:USDXXX");
			quoteStyle = quoteStyleIter->second;
			quoteStyle = Poco::replace(quoteStyle, "XXX", ccy.c_str());
		}
		else
			quoteStyle = quoteStyleIter->second;

		return quoteStyle;
	}

	double AdjustedPrices::getAdjFactor(IDataRegistry& dr, const DataCacheInfo& dci, const Security& sec) {
		if (m_adjFactorId.empty())
			return 1.0;

		if (!m_adjFactor) {
			m_adjFactor = dr.floatData(dci.universe, m_adjFactorId);
			ASSERT(m_adjFactor, "Unable to load adjustment factor: " << m_adjFactorId);
		}

		return (*m_adjFactor)(dci.di, sec.index);
	}

	double AdjustedPrices::getCcyRate(IDataRegistry& dr, const pesa::DataCacheInfo& dci, const std::string& ccy, IntDay diCcy, const std::string& ccyQuote) {
		double ccyValue = 1.0;

		if (m_ccyInstrumentId.empty()) {
			auto ccyDataId = m_ccyDataPrefix + "." + (!m_useFullQuote ? ccy : ccyQuote) + m_ccyDataSuffix;
			auto* ccyData = dr.floatData(dci.universe, ccyDataId);
			ASSERT(ccyData, "Unable to load data: " << ccyDataId);

			if (ccyData->rows() == 1 && ccyData->def().isStatic())
				ccyData->diOffset() = diCcy;

			ccyValue = (double)(*ccyData)(diCcy, 0); 
		}
		else {
			std::string ccyFull = m_ccyDataPrefix + ccyQuote + m_ccyDataSuffix;

			if (m_instrumentIndexLUT.empty()) {
				const StringData* instruments = dr.stringData(dci.universe, m_ccyInstrumentId, true);
				ASSERT(instruments, "Unable to load currency instruments: " << m_ccyInstrumentId);

				for (size_t ii = 0; ii < instruments->cols(); ii++)
					m_instrumentIndexLUT[instruments->getValue(0, ii)] = ii;
			}

			auto iter = m_instrumentIndexLUT.find(ccyFull);
			ASSERT(iter != m_instrumentIndexLUT.end(), "Unable to find currency: " << ccy << " [Full: " << ccyFull << "]");

			size_t ii = iter->second;

			const FloatMatrixData* ccyData = dr.floatData(dci.universe, m_ccyDataId, true);
			ASSERT(ccyData, "Unable to load currency data: " << m_ccyDataId);

			ccyValue = (*ccyData)(diCcy, ii);
		}

		return ccyValue;
	}

	double AdjustedPrices::getCcyFactor(IDataRegistry& dr, const DataCacheInfo& dci, const Security& sec) {
		if (m_ccyDataPrefix.empty() && m_ccyInstrumentId.empty())
			return 1.0;

		/// First of all fetch the currency of the security
		auto ccy = sec.currencyStr();

		//ASSERT(!ccy.empty(), "Invalid currency for security: " << sec.nameStr() << " - UUID: " << sec.uuidStr());
		double ccyValue = 1.0;
		auto targetCcy = m_config.getCurrency();
		ASSERT(!targetCcy.empty(), "There is no valid currency set for this config. Either have a currency=\"XXX\" in the Defs section or in the Portfolio section");

		if (ccy.length() != 3) {
			//CTrace("Possibly incorrect currency '%s' for UUID: %s - %s", ccy, sec.uuidStr(), sec.nameStr());
			//return 1.0;

			/// Try to get the currency from the currency data
			if (!m_missingCurrencyDataId.empty()) {
				const StringData* missingCurrencyData = dr.stringData(dr.getSecurityMaster(), m_missingCurrencyDataId);
				ccy = missingCurrencyData->getValue(0, (size_t)sec.index);

				if (ccy.length() != 3) {
					CWarning("Incorrect currency '%s' for UUID: %s - %s", ccy, sec.uuidStr(), sec.nameStr());
					return std::nan("");
				}

				/// We ignore the case right now ... we have assumptions in the eu_base.xml config that we adhere to ...
				ccy = Poco::toUpper(ccy);
			}
		}

		IntDay diCcy = dci.di - m_ccyDelay;

		if (ccy != "USD") {
			ASSERT(!ccy.empty(), "Unable to find currency for security: " << sec.debugString());

			if (ccy == "GBX" || ccy == "GBx" || ccy == "GBp")
				ccy = "GBP";

			std::string ccyQuote = getQuoteStyle(ccy);
			ccyValue = getCcyRate(dr, dci, ccy, diCcy, ccyQuote);

			auto pos = ccyQuote.find(ccy);
			ASSERT(pos != std::string::npos, "Invalid quote style: " << ccyQuote << " - for currency: " << ccy);

			/// USDXXX
			if (pos != 0)
				ccyValue = 1.0 / (double)ccyValue;

			auto factorIter = m_ccyFactors.find(ccy);
			if (factorIter != m_ccyFactors.end()) {
				bool applyFactor = true;

				if (!m_excludeCcyFactors.empty()) {
					std::string dataName = dci.def.name;
					for (const auto& ename : m_excludeCcyFactors) {
						if (ename == dataName) {
							applyFactor = false;
							break;
						}
					}
				}

				if (applyFactor) {
					double factor = factorIter->second;
					ccyValue *= factor;
				}
			}
		}
		else
			CTrace("Possibly incorrect currency '%s' for UUID: %s - %s", ccy, sec.uuidStr(), sec.nameStr());

		/// If the target currency is in our quote style
		if (targetCcy == "USD") 
			return ccyValue;
		 
		/// Ok, so we can't map directly to the target currency (non-USD target)
		/// In this case, we use the cross
		auto targetQuoteStyle = getQuoteStyle(targetCcy);
		auto targetPos = targetQuoteStyle.find(targetCcy);
		ASSERT(targetPos != std::string::npos, "Invalid quote style: " << targetQuoteStyle << " - for currency: " << ccy);

		auto targetCcyDataId = m_ccyDataPrefix + "." + targetCcy + m_ccyDataSuffix;
		auto* targetCcyData = dr.floatData(dci.universe, targetCcyDataId);
		ASSERT(targetCcyData, "Unable to load data: " << targetCcyDataId);

		if (targetCcyData->rows() == 1 && targetCcyData->def().isStatic())
			targetCcyData->diOffset() = diCcy;

		double targetCcyValue = (double)(*targetCcyData)(diCcy, 0);
		
		/// XXXUSD
		if (targetPos == 0)
			targetCcyValue = 1.0 / targetCcyValue;

		/// This will get us from source currency to target currency directly (NOTE: this might not be in the official quote style, but that 
		/// is fine)
		return ccyValue * targetCcyValue;
	}

	void AdjustedPrices::build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		std::string dataName = dci.def.name;
		auto iter = m_rmappings.find(dataName);

		ASSERT(iter != m_rmappings.end(), "Unable to find data: " << dataName);

		std::string srcDataId = iter->second;
		auto* srcData		= dr.floatData(dci.universe, srcDataId);

		ASSERT(srcData, "Unable to load data: " << srcDataId);
		auto numCols		= srcData->cols();
		auto* dstData		= new FloatMatrixData(dci.universe, 1, numCols);

		dstData->ensureSize(1, numCols);

		if (srcData->rows() == 1 && srcData->def().isStatic())
			srcData->diOffset() = dci.di;

		/// If we have a currency factor then that needs to be calculated individually and hence we cannot
		/// vectorise this operation
		if (!m_ccyDataPrefix.empty()) {
			for (auto ii = 0; ii < numCols; ii++) {
				auto sec		= dci.universe->security(dci.di, ii);
				auto ccyFactor	= getCcyFactor(dr, dci, sec);
				auto adjFactor	= getAdjFactor(dr, dci, sec);
				double value	= (double)(*srcData)(dci.di, ii);
				double newValue = value * ccyFactor * adjFactor;
			
				(*dstData)(0, ii) = (float)newValue;
			}
		}
		else {
			if (!m_adjFactorId.empty()) {
				if (!m_adjFactor) {
					m_adjFactor = dr.floatData(dci.universe, m_adjFactorId);
					ASSERT(m_adjFactor, "Unable to load adjustment factor: " << m_adjFactorId);
				}
			}

			/// Otherwise we can!
			if (m_adjFactor) 
				(*dstData)[0] = (*srcData)[dci.di] * (*m_adjFactor)[dci.di];
			else
				(*dstData)[0] = (*srcData)[dci.di];
		}


		CDebug("[%u] - %s.%s", dci.rt.dates[dci.di], m_datasetName, std::string(dci.def.name));

		frame.data			= FloatMatrixDataPtr(dstData);
		frame.dataOffset	= 0;
		frame.dataSize		= dstData->dataSize();
		frame.noDataUpdate	= false;
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createAdjustedPrices(const pesa::ConfigSection& config) {
	return new pesa::AdjustedPrices((const pesa::ConfigSection&)config);
}

