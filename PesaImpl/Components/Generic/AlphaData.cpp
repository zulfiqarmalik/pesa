/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// AlphaData.cpp
///
/// Created on: 06 Oct 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"

#include <Poco/Exception.h>
#include <Poco/Zip/Compress.h>
#include <Poco/DateTime.h>
#include <Poco/MemoryStream.h>
#include "Poco/Thread.h"

namespace pesa {
	class AlphaDataDownloader : public Poco::Runnable {
	public:
		IDataRegistry&			m_dr;							/// Reference to the data registry
		IntDay					m_di;							/// The day to download
		RuntimeData				m_rt;							/// A copy of the Runtime data
		DataFrame				m_dataFrame;					/// The data frame 
		ConfigSection*			m_alphaConfig = nullptr;		/// The alpha config
		IUniverse*				m_universe = nullptr;			/// The universe

	public:

								AlphaDataDownloader(IDataRegistry& dr);
								~AlphaDataDownloader();

		void					run();
		void					build(IDataRegistry& dr);
	};

	AlphaDataDownloader::AlphaDataDownloader(IDataRegistry& dr) : m_dr(dr) {
	}

	AlphaDataDownloader::~AlphaDataDownloader() {
		delete m_alphaConfig;
	}

	void AlphaDataDownloader::run() {
		build(m_dr);
	}

	void AlphaDataDownloader::build(IDataRegistry& dr) {
		auto& rt = m_rt;
		rt.di = m_di;

		auto alpha = helper::ShLib::global().createComponent<IAlpha>(*m_alphaConfig, rt.dataRegistry);
		ASSERT(alpha, "Unable to create alpha from config: " << m_alphaConfig->toString());

		//ASSERT(dci.universe, "No universe available in DataCacheInfo");
		//IUniverse* universe = dci.universe;

		///// Set the universe for the alpha
		//m_alpha->universe() = universe;

		IUniverse* universe = m_universe;

		/// Set the universe for the alpha
		alpha->universe() = universe;

		size_t size = universe->size(rt.di);
		ASSERT(size, "Universe-%s" << universe->name() << " invalid size returned on di: " << rt.di);

		/// Create the buffer to contain the results of the alpha
		alpha->data().ensureAlphaSize(1, size);

		/// run the alpha ...
		alpha->run(rt);

		/// Invalidate stuff that does not exist ...

		FloatMatrixDataPtr alphaData = alpha->data().alpha;

		/// Create a new data frame
		m_dataFrame = DataFrame();
		pesa::DataFrame& frame = m_dataFrame;

		frame.data = alphaData;
		frame.dataOffset = 0;
		frame.dataSize = alphaData->dataSize();

		//CDebug("[%u]-[Count: %z x %z] - %s", dci.rt.dates[dci.di], alphaData->rows(), alphaData->cols(), dci.def.toString(dci.ds));
	}

	////////////////////////////////////////////////////////////////////////////
	/// AlphaData - Takes the output of an Alpha and converts it into a data.
	/// Only supports floating point for the time being since the output of
	/// all Alphas has to be a floating point right now.
	////////////////////////////////////////////////////////////////////////////
	class AlphaData : public IDataLoader {
	private:
		static const int		s_minDays = 10;					/// Minimum number of days to use threading for

		std::string				m_datasetName = "baseData";		/// The name of the dataset
		std::string				m_dataId;						/// The name of the data value
		bool					m_alwaysOverwrite = false;		/// Always overwrite
		bool					m_isUniversal = false;			/// Whether the data is univeresal or not ...
		int						m_numThreads = 1;				/// How many threads to use
		IntDay					m_diCache = 0;					/// The day that we're caching right now

		std::vector<AlphaDataDownloader*> m_downloaders;		/// The downloaders
		std::vector<Poco::Thread*> m_threads;					/// Threads used for downloading 

		virtual void 			initThreads(IDataRegistry& dr, const pesa::DataCacheInfo& dci);
	public:
								AlphaData(const ConfigSection& config);
		virtual 				~AlphaData();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);
		virtual void 			postDataBuild(IDataRegistry& dr, const pesa::DataCacheInfo& dci);

		virtual std::string 	id() const {
			return m_datasetName + "." + m_dataId;
		}
	};

	AlphaData::AlphaData(const ConfigSection& config) : IDataLoader(config, "ALPHA_DATA") {
		m_datasetName	= config.getString("datasetName", m_datasetName);
		m_dataId		= config.getRequired("dataId");
		m_alwaysOverwrite = config.getBool("alwaysOverwrite", m_alwaysOverwrite);
		m_isUniversal	= config.getBool("isUniversal", m_isUniversal);
		m_numThreads	= config.getInt("threads", m_numThreads, true);
	}

	AlphaData::~AlphaData() {
		CTrace("Deleting AlphaData");
	}

	void AlphaData::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		DataFrequency::Type frequency = DataFrequency::kDaily;
		int flags = 0;

		if (m_alwaysOverwrite)
			flags |= DataFlags::kAlwaysOverwrite;

		if (m_isUniversal) {
			frequency = DataFrequency::kStatic;
			flags |= DataFlags::kUpdatesInOneGo;
		}

		datasets.add(Dataset(m_datasetName, {
			DataValue(m_dataId, DataType::kFloat, sizeof(float), nullptr, frequency, flags),
		}));
	}

	void AlphaData::postDataBuild(IDataRegistry& dr, const pesa::DataCacheInfo& dci) {
		for (size_t ti = 0; ti < m_threads.size(); ti++) {
		  	ASSERT(!m_threads[ti]->isRunning(), "Thread is still running: " << ti);
			delete m_threads[ti];
		}

		m_threads.clear();

		for (size_t ti = 0; ti < m_downloaders.size(); ti++)
			delete m_downloaders[ti];

		m_downloaders.clear();
	}

	void AlphaData::initThreads(IDataRegistry& dr, const pesa::DataCacheInfo& dci) {
	    if (m_downloaders.size())
            return;

		IntDay numDays = dci.diEnd - dci.diStart + 1;
		int numThreads = m_numThreads;

		if (numDays < s_minDays) 
			numThreads = 1;

		m_downloaders.resize(numThreads);

		auto rt = dci.rt;
		rt.di = dci.di;

		for (size_t ti = 0; ti < m_downloaders.size(); ti++) {
			ConfigSection* palphaConfig = new ConfigSection(config());
			ConfigSection& alphaConfig = *palphaConfig;
			alphaConfig.set("id", alphaConfig.getRequired("a_id"));
			std::string moduleId = alphaConfig.getString("a_moduleId", "");

			if (!moduleId.empty())
				alphaConfig.set("moduleId", moduleId);

			alphaConfig.set("delay", 0);
			alphaConfig.set("noApplyDelay", "true");

			const ConfigSection* moduleConfig = parentConfig()->getModuleFromModuleId(alphaConfig);
			ASSERT(moduleConfig, "Unable to find moduleId: " << alphaConfig.moduleId());

			/// Merge the module and portfolio configs
			alphaConfig.mergeProperties(*moduleConfig, nullptr, false);
			alphaConfig.mergeProperties(parentConfig()->portfolio(), nullptr, false);

			AlphaDataDownloader* downloader = new AlphaDataDownloader(dr);
			m_downloaders[ti] = downloader; 

			//IAlphaPtr alpha = helper::ShLib::global().createComponent<IAlpha>(alphaConfig, rt.dataRegistry);
			//ASSERT(alpha, "Unable to create alpha from config: " << alphaConfig.toString());

			//ASSERT(dci.universe, "No universe available in DataCacheInfo");
			//IUniverse* universe = dci.universe;

			///// Set the universe for the alpha
			//alpha->universe() = universe;

			//size_t size = universe->size(rt.di);
			//ASSERT(size, "Universe-%s" << universe->name() << " invalid size returned on di: " << rt.di);

			///// Create the buffer to contain the results of the alpha
			//alpha->data().ensureAlphaSize(1, size);

			downloader->m_alphaConfig = palphaConfig;
			downloader->m_universe = dci.universe;
			//downloader->m_alpha = alpha;
			//downloader->m_alphaConfig = alphaConfig;
			downloader->m_rt = dci.rt;
		}

		m_diCache = dci.diStart;

		if (numThreads > 1) {
			m_threads.resize(numThreads);

			/// create the threads
			//for (size_t ti = 0; ti < m_threads.size(); ti++) 
			//	m_threads[ti] = new Poco::Thread();

			/// Start the threads
			for (size_t ti = 0; ti < m_threads.size(); ti++) {
				size_t threadIndex = m_diCache % m_threads.size();
				m_threads[threadIndex] = new Poco::Thread();
				m_downloaders[threadIndex]->m_di = m_diCache;
				m_threads[threadIndex]->start(*(m_downloaders[threadIndex]));

				m_diCache++;
			}
		}

		///// run the alpha ...
		//alpha->run(rt);

		///// Invalidate stuff that does not exist ...

		//FloatMatrixDataPtr alphaData = alpha->data().alpha;

		//frame.data = alphaData;
		//frame.dataOffset = 0;
		//frame.dataSize = alphaData->dataSize();

		//CDebug("[%u]-[Count: %z x %z] - %s", dci.rt.dates[dci.di], alphaData->rows(), alphaData->cols(), dci.def.toString(dci.ds));

	}

	void AlphaData::build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
	    if (!m_downloaders.size())
		    initThreads(dr, dci);

		if (m_downloaders.size() == 1) {
			m_downloaders[0]->m_di = dci.di;
			m_downloaders[0]->build(dr);
			frame = m_downloaders[0]->m_dataFrame;
		}
		else {
			size_t threadIndex = dci.di % m_threads.size();
			m_threads[threadIndex]->join();
			frame = m_downloaders[threadIndex]->m_dataFrame;

			if (m_diCache <= dci.diEnd) {
				size_t newThreadIndex = m_diCache % m_threads.size();

				m_downloaders[newThreadIndex]->m_di = m_diCache;
				ASSERT(newThreadIndex == threadIndex, "Thread index mismatch. Expecting: " << threadIndex << " - Found: " << newThreadIndex << " - Num downloaders: " << m_downloaders.size());
				m_threads[threadIndex]->start(*(m_downloaders[threadIndex]));

				m_diCache++;
			}
		}

		CDebug("[%u] - %s [Count: %z x %z]", dci.rt.dates[dci.di], dci.def.toString(dci.ds), frame.data->rows(), frame.data->cols());
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createAlphaData(const pesa::ConfigSection& config) {
	return new pesa::AlphaData((const pesa::ConfigSection&)config);
}

