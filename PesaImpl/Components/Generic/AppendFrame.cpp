/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// AppendFrame.cpp
///
/// Created on: 25 Jan 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Poco/String.h"
#include "Framework/Pipeline/IDataRegistry.h"
#include "Framework/Components/IDataLoader.h"
#include "Framework/Helper/FrameworkUtil.h"

namespace pesa {
	class AppendFrame : public IDataLoader {
		typedef std::map<std::string, std::string> StringMap;

		StringMap				m_mappings;					/// The mappings
		StringMap				m_rmappings;				/// Reverse mappings

		std::string				m_datasetName;				/// The name of this dataset
		StringVec 				m_datasets; 				/// What datasets are we supposed to load

	public:
								AppendFrame(const ConfigSection& config);
		virtual 				~AppendFrame();

		////////////////////////////////////////////////////////////////////////////
		/// AppendFrame overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
	};

	AppendFrame::AppendFrame(const ConfigSection& config)
		: IDataLoader(config, std::string("AppendFrame")) {
		setLogChannel("AppendFrame");

		m_datasetName = config.getRequired("datasetName");
		std::string datasets = config.getString("datasets", "");
		if (!datasets.empty())
			m_datasets = Util::split(datasets, ",");

		std::string mappings = config.getRequired("mappings");
		FrameworkUtil::parseMappings(mappings, &m_mappings, &m_rmappings);
	}

	AppendFrame::~AppendFrame() {
		CTrace("Deleting AppendFrame");
	}

	void AppendFrame::initDatasets(IDataRegistry& dr, Datasets& dss) {
		Dataset ds(m_datasetName);
		for (const auto& dataset : m_datasets) {
			DataFrequency::Type frequency;
			DataType::Type type;
			std::string dataName;
			bool addToRequiredList = true;

			DataDefinition::parseDefString(dataset, dataName, type, frequency);

			DataValue dv(dataName, type, DataType::size(type), nullptr, frequency, 0);
			ds.add(dv);
		}

		dss.add(ds);
	}

	void AppendFrame::build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		try { 
			ASSERT(dci.di == dci.diEnd && dci.diStart == dci.diEnd, "Invalid AppendFrame call. Cannot call AppendFrame only on the last day: " << dci.di);
			std::string dataName = dci.def.name;
			std::string srcDataName = m_rmappings[dataName];

			ASSERT(!srcDataName.empty(), "Invalid mapping: " << dataName);
			DataPtr data = dr.getDataPtr(dci.universe, srcDataName);
			ASSERT(data, "Unable to load data: " << srcDataName);

			Data* dataCopy = data->clone(nullptr, false);

			frame.data = DataPtr(dataCopy);
			frame.dataOffset = 0;
			frame.dataSize = dataCopy->dataSize();

			CInfo("Updated data: [%-8u] - %s [Src = %s]", dci.rt.dates[dci.di], dataName, srcDataName);
		}
		catch (const std::exception& e) {
			Util::reportException(e);
		}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createAppendFrame(const pesa::ConfigSection& config) {
	return new pesa::AppendFrame((const pesa::ConfigSection&)config);
}

