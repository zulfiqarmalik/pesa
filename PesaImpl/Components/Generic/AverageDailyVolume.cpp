/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// AverageDailyVolume.cpp
///
/// Created on: 14 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class AverageDailyVolume : public IDataLoader {
	private:
		int						m_days = 20; 				/// Default to 20 days
		std::string				m_dataId = Constants::s_defVolume;	/// The default data id that we're going touse
		std::string				m_datasetName = "baseData";	/// The name of the target dataset that this is going to go ...
		std::string				m_dataName;					/// What is the name of this dataset

	public:
								AverageDailyVolume(const ConfigSection& config);
		virtual 				~AverageDailyVolume();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);

		virtual std::string 	id() const {
			return IComponent::id() + "." + Util::cast(m_days);
		}
	};

	AverageDailyVolume::AverageDailyVolume(const ConfigSection& config) : IDataLoader(config, "ADV") {
		m_days = config.get<int>("days", 20);
		m_dataId = config.getString("dataId", m_dataId);
		m_dataName = config.getString("name", "adv" + Util::cast(m_days));
		m_datasetName = config.getString("datasetName", m_datasetName);
	}

	AverageDailyVolume::~AverageDailyVolume() {
		CTrace("Deleting ADV%d", m_days);
	}

	void AverageDailyVolume::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		datasets.add(Dataset(m_datasetName, {
			DataValue(m_dataName, DataType::kFloat, sizeof(float), nullptr, DataFrequency::kDaily, 0),
		}));
	}

	void AverageDailyVolume::build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		size_t size = dci.universe->size(dci.di);
		auto data = std::make_shared<FloatMatrixData>(dci.universe, dci.def, 1, size);
		auto* volume = dr.get<FloatMatrixData>(dr.getSecurityMaster(), m_dataId);

		if (dci.di >= volume->rows()) {
			frame.noDataUpdate = true;
			return;
		}

		CDebug("ADV%d [%d] - Number of stocks: %z", m_days, (int)dci.rt.dates[dci.di], size);

		for (size_t ai = 0; ai < size; ai++) {
			Security::Index ii = dci.universe->index(dci.di, ai);
			ASSERT(ii != Security::s_invalidIndex, "Invalid index at point in universe: [" << dci.di << ", " << ai << "]");
			float adv = 0.0f;
			size_t numValidDays = 0;

			for (int di = dci.di; di >= 0 && di > dci.di - m_days; di--) {
				if (volume->isValid(di, ii)) {
					adv += (*volume)(di, ii);
					numValidDays++;
				}
			}

			if (numValidDays)
				(*data)(0, (UIntCount)ai) = adv / (float)numValidDays;
			else
				data->makeInvalid(0, (UIntCount)ai);
		}

		frame.data = data;
		frame.dataOffset = 0;
		frame.dataSize = data->dataSize();
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createAverageDailyVolume(const pesa::ConfigSection& config) {
	return new pesa::AverageDailyVolume((const pesa::ConfigSection&)config);
}

