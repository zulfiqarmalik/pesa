/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// BackPropagateAdjFactor.cpp
///
/// Created on: 07 May 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Components/IDataLoader.h"
#include "Framework/Data/MatrixData.h" 
#include "Framework/Pipeline/IDataRegistry.h"

#include "Poco/String.h"

namespace pesa {
	class BackPropagateAdjFactor : public IDataLoader { 
	private:
		FloatMatrixDataPtr		m_adjFactor;			/// Daily factor

		void					buildDailyFactor(IDataRegistry& dr, const DataCacheInfo& dci);

	public:
								BackPropagateAdjFactor(const ConfigSection& config);
		virtual 				~BackPropagateAdjFactor();

		////////////////////////////////////////////////////////////////////////////
		/// BackPropagateAdjFactor overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
	};

	BackPropagateAdjFactor::BackPropagateAdjFactor(const ConfigSection& config) 
		: IDataLoader(config, "BackPropagateAdjFactor") {
	}

	BackPropagateAdjFactor::~BackPropagateAdjFactor() {
		CTrace("Deleting BackPropagateAdjFactor");
	}

	void BackPropagateAdjFactor::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		std::string datasetName			= config().getRequired("datasetName");
		std::string dataName			= config().getRequired("dataName");

		datasets.add(Dataset(datasetName, {
			DataValue(dataName, DataType::kFloat, sizeof(float), nullptr, DataFrequency::kDaily, DataFlags::kAlwaysOverwrite),
		}));
	}

	void BackPropagateAdjFactor::buildDailyFactor(IDataRegistry& dr, const DataCacheInfo& dci) { 
		/// We've already built the dividend factor
		if (m_adjFactor)
			return;

		std::string factorDataId		= config().getRequired("factorData");
		const FloatMatrixData* factor	= dr.floatData(dci.universe, factorDataId);

		IntDay numRows					= (IntDay)factor->rows();
		size_t numCols					= factor->cols();

		m_adjFactor						= std::make_shared<FloatMatrixData>(dci.universe, dci.def, numRows, numCols);
		m_adjFactor->setMemory(1.0f);

		(*m_adjFactor)[dci.diEnd]		= (*factor)[dci.diEnd];

		/// We don't want to calculate the dividend factor on the first day of the cache. It makes no sense.
		for (IntDay di = dci.diEnd - 1; di >= 0; di--) {
			(*m_adjFactor)[di]			= (*m_adjFactor)[di + 1] * (*factor)[di + 1];
		}
	}

	void BackPropagateAdjFactor::build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		if (!m_adjFactor)
			buildDailyFactor(dr, dci);

		DataPtr data		= DataPtr(m_adjFactor->extractRow(dci.di - dci.diStart));
		frame.data			= data;
		frame.dataOffset	= 0;
		frame.dataSize		= data->dataSize();
		frame.noDataUpdate	= false;
	}

} /// namespace pesa

  ////////////////////////////////////////////////////////////////////////////
  /// To be called from the simulator
  ////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createBackPropagateAdjFactor(const pesa::ConfigSection& config) {
	return new pesa::BackPropagateAdjFactor((const pesa::ConfigSection&)config);
}

