/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// BackPropagateSharesOutstanding.cpp
///
/// Created on: 17 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Components/IDataLoader.h"
#include "Framework/Data/MatrixData.h" 
#include "Framework/Pipeline/IDataRegistry.h"

#include "Poco/String.h"

namespace pesa {
	class BackPropagateSharesOutstanding : public IDataLoader { 
	private:
		FloatMatrixDataPtr		m_sharesOutstanding;			/// Daily factor

		void					buildDailyFactor(IDataRegistry& dr, const DataCacheInfo& dci);

	public:
								BackPropagateSharesOutstanding(const ConfigSection& config);
		virtual 				~BackPropagateSharesOutstanding();

		////////////////////////////////////////////////////////////////////////////
		/// BackPropagateSharesOutstanding overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
	};

	BackPropagateSharesOutstanding::BackPropagateSharesOutstanding(const ConfigSection& config) 
		: IDataLoader(config, "BackPropagateSharesOutstanding") {
	}

	BackPropagateSharesOutstanding::~BackPropagateSharesOutstanding() {
		CTrace("Deleting BackPropagateSharesOutstanding");
	}

	void BackPropagateSharesOutstanding::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		std::string datasetName			= config().getRequired("datasetName");
		std::string dataName			= config().getRequired("dataName");

		datasets.add(Dataset(datasetName, {
			DataValue(dataName, DataType::kFloat, sizeof(float), nullptr, DataFrequency::kDaily, DataFlags::kAlwaysOverwrite),
		}));
	}

	void BackPropagateSharesOutstanding::buildDailyFactor(IDataRegistry& dr, const DataCacheInfo& dci) { 
		/// We've already built the dividend factor
		if (m_sharesOutstanding)
			return;

		std::string factorDataId		= config().getRequired("factorData");
		const FloatMatrixData* factor	= dr.floatData(dci.universe, factorDataId);

		IntDay numRows					= (IntDay)factor->rows();
		size_t numCols					= factor->cols();

		std::string srcDataId			= config().getRequired("srcData");
		const FloatMatrixData* srcData	= dr.floatData(dci.universe, srcDataId);

		m_sharesOutstanding				= std::make_shared<FloatMatrixData>(dci.universe, dci.def, numRows, numCols);
		m_sharesOutstanding->setMemory(1.0f);

		/// Copy the first row
		(*m_sharesOutstanding)[dci.diEnd]= (*srcData)[0];

		/// We don't want to calculate the dividend factor on the first day of the cache. It makes no sense.
		for (IntDay di = dci.diEnd - 1; di >= 0; di--) 
			(*m_sharesOutstanding)[di] = ((*m_sharesOutstanding)[di + 1] / (*factor)[di + 1]).round();
	}

	void BackPropagateSharesOutstanding::build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		if (!m_sharesOutstanding)
			buildDailyFactor(dr, dci);

		DataPtr data		= DataPtr(m_sharesOutstanding->extractRow(dci.di - dci.diStart));
		frame.data			= data;
		frame.dataOffset	= 0;
		frame.dataSize		= data->dataSize();
		frame.noDataUpdate	= false;
	}

} /// namespace pesa

  ////////////////////////////////////////////////////////////////////////////
  /// To be called from the simulator
  ////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createBackPropagateSharesOutstanding(const pesa::ConfigSection& config) {
	return new pesa::BackPropagateSharesOutstanding((const pesa::ConfigSection&)config);
}

