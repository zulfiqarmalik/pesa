/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// BasicTradingTimes.cpp
///
/// Created on: 15 Nov 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Framework/Helper/FileDataLoader.h"
#include "Poco/String.h"
#include "Core/Math/Stats.h"

namespace pesa {
	struct DSTInfo {
		IntDate 				start = 0;
		IntDate 				end = 0;
		Poco::Timespan			offset;
	};

	class BasicTradingTimes : public IDataLoader {
	protected:
		typedef std::unordered_map<IntDate, Poco::Timespan> DateTimespanMap;
		typedef std::unordered_map<IntDate, DSTInfo> YearDSTInfoMap;

		DateTimespanMap 		m_unusualTradingStartTime;	/// To cater for half days etc.
		DateTimespanMap 		m_unusualTradingEndTime;	/// To cater for half days etc.

		Poco::Timespan 			m_defaultStartTime;			/// The default starting time for this market
		Poco::Timespan			m_defaultEndTime;			/// The default ending time for this market

		YearDSTInfoMap			m_yearDSTMap;						/// The DST date map

		void					buildDatesMap(IDataRegistry& dr, const DataCacheInfo& dci);
		virtual void 			buildGeneric(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame, const Poco::Timespan& refTime);

	public:
								BasicTradingTimes(const ConfigSection& config);
		virtual 				~BasicTradingTimes();

		////////////////////////////////////////////////////////////////////////////
		/// SpGeneric overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {}
		virtual void 			buildOpen(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void 			buildClose(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void 			buildIsDst(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
	};

	BasicTradingTimes::BasicTradingTimes(const ConfigSection& config) : IDataLoader(config, "BasicTradingTimes") {
	}

	BasicTradingTimes::~BasicTradingTimes() {
		CTrace("Deleting BasicTradingTimes");
	}

	void BasicTradingTimes::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		Dataset ds("calendar");

		std::string market = parentConfig()->defs().market();

		ds.add(DataValue("gmt_open_" + market, DataType::kTimestamp, sizeof(int64_t), THIS_DATA_FUNC(BasicTradingTimes::buildOpen), DataFrequency::kDaily, 0));
		ds.add(DataValue("gmt_close_" + market, DataType::kTimestamp, sizeof(int64_t), THIS_DATA_FUNC(BasicTradingTimes::buildClose), DataFrequency::kDaily, 0));
		ds.add(DataValue("gmt_isdst_" + market, DataType::kUInt16, sizeof(uint16_t), THIS_DATA_FUNC(BasicTradingTimes::buildIsDst), DataFrequency::kDaily, 0));

		datasets.add(ds);
	}

	void BasicTradingTimes::buildDatesMap(IDataRegistry& dr, const DataCacheInfo& dci) {
		/// If the lookup map is not empty then we have constructed the dates already ...
		if (!m_yearDSTMap.empty())
			return;

		std::string tradingHoursFile = config().getRequired("tradingHours");
		tradingHoursFile = parentConfig()->defs().resolve(tradingHoursFile, 0U);

		CDebug("Loading trading hours from: %s", tradingHoursFile);

		helper::FileDataLoader loader(tradingHoursFile, ",", false, false);
		std::string lineStr;
		StringVec data = loader.read(&lineStr);
		size_t lineNumber = 0;
		const RuntimeData& rt = dci.rt;

		while (data.size() && ++lineNumber) {
			if (data[0] == "DST-ON") {
				IntDate date = Util::cast<IntDate>(data[1]);
				unsigned int year, month, day;
				Util::splitIntDate(date, year, month, day);

				ASSERT(m_yearDSTMap.find(year) == m_yearDSTMap.end(), "DST for this year has already been defined: " << year);

				m_yearDSTMap[year] = DSTInfo { date, 0, Util::parseTime(data[2]) };
			}
			else if (data[0] == "DST-OFF") {
				IntDate date = Util::cast<IntDate>(data[1]);
				unsigned int year, month, day;
				Util::splitIntDate(date, year, month, day);

				ASSERT(m_yearDSTMap.find(year) != m_yearDSTMap.end(), "DST-ON section NOT defined. DST-ON must precede DST-OFF: " << date);
				DSTInfo& dstDt = m_yearDSTMap[year];
				dstDt.end = date;
			}
			else if (data[0] == "TRADING-HOURS") {
				//rt.timeZone = Util::parseTime(data[1]);
				m_defaultStartTime = Util::parseTime(data[2]);
				m_defaultEndTime = Util::parseTime(data[3]);
			}
			else {
				IntDate date = Util::cast<IntDate>(data[0]);
				m_unusualTradingStartTime[date] = Util::parseTime(data[1]);
				m_unusualTradingEndTime[date] = Util::parseTime(data[2]);
			}

			data = loader.read(&lineStr);
		}
	}

	void BasicTradingTimes::buildOpen(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		buildGeneric(dr, dci, frame, m_defaultStartTime);
	}

	void BasicTradingTimes::buildClose(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		buildGeneric(dr, dci, frame, m_defaultEndTime);
	}

	void BasicTradingTimes::buildIsDst(pesa::IDataRegistry &dr, const pesa::DataCacheInfo &dci, pesa::DataFrame &frame) {
		buildDatesMap(dr, dci);

		IntDate date = dci.rt.dates[dci.di];
		Poco::DateTime dtToday = Util::intToDate(date);

		const auto dstIter = m_yearDSTMap.find(dtToday.year());
		UShortMatrixData* data = new UShortMatrixData(nullptr, 1, 1);

		/// Default is NO-DST
		(*data)(0, 0) = 0;

		/// If there is entry for DST
		if (dstIter != m_yearDSTMap.end()) {
			const DSTInfo& dstInfo = dstIter->second;

			if (dstInfo.start > 0 && date >= dstInfo.start && dstInfo.end > 0 && date < dstInfo.end)
				(*data)(0, 0) = 1;
		}
		else
			(*data)(0, 0) = 0;

		frame.data = DataPtr(data);
		frame.dataOffset = 0;
		frame.dataSize = data->dataSize();
	}

	void BasicTradingTimes::buildGeneric(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame, const Poco::Timespan& refTime) {
		buildDatesMap(dr, dci);

		IntDate date = dci.rt.dates[dci.di];
		Poco::DateTime dtToday = Util::intToDate(date);

		const auto dstIter = m_yearDSTMap.find(dtToday.year());

		//ASSERT(dstIter != m_yearDSTMap.end(), "Unable to find year: " << dtToday.year() << " in the date lookup map");

		Poco::DateTime tradingTime;
		Poco::Timespan startTime = refTime;

		/// If there is entry for DST
		if (dstIter != m_yearDSTMap.end()) {
			const DSTInfo& dstInfo = dstIter->second;

			if (dstInfo.start > 0 && date >= dstInfo.start && dstInfo.end > 0 && date < dstInfo.end) 
				startTime = startTime + dstInfo.offset;
		}
		else {
			//CInfo("Not using DST for year: %d", (int)dtToday.year());

			/// Do nothing to the startTime ...
		}

		tradingTime = Poco::DateTime(dtToday.year(), dtToday.month(), dtToday.day(), startTime.hours(), startTime.minutes(), startTime.seconds());

		int64_t time = tradingTime.timestamp().epochMicroseconds() * 1000; /// We're looking for Epoch NANO-seconds (Python)

		Int64MatrixData* data = new Int64MatrixData(dci.universe, dci.def, 1, 1);
		data->setValue(0, 0, time);

		frame.data = DataPtr(data);
		frame.dataOffset = 0;
		frame.dataSize = frame.data->dataSize();
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createBasicTradingTimes(const pesa::ConfigSection& config) {
	return new pesa::BasicTradingTimes((const pesa::ConfigSection&)config);
}

