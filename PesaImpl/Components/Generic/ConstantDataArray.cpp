/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// ConstantDataArray.cpp
///
/// Created on: 19 Aug 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// ConstantDataArray - Loads a constant array for some securities
	////////////////////////////////////////////////////////////////////////////
	class ConstantDataArray : public IDataLoader {
	private:
		typedef std::vector<FloatVec> FloatVecVec;
		std::string 			m_name;
		SizeVec					m_index;
		StringVec				m_dataIds;
		FloatVecVec				m_data;

	public:
								ConstantDataArray(const ConfigSection& config);
		virtual 				~ConstantDataArray();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);
	};

	ConstantDataArray::ConstantDataArray(const ConfigSection& config) : IDataLoader(config, "CONSTANT_ARRAY") {
		m_name = config.get("name");
		ASSERT(!m_name.empty(), "ConstantDataArray must have a name!");

		const ConfigSection* valuesSec = config.getChildSection("Values");
		ASSERT(valuesSec, "ConstantDataArray must have a values section with FIGIs and data for each FIGI");

		const ConfigSection::StrMap& ids = valuesSec->map();

		for (auto iter = ids.begin(); iter != ids.end(); iter++) {
			auto& uuid = iter->first;
			auto& values = iter->second;
			auto valuesVec = Util::split(values, " ");
			FloatVec data;

			for (auto& value : valuesVec) {
				float fvalue = Util::cast<float>(value);
				data.push_back(fvalue);
			}

			m_index.push_back(0);
			m_data.push_back(data);
			m_dataIds.push_back(uuid);
		}
	}

	ConstantDataArray::~ConstantDataArray() {
		CTrace("Deleting ConstantDataArray-%s", m_name);
	}

	void ConstantDataArray::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		datasets.add(Dataset("Constant", {
			DataValue(m_name, DataType::kFloat, sizeof(float), nullptr, DataFrequency::kDaily, 0),
		}));
	}

	void ConstantDataArray::build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		size_t size = dci.universe->size(dci.di);
		auto data = std::make_shared<FloatMatrixData>(dci.universe, dci.def, 1, size);

		/// make all the data invalid first ...
		data->invalidateMemory();

		for (size_t i = 0; i < m_dataIds.size(); i++) {
			auto& uuid = m_dataIds[i];
			auto& index = m_index[i];
			auto& values = m_data[i];
			auto* sec = dci.universe->mapSymbol(uuid);

			ASSERT(sec && sec->index != Security::s_invalidIndex, "Unable to resolve security: " << uuid);

			if (index >= values.size())
				index = 0;

			(*data)(0, sec->index) = values[index++];
		}

		frame.data = data;
		frame.dataOffset = 0;
		frame.dataSize = data->dataSize();
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createConstantDataArray(const pesa::ConfigSection& config) {
	return new pesa::ConstantDataArray((const pesa::ConfigSection&)config);
}

