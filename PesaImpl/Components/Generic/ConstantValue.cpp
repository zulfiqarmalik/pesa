/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// ConstantValue.cpp
///
/// Created on: 02 Dec 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class ConstantValue : public IDataLoader {
	private:
		float					m_constValue = 0.0f;				/// Constant ConstantValue value (if any)
		std::string 			m_datasetName = "TEMP";				/// The name of the dataset
		StringVec 				m_datasets;							/// The name of the data value

		virtual void 			buildConstant(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);

	public:
								ConstantValue(const ConfigSection& config);
		virtual 				~ConstantValue();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);

		////////////////////////////////////////////////////////////////////////////
		/// IUniverse overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			combine(FloatMatrixData& thisAlpha, IUniversePtr other, FloatMatrixData& otherAlpha) {}
	};

	ConstantValue::ConstantValue(const ConfigSection& config) : IDataLoader(config, "ConstantValue") {
		std::string cvalue;

		if (config.get("value", cvalue))
			m_constValue = Util::cast<float>(cvalue);

		m_datasetName = config.getRequired("datasetName");
		auto datasets = config.getRequired("datasets");
		m_datasets = Util::split(datasets, ",");
		ASSERT(m_datasets.size(), "Invalid datasets: " << datasets);
	}

	ConstantValue::~ConstantValue() {
		CTrace("Deleting ConstantValue!");
	}

	void ConstantValue::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		for (auto& dataset : m_datasets) {
			datasets.add(Dataset(m_datasetName, {
				DataValue(dataset, DataType::kFloatConstant, sizeof(float), THIS_DATA_FUNC(ConstantValue::buildConstant), DataFrequency::kStatic, 
					DataFlags::kUpdatesInOneGo | DataFlags::kAlwaysOverwrite),
			}));
		}
	}

	void ConstantValue::buildConstant(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		frame.data = std::make_shared<ConstantFloat>(m_constValue, dci.def);
		frame.dataSize = frame.data->dataSize();
		frame.dataOffset = 0;
	}

	void ConstantValue::build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createConstantValue(const pesa::ConfigSection& config) {
	return new pesa::ConstantValue((const pesa::ConfigSection&)config);
}

