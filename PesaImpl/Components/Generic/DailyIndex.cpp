/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// DailyIndex.cpp
///
/// Created on: 15 Aug 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Poco/String.h"
#include "Poco/File.h"
#include "Helper/JSON/gason.hpp"

namespace pesa {
	typedef std::vector<StringMap> StringMapVec;
	typedef std::shared_ptr<StringMapVec> StringMapVecPtr;
	////////////////////////////////////////////////////////////////////////////
	/// DailyIndex - Constructs daily indices
	////////////////////////////////////////////////////////////////////////////
	class DailyIndex : public IDataLoader {
	private:

		typedef std::unordered_map<std::string, StringMapVecPtr> StringMapVecPtr_Map;
		StringVec				m_indices;								/// The indices that we're going to use
		std::string				m_indexPath;							/// The path to where all the indices are put

		StringMap				m_symbolLUT;							/// Symbol mapping lookup table

		StringMapVecPtr_Map		m_symbolCache;							/// The symbol cache that we've constructed

		std::string				m_tickerDataId = "bloomberg.ticker";	/// What is the ID of the ticker data
		std::string				m_bbTickerDataId = "bloomberg.bbTicker";/// What is the ID of the ticker data
		std::string				m_figiDataId = "bloomberg.figi";		/// What is the FIGI of the ticker data
		std::string				m_cfigiDataId = "bloomberg.cfigi";		/// What is the C-FIGI of the ticker data
		std::string				m_isinDataId = "bloomberg.isin";		/// What is the ISIN of the ticker data

		void					constructFigiLUT(IDataRegistry& dr);
		std::string				mapSymbol(const std::string& symbol);

		StringMapVecPtr			buildSymbolCache(IDataRegistry& dr, const DataCacheInfo& dci);
		static std::string		makeKey(const DataCacheInfo& dci);

	public:
								DailyIndex(const ConfigSection& config);
		virtual 				~DailyIndex();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
	};

	DailyIndex::DailyIndex(const ConfigSection& config) 
		: IDataLoader(config, "DailyIndex") {
		auto indicesStr			= config.getResolvedRequiredString("indices");
		m_indices				= Util::split(indicesStr, "|");
		m_indexPath				= config.getResolvedRequiredString("indexPath");

		m_tickerDataId 			= config.getString("tickerDataId", m_tickerDataId);
		m_bbTickerDataId 		= config.getString("bbTickerDataId", m_bbTickerDataId);
		m_figiDataId 			= config.getString("figiDataId", m_figiDataId);
		m_cfigiDataId 			= config.getString("cfigiDataId", m_cfigiDataId);
		m_isinDataId 			= config.getString("isinDataId", m_isinDataId);
	}

	DailyIndex::~DailyIndex() {
		CTrace("Deleting DailyIndex!");
	}

	void DailyIndex::initDatasets(IDataRegistry& dr, Datasets& dss) {
		ASSERT(m_indices.size(), "No indices specified!");

		auto name = config().getRequired("datasetName");
		//Dataset ds(name);

		for (const auto& index : m_indices) {
			Dataset ds(index);

			ds.add(DataValue("cfigi", DataType::kIndexedString, 0, DataFrequency::kDaily, 0));
			ds.add(DataValue("figi", DataType::kIndexedString, 0, DataFrequency::kDaily, 0));
			ds.add(DataValue("isin", DataType::kIndexedString, 0, DataFrequency::kDaily, 0));
			ds.add(DataValue("ticker", DataType::kIndexedString, 0, DataFrequency::kDaily, 0));

			dss.add(ds);
		}

	}

	std::string DailyIndex::mapSymbol(const std::string& symbol) {
		auto iter = m_symbolLUT.find(symbol);

		if (iter != m_symbolLUT.end())
			return iter->second;

		return "";
	}

	void DailyIndex::constructFigiLUT(IDataRegistry& dr) {
		if (m_symbolLUT.size())
			return;

		const StringData* ticker		= dr.stringData(nullptr, m_tickerDataId, true);
		ASSERT(ticker, "Unable to load data: " << m_tickerDataId);
		
		const StringData* bbTicker		= dr.stringData(nullptr, m_bbTickerDataId, true);
		ASSERT(bbTicker, "Unable to load data: " << m_bbTickerDataId);
		
		const StringData* figi			= dr.stringData(nullptr, m_figiDataId, true);
		ASSERT(figi, "Unable to load data: " << m_figiDataId);
		
		const StringData* cfigi			= dr.stringData(nullptr, m_cfigiDataId, true);
		ASSERT(cfigi, "Unable to load data: " << m_cfigiDataId);
		
		const StringData* isin			= dr.stringData(nullptr, m_isinDataId, true);
		ASSERT(isin, "Unable to load data: " << m_isinDataId);

		auto tickerCount				= ticker->cols();
		auto bbTickerCount				= bbTicker->cols();
		auto figiCount					= figi->cols();
		auto cfigiCount					= cfigi->cols();
		auto isinCount					= isin->cols();

		ASSERT(tickerCount, "No columns for the ticker data!");
		
		ASSERT(tickerCount == bbTickerCount, Poco::format("Ticker and BB-Ticker count mismatch. Ticker Count: %z - BB-Ticker Count: %z", tickerCount, bbTickerCount));
		ASSERT(tickerCount == figiCount, Poco::format("Ticker and FIGI count mismatch. Ticker Count: %z - FIGI Count: %z", tickerCount, figiCount));

		CDebug("Constructing LUT for num tickers: %z", tickerCount);

		for (auto ii = 0; ii < tickerCount; ii++) {
			auto tickerId				= ticker->getValue(0, ii);
			auto bbTickerId				= bbTicker->getValue(0, ii);
			auto figiId					= figi->getValue(0, ii);
			auto cfigiId				= cfigi->getValue(0, ii);
			auto isinId					= isin->getValue(0, ii);

			auto spacePos				= tickerId.find(' ');

			if (isinId == "nan")
				isinId					= "";

			if (spacePos != std::string::npos)
				tickerId				= tickerId.substr(0, spacePos);

			if (!cfigiId.empty()) {
				m_symbolLUT[tickerId]	= cfigiId;
				m_symbolLUT[bbTickerId]	= cfigiId;
				m_symbolLUT[figiId]		= cfigiId;
				m_symbolLUT[cfigiId]	= cfigiId;

				if (!isinId.empty())
					m_symbolLUT[isinId] = cfigiId;
			}
		}

		CDebug("Total LUT Count: %z", m_symbolLUT.size());
	}

	void DailyIndex::build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		StringMapVecPtr symData = buildSymbolCache(dr, dci);
		IntDate date = dci.rt.dates[dci.di];

		if (!symData || !symData->size()) {
			frame.data			= nullptr;
			frame.dataOffset	= 0;
			frame.dataSize		= 0;
			frame.noDataUpdate	= true;

			return;
		}

		ShortIndexedStringData* data = new ShortIndexedStringData(nullptr, dci.def);
		data->setMetadata(dci.customMetadata);

		data->ensureSize(1, symData->size());

		std::string dataName = dci.def.name;

		for (size_t ii = 0; ii < symData->size(); ii++) {
			const StringMap& secData = symData->at(ii);
			auto iter = secData.find(dataName);
			ASSERT(iter != secData.end(), Poco::format("[%u] Unable to find key: %s at symbol data entry: %z", date, dataName, ii));
			data->setValue(0, ii, iter->second);
		}

		frame.data			= DataPtr(data);
		frame.dataSize		= frame.data->dataSize();
		frame.noDataUpdate	= false;
		frame.customMetadata= data->getMetadata();

		CDebug("[%u]-[Count: %z] - Loaded index: %s.%s", date, symData->size(), dci.ds.name, dataName);
	}

	std::string DailyIndex::makeKey(const DataCacheInfo& dci) {
		return Poco::format("%s.%u", dci.ds.name, dci.rt.dates[dci.di]);
	}

	StringMapVecPtr DailyIndex::buildSymbolCache(IDataRegistry& dr, const DataCacheInfo& dci) {
		std::string key = makeKey(dci);
		auto symIter = m_symbolCache.find(key);

		if (symIter != m_symbolCache.end())
			return symIter->second;

		/// OK, a new key has been requested here. Clear the old cache
		m_symbolCache.clear();

		IntDay di = std::max(dci.di - 1, 0);
		IntDate date = dci.rt.dates[di];
		std::string indexName = dci.ds.name;
		std::string contentsFilename = Util::cast(date) + ".json";
		std::string zipFilename = (Poco::Path(m_indexPath).append(indexName).append(contentsFilename + ".zip")).toString();
		Poco::File zipFile(zipFilename);

		/// If the source file does not exist then we just return an empty frame
		if (!zipFile.exists()) {
			m_symbolCache[key] = nullptr;
			CWarning("Unable to find zip file: %s", zipFilename);
			return nullptr;
		}

		/// Read the contents of the JSON. 
		std::string indexDataStr = Util::readEntireTextFileFromZip(zipFilename, contentsFilename);
		size_t jsonDataLen = indexDataStr.length() + 1;
		char* jsonData = new char[jsonDataLen];
		memcpy(jsonData, indexDataStr.c_str(), indexDataStr.length());
		jsonData[jsonDataLen - 1] = '\0';

		char* startPtr = jsonData;
		char* endPtr = nullptr;
		gason::JsonAllocator jsonAllocator;
		gason::JsonValue jsonResult;

		gason::JsonParseStatus jsonStatus = gason::Json::parse(startPtr, &endPtr, &jsonResult, jsonAllocator);

		ASSERT(jsonStatus == gason::JSON_PARSE_OK, Poco::format("Unable to parse json in file: %s@%s", zipFilename, contentsFilename));

		auto indicesList = jsonResult.child("results");
		auto iter = gason::begin(indicesList);

		ASSERT(iter.isValid(), Poco::format("Unsupported JSON in file: %s@%s", zipFilename, contentsFilename));

		StringMapVecPtr symData = std::make_shared<StringMapVec>();

		for (gason::JsonIterator iter = gason::begin(indicesList); iter != gason::end(indicesList); iter++) {
			auto value = iter->value;
			auto tag = value.getTag();

			StringMap secData;

			secData["cfigi"] = std::string(value.child("cfigi").toString());
			secData["figi"] = std::string(value.child("figi").toString());
			secData["isin"] = std::string(value.child("isin").toString());
			secData["ticker"] = std::string(value.child("ticker").toString());

			if (!secData["cfigi"].empty()) {
				symData->push_back(secData);
			}
		}

		delete[] jsonData;

		if (!symData->size()) {
			//frame.noDataUpdate = true;
			//return;

			m_symbolCache[key] = nullptr;
			return nullptr;
		}

		m_symbolCache[key] = symData;
		return symData;
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createDailyIndex(const pesa::ConfigSection& config) {
	return new pesa::DailyIndex((const pesa::ConfigSection&)config);
}

