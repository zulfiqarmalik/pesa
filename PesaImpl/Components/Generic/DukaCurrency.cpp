/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// DukaCurrency.cpp
///
/// Created on: 16 Jul 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Components/IDataLoader.h"
#include "Framework/Helper/FrameworkUtil.h"
#include "Framework/Helper/FileDataLoader.h"
#include "Framework/Data/MatrixData.h"

#include "Poco/DateTimeParser.h"
#include "Poco/String.h"

namespace pesa {
	struct TimingInfo {
		std::string				currency;
		int 					ihour;
		std::string				hour;
		std::string				minute;
	};

	using namespace				pesa::helper;

	class DukaCurrency : public IDataLoader {
	private:
		std::string				m_separator = ",";	/// The CSV separator
		StringVec				m_currencies;		/// The currency pairs that we'll be getting
		StringVec				m_times =	{	
												"0000","0030","0100","0130","0200","0230","0300","0330","0400","0430","0500","0530","0600","0630","0700","0730",
												"0800","0830","0900","0930","1000","1030","1100","1130","1200","1230","1300","1330","1400","1430","1500","1530",
												"1600","1630","1700","1730","1800","1830","1900","1930","2000","2030","2100","2130","2200","2230","2300","2330"
											};

		std::vector<TimingInfo>	m_timings;			/// The timings array
		FileDataLoaderPtr		m_file;				/// The currency data file
		StringVec				m_values;			/// The last set of values that we loaded
		FloatVec				m_fvalues;			/// Last floating point values
		std::string				m_timeFormat;		/// The format of the time
		IntDay					m_diLast = -1;		/// The last day that we loaded
		
		int						m_timingIndex = -1;
		std::string             m_oldTime;

		std::string				getConfigVar(std::string name, IntDate date, const TimingInfo& timing);

	public:
								DukaCurrency(const ConfigSection& config);
		virtual 				~DukaCurrency();

		////////////////////////////////////////////////////////////////////////////
		/// DukaCurrency overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
	};

	DukaCurrency::DukaCurrency(const ConfigSection& config) 
		: IDataLoader(config, "Duka") {
		m_separator = config.getString("separator", m_separator);
		m_timeFormat = config.getRequired("timeFormat");

		auto timesStr = config.getString("times", "");
		if (!timesStr.empty())
			m_times = Util::split(timesStr, ",");

		m_fvalues.resize(5);
		memset(&m_fvalues[0], 0, sizeof(float) * m_fvalues.size());
	}

	DukaCurrency::~DukaCurrency() {
		CTrace("Deleting DukaCurrency");
	}

	void DukaCurrency::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		auto datasetName = config().getRequired("datasetName");
		auto currenciesStr = config().getResolvedRequiredString("currencies");

		m_currencies = Util::split(currenciesStr, ",");

		for (auto& currency : m_currencies) {
			for (auto& time : m_times) {
				ASSERT(time.size() == 4, "Invalid time: " << time << ". Must be of the format HHmm in 24 hour format!");

				std::string hour = time.substr(0, 2);
				std::string minute = time.substr(2, 4);
				int ihour = Util::cast<int>(hour);

				m_timings.push_back({ currency, ihour, hour, minute });

				size_t index = m_timings.size() - 1;

				datasets.add(Dataset(datasetName, {
					DataValue(currency + "_" + time + "_close", DataType::kFloat, sizeof(float), nullptr, DataFrequency::kDaily, 0, reinterpret_cast<void*>(index)),
				}));
			}
		}
	}

	std::string DukaCurrency::getConfigVar(std::string name, IntDate date, const TimingInfo& timing) {
		auto str = config().getRequired(name);
		str = Poco::replace(str, "{$CURRENCY}", timing.currency.c_str());
		return parentConfig()->defs().resolve(str, date);
	}

	void DukaCurrency::build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		try { 
			int index = (int)reinterpret_cast<size_t>(dci.dv.customInfo);
			ASSERT(index >= 0 && index < m_timings.size(), "Invalid timing info retrieved from customInfo: " << index << " - Def: " << dci.def.toString(dci.ds));

			const TimingInfo* oldTiming = m_timingIndex >= 0 && m_timingIndex < (int)m_timings.size() ? &m_timings[m_timingIndex] : nullptr;
            std::string oldTimingHour = m_oldTime;
			const TimingInfo& timing = m_timings[index];
			std::string hour = timing.hour;

			/// If it's a DST date then move one hour ahead ...
			if (dci.rt.isDST[dci.di])
				hour = Util::castPadded(timing.ihour - 1, '0');

			std::string requiredTime = hour + timing.minute;

			IntDate date = dci.rt.dates[dci.di];
			StringVec values;
			bool loadFile = true;
			bool loadValues = true;

			if (m_diLast == dci.di && oldTiming) {
				if (timing.currency == oldTiming->currency) {
					loadFile = false;

					if (requiredTime == m_oldTime) {
						values = m_values;
						loadValues = false;
					}
				}
			}

			if (loadFile) {
				auto baseDir = getConfigVar("baseDir", date, timing);
				auto currentFilename = getConfigVar("currentFilename", date, timing);
				auto zipFilename = getConfigVar("zipFilename", date, timing);
				Poco::Path path(baseDir);
				path.append(currentFilename + "@" + zipFilename);

				m_file = std::make_shared<FileDataLoader>(path.toString(), m_separator.c_str(), false, true, nullptr, true);
			}

			if (loadValues) {
				bool tsFound = false;

				while (!tsFound && !m_file->eof()) {
					values = m_file->read();

					if (values.size()) {
						auto dtStr = values[0];
						Poco::DateTime dt;
						int tz = 0;

						Poco::DateTimeParser::parse(m_timeFormat, dtStr, dt, tz);

						//auto dt = Util::parseFormattedDate(dtStr, m_timeFormat);
						auto time = Util::castPadded(dt.hour(), 2) + Util::castPadded(dt.minute(), 2);

						if (time == requiredTime)
							tsFound = true;
					}
				}
			}

			m_timingIndex = index;
			m_oldTime = requiredTime;
			m_values = values;
			m_diLast = dci.di;

			size_t valueIndex = 1;
			std::string dataName = dci.def.name;

//			if (dataName.find("_open") != std::string::npos)
//				valueIndex = 1;
			if (dataName.find("_close") != std::string::npos)
				valueIndex = 1;
//			else if (dataName.find("_high") != std::string::npos)
//				valueIndex = 3;
//			else if (dataName.find("_low") != std::string::npos)
//				valueIndex = 4;

			float fvalue = m_fvalues[valueIndex];

			ASSERT(m_file, "Unable to load file for currency: " << timing.currency << " - Time slot: " << requiredTime);

			/// Now we try to read the file ...
			FloatMatrixData* data = new FloatMatrixData(dci.universe, dci.def);
			data->ensureSize(1, 1, false);
			data->makeInvalid(0, 0);

			if (valueIndex < values.size())
				fvalue = Util::cast<float>(values[valueIndex]);

			(*data)(0, 0)			= fvalue;
			m_fvalues[valueIndex]	= fvalue;

			frame.data				= DataPtr(data);
			frame.dataSize			= data->dataSize();
			frame.dataOffset		= 0;
			frame.noDataUpdate		= true;

			CInfo("Updated currency: [%-8u]-[%s] - %s", date, timing.currency, dataName);
		}
		catch (const std::exception& e) {
			Util::reportException(e);
		}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createDukaCurrency(const pesa::ConfigSection& config) {
	return new pesa::DukaCurrency((const pesa::ConfigSection&)config);
}

