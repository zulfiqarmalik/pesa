/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// GenericDataLoader.cpp
///
/// Created on: 03 Feb 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "./GenericDataLoader.h"
#include "Helper/JSON/gason.hpp"

#include "Poco/String.h"
#include "Poco/File.h"

namespace pesa {
	GenericDataLoader::DataCacheThread::DataCacheThread(GenericDataLoader* self_, const RuntimeData* rt_, IntDay di_, DataDefinition def_, DataPtr customMetadata_)
		: self(self_), rt(rt_), di(di_), def(def_), customMetadata(customMetadata_) {
		promise = new std::promise<int>;
	}

	GenericDataLoader::DataCacheThread::~DataCacheThread() {
		delete promise;
	}

	void GenericDataLoader::DataCacheThread::run() {
		int errCode = self->preCacheDayAsync(rt, di, def, customMetadata);
		promise->set_value(errCode);
	}

	GenericDataLoader::CacheInfo::~CacheInfo() {
		delete dcObject;
		delete thread;
	}

	//////////////////////////////////////////////////////////////////////////
	GenericDataLoader::GenericDataLoader(const ConfigSection& config, std::string logger) 
		: IDataLoader(config, logger) { 

		/// For debug builds we just use a single thread
#if defined(NDEBUG)
		std::string maxPrecacheStr = config.getResolvedString("maxPrecache", Util::cast(s_maxPrecache));
		m_maxPrecache			= Util::cast<IntDay>(maxPrecacheStr);
#else
		m_maxPrecache			= 1;
#endif /// NDEBUG

		//m_maxPrecache			= config.get<IntDay>("maxPrecache", s_maxPrecache);
		m_datasetName			= config.getString("datasetName", "");
		m_alwaysOverwrite		= config.getBool("alwaysOverwrite", m_alwaysOverwrite);
		m_idTypes				= Util::split(config.getString("idType", ""), ",");
		m_addNewUuid			= config.getBool("addNewUuid", false);
		m_stringIndices			= config.getBool("stringIndices", false);
		m_needsId				= config.getBool("needsId", false);

		std::string mappings	= config.getString("mappings", "");
		if (!mappings.empty()) 
			FrameworkUtil::parseMappings(mappings, &m_mappings, &m_rmappings);

		std::string datasets	= config.getString("datasets", "");
		if (!datasets.empty())
			m_datasets			= Util::split(datasets, ",");

		m_baseDir				= parentConfig()->defs().resolve(config.getRequired("baseDir"), (Poco::DateTime*)nullptr);
		m_historicalFilename	= config.getString("historicalFilename", "");
		m_currentFilename		= config.getRequired("currentFilename");
		m_dateIndex				= config.get<int>("dateIndex", m_dateIndex);
		//m_idIndexes				= !m_stringIndices ? config.get<int>("idIndex", m_idIndexes) : -1;
		m_hasHeader				= config.getBool("hasHeader", m_hasHeader);

		if (!m_stringIndices) {
			auto idIndexesStr	= config.getString("idIndex", "");

			if (!idIndexesStr.empty()) {
				m_idIndexes.clear();

				auto idIndexes	= Util::split(idIndexesStr, ",");

				for (const auto& idIndex : idIndexes) 
					m_idIndexes.push_back(Util::cast<int>(idIndex));
			}
		}
		else 
			m_idIndexes			= { -1 };

		std::string matchColumnsInfo = config.getString("matchColumns", "");
		if (!matchColumnsInfo.empty()) {
			auto parts = Util::split(matchColumnsInfo, ",");
			for (const auto& part : parts) {
				auto mappings = Util::split(part, "=");
				ASSERT(mappings.size() == 2, "Invalid mapping: " << part);
				int index = Util::cast<int>(mappings[0]);
				std::string dataId = mappings[1];
				m_matchColumns.push_back({ index, dataId });
			}
		}
	}

	GenericDataLoader::~GenericDataLoader() {
		CTrace("Deleting GenericDataLoader!");
	}

	void GenericDataLoader::initDatasets(IDataRegistry& dr, Datasets& dss) {
		Dataset ds(m_datasetName);

		for (std::string dataset : m_datasets) {
			DataFrequency::Type frequency;
			DataType::Type type;
			std::string dataName;
			bool addToRequiredList = true;

			DataDefinition::parseDefString(dataset, dataName, type, frequency);

			auto mappingIter = m_mappings.find(dataset);
			//ASSERT(mappingIter != m_mappings.end(), "Unable to find mapping for dataset: " << dataset << " [Dataset: " << m_datasetName << "]");

			if (mappingIter != m_mappings.end() && mappingIter->first != dataName)
				m_mappings[dataName] = mappingIter->second;

			m_datasetNames.push_back(dataName);
			m_datasetTypes.push_back((int)type);

			int flags = 0;

			if (frequency != DataFrequency::kStatic) {
				DataValue dv(dataName, type, DataType::size(type), THIS_DATA_FUNC(GenericDataLoader::buildDaily), frequency, m_alwaysOverwrite ? DataFlags::kAlwaysOverwrite : 0);
				ds.add(dv);
			}
			else {
				m_isUniversal = true;
				int flags = DataFlags::kUpdatesInOneGo | DataFlags::kAlwaysOverwrite;

				//if (m_addNewUuid && m_idType == dataName) {
				//	flags |= DataFlags::kPreLoadExisting;
				//}

				DataValue dv(dataName, type, DataType::size(type), THIS_DATA_FUNC(GenericDataLoader::buildDaily), frequency, flags);
				ds.add(dv);
			}
		}

		dss.add(ds);
	}

	void GenericDataLoader::loadUUIDs(IDataRegistry& dr) {
		if (m_idTypes.empty() || m_addNewUuid)
			return;

		for (size_t id = 0; id < m_idTypes.size(); id++) { 
			const auto& idType = m_idTypes[id];
			bool isData = idType.find(".") != std::string::npos;

			if (!isData) {
				if (idType == "FIGI" || idType == "UUID" || idType == "CFIGI") 
					loadFIGIs(dr);
				else if (idType == "ISIN") 
					loadISINs(dr);
				else if (idType == "TICKER") 
					loadTickers(dr);
				else if (idType == "BB_TICKER") 
					loadBBTickers(dr);
				else {
					ASSERT(false, "Invalid/Unsupported idType: " << idType);
				}
			}
			else {
				if (m_idTypes.size() == 1) {
					m_idData = dr.stringData(dr.getSecurityMaster(), idType, true);
					return;
				}
				else {
					auto idData = dr.stringData(dr.getSecurityMaster(), idType, true);

					ASSERT(idData, "Unable to load id data: " << idType);
					ASSERT(idData->rows() == 1, "Only universal datasets are allowed in this format. Dataset not universal: " << idType);

					SizeMap uuidMap;

					for (size_t ii = 0; ii < idData->cols(); ii++) {
						const std::string& id = idData->getValue(0, ii);
						if (!id.empty())
							uuidMap[id].push_back(ii);
					}

					m_uuidMaps.push_back(uuidMap);
				}
			}

		}

	}

	void GenericDataLoader::loadFIGIs(IDataRegistry& dr) {
		auto* secMaster = dr.getSecurityMaster(); 
		size_t size = secMaster->size(0);
		SizeMap uuidMap;

		for (size_t ii = 0; ii < size; ii++) {
			auto sec = secMaster->security(0, (Security::Index)ii);
			auto id = sec.uuidStr();
			if (!id.empty())
				uuidMap[id].push_back(ii);
		}

		m_uuidMaps.push_back(uuidMap);
	}

	void GenericDataLoader::loadISINs(IDataRegistry& dr) {
		auto* secMaster = dr.getSecurityMaster();
		size_t size = secMaster->size(0);
		SizeMap uuidMap;

		for (size_t ii = 0; ii < size; ii++) {
			auto sec = secMaster->security(0, (Security::Index)ii);
			auto id = sec.isinStr();
			if (!id.empty())
				uuidMap[id].push_back(ii);
		}

		m_uuidMaps.push_back(uuidMap);
	}

	void GenericDataLoader::loadTickers(IDataRegistry& dr) {
		auto* secMaster = dr.getSecurityMaster();
		size_t size = secMaster->size(0);
		SizeMap uuidMap;

		for (size_t ii = 0; ii < size; ii++) {
			auto sec = secMaster->security(0, (Security::Index)ii);
			auto id = sec.tickerStr();
			if (!id.empty())
				uuidMap[id].push_back(ii);
		}

		m_uuidMaps.push_back(uuidMap);
	}

	void GenericDataLoader::loadBBTickers(IDataRegistry& dr) {
		auto* secMaster = dr.getSecurityMaster();
		size_t size = secMaster->size(0);
		SizeMap uuidMap;

		for (size_t ii = 0; ii < size; ii++) {
			auto sec = secMaster->security(0, (Security::Index)ii);
			auto id = sec.bbTickerStr();
			if (!id.empty())
				uuidMap[id].push_back(ii);
		}

		m_uuidMaps.push_back(uuidMap);
	}

	void GenericDataLoader::clearCache(IntDate date) {
		auto iter = m_caching.find(date);
		if (iter == m_caching.end())
			return;

		/// Otherwise we wait for the data to finish caching first
		CacheInfoPtr ci = iter->second;

		/// Only wait if the future is still valid ...
		if (ci->future.valid())
			ci->future.wait();

		m_caching.erase(iter);
	}

	void GenericDataLoader::preCache(const DataCacheInfo& dci) {
		/// Delete 2 day old data
		if (dci.di >= 2)
			clearCache(dci.rt.dates[dci.di - 2]);

		/// See how many items do we have left
		if (m_caching.size() >= m_maxPrecache)
			return;

		if (m_diLast < 0)
			m_diLast = dci.di;

		size_t numExisting = m_caching.size();
		while (m_caching.size() < (size_t)m_maxPrecache && m_diLast <= dci.rt.diEnd) {
			preCacheDay(dci, m_diLast);
			if (m_caching.size() > numExisting) {
				numExisting = m_caching.size();
			}
			m_diLast++;
		}
	}

	std::string GenericDataLoader::getFilename(const RuntimeData& rt, IntDay di, bool& isHistorical) {
		IntDate date = rt.dates[di];
		std::string currFilename = Poco::Path(m_baseDir).append(m_currentFilename).absolute().toString();
		std::string histFilename;

		if (!m_historicalFilename.empty()) {
			std::string histFilename = Poco::Path(m_baseDir).append(m_historicalFilename).absolute().toString();
			histFilename = parentConfig()->defs().resolve(histFilename, date);
		}

		currFilename = parentConfig()->defs().resolve(currFilename, date);

		std::string filename;
		Poco::File currFile(currFilename);

		if (currFile.exists()) {
			filename = currFilename;
			isHistorical = false;
		}
		else {
			if (histFilename.empty())
				return "";

			Poco::File histFile(histFilename);
			if (!histFile.exists())
				return "";

			isHistorical = true;
			filename = histFilename;
		}

		return filename;
	}

	SizeVec* GenericDataLoader::findUuidInternal(const std::string& uuid, const RuntimeData* rt, IntDay di, size_t index /* = 0 */) { 
		ASSERT(index < m_uuidMaps.size(), "Invalid uuid map index: " << index << " - Max count: " << m_uuidMaps.size());

		auto& uuidMap = m_uuidMaps[index];
		auto iter = uuidMap.find(uuid);

		if (iter == uuidMap.end())
			return nullptr;

		return &iter->second;
	}

	SizeVec* GenericDataLoader::findDailyUuidInternal(SizeMapPtr dmap, const std::string& uuid, const RuntimeData* rt, IntDay di) {
		auto uuidIter = dmap->find(uuid);

		if (uuidIter == dmap->end()) {
			if (!m_addNewUuid)
				return nullptr; 
			else {
				size_t count = dmap->size();
				SizeVec indexes(1);
				indexes[0] = count;

				{
					AUTO_LOCK(m_dataMutex);
					(*dmap)[uuid] = indexes;
				}

				uuidIter = dmap->find(uuid);
			}
		}

		return &uuidIter->second;
	}

	void GenericDataLoader::preDataBuild(IDataRegistry& dr, const pesa::DataCacheInfo& dci) {
		for (ColumnMatch& cm : m_matchColumns) {
			cm.data = dr.stringData(dr.getSecurityMaster(), cm.dataId);
			ASSERT(cm.data, "Invalid matchColumns info. Unable to load data source: " << cm.dataId);
		}

		if (!m_idTypes.empty())
			loadUUIDs(dr);
	}

	size_t GenericDataLoader::uuidCount(const RuntimeData* rt, IntDay di) const {
		/// If we already have the ID data loaded then we just return the column count for that ...
		if (m_idData) 
			return m_idData->cols();

		if (di == 0)
			return 0;

		/// Otherwise we need to look from the UUID lookup
		auto diIter = m_dailyUuidMap.find(di);

		if (diIter != m_dailyUuidMap.end())
			return diIter->second->size();

		return 0;
	}

	SizeVec* GenericDataLoader::findUuid(const std::string& uuid, const RuntimeData* rt, IntDay di, size_t index /* = 0 */) {
		//return findUuidInternal(uuid, rt, di);
		if (!m_idData) {
			if (!m_addNewUuid)
				return findUuidInternal(uuid, rt, di, index);
		}

		if (m_idData)
			di = std::min(di, (IntDay)m_idData->rows() - 1);
		else
			di = 0;

		SizeMapPtr dmap = nullptr;

		{
			AUTO_LOCK(m_dataMutex);
			auto diIter = m_dailyUuidMap.find(di);

			if (diIter != m_dailyUuidMap.end())
				dmap = diIter->second;
		}

		if (dmap) 
			return findDailyUuidInternal(dmap, uuid, rt, di);
		else if (!m_idData && m_addNewUuid) {
			/// If there is no ID data then load an empty map
			dmap = std::make_shared<SizeMap>();
			{
				AUTO_LOCK(m_dataMutex);
				m_dailyUuidMap[di] = dmap;
			}

			//auto diIter = m_dailyUuidMap.find(di);
			return findDailyUuidInternal(dmap, uuid, rt, di);
		}

		/// Otherwise we load the data and construct the right ID map
		const StringData* idType = m_idData;
		size_t numCols = idType->cols();

		ASSERT(di < idType->rows(), "Invalid di: " << di << " requested for data: " << m_idTypes[0] << ". Max rows: " << idType->rows());

		dmap = std::make_shared<SizeMap>();

		for (size_t ii = 0; ii < numCols; ii++) {
			const auto& id = idType->getValue(di, ii);
			(*dmap)[id].push_back(ii);
		}

		{
			AUTO_LOCK(m_dataMutex);
			m_dailyUuidMap[di] = dmap;
		}

		return findDailyUuidInternal(dmap, uuid, rt, di);
	}

	void GenericDataLoader::preCacheDay(const DataCacheInfo& dci, IntDay di) {
		IntDate date = dci.rt.dates[di];
		auto existingIter = m_caching.find(date);

		/// If we have already cached this then do not bother ...
		if (existingIter != m_caching.end())
			return;

		//CDebug("Precaching date: %u", date);

		CacheInfoPtr ci = std::make_shared<CacheInfo>();
		//ci->future = std::async(std::launch::async, &GenericDataLoader::preCacheDayAsyncEntry, this, &dci.rt, dci.di, dci.def);
		ci->dcObject = new GenericDataLoader::DataCacheThread(this, &dci.rt, di, dci.def, dci.customMetadata);
		ci->thread = new Poco::Thread();
		ci->thread->start(*ci->dcObject);
		ci->future = ci->dcObject->promise->get_future();
		m_caching[date] = ci;
	}

	DataPtr GenericDataLoader::waitForResult(const DataCacheInfo& dci, const std::string& dataName) {
		IntDate date = dci.rt.dates[dci.di];
		auto iter = m_caching.find(date);

		if (iter != m_caching.end()) {
			ASSERT(iter != m_caching.end(), "Invalid date to get that is not in the cache right now: " << date);

			CacheInfoPtr ci = iter->second;

			/// Only wait if the future is still valid ...
			ci->future.wait();

			m_caching.erase(iter);
		}

		AUTO_LOCK(m_dataMutex);
		/// OK, we are done here. Get the data from the cache ...
		auto key = makeKey(dataName, date);
		auto dataIter = m_dataCache.find(key);
		if (dataIter == m_dataCache.end())
			return nullptr;

		DataPtr data = dataIter->second;
		m_dataCache.erase(dataIter);
		return data;
	}

	void GenericDataLoader::buildDaily(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		preCache(dci);

		IntDate date = dci.rt.dates[dci.di];
		std::string dataName = dci.def.name;
		DataPtr data = waitForResult(dci, dataName);

		if (!data) {
			frame.noDataUpdate = true;
			return;
		}

		if (!data->dataSize() && m_isUniversal) {
			data->ensureSize(1, 1);
			data->setString(0, 0, 0, "");
		}

		data->setMetadata(dci.customMetadata);

		CDebug("Update Data: [%-8u]-[%z x %z] - %s", date, data->rows(), data->cols(), std::string(dci.def.name));

		frame.data			= data;
		frame.dataSize		= data->dataSize();
		frame.dataOffset	= 0;
		frame.noDataUpdate	= false;
	}

	void GenericDataLoader::build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		preCache(dci);
	}
} /// namespace pesa
