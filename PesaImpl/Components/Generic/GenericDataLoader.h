/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// GenericDataLoader.cpp
///
/// Created on: 03 Feb 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Framework/Helper/FileDataLoader.h"
#include "Poco/String.h"

#include "Poco/Thread.h"
#include <future>

namespace pesa {
	typedef std::future<int> IntFuture;

	////////////////////////////////////////////////////////////////////////////
	/// GenericDataLoader (Generic Threaded Data Loader)
	////////////////////////////////////////////////////////////////////////////
	class GenericDataLoader : public IDataLoader {
	protected:
		struct DataCacheThread : public Poco::Runnable {
			GenericDataLoader*	self;
			const RuntimeData*	rt;
			IntDay				di;
			DataDefinition		def;
			DataPtr				customMetadata = nullptr;
			std::promise<int>*	promise = nullptr;

								DataCacheThread(GenericDataLoader* self_, const RuntimeData* rt_, IntDay di_, DataDefinition def_, DataPtr customMetadata_);
								~DataCacheThread();
			virtual void		run();
		};

		struct CacheInfo {
			IntDay				di;
			IntDate				date;
			IntFuture 			future;
			DataCacheThread*	dcObject = nullptr;
			Poco::Thread*		thread = nullptr;

								~CacheInfo();
		};

		struct ColumnMatch {
			int					index = -1;
			std::string			dataId;
			const StringData*	data = nullptr;
		};

		typedef std::vector<ColumnMatch> ColumnMatchVec;
		typedef std::shared_ptr<CacheInfo> CacheInfoPtr;
		typedef std::map<std::string, DataPtr> DataPtrMap;
		typedef std::map<IntDate, CacheInfoPtr> CacheInfoPtrMap; 
		typedef std::unordered_map<std::string, SizeVec> SizeMap;
		typedef std::shared_ptr<SizeMap> SizeMapPtr;
		typedef std::unordered_map<IntDay, SizeMapPtr> SizeMapPtrMap;

		static const IntDay		s_maxPrecache = 16;			/// Precache upto 30 items

		IntDay					m_maxPrecache;				/// What is the max size of the precache ring buffer
		std::string				m_datasetName;				/// The name of the dataset 

		std::mutex 				m_dataMutex;				/// Data mutex
		CacheInfoPtrMap			m_caching;					/// Existing stuff that is caching ...
		DataPtrMap				m_dataCache;				/// Stuff that has already been cached ...
		IntDay					m_diLast = -1;				/// The last item we cached
		StringVec 				m_datasets; 				/// What datasets are we supposed to load
		std::map<std::string, std::string> m_mappings;		/// The mappings
		std::map<std::string, std::string> m_rmappings;		/// Reverse mappings 
		StringVec				m_datasetNames;				/// The names of the datasets
		IntVec					m_datasetTypes;				/// The types of the datasets

		std::string				m_baseDir;					/// The base directory, fully resolved
		std::string				m_historicalFilename;		/// Historical filename
		std::string				m_currentFilename;			/// The current filename
		int						m_dateIndex = 0;			/// The index of the date
		IntVec					m_idIndexes = { 1 };		/// The index of the ID
		StringVec				m_idTypes;					/// The type of the ID used
		const StringData*		m_idData = nullptr;			/// The ID data
		bool					m_needsId = false;			/// Does the data loader need to have an ID or not

		std::vector<SizeMap>	m_uuidMaps;					/// The uuid mapping
		SizeMapPtrMap			m_dailyUuidMap;				/// Daily UUID Map

		IntDay					m_diUuidMap = 0;			/// The di for which the UUID map was calculated
		bool					m_alwaysOverwrite = false;	/// Always overwrite or not
		bool					m_isUniversal = false;		/// Is this a universal dataset
		bool					m_hasHeader = true;			/// Whether the file has header or not
		bool					m_addNewUuid = false;		/// Indexes onto self
		bool					m_stringIndices = false;	/// Are we using string indices or not

		ColumnMatchVec			m_matchColumns;				/// Additional column matchind

		SizeVec*				findUuidInternal(const std::string& uuid, const RuntimeData* rt, IntDay di, size_t index = 0);
		SizeVec*				findDailyUuidInternal(SizeMapPtr dmap, const std::string& uuid, const RuntimeData* rt, IntDay di);
		size_t					uuidCount(const RuntimeData* rt, IntDay di) const;
		virtual SizeVec*		findUuid(const std::string& uuid, const RuntimeData* rt, IntDay di, size_t index = 0);
		virtual void			loadUUIDs(IDataRegistry& dr);
		void					loadFIGIs(IDataRegistry& dr);
		void					loadISINs(IDataRegistry& dr);
		void					loadTickers(IDataRegistry& dr);
		void					loadBBTickers(IDataRegistry& dr);

		virtual void			clearCache(IntDate date);
		virtual int 			preCacheDayAsync(const RuntimeData* rt, IntDay di, DataDefinition def, DataPtr customMetadata) = 0;
		virtual void			preCacheDay(const DataCacheInfo& dci, IntDay di);
		virtual void			preCache(const DataCacheInfo& dci);
		virtual DataPtr			waitForResult(const DataCacheInfo& dci, const std::string& dataName);

		std::string				getFilename(const RuntimeData& rt, IntDay di, bool& isHistorical);
		virtual void 			build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);

		inline std::string		makeKey(const std::string& dataName, IntDate date, std::string prefix = "", std::string suffix = "") const { 
									return prefix + dataName + "-" + Util::cast(date) + suffix; 
								}

	public:
								GenericDataLoader(const ConfigSection& config, std::string logger);
		virtual 				~GenericDataLoader();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			preDataBuild(IDataRegistry& dr, const pesa::DataCacheInfo& dci);
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			buildDaily(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);
	};
} /// namespace pesa

