/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// GenericDataLoaderCSV.cpp
///
/// Created on: 22 Apr 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "./GenericDataLoaderCSV.h"
#include "Poco/String.h"
#include "Poco/File.h"

namespace pesa {
	//////////////////////////////////////////////////////////////////////////
	/// GenericDataLoaderCSV
	//////////////////////////////////////////////////////////////////////////
	GenericDataLoaderCSV::GenericDataLoaderCSV(const ConfigSection& config, std::string logger /* = G_CSV */)
		: GenericDataLoader(config, logger) {
		auto&& datasetName		= config.getString("datasetName", "");
		if (!datasetName.empty())
			setLogChannel(datasetName);

		m_separator				= config.getString("separator", ",");
		m_commentString			= config.getString("comment", "");
		m_namedDataIndices 		= config.getBool("namedDataIndices", false);

		/// TODO: Figure out why the \t in XML is coming out as \\t
		if (m_separator == "\\t")
			m_separator			= "\t";
	}

	GenericDataLoaderCSV::~GenericDataLoaderCSV() {
	}

	void GenericDataLoaderCSV::initDatasets(pesa::IDataRegistry &dr, pesa::Datasets &datasets) {
		/// Always call the base ...
		GenericDataLoader::initDatasets(dr, datasets);

		/// If it's not a named dataset (majority of the cases), then we're done over here ...
		if (!m_namedDataIndices)
			return;

		auto idName = config().getResolvedRequiredString("idName");

		auto oldMappings = m_mappings;
		m_mappings.clear();

		size_t mappingIndex = 1; /// 0 is always reserved for the id
		for (const auto& mapping : oldMappings) {
			auto name = mapping.first;
			auto index = mapping.second;
			auto iindex = mappingIndex;
			bool isFixed = false;

			if (name == idName) 
				iindex = 0;
			else 
				mappingIndex++;

			if (index[0] != '#' || name == idName) 
				m_namedIndexMappings[index] = iindex;
			else {
				auto fixedIndex = Util::cast<size_t>(index.substr(1));
				m_fixedIndexMappings[fixedIndex] = iindex;
			}

			auto sindex = Util::cast(iindex);
			m_mappings[name] = sindex;
			m_rmappings[sindex] = name;
		}
	}

	StringVec GenericDataLoaderCSV::read(helper::FileDataLoader& reader) {
		/// If it's the normal case of non-named data indices, then just read one line at a time ...
		if (!m_namedDataIndices)
			return std::move(reader.read());

		/// Otherwise we read one line at a time ...
		auto values = reader.read();

		/// If nothing was read then, there is nothing to read at all ...
		if (values.empty())
			return values;

		StringVec rvalues(m_mappings.size());

		int idIndex = m_idIndexes.size() ? m_idIndexes[0] : -1;
		ASSERT(idIndex >= 0, "The first idIndex must specify the UUID that is used for mapping for Named Data Indices! idIndex: " << idIndex);

		int nameIndex = config().getInt("nameIndex", -1);
		ASSERT(nameIndex >= 0, "CSVs indexed on named data values must give the 'dataIndex' value which tells the reader, where to get the data index from!");

		int valueIndex = config().getInt("valueIndex", -1);
		ASSERT(valueIndex >= 0, "CSVs indexed on named data values must give the 'valueIndex' value which tells the reader, where to get the value from!");

		auto desiredId = values[idIndex];
		bool isNewId = false;

		rvalues[0] = desiredId;

		for (const auto& fiter : m_fixedIndexMappings)
			rvalues[fiter.second] = values[fiter.first];

		while (!values.empty()) {
			int numValues = (int)values.size();

			if (numValues > idIndex && numValues > nameIndex && numValues > valueIndex) {
				auto& id = values[idIndex];

				/// If the ids match then we can proceed with further operations ...
				if (id == desiredId) {
					auto& dataIndexId = values[nameIndex];
					auto miter = m_namedIndexMappings.find(dataIndexId);

					if (miter != m_namedIndexMappings.end()) {
						auto& value = values[valueIndex];
						rvalues[miter->second] = value;
					}
				}
				/// Otherwise we reset the file pointer to the saved location ...
				else 
					break;
			}

			/// Consume the previous buffer (if there was one)
			reader.consume();

			/// Read the next line ...
			values = reader.peek();
		}

		return rvalues;
	}

	int GenericDataLoaderCSV::preCacheDayAsyncCSV(const RuntimeData* rt, IntDay di, DataDefinition def, DataPtr customMetadata) {
		bool isHistorical = false;
		std::string filename = getFilename(*rt, di, isHistorical);

		if (filename.empty())
			return -1;

		std::string zipFilename = config().getString("zipFilename", "");
		bool isZip = false;

		if (!zipFilename.empty()) {
			isZip = true;
			filename += "@" + parentConfig()->defs().resolve(zipFilename, (unsigned int)rt->dates[di]);
		}

		IntDate date = rt->dates[di];
		std::string dataName = def.name;

		{
			/// OK, have we cached the data already. If we have then there's nothing to do over here ...
			AUTO_LOCK(m_dataMutex);
			auto key = makeKey(dataName, date);
			auto dataIter = m_dataCache.find(key);
			if (dataIter != m_dataCache.end())
				return 0;
		}

		CTrace("Loading filename: %s", filename);

		helper::FileDataLoader reader(filename, m_separator.c_str(), false, m_hasHeader, m_commentString.c_str(), isZip);
		size_t numLines = 0;

		StringVec dataNames = m_datasetNames;
		IntVec dataIndices(m_datasetNames.size());
		DataPtrVec dataValues(m_datasetNames.size());
		auto* secMaster = !m_addNewUuid ? rt->dataRegistry->getSecurityMaster() : nullptr;
		size_t numCols = 0;
		size_t numRows = 1;

		if (secMaster)
			numCols = secMaster->size(di);
		else {
			//numCols = uuidCount(rt, di);

			//if (!numCols)
			if (!m_namedDataIndices)
			    numCols = reader.lineCount();
		}

		for (size_t i = 0; i < m_datasetNames.size(); i++) {
			const auto iter = m_mappings.find(dataNames[i]);
			ASSERT(iter != m_mappings.end(), "Unable to find the mapping for data: " << dataNames[i]);

			int index = -1;

			if (!m_stringIndices) 
				index = Util::cast<int>(iter->second);
			else {
				static const std::string sep = "|";
				auto mapping = iter->second;
				auto mappings = Util::split(mapping, sep);

				for (size_t i = 0; i < mappings.size() && index < 0; i++) 
					index = reader.headerIndex(mappings[i]);
			}

			dataIndices[i] = index;

			if (index < 0)
				continue;

			DataDefinition ndef = def;
			ndef.itemType = (DataType::Type)m_datasetTypes[i];
			ndef.itemSize = DataType::size(ndef.itemType);
			ndef.setName(dataNames[i]);

			DataPtr data = rt->dataRegistry->createDataBuffer(nullptr, ndef);
			ASSERT(data, "Unable to create data for type: " << def.itemType);

			//data->setMetadata(customMetadata);

			if (m_idIndexes.size() && m_idIndexes[0] >= 0) {
				if (!m_idData) {
					if (!ndef.isStatic() && !m_isUniversal) 
						data->ensureSize(numRows, numCols);
				}
				else {
					size_t numCols = m_idData->cols();
					data->ensureSize(numRows, numCols);
				}
			}
			else {
				data->ensureSize(numRows, numCols);
			}

			dataValues[i] = data;

			{
				AUTO_LOCK(m_dataMutex);
				auto key = makeKey(dataNames[i], date);
				m_dataCache[key] = data;
			}
		}

		int idIndex = m_idIndexes.size() ? m_idIndexes[0] : -1;

		if (m_stringIndices) {
			auto idIndexStr = config().getString("idIndex", "");
			idIndex = -1;

			if (!idIndexStr.empty()) {
				auto idIndexMapping = m_mappings[idIndexStr];
				auto parts = Util::split(idIndexMapping, "|");

				for (size_t i = 0; i < parts.size() && idIndex < 0; i++)
					idIndex = reader.headerIndex(parts[i]);
			}
		}

		ASSERT(idIndex >= 0 || !m_needsId, "Unable to find the ID index!");

		size_t numValid = 0;

		auto values					= read(reader);
		const auto&& dateFormat		= config().getString("dateFormat", "");
		const auto&& invalidString	= config().getString("invalidString", "");
		bool throwUnknownIndex		= config().getBool("throwUnknownIndex", false);

		if (!m_isUniversal) {
			size_t uuidIndex = 0;

			while (values.size()) {
				numLines++;
				std::string dateTimeStr;
				int date = 0;

				if (m_dateIndex >= 0) {
					try {
						dateTimeStr = values[m_dateIndex];

						if (!dateTimeStr.empty()) {
							if (dateFormat.empty())
								date = (int)Util::strDateTimeToIntDate(dateTimeStr);
							else
								date = (int)Util::dateToInt(Util::parseFormattedDate(dateFormat, dateTimeStr));
						}
					}
					catch (const std::exception& e) {
						std::cerr << e.what() << std::endl;
					}
				}


				SizeVec* uuids = nullptr;
				SizeVec universeUuid(1);

				for (size_t id = 0; id < m_idIndexes.size() && !uuids; id++) {
					idIndex = m_idIndexes[id];

					if (idIndex >= 0) {
						ASSERT(values.size() > idIndex, "Invalid values array returned by CSV reader. Array size: " << values.size() << " - Id Index: " << idIndex);
						const auto& dataUuid = values[idIndex];
						if (!dataUuid.empty())
							uuids = findUuid(dataUuid, rt, di, id);
					}
				}

				if (!uuids && !m_idData && secMaster) {
					for (size_t id = 0; id < m_idIndexes.size() && !uuids; id++) {
						idIndex = m_idIndexes[id];

						if (idIndex >= 0) {
							const auto& dataUuid = values[idIndex];

							if (!dataUuid.empty()) {
								const auto* security = secMaster->mapUuid(dataUuid);

								if (security && security->isValid()) {
									universeUuid[0] = (size_t)security->index;
									uuids = &universeUuid;
								}
							}
						}
					}

					ASSERT(!throwUnknownIndex || uuids, "Fatal Error: Unable to find any index for data");
				}

				if (uuids || idIndex < 0) {
					//CError("Unable to find data uuid: %s", dataUuid);
					numValid++;

					for (size_t ii = 0; ii < dataIndices.size(); ii++) {
						if (dataIndices[ii] < 0)
							continue;

						size_t index = dataIndices[ii];
						std::string dataName = dataNames[ii];

						if (index >= values.size())
							continue;

						ASSERT(index < values.size(), "Invalid index:" << numLines << " - " << index << " max indices: " << values.size());
						DataPtr data = dataValues[ii];
						bool isInvalid = false;
						auto& value = values[index];

						if (value.empty() || (!invalidString.empty() && value == invalidString))
							isInvalid = true;

						if (uuids && idIndex >= 0) {
							for (auto& iiDst : *uuids) {
								bool ignore = false;

								for (size_t mi = 0; mi < m_matchColumns.size() && !ignore; mi++) {
									int index = m_matchColumns[mi].index;
									std::string dataId = m_matchColumns[mi].dataId;
									const StringData* matchData = m_matchColumns[mi].data;

									ASSERT(matchData, "Invalid matchColumns info. Unable to load data source: " << dataId);

									std::string dataValue = values[index];
									std::string expectedValue = matchData->getValue(std::min(di, (IntDay)matchData->rows() - 1), iiDst);

									ignore = dataValue != expectedValue;
								}

								/// If there are more than 1 id indexes to match and value has been written before AND
								/// is still valid, then don't ovrewrite
								if (m_idIndexes.size() > 1 && data->isValid(0, iiDst, 0))
								    ignore = true;

								if (!ignore) {
									/// Ensure that we have enough size in the data buffer. Make sure we keep the old data
									if (iiDst >= data->cols())
										data->ensureSize(data->rows(), iiDst + 1, true);

									if (!isInvalid)
										data->setString(0, iiDst, 0, value);
									else
										data->makeInvalid(0, iiDst);
								}
							}
						}
						else {
							auto iiDst = uuidIndex;

							if (!isInvalid)
								data->setString(0, uuidIndex, 0, value);
							else
								data->makeInvalid(0, uuidIndex);
						}
					}
				}
				else {
					//CWarning("Unable to find index for uuid: %s", dataUuid);
				}

				uuidIndex++;
				values = read(reader);
			}

			//CDebug("Precached Data: [%-8u]-[Valid: %5z]-%s", date, numValid, dataName);
		}
		else {
			std::map<std::string, StringVec> dataValuesMap;

			/// Load the data into string arrays
			while (values.size()) {
				numLines++;

				for (size_t ii = 0; ii < dataIndices.size(); ii++) {
					size_t index = dataIndices[ii];
					std::string dataName = dataNames[ii];

					if (index >= values.size())
						continue;

					ASSERT(index < values.size(), "Invalid index:" << numLines << " - " << index << " max indices: " << values.size());
					std::string value = values[index];

					dataValuesMap[dataName].push_back(value);
				}

				values = read(reader);
			}

			/// Once all the data has been loaded then we move it to DataPtr
			for (size_t ii = 0; ii < dataIndices.size(); ii++) {
				size_t index = dataIndices[ii];
				std::string dataName = dataNames[ii];
				const StringVec& values = dataValuesMap[dataName];
				DataPtr data = dataValues[ii];

				data->ensureSize(1, values.size(), false);

				for (size_t jj = 0; jj < values.size(); jj++) {
					const std::string& value = values[jj];
					data->setString(0, jj, 0, value);
				}
			}
		}

		return 0;
	}

	int GenericDataLoaderCSV::preCacheDayAsync(const RuntimeData* rt, IntDay di, DataDefinition def, DataPtr customMetadata) {
        return GenericDataLoaderCSV::preCacheDayAsyncCSV(rt, di, def, customMetadata);
	}

} /// namespace pesa

  ////////////////////////////////////////////////////////////////////////////
  /// To be called from the simulator
  ////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createGenericDataLoaderCSV(const pesa::ConfigSection& config) {
	return new pesa::GenericDataLoaderCSV((const pesa::ConfigSection&)config);
}

