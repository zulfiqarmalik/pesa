/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// GenericDataLoaderCSV.cpp
///
/// Created on: 22 Apr 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Framework/Helper/FileDataLoader.h"
#include "Poco/String.h"
#include "./GenericDataLoader.h"

#include "Poco/Thread.h"
#include <future>

namespace pesa {
	//////////////////////////////////////////////////////////////////////////
	/// GenericDataLoaderCSV
	//////////////////////////////////////////////////////////////////////////
	class GenericDataLoaderCSV : public GenericDataLoader {
	protected:
		std::string				m_separator = ",";			/// What is the separator [CSV]
		std::string				m_commentString;			/// What does a comment start with
		bool 					m_namedDataIndices = false;	/// Is this a MorningStar CSV file where there is a new field in each line and
															/// the data index is tagged by a field name

		std::map<std::string, size_t> m_namedIndexMappings;	/// Named index mappings
		std::map<size_t, size_t> m_fixedIndexMappings;		/// Fixed index mappings

		virtual int				preCacheDayAsyncCSV(const RuntimeData* rt, IntDay di, DataDefinition def, DataPtr customMetadata);
		virtual StringVec		read(helper::FileDataLoader& reader);

	public:
								GenericDataLoaderCSV(const ConfigSection& config, std::string logger = "G_CSV");
		virtual 				~GenericDataLoaderCSV();

		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		int						preCacheDayAsync(const RuntimeData* rt, IntDay di, DataDefinition def, DataPtr customMetadata);
	};

} /// namespace pesa

