/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// GenericDataLoaderJSON.cpp
///
/// Created on: 14 Oct 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "./GenericDataLoaderJSON.h"
#include "Poco/String.h"
#include "Poco/File.h"

namespace pesa {
	//////////////////////////////////////////////////////////////////////////
	/// GenericDataLoaderJSON
	//////////////////////////////////////////////////////////////////////////
	GenericDataLoaderJSON::GenericDataLoaderJSON(const ConfigSection& config, std::string logger /* = G_CSV */)
		: GenericDataLoader(config, logger) {
		auto&& datasetName		= config.getString("datasetName", "");
		if (!datasetName.empty())
			setLogChannel(datasetName);
	}

	GenericDataLoaderJSON::~GenericDataLoaderJSON() {
	}

	int GenericDataLoaderJSON::preCacheDayAsyncJSON(const RuntimeData* rt, IntDay di, DataDefinition def, DataPtr customMetadata) {
		return 0;
	}

	int GenericDataLoaderJSON::preCacheDayAsync(const RuntimeData* rt, IntDay di, DataDefinition def, DataPtr customMetadata) {
		return GenericDataLoaderJSON::preCacheDayAsyncJSON(rt, di, def, customMetadata);
	}

} /// namespace pesa

  ////////////////////////////////////////////////////////////////////////////
  /// To be called from the simulator
  ////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createGenericDataLoaderJSON(const pesa::ConfigSection& config) {
	return new pesa::GenericDataLoaderJSON((const pesa::ConfigSection&)config);
}

