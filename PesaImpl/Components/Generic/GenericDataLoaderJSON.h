/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// GenericDataLoaderJSON.cpp
///
/// Created on: 14 Oct 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Framework/Helper/FileDataLoader.h"
#include "Poco/String.h"
#include "./GenericDataLoader.h"

#include "Poco/Thread.h"
#include <future>

namespace pesa {
	//////////////////////////////////////////////////////////////////////////
	/// GenericDataLoaderJSON
	//////////////////////////////////////////////////////////////////////////
	class GenericDataLoaderJSON : public GenericDataLoader {
	protected:
		virtual int				preCacheDayAsyncJSON(const RuntimeData* rt, IntDay di, DataDefinition def, DataPtr customMetadata);

	public:
								GenericDataLoaderJSON(const ConfigSection& config, std::string logger = "G_JSON");
		virtual 				~GenericDataLoaderJSON();

		int						preCacheDayAsync(const RuntimeData* rt, IntDay di, DataDefinition def, DataPtr customMetadata);
	};

} /// namespace pesa

