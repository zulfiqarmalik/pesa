/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// HistoricalTickers.cpp
///
/// Created on: 23 Jan 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// HistoricalTickers - Historical tickers and BB tickers.
	////////////////////////////////////////////////////////////////////////////
	class HistoricalTickers : public IDataLoader {
	private:
		std::string				m_datasetName = "secData";		/// The name of the dataset

	public:
								HistoricalTickers(const ConfigSection& config);
		virtual 				~HistoricalTickers();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {}
		virtual void 			buildFigis(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);
		virtual void 			buildTickers(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);
		virtual void 			buildBBTickers(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);
		virtual void 			buildCountryCode2(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);
		virtual void 			buildCountryCode3(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);
		virtual void 			buildCurrency(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);
	};

	HistoricalTickers::HistoricalTickers(const ConfigSection& config) : IDataLoader(config, "ALPHA_DATA") {
		m_datasetName		= config.getString("datasetName", m_datasetName);
	}

	HistoricalTickers::~HistoricalTickers() {
		CTrace("Deleting HistoricalTickers");
	}

	void HistoricalTickers::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		datasets.add(Dataset(m_datasetName, {
			DataValue("cfigis", DataType::kString, DataType::size(DataType::kString), THIS_DATA_FUNC(HistoricalTickers::buildFigis), DataFrequency::kDaily, 0),
			DataValue("tickers", DataType::kString, DataType::size(DataType::kString), THIS_DATA_FUNC(HistoricalTickers::buildTickers), DataFrequency::kDaily, 0),
			DataValue("bbTickers", DataType::kString, DataType::size(DataType::kString), THIS_DATA_FUNC(HistoricalTickers::buildBBTickers), DataFrequency::kDaily, 0),
			DataValue("countryCode2", DataType::kString, DataType::size(DataType::kString), THIS_DATA_FUNC(HistoricalTickers::buildCountryCode2), DataFrequency::kStatic, 
				DataFlags::kUpdatesInOneGo | DataFlags::kAlwaysOverwrite),
			DataValue("countryCode3", DataType::kString, DataType::size(DataType::kString), THIS_DATA_FUNC(HistoricalTickers::buildCountryCode3), DataFrequency::kStatic, 
				DataFlags::kUpdatesInOneGo | DataFlags::kAlwaysOverwrite),
			DataValue("currency", DataType::kString, DataType::size(DataType::kString), THIS_DATA_FUNC(HistoricalTickers::buildCurrency), DataFrequency::kDaily, 0),
		}));
	}

	void HistoricalTickers::buildFigis(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		size_t numSecurities = dci.universe->size(dci.di);
		StringDataPtr figiData = std::make_shared<StringData>(dci.universe, numSecurities);

		for (size_t ii = 0; ii < numSecurities; ii++) {
			Security sec = dci.universe->security(dci.di, (Security::Index)ii);
			std::string figi = sec.uuidStr();
			figiData->setValue(0, ii, figi);
		}

		frame.data = figiData;
		frame.dataSize = figiData->dataSize();
		frame.dataOffset = 0;
	}

	void HistoricalTickers::buildTickers(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		size_t numSecurities = dci.universe->size(dci.di);
		StringDataPtr tickerData = std::make_shared<StringData>(dci.universe, numSecurities);

		for (size_t ii = 0; ii < numSecurities; ii++) {
			Security sec = dci.universe->security(dci.di, (Security::Index)ii);
			std::string ticker = sec.tickerStr();
			tickerData->setValue(0, ii, ticker);
		}

		frame.data = tickerData;
		frame.dataSize = tickerData->dataSize();
		frame.dataOffset = 0;
	}

	void HistoricalTickers::buildBBTickers(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		size_t numSecurities = dci.universe->size(dci.di);
		StringDataPtr tickerData = std::make_shared<StringData>(dci.universe, numSecurities);

		for (size_t ii = 0; ii < numSecurities; ii++) {
			Security sec = dci.universe->security(dci.di, (Security::Index)ii);
			std::string bbTicker = sec.bbTickerStr();
			tickerData->setValue(0, ii, bbTicker);
		}

		frame.data = tickerData;
		frame.dataSize = tickerData->dataSize();
		frame.dataOffset = 0;
	}

	void HistoricalTickers::buildCountryCode2(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		size_t numSecurities = dci.universe->size(dci.rt.diEnd);
		StringDataPtr countryCodeData = std::make_shared<StringData>(dci.universe, numSecurities);

		for (size_t ii = 0; ii < numSecurities; ii++) {
			Security sec = dci.universe->security(dci.di, (Security::Index)ii);
			std::string countryCode = sec.countryCode2Str();
			countryCodeData->setValue(0, ii, countryCode);
		}

		frame.data = countryCodeData;
		frame.dataSize = countryCodeData->dataSize();
		frame.dataOffset = 0;
	}

	void HistoricalTickers::buildCountryCode3(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		size_t numSecurities = dci.universe->size(dci.rt.diEnd);
		StringDataPtr countryCodeData = std::make_shared<StringData>(dci.universe, numSecurities);

		for (size_t ii = 0; ii < numSecurities; ii++) {
			Security sec = dci.universe->security(dci.di, (Security::Index)ii);
			std::string countryCode = sec.countryCode3Str();
			countryCodeData->setValue(0, ii, countryCode);
		}

		frame.data = countryCodeData;
		frame.dataSize = countryCodeData->dataSize();
		frame.dataOffset = 0;
	}

	void HistoricalTickers::buildCurrency(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		size_t numSecurities = dci.universe->size(dci.rt.diEnd);
		StringDataPtr currencyData = std::make_shared<StringData>(dci.universe, numSecurities);

		for (size_t ii = 0; ii < numSecurities; ii++) {
			Security sec = dci.universe->security(dci.di, (Security::Index)ii);
			std::string currency = sec.currencyStr();
			currencyData->setValue(0, ii, currency);
		}

		frame.data = currencyData;
		frame.dataSize = currencyData->dataSize();
		frame.dataOffset = 0;
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createHistoricalTickers(const pesa::ConfigSection& config) {
	return new pesa::HistoricalTickers((const pesa::ConfigSection&)config);
}

