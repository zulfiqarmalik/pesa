/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Locates.cpp
///
/// Created on: 19 Sep 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Locates data
	////////////////////////////////////////////////////////////////////////////
	class Locates : public IDataLoader {
	private:
		std::string				m_dataId = "cost.borrowRate";			/// The data to use for borrowing costs 
		float					m_threshold = 100.0f;					/// 100 basis points
		std::string 			m_operator = "GT";						/// The operator
		std::string				m_dataName = "locates";					/// The name of the data
		bool					m_ignoreNans = false;					/// Ignore the NaN

	public:
								Locates(const ConfigSection& config);
		virtual 				~Locates();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);
	};

	Locates::Locates(const ConfigSection& config) : IDataLoader(config, "Locates") {
		m_dataId		= config.getString("dataId", m_dataId);
		m_dataName		= config.getString("dataName", m_dataName);
		m_threshold		= config.get<float>("threshold", m_threshold);
		m_operator 		= config.getString("operator", m_operator);
		m_ignoreNans	= config.getBool("ignoreNans", m_ignoreNans);
	}

	Locates::~Locates() {
		CTrace("Deleting Locates!");
	}

	void Locates::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		datasets.add(Dataset("cost", {
			DataValue(m_dataName, DataType::kInt, sizeof(int), nullptr, DataFrequency::kDaily, 0),
		}));
	}

	void Locates::build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		const auto* secMaster = dr.getSecurityMaster();
		const FloatMatrixData* data = dr.get<FloatMatrixData>(nullptr, m_dataId);
		ASSERT(data, "Unable to load data: " << m_dataId);

		size_t size = data->cols();
		IntMatrixData* locates = new IntMatrixData(dci.universe, dci.def);
		size_t hardToBorrowCount = 0;
		locates->ensureSize(1, size, false);

		for (size_t ii = 0; ii < size; ii++) {
			const Security& sec = secMaster->security(dci.di, (Security::Index)ii);
			float bcost = (*data)(dci.di, ii);
			int canBorrow = 1;

			if (!m_ignoreNans && std::isnan(bcost)) {
				canBorrow = 0;
				hardToBorrowCount++;
			}
			else {
				if (m_operator == "GT") {
					if (bcost <= 0.0f) {
						canBorrow = 0;
						hardToBorrowCount++;
					}
					else if (bcost > m_threshold) {
						canBorrow = 0;
						hardToBorrowCount++;
					}
				}
				else if (m_operator == "GTE") {
					if (bcost >= m_threshold) {
						canBorrow = 0;
						hardToBorrowCount++;
					}
				}
				else if (m_operator == "LT") {
					if (bcost < m_threshold) {
						canBorrow = 0;
						hardToBorrowCount++;
					}
				}
				else if (m_operator == "LTE") {
					if (bcost <= m_threshold) {
						canBorrow = 0;
						hardToBorrowCount++;
					}
				}
			}

			(*locates)(0, ii) = canBorrow;
		}

		CDebug("[%u]-[%z] - %s", dci.rt.dates[dci.di], hardToBorrowCount, m_dataName);

		frame.data			= IntMatrixDataPtr(locates);
		frame.dataOffset	= 0;
		frame.dataSize		= locates->dataSize();
		frame.noDataUpdate	= false;
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createLocates(const pesa::ConfigSection& config) {
	return new pesa::Locates((const pesa::ConfigSection&)config);
}

