/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// MergeData.cpp
///
/// Created on: 18 Sep 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Poco/String.h"
#include "Framework/Helper/FileDataLoader.h"
#include "PesaImpl/Components/SP/SpGeneric.h"
#include "PesaImpl/Components/SP/SpRest.h"
#include "PesaImpl/Components/Quandl/Quandl_JsonParser.h"
#include "PesaImpl/Pipeline/DataRegistry/DataRegistry.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// MergeData: This module remaps the data from one security master to another
	////////////////////////////////////////////////////////////////////////////
	class MergeData : public IDataLoader {
	private:
		typedef std::map<std::string, StringVec> DataSources;
		DataSources				m_dataSources;			/// The sources of the data

	public:
								MergeData(const ConfigSection& config);
		virtual 				~MergeData();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
	};

	MergeData::MergeData(const ConfigSection& config) : IDataLoader(config, "MergeData") {
	}

	MergeData::~MergeData() {
		CTrace("Deleting MergeData!");
	}

	void MergeData::initDatasets(IDataRegistry& dr, Datasets& dss) {
		auto datasetName	= config().getResolvedRequiredString("datasetName"); 
		auto datasetsStr	= config().getRequired("datasets");
		auto mappingsStr	= config().getResolvedRequiredString("mappings");

		auto mappings		= Util::split(mappingsStr, ",");
		for (const auto& mapping : mappings) {
			auto parts		= Util::split(mapping, "=");
			ASSERT(parts.size() == 2, "Invalid mapping: " << mapping);
			
			m_dataSources[parts[0]] = Util::split(parts[1], "|");
		}

		auto datasets		= Util::split(datasetsStr, ",");

		for (const auto& dataset : datasets) {
			DataFrequency::Type frequency;
			DataType::Type type;
			std::string dataName;
			bool addToRequiredList = true;

			DataDefinition::parseDefString(dataset, dataName, type, frequency);

			ASSERT(m_dataSources.find(dataName) != m_dataSources.end(), "Unable to find a mapping for dataset: " << dataset);

			dss.add(Dataset(datasetName, {
				DataValue(dataName, type, DataType::size(type), nullptr, frequency, frequency == DataFrequency::kStatic ? (DataFlags::kUpdatesInOneGo | DataFlags::kAlwaysOverwrite) : 0),
			}));
		}
	}

	void MergeData::build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		auto dataName		= std::string(dci.def.name);
		auto iter			= m_dataSources.find(dataName);

		ASSERT(iter != m_dataSources.end(), "Unable to find a mapping for dataset: " << dataName);

		const StringVec& dataSourceIds = iter->second;

		ASSERT(dataSourceIds.size(), "No valid mappings for dataset: " << dataName);

		DataPtr ds0			= dr.getDataPtr(dci.universe, dataSourceIds[0]);
		Data* data			= ds0->createInstance();
		size_t numCols		= ds0->cols();
		size_t depth		= ds0->depth();
		IntDay di			= std::min(dci.di, (IntDay)ds0->rows());

		/// Get the max depth
		for (size_t dsii = 1; dsii < dataSourceIds.size(); dsii++) {
			const auto dataSourceId	= dataSourceIds[dsii];
			DataPtr dsi		= dr.getDataPtr(dci.universe, dataSourceId);

			if (dsi->cols() > numCols)
				numCols 	= dsi->cols();
		}

		data->ensureSizeAndDepth(1, numCols, depth, false);
		data->invalidateAll();

		for (const auto& dataSourceId : dataSourceIds) {
			DataPtr dsi		= dr.getDataPtr(dci.universe, dataSourceId);
			ASSERT(dsi, "Unable to load data: " << dataSourceId);

			ASSERT(numCols >= dsi->cols(), "Number of columns MORE than the data: " << dataSourceId << ". Expecting: " << numCols << " - Found: " << dsi->cols());
			ASSERT(depth == dsi->depth(), "Depth mismatch while merging dataset: " << dataSourceId << ". Expecting: " << depth << " - Found: " << dsi->depth());

			/// Copy the data if it's invalid right now
			for (size_t kk = 0; kk < depth; kk++) {
				for (size_t ii = 0; ii < numCols; ii++) {
					if (!data->isValid(0, ii, kk) && ii < dsi->cols()) {
						data->copyElement(dsi.get(), di, ii, 0, ii, kk, kk);
					}
				}
			}
		}

		frame.data			= DataPtr(data);
		frame.dataSize		= data->dataSize();
		frame.dataOffset	= 0;
		frame.noDataUpdate	= false;

		CInfo("Updated data: [%-8u]-[Count: %z] - %s", dci.rt.dates[dci.di], data->cols(), dci.def.toString(dci.ds));
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createMergeData(const pesa::ConfigSection& config) {
	return new pesa::MergeData((const pesa::ConfigSection&)config);
}

