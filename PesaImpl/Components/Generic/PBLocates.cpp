/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// PBLocates.cpp
///
/// Created on: 17 Feb 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Framework/Helper/FileDataLoader.h"
#include "./GenericDataLoaderCSV.h"

#include "Poco/String.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// PBLocates
	////////////////////////////////////////////////////////////////////////////
	class PBLocates : public GenericDataLoaderCSV {
	protected:
		typedef std::map<IntDate, GenericDataLoader::SizeMap>	UUIDMap;
		typedef std::map<std::string, StringVec>				ExchangeIdsMap;

		UUIDMap					m_uuidMaps;
		IntDate					m_dataStartDate; 
		float					m_rateThreshold = -7.5;
		std::string				m_priceDataId = "baseData.adjClosePrice";
		std::string				m_markitQuantityDataId = "cost.markitDollarQuantity";
		std::string				m_tickerDataId = "secData.tickers";
		bool					m_basicTickers = true;
		ExchangeIdsMap			m_exchangeIdsMap;

		virtual int 			preCacheDayAsync(const RuntimeData* rt, IntDay di, DataDefinition def, DataPtr customMetadata);
		virtual void			preCacheDay(const DataCacheInfo& dci, IntDay di);
		virtual void			clearCache(IntDate date);
		virtual SizeVec*		findUuid(const std::string& uuid, const RuntimeData* rt, IntDay di);

		void					initExchangeIdsMap(const std::string& exchangeTickerMap);

	public:
								PBLocates(const ConfigSection& config);
		virtual 				~PBLocates();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);
		virtual void 			buildLocates(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);
		virtual void 			buildDollarQuantity(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);
	};

	PBLocates::PBLocates(const ConfigSection& config) 
		: GenericDataLoaderCSV(config, "PBLocates") {
		m_dataStartDate = config.get<IntDate>("dataStartDate", parentConfig()->defs().startDate());
		m_rateThreshold = config.get<float>("rateThreshold", m_rateThreshold);
		m_priceDataId = config.getString("priceDataId", m_priceDataId);
		m_tickerDataId = config.getString("tickerDataId", m_tickerDataId);
		m_basicTickers = config.getBool("basicTickers", m_basicTickers);

		if (!m_basicTickers)
			initExchangeIdsMap(parentConfig()->defs().getRequired("exchangeTickerMap"));
	}

	PBLocates::~PBLocates() {
		CTrace("Deleting PBLocates!");
	}

	void PBLocates::initExchangeIdsMap(const std::string& exchangeTickerMap) {
		auto exchangeInfos = Util::split(exchangeTickerMap, ",");
		for (const auto& exchangeInfo : exchangeInfos) {
			auto parts = Util::split(exchangeInfo, ":");
			auto exchange = parts[0];
			auto tickerMaps = Util::split(parts[1], "|");

			if (tickerMaps.size() > 1) {
				for (size_t i = 0; i < tickerMaps.size(); i++) {
					const auto& tickerMap = tickerMaps[i];
					for (size_t j = 0; j < tickerMaps.size(); j++) {
						if (j != i) {
							m_exchangeIdsMap[tickerMap].push_back(tickerMaps[j]);
						}
					}
				}
			}
		}
	}

	void PBLocates::initDatasets(IDataRegistry& dr, Datasets& dss) {
		//GenericDataLoader::initDatasets(dr, dss);
		//Dataset ds(m_datasetName);

		//for (std::string dataset : m_datasets) {
		//	DataFrequency::Type frequency;
		//	DataType::Type type;
		//	std::string dataName;
		//	bool addToRequiredList = true;

		//	DataDefinition::parseDefString(dataset, dataName, type, frequency);

		//	m_datasetNames.push_back(dataName);
		//	m_datasetTypes.push_back((int)type);

		//	DataValue dv(dataName, type, DataType::size(type), THIS_DATA_FUNC(GenericDataLoader::buildDaily), frequency, DataFlags::kCollectAllBeforeWriting);
		//	ds.add(dv);
		//}

		//dss.add(ds);

		GenericDataLoader::initDatasets(dr, dss);

		dss.datasets[0].flushDataset = true;

		dss.add(Dataset(m_datasetName, {
			DataValue("locates", DataType::kInt, sizeof(int), THIS_DATA_FUNC(PBLocates::buildLocates), DataFrequency::kDaily, 0),
			DataValue("dollarQuantity", DataType::kFloat, sizeof(float), THIS_DATA_FUNC(PBLocates::buildDollarQuantity), DataFrequency::kDaily, 0),
		}));
	}

	void PBLocates::preCacheDay(const DataCacheInfo& dci, IntDay di) {
		AUTO_LOCK(m_dataMutex);

		bool isHistorical;
		std::string filename = getFilename(dci.rt, di, isHistorical);
		IntDate date = dci.rt.dates[di];

		/// Don't even attempt if no data is present!
		if (filename.empty()) {
			ASSERT(date < m_dataStartDate || !Application::instance()->appOptions().forceLocates || dci.rt.isTodayHoliday, "No PrimeBroker locates file for date: " << date);
			return;
		}

		auto* rt = &dci.rt;
		IntDate uuidMapDate = rt->dates[di];
		auto iter = m_uuidMaps.find(uuidMapDate);

		if (iter == m_uuidMaps.end()) {
			CDebug("Loading tickers on date: %u", uuidMapDate);

			auto tickerDataIds = Util::split(m_tickerDataId, "|");

			for (const auto& tickerDataId : tickerDataIds) {
				auto* secMaster = rt->dataRegistry->getSecurityMaster();
				StringData* tickers = rt->dataRegistry->stringData(secMaster, tickerDataId);
				ASSERT(tickers, "Unable to load ticker data: " << tickerDataId);

				//size_t numCols = tickers->cols();
				size_t numSecurities = secMaster->size(di);
				 
				for (size_t ii = 0; ii < numSecurities; ii++) {
					auto sec = secMaster->security(di, (Security::Index)ii);
					std::string ticker = tickers->getValue(di, ii);
					std::string yesterdayTicker = tickers->getValue(std::max(di - 1, 0), ii);
					StringVec usedTickers { ticker };

					if (!yesterdayTicker.empty() && yesterdayTicker != ticker) 
						usedTickers.push_back(yesterdayTicker);

					for (const std::string& usedTicker : usedTickers) {
						if (!usedTicker.empty()) {
							auto tickerParts = Util::split(usedTicker, " ");

							if (m_basicTickers) {
								const auto& tickerId = tickerParts[0];
								m_uuidMaps[uuidMapDate][tickerId].push_back(ii);
							}
							else {
								ASSERT(tickerParts.size() == 2, "Invalid ticker: " << usedTicker);
								const auto& tickerId = tickerParts[0];
								const auto& exchangeId = tickerParts[1];
								auto exchangeIter = m_exchangeIdsMap.find(exchangeId);

								m_uuidMaps[uuidMapDate][usedTicker].push_back(ii);

								if (exchangeIter != m_exchangeIdsMap.end()) {
									const auto& exchangeIds = exchangeIter->second;

									for (const auto& eid : exchangeIds) {
										m_uuidMaps[uuidMapDate][tickerId + " " + eid].push_back(ii);
									}
								}
							}
						}
					}
				}
			}
		}

		GenericDataLoader::preCacheDay(dci, di);
	}

	SizeVec* PBLocates::findUuid(const std::string& uuid, const RuntimeData* rt, IntDay di) {
		IntDate date = rt->dates[di];
		auto iter = m_uuidMaps.find(date);

		if (iter == m_uuidMaps.end())
			return nullptr;

		auto iter2 = iter->second.find(uuid);
		if (iter2 == iter->second.end())
			return nullptr;

		return &iter2->second;
	}

	void PBLocates::clearCache(IntDate date) {
		auto iter = m_uuidMaps.find(date);
		if (iter != m_uuidMaps.end())
			m_uuidMaps.erase(date);
	}

	int PBLocates::preCacheDayAsync(const RuntimeData* rt, IntDay di, DataDefinition def, DataPtr customMetadata) {
		return GenericDataLoaderCSV::preCacheDayAsyncCSV(rt, di, def, customMetadata);
	}

	void PBLocates::build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		preCache(dci);
	}

	void PBLocates::buildDollarQuantity(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		auto* secMaster = dr.getSecurityMaster();
		const FloatMatrixData* quantityData = dr.floatData(secMaster, "primeBroker.quantity");
		ASSERT(quantityData, "Unable to load primeBroker.quantity data!");

		const FloatMatrixData* prices = dr.floatData(secMaster, m_priceDataId);
		ASSERT(prices, "Unable to load prices data: " << m_priceDataId);

		const FloatMatrixData* markitData = dr.floatData(secMaster, m_markitQuantityDataId);
		ASSERT(markitData, "Unable to load markit data: " << m_markitQuantityDataId);

		size_t size = secMaster->size(dci.di);
		FloatMatrixData* newData = new FloatMatrixData(dci.universe, dci.def);
		IntDate date = dci.rt.dates[dci.di];

		newData->ensureSize(1, size, false);

		if (date < m_dataStartDate) 
			(*newData)[0] = (*markitData)[dci.di];
		else 
			(*newData)[0] = (*prices)[dci.di - 1] * (*quantityData)[dci.di];

		frame.data = FloatMatrixDataPtr(newData);
		frame.dataOffset = 0;
		frame.dataSize = newData->dataSize();
		frame.noDataUpdate = false;
	}

	void PBLocates::buildLocates(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		const auto* secMaster = dr.getSecurityMaster();
		const FloatMatrixData* quantityData = dr.floatData(nullptr, "primeBroker.quantity");
		const FloatMatrixData* rateData = dr.floatData(nullptr, "primeBroker.rate");

		ASSERT(quantityData, "Unable to load primeBroker.quantity data!");
		ASSERT(rateData, "Unable to load primeBroker.rate data!");

		size_t size = secMaster->size(dci.di);
		IntMatrixData* locates = new IntMatrixData(dci.universe, dci.def);
		size_t hardToBorrowCount = 0;
		IntDate date = dci.rt.dates[dci.di];

		locates->ensureSize(1, size, false);

		if (date < m_dataStartDate) {
			locates->setMemory(1);
		}
		else {
			for (size_t ii = 0; ii < size; ii++) {
				const Security& sec = secMaster->security(dci.di, (Security::Index)ii);
				float quantity = (*quantityData)(dci.di, ii);
				float rate = (*rateData)(dci.di, ii);
				int canBorrow = 1;

				if ((quantity < 0.0001f && std::abs(rate) < 0.0001f) ||
					(m_rateThreshold < 0.0f && rate < m_rateThreshold) ||
					(m_rateThreshold > 0.0f && rate > m_rateThreshold)) {
					canBorrow = 0;
					hardToBorrowCount++;
				}
				else if (std::isnan(quantity) || std::isnan(rate)) {
					canBorrow = 0;
					hardToBorrowCount++;
				}

				(*locates)(0, ii) = canBorrow;
			}
		}

		CDebug("[%u]-[%z] - primeBroker.locates", date, hardToBorrowCount);

		frame.data = IntMatrixDataPtr(locates);
		frame.dataOffset = 0;
		frame.dataSize = locates->dataSize();
		frame.noDataUpdate = false;
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createPBLocates(const pesa::ConfigSection& config) {
	return new pesa::PBLocates((const pesa::ConfigSection&)config);
}

