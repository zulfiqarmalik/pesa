/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// PIT_AdjData.cpp
///
/// Created on: 09 Nov 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include <unordered_map>
#include "Core/Math/Stats.h"

#include "Poco/FileStream.h"
#include "Poco/File.h"
#include "Poco/String.h"

#include "Helper/DB/MongoStream.h"

#include "Core/Lib/Profiler.h"

namespace pesa {
	typedef std::shared_ptr<Poco::FileOutputStream> FOSPtr;

	////////////////////////////////////////////////////////////////////////////
	/// PIT_AdjData - Writes the positions on a daily basis to some file on 
	/// the disk
	////////////////////////////////////////////////////////////////////////////
	class PIT_AdjData : public IDataLoader {
	private:
		struct UsedData {
			FloatMatrixDataPtr	data;
			std::string			dataName;
			std::string			cacheId;
			IUniverse*			universe; 
			FloatMatrixData*	adjFactor = nullptr;				/// The adjustment factor
			IntDay				diEnd = 0;							/// The last location to which the data has been updated
		};

		typedef std::vector<UsedData> UsedDataVec;

		typedef std::map<std::string, bool> IsPriceDataMap;
		typedef std::map<std::string, IntDay> DiMap;
		typedef std::map<std::string, FloatMatrixDataPtr> DiFactorMap;

		std::string 			m_datasetName = "baseData";			/// The name of the dataset
		IsPriceDataMap			m_priceDataMap;						/// Map telling us whether something is price or volume
		StringVec 				m_priceDatasets;					/// The price datasets
		StringVec				m_volumeDatasets;					/// The volume datasets
		StringMap				m_mappings;							/// Forward mappings from pit_* to the actual datasets
		StringMap				m_rmappings;						/// The reverse mappings from actual datasets to pit_*
		int						m_backdays;							/// The total number of backdays that we have for this
		DiFactorMap				m_currAdjFactors;					/// The current adjustment factors
		DiMap					m_diAdjFactors;						/// The day on which this was last update
		IntDay					m_delay = 1;						/// What is the delay of this data

		UsedDataVec				m_dataList;							/// The data that we need to update

		void					loadAdjustmentFactors(IDataRegistry& dr, UsedData& udata);
		void					adjustData(IDataRegistry& dr, UsedData& udata, IntDay diEnd);

	public:
								PIT_AdjData(const ConfigSection& config);
		virtual 				~PIT_AdjData();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);

		virtual DataPtr			dataLoaded(const RuntimeData& rt, IDataRegistry& dr, DataPtr data, const std::string& cacheId, IUniverse* universe);
		virtual void			dataTick(const RuntimeData& rt, IDataRegistry& dr);

		//inline IntDay			getDiAdjFactors(const std::string& dataName, FloatMatrixData** factors) {
		//	auto iter			= m_diAdjFactors.find(dataName);

		//	if (iter == m_diAdjFactors.end()) {
		//		auto secMaster = dataRegistry()->getSecurityMaster();
		//		size_t numSecurities = secMaster->size(0);
		//		auto currAdjFactors = std::make_shared<FloatMatrixData>(nullptr, DataDefinition(), 1, numSecurities);
		//		currAdjFactors->setMemory(1.0f);
		//		m_currAdjFactors[dataName] = currAdjFactors;
		//		m_diAdjFactors[dataName] = 0; 
		//		*factors = currAdjFactors.get();
		//		return 0;
		//	}

		//	auto diter			= m_currAdjFactors.find(dataName);
		//	ASSERT(diter != m_currAdjFactors.end(), "Invalid state for di factors!");
		//	*factors = diter->second.get();
		//	return iter->second;
		//}

		//inline void				setDiAdjFactors(const std::string& dataName, IntDay di) {
		//	m_diAdjFactors[dataName] = di;
		//}
	};

	PIT_AdjData::PIT_AdjData(const ConfigSection& config) : IDataLoader(config, "PIT_AdjData") {
		CTrace("Created PIT_AdjData!");
		m_backdays = (int)parentConfig()->defs().backdays();
		m_datasetName = config.getRequired("datasetName");
		m_delay = (IntDay)config.get<int>("delay", (int)m_delay);
		
		auto priceDatasets = config.getRequired("priceDatasets");
		m_priceDatasets = Util::split(priceDatasets, ",");
		ASSERT(m_priceDatasets.size(), "Invalid price datasets: " << priceDatasets);

		for (auto& priceDataset : m_priceDatasets) {
			std::string pitPriceDataset = "pit_adj_" + priceDataset;
			m_mappings[pitPriceDataset] = m_datasetName + "." + priceDataset;
			m_rmappings[priceDataset] = pitPriceDataset;
			m_priceDataMap[pitPriceDataset] = true;
		}

		auto volumeDatasets = config.getRequired("volumeDatasets");
		m_volumeDatasets = Util::split(volumeDatasets, ",");
		ASSERT(m_priceDatasets.size(), "Invalid volume datasets: " << volumeDatasets);
		
		for (auto& volumeDataset : m_volumeDatasets) {
			std::string pitVolumeDataset = "pit_adj_" + volumeDataset;
			m_mappings[pitVolumeDataset] = m_datasetName + "." + volumeDataset;
			m_rmappings[volumeDataset] = pitVolumeDataset;
			m_priceDataMap[pitVolumeDataset] = false;
		}
	}

	PIT_AdjData::~PIT_AdjData() {
		CTrace("Deleting PIT_AdjData!");
	}

	void PIT_AdjData::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		Dataset ds(m_datasetName);

		for (auto iter = m_mappings.begin(); iter != m_mappings.end(); iter++) {
			std::string dataset = iter->first;
			ds.values.push_back(DataValue(dataset, DataType::kFloat, sizeof(float), THIS_DATA_FUNC(PIT_AdjData::build), DataFrequency::kDaily, 0));
		}

		datasets.add(ds);
	}

	void PIT_AdjData::loadAdjustmentFactors(IDataRegistry& dr, UsedData& udata) { 
		if (udata.adjFactor)
			return;

		std::string adjFactorId = config().getRequired("adjFactorId");
		std::string adjVolFactorId = config().getRequired("adjVolFactorId");
		std::string factorToLoad = config().getRequired("adjFactorId");

		if (m_priceDataMap[udata.dataName] == false)
			factorToLoad = config().getRequired("adjVolFactorId");

		udata.adjFactor = dr.floatData(udata.universe, factorToLoad);
	}

	void PIT_AdjData::build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		auto fdata = std::make_shared<FloatMatrixData>(dci.universe, dci.def, 1, 1);
		frame.data = fdata;
		(*fdata.get())(0, 0) = 0.0f;
		frame.dataSize = frame.data->dataSize();
		frame.dataOffset = 0;
	}

	void PIT_AdjData::adjustData(IDataRegistry& dr, UsedData& udata, IntDay diEnd) {
		if (diEnd <= udata.diEnd)
			return;

		loadAdjustmentFactors(dr, udata);

		std::string orgData = m_mappings[udata.dataName];
		ASSERT(!orgData.empty(), "Invalid dataName: " << udata.dataName << " - cannot find the original PIT data source!");
		const FloatMatrixData* srcData = dr.floatData(udata.universe, orgData);
		FloatMatrixData* dstData = udata.data.get();
		size_t numCols = dstData->cols();

		const FloatMatrixData* factors = udata.adjFactor;

		FloatMatrixData thisFactors(nullptr, factors->def(), 1, factors->cols());
		thisFactors[0] = factors->row(diEnd);

		//factors->applyDelay() = dstData->applyDelay();
		//srcData->applyDelay() = dstData->applyDelay();

		//if (m_delay == 0)

		/// Since this is delayed data
		if (diEnd < dstData->rows())
			dstData->row(diEnd) = srcData->row(diEnd);

		//ASSERT(srcData->cols() == dstData->cols(), "Column mismatch. Src")

		for (int di = diEnd - 1; di >= std::max(diEnd - m_backdays, 0); di--) {
			dstData->row(di) = (srcData->row(di) * thisFactors[0]).eval();
			thisFactors[0] = thisFactors[0] * factors->row(di);
		}

		udata.diEnd = diEnd;

		//factors->applyDelay() = false;
		//srcData->applyDelay() = false;
	}

	DataPtr PIT_AdjData::dataLoaded(const RuntimeData& rt, IDataRegistry& dr, DataPtr data_, const std::string& cacheId, IUniverse* universe) {
		/// We don't handle security master. 
		/// Since this is a floating point data, therefore we always use and allow the optimised data
		if (!universe || universe == dr.getSecurityMaster())
			return data_;

		std::string dataName = data_->name();
		std::string orgData = m_mappings[dataName];
		const FloatMatrixData* srcData = dr.floatData(universe, orgData);

		ASSERT(srcData, "Unable to load data: " << orgData);
		FloatMatrixData* data = new FloatMatrixData(nullptr, data_->def(), srcData->rows(), srcData->cols());
		data->invalidateAll();

		/// Copy, other vital bits of information from the data 
		data->delay()		= srcData->delay();
		data->diOffset()	= srcData->diOffset();
		data->universe()	= srcData->universe();
		data->applyDelay()	= srcData->applyDelay();
		data->diLookup()	= srcData->diLookup();
		data->namedArgument() = srcData->namedArgument();
		data->name()		= srcData->name();

		UsedData udata;
		udata.data			= FloatMatrixDataPtr(data);
		udata.universe		= universe;
		udata.cacheId		= cacheId;
		udata.dataName		= dataName;

		adjustData(dr, udata, rt.diStart);

		m_dataList.push_back(udata);
		return udata.data;
	}

	void PIT_AdjData::dataTick(const RuntimeData& rt, IDataRegistry& dr) {
		for (UsedData& udata : m_dataList) {
			adjustData(dr, udata, rt.di);
		}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createPIT_AdjData(const pesa::ConfigSection& config) {
	return new pesa::PIT_AdjData((const pesa::ConfigSection&)config);
}
