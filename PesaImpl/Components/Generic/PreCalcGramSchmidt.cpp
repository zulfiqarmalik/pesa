/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// PreCalcGramSchmidt.cpp
///
/// Created on: 08 May 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Core/Math/Stats.h"

namespace pesa {
	class PreCalcGramSchmidt : public IDataLoader {
	private:
		StringVec				m_factorIds; 
		std::map<std::string, size_t> m_indexMap;
		std::string				m_datasetName = "gs_axioma";
		std::string				m_srcDatasetName = "axioma";

		Eigen::VectorXf 		getFactor(const DataCacheInfo& dci, size_t fid, IntDay di, std::string dsName);

	public:
								PreCalcGramSchmidt(const ConfigSection& config);
		virtual 				~PreCalcGramSchmidt();

		virtual void 			initDatasets(IDataRegistry& dr, pesa::Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);
	};

	PreCalcGramSchmidt::PreCalcGramSchmidt(const ConfigSection& config) : IDataLoader(config, "OP_MULTI_FACTOR_NEUTRALISE") {
		std::string factorIds = config.getRequired("factors");
		m_factorIds = Util::split(factorIds, ",");
		ASSERT(m_factorIds.size(), "Invalid factors: " << factorIds);

		m_datasetName = config.getRequired("datasetName");
		m_srcDatasetName = config.getString("srcDatasetName", m_srcDatasetName);
	}

	PreCalcGramSchmidt::~PreCalcGramSchmidt() {
	}

	Eigen::VectorXf PreCalcGramSchmidt::getFactor(const DataCacheInfo& dci, size_t fid, IntDay di, std::string dsName) {
		const std::string& factorId = dsName + "." + m_factorIds[fid];
		const FloatMatrixData* factor = dci.rt.dataRegistry->floatData(dci.universe, factorId);
		return factor->mat().mat().row(std::max(di, 0)).nanClear();
	}

	void PreCalcGramSchmidt::initDatasets(IDataRegistry& dr, pesa::Datasets& dss) {
		std::string datasetName = config().getRequired("datasetName");
		Dataset ds(datasetName);

		for (size_t fid = 0; fid < m_factorIds.size(); fid++) {
			const std::string& factorId = m_factorIds[fid];
			m_indexMap[factorId] = fid;
			DataValue dv(factorId, DataType::kFloat, DataType::size(DataType::kFloat), nullptr, DataFrequency::kDaily, DataFlags::kCollectAllBeforeWriting);
			ds.add(dv);
		}

		dss.add(ds);
	}

	void PreCalcGramSchmidt::build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		//Eigen::VectorXf prevFactor = getFactor(dci, 0, dci.di - 1);
		std::string factorId = dci.def.name;
		size_t index = m_indexMap[factorId];
		Eigen::VectorXf nextFactor = getFactor(dci, index, dci.di - 1, m_srcDatasetName);
		float len = std::sqrt(nextFactor.dot(nextFactor));

		if (len != 0.0f) {
			for (size_t fid = 0; fid < index; fid++) {
				nextFactor = Stats::neutralise(getFactor(dci, fid, dci.di - 1, m_datasetName), nextFactor);
			}
		}
		else {
			CWarning("Factor length 0");
		}

		FloatMatrixData* data = new FloatMatrixData(nullptr, 1, nextFactor.rows()) ;
		data->row(0) = nextFactor;

		frame.data = FloatMatrixDataPtr(data);
		frame.dataSize = frame.data->dataSize();
		frame.dataOffset = 0;
		frame.noDataUpdate = false;

		CDebug("[%u - di = %d] %s.%s.%s", dci.rt.dates[dci.di], dci.di, dci.universe ? dci.universe->name() : "SecMaster", m_datasetName, factorId);
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createPreCalcGramSchmidt(const pesa::ConfigSection& config) {
	return new pesa::PreCalcGramSchmidt((const pesa::ConfigSection&)config);
}

