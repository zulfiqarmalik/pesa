/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// QV_Preprocess.cpp
///
/// Created on: 27 Jul 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Poco/Timestamp.h" 
#include "Poco/String.h"
#include "Poco/Format.h" 
#include "PesaImpl/Components/MorningStar/MS_Util.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// MorningStar Security Master
	////////////////////////////////////////////////////////////////////////////
	class QV_Preprocess : public IDataLoader {
	private:
		typedef std::unordered_map<IntDate, UIntVec>    DailySecMap;
		typedef std::unordered_map<std::string, size_t> SymbolLookupMap;
		typedef std::vector<SymbolLookupMap>			SymbolLookupMapVec;
        typedef std::unordered_map<std::string, bool>   ExchangeBoolMap;

		typedef std::unordered_map<std::string, StringData*> DataMap;

		MS_Util::SecurityLookup	m_securityLookup;						/// Which securities do we already have? 
		SecurityVec				m_allSecurities;						/// All the securities within the system
		SymbolLookupMap			m_bbSymbolLUT;							/// Lookup index
		SymbolLookupMapVec		m_msSymbolLUT;							/// Lookup index from MorningStar
		int						m_delay = 1;							/// Delay

		DataMap 				m_classifications;						/// The classification information for each of the securities
		DataMap					m_metadata;								/// The metadata information that we're building up!
		DataMap					m_secInfo;								/// The sec info
		StringData*				m_tickers = nullptr;					/// Historical tickers

		StringVec				m_exchangeIds;							/// All the exchange ids
		StringMap				m_exchangeCountries;					/// ExchangeId code to country mapping
		StringMap				m_exchangeNames;						/// ExchangeId to name/MIC name mapping
		StringMap				m_countryCodeToCurrency;				/// Country code to currency pair
		StringMap				m_currencyPairToCurrency;				/// Currency pair to currency mapping
		StringMap				m_currencyToCurrencyPair;				/// From currency to currency pair

		bool					m_needsCurrency = false;				/// Whether we need the currency or not

		StringVec 				m_metadataFields = {
			"figi", "cfigi", "parentFigi", "isin", "permId", "quotePermId", "issuerPermId", "perfId", "country", "currency"
		};

		StringVec				m_secInfoFields = {
			"tickerId", "instrumentId"
		};

		void 					buildSecurities(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities);
		void 					buildIndices(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities);
		void 					buildIndex(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities, const std::string& index);
		void 					buildDetails(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities);

		void					buildClassifications(IDataRegistry& dr, const DataCacheInfo& dci);
		void 					buildClassification(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);

		void 					buildAllMetadata(IDataRegistry& dr, const DataCacheInfo& dci);
		void 					buildMetadata(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		//void 					buildTickers(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);

		void 					buildAllSecInfo(IDataRegistry& dr, const DataCacheInfo& dci);
		void 					buildSecInfo(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);

		void					constructLUT(const RuntimeData& rt, IDataRegistry& dr, IntDay diStart, IntDay diEnd);
		void					constructBBLUT(const RuntimeData& rt, IDataRegistry& dr, IntDay diStart, IntDay diEnd);
		void					constructMSLUT(const RuntimeData& rt, IDataRegistry& dr, IntDay diStart, IntDay diEnd);

		int						getBBSymbolIndex(const std::string& symbol);
		int						getMSSymbolIndex(const RuntimeData& rt, IDataRegistry& dr, IntDay di, const std::string& symbol);

		bool					fillSecData(IDataRegistry& dr, const DataCacheInfo& dci, IntDay di, std::vector<Security>& newSecurities, const std::string& symbol, Security& sec);
		bool					fillSecWithBBData(IDataRegistry& dr, IntDay di, int ii, Security& sec);
		bool					fillSecWithMSData(IDataRegistry& dr, IntDay di, int ii, Security& sec);

		void					getBBClassification(IDataRegistry& dr, const DataCacheInfo& dci, size_t ii, std::string& sector, std::string& group, std::string& industry, std::string& subIndustry);
		void					getMSClassification(IDataRegistry& dr, const DataCacheInfo& dci, size_t ii, std::string& sector, std::string& group, std::string& industry, std::string& subIndustry);

		void					loadExchangeAndCurrencyInfo();

	public:
								QV_Preprocess(const ConfigSection& config);
		virtual 				~QV_Preprocess();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			buildSecurityMaster(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);

		virtual void 			preDataBuild(IDataRegistry& dr, const DataCacheInfo& dci);
		virtual void 			postDataBuild(IDataRegistry& dr, const DataCacheInfo& dci);
	};

	QV_Preprocess::QV_Preprocess(const ConfigSection& config) : IDataLoader(config, "QV_Preprocess") {
		m_needsCurrency			= config.getBool("needsCurrency", m_needsCurrency);
		m_delay					= config.getInt("delay", m_delay);
		loadExchangeAndCurrencyInfo();
	}

	QV_Preprocess::~QV_Preprocess() {
		CTrace("Deleting QV_Preprocess");
	}

	void QV_Preprocess::initDatasets(IDataRegistry& dr, Datasets& dss) {
		std::string name = config().getRequired("name");
		std::string prefix = config().getString("prefix", "");

		Dataset ds(name);
		ds.add(DataValue(prefix + "Master", DataType::kSecurityData, Security::s_version, THIS_DATA_FUNC(QV_Preprocess::buildSecurityMaster),
			DataFrequency::kStatic, DataFlags::kAlwaysLoad | DataFlags::kUpdatesInOneGo | DataFlags::kSecurityMaster));

		for (const auto& field : m_secInfoFields)
			ds.add(DataValue(field, DataType::kIndexedString, 0, THIS_DATA_FUNC(QV_Preprocess::buildSecInfo), DataFrequency::kDaily, 0));

		ds.add(DataValue("sector", DataType::kIndexedString, 0, THIS_DATA_FUNC(QV_Preprocess::buildClassification), DataFrequency::kStatic, DataFlags::kUpdatesInOneGo | DataFlags::kAlwaysOverwrite));
		ds.add(DataValue("group", DataType::kIndexedString, 0, THIS_DATA_FUNC(QV_Preprocess::buildClassification), DataFrequency::kStatic, DataFlags::kUpdatesInOneGo | DataFlags::kAlwaysOverwrite));
		ds.add(DataValue("industry", DataType::kIndexedString, 0, THIS_DATA_FUNC(QV_Preprocess::buildClassification), DataFrequency::kStatic, DataFlags::kUpdatesInOneGo | DataFlags::kAlwaysOverwrite));
		ds.add(DataValue("subIndustry", DataType::kIndexedString, 0, THIS_DATA_FUNC(QV_Preprocess::buildClassification), DataFrequency::kStatic, DataFlags::kUpdatesInOneGo | DataFlags::kAlwaysOverwrite));

		for (const auto& field : m_metadataFields)
			ds.add(DataValue(field, DataType::kIndexedString, 0, THIS_DATA_FUNC(QV_Preprocess::buildMetadata), DataFrequency::kStatic, DataFlags::kUpdatesInOneGo | DataFlags::kAlwaysOverwrite));

		//ds.add(DataValue("ticker", DataType::kIndexedString, 0, THIS_DATA_FUNC(QV_Preprocess::buildTickers), DataFrequency::kDaily, 0));

		dss.add(ds);
	}

	void QV_Preprocess::loadExchangeAndCurrencyInfo() {
		std::string s_exchangeIds = config().getResolvedString("exchangeIds", "");
		//std::string s_exchangeMappings = config().getResolvedRequiredString("exchangeMappings");
		std::string s_currencies = config().getResolvedString("currencies", "");
		std::string s_currencyQuotes = config().getResolvedString("currencyQuotes", "");

		if (s_exchangeIds.empty() || s_currencies.empty() || s_currencyQuotes.empty())
			return;

		/// OK, now we parse the data
		FrameworkUtil::parseExchangeIds_Detailed(s_exchangeIds, m_exchangeIds, m_exchangeNames, m_exchangeCountries);

		StringVecMap currencies;
		StringMap countryCcy;
		FrameworkUtil::parseCurrencies(s_currencies, currencies, &countryCcy);

		FrameworkUtil::parseCurrencyQuoteStyles(s_currencyQuotes, m_currencyPairToCurrency, m_currencyToCurrencyPair);

		for (const auto iter : countryCcy) {
			std::string country = iter.first;
			std::string currency = iter.second;
//			std::string currencyPair = iter.second;
//
//			auto ccyIter = m_currencyPairToCurrency.find(currencyPair);
//			ASSERT(ccyIter != m_currencyPairToCurrency.end(), "Unable to find currency pair: " << currencyPair);

			m_countryCodeToCurrency[country] = currency;
		}
	}

	void QV_Preprocess::buildAllMetadata(IDataRegistry& dr, const DataCacheInfo& dci) {
		if (m_metadata["figi"])
			return;

		constructLUT(dci.rt, dr, dci.diEnd, dci.diEnd);
		size_t numSecurities			= m_allSecurities.size();

		for (const auto& field : m_metadataFields) {
			auto* data					= new ShortIndexedStringData(nullptr, DataDefinition(field, dci.def.itemType, dci.def.itemSize, dci.def.frequency, dci.def.flags, dci.def.typeName));
			data->ensureSize(1, numSecurities);
			
			m_metadata[field] 			= data;
		}

		for (size_t ii = 0; ii < numSecurities; ii++) {
			const auto& sec				= m_allSecurities[ii];
			auto symbol					= sec.uuidStr();
			int bbIndex					= getBBSymbolIndex(symbol);
			int msIndex					= getMSSymbolIndex(dci.rt, dr, dci.diEnd, symbol);

			std::string					figi, cfigi, parentFigi, isin, permId, quotePermId, issuerPermId, perfId, country, currency = "USD";

			if (bbIndex >= 0) {
				const StringData* figis = dr.stringData(nullptr, "bloomberg.figi", true);
				const StringData* cfigis = dr.stringData(nullptr, "bloomberg.cfigi", true);
				const StringData* isins = dr.stringData(nullptr, "bloomberg.isin", true);
				const StringData* parentFigis = dr.stringData(nullptr, "bloomberg.parentFigi", true);
				const StringData* countries = dr.stringData(nullptr, "bloomberg.country", true);

				figi					= figis->getValue(0, bbIndex);
				cfigi					= cfigis->getValue(0, bbIndex);
				isin					= isins->getValue(0, bbIndex);
				parentFigi				= parentFigis->getValue(0, bbIndex);
				country					= countries->getValue(0, bbIndex);
			}

			if (msIndex >= 0) {
				const StringData* figis = dr.stringData(nullptr, "preprocess.figi", true);
				const StringData* cfigis = dr.stringData(nullptr, "preprocess.cfigi", true);
				const StringData* isins = dr.stringData(nullptr, "preprocess.isin", true);
				const StringData* perfIds = dr.stringData(nullptr, "preprocess.ms_perfId", true);
				const StringData* permIds = dr.stringData(nullptr, "preprocess.permId", true);
				const StringData* issuerPermIds = dr.stringData(nullptr, "preprocess.issuerPermId", true);
				const StringData* quotePermIds = dr.stringData(nullptr, "preprocess.quotePermId", true);
				const StringData* countries = dr.stringData(nullptr, "preprocess.country", true);
				const StringData* currencies = dr.stringData(nullptr, "preprocess.country", true);

				IntDay di				= (IntDay)figis->rows() - 1;

				if (figi.empty())
					figi				= figis->getValue(di, msIndex);
				if (cfigi.empty())
					cfigi				= cfigis->getValue(di, msIndex);

				if (isin.empty())
					isin				= isins->getValue(di, msIndex);

				if (country.empty())
					country				= countries->getValue(di, msIndex);

				perfId					= perfIds->getValue(di, msIndex);
				permId					= permIds->getValue(di, msIndex);
				issuerPermId			= issuerPermIds->getValue(di, msIndex);
				quotePermId				= quotePermIds->getValue(di, msIndex);
				currency				= currencies->getValue(di, msIndex);
			}

			if (isin == "nan")
				isin					= "";

			m_metadata["figi"]->setValue(0, ii, figi);
			m_metadata["cfigi"]->setValue(0, ii, cfigi);
			m_metadata["isin"]->setValue(0, ii, isin);
			m_metadata["parentFigi"]->setValue(0, ii, parentFigi);
			m_metadata["permId"]->setValue(0, ii, permId);
			m_metadata["quotePermId"]->setValue(0, ii, quotePermId);
			m_metadata["issuerPermId"]->setValue(0, ii, issuerPermId);
			m_metadata["perfId"]->setValue(0, ii, perfId);
			m_metadata["country"]->setValue(0, ii, country);
			m_metadata["currency"]->setValue(0, ii, currency);
		}
	}

	void QV_Preprocess::buildMetadata(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		buildAllMetadata(dr, dci);

		std::string dataName= dci.def.name;

		auto* data 			= m_metadata[dataName];
		ASSERT(data, "Unable to find metadata: " << dataName);

		frame.data 			= DataPtr(data);
		frame.dataSize 		= data->dataSize();
		frame.noDataUpdate 	= false;
		frame.dataOffset 	= 0;

		CDebug("Built metadata: %s", dataName);
	}

	void QV_Preprocess::buildAllSecInfo(IDataRegistry& dr, const DataCacheInfo& dci) {
		if (m_secInfo["tickerId"])
			return;

		constructLUT(dci.rt, dr, dci.diStart, dci.diEnd);

		size_t numSecurities			= m_allSecurities.size();
		IntDay numRows					= dci.diEnd - dci.diStart + 1;

		for (const auto& field : m_secInfoFields) {
			auto* data					= new ShortIndexedStringData(nullptr, DataDefinition(field, dci.def.itemType, dci.def.itemSize, dci.def.frequency, dci.def.flags, dci.def.typeName));
			data->ensureSize(numRows, numSecurities);
			m_secInfo[field] 			= data;
		}

		for (size_t ii = 0; ii < numSecurities; ii++) {
			const auto& sec				= m_allSecurities[ii];
			auto symbol					= sec.uuidStr();
			int bbIndex					= getBBSymbolIndex(symbol);

			const StringData* tickerIds = dr.stringData(nullptr, "preprocess.ticker", true);
			const StringData* instrumentIds = dr.stringData(nullptr, "preprocess.instrumentId", true);

			std::string					figi, cfigi, parentFigi, isin, permId, quotePermId, issuerPermId, perfId;

			for (IntDay di = dci.diStart; di <= dci.diEnd; di++) {
				/// Symbol index already take delay into account
				int msIndex = getMSSymbolIndex(dci.rt, dr, di, symbol);
				IntDay dii = std::max(di - m_delay, 0);

				if (msIndex >= 0) {
					std::string tickerId= tickerIds->getValue(dii, msIndex);
					std::string instrumentId =instrumentIds->getValue(dii, msIndex);

					m_secInfo["tickerId"]->setValue(di - dci.diStart, ii, tickerId);
					m_secInfo["instrumentId"]->setValue(di - dci.diStart, ii, instrumentId);
				}
			}
		}
	}

	void QV_Preprocess::buildSecInfo(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		buildAllSecInfo(dr, dci);

		std::string dataName= dci.def.name;

		auto* secInfoData	= m_secInfo[dataName];
		ASSERT(secInfoData, "Unable to find sec-info data: " << dataName);

		IntDay di			= dci.di - dci.diStart;
		IntDate date		= dci.rt.dates[dci.di]; 

		ASSERT(di < secInfoData->rows(), "Insufficient rows for data: " << dataName << " - Expecting at least: " << di << " - Max count: " << secInfoData->rows());
		DataPtr data		= secInfoData->extractRow(di);

		frame.data 			= data;
		frame.dataSize 		= data->dataSize();
		frame.noDataUpdate 	= false;
		frame.dataOffset 	= 0;

		CDebug("[%u] Built sec info: %s", date, dataName);
	}

	void QV_Preprocess::buildClassifications(IDataRegistry& dr, const DataCacheInfo& dci) {
		if (m_classifications["sector"])
			return;

		constructLUT(dci.rt, dr, dci.diEnd, dci.diEnd);

		size_t numSecurities			= m_allSecurities.size();

		m_classifications["sector"]		= new ShortIndexedStringData(nullptr, DataDefinition("sector", dci.def.itemType, dci.def.itemSize, dci.def.frequency, dci.def.flags, dci.def.typeName));
		m_classifications["group"]		= new ShortIndexedStringData(nullptr, DataDefinition("group", dci.def.itemType, dci.def.itemSize, dci.def.frequency, dci.def.flags, dci.def.typeName));
		m_classifications["industry"]	= new ShortIndexedStringData(nullptr, DataDefinition("industry", dci.def.itemType, dci.def.itemSize, dci.def.frequency, dci.def.flags, dci.def.typeName));
		m_classifications["subIndustry"]= new ShortIndexedStringData(nullptr, DataDefinition("subIndustry", dci.def.itemType, dci.def.itemSize, dci.def.frequency, dci.def.flags, dci.def.typeName));

		m_classifications["sector"]->ensureSize(1, numSecurities);
		m_classifications["group"]->ensureSize(1, numSecurities);
		m_classifications["industry"]->ensureSize(1, numSecurities);
		m_classifications["subIndustry"]->ensureSize(1, numSecurities);

		for (size_t ii = 0; ii < numSecurities; ii++) {
			std::string sector, group, industry, subIndustry;

			getBBClassification(dr, dci, ii, sector, group, industry, subIndustry);
			getMSClassification(dr, dci, ii, sector, group, industry, subIndustry);

			m_classifications["sector"]->setValue(0, ii, sector);
			m_classifications["group"]->setValue(0, ii, group);
			m_classifications["industry"]->setValue(0, ii, industry);
			m_classifications["subIndustry"]->setValue(0, ii, subIndustry);
		}
	}

	void QV_Preprocess::buildClassification(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		buildClassifications(dr, dci);

		std::string dataName= dci.def.name;

		auto* data 			= m_classifications[dataName];
		ASSERT(data, "Unable to find classification data: " << dataName);

		frame.data 			= DataPtr(data);
		frame.dataSize 		= data->dataSize();
		frame.noDataUpdate 	= false;
		frame.dataOffset 	= 0;

		CDebug("Built classification: %s", dataName);
	}

	void QV_Preprocess::postDataBuild(IDataRegistry& dr, const DataCacheInfo& dci) {
		/// We do not need this data anymore ...
		m_securityLookup.clear();
		m_allSecurities.clear();
	} 

	void QV_Preprocess::preDataBuild(IDataRegistry& dr, const DataCacheInfo& dci) {
		std::string name = config().getRequired("name");
		std::string prefix = config().getString("prefix", "");

		const SecurityData* secData = dr.get<SecurityData>(nullptr, name + "." + prefix + "Master", true);

		if (secData) {
			size_t numSecurities = secData->numSecurities();
			m_allSecurities.resize(numSecurities);
			CInfo("Existing number of securities: %z", numSecurities);

			for (size_t i = 0; i < numSecurities; i++) {
				const Security& sec = secData->security(i);

				auto iter = m_securityLookup.find(sec.uuidStr());

				ASSERT(iter == m_securityLookup.end(), "Security exists more than once in the security master. \nSecurity: " << sec.debugString() << "\nExisting Security: " << m_allSecurities[iter->second].debugString());
				//m_securityLookup[sec.uuidStr()] = sec.index;
				//m_securityLookup[sec.instrumentId()] = sec.index;

				m_securityLookup[sec.uuidStr()] = sec.index;
				m_securityLookup[sec.cfigiStr()] = sec.index;
				m_securityLookup[sec.isinStr()] = sec.index;
				m_securityLookup[sec.figiStr()] = sec.index;
				m_securityLookup[sec.permIdStr()] = sec.index;
				m_securityLookup[sec.perfIdStr()] = sec.index;

				m_allSecurities[i] = sec;
			}
		}
	}

	void QV_Preprocess::build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
	}

	void QV_Preprocess::constructBBLUT(const RuntimeData& rt, IDataRegistry& dr, IntDay diStart, IntDay diEnd) {
		if (m_bbSymbolLUT.size())
			return;

		const StringData* tickers 	= dr.stringData(nullptr, "bloomberg.ticker", true);
		ASSERT(tickers, "Unable to load bloomberg tickers data!");

		const StringData* bbTickers	= dr.stringData(nullptr, "bloomberg.bbTicker", true);
		ASSERT(bbTickers, "Unable to load bloomberg bbTickers data!");

		const StringData* figis		= dr.stringData(nullptr, "bloomberg.figi", true);
		ASSERT(figis, "Unable to load bloomberg figi data!");

		const StringData* cfigis	= dr.stringData(nullptr, "bloomberg.cfigi", true);
		ASSERT(cfigis, "Unable to load bloomberg cfigi data!");

		const StringData* isins		= dr.stringData(nullptr, "bloomberg.isin", true);
		ASSERT(isins, "Unable to load bloomberg isin data!");

		auto tickerCount 	= tickers->cols();
		auto bbTickerCount 	= bbTickers->cols();
		auto figiCount 		= figis->cols();
		auto cfigiCount 	= cfigis->cols();
		auto isinCount		= isins->cols();

		ASSERT(tickerCount, "No columns for the ticker data!");

		ASSERT(tickerCount == bbTickerCount, Poco::format("Ticker and BB-Ticker count mismatch. Ticker Count: %z - BB-Ticker Count: %z", tickerCount, bbTickerCount));
		ASSERT(tickerCount == figiCount, Poco::format("Ticker and FIGI count mismatch. Ticker Count: %z - FIGI Count: %z", tickerCount, figiCount));

		CDebug("Constructing Bloomberg LUT for num tickers: %z", tickerCount);

		for (auto ii = 0; ii < tickerCount; ii++) {
			auto ticker		= tickers->getValue(0, ii);
			auto bbTicker	= bbTickers->getValue(0, ii);
			auto figi		= figis->getValue(0, ii);
			auto cfigi		= cfigis->getValue(0, ii);
			auto isin		= isins->getValue(0, ii);

			auto spacePos	= ticker.find(' ');

			if (spacePos != std::string::npos)
				ticker		= ticker.substr(0, spacePos);

			bbTicker		= Poco::replace(bbTicker, " Equity", "");

			//m_bbSymbolLUT[ticker] = ii;
			//m_bbSymbolLUT[bbTicker] = ii;
			m_bbSymbolLUT[figi] = ii;
			m_bbSymbolLUT[cfigi] = ii;
			m_bbSymbolLUT[isin] = ii;
		}

		CDebug("Bloomberg Symbol LUT Count: %z", m_bbSymbolLUT.size());
	}

	void QV_Preprocess::constructMSLUT(const RuntimeData& rt, IDataRegistry& dr, IntDay diStart, IntDay diEnd) {
		const StringData* tickers 	= dr.stringData(nullptr, "preprocess.ticker", true);
		const StringData* names 	= dr.stringData(nullptr, "preprocess.name", true);
		const StringData* figis		= dr.stringData(nullptr, "preprocess.figi", true);
		const StringData* cfigis 	= dr.stringData(nullptr, "preprocess.cfigi", true);
		const StringData* isins		= dr.stringData(nullptr, "preprocess.isin", true);

		size_t tickerCount			= tickers->cols();
		IntDay numDays				= (IntDay)rt.dates.size();

		CDebug("Constructing MorningStar LUT for num tickers: %z", tickerCount);

		/// Make the LUT array full size but only calculate for the days that are needed!
		if (!m_msSymbolLUT.size())
			m_msSymbolLUT.resize(numDays);

		for (IntDay di = diStart; di <= diEnd; di++) {
			/// If we've already calculated for this index
			if (m_msSymbolLUT[di].size())
				continue;

			IntDay dii				= std::max(di - m_delay, 0);

			for (size_t ii = 0; ii < tickerCount; ii++) {
				auto ticker			= tickers->getValue(dii, ii);
				auto cfigi			= cfigis->getValue(dii, ii);
				auto figi			= figis->getValue(dii, ii);
				auto isin			= isins->getValue(dii, ii);
				auto name			= names->getValue(dii, ii);

				m_msSymbolLUT[di][figi]	= ii;
				m_msSymbolLUT[di][cfigi]= ii;
				m_msSymbolLUT[di][isin]	= ii;
			}
		}

		CDebug("MorningStar Symbol LUT Count: %z", tickerCount);
	}

	void QV_Preprocess::constructLUT(const RuntimeData& rt, IDataRegistry& dr, IntDay diStart, IntDay diEnd) {
		constructBBLUT(rt, dr, diStart, diEnd);
		constructMSLUT(rt, dr, diStart, diEnd);
	}

	int QV_Preprocess::getBBSymbolIndex(const std::string& symbol) {
		auto iter				= m_bbSymbolLUT.find(symbol);
		return iter == m_bbSymbolLUT.end() ? -1 : (int)iter->second;
	}

	int QV_Preprocess::getMSSymbolIndex(const RuntimeData& rt, IDataRegistry& dr, IntDay di, const std::string& symbol) {
		ASSERT(di < (IntDay)m_msSymbolLUT.size(), "Invalid di: " << di << " requested. Total days in MS Symbol LUT: " << m_msSymbolLUT.size());

		if (!m_msSymbolLUT[di].size())
			constructMSLUT(rt, dr, di, di);

		auto iter				= m_msSymbolLUT[di].find(symbol);
		return iter == m_msSymbolLUT[di].end() ? -1 : (int)iter->second;
	}

	bool QV_Preprocess::fillSecData(IDataRegistry& dr, const DataCacheInfo& dci, IntDay di, std::vector<Security>& newSecurities, const std::string& symbol, Security& sec) {
		int bbIndex = getBBSymbolIndex(symbol);
		int msIndex = getMSSymbolIndex(dci.rt, dr, di, symbol);

		/// Always try to find BB data first!
		if (bbIndex >= 0)
			fillSecWithBBData(dr, di, bbIndex, sec);

		if (msIndex >= 0)
			fillSecWithMSData(dr, di, msIndex, sec);

		if (sec.uuidStr().empty())
			return false;

		/// Do we need currency for sure?

		//if (sec.currencyStr().empty() || sec.exchangeStr().empty() || sec.countryCode2Str().empty()) { 
		auto instParts = Util::split(sec.instrumentId(), ".");
		if (instParts.size() != 3 && m_needsCurrency) 
			return false;

		if (instParts.size() == 3) {
			auto exchangeId = instParts[0];
			auto exchangeName = m_exchangeNames[exchangeId];
			auto country = m_exchangeCountries[exchangeId];
			auto currency = m_countryCodeToCurrency[country];

			if (!currency.empty())
				sec.setCurrency(currency);

			if (!exchangeName.empty())
				sec.setExchange(exchangeName);

			if (sec.countryCode2Str().empty() && !country.empty())
				sec.setCountryCode2(country);
		}

		if (m_needsCurrency) { 
			std::string currency = sec.currencyStr();

			if (currency.empty()) {
				CWarning("IGNORING security because couldn't deduce currency: %s", sec.debugString());
				return false;
			}

			auto ccyPairIter = m_currencyToCurrencyPair.find(currency);
			if (ccyPairIter == m_currencyToCurrencyPair.end()) {
				CWarning("IGNORING security because of UNSUPPORTED currency: %s. Security: %s", currency, sec.debugString());
				return false;
			}
		}

		/// TODO: If this is an existing security, then update the information ...
		unsigned int existingSecIndex;
		Security* existingSec = MS_Util::mergeWithExistingSecurity(m_securityLookup, m_allSecurities, newSecurities, sec.uuidStr(), sec.cfigiStr(), "", "", "", "", existingSecIndex, false);

		if (existingSec) {
		    /// See what the exchange id is ...
		    auto existingExchangeId = Util::split(existingSec->instrumentId(), ".")[0];
		    auto newExchangeId = instParts[0];

		    /// If the exchanges don't match, then don't do anything!
		    if (existingExchangeId != newExchangeId)
                return false;

			/// Override the information from the new security into the existing one
			existingSec->override(sec);
			return false;
		}

		newSecurities.push_back(sec);

		/// This is where the security will end up in the LUT
		unsigned int secIndex = (unsigned int)(m_allSecurities.size() + (newSecurities.size() - 1));

		if (!sec.cfigiStr().empty())
			m_securityLookup[sec.cfigiStr()] = secIndex;

		if (!sec.isinStr().empty())
			m_securityLookup[sec.isinStr()] = secIndex;

		if (!sec.figiStr().empty())
			m_securityLookup[sec.figiStr()] = secIndex;

		if (!sec.permIdStr().empty())
			m_securityLookup[sec.permIdStr()] = secIndex;

		if (!sec.perfIdStr().empty())
			m_securityLookup[sec.perfIdStr()] = secIndex;
		//m_securityLookup[sec.uuidStr()] = (unsigned int)(secIndex);

		//if (!sec.instrumentId().empty())
		//	m_securityLookup[sec.instrumentId()] = (unsigned int)(secIndex);

		return true;
	}

	void QV_Preprocess::buildIndex(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities, const std::string& indexName) {
		//auto indexDataPrefix	= config().getResolvedRequiredString("indexDataPrefix"); 
		//std::string indexDataId	= Poco::format("%s.%s", indexDataPrefix, indexName);
		const auto* cfigis		= dr.stringData(nullptr, Poco::format("%s.cfigi", indexName));
		const auto* figis		= dr.stringData(nullptr, Poco::format("%s.figi", indexName));
		const auto* isins		= dr.stringData(nullptr, Poco::format("%s.isin", indexName));
		const auto* tickers		= dr.stringData(nullptr, Poco::format("%s.ticker", indexName));

		ASSERT(cfigis, "Unable to load C-FIGI data for index: " << indexName);
		ASSERT(figis, "Unable to load FIGI data for index: " << indexName);
		ASSERT(isins, "Unable to load ISIN data for index: " << indexName);
		ASSERT(tickers, "Unable to load TICKER data for index: " << indexName);

		/// Now that the index has been loaded, then we construct the data out of it ...
		auto numRows			= cfigis->rows();
		auto numCols			= cfigis->cols();
		//IntDay diStart			= (IntDay)dci.metadata.getInt("lastDi", 0);
		//IntDay diEnd			= std::min(dci.rt.diEnd, (IntDay)numRows - 1);

		CDebug("Building Index %s in range: [%d, %d]", indexName, dci.diStart, dci.diEnd);

		for (auto di = dci.diStart; di <= dci.diEnd; di++) {
			IntDate date		= dci.rt.dates[di];
			CDebug("[%u] %s", date, indexName);

			for (auto ii = 0; ii < numCols; ii++) {
				const std::string& cfigi = cfigis->getValue(di - m_delay, ii);
				const std::string& figi = figis->getValue(di - m_delay, ii);
				const std::string& isin = isins->getValue(di - m_delay, ii);
				const std::string& ticker = tickers->getValue(di - m_delay, ii);

				Security sec;

				sec.setCFIGI(cfigi);
				sec.setFIGI(figi);
				sec.setISIN(isin);

				std::string permId, perfId;

				/// This security already exists
				if (MS_Util::findIndex(m_securityLookup, cfigi, cfigi, figi, isin, permId, perfId) >= 0) {
					if (!ticker.empty()) {
						if (sec.tickerStr().empty())
							sec.setTicker(ticker);

						if (sec.bbTickerStr().empty())
							sec.setBBTicker(ticker);
					}

					continue;
				}

				if (fillSecData(dr, dci, di, newSecurities, cfigi, sec)) {
					if (!ticker.empty()) {
						if (sec.tickerStr().empty())
							sec.setTicker(ticker);

						if (sec.bbTickerStr().empty())
							sec.setBBTicker(ticker);
					}

					size_t secIndex = newSecurities.size() - 1;

					//if (newSecurities[secIndex].instrumentId().empty())
					//	newSecurities[secIndex].setInstrumentId(Poco::format("%s.%", indexName, sec.tickerStr()));

					if (newSecurities[secIndex].exchangeStr().empty())
						newSecurities[secIndex].setExchange(indexName);
				}
			}
		}

		CDebug("Num index securities: %z", newSecurities.size());
	}

	void QV_Preprocess::buildSecurities(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities) {
		const StringData* figis			= dr.stringData(nullptr, "preprocess.figi", true);
		const StringData* cfigis		= dr.stringData(nullptr, "preprocess.cfigi", true);
		const StringData* isins			= dr.stringData(nullptr, "preprocess.isin", true);

		size_t count					= figis->cols();
		IntDay diStart					= dci.diStart; ///(IntDay)dci.metadata.getInt("lastDi", 0);
		IntDay diEnd					= dci.diEnd; ///std::min(dci.rt.diEnd, (IntDay)figis->rows() - 1);
		size_t numExisting				= newSecurities.size();

		CDebug("Building Preprocess SecMaster in range: [%d, %d]", diStart, diEnd);

		for (IntDay di = diStart; di <= diEnd; di++) {

			IntDate date				= dci.rt.dates[di];
			size_t numExistingThisIter	= newSecurities.size();
			IntDay dii					= std::max(di - m_delay, 0);

			for (size_t ii = 0; ii < count; ii++) {
				std::string figi		= figis->getValue(dii, ii);
				std::string cfigi		= cfigis->getValue(dii, ii);
				std::string isin		= isins->getValue(dii, ii);

				std::string symbol		= !cfigi.empty() ? cfigi : (!figi.empty() ? figi : isin);

				if (symbol.empty())
					continue;

				Security sec;
				fillSecData(dr, dci, di, newSecurities, symbol, sec);
			}

			CDebug("[%u] Added: %z [Count: %z]", date, (newSecurities.size() - numExistingThisIter), count);
		}

		CInfo("NEW sec master securities: %z", (newSecurities.size() - numExisting));
	}

	bool QV_Preprocess::fillSecWithBBData(IDataRegistry& dr, IntDay di, int ii, Security& sec) {
		/// Load various other datasets that'll allow us to get more information on the dataset
		const StringData* bbTickers	= dr.stringData(nullptr, "bloomberg.bbTicker", true);
		ASSERT(bbTickers, "Unable to load bloomberg bbTickers data!");

		const StringData* tickers	= dr.stringData(nullptr, "bloomberg.ticker", true);
		ASSERT(tickers, "Unable to load bloomberg tickers data!");

		const StringData* figis		= dr.stringData(nullptr, "bloomberg.figi", true);
		ASSERT(figis, "Unable to load bloomberg figi data!");

		const StringData* cfigis	= dr.stringData(nullptr, "bloomberg.cfigi", true);
		ASSERT(cfigis, "Unable to load bloomberg cfigi data!");

		const StringData* isins		= dr.stringData(nullptr, "bloomberg.isin", true);
		ASSERT(isins, "Unable to load bloomberg isin data!");

		const StringData* parentFigis = dr.stringData(nullptr, "bloomberg.parentFigi", true);
		ASSERT(parentFigis, "Unable to load bloomberg parentFigi data!");

		const StringData* names		= dr.stringData(nullptr, "bloomberg.name", true);
		ASSERT(names, "Unable to load bloomberg name data!");

		const StringData* countries	= dr.stringData(nullptr, "bloomberg.country", true);
		ASSERT(countries, "Unable to load bloomberg country data!");

		const StringData* marketStatuses = dr.stringData(nullptr, "bloomberg.marketStatus", true);
		ASSERT(marketStatuses, "Unable to load bloomberg market status data!");

		auto figi			= figis->getValue(0, ii);
		auto cfigi			= cfigis->getValue(0, ii);
		auto isin			= isins->getValue(0, ii);
		auto uuidType		= SecId::kCFigi;
		auto uuid			= cfigi;

		/// Bloomberg specific issue here!
		if (isin == "nan")
			isin			= "";

		if (uuid.empty()) {
			uuid = figi;
			uuidType = SecId::kFigi;
		}

		if (uuid.empty()) {
			uuid = isin;
			uuidType = SecId::kISIN;
		}

		if (uuid.empty())
			return false;

		auto ticker			= tickers->getValue(0, ii);
		auto bbTicker		= bbTickers->getValue(0, ii);
		auto parentFigi		= parentFigis->getValue(0, ii);
		auto name			= names->getValue(0, ii);
		auto country		= countries->getValue(0, ii);
		auto marketStatus	= marketStatuses->getValue(0, ii);

		auto spacePos		= ticker.find(' ');

		if (spacePos != std::string::npos)
			ticker			= ticker.substr(0, spacePos);

		bbTicker = Poco::replace(bbTicker, " Equity", "");

		if (sec.uuidStr().empty()) {
			sec.setUuid(uuid);
			sec.uuidType	= uuidType;
		}

		if (sec.cfigiStr().empty())
			sec.setCFIGI(cfigi);

		if (sec.isinStr().empty())
			sec.setISIN(isin);

		if (sec.figiStr().empty())
			sec.setFIGI(figi);

		if (sec.tickerStr().empty())
			sec.setTicker(ticker);

		if (!bbTicker.empty())
			sec.setBBTicker(bbTicker); //bbTicker->getValue(di, ii) + " " + exchCode->getValue(di, ii));

		if (sec.nameStr().empty())
			sec.setName(name); /// name->getValue(di, ii)); 

		if (sec.parentUuidStr().empty())
			sec.setParentUuid(parentFigi);

		if (sec.countryCode3Str().empty())
			sec.setCountryCode3(country);

		sec.isInactive		= marketStatus == "ACTV"  ? false : true;

		return true;
	}

	void QV_Preprocess::getBBClassification(IDataRegistry& dr, const DataCacheInfo& dci, size_t ii, std::string& sector, std::string& group, std::string& industry, std::string& subIndustry) {
		const Security& sec						= m_allSecurities[ii];
		int	bbIndex								= getBBSymbolIndex(sec.uuidStr());

		if (bbIndex < 0)
			return;

		const StringData* sectors				= dr.stringData(nullptr, "bloomberg.sector", true); 
		const StringData* industries			= dr.stringData(nullptr, "bloomberg.industry", true); 
		const StringData* subIndustries 		= dr.stringData(nullptr, "bloomberg.subIndustry", true); 
		const StringData* groups 				= dr.stringData(nullptr, "bloomberg.group", true); 

		if (sector.empty())
			sector								= sectors->getValue(0, bbIndex);

		if (group.empty())
			group								= groups->getValue(0, bbIndex);

		if (industry.empty())
			industry							= industries->getValue(0, bbIndex);

		if (subIndustry.empty())
			subIndustry							= subIndustries->getValue(0, bbIndex);
	}

	void QV_Preprocess::getMSClassification(IDataRegistry& dr, const DataCacheInfo& dci, size_t ii, std::string& sector, std::string& group, std::string& industry, std::string& subIndustry) {
		const Security& sec						= m_allSecurities[ii];

		/// Symbol indices are already taking delay into account ...
		int	msIndex								= getMSSymbolIndex(dci.rt, dr, dci.diEnd, sec.uuidStr());

		if (msIndex < 0)
			return;

		const StringData* reuters_sector 		= dr.stringData(nullptr, "preprocess.reuters_sector", true); 
		const StringData* ms_sector 			= dr.stringData(nullptr, "preprocess.ms_sector", true); 
		const StringData* reuters_industryGroup = dr.stringData(nullptr, "preprocess.reuters_industryGroup", true); 
		const StringData* ms_industryGroup 		= dr.stringData(nullptr, "preprocess.ms_industryGroup", true); 
		const StringData* reuters_industry 		= dr.stringData(nullptr, "preprocess.reuters_industry", true); 
		const StringData* ms_industry 			= dr.stringData(nullptr, "preprocess.ms_industry", true); 
		const StringData* reuters_subIndustry 	= dr.stringData(nullptr, "preprocess.reuters_subIndustry", true); 

		/// Take delay into account ...
		IntDay di								= std::max(dci.diEnd - m_delay, 0); ///(IntDay)ms_sector->rows() - 1;

		/// Try from MS first ...
		if (sector.empty())
			sector								= ms_sector->getValue(di, msIndex);

		/// Then try from Reuters
		if (sector.empty())
			sector								= reuters_sector->getValue(di, msIndex);

		if (group.empty())
			group								= ms_industryGroup->getValue(di, msIndex);

		if (group.empty())
			group								= reuters_industryGroup->getValue(di, msIndex);

		if (industry.empty())
			industry							= ms_industry->getValue(di, msIndex);

		if (industry.empty())
			industry							= reuters_industry->getValue(di, msIndex);

		if (subIndustry.empty())
			subIndustry							= reuters_industry->getValue(di, msIndex);

		if (subIndustry.empty())
			subIndustry							= industry;
	}

	bool QV_Preprocess::fillSecWithMSData(IDataRegistry& dr, IntDay di, int ii, Security& sec) {
		const StringData* tickers 				= dr.stringData(nullptr, "preprocess.ticker", true);
		const StringData* names 				= dr.stringData(nullptr, "preprocess.name", true);
		const StringData* instrumentIds 		= dr.stringData(nullptr, "preprocess.instrumentId", true);
		const StringData* figis 				= dr.stringData(nullptr, "preprocess.figi", true);
		const StringData* cfigis 				= dr.stringData(nullptr, "preprocess.cfigi", true);
		const StringData* isins					= dr.stringData(nullptr, "preprocess.isin", true);
		const StringData* currencies 			= dr.stringData(nullptr, "preprocess.currency", true);
		const StringData* instrumentStatus 		= dr.stringData(nullptr, "preprocess.instrumentStatus", true);
		const StringData* exchangeIds			= dr.stringData(nullptr, "preprocess.exchangeId", true);
		const StringData* countries				= dr.stringData(nullptr, "preprocess.country", true);
		const StringData* permIds				= dr.stringData(nullptr, "preprocess.permId", true);
		const StringData* perfIds				= dr.stringData(nullptr, "preprocess.ms_perfId", true);
		const StringData* issuerPermIds			= dr.stringData(nullptr, "preprocess.issuerPermId", true);

		di										= std::max(di - m_delay, 0);

		auto cfigi								= cfigis->getValue(di, ii);
		auto figi								= figis->getValue(di, ii); 
		auto isin								= isins->getValue(di, ii); 
		auto permId								= permIds->getValue(di, ii);
		auto perfId								= perfIds->getValue(di, ii);
		auto ticker								= tickers->getValue(di, ii);
		auto issuerPermId						= issuerPermIds->getValue(di, ii);
		auto name 								= names->getValue(di, ii);
		auto instrumentId						= instrumentIds->getValue(di, ii);
		auto country							= countries->getValue(di, ii);
		auto currency							= currencies->getValue(di, ii);
		auto exchangeId							= exchangeIds->getValue(di, ii);

		std::string uuid						= cfigi;
		SecId::Type uuidType					= SecId::kCFigi;

		if (uuid.empty()) {
			uuid = figi;
			uuidType = SecId::kFigi;
		}

		if (uuid.empty()) {
			uuid = isin;
			uuidType = SecId::kISIN;
		}

		if (uuid.empty()) {
			uuid = perfId;
			uuidType = SecId::kPerfId;
		}

		if (uuid.empty()) {
			uuid = permId;
			uuidType = SecId::kPermId;
		}

		if (uuid.empty())
			return false;

		if (sec.uuidStr().empty()) {
			sec.setUuid(uuid);
			sec.uuidType = uuidType;
		}

		if (sec.cfigiStr().empty())
			sec.setCFIGI(cfigi);

		if (sec.isinStr().empty())
			sec.setISIN(isin);

		if (sec.figiStr().empty())
			sec.setFIGI(figi);

		sec.setPermId(permId);
		sec.setPerfId(perfId);
		sec.setIssuerPermId(issuerPermId);

		if (sec.parentUuidStr().empty())
			sec.setParentUuid(issuerPermId);

		if (!instrumentId.empty()) {
			sec.setInstrumentId(instrumentId);

			if (ticker.empty())
				ticker = Util::split(instrumentId, ".")[2];
		}

		/// Always replace the tickers ... We give preference to Point-in-Time tickers
		if (!ticker.empty()) {
			sec.setTicker(ticker);

			if (sec.bbTickerStr().empty())
				sec.setBBTicker(ticker);
		}

		/// BB ticker is usually acquired from BB, so we only replace if there is nothing ...
		if (sec.bbTickerStr().empty() && !ticker.empty())
			sec.setBBTicker(ticker);

		if (!name.empty())
			sec.setName(name); 

		if (!exchangeId.empty())
		  	sec.setExchange(exchangeId);

		if (!country.empty())
		  	sec.setCountryCode2(country);

		if (!currency.empty())
		  	sec.setCurrency(currency);

		sec.isInactive = instrumentStatus->getValue((IntDay)instrumentStatus->rows() - 1, ii) == "instrumentStatusActive" ? false : true;

		return true;
	}

	void QV_Preprocess::buildIndices(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities) {
		auto indicesStr			= config().getResolvedString("indices", "");

		if (indicesStr.empty()) {
			CInfo("No indices supplied. Ignoring ...");
			return;
		}

		auto indices			= Util::split(indicesStr, "|");

		for (const auto& index : indices) 
			buildIndex(dr, dci, newSecurities, index);
	}

    void QV_Preprocess::buildSecurityMaster(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		constructLUT(dci.rt, dr, dci.diStart, dci.diEnd);

        SecurityVec newSecurities;

		buildIndices(dr, dci, newSecurities);
        buildSecurities(dr, dci, newSecurities);

        CDebug("Number of new securities found: %z", newSecurities.size());

        size_t numExisting = m_allSecurities.size();
        size_t numNew = newSecurities.size();
        size_t numFiltered = numNew;

        /// Assign the correct indexes to the securities
        for (auto& sec : newSecurities) {
            sec.index = (Security::Index)m_allSecurities.size();
            m_allSecurities.push_back(sec);
        }

        try {
            CDebug("Number of new securities: %z - After filteration: %z [Existing: %z, DataSize: %z]", numNew, numFiltered, numExisting, m_allSecurities.size() * sizeof(Security));

            /// otherwise we create a new data for these
            frame.data			= std::make_shared<SecurityData>(dci.universe, m_allSecurities, dci.def);
            frame.dataOffset	= 0;
            frame.dataSize		= frame.data->dataSize();
            frame.noDataUpdate	= false;

			(const_cast<DataCacheInfo&>(dci)).metadata.set("lastDi", dci.diEnd);
        }
        catch (const std::exception& e) {
            Util::reportException(e);
            Util::exit(1);
        }
    }

    void QV_Preprocess::buildDetails(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities) {
        CDebug("Getting detailed information about the securities!");
    }
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createQV_Preprocess(const pesa::ConfigSection& config) {
	return new pesa::QV_Preprocess((const pesa::ConfigSection&)config);
}

