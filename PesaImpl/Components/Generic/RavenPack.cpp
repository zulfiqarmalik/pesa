/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// RavenPack.cpp
///
/// Created on: 03 Feb 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Framework/Helper/FileDataLoader.h"
#include "./GenericDataLoaderCSV.h"

#include "Poco/String.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class RavenPack : public GenericDataLoaderCSV {
	protected:
		virtual int 			preCacheDayAsync(const RuntimeData* rt, IntDay di, DataDefinition def, DataPtr customMetadata);
		virtual void			preCache(const DataCacheInfo& dci);

	public:
								RavenPack(const ConfigSection& config);
		virtual 				~RavenPack();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);
		virtual void 			buildIndex(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);
	};

	RavenPack::RavenPack(const ConfigSection& config) 
		: GenericDataLoaderCSV(config, "RavenPack") {
	}

	RavenPack::~RavenPack() {
		CTrace("Deleting RavenPack!");
	}

	void RavenPack::initDatasets(IDataRegistry& dr, Datasets& dss) {
		dss.add(Dataset(m_datasetName, {
			DataValue("index", DataType::kString, 0, THIS_DATA_FUNC(RavenPack::buildIndex), DataFrequency::kStatic, 
				DataFlags::kCollectAllBeforeWriting | DataFlags::kUpdatesInOneGo | DataFlags::kAlwaysOverwrite),
		}));

		GenericDataLoader::initDatasets(dr, dss);
	}

	void RavenPack::preCache(const DataCacheInfo& dci) {
		if (!m_uuidMaps.size() || !m_uuidMaps[0].size()) {
			std::string dataId = m_datasetName + ".index";
			StringData* indexData = dci.rt.dataRegistry->stringData(nullptr, dataId);
			ASSERT(indexData, "Unable to load index data: " << dataId);
			SizeMap uuidMap;

			for (size_t ii = 0; ii < indexData->cols(); ii++) {
				std::string dataUuid = indexData->getValue(0, ii);
				uuidMap[dataUuid].push_back(ii);
			}

			if (!m_uuidMaps.size())
				m_uuidMaps.push_back(uuidMap);
			else
				m_uuidMaps[0] = uuidMap;
		}

		GenericDataLoader::preCache(dci);
	}

	int RavenPack::preCacheDayAsync(const RuntimeData* rt, IntDay di, DataDefinition def, DataPtr customMetadata) {
		return GenericDataLoaderCSV::preCacheDayAsyncCSV(rt, di, def, customMetadata);
	}

	void RavenPack::buildIndex(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		std::string indexMapping = config().getRequired("indexMapping");
		indexMapping = Poco::Path(m_baseDir).append(indexMapping).absolute().toString();
		helper::FileDataLoader indexReader(indexMapping);
		StringVec values = indexReader.read();
		StringMap tickerMap, cusipMap, sedolMap, isinMap, nameMap;
		size_t numLines = 0; 

		CDebug("Loading index mappings: %s", indexMapping);

		while (values.size()) {
			auto& entityId		= values[0];
			auto& entityType	= values[1];
			auto& dataType		= values[2];
			auto& dataValue		= values[3];
			auto& rangeStart	= values[4];

			ASSERT(!entityId.empty(), "Invalid entity id!");

			if (entityType == "COMP") {
				if (dataType == "TICKER")
					tickerMap[dataValue] = entityId;
				else if (dataType == "CUSIP")
					cusipMap[dataValue] = entityId;
				else if (dataType == "SEDOL")
					sedolMap[dataValue] = entityId;
				else if (dataType == "ISIN")
					isinMap[dataValue] = entityId;
				else if (dataType == "ENTITY_NAME")
					nameMap[dataValue] = Poco::toLower(entityId);
			}

			values = indexReader.read();
			numLines++;
		}

		CDebug("Index mappings loaded! Total lines: %z", numLines);

		/// Now we find the right value for all the FIGIs
		size_t numSecurities = dci.universe->size(dci.di);
		StringData* entityIndex = new StringData(nullptr, 1, numSecurities, dci.def);
		size_t numInvalid = 0;
		size_t numISINMapped = 0, numCusipMapped = 0, numSedolMapped = 0, numTickerMapped = 0, numNameMapped = 0;

		for (size_t ii = 0; ii < numSecurities; ii++) {
			Security sec = dci.universe->security(dci.di, (Security::Index)ii);
			if (sec.isValid()) {
				auto isin = sec.isinStr();
				//auto cusip = sec.cusipStr();
				//auto sedol = sec.sedolStr();
				auto name = Poco::toLower(sec.nameStr());
				auto ticker = sec.tickerStr();

				if (!isin.empty() && isinMap.find(isin) != isinMap.end()) {
					entityIndex->setValue(0, ii, isinMap[isin]);
					numISINMapped++;
				}
				//else if (!cusip.empty() && cusipMap.find(cusip) != cusipMap.end()) {
				//	entityIndex->setValue(0, ii, cusipMap[cusip]);
				//	numCusipMapped++;
				//}
				//else if (!sedol.empty() && sedolMap.find(sedol) != sedolMap.end()) {
				//	entityIndex->setValue(0, ii, sedolMap[sedol]);
				//	numSedolMapped++;
				//}
				else if (!ticker.empty() && tickerMap.find(ticker) != tickerMap.end()) {
					entityIndex->setValue(0, ii, tickerMap[ticker]);
					numTickerMapped++;
				}
				else if (!name.empty() && nameMap.find(name) != nameMap.end()) {
					entityIndex->setValue(0, ii, nameMap[name]);
					numNameMapped++;
				}
				else {
					numInvalid++;
				}
			}
		}

		CInfo("Updated data: [%-8u]-[Found: %5z / %5z - Invalid: %5z] - %s [isin: %z, cusip: %z, sedol, %z, ticker: %z, name: %z]", 
			dci.rt.dates[dci.di], (numSecurities - numInvalid), numSecurities, numInvalid, dci.def.toString(dci.ds),
			numISINMapped, numCusipMapped, numSedolMapped, numTickerMapped, numNameMapped);

		frame.data = DataPtr(entityIndex);
		frame.dataSize = entityIndex->dataSize();
		frame.dataOffset = 0;
	}

	void RavenPack::build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		preCache(dci);
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createRavenPack(const pesa::ConfigSection& config) {
	return new pesa::RavenPack((const pesa::ConfigSection&)config);
}

