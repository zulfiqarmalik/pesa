/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// RemapData.cpp
///
/// Created on: 26 Nov 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Poco/String.h"
#include "Framework/Helper/FileDataLoader.h"
#include "PesaImpl/Components/SP/SpGeneric.h"
#include "PesaImpl/Components/SP/SpRest.h"
#include "PesaImpl/Components/Quandl/Quandl_JsonParser.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// RemapData: This module remaps the data from one security master to another
	////////////////////////////////////////////////////////////////////////////
	class RemapData : public IDataLoader {
	private:
		typedef std::unordered_map<std::string, DataPtr>	DataPtrMap;
		typedef std::vector<Security::Index>				SecIndexVec;

		SpRest::StringMap		m_mappings;					/// Mappings from the original name to our custom name
		SpRest::StringMap		m_rmappings;				/// Reverse mappings

		bool					m_fromString = false;		/// Whether we're dealing with multi type of data. The source data is in string
		bool					m_universalToDaily = false;	/// Map from universal to daily
		DataPtrMap				m_udata;					/// Data that has been mapped from universal to daily

		std::string				m_datasetName;				/// The name of the dataset
		//std::string				m_srcUniverseId;			/// The source universe id
		StringVec				m_srcUniverseIds;			/// The source universe ids
		bool					m_isSrcUniverseDataSource = false; /// Are we building source universe from a data source
		std::string				m_dstUniverseId;			/// The destination universe id

		std::unordered_map<std::string, SecIndexVec> m_dataMap;

		void					copyValue(DataPtr srcData, DataPtr dstData, const std::string& searchId, IntDay di, size_t ii, size_t& numCopied);

	public:
								RemapData(const ConfigSection& config);
		virtual 				~RemapData();

		virtual void 			buildInstrument(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
	};

	RemapData::RemapData(const ConfigSection& config) : IDataLoader(config, "Remap") {
		m_datasetName			= config.getRequired("datasetName");
		auto srcUniverseId		= config.getResolvedRequiredString("srcUniverse");
		m_isSrcUniverseDataSource= srcUniverseId.find(".") != std::string::npos ? true : false;
		m_srcUniverseIds		= Util::split(srcUniverseId, "|");
		m_dstUniverseId			= config.getResolvedRequiredString("dstUniverse");
		m_fromString			= config.getBool("fromString", m_fromString);
		m_universalToDaily		= config.getBool("universalToDaily", m_universalToDaily);

		std::string mappings = config.get("mappings");

		if (!mappings.empty()) {
			StringVec mappingsVec = Util::split(mappings, ",");
			for (auto& mapping : mappingsVec) {
				StringVec parts = Util::split(mapping, "=");
				ASSERT(parts.size() == 2, "Invalid mapping: " << mapping);
				m_mappings[parts[0]] = parts[1];
				m_rmappings[parts[1]] = parts[0];
			}
		}
	}

	RemapData::~RemapData() {
		CTrace("Deleting RemapData!");
	}

	void RemapData::initDatasets(IDataRegistry& dr, Datasets& dss) {
		Dataset ds(m_datasetName);
		StringVec datasets(Util::split(config().getRequired("datasets"), ","));
		//bool isInstrument = m_srcUniverseId.find(".") != std::string::npos ? true : false;
		DataValue::Callback callback = !m_isSrcUniverseDataSource ? THIS_DATA_FUNC(RemapData::build) : THIS_DATA_FUNC(RemapData::buildInstrument);

		for (std::string dataset : datasets) {
			DataFrequency::Type		frequency;
			DataType::Type			type;
			std::string				dataName;

			DataDefinition::parseDefString(dataset, dataName, type, frequency);

			int flags = frequency == DataFrequency::kStatic ? DataFlags::kUpdatesInOneGo | DataFlags::kAlwaysOverwrite : 0;

			DataValue dv(dataName, type, DataType::size(type), callback, frequency, flags);
			ds.add(dv);
			//else {
			//	ASSERT(false, "Unsupported dataset: " << dataset << " - Only daily and universal datasets are supported for the time being!");
			//}
		}

		dss.add(ds);
	}

	void RemapData::build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		std::string dataName	= dci.def.name;

		if (m_universalToDaily) {
			auto uiter			= m_udata.find(dataName);

			if (uiter != m_udata.end()) {
				frame.data		= uiter->second;
				frame.dataSize	= frame.data->dataSize();
				frame.dataOffset= 0;

				return;
			}
		}

		bool global				= config().getBool("global", false);
		std::string srcDataId	= m_rmappings[dataName]; 

		ASSERT(m_srcUniverseIds.size() == 1, "Cannot copy from multiple source universes! That feature is only available when using a data source mapping!");

		IUniverse* srcUniverse	= dr.getUniverse(m_srcUniverseIds[0]);
		ASSERT(srcUniverse, "Unable to load universe: " << m_srcUniverseIds[0]);

		IUniverse* dstUniverse	= dr.getUniverse(m_dstUniverseId);
		ASSERT(dstUniverse, "Unable to load universe: " << m_dstUniverseId);

		size_t numDstCols		= dstUniverse->size(dci.diEnd);
		DataPtr srcData			= dr.getDataPtr(dci.def.isStatic() || m_universalToDaily || global ? nullptr : srcUniverse, srcDataId, true);
		ASSERT(srcData, "Unable to load data: " << srcDataId);

		DataPtr dstData			= dr.createDataBuffer(dstUniverse, dci.def);;
		size_t numSrcCols		= srcData->cols();
		IntDay di				= dci.def.isStatic() ? 0 : dci.di;

		dstData->ensureSize(1, numDstCols);

		size_t numCopied		= 0;

		for (size_t ii = 0; ii < numDstCols; ii++) {
			Security dstSec(dstUniverse->security(di, (Security::Index)ii));
			const Security* srcSec = srcUniverse->mapUuid(dstSec.uuidStr()); 

			ASSERT(srcSec, "Unable to find dst security in src universe. Note that src universe must be a super-set of dst universe. Security: " << dstSec.debugString());
			ASSERT(srcSec->uuidStr() == dstSec.uuidStr(), "Src and Dst securities do not match up. Src UUID: " << srcSec->uuidStr() << " - Dst UUID: " << dstSec.uuidStr());
			ASSERT(srcSec->tickerStr() == dstSec.tickerStr(), "Src and Dst securities do not match up. Src Ticker: " << srcSec->tickerStr() << " - Dst Ticker: " << dstSec.tickerStr());

			//CDebug("Copying: %s [%u] => %s [%u]", srcSec->tickerStr(), srcSec->index, dstSec.tickerStr(), dstSec.index);

			numCopied++;

			IntDay diSrc = std::min(di, (IntDay)(srcData->rows() - 1));
			size_t iiSrc = (size_t)srcSec->index;

			if (!m_fromString)
				dstData->copyElement(srcData.get(), diSrc, iiSrc, 0, ii);
			else {
				auto strValue = srcData->getString(diSrc, iiSrc, 0);
				dstData->setString(0, ii, 0, strValue);
			}
		}

		frame.data				= dstData;
		frame.dataSize			= dstData->dataSize();
		frame.dataOffset		= 0;

		if (m_universalToDaily) 
			m_udata[dataName]	= frame.data;

		CInfo("Updated data: [%-8u]-[Copied: %5z / %5z] - %s", dci.rt.dates[dci.di], numCopied, numSrcCols, dci.def.toString(dci.ds));
	}

	void RemapData::copyValue(DataPtr srcData, DataPtr dstData, const std::string& searchId, IntDay di, size_t ii, size_t& numCopied) {
		/// If there is no searchId and 
		if (searchId.empty() || dstData->isValid(0, ii))
			return;

		auto srcIter = m_dataMap.find(searchId);

		if (srcIter == m_dataMap.end())
			return;

		//ASSERT(srcIter != m_dataMap.end(), "Unable to find dst security in src universe. Note that src universe must be a super-set of dst universe. Security: " << dstSec.debugString());

		//numCopied++;

		const SecIndexVec& iiSrcs = srcIter->second;
		size_t iiSrcIter = 0;
		IntDay diSrc = std::min(di, (IntDay)(srcData->rows() - 1));

		for (size_t iiSrcIter = 0; iiSrcIter < iiSrcs.size() && !dstData->isValid(0, ii); iiSrcIter++) {
			size_t iiSrc =  iiSrcs[iiSrcIter]; ///(size_t)srcIter->second;

			numCopied++;

			if (!m_fromString)
				dstData->copyElement(srcData.get(), diSrc, iiSrc, 0, ii);
			else {
				auto strValue = srcData->getString(diSrc, iiSrc, 0);
				dstData->setString(0, ii, 0, strValue);
			}
		}

	}

	void RemapData::buildInstrument(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		std::string dataName	= dci.def.name;

		if (m_universalToDaily) {
			auto uiter			= m_udata.find(dataName);

			if (uiter != m_udata.end()) {
				frame.data		= uiter->second;
				frame.dataSize	= frame.data->dataSize();
				frame.dataOffset= 0;

				return;
			}
		}

		std::string srcDataId	= m_rmappings[dataName];

		IUniverse* dstUniverse	= dr.getUniverse(m_dstUniverseId);
		ASSERT(dstUniverse, "Unable to load universe: " << m_dstUniverseId);

		size_t numDstCols		= dstUniverse->size(dci.diEnd);
		DataPtr srcData			= dr.getDataPtr(nullptr, srcDataId, true);
		ASSERT(srcData, "Unable to load data: " << srcDataId);

		DataPtr dstData			= dr.createDataBuffer(dstUniverse, dci.def);/// srcData->createInstance();
		size_t numSrcCols		= srcData->cols();
		IntDay di				= dci.def.isStatic() ? 0 : dci.di;

		//ASSERT(numSrcCols == mapData->cols(), "Data mismatch between index " << m_srcUniverseId << " [Cols: " << mapData->cols() << "] and " << srcDataId << "[Cols: " << numSrcCols << "]");

		if (m_dataMap.empty()) {
			for (const auto& srcUniverseId : m_srcUniverseIds) {
				const StringData* mapData = dr.stringData(nullptr, srcUniverseId);
				ASSERT(mapData, "Unable to load src map data: " << srcUniverseId);

				for (size_t ii = 0; ii < mapData->cols(); ii++) {
					const auto& srcId = mapData->getValue(0, ii);
					if (!srcId.empty())
						m_dataMap[srcId].push_back((Security::Index)ii);
				}
			}
		}

		dstData->ensureSize(1, numDstCols);

		size_t numCopied		= 0;

		for (size_t ii = 0; ii < numDstCols; ii++) {
			Security dstSec(dstUniverse->security(di, (Security::Index)ii));

			if (dstSec.tickerStr() == "MSFT" || dstSec.tickerStr() == "AAPL")
				int a = 10;

			copyValue(srcData, dstData, dstSec.uuidStr(), di, ii, numCopied);
			copyValue(srcData, dstData, dstSec.cfigiStr(), di, ii, numCopied);
			copyValue(srcData, dstData, dstSec.figiStr(), di, ii, numCopied);
			copyValue(srcData, dstData, dstSec.isinStr(), di, ii, numCopied);
			copyValue(srcData, dstData, dstSec.permIdStr(), di, ii, numCopied);
			copyValue(srcData, dstData, dstSec.perfIdStr(), di, ii, numCopied);
			copyValue(srcData, dstData, dstSec.instrumentId(), di, ii, numCopied);
			copyValue(srcData, dstData, dstSec.tickerStr(), di, ii, numCopied);

			//auto srcIter = m_dataMap.find(dstSec.instrumentId());

			//if (srcIter == m_dataMap.end())
			//	srcIter = m_dataMap.find(dstSec.uuidStr());

			//ASSERT(srcIter != m_dataMap.end(), "Unable to find dst security in src universe. Note that src universe must be a super-set of dst universe. Security: " << dstSec.debugString());

			//numCopied++;

			//IntDay diSrc = std::min(di, (IntDay)(srcData->rows() - 1));
			//size_t iiSrc = (size_t)srcIter->second;

			//if (!m_fromString)
			//	dstData->copyElement(srcData.get(), diSrc, iiSrc, 0, ii);
			//else {
			//	auto strValue = srcData->getString(diSrc, iiSrc, 0);
			//	dstData->setString(0, ii, 0, strValue);
			//}
		}

		frame.data				= dstData;
		frame.dataSize			= dstData->dataSize();
		frame.dataOffset		= 0;

		if (m_universalToDaily)
			m_udata[dataName]	= frame.data;

		CInfo("Updated data: [%-8u]-[Copied: %5z / %5z] - %s", dci.rt.dates[dci.di], numCopied, numSrcCols, dci.def.toString(dci.ds));
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createRemapData(const pesa::ConfigSection& config) {
	return new pesa::RemapData((const pesa::ConfigSection&)config);
}

