/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// SplitData.cpp
///
/// Created on: 20 May 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Poco/String.h"
#include "Framework/Helper/FileDataLoader.h"
#include "PesaImpl/Components/SP/SpGeneric.h"
#include "PesaImpl/Components/SP/SpRest.h"
#include "PesaImpl/Components/Quandl/Quandl_JsonParser.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// SplitData: This module remaps the data from one security master to another
	////////////////////////////////////////////////////////////////////////////
	class SplitData : public IDataLoader {
	public:
								SplitData(const ConfigSection& config);
		virtual 				~SplitData();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
	};

	SplitData::SplitData(const ConfigSection& config) : IDataLoader(config, "SplitData") {
	}

	SplitData::~SplitData() {
		CTrace("Deleting SplitData!");
	}

	void SplitData::initDatasets(IDataRegistry& dr, Datasets& dss) {
		auto datasetName	= config().getRequired("datasetName");
		auto dataName		= config().getRequired("dataName");
		auto srcDataName	= config().getRequired("srcData");
		auto srcData		= dr.stringData(dr.getSecurityMaster(), srcDataName);

		if (!srcData)
			return;

		auto srcDef			= srcData->def();

		dss.add(Dataset(datasetName, {
			DataValue(dataName, DataType::kIndexedString, 0, nullptr, srcDef.frequency, srcDef.flags),
		}));
	}

	void SplitData::build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		auto srcDataName	= config().getRequired("srcData");
		auto defDataName	= config().getRequired("defaultData");
		auto separator		= config().getRequired("separator");
		auto valueSeparator = config().getString("valueSeparator", "");
		auto index			= (size_t)config().getInt("index", 0);
		auto srcData		= dr.stringData(dci.universe, srcDataName);
		auto defData		= dr.stringData(dci.universe, defDataName);
		StringData* dstData = new ShortIndexedStringData(nullptr, 1, 1, dci.def);

		dstData->setMetadata(dci.customMetadata);
		dstData->ensureSize(1, srcData->cols());

		for (size_t ii = 0; ii < srcData->cols(); ii++) {
			std::string dstValue;
			auto srcValue	= srcData->getValue(dci.di, ii);
			auto parts		= Util::split(srcValue, separator);

			if (parts.size() > index)
				dstValue	= parts[index];
			else
				dstValue	= defData->getValue(std::min(dci.di, (IntDay)defData->rows() - 1), ii);

			if (!valueSeparator.empty() && !dstValue.empty()) {
				auto dparts	= Util::split(dstValue, valueSeparator);
				dstValue	= dparts[0];
			}

			dstData->setValue(0, ii, dstValue);
		}

		frame.data			= DataPtr(dstData);
		frame.dataSize		= dstData->dataSize();
		frame.dataOffset	= 0;
		frame.noDataUpdate	= false;

		CInfo("Updated data: [%-8u]-[Count: %z] - %s", dci.rt.dates[dci.di], dstData->cols(), dci.def.toString(dci.ds));
	}

	////////////////////////////////////////////////////////////////////////////
	/// SplitIndexedData: This module remaps the data that is indexed on another
	/// string data, into an independent one
	////////////////////////////////////////////////////////////////////////////
	class SplitIndexedData : public IDataLoader {
	private:
		int						m_index = -1;

	public:
								SplitIndexedData(const ConfigSection& config);
		virtual 				~SplitIndexedData();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
	};

	SplitIndexedData::SplitIndexedData(const ConfigSection& config) : IDataLoader(config, "SplitIndexedData") {
	}

	SplitIndexedData::~SplitIndexedData() {
		CTrace("Deleting SplitData!");
	}

	void SplitIndexedData::initDatasets(IDataRegistry& dr, Datasets& dss) {
		auto datasetName	= config().getRequired("datasetName");
		auto dataName		= config().getRequired("dataName");
		auto indexDataName	= config().getRequired("indexData");
		auto srcDataName	= config().getRequired("srcData");
		auto indexData		= dr.stringData(dr.getSecurityMaster(), indexDataName); 
		auto srcData		= dr.getDataPtr(dr.getSecurityMaster(), srcDataName);

		if (!srcData || !indexData)
			return;

		auto srcDef = srcData->def();

		dss.add(Dataset(datasetName, {
			DataValue(dataName, DataType::kIndexedString, 0, nullptr, srcDef.frequency, srcDef.flags),
		}));
	}

	void SplitIndexedData::build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) { 
		std::string dataName	= dci.def.toString(dci.ds);

		if (m_index < 0) {
			auto indexDataName	= config().getRequired("indexData");
			auto indexData		= dr.stringData(dr.getSecurityMaster(), indexDataName); 

			ASSERT(indexData, "Unable to load index data: " << indexDataName);

			for (size_t ii = 0; ii < indexData->cols() && m_index < 0; ii++) {
				if (indexData->getValue(0, ii) == dataName)
					m_index		= (int)ii;
			}
		}

		auto srcDataName		= config().getRequired("srcData");
		auto srcData			= dr.getDataPtr(dr.getSecurityMaster(), srcDataName);

		ASSERT(srcData, "Unable to load source data: " << srcDataName);

		Data* dstData			= srcData->createInstance();

		dstData->setMetadata(dci.customMetadata);
		dstData->ensureSize(1, 1);

		dstData->copyElement(srcData.get(), 0, (IntDay)m_index, 0, 0);

		frame.data				= DataPtr(dstData);
		frame.dataSize			= dstData->dataSize();
		frame.dataOffset		= 0;
		frame.noDataUpdate		= false;

		CInfo("Updated data: [%-8u]-[Index: %d] - %s", dci.rt.dates[dci.di], m_index, dataName);
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createSplitData(const pesa::ConfigSection& config) {
	return new pesa::SplitData((const pesa::ConfigSection&)config);
}

EXPORT_FUNCTION pesa::IComponent* createSplitIndexedData(const pesa::ConfigSection& config) {
	return new pesa::SplitIndexedData((const pesa::ConfigSection&)config);
}

