/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// StaticData.cpp
///
/// Created on: 29 May 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Poco/String.h"
#include "Framework/Helper/FileDataLoader.h"
#include "PesaImpl/Components/SP/SpGeneric.h"
#include "PesaImpl/Components/SP/SpRest.h"
#include "PesaImpl/Components/Quandl/Quandl_JsonParser.h"
#include "PesaImpl/Pipeline/DataRegistry/DataRegistry.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// StaticData: This module remaps the data from one security master to another
	////////////////////////////////////////////////////////////////////////////
	class StaticData : public IDataLoader {
	private:
		DataPtr					m_data;					/// The data

	public:
								StaticData(const ConfigSection& config);
		virtual 				~StaticData();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
	};

	StaticData::StaticData(const ConfigSection& config) : IDataLoader(config, "StaticData") {
	}

	StaticData::~StaticData() {
		CTrace("Deleting StaticData!");
	}

	void StaticData::initDatasets(IDataRegistry& dr, Datasets& dss) {
		auto datasetName	= config().getRequired("datasetName"); 
		auto dataset		= config().getRequired("datasets");

		DataFrequency::Type frequency;
		DataType::Type type;
		std::string dataName;
		bool addToRequiredList = true;

		DataDefinition::parseDefString(dataset, dataName, type, frequency);

		dss.add(Dataset(datasetName, {
			DataValue(dataName, type, DataType::size(type), nullptr, frequency, frequency == DataFrequency::kStatic ? (DataFlags::kUpdatesInOneGo | DataFlags::kAlwaysOverwrite) : 0),
		}));
	}

	void StaticData::build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		if (!m_data) {
			m_data			= DataRegistry::createDataBuffer(dci.universe, dci.def, nullptr);

			auto sep		= config().getString("separator", ",");
			auto valuesStr	= config().getResolvedRequiredString("values");
			auto parts		= Util::split(valuesStr, sep);
			auto prefix		= config().getString("prefix", "");
			auto suffix		= config().getString("suffix", "");

			m_data->ensureSize(1, parts.size(), false);

			for (size_t ii = 0; ii < parts.size(); ii++) {
				auto value	= parentConfig()->defs().resolve(parts[ii], (unsigned int)dci.rt.dates[dci.di]);
				m_data->setString(0, ii, 0, prefix + value + suffix);
			}
		}

		Data* data			= m_data->clone(dci.universe, false);

		frame.data			= DataPtr(data);
		frame.dataSize		= data->dataSize();
		frame.dataOffset	= 0;
		frame.noDataUpdate	= false;

		CInfo("Updated data: [%-8u]-[Count: %z] - %s", dci.rt.dates[dci.di], data->cols(), dci.def.toString(dci.ds));
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createStaticData(const pesa::ConfigSection& config) {
	return new pesa::StaticData((const pesa::ConfigSection&)config);
}

