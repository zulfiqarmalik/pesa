/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Thinknum.cpp
///
/// Created on: 03 Feb 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Framework/Helper/FileDataLoader.h"
#include "./GenericDataLoader.h"
#include "Helper/JSON/gason.hpp"

#include "Poco/String.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class Thinknum : public GenericDataLoader {
	protected:
		typedef std::unordered_map<std::string, size_t> MySizeMap;
		typedef std::map<IntDate, MySizeMap> UUIDMap;
		virtual int 			preCacheDayAsync(const RuntimeData* rt, IntDay di, DataDefinition def, DataPtr customMetadata);
		virtual void			preCacheDay(const DataCacheInfo& dci, IntDay di);
		virtual void			clearCache(IntDate date);

		virtual int				preCacheDayAsyncJSON(const RuntimeData* rt, IntDay di, DataDefinition def, std::string& jsonStr, DataPtr customMetadata);

		IntDate					m_tickerHistoryStartDate = 0;
		std::string				m_dataName;
		UUIDMap					m_uuidMaps;

	public:
								Thinknum(const ConfigSection& config);
		virtual 				~Thinknum();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);
	};

	Thinknum::Thinknum(const ConfigSection& config) 
		: GenericDataLoader(config, "Thinknum") {
		m_dataName = config.getRequired("dataName");
		std::string tickerHistoryStartDate = config.getString("tickerHistoryStart", "");
		if (!tickerHistoryStartDate.empty())
			m_tickerHistoryStartDate = Util::cast<IntDate>(tickerHistoryStartDate);
	}

	Thinknum::~Thinknum() {
		CTrace("Deleting Thinknum!");
	}

	void Thinknum::initDatasets(IDataRegistry& dr, Datasets& dss) {
		GenericDataLoader::initDatasets(dr, dss);

		for (auto& ds : dss.datasets) {
			for (auto& dv : ds.values) {
				std::string name = m_dataName + "_" + std::string(dv.definition.name);
				dv.definition.setName(name.c_str());
			}
		}
	}

	void Thinknum::preCacheDay(const DataCacheInfo& dci, IntDay di) {
		AUTO_LOCK(m_dataMutex);

		auto* rt = &dci.rt;
		IntDate date = rt->dates[di];
		auto uuidMapDate = std::max(date, m_tickerHistoryStartDate);
		auto iter = m_uuidMaps.find(uuidMapDate);

		if (iter == m_uuidMaps.end()) {
			CDebug("Loading tickers on date: %u", uuidMapDate);

			StringData* tickers = rt->dataRegistry->stringData(rt->dataRegistry->getSecurityMaster(), "secData.tickers");
			ASSERT(tickers, "Unable to load ticker data: secData.tickers");

			for (size_t ii = 0; ii < tickers->cols(); ii++) {
				std::string ticker = tickers->getValue(0, ii);
				if (!ticker.empty()) {
					ticker = Poco::toLower(Util::split(ticker, " ")[0]);
					m_uuidMaps[uuidMapDate][ticker] = ii;
				}
			}
		}

		GenericDataLoader::preCacheDay(dci, di);
	}

	void Thinknum::clearCache(IntDate date) {
		auto iter = m_uuidMaps.find(date);
		if (iter != m_uuidMaps.end())
			m_uuidMaps.erase(date);
	}

	int Thinknum::preCacheDayAsyncJSON(const RuntimeData* rt, IntDay di, DataDefinition def, std::string& jsonStr, DataPtr customMetadata) {
		bool isHistorical = false;
		std::string filename = getFilename(*rt, di, isHistorical);

		if (filename.empty())
			return -1;

		CTrace("Loading JSON filename: %s", filename);
		jsonStr = Util::readEntireTextFile(filename);

		return 0;
	}

	int Thinknum::preCacheDayAsync(const RuntimeData* rt, IntDay di, DataDefinition def, DataPtr customMetadata) {
		std::string jsonStr;
		IntDate date = rt->dates[di];
		int errCode = preCacheDayAsyncJSON(rt, di, def, jsonStr, customMetadata);
		std::string dataName = std::string(def.name);

		/// If there was an error ...
		if (errCode) 
			return errCode;


		MySizeMap* uuidMap = nullptr;

		{
			/// OK, have we cached the data already. If we have then there's nothing to do over here ...
			AUTO_LOCK(m_dataMutex);
			auto key = makeKey(dataName, date);
			auto dataIter = m_dataCache.find(key);
			if (dataIter != m_dataCache.end())
				return 0;

			auto uuidMapDate = std::max(date, m_tickerHistoryStartDate);
			auto iter = m_uuidMaps.find(uuidMapDate);

			if (iter != m_uuidMaps.end())
				uuidMap = &iter->second;
		}

		ASSERT(uuidMap, "Invalid UUID Map on date: " << date);

		/// Now we get the data ...
		size_t jsonDataLen = jsonStr.length() + 1;
		char* jsonData = new char[jsonDataLen];
		memcpy(jsonData, jsonStr.c_str(), jsonStr.length());
		jsonData[jsonDataLen - 1] = '\0';

		char* startPtr = jsonData;
		char* endPtr = nullptr;
		gason::JsonAllocator jsonAllocator;
		gason::JsonValue jsonResult;

		gason::JsonParseStatus jsonStatus = gason::Json::parse(startPtr, &endPtr, &jsonResult, jsonAllocator);

		ASSERT(jsonStatus == gason::JSON_PARSE_OK, "Unable to parse json at date: " << date);
		auto fieldsList = jsonResult.child("items").child("fields");
		auto fieldsIter = gason::begin(fieldsList);

		if (!fieldsIter.isValid()) {
			return 0;
		}

		//ASSERT(fieldsIter.isValid(), "Invalid iterator for items.fields! Unknown JSON on date: " << date);
		int idIndex = -1;
		size_t index = 0;
		IntVec dataIndexes(m_datasetNames.size());
		FloatMatrixDataPtrVec fdata(dataIndexes.size());
		auto secMaster = rt->dataRegistry->getSecurityMaster();
		auto numSecurities = secMaster->size(di);

		std::fill_n(&dataIndexes[0], dataIndexes.size(), -1);

		for (; fieldsIter != gason::end(fieldsList); fieldsIter++, index++) { 
			std::string fieldId = fieldsIter->value.child("id").toString();
			if (idIndex < 0 && fieldId == std::string("dataset__entity__entity_ticker__ticker__ticker"))
				idIndex = (int)index;
			else {
				for (size_t i = 0; i < m_datasetNames.size(); i++) {
					const auto& datasetName = m_datasetNames[i];
					int& dataIndex = dataIndexes[i];
					if (dataIndex < 0 && datasetName == fieldId) {
						dataIndex = (int)index;
						DataDefinition ndef = def;
						ndef.setName(datasetName);
						fdata[i] = FloatMatrixDataPtr(new FloatMatrixData(nullptr, ndef, 1, numSecurities));
						std::string key = makeKey(datasetName, date, m_dataName + "_");
						{
							AUTO_LOCK(m_dataMutex);
							m_dataCache[key] = fdata[i];
						}
					}
				}
			}
		}

		auto rowsData = jsonResult.child("items").child("rows");
		size_t numUnmapped = 0;
		size_t numMapped = 0;

		for (auto rowsIter = gason::begin(rowsData); rowsIter != gason::end(rowsData); rowsIter++) {
			ASSERT(rowsIter->value.isArray(), "Invalid row iterator. Expecting array!");

			std::string orgTicker = rowsIter->value.at((size_t)idIndex).toString();
			auto parts = Util::split(orgTicker, ":");
			if (parts.size() != 2) {
				CInfo("[%u] Ignoring ticker: %s", date, orgTicker);
				continue;
			}

			auto ticker = parts[1];
			auto tickerIter = uuidMap->find(ticker);

			if (tickerIter != uuidMap->end()) {
				size_t ii = tickerIter->second;
				numMapped++;

				for (size_t i = 0; i < dataIndexes.size(); i++) {
					FloatMatrixDataPtr dataPtr = fdata[i];
					const auto& datasetName = m_datasetNames[i];
					int dataIndex = dataIndexes[i];
					ASSERT(dataIndex >= 0, "Invalid data index for dataset: " << datasetName << ". Date: " << date);
					auto jvalue = rowsIter->value.at((size_t)dataIndex);
					auto tag = jvalue.getTag();
					float fvalue = std::nanf("");
					if (tag == gason::JSON_STRING)
						fvalue = Util::cast<float>(jvalue.toString());
					else if (tag == gason::JSON_NUMBER)
						fvalue = (float)jvalue.toNumber();
					else {
						ASSERT(false, "Invalid json value. Tag: " << tag << " - Value: " << jvalue.toString());
					}
					dataPtr->setValue(0, ii, fvalue);
				}
			}
			else {
				CTrace("Ticker mapping failed for ticker: [Date: %u] - %s ", date, orgTicker);
				numUnmapped++;
			}
		}

		delete[] jsonData;

		CInfo("Updated data: [%-8u]-[Found: %5z / %5z - Unmapped: %5z] - %s", date, numMapped, numSecurities, numUnmapped, dataName);
		return 0;
	}

	void Thinknum::build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		preCache(dci);
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createThinknum(const pesa::ConfigSection& config) {
	return new pesa::Thinknum((const pesa::ConfigSection&)config);
}

