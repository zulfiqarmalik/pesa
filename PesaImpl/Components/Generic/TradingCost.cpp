/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// TradingCost.cpp
///
/// Created on: 13 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class TradingCost : public IDataLoader {
	private:
		float					m_constantTradingCost = 0.0f;		/// Constant TradingCost value (if any)
		bool					m_isConstant = false;				/// Whether constant TradingCost has been specified

		virtual void 			buildConstant(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);

	public:
								TradingCost(const ConfigSection& config);
		virtual 				~TradingCost();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);

		////////////////////////////////////////////////////////////////////////////
		/// IUniverse overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			combine(FloatMatrixData& thisAlpha, IUniversePtr other, FloatMatrixData& otherAlpha) {}
	};

	TradingCost::TradingCost(const ConfigSection& config) : IDataLoader(config, "TradingCost") {
		std::string tradingCost;

		if (config.get("tradingCost", tradingCost)) {
			m_constantTradingCost = Util::percent(tradingCost);
			m_isConstant = true;
			trace("Got constant trading cost: %0.2hf", m_constantTradingCost);
			ASSERT(m_constantTradingCost > 0.0f, "Cannot have 0 or negative TradingCost");
		}
	}

	TradingCost::~TradingCost() {
		CTrace("Deleting TradingCost!");
	}

	void TradingCost::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		if (m_isConstant) {
			datasets.add(Dataset("cost", {
				DataValue("trade", DataType::kFloatConstant, sizeof(float), THIS_DATA_FUNC(TradingCost::buildConstant), DataFrequency::kStatic, 
					DataFlags::kUpdatesInOneGo | DataFlags::kAlwaysOverwrite),
			}));
		}
	}

	void TradingCost::buildConstant(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		ASSERT(m_isConstant, "Constant function called for non-constant data!");
		frame.data = std::make_shared<ConstantFloat>(m_constantTradingCost, dci.def);
		frame.dataSize = frame.data->dataSize();
		frame.dataOffset = 0;

		//dci.metadata.clear();
		//dci.metadata.set("isConstant", 1);
		//dci.metadata.set("value", m_constantSlippage);
	}

	void TradingCost::build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createTradingCost(const pesa::ConfigSection& config) {
	return new pesa::TradingCost((const pesa::ConfigSection&)config);
}

