/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// MS_DeadInstruments.cpp
///
/// Created on: 23 Oct 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Poco/Timestamp.h"
#include "../MS_Util.h"

#include <stdio.h>
#include <ctype.h>

namespace pesa {
    struct InstInfo {
        std::string             instrument;
        std::string             perfId;
        std::string             ticker;
        int                     ipoDate;
        int                     delistedDate;
        int                     tradingStatus;
        int                     lastUpdated;
    };
	////////////////////////////////////////////////////////////////////////////
	/// MorningStar Security Master
	////////////////////////////////////////////////////////////////////////////
	class MS_DeadInstruments : public IDataLoader {
	private:
	    typedef std::map<DataPtr> DataPtrMap;
	    typedef std::vector<InstInfo> InstInfoVec;

	    std::string             m_datasetName;          /// The name of the data that we're using
        InstInfoVec             m_allInstruments;       /// All the instruments that we have

	public:
								MS_DeadInstruments(const ConfigSection& config);
		virtual 				~MS_DeadInstruments();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);

		virtual void 			preDataBuild(IDataRegistry& dr, const DataCacheInfo& dci);
	};

	MS_DeadInstruments::MS_DeadInstruments(const ConfigSection& config) : IDataLoader(config, "MS_DeadInstruments") {
        m_datasetName = config.getResolvedRequiredString("datasetName");
	}

	MS_DeadInstruments::~MS_DeadInstruments() {
		CTrace("Deleting MS_DeadInstruments");
	}

	void MS_DeadInstruments::initDatasets(pesa::IDataRegistry &dr, pesa::Datasets &datasets) {

        datasets.add(Dataset(m_datasetName, {
            DataValue("instrument", DataType::kIndexedString, DataType::size(DataType::kIndexedString), nullptr, DataFrequency::kStatic, 0),
            DataValue("perfId", DataType::kIndexedString, DataType::size(DataType::kIndexedString), nullptr, DataFrequency::kStatic, 0),
            DataValue("ticker", DataType::kIndexedString, DataType::size(DataType::kIndexedString), nullptr, DataFrequency::kStatic, 0),
            DataValue("ipoDate", DataType::kInt, DataType::size(DataType::kInt), nullptr, DataFrequency::kStatic, 0),
            DataValue("delistedDate", DataType::kInt, DataType::size(DataType::kInt), nullptr, DataFrequency::kStatic, 0),
            DataValue("tradingStatus", DataType::kInt, DataType::size(DataType::kInt), nullptr, DataFrequency::kStatic, 0),
            DataValue("lastUpdated", DataType::kInt, DataType::size(DataType::kInt), nullptr, DataFrequency::kStatic, 0),
        }));
	}

	void MS_DeadInstruments::preDataBuild(pesa::IDataRegistry &dr, const pesa::DataCacheInfo &dci) {
	    auto perfIds = dr.stringData(nullptr, m_datasetName + ".perfId");
	    auto instruments = dr.stringData(nullptr, m_datasetName);
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createMS_DeadInstruments(const pesa::ConfigSection& config) {
	return new pesa::MS_DeadInstruments((const pesa::ConfigSection&)config);
}

