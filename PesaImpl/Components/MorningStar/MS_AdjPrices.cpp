/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// MS_AdjPrices.cpp
///
/// Created on: 28 Sep 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Poco/Timestamp.h"
#include "Poco/String.h" 
#include "PesaImpl/Components/SP/SpGeneric.h"
#include "./MS_Util.h"

namespace pesa {
	class MS_AdjPrices : public SpGeneric { 
	private:
		static const int		s_numItems = 6;
		static const StringVec	s_unadjustedPrices;
		static const StringVec	s_adjustedPrices;

		const FloatMatrixData*	m_unadjustedPrices[s_numItems];		/// Loaded unadjusted prices
		const FloatMatrixData*	m_adjustedPrices[s_numItems];		/// Loaded adjusted prices. 
		mutable StringVec		m_symbols;							/// Symbols for which the data will be update in this run
		mutable SizeVec			m_symbolIds;						/// The ids of the symbols (in SecMaster) that we're fetching
		mutable std::unordered_map<std::string, size_t> m_symbolMap;/// Symbol mapping

		IntDay					m_diDelta = 0;						/// The delta (in terms of number of days) for which the data is being update

		virtual bool			doesProvideUUIDMapping() const;
		virtual size_t			mapUuid(const std::string& uuid) const;

	public:
								MS_AdjPrices(const ConfigSection& config);
		virtual 				~MS_AdjPrices();

		////////////////////////////////////////////////////////////////////////////
		/// SpGeneric/IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			buildDaily(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void			preDataBuild(IDataRegistry& dr, const DataCacheInfo& dci);
		virtual void			getAllSymbols(IDataRegistry& dr, const DataCacheInfo& dci, IntDay di, std::vector<std::string>& ids) const;
	};

	const StringVec				MS_AdjPrices::s_unadjustedPrices = {
		"closePrice", "openPrice", "lowPrice", "highPrice", "volume", "vwap"
	};

	const StringVec				MS_AdjPrices::s_adjustedPrices = {
		"adjClosePrice", "adjOpenPrice", "adjLowPrice", "adjHighPrice", "adjVolume", "adjVWAP"
	};

	MS_AdjPrices::MS_AdjPrices(const ConfigSection& config) 
		: SpGeneric(config) {
		setLogChannel("MS_AdjPrices");
	}

	MS_AdjPrices::~MS_AdjPrices() {
		CTrace("Deleting MS_AdjPrices");
	}

	bool MS_AdjPrices::doesProvideUUIDMapping() const {
		return true;
	}

	size_t MS_AdjPrices::mapUuid(const std::string& uuid) const {
		auto uuidIter = m_symbolMap.find(uuid);
		ASSERT(uuidIter != m_symbolMap.end(), "Unable to map uuid: " << uuid << std::endl);
		return uuidIter->second;
	}

	void MS_AdjPrices::preDataBuild(IDataRegistry& dr, const DataCacheInfo& dci) {
		for (size_t i = 0; i < s_adjustedPrices.size(); i++)
			m_adjustedPrices[i] = dr.floatData(dci.universe, "baseData." + s_adjustedPrices[i], true);

		if (!m_adjustedPrices[0]) {
			m_diDelta = 0;
			return;
		}

		for (size_t i = 0; i < s_unadjustedPrices.size(); i++)
			m_unadjustedPrices[i] = dr.floatData(dci.universe, "baseData." + s_unadjustedPrices[i], true);

		ASSERT(m_unadjustedPrices[0]->rows() >= m_adjustedPrices[0]->rows(), "Adjusted prices data cannot be larger than the unadjusted price data. Unadjusted prices rows: " 
			<< m_unadjustedPrices[0]->rows() << " - Adjusted prices rows: " << m_adjustedPrices[0]->rows());

		m_diDelta = (IntDay)(m_unadjustedPrices[0]->rows() - m_adjustedPrices[0]->rows());

		if (!m_diDelta)
			m_diDelta = (size_t)std::max(config().getInt("minDaysToUpdate", 5), 0);
	}

	void MS_AdjPrices::buildDaily(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) { 
		bool useBaseImplementation = true;

		/// If we loaded all the symbols then we don't need to do anything else. 
		/// Data has already been calculated
		if (m_symbols.size() || m_diDelta) 
			useBaseImplementation = false;

		m_noMappingOnCopy = !useBaseImplementation;

		SpGeneric::buildDaily(dr, dci, frame);

		/// Data is already done here
		if (useBaseImplementation)
			return;

		std::string dataName = dci.def.name;
		int dataIndex = -1;

		for (size_t dindex = 0; dindex < s_adjustedPrices.size() && dataIndex < 0; dindex++) {
			if (s_adjustedPrices[dindex] == dataName)
				dataIndex = (int)dindex;
		}

		ASSERT(dataIndex >= 0 && dataIndex < (int)s_adjustedPrices.size(), "Unable to find dataName: " << dataName);
		DataPtr baseData = nullptr;
		size_t numNew = 0, numUpdated = 0;
		size_t numSecurities = dci.universe->size(dci.di);

		/// If we're within the bounds of the adjusted data fetch then we use that
		/// otherwise we can just use the unadjusted values as the base 
		if (dci.di < (IntDay)m_adjustedPrices[dataIndex]->rows())
			baseData = (const_cast<FloatMatrixData *>(m_adjustedPrices[dataIndex]))->extractRow(dci.di);
		else
			baseData = (const_cast<FloatMatrixData*>(m_unadjustedPrices[dataIndex]))->extractRow(dci.di);

		size_t existingSize = baseData->cols();

		/// If the size is not the same as the universe size, then resize the array ...
		if (numSecurities > existingSize) {
            baseData->ensureSize(1, numSecurities, true);

            /// If the array has been resized, that means that new elements have been added. For these, we have put the logic
            /// in getAllSymbols to fetch everything. We just copy the adjusted valuse verbatim
        }

		/// Now that we have a basic data to play with, we copy the values over
		/// for the symbols that we'se just fetched!
		for (size_t si = 0; si < m_symbols.size(); si++) {
			size_t ii = m_symbolIds[si];
			baseData->copyElement(frame.data.get(), 0, si, 0, ii);
		}

		frame.data			= baseData;
		frame.dataSize		= baseData->dataSize();
		frame.dataOffset	= 0;
		frame.noDataUpdate	= false;

        CInfo("Updated data: [%-8u]-[Count: %z / %z] - %s", dci.rt.dates[dci.di], baseData->cols(), numSecurities, dci.def.toString(dci.ds));
	}

	void MS_AdjPrices::getAllSymbols(IDataRegistry& dr, const DataCacheInfo& dci, IntDay di, std::vector<std::string>& ids) const {
		/// The symbol type parsing is already handled so we don't reinvent that and 
		/// us the base implementation to fetch all the symbols ...
		SpGeneric::getAllSymbols(dr, dci, di, ids);

		/// If no adjusted prices have been loaded then we load the entire database [just send the same ids back]
		if (!m_adjustedPrices[0]) {
			m_symbols.clear();
			return;
		} 
		else if (!m_symbols.empty()) { /// If we've already constructed a list of symbols
			ids = m_symbols;
			return;
		}

		/// Otherwise build a list of instruments for which a price changing corporate action has happened

		/// Load the corporate actions events calendar
		std::string eventLabelId	= config().getRequired("eventLabel");
		const auto* eventLabel		= dr.stringData(dci.universe, eventLabelId);

		ASSERT(eventLabel, "Unable to load event label data: " << eventLabelId);

		IntDay numRows				= (IntDay)eventLabel->rows();
		size_t numCols				= eventLabel->cols();
		size_t numExistingCols      = m_adjustedPrices[0] ? m_adjustedPrices[0]->cols() : numCols;
		size_t numSecurities 		= dci.universe->size(dci.di);
		IntDay diStart				= std::max(dci.diEnd - m_diDelta, dci.diStart);

		ASSERT(numCols == numSecurities, "Security mismatch. Number of securities: " << numSecurities << " - do not match number of columns in the event label data: " << numCols);

		/// Only look m_diDelta number of days
		for (IntDay di = diStart; di < std::min(numRows, dci.diEnd + 1); di++) {
			IntDate date = dci.rt.dates[di];

			for (size_t ii = 0; ii < numCols; ii++) {
				size_t count = eventLabel->depthAt(di, ii);
				std::string secId = ids[ii];
				bool didAdd = false;

				ASSERT(!secId.empty(), "Invalid state where empty symbol was returned by the universe at index: " << ii 
					<< " - Actual security at this index is: " << dci.universe->security((IntDay)di, (Security::Index)ii).debugString());

				/// didAdd is used to prevent a symbol being added multiple times since the 
				/// corporate actions event data can have a depth more than 1
				for (size_t kk = 0; kk < count && !didAdd; kk++) {
					auto eventType = eventLabel->getValueAt(di, ii, kk);

					/// If its a price affecing event, then we get the adjusted prices
					if (MS_Util::isPriceAffectingEvent(eventType)) {
						m_symbols.push_back(secId);
						m_symbolIds.push_back(ii);
						m_symbolMap[secId] = m_symbols.size() - 1;

						didAdd = true;
					}
				}
			}

			/// Also get the data for any new symbols that might have been added from yesterday => today
			/// These are all the new securities, so we fetch the data for these anyway!
			for (size_t ii = numExistingCols; ii < numCols; ii++) {
				std::string secId = ids[ii];

				if (m_symbolMap.find(secId) == m_symbolMap.end()) {
                    m_symbols.push_back(secId);
                    m_symbolIds.push_back(ii);
                    m_symbolMap[secId] = m_symbols.size() - 1;
                }
			}
		}

		/// Now we send the real instrument list
		ids = m_symbols;
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createMS_AdjPrices(const pesa::ConfigSection& config) {
	return new pesa::MS_AdjPrices((const pesa::ConfigSection&)config);
}

