/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// MS_DivAdjFactor.cpp
///
/// Created on: 09 May 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "PesaImpl/Components/SP/SpGeneric.h"
#include "Poco/String.h"

namespace pesa {
	class MS_DivAdjFactor : public IDataLoader { 
	private:
        typedef std::unordered_map<std::string, float> FloatMap;
		FloatMatrixDataPtr		m_divFactor;			    /// Cumulative factor
        FloatMap				m_ccyFactors;				/// Additional currency factors e.g. GBP which is quoted in pence

		void					buildDivFactor(IDataRegistry& dr, const DataCacheInfo& dci);

	public:
								MS_DivAdjFactor(const ConfigSection& config);
		virtual 				~MS_DivAdjFactor();

		////////////////////////////////////////////////////////////////////////////
		/// MS_DivAdjFactor overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
	};

	MS_DivAdjFactor::MS_DivAdjFactor(const ConfigSection& config) 
		: IDataLoader(config, "MS_DivAdjFactor") {
        auto ccyFactorsStr = config.getResolvedRequiredString("currencyFactors");
        auto ccyFactors = Util::split(ccyFactorsStr, ",");

        for (auto ccyFactor : ccyFactors) {
            auto parts = Util::split(ccyFactor, ":");
            ASSERT(parts.size() == 2, "Invalid currency factor: " << ccyFactor << " - Expecting in form: CCY:0.01 or some other floating point value");
            m_ccyFactors[parts[0]] = Util::cast<float>(parts[1]);
        }
    }

	MS_DivAdjFactor::~MS_DivAdjFactor() {
		CTrace("Deleting MS_DivAdjFactor");
	}

	void MS_DivAdjFactor::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		std::string datasetName			= config().getRequired("datasetName");
		std::string dataName			= config().getRequired("dataName");

		datasets.add(Dataset(datasetName, {
			DataValue(dataName, DataType::kFloat, sizeof(float), nullptr, DataFrequency::kDaily, DataFlags::kAlwaysOverwrite),
		}));
	}

	void MS_DivAdjFactor::buildDivFactor(IDataRegistry& dr, const DataCacheInfo& dci) { 
		/// We've already built the dividend factor
		if (m_divFactor)
			return;

		std::string exDateId			= config().getRequired("exDate");
		std::string divTypeId			= config().getRequired("divType");
		std::string divAmountId			= config().getRequired("divAmount");
		std::string priceId				= config().getRequired("price");
		std::string divCcyId            = config().getRequired("divCcy");

		const auto* exDates				= dr.int64VarData(dci.universe, exDateId);
		const auto* divTypes			= dr.stringData(dci.universe, divTypeId);
		const auto* divAmounts			= dr.floatVarData(dci.universe, divAmountId);
        const auto* prices				= dr.floatData(dci.universe, priceId);

		ASSERT(exDates, "Unable to load exDates: " << exDateId);
		ASSERT(divTypes, "Unable to load divTypes: " << divTypeId);
		ASSERT(divAmounts, "Unable to load divAmounts: " << divAmounts);

		IntDay numRows					= (IntDay)exDates->rows();
		size_t numCols					= exDates->cols();

		m_divFactor						= std::make_shared<FloatMatrixData>(dci.universe, dci.def, numRows, numCols);
		m_divFactor->setMemory(1.0f);

		/// We don't want to calculate the dividend factor on the first day of the cache. It makes no sense.
		for (IntDay di = std::max(dci.diStart, 1); di < std::min(numRows, dci.diEnd + 1); di++) {
			for (size_t ii = 0; ii < numCols; ii++) {
				size_t numDates = exDates->depthAt(di, ii);
				auto sec = dci.universe->security(di, ii);
				auto divCcy = sec.currencyStr();

				ASSERT(!divCcy.empty(), "Invalid currency on security: " << sec.debugString());

				for (size_t kk = 0; kk < numDates; kk++) {
					int64_t iexDate		= exDates->item(di, ii, kk);

					/// This is not a valid date
					if (!iexDate)
						continue;

					IntDate exDate		= Util::dateToInt(exDates->getDTValue(di, ii, kk));

					/// If the ex-date is outside our simulation range then ignore it for the time being
					if (exDate < dci.rt.startDate || exDate > dci.rt.endDate)
						continue;

					IntDay diDst		= dci.rt.getDateIndex(exDate);

					ASSERT(diDst > 0 && diDst < (IntDay)numRows, "Unexpected dividend ex-date: " << exDate << " - diDst: " << diDst << " - Range: [0, " << numRows << ")");

					auto divType		= divTypes->getValueAt(di, ii, kk);
					float divAmount		= divAmounts->item(di, ii, kk);
					float price			= (*prices)(di - 1, ii);
					float divFactor		= 1.0f;
					auto sec			= dci.universe->security(di, (Security::Index)ii);
					float ccyFactor     = 1.0f;

                    auto ccyFactorIter = m_ccyFactors.find(divCcy);

                    if (ccyFactorIter != m_ccyFactors.end())
                        ccyFactor   = ccyFactorIter->second;

					/// Assign a default dividend type if there isn't any
					if (divType.empty())
						divType 		= "Cash Dividend";

					if (!divAmount || std::isnan(divAmount))
						continue;

					if (std::isnan(price)) 
						continue;

					//////////////////////////////////////////////////////////////////////////
					/// Cash Dividend & Special Dividend: 
					///		This is supposed to be a subtractive entity. So a 0.5 cash dividend means 50 cents
					///		per share. In order co turn this into a multiplicative entity (Bloomberg, Yahoo style)
					///		we use the following calculation:
					///		adjPrice = price - cashDividend [This is the actual calculation that ...
					///		adjPrice = price * divFactor    [... needs to be converted to this]
					///		price - cashDividend = price * divFactor
					///		divFactor = (price - cashDividend) / price
					///		divFactor = 1.0 - cashDividend / price
					//////////////////////////////////////////////////////////////////////////
					if (divType == "Cash Dividend" || 
						divType == "Special Dividend" || 
						divType == "Special Cash Dividend" || 
						divType == "Final Dividend" || 
						divType == "Interim Dividend" || 
						divType == "Interest on Capital" ||
						divType == "Memorial" || 
						divType == "Supplemental Dividend") {
						divFactor = 1.0f - ((divAmount / ccyFactor) / price);
					}
					else if (divType == "Unspecified Dividend") {
						CInfo("[%u] No adjustment required for dividend: %s [%s : %s]. Amount = %0.2hf", exDate, divType, sec.tickerStr(), sec.uuidStr(), divAmount);
					}
					//else if (divType == "Return on capital") {
					//	ASSERT(false, "Unsupporte;
					//}
					else if (!divType.empty()) {
						ASSERT(false, "Unsupported dividend type: " << divType << ". Date: " << exDate << ". Security: " << sec.debugString());
					}

					(*m_divFactor)(diDst, ii) *= divFactor;
				}
			}
		}
	}

	void MS_DivAdjFactor::build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		if (!m_divFactor)
			buildDivFactor(dr, dci);

		DataPtr data		= DataPtr(m_divFactor->extractRow(dci.di - dci.diStart));
		frame.data			= data;
		frame.dataOffset	= 0;
		frame.dataSize		= data->dataSize();
		frame.noDataUpdate	= false;
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createMS_DivAdjFactor(const pesa::ConfigSection& config) {
	return new pesa::MS_DivAdjFactor((const pesa::ConfigSection&)config);
}

