/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// MS_JsonParser.cpp
///
/// Created on: 04 May 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "MS_JsonParser.h"

#include "PesaImpl/Pipeline/DataRegistry/DataRegistry.h"
#include "PesaImpl/Components/SP/SpGeneric.h"

#include "Poco/DateTimeParser.h"
#include "Poco/DateTimeFormat.h"
#include "Poco/DateTimeFormatter.h"
#include "Poco/String.h"

namespace pesa {
	MS_JsonParser::MS_JsonParser(const SpGeneric* dataLoader)
		: m_dataLoader(dataLoader) {
		const auto& dataRequired = dataLoader->dataRequired();
		const auto& requiredDataTypes = dataLoader->requiredDataTypes();

		ASSERT(dataRequired.size() == requiredDataTypes.size(), "Invalid data info passed. Expecting both data names and types to be of the same length. Data names: " 
			<< dataRequired.size() << ", Data Types: " << requiredDataTypes.size());

		auto rmappings = dataLoader->rmapping();

		for (size_t i = 0; i < dataRequired.size(); i++) {
			auto rmappedNameIter = rmappings.find(dataRequired[i]);
			m_def[dataRequired[i]] = requiredDataTypes[i];

			if (rmappedNameIter != rmappings.end())
				m_def[rmappedNameIter->second] = requiredDataTypes[i];
		}
	}

	static inline DataPtr createColumn(SpRest::Result& results, const std::string& columnId, DataType::Type type, size_t rows, size_t cols) {
		auto iter = results.columnDefs.find(columnId);
		if (iter != results.columnDefs.end())
			return results.data[columnId];

		results.columnDefs[columnId] = DataType::toString(type);

		DataDefinition def(type, DataType::size(type));
		DataPtr data = DataRegistry::createDataBuffer(nullptr, def, nullptr);

		data->ensureSize(rows, cols, false);
		data->invalidateAll();
		results.data[columnId] = data;

		return data;
	}

	inline Poco::DateTime parseDate(const std::string& date, const std::string& fmt) {
		Poco::DateTime dt;
		int tz = 0;
		//const std::string fmt = "%d-%m-%Y";
		Poco::DateTimeParser::parse(fmt, date, dt, tz);
		dt.makeUTC(tz);
		return dt;
	}

	int MS_JsonParser::parseResults(SpRest* rcall, SpRest::Result& result) {
		gason::JsonValue jsonResult = rcall->jsonResult();
		StringVec ctypes, cnames;
		std::vector<Data*> dptrs;
		auto timeseries	= jsonResult.child("ts");
		auto results	= timeseries.child("results");
		auto errors		= timeseries.child("error");
		bool hasErrors	= false;
		
		for (gason::JsonIterator errIter = gason::begin(errors); errIter != gason::end(errors); errIter++) {
			auto errValue = std::string(errIter->value.toString());

			if (errValue == "No Data" || 
				errValue.find("Invalid enablement") != std::string::npos ||
				errValue.find("Invalid Instrument") != std::string::npos ||
				errValue.find("Duplicate instrument") != std::string::npos)
				continue;

			Error("MS_JsonParser", "JSON response error: %s", std::string(errValue));
			return -1;
			//hasErrors = true;
		}

		//ASSERT(!hasErrors, "Errors in JSON reponse. Cannot continue!");

		/// Need to optimise this! Don't think there is a straight forward way of doing this though using gason
		size_t cols	= 0;
		size_t rows = 0;
		const auto& dataRequired = m_dataLoader->dataRequired();
		const auto& requiredDataTypes = m_dataLoader->requiredDataTypes();
		std::string timestampColName = m_dataLoader->config().getString("timestampColName", "");
		std::string dateFormat = m_dataLoader->config().getString("timestampFormat", "{$DD}-{$MM}-{$YYYY}");
		bool ensureDepthConsistency = m_dataLoader->config().getBool("ensureDepthConsistency", false);
		auto depthConsistencyReferenceId = m_dataLoader->config().getResolvedString("depthConsistencyReference", "");

		ASSERT(!ensureDepthConsistency || !depthConsistencyReferenceId.empty(), "No depthConsistencyReferenceId given with ensureDepthConsistency flag!");

		StringMap defaultValues;
		auto defaultValueId = m_dataLoader->config().getResolvedString("defaultValue", "");

        if (!defaultValueId.empty()) {
            auto dvIds = Util::split(defaultValueId, ",");
            for (const auto& dvId : dvIds) {
                auto mapping = Util::split(dvId, "=");
                ASSERT(mapping.size() == 2, "Invalid defaultValue mapping: " << dvId);
                defaultValues[mapping[0]] = mapping[1];
            }
        }

		dateFormat = Poco::replace(dateFormat, "{$DD}", "%d");
		dateFormat = Poco::replace(dateFormat, "{$MM}", "%m");
		dateFormat = Poco::replace(dateFormat, "{$YYYY}", "%Y");

		for (gason::JsonIterator iter = gason::begin(results); iter != gason::end(results); iter++) {
			cols++;

			auto item				= iter->value;
			auto rdata				= item.child("data");
			size_t thisRows			= 0;

			//for (gason::JsonIterator dataIter = gason::begin(rdata); dataIter != gason::end(rdata); dataIter++) {
			//	auto dataItem		= dataIter->value;
			//	for (auto diter = gason::begin(dataItem); diter != gason::end(dataItem); diter++) {
			//		auto name = diter->key;
			//		names.push_back(name);
			//		if (name == timestampColName) {
			//			didFindTS = true;
			//		}
			//	}
			//}

			for (gason::JsonIterator dataIter = gason::begin(rdata); dataIter != gason::end(rdata); dataIter++) {
				auto dataItem		= dataIter->value;

				/// We do this because we need to have the same date map for every instrument that's returned.
				/// Since the order of dates in the returned array for each instrument can be different, we need
				/// to preprocess the dates so that all of them have the same 'di' in a batch. This is done to 
				/// save memory and have consistency for the caller. Otherwise, we'd need to keep a dateMap for
				/// each of the returned securities, which is a huge overhead. 

				if (!timestampColName.empty()) {
					auto tsChild	= dataItem.child(timestampColName.c_str());
					auto dt			= parseDate(tsChild.toString(), dateFormat);

					/// Check whether this date exists within the date map. If it does, then we don't need to do
					/// anything, other then change di
					auto idate		= Util::dateToInt(dt);
					auto dateIter	= result.dateMap.find(idate);

					if (dateIter == result.dateMap.end()) {
					    auto index = (IntDay)result.dateMap.size();
						result.dateMap[idate] = (IntDay)index;
					}
				}
			}
		}

		rows						= result.dateMap.size();
		size_t ii					= 0;
//        auto tsDefIter	            = m_def.find(timestampColName);
//        auto tsDataType             = DataType::kDateTime;
//
//        if (tsDefIter != m_def.end()) {
//            tsDataType		        = tsDefIter->second;
//        }

		for (gason::JsonIterator iter = gason::begin(results); iter != gason::end(results); iter++, ii++) {
			auto item				= iter->value;
			auto rdata				= item.child("data");
			std::string symbol		= item.child("symbol").toString();
			std::string exchangeId	= item.child("exchangeid").toString();
			std::string type		= item.child("type").toString();

			std::string instrument;
			Poco::format(instrument, "%s.%s.%s", exchangeId, type, symbol);

			DataPtr instrumentData	= createColumn(result, "instrument", DataType::kString, 1, cols);
			instrumentData->setString(0, ii, 0, instrument);

			IntDay di				= 0;
			std::unordered_map<IntDate, size_t> dateCounts;
			StringVec columnsHandled;

			for (gason::JsonIterator dataIter = gason::begin(rdata); dataIter != gason::end(rdata); dataIter++, di++) {
				auto dataItem		= dataIter->value;

				/// We do this because we need to have the same date map for every instrument that's returned.
				/// Since the order of dates in the returned array for each instrument can be different, we need
				/// to preprocess the dates so that all of them have the same 'di' in a batch. This is done to 
				/// save memory and have consistency for the caller. Otherwise, we'd need to keep a dateMap for
				/// each of the returned securities, which is a huge overhead. 

				if (!timestampColName.empty()) {
					auto tsChild	= dataItem.child(timestampColName.c_str());
					auto data		= createColumn(result, timestampColName, DataType::kDateTime, rows, cols);
					auto dt			= parseDate(tsChild.toString(), dateFormat);
					auto* dtData	= dynamic_cast<Int64MatrixData*>(data.get());

					/// Check whether this date exists within the date map. If it does, then we don't need to do
					/// anything, other then change di
					auto idate		= Util::dateToInt(dt);
					auto dateIter	= result.dateMap.find(idate);

					ASSERT(dateIter != result.dateMap.end(), "Unable to find date: " << idate << " in the dateMap. The dateMap should've been pre-processed by now!");

					di				= dateIter->second;

					ASSERT(di < dtData->rows(), "Invalid di: " << di << " - Date: " << idate << " - for data: " << timestampColName << " - Max rows: " << dtData->rows());

					/// We also need to set this correctly for the timestamp data as well ...
					dtData->setDTValue(di, ii, dt);
				}

				StringMap handledThisFrame;

				for (auto diter = gason::begin(dataItem); diter != gason::end(dataItem); diter++) {
					auto name		= diter->key;
					auto svalue		= diter->value.toString();
					auto defIter	= m_def.find(name);

					if (defIter == m_def.end())
						continue;

					/// We've handled this already before the for loop
					if (timestampColName == name)
						continue;

					if (ensureDepthConsistency)
                        handledThisFrame[name] = "1";

					auto type		= defIter->second;
					auto data		= createColumn(result, name, type, rows, cols);

					ASSERT(di < rows, "Invalid row index di: " << di << " [Max: " << rows << "]");
					ASSERT(ii < cols, "Invalid col index ii: " << ii << " [Max: " << cols << "]");

					if (type != DataType::kDateTime && type != DataType::kVarDateTime && 
						type != DataType::kTimestamp && type != DataType::kVarTimestamp) { 
						/// If it's the first value then set at depth 0 ...
						if (!data->isVariableDepth())
							data->setString(di, ii, 0, svalue);

						/// Otherwise, expand the depth to accomodate the extra data for the same day
						else {
							size_t existingDepth = data->depthAt(di, ii);
							data->ensureDepthAt(di, ii, existingDepth + 1);
							ASSERT(data->depthAt(di, ii) > existingDepth, Poco::format("[%s] Unable to resize variable depth at: [%d, %z]", name, di, ii));
							data->setString(di, ii, existingDepth, svalue);
						}
					}
					else {
						Poco::DateTime dt = parseDate(svalue, dateFormat);
						std::string dtString = Poco::DateTimeFormatter::format(dt.timestamp(), Poco::DateTimeFormat::ISO8601_FORMAT);

						if (!data->isVariableDepth())
							data->setString(di, ii, 0, dtString);
						else {
							size_t existingDepth = data->depthAt(di, ii);
							data->ensureDepthAt(di, ii, existingDepth + 1);
							ASSERT(data->depthAt(di, ii) > existingDepth, Poco::format("[%s] Unable to resize variable depth at: [%d, %z]", name, di, ii));
							data->setString(di, ii, existingDepth, dtString);
						}
						//Int64MatrixData* dtData = dynamic_cast<Int64MatrixData*>(data.get());
						//dtData->setDTValue(di, ii, dt);
					}
				}

				/// Make sure that the depth of each item is exactly the same, if depth consistency has been requested
				if (ensureDepthConsistency) {
				    auto depthConsistencyReference = result.data[depthConsistencyReferenceId];
				    ASSERT(depthConsistencyReference, "Invalid state where depthConsistencyReference is invalid: " << depthConsistencyReferenceId);

				    size_t desiredDepth = depthConsistencyReference->depthAt(di, ii);

				    /// We only do depth consistency if the reference data is variable depth!
                    for (const auto iter : m_def) {
                        auto name = iter.first;
                        auto type = iter.second;
                        DataPtr defaultData = nullptr;
                        auto defaultValueIter = defaultValues.find(name);

                        if (defaultValueIter != defaultValues.end())
                            defaultData = result.data[defaultValueIter->second];

                        if (name != depthConsistencyReferenceId && name != timestampColName) {
                            /// If an item hasn't been handled in this frame then assign an invalid value to it
                            if (handledThisFrame.find(name) == handledThisFrame.end()) {
                                auto data = createColumn(result, name, type, rows, cols);
                                size_t currentDepth = data->depthAt(di, ii);
                                size_t desiredDepthForThisData = data->isVariableDepth() ? desiredDepth : 1;

                                ASSERT(currentDepth <= desiredDepthForThisData, Poco::format("Invalid state where the depth of the data: %s [%z] is MORE than the reference data: %s [%z]", name, currentDepth, depthConsistencyReferenceId, desiredDepthForThisData));

                                /// Make sure depth is the same and invalidate all the new entries!
                                if (currentDepth < desiredDepthForThisData) {
                                    data->ensureDepthAt(di, ii, desiredDepthForThisData);

                                    if (!defaultData) {
                                        for (size_t kk = currentDepth; kk < desiredDepthForThisData; kk++)
                                            data->makeInvalid(di, ii, kk);
                                    }
                                    else {
                                        auto defDepth = defaultData->depthAt(di, ii);

                                        for (size_t kk = currentDepth; kk < desiredDepth; kk++)
                                            data->copyElement(defaultData.get(), di, ii, di, ii, std::min(kk, defDepth - 1), kk);
                                    }
                                }
                            }
                        }
                    }
				}

				//for (size_t dataIndex = 0; dataIndex < dataRequired.size(); dataIndex++, di++) {
				//	auto name		= dataRequired[dataIndex];
				//	auto type		= requiredDataTypes[dataIndex];

				//	if (name != "instrument") {
				//		auto svalue = dataItem.child(name.c_str()).toString();
				//		auto data	= createColumn(result, name, type, rows, cols);

				//		if (type != DataType::kDateTime)
				//			data->setString(di, ii, 0, svalue);
				//		else {
				//			Poco::DateTime dt = parseDate(svalue);
				//			Int64MatrixData* dtData = dynamic_cast<Int64MatrixData*>(data.get());
				//			dtData->setDTValue(di, ii, dt);

				//			if (!timestampColName.empty() && timestampColName == name) {
				//				IntDate date = Util::dateToInt(dt);
				//				result.dateMap[date] = di;
				//			}
				//		}
				//	}
				//}
			}
		}

		return 0;
	}
} /// namespace pesa
