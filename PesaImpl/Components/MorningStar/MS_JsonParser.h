/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// MS_JsonParser.h
///
/// Created on: 04 May 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "PesaImpl/Components/SP/SpRest.h"

namespace pesa {
	class SpGeneric;

	////////////////////////////////////////////////////////////////////////////
	/// MorningStar JSON parser
	////////////////////////////////////////////////////////////////////////////
	class MS_JsonParser : public SpRest::IParser {
	private:
		typedef std::unordered_map<std::string, DataType::Type> DataDefMap;
		const SpGeneric*		m_dataLoader;				/// The SpGeneric (or derived) data loader that is being used
		DataDefMap				m_def;						/// What are our data definitions along with their types

	public:
								MS_JsonParser(const SpGeneric* dataLoader);
		int						parseResults(SpRest* rcall, SpRest::Result& result);
	};
} /// namespace pesa

