/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// MS_PriceAdjFactors.cpp
///
/// Created on: 28 Sep 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Poco/Timestamp.h"
#include "Poco/String.h" 
#include "PesaImpl/Components/SP/SpGeneric.h"
#include "./MS_Util.h"

namespace pesa {
	class MS_PriceAdjFactor : public IDataLoader { 
	private:
		FloatMatrixData*		m_adjPriceFactors = nullptr;			/// Price adjustment factors are calculated in one go and then this cache is used
		FloatMatrixData*		m_adjVolumeFactors = nullptr;			/// Volume adjustment factors are calculated in one go and then this cache is used

		void					calculateAdjFactors(IDataRegistry& dr, const DataCacheInfo& dci, FloatMatrixData** adjFactors, const std::string& unadjustedDataId, const std::string& adjustedDataId);
		void					calculatePriceAdjFactors(IDataRegistry& dr, const DataCacheInfo& dci);
		void					calculateVolumeAdjFactors(IDataRegistry& dr, const DataCacheInfo& dci);

	public:
								MS_PriceAdjFactor(const ConfigSection& config);
		virtual 				~MS_PriceAdjFactor();

		////////////////////////////////////////////////////////////////////////////
		/// SpGeneric/IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {}
		virtual void			buildPriceAdjFactors(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void			buildVolumeAdjFactors(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void			buildCAXPriceEvent(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
	};

	MS_PriceAdjFactor::MS_PriceAdjFactor(const ConfigSection& config) 
		: IDataLoader(config, "MS_PriceAdjFactor") {
	}

	MS_PriceAdjFactor::~MS_PriceAdjFactor() {
		CTrace("Deleting MS_PriceAdjFactor");
	}

	void MS_PriceAdjFactor::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		Dataset ds1(config().getRequired("datasetName"));
		ds1.add(DataValue("f_caxPriceEvent", DataType::kFloat, DataType::size(DataType::kFloat), THIS_DATA_FUNC(MS_PriceAdjFactor::buildCAXPriceEvent), DataFrequency::kDaily, 0));
		ds1.flushDataset = true;

		Dataset ds2(config().getRequired("datasetName"));
		ds2.add(DataValue("_f_adjPrice", DataType::kFloat, DataType::size(DataType::kFloat), THIS_DATA_FUNC(MS_PriceAdjFactor::buildPriceAdjFactors), DataFrequency::kDaily, 0));
		ds2.flushDataset = true;

		Dataset ds3(config().getRequired("datasetName"));
		ds2.add(DataValue("_f_adjVolume", DataType::kFloat, DataType::size(DataType::kFloat), THIS_DATA_FUNC(MS_PriceAdjFactor::buildVolumeAdjFactors), DataFrequency::kDaily, 0));
		ds2.flushDataset = true;

		datasets.add(ds1);
		datasets.add(ds2);
		datasets.add(ds3);
	}

	void MS_PriceAdjFactor::buildCAXPriceEvent(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		/// Load the corporate actions events calendar
		std::string eventLabelId	= config().getRequired("eventLabel");
		const auto* eventLabel		= dr.stringData(dci.universe, eventLabelId);

		ASSERT(eventLabel, "Unable to load event label data: " << eventLabelId);

		IntDay numRows				= (IntDay)eventLabel->rows();
		size_t numCols				= eventLabel->cols();
		size_t numSecurities		= dci.universe->size(dci.di);

		ASSERT(numCols == numSecurities, "Security mismatch. Number of securities: " << numSecurities << " - do not match number of columns in the event label data: " << numCols);

		/// Only look for current date
		IntDate date				= dci.rt.dates[dci.di];
		FloatMatrixData* data		= new FloatMatrixData(dci.universe, dci.def);

		data->ensureSize(1, numSecurities);
		data->invalidateAll();

		size_t numEvents = 0;

		for (size_t ii = 0; ii < numCols; ii++) {
			size_t count = eventLabel->depthAt(dci.di, ii);
			bool didHandle = false;

			/// didHandle is used to prevent a symbol being added multiple times since the 
			/// corporate actions event data can have a depth more than 1
			for (size_t kk = 0; kk < count && !didHandle; kk++) {
				auto eventType = eventLabel->getValueAt(dci.di, ii, kk);

				/// If its a price affecing event, then we get the adjusted prices
				if (MS_Util::isPriceAffectingEvent(eventType)) {
					(*data)(0, ii) = 1.0f;
					didHandle = true;
					numEvents++;
				}
			}
		}

		frame.data			= DataPtr(data);
		frame.dataSize		= data->dataSize();
		frame.dataOffset	= 0;
		frame.noDataUpdate	= false;

		CInfo("[%u] - Num CAX Price Events: %z", dci.rt.dates[dci.di], numEvents);
	}

	//void MS_PriceAdjFactor::calculatePriceAdjFactors(IDataRegistry& dr, const DataCacheInfo& dci) {
	//	/// We've already calculated this
	//	if (m_adjPriceFactors)
	//		return;

	//	const FloatMatrixData* closePrice = dr.floatData(dci.universe, "baseData.closePrice");
	//	const FloatMatrixData* adjClosePrice = dr.floatData(dci.universe, "baseData.adjClosePrice"); 

	//	ASSERT(closePrice->rows() == adjClosePrice->rows(), "Adjusted and unadjusted price data rows do not match. Adjusted rows: " << adjClosePrice->rows() << " - Un-adjusted rows: " << closePrice->rows());
	//	ASSERT(closePrice->cols() == adjClosePrice->cols(), "Adjusted and unadjusted price data columns do not match. Adjusted cols: " << adjClosePrice->cols() << " - Un-adjusted columns: " << closePrice->cols());

	//	//const FloatMatrixData* caxPriceEvent = dr.floatData(dci.universe, "baseData.f_caxPriceEvent");
	//	IntDay numRows = dci.diEnd - dci.diStart + 1;
	//	size_t numSecurities = dci.universe->size(dci.diEnd);

	//	m_adjPriceFactors = new FloatMatrixData(dci.universe, dci.def);
	//	m_adjPriceFactors->ensureSize(numRows, numSecurities, false);
	//	m_adjPriceFactors->setMemory(1.0f);

	//	/// Accumulator 
	//	FloatMatrixData accum(dci.universe, dci.def);
	//	accum.ensureSize(1, numSecurities);
	//	accum.setMemory(1.0f);

	//	std::string eventLabelId = config().getRequired("eventLabel");
	//	const auto* eventLabel = dr.stringData(dci.universe, eventLabelId);

	//	ASSERT(eventLabel, "Unable to load event label data: " << eventLabelId);

	//	for (IntDay di = dci.diEnd; di >= std::max(dci.diStart, 1); di--) {
	//		IntDate date = dci.rt.dates[di];
	//		IntDay dix = di - dci.diStart;

	//		for (size_t ii = 0; ii < numSecurities; ii++) {
	//			size_t count = eventLabel->depthAt(di, ii);
	//			bool didHandle = false;

	//			/// didHandle is used to prevent a symbol being added multiple times since the 
	//			/// corporate actions event data can have a depth more than 1
	//			for (size_t kk = 0; kk < count && !didHandle; kk++) {
	//				auto eventType = eventLabel->getValueAt(di, ii, kk);

	//				/// If its a price affecing event, then we get the adjusted prices
	//				if (MS_Util::isPriceAffectingEvent(eventType)) 
	//					didHandle = true;
	//			}

	//			//float eventVal = (*caxPriceEvent)(di, ii);

	//			if (didHandle) {
	//				auto sec = dci.universe->security(di, (Security::Index)ii);
	//				float cp = (*closePrice)(std::max(di - 1, 0), ii);
	//				float acp = (*adjClosePrice)(std::max(di - 1, 0), ii);
	//				float acc = accum(0, ii);
	//				float factor = (acp / cp) * acc;
	//				accum(0, ii) *= factor;
	//				(*m_adjPriceFactors)(dix, ii) = factor;
	//			}
	//		}
	//	}
	//}

	void MS_PriceAdjFactor::calculateAdjFactors(IDataRegistry& dr, const DataCacheInfo& dci, FloatMatrixData** adjFactors, const std::string& unadjustedDataId, const std::string& adjustedDataId) {
		/// We've already calculated this
		if (*adjFactors)
			return;

		const FloatMatrixData* data = dr.floatData(dci.universe, unadjustedDataId);
		const FloatMatrixData* adjData = dr.floatData(dci.universe, adjustedDataId);

		ASSERT(data->rows() == adjData->rows(), "Adjusted and unadjusted data rows do not match. Adjusted rows: " << adjData->rows() << " - Un-adjusted rows: " << data->rows());
		ASSERT(data->cols() == adjData->cols(), "Adjusted and unadjusted data columns do not match. Adjusted cols: " << adjData->cols() << " - Un-adjusted columns: " << data->cols());

		//const FloatMatrixData* caxPriceEvent = dr.floatData(dci.universe, "baseData.f_caxPriceEvent");
		IntDay numRows = dci.diEnd - dci.diStart + 1;
		size_t numSecurities = dci.universe->size(dci.diEnd);

		*adjFactors = new FloatMatrixData(dci.universe, dci.def);
		(*adjFactors)->ensureSize(numRows, numSecurities, false);
		(*adjFactors)->setMemory(1.0f);

		/// Accumulator 
		FloatMatrixData accum(dci.universe, dci.def);
		accum.ensureSize(1, numSecurities);
		accum.setMemory(1.0f);

		std::string eventLabelId = config().getRequired("eventLabel");
		const auto* eventLabel = dr.stringData(dci.universe, eventLabelId);

		ASSERT(eventLabel, "Unable to load event label data: " << eventLabelId);

		for (IntDay di = dci.diEnd; di >= std::max(dci.diStart, 1); di--) {
			IntDate date = dci.rt.dates[di];
			IntDay dix = di - dci.diStart;

			for (size_t ii = 0; ii < numSecurities; ii++) {
				size_t count = eventLabel->depthAt(di, ii);
				bool didHandle = false;

                /// didHandle is used to prevent a symbol being added multiple times since the
				/// corporate actions event data can have a depth more than 1
				for (size_t kk = 0; kk < count && !didHandle; kk++) {
					auto eventType = eventLabel->getValueAt(di, ii, kk);

					/// If its a price affecing event, then we get the adjusted prices
					if (MS_Util::isPriceAffectingEvent(eventType))
						didHandle = true;
				}

				//float eventVal = (*caxPriceEvent)(di, ii);

				if (didHandle) {
//					auto sec = dci.universe->security(di, (Security::Index)ii);

                    float cp = (*data)(std::max(di - 1, 0), ii);
					float acp = (*adjData)(std::max(di - 1, 0), ii);
					float acc = accum(0, ii);

                    float factor = (acp / cp) / acc;
					accum(0, ii) *= factor;
					(*(*adjFactors))(dix, ii) = factor;
				}
			}
		}
	}

	void MS_PriceAdjFactor::calculatePriceAdjFactors(IDataRegistry& dr, const DataCacheInfo& dci) {
		calculateAdjFactors(dr, dci, &m_adjPriceFactors, "baseData.closePrice", "baseData.adjClosePrice");
	}

	void MS_PriceAdjFactor::buildPriceAdjFactors(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		calculatePriceAdjFactors(dr, dci);

		IntDate date		= dci.rt.dates[dci.di];
		DataPtr data		= m_adjPriceFactors->extractRow(dci.di - dci.diStart);

		frame.data			= data;
		frame.dataSize		= data->dataSize();
		frame.dataOffset	= 0;
		frame.noDataUpdate	= false;

		CInfo("Updated data: [%-8u]-[Count: %z] - %s", date, data->cols(), std::string(dci.def.name));
	}

	void MS_PriceAdjFactor::calculateVolumeAdjFactors(IDataRegistry& dr, const DataCacheInfo& dci) {
		calculateAdjFactors(dr, dci, &m_adjVolumeFactors, "baseData.volume", "baseData.adjVolume");
	}

	void MS_PriceAdjFactor::buildVolumeAdjFactors(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		calculateVolumeAdjFactors(dr, dci);

		IntDate date		= dci.rt.dates[dci.di];
		DataPtr data		= m_adjVolumeFactors->extractRow(dci.di - dci.diStart);

		frame.data			= data;
		frame.dataSize		= data->dataSize();
		frame.dataOffset	= 0;
		frame.noDataUpdate	= false;

		CInfo("Updated data: [%-8u]-[Count: %z] - %s", date, data->cols(), std::string(dci.def.name));
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createMS_PriceAdjFactor(const pesa::ConfigSection& config) {
	return new pesa::MS_PriceAdjFactor((const pesa::ConfigSection&)config);
}

