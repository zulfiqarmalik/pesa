/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// MS_PriceAdjustments.cpp
///
/// Created on: 13 May 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Poco/Timestamp.h"
#include "Poco/String.h"
#include "./MS_Util.h"

namespace pesa {
	class MS_PriceAdjustments : public IDataLoader { 
	private:
		FloatMatrixDataPtr		m_dailyFactor;			/// Daily factor

		void					buildDailyFactor(IDataRegistry& dr, const DataCacheInfo& dci);

	public:
								MS_PriceAdjustments(const ConfigSection& config);
		virtual 				~MS_PriceAdjustments();

		////////////////////////////////////////////////////////////////////////////
		/// MS_PriceAdjustments overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
	};

	MS_PriceAdjustments::MS_PriceAdjustments(const ConfigSection& config) 
		: IDataLoader(config, "MS_PriceAdjustments") {
	}

	MS_PriceAdjustments::~MS_PriceAdjustments() {
		CTrace("Deleting MS_PriceAdjustments");
	}

	void MS_PriceAdjustments::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		std::string datasetName			= config().getRequired("datasetName");
		std::string dataName			= config().getRequired("dataName");

		datasets.add(Dataset(datasetName, {
			DataValue(dataName, DataType::kFloat, sizeof(float), nullptr, DataFrequency::kDaily, DataFlags::kAlwaysOverwrite),
		}));
	}

	void MS_PriceAdjustments::buildDailyFactor(IDataRegistry& dr, const DataCacheInfo& dci) { 
		/// We've already built the dividend factor
		if (m_dailyFactor)
			return;

		std::string eventLabelId		= config().getRequired("eventLabel");
		std::string exDateId			= config().getRequired("exDate");
		std::string ratioOldId			= config().getRequired("ratioOld");
		std::string ratioNewId			= config().getRequired("ratioNew");
		std::string spinoffChildPriceId = config().getRequired("spinoffChildPrice");
		std::string spinoffParentPriceId= config().getRequired("spinoffParentPrice");
		std::string issuePriceId		= config().getRequired("issuePrice");
		std::string closePriceId		= config().getString("closePrice", "baseData.closePrice");

		const StringData* eventLabel	= dr.stringData(dci.universe, eventLabelId);
		const auto* exDates				= dr.int64VarData(dci.universe, exDateId);
		const auto* ratioOld			= dr.floatVarData(dci.universe, ratioOldId);
		const auto* ratioNew			= dr.floatVarData(dci.universe, ratioNewId);
		const auto* spinoffChildPrice	= dr.floatVarData(dci.universe, spinoffChildPriceId);
		const auto* spinoffParentPrice	= dr.floatVarData(dci.universe, spinoffParentPriceId);
		const auto* issuePrice			= dr.floatVarData(dci.universe, issuePriceId);
		const auto* closePrice			= dr.floatData(dci.universe, closePriceId);

		IntDay numRows					= (IntDay)ratioOld->rows();
		size_t numCols					= ratioOld->cols();
		size_t numSecurities			= dci.universe->size(dci.diStart);

		ASSERT(numCols == numSecurities, "Invalid number of columns: " << numCols << " - Num securities: " << numSecurities);

		m_dailyFactor					= std::make_shared<FloatMatrixData>(dci.universe, dci.def, numRows, numCols);
		m_dailyFactor->setMemory(1.0f);

		/// We don't want to calculate the dividend factor on the first day of the cache. It makes no sense.
		for (IntDay di = std::max(dci.diStart, 1); di < std::min(numRows, dci.diEnd + 1); di++) {
			IntDate date				= dci.rt.dates[di];

			for (size_t ii = 0; ii < numCols; ii++) {
				size_t count			= eventLabel->depthAt(di, ii);

				/// These are specific to THIS date and THIS security. This is used to track down multiple corporate
				/// actions of the same type happening on the same day
				size_t kkParentPrice	= 0;
				size_t kkChildPrice		= 0;
				size_t kkIssuePrice		= 0;

				for (size_t kk = 0; kk < count; kk++) {
					auto eventType		= eventLabel->getValueAt(di, ii, kk);

					auto sec			= dci.universe->security((IntDay)di, (Security::Index)ii);

					if (!MS_Util::isPriceAffectingEvent(eventType))
						continue;

					float factor		= 1.0f;
					//int64_t iexDate			= exDates->getValue(di, ii);

					/// This is not a valid date
					//if (!iexDate)
					//	continue;

					//IntDate exDate			= Util::dateToInt(exDates->getDTValue(di, ii));

					/// If the ex-date is outside our simulation range then ignore it for the time being
					//if (exDate < dci.rt.startDate && exDate > dci.rt.endDate)
					//	continue;

					IntDay diDst		= di; ///dci.rt.getDateIndex(exDate);

					//ASSERT(diDst > 0 && diDst < (IntDay)numCols, "Unexpected dividend ex-date: " << exDate << " - diDst: " << diDst << " - Range: [0, " << numCols << "]");

					float oldRatio		= ratioOld->item(di, ii, kk);
					float newRatio		= ratioNew->item(di, ii, kk);

					if (std::isnan(oldRatio) || std::isnan(newRatio))
						continue;
					else if (oldRatio <= std::numeric_limits<float>::epsilon() && newRatio <= std::numeric_limits<float>::epsilon()) {
						CWarning("[%u] - %s No old & new ratio for security: %s [TKR: %s, INST: %s]", date, eventType, sec.uuidStr(), sec.tickerStr(), sec.instrumentId());
						continue;
					}

					if (eventType == "Demerger") {
						float childPrice = spinoffChildPrice->item(di, ii, kkChildPrice++);
						float parentPrice = spinoffParentPrice->item(di, ii, kkParentPrice++);

						if (parentPrice < std::numeric_limits<float>::epsilon() && childPrice < std::numeric_limits<float>::epsilon()) {
							CWarning("[%u - %s] No parent & child price for security: %s [TKR: %s, INST: %s]", date, eventType, sec.uuidStr(), sec.tickerStr(), sec.instrumentId());
							continue;
						}

						if (std::isnan(childPrice)) {
							float yesterdayPrice = (*closePrice)(std::max(di - 1, 0), ii);
							ASSERT(yesterdayPrice >= parentPrice, "Invalid situation where yesterday's price is less than the spin-off price of the parent. Spin off price: : " << parentPrice 
								<< " - Yesterday's price: " << yesterdayPrice);
							childPrice = yesterdayPrice - parentPrice;
						}

						ASSERT(childPrice > 0.0f && parentPrice > 0.0f, Poco::format("[%u] - Demerger: NULL childPrice [%0.2hf] / parentPrice [%0.2hf] for security: %s", date, childPrice, parentPrice, sec.debugString()));
						ASSERT(!std::isnan(childPrice) && !std::isnan(parentPrice), Poco::format("[%u] - Demerger: NAN childPrice / parentPrice for security: %s", date, sec.debugString()));

						float ratio		= oldRatio / newRatio;
						float denom		= parentPrice * ratio;
						ASSERT(denom > 0.0f, Poco::format("[%u] - Demerger: Invalid denominator calculated for parent price: %0.2hf [Ratio: %0.2hf]. Security: %s", date, parentPrice, ratio, sec.debugString()));

						float reduction	= childPrice / denom;

						ASSERT(reduction > 0.0f && reduction < 1.0f, Poco::format("[%u] - Demerger: Invalid reduction calculated for parent price: %0.2hf [Child price: 0.2hf, Ratio: %0.2hf]. Security: %s", date, parentPrice, childPrice, ratio, sec.debugString()));
						(*m_dailyFactor)(diDst, ii) *= (1.0f - reduction);
					}
					else if (eventType == "Rights" || eventType == "Entitlement Issue") {
						float newShares = oldRatio / newRatio;
						float offerPrice = issuePrice->item(di, ii, kkIssuePrice);
						float existingPrice = (*closePrice)(std::max(di - 1, 0), ii);

						/// This should match the closing price
						if (eventType == "Entitlement Issue")
							existingPrice = spinoffParentPrice->item(di, ii, kkParentPrice++);

						ASSERT(!std::isnan(offerPrice) && !std::isnan(existingPrice) && offerPrice > 0.0f && existingPrice > 0.0f, Poco::format("[%u] - Entitlement: Invalid offer-price [%0.2hf] / close-price [%0.2hf] for security: %s", date, offerPrice, existingPrice, sec.debugString()));

						float adjFactor = (oldRatio * existingPrice + newShares * offerPrice) / ((oldRatio + newShares) * existingPrice);
						(*m_dailyFactor)(diDst, ii) *= adjFactor;
					}
					else if (eventType == "Bonus") {
						float percentSharesIssued = newRatio / oldRatio;
						float dilution = oldRatio + (oldRatio * percentSharesIssued);
						ASSERT(dilution > 0.0f, Poco::format("[%u] - Bonus: Invalid diluation calculated for ratios: %0.2hf | %0.2hf. Security: %s", oldRatio, newRatio, sec.debugString()));
						(*m_dailyFactor)(diDst, ii) = 1.0f / dilution;
					}
					else {
						(*m_dailyFactor)(diDst, ii) *= oldRatio / newRatio;
					}
				}
			}
		}
	}

	void MS_PriceAdjustments::build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		if (!m_dailyFactor)
			buildDailyFactor(dr, dci);

		DataPtr data		= DataPtr(m_dailyFactor->extractRow(dci.di - dci.diStart));
		frame.data			= data;
		frame.dataOffset	= 0;
		frame.dataSize		= data->dataSize();
		frame.noDataUpdate	= false;

		CInfo("[%u] - %s", dci.rt.dates[dci.di], std::string(dci.def.name));
	}

} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createMS_PriceAdjustments(const pesa::ConfigSection& config) {
	return new pesa::MS_PriceAdjustments((const pesa::ConfigSection&)config);
}

