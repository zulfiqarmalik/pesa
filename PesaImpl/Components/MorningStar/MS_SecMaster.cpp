/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// MS_SecMaster.cpp
///
/// Created on: 02 May 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Poco/Timestamp.h"
#include "MS_Util.h"

#include <stdio.h>
#include <ctype.h>

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// MorningStar Security Master
	////////////////////////////////////////////////////////////////////////////
	class MS_SecMaster : public IDataLoader {
	private:
		typedef std::unordered_map<IntDate, UIntVec>    DailySecMap;
        typedef std::unordered_map<std::string, size_t> SymbolLookupMap;
        typedef std::unordered_map<std::string, bool>   ExchangeBoolMap;

		MS_Util::SecurityLookup m_securityLookup;		/// Which securities do we already have? 
		SecurityVec				m_allSecurities;		/// All the securities within the system

		StringVecMap			m_ccyCountries;			/// Maps from currency => list of countries
		StringMap				m_countryCcy;			/// Maps from country => currency

		StringVecMap			m_countryExchanges;		/// Maps from country => list of exchanges
		StringMap				m_exchangeCountry;		/// Maps from exchange => country
		StringMap				m_exchangeId;			/// Maps from ID => exchange
		StringMap				m_idExchange;			/// Maps from ID => exchange
		StringMap 				m_idToBBExchange;		/// Maps from ID => BB Exchange ID
		StringMap 				m_bbExchangeToId;		/// Maps from BB Exchange ID => ID

		std::string				m_forceInstrumentId;	/// Force an instrument id
		std::string				m_forceInstrumentExchange; /// The exchange id for forced instruments

		void 					buildSecurities(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities);
		void 					buildMSSecurities(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities);
		void 					buildBBSecurities(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities);

		void 					buildDetails(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities);

	public:
								MS_SecMaster(const ConfigSection& config);
		virtual 				~MS_SecMaster();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			buildSecurityMaster(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void 			buildInstrument(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);

		virtual void 			preDataBuild(IDataRegistry& dr, const DataCacheInfo& dci);
		virtual void 			postDataBuild(IDataRegistry& dr, const DataCacheInfo& dci);
	};

	MS_SecMaster::MS_SecMaster(const ConfigSection& config) : IDataLoader(config, "MS_SecMaster") {
		m_forceInstrumentId		= config.getResolvedString("forceInstrumentId", "");
		if (!m_forceInstrumentId.empty())
			m_forceInstrumentExchange = Util::split(m_forceInstrumentId, ".")[0];
	}

	MS_SecMaster::~MS_SecMaster() {
		CTrace("Deleting MS_SecMaster");
	}

	void MS_SecMaster::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		std::string name = config().getRequired("name");
		std::string prefix = config().getString("prefix", "");

		datasets.add(Dataset(name, {
			DataValue(prefix + "Master", DataType::kSecurityData, Security::s_version, THIS_DATA_FUNC(MS_SecMaster::buildSecurityMaster), 
				DataFrequency::kStatic, DataFlags::kAlwaysLoad | DataFlags::kUpdatesInOneGo | DataFlags::kSecurityMaster),
		}));

		Dataset secDataDS = Dataset(name);

		secDataDS.add(DataValue(prefix + "instrument", DataType::kString, 0, THIS_DATA_FUNC(MS_SecMaster::buildInstrument), DataFrequency::kStatic, DataFlags::kUpdatesInOneGo));

		datasets.add(secDataDS);

		std::string currenciesStr = config().getResolvedString("currencies", "");
		if (!currenciesStr.empty()) 
			FrameworkUtil::parseCurrencies(currenciesStr, m_ccyCountries, &m_countryCcy);

		std::string bbExchangeSuffixStr = config().getResolvedString("bbExchangeSuffix", "");
		if (!bbExchangeSuffixStr.empty())
			FrameworkUtil::parseBBExchangeSuffixes(bbExchangeSuffixStr, m_idToBBExchange, m_bbExchangeToId);

		std::string exchangesStr = config().getResolvedString("exchanges", "");
		if (!exchangesStr.empty())
			FrameworkUtil::parseExchanges(exchangesStr, m_countryExchanges, &m_exchangeCountry);

		std::string exchangeIdsStr = config().getResolvedString("exchangeIds", "");
		if (!exchangeIdsStr.empty()) {
			FrameworkUtil::parseExchangeIds(exchangeIdsStr, m_exchangeId);
			for (const auto& iter : m_exchangeId)
				m_idExchange[iter.second] = iter.first;
		}
	}

	void MS_SecMaster::postDataBuild(IDataRegistry& dr, const DataCacheInfo& dci) {
		/// We do not need this data anymore ...
		m_securityLookup.clear();
		m_allSecurities.clear();
	}

	void MS_SecMaster::preDataBuild(IDataRegistry& dr, const DataCacheInfo& dci) {
		std::string name = config().getRequired("name");
		std::string prefix = config().getString("prefix", "");

		const SecurityData* secData = dr.get<SecurityData>(nullptr, name + "." + prefix + "Master", true);

		if (secData) {
			size_t numSecurities = secData->numSecurities();
			m_allSecurities.resize(numSecurities);
			CInfo("Existing number of securities: %z", numSecurities);

			for (size_t i = 0; i < numSecurities; i++) {
				const Security& sec = secData->security(i);

				ASSERT(sec.index >= 0 && sec.index < numSecurities, "Invalid security index: " << sec.index << " - Security: " << sec.debugString());

				m_securityLookup[sec.uuidStr()] = sec.index;
				m_securityLookup[sec.cfigiStr()] = sec.index;
				m_securityLookup[sec.isinStr()] = sec.index;
				m_securityLookup[sec.figiStr()] = sec.index;
				m_securityLookup[sec.permIdStr()] = sec.index;
                m_securityLookup[sec.perfIdStr()] = sec.index;

                m_allSecurities[i] = sec;
            }
		}
	}

	void MS_SecMaster::build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
	}

	void MS_SecMaster::buildInstrument(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		StringDataPtr instrumentIds		= std::make_shared<StringData>(nullptr, m_allSecurities.size());

		for (size_t ii = 0; ii < m_allSecurities.size(); ii++) {
            const auto& sec             = m_allSecurities[ii];
			size_t index				= (size_t)sec.index;

			ASSERT(index < m_allSecurities.size(), "Invalid security index: " << index << " @ ii = : " << ii << ". Expecting no more than: " << m_allSecurities.size());
			instrumentIds->setValue(0, index, sec.instrumentId());
		}

		frame.data						= instrumentIds;
		frame.dataOffset				= 0;
		frame.dataSize					= frame.data->dataSize();
		frame.noDataUpdate				= false;
	}

	void MS_SecMaster::buildSecurities(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities) {
		buildMSSecurities(dr, dci, newSecurities);
        CDebug("[MorningStar] Number of new securities found: %z", newSecurities.size());

        buildBBSecurities(dr, dci, newSecurities);
        CDebug("[Bloomberg-IDMap] Number of new securities found: %z", newSecurities.size());
	}

	void MS_SecMaster::buildMSSecurities(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities) {
		DataInfo dinfo;
		dinfo.noCacheUpdate				= true;
		dinfo.noAssertOnMissingDataLoader = true;

		const StringData* instrument	= dr.stringData(nullptr, "preprocess.ms_instrument", (DataInfo*)&dinfo);
		const StringData* name			= dr.stringData(nullptr, "preprocess.ms_name", (DataInfo*)&dinfo);
		const StringData* country		= dr.stringData(nullptr, "preprocess.ms_country", (DataInfo*)&dinfo);
		const StringData* currency		= dr.stringData(nullptr, "preprocess.ms_currency", (DataInfo*)&dinfo);
		const StringData* cfigi			= dr.stringData(nullptr, "preprocess.ms_cfigi", (DataInfo*)&dinfo);
		const StringData* figi			= dr.stringData(nullptr, "preprocess.ms_figi", (DataInfo*)&dinfo);
		const StringData* perfId		= dr.stringData(nullptr, "preprocess.ms_perfId", (DataInfo*)&dinfo);
		const StringData* isin			= dr.stringData(nullptr, "preprocess.ms_isin", (DataInfo*)&dinfo);
		const StringData* exchangeId	= dr.stringData(nullptr, "preprocess.ms_exchangeId", (DataInfo*)&dinfo);
		const StringData* exchangeName	= dr.stringData(nullptr, "preprocess.ms_exchangeName", (DataInfo*)&dinfo);
		const StringData* issueClass	= dr.stringData(nullptr, "preprocess.ms_issueClass", (DataInfo*)&dinfo);
		const StringData* ticker		= dr.stringData(nullptr, "preprocess.ms_ticker", (DataInfo*)&dinfo);
		const StringData* permId		= dr.stringData(nullptr, "pid.id", (DataInfo*)&dinfo);
		const StringData* issuerId		= dr.stringData(nullptr, "pid.instrumentIssuerId", (DataInfo*)&dinfo);
		const StringData* quoteId		= dr.stringData(nullptr, "pid.quoteId", (DataInfo*)&dinfo);
		const StringData* quoteRIC		= dr.stringData(nullptr, "pid.quoteRIC", (DataInfo*)&dinfo);
		const StringData* instrumentSts = dr.stringData(nullptr, "pid.instrumentStatus", (DataInfo*)&dinfo);
		const StringData* instrumentName= dr.stringData(nullptr, "pid.instrumentName", (DataInfo*)&dinfo);
		const StringData* quoteCcy		= dr.stringData(nullptr, "pid.quoteCcy", (DataInfo*)&dinfo);
		//const StringData* assetClassName= dr.stringData(nullptr, "pid.assetClassName", (DataInfo*)&dinfo);
		//const StringData* assetClassDesc= dr.stringData(nullptr, "pid.assetClassDesc", (DataInfo*)&dinfo);
		//const StringData* orgId			= dr.stringData(nullptr, "pid.organisationId", (DataInfo*)&dinfo);
		//const StringData* orgName		= dr.stringData(nullptr, "pid.organisationName", (DataInfo*)&dinfo);
		//const StringData* orgStatus		= dr.stringData(nullptr, "pid.organisationStatus", (DataInfo*)&dinfo);
		//const StringData* orgHAddr		= dr.stringData(nullptr, "pid.organisationHeadquatersAddress", (DataInfo*)&dinfo);
		//const StringData* orgRAddr		= dr.stringData(nullptr, "pid.organisationRegisteredAddress", (DataInfo*)&dinfo);
		const Int64MatrixData* ipoDate	= dr.int64Data(nullptr, "pid.organisationIPODate");
		const Int64MatrixData* delistedDate = dr.int64Data(nullptr, "pid.organisationInactiveDate");

		//const StringData* isin			= dr.stringData(nullptr, "preprocess.ms_isin", (DataInfo*)&dinfo);

		bool ignoreMissingUuid			= config().getBool("ignoreMissingUuid", false);
		auto mergeStr					= config().getResolvedString("merge", "false");
		bool merge						= mergeStr == "true";

        auto matchAllStr				= config().getResolvedString("matchMultiple", "false");
        bool matchAll					= matchAllStr == "true";

        size_t count					= ticker->cols();

		for (size_t ii = 0; ii < count; ii++) {
			auto inst					= instrument->getValue(0, ii);
			auto cfigiId				= cfigi->getValue(0, ii);
			auto figiId					= figi->getValue(0, ii);
			auto uuid					= cfigiId; 
			auto tickerId				= ticker->getValue(0, ii);
			auto isinId					= isin->getValue(0, ii); 
			auto permIdValue			= permId->getValue(0, ii);
			auto perfIdValue			= perfId->getValue(0, ii); 
			auto quoteIdValue			= quoteId->getValue(0, ii); 
			auto issuerIdValue			= issuerId->getValue(0, ii);
			auto exchId                 = exchangeId->getValue(0, ii);
			//auto className				= assetClassName->getValue(0, ii);
			//auto classDesc				= assetClassDesc->getValue(0, ii);
			auto listingDate			= Util::cast<int>(ipoDate->getString(0, ii, 0));
			auto delistingDate			= Util::cast<int>(delistedDate->getString(0, ii, 0));
			auto ric					= quoteRIC->getValue(0, ii);
			SecId::Type uuidType		= SecId::kCFigi; 
			auto issueClassValue		= issueClass->getValue(0, ii);

			if (uuid.empty()) {
				uuid					= figiId;
				uuidType				= SecId::kFigi;
			}

			if (uuid.empty()) {
				uuid					= isinId;
				uuidType				= SecId::kISIN;
			}

			if (uuid.empty()) {
				uuid					= perfIdValue;
				uuidType				= SecId::kPerfId;
			}

			if (uuid.empty()) {
				uuid					= permIdValue;
				uuidType				= SecId::kPermId;
			}

			if (uuid.empty() || inst.empty())
				continue;

			/// If we are not merging securities then we first try to see whether there is a match with the UUID only
			if (!merge) {
                if (MS_Util::findIndex(m_securityLookup, uuid) >= 0)
                    continue;
			}

			auto instName				= Util::split(inst, ".")[2];

			Security lsec;
			Security& sec = lsec;
			Security* existingSec = nullptr; 
			unsigned int index = (unsigned int)(m_allSecurities.size() + newSecurities.size());

			if (merge) {
				existingSec = MS_Util::mergeWithExistingSecurity(m_securityLookup, m_allSecurities, newSecurities, uuid, cfigiId, figiId, isinId,
				        permIdValue, perfIdValue, index, false);
				if (existingSec)
					sec = *existingSec;
			}

			/// If there is no UUID or the UUID is not the same as a CFIGI id then we overwrite that
			if (sec.uuidStr().empty() || (!cfigiId.empty() && sec.uuidStr() != cfigiId)) {
				sec.setUuid(uuid);
				sec.uuidType = uuidType;
			}

			ASSERT(!sec.uuidStr().empty(), "Cannot have a security with no UUID!");

			/// If an instrument ID is being enforced, then we just deduce the instrument id from the forced prefix
			if (!m_forceInstrumentId.empty())
				inst = Poco::format("%s.%s", m_forceInstrumentId, instName);

            auto instParts = Util::split(inst, ".");
            auto exchangeId = instParts[0];

            /// IMPORTANT: OK, the instrument id is important. We prefer the instrument id for instruments which have the latest figi
			/// and cfigi provided for them ...
			if (sec.instrumentId().empty() || (existingSec && (!cfigiId.empty() && !figiId.empty() && !isinId.empty() && !tickerId.empty()))) {
			    /// First of all we check whether the exchange id matches or not!
			    bool doesExchangeIdMatch = true;

			    if (!sec.instrumentId().empty()) {
                    auto existingExchangeId = Util::split(sec.instrumentId(), ".")[0];
                    if (existingExchangeId != exchangeId) {
                        /// If the exchange doesn't match then we just ignore this
                        if (existingSec) {
                            continue;
                        }

                        doesExchangeIdMatch = false;
                    }
                }

                if (doesExchangeIdMatch) {
                    sec.setInstrumentId(inst);
                    sec.setTicker(tickerId);
                }
            }

            if (tickerId.empty())
                tickerId				= instName;

            if (sec.nameStr().empty())
				sec.setName(name->getValue(0, ii)); 

			if (sec.nameStr().empty())
				sec.setName(instrumentName->getValue(0, ii));

			if (sec.parentUuidStr().empty())
				sec.setParentUuid(issuerIdValue);

			if (!existingSec) {
				sec.setCFIGI(cfigiId);
				sec.setISIN(isinId);
				sec.setFIGI(figiId);
				sec.setPerfId(perfIdValue);
				sec.setPermId(permIdValue);
			}

			if (sec.issuerPermIdStr().empty())
				sec.setIssuerPermId(issuerIdValue);

			if (sec.quotePermIdStr().empty())
				sec.setQuotePermId(quoteIdValue);

			if (sec.ricStr().empty())
				sec.setRIC(ric);

            if (sec.tickerStr().empty())
                sec.setTicker(tickerId);

            auto bbExchangeIter = m_idToBBExchange.find(exchangeId);
            ASSERT(bbExchangeIter != m_idToBBExchange.end(), "Unable to find BB-Exchange for exchange id: " << exchangeId);

            auto bbExchange = bbExchangeIter->second;
            ASSERT(!bbExchange.empty(), "Invalid BB exchange for exchange id: " << exchangeId);

            if (sec.bbTickerStr().empty())
                sec.setBBTicker(Poco::format("%s %s", tickerId, bbExchange)); //bbTicker->getValue(0, ii) + " " + exchCode->getValue(0, ii));

            auto exchangeIter = m_idExchange.find(exchangeId);
			ASSERT(exchangeIter != m_idExchange.end(), "Unable to find exchange for id: " << exchangeId << " - Instrument: " << inst);

			auto exchange = exchangeIter->second;
			ASSERT(!exchange.empty(), "Empty exchange name for id: " << exchangeId);

			auto exchangeCountryIter = m_exchangeCountry.find(exchange);
			ASSERT(exchangeCountryIter != m_exchangeCountry.end(), "Unable to find country for exchange: " << exchange << " - Instrument: " << inst);

			auto countryCode = exchangeCountryIter->second;
			ASSERT(!countryCode.empty(), "Empty country code for exchange: " << exchange);

			auto countryCcyIter = m_countryCcy.find(countryCode);
			ASSERT(countryCcyIter != m_countryCcy.end(), "Unable to find currency code for country: " << countryCode);

			auto currencyCode = countryCcyIter->second;
			ASSERT(!currencyCode.empty(), "Empty currency for country: " << countryCode);

			/// Try to set MorningStar exchange code, otherwise get from the config
			if (sec.exchangeStr().empty())
				sec.setExchange(exchange);

			/// Try to get currency from MS, then try Reuters and then use the one in config
			if (sec.currencyStr().empty())
				sec.setCurrency(currencyCode);

			/// Try to get the country code from MS and then from the config
			if (sec.countryCode2Str().empty())
				sec.setCountryCode2(countryCode);

			/// Get issue class from MorningStar
			if (sec.issueClassStr().empty() && issueClassValue.length() < sizeof(sec.issueClass) - 1)
				sec.setIssueClass(issueClass->getValue(0, ii));

			//sec.setIssueClass(issueClass->getValue(0, ii));

			//bool adr = isForeign->getValue(0, ii) == "Y" || name->getValue(0, ii).find("(Foreign)") != std::string::npos ? true : false;
			//sec.setBBSecurityType(!adr ? secType->getValue(0, ii) : "ADR");
			//sec.setCurrency(quoteCcy->getValue(0, ii));
			//sec.setCountryCode2(country->getValue(0, ii));
			//sec.setISIN(isin->getValue(0, ii));
			//sec.setIssueType("Common Stock");

			sec.isInactive = instrumentSts->getValue(0, ii) == "instrumentStatusActive" ? false : true;

			if (sec.ipoDate == 0 && listingDate)
				sec.ipoDate = listingDate;
			if (sec.isInactive && !sec.delistingDate && delistingDate)
				sec.delistingDate = delistingDate;

			/// Add it as a new security
			if (!existingSec) 
				newSecurities.push_back(sec);

			m_securityLookup[uuid] = index;

			if (!cfigiId.empty())
				m_securityLookup[cfigiId] = index;

			if (!isinId.empty())
				m_securityLookup[isinId] = index;

			if (!figiId.empty())
				m_securityLookup[figiId] = index;

			if (!permIdValue.empty())
				m_securityLookup[permIdValue] = index;

			if (!perfIdValue.empty())
				m_securityLookup[perfIdValue] = index;
		}
	}

	void MS_SecMaster::buildBBSecurities(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities) {
		DataInfo dinfo;
		dinfo.noCacheUpdate = true;
		dinfo.noAssertOnMissingDataLoader = true;

		const StringData* tickers		= dr.stringData(nullptr, "bloomberg.ticker", (DataInfo*)&dinfo);
		const StringData* figis			= dr.stringData(nullptr, "bloomberg.figi", (DataInfo*)&dinfo);
		const StringData* cfigis		= dr.stringData(nullptr, "bloomberg.cfigi", (DataInfo*)&dinfo);
		const StringData* isins			= dr.stringData(nullptr, "bloomberg.isin", (DataInfo*)&dinfo);
		const StringData* parentFigis	= dr.stringData(nullptr, "bloomberg.parentFigi", (DataInfo*)&dinfo);
		const StringData* names			= dr.stringData(nullptr, "bloomberg.name", (DataInfo*)&dinfo);
		const StringData* marketStatuses= dr.stringData(nullptr, "bloomberg.marketStatus", (DataInfo*)&dinfo);

		auto mergeStr					= config().getResolvedString("merge", "false");
		bool merge						= mergeStr == "true";
		size_t count					= tickers->cols();

		for (size_t ii = 0; ii < count; ii++) {
			auto ticker					= tickers->getValue(0, ii);
			auto tickerParts			= Util::split(ticker, " ");
			auto figi					= figis->getValue(0, ii);
			auto cfigi					= cfigis->getValue(0, ii);
			auto isin					= isins->getValue(0, ii);
			auto parentFigi				= parentFigis->getValue(0, ii);
			auto name					= names->getValue(0, ii);
			auto marketStatus			= marketStatuses->getValue(0, ii);

            if (!ticker.length() || tickerParts.size() != 2)
				continue;

			/// This is a bloomberg convention for ticker re-assignments. If it's one of those corner
			/// cases, then we just ignore them
			if (ticker[0] >= '0' && ticker[0] <= '9') {
				CInfo("Ignoring bloomberg internal ticker: %s", ticker);
				continue;
			}

			auto bbTicker				= ticker;
			ticker						= tickerParts[0];
			auto bbExchange				= tickerParts[1];

			auto uuid					= cfigi;
			SecId::Type uuidType		= SecId::kCFigi;

            if (uuid.empty()) {
				uuid = figi;
				uuidType = SecId::kFigi;
			}

			if (uuid.empty()) {
				uuid = isin;
				uuidType = SecId::kISIN;
			}

			/// If there is no valid UUID, then just ignore this id
			if (uuid.empty())
				continue;

			std::string exchangeId;

			/// Now try to find the exchange from BB exchange id
			if (m_forceInstrumentId.empty()) {
				auto eiter				= m_bbExchangeToId.find(bbExchange);
				if (eiter == m_bbExchangeToId.end()) {
					CWarning("Unable to find BB exchange: %s [Ignoring: %s]", bbExchange, bbTicker);
					continue;
				}

				exchangeId				= eiter->second;
			}
			else 
				exchangeId				= m_forceInstrumentExchange;

			ASSERT(!exchangeId.empty(), "Unable to find exchangeId for: " << ticker << " - BB-Ticker: " << bbTicker);

			std::string instrument		= Poco::format("%s.1.%s", exchangeId, ticker);

			/// If we are not merging securities then we first try to see whether there is a match with the UUID only
			if (!merge) {
				if (MS_Util::findIndex(m_securityLookup, uuid) >= 0)
					continue;
			}

			Security lsec;
			Security& sec = lsec;
			Security* existingSec = nullptr;
			unsigned int index = (unsigned int)(m_allSecurities.size() + newSecurities.size());

			if (merge) {
				existingSec = MS_Util::mergeWithExistingSecurity(m_securityLookup, m_allSecurities, newSecurities, uuid, cfigi, figi, isin, "", "", index);
				if (existingSec)
					sec = *existingSec;
			}

			/// If there is no UUID or the UUID is not the same as a CFIGI id then we overwrite that
			if (sec.uuidStr().empty() || (!cfigi.empty() && sec.uuidStr() != cfigi)) {
				sec.setUuid(uuid);
				sec.uuidType = uuidType;
			}

			ASSERT(!sec.uuidStr().empty(), "Cannot have a security with no UUID!");

			if (sec.instrumentId().empty())
				sec.setInstrumentId(instrument);

			if (sec.nameStr().empty())
				sec.setName(name);

			if (sec.parentUuidStr().empty())
				sec.setParentUuid(parentFigi);

			if (sec.cfigiStr().empty())
				sec.setCFIGI(cfigi);

			if (sec.isinStr().empty())
				sec.setISIN(isin);

			if (sec.figiStr().empty())
				sec.setFIGI(figi);

			if (sec.tickerStr().empty())
				sec.setTicker(ticker);

			if (sec.bbTickerStr().empty())
				sec.setBBTicker(bbTicker);

			auto exchangeIter = m_idExchange.find(exchangeId);
			ASSERT(exchangeIter != m_idExchange.end(), "Unable to find exchange for id: " << exchangeId << " - Instrument: " << instrument);

			auto exchange = exchangeIter->second;
			ASSERT(!exchange.empty(), "Empty exchange name for id: " << exchangeId);

			auto exchangeCountryIter = m_exchangeCountry.find(exchange);
			ASSERT(exchangeCountryIter != m_exchangeCountry.end(), "Unable to find country for exchange: " << exchange << " - Instrument: " << instrument);

			auto countryCode = exchangeCountryIter->second;
			ASSERT(!countryCode.empty(), "Empty country code for exchange: " << exchange);

			auto countryCcyIter = m_countryCcy.find(countryCode);
			ASSERT(countryCcyIter != m_countryCcy.end(), "Unable to find currency code for country: " << countryCode);

			auto currencyCode = countryCcyIter->second;
			ASSERT(!currencyCode.empty(), "Empty currency for country: " << countryCode);

			/// Try to set MorningStar exchange code, otherwise get from the config
			if (sec.exchangeStr().empty())
				sec.setExchange(exchange);

			/// Try to get currency from MS, then try Reuters and then use the one in config
			if (sec.currencyStr().empty())
				sec.setCurrency(currencyCode);

			/// Try to get the country code from MS and then from the config
			if (sec.countryCode2Str().empty())
				sec.setCountryCode2(countryCode);

			sec.isInactive = marketStatus == "ACTV" ? false : true;

			/// Add it as a new security
			if (!existingSec)
				newSecurities.push_back(sec);

			m_securityLookup[uuid] = index;

			if (!cfigi.empty())
				m_securityLookup[cfigi] = index;

			if (!isin.empty())
				m_securityLookup[isin] = index;

			if (!figi.empty())
				m_securityLookup[figi] = index;
		}
	}

	void MS_SecMaster::buildSecurityMaster(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
        SecurityVec newSecurities;
        buildSecurities(dr, dci, newSecurities);

        CDebug("Number of new securities found: %z", newSecurities.size());

        size_t numExisting = m_allSecurities.size();
        size_t numNew = newSecurities.size();
        size_t numFiltered = numNew;

        /// Assign the correct indexes to the securities
        for (auto& sec : newSecurities) {
            sec.index = (Security::Index)m_allSecurities.size();
            m_allSecurities.push_back(sec);
        }

        try {
            CDebug("Number of new securities: %z - After filteration: %z [Existing: %z, DataSize: %z]", numNew, numFiltered, numExisting, m_allSecurities.size() * sizeof(Security));

            /// otherwise we create a new data for these
            frame.data = std::make_shared<SecurityData>(dci.universe, m_allSecurities, dci.def);
            frame.dataOffset = 0;
            frame.dataSize = frame.data->dataSize();
            frame.noDataUpdate = false;
        }
        catch (const std::exception& e) {
            Util::reportException(e);
            Util::exit(1);
        }
    }

    void MS_SecMaster::buildDetails(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities) {
        CDebug("Getting detailed information about the securities!");
    }
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createMS_SecMaster(const pesa::ConfigSection& config) {
	return new pesa::MS_SecMaster((const pesa::ConfigSection&)config);
}

