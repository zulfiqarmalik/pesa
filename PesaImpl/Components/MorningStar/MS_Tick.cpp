/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// MS_Tick.cpp
///
/// Created on: 04 Oct 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Poco/Timestamp.h"
#include "Poco/String.h" 
#include "./MS_Util.h"
#include "PesaImpl/Components/Generic/GenericDataLoaderJSON.h"
#include "Helper/JSON/gason.hpp"

namespace pesa {
	class MS_Tick : public GenericDataLoaderJSON { 
	private:
		int						preCacheDayAsyncJSON(const RuntimeData* rt, IntDay di, DataDefinition def, DataPtr customMetadata);

	public:
								MS_Tick(const ConfigSection& config);
		virtual 				~MS_Tick();
	};

	MS_Tick::MS_Tick(const ConfigSection& config) 
		: GenericDataLoaderJSON(config, "MS_Tick") {
	}

	MS_Tick::~MS_Tick() {
		CTrace("Deleting MS_Tick");
	}

	int MS_Tick::preCacheDayAsyncJSON(const RuntimeData* rt, IntDay di, DataDefinition def, DataPtr customMetadata) {
		bool isHistorical = false;
		std::string zipFilename = getFilename(*rt, di, isHistorical);

		if (zipFilename.empty())
			return -1;

		std::string contentsName = config().getResolvedRequiredString("zipFilename", (unsigned int)rt->dates[di]);
		char* contents = nullptr;
		size_t contentsLength = Util::readEntireTextFileFromZip(zipFilename, contentsName, &contents);

		ASSERT(!contents || !contentsLength, Poco::format("Unable to find any contents in file: %s@%s", zipFilename, contentsName));

		/// Now parse the json
		char* startPtr = contents;
		char* endPtr = nullptr;

		gason::JsonAllocator jsonAllocator;
		gason::JsonValue jsonResult;
		gason::JsonParseStatus jsonStatus = gason::Json::parse(startPtr, &endPtr, &jsonResult, jsonAllocator);

		ASSERT(jsonStatus == gason::JSON_PARSE_OK, Poco::format("Unable to parse json in file: %s@%s", zipFilename, contentsName));

		size_t iiResult = 0;
		for (auto iter = gason::begin(jsonResult); iter != gason::end(jsonResult); iter++, iiResult++) {
			auto dataArray = iter->value;
			size_t iiData = 0;

			for (auto dataIter = gason::begin(dataArray); dataIter != gason::end(dataArray); dataIter++, iiData++) {
				auto secDataItem			= dataIter->value;
				std::string type			= secDataItem.child("type").toString();
				std::string exchangeId		= secDataItem.child("exchangeid").toString();
				std::string symbol			= secDataItem.child("symbol").toString();
				std::string instrumentId	= Poco::format("%s.%s.%s", exchangeId, type, symbol);

				ASSERT(!type.empty() && !exchangeId.empty() && !symbol.empty(), Poco::format("Invalid data item: %s@%s [%z.%z]", zipFilename, contentsName, iiResult, iiData));

				/// Now we try to find the  
			}
		}

		//auto indicesList = jsonResult.child("results");
		//auto iter = gason::begin(indicesList);


		return 0;
	}

} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
//EXPORT_FUNCTION pesa::IComponent* createMS_Tick(const pesa::ConfigSection& config) {
//	return new pesa::MS_Tick((const pesa::ConfigSection&)config);
//}
//
