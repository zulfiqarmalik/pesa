/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// MS_Util.cpp
///
/// Created on: 13 Oct 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////
 
#include "MS_Util.h"

namespace pesa {
	bool MS_Util::isPriceAffectingEvent(std::string eventType) {
		if (eventType == "Bonus" || eventType == "bonus" ||
			eventType == "Consolidation" || eventType == "Consolidation" ||
			eventType == "Sub Division" || eventType == "sub division" || eventType == "Sub-Division" || eventType == "sub-division" ||
			eventType == "DeMerger" || eventType == "Demerger" || eventType == "demerger" ||
			eventType == "Rights Issue" || eventType == "Rights issue" || eventType == "rights issue" ||
			eventType == "Trading Currency Change" || eventType == "Trading Currency change" || eventType == "Trading currency change" || eventType == "trading currency change" ||
			eventType == "Arrangement" || eventType == "arrangement")
			return true;

		return false;
	}

	int MS_Util::findIndex(const SecurityLookup& securityLookup, std::string uuid, std::string cfigiId, std::string figiId, std::string isinId, std::string permId, std::string perfId) {
#define SEC_LOOKUP_MATCH(id)	if (iter == securityLookup.end() && !id.empty()) { \
									iter = securityLookup.find(id); \
									if (iter != securityLookup.end()) { \
										index = (int)iter->second; \
										return index; \
									} \
								}

		auto iter = securityLookup.end();
		int index = -1;

		SEC_LOOKUP_MATCH(uuid);
		SEC_LOOKUP_MATCH(cfigiId);
		SEC_LOOKUP_MATCH(figiId);
		SEC_LOOKUP_MATCH(isinId);
		SEC_LOOKUP_MATCH(permId);
		SEC_LOOKUP_MATCH(perfId);

#undef SEC_LOOKUP_MATCH

		return index;
	}

	int MS_Util::findIndexAll(const SecurityLookup& securityLookup, std::string uuid, std::string cfigiId, std::string figiId, std::string isinId, std::string permId, std::string perfId) {
#define SEC_LOOKUP_MATCH_ALL(id) if (!id.empty()) { \
									auto iter = securityLookup.find(id); \
									if (iter != securityLookup.end() && (index < 0 || index == (int)iter->second)) \
										index = (int)iter->second; \
									else \
										return -1; \
								}

		int index = -1;

		SEC_LOOKUP_MATCH_ALL(uuid);
		SEC_LOOKUP_MATCH_ALL(cfigiId);
		SEC_LOOKUP_MATCH_ALL(figiId);
		SEC_LOOKUP_MATCH_ALL(isinId);
		SEC_LOOKUP_MATCH_ALL(permId);
		SEC_LOOKUP_MATCH_ALL(perfId);

#undef SEC_LOOKUP_MATCH_ALL

		return index;
	}

	Security* MS_Util::mergeWithExistingSecurity(const SecurityLookup& securityLookup, SecurityVec& allSecurities, 
		SecurityVec& newSecurities, const std::string& uuid, const std::string& cfigiId, const std::string& figiId, 
		const std::string& isinId, const std::string& permId, const std::string& perfId, unsigned int& secIndex, bool matchAll /* = false */) {
		int iindex;

		if (!matchAll) {
			if (cfigiId.empty())
				iindex = findIndex(securityLookup, uuid, cfigiId, figiId, isinId, permId, perfId);
			else
				iindex = findIndex(securityLookup, uuid, "", "", "", "", "");
		}
		else {
			iindex = findIndexAll(securityLookup, uuid, cfigiId, figiId, isinId, permId, perfId);
		}

		/// If we couldn't find the index ...
		if (iindex < 0)
			return nullptr;

		size_t index = (size_t)iindex;
		Security* sec = nullptr;

		if (index < allSecurities.size())
			sec = &allSecurities[index];
		else {
			ASSERT((index - allSecurities.size()) < newSecurities.size(), "Invalid index for existing security retrieved: " << index << " - All sec count: " << allSecurities.size() << " - New sec count: " << newSecurities.size());
			index -= allSecurities.size();
			sec = &newSecurities[index];
		}

		ASSERT(sec, "Unable to find security at index: " << iindex);

		if (sec->cfigiStr().empty() && !cfigiId.empty()) {
			sec->setUuid(cfigiId);
			sec->setCFIGI(cfigiId);
			sec->uuidType = SecId::kCFigi;
		}

		if (sec->figiStr().empty())
			sec->setFIGI(figiId);

		if (sec->isinStr().empty())
			sec->setISIN(isinId);

		if (sec->permIdStr().empty())
			sec->setPermId(permId);

		if (sec->perfIdStr().empty())
			sec->setPerfId(perfId);

		/// Always return the original index
		secIndex = (unsigned int)iindex;

		return sec;
	}


} /// namespace pesa
