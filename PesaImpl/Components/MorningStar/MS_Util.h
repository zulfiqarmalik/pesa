/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// MS_Util.h
///
/// Created on: 13 Oct 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Poco/Timestamp.h"
#include "Poco/String.h"

namespace pesa {
	class MS_Util { 
	public:
		typedef std::unordered_map<std::string, Security::Index>  SecurityLookup;

		static bool						isPriceAffectingEvent(std::string eventType);

		static int						findIndex(const SecurityLookup& securityLookup, std::string uuid, std::string cfigiId = "", std::string figiId = "",
										std::string isinId = "", std::string permId = "", std::string perfId = "");
	
		static int						findIndexAll(const SecurityLookup& securityLookup, std::string uuid, std::string cfigiId = "", std::string figiId = "",
										std::string isinId = "", std::string permId = "", std::string perfId = "");
	
		static Security*				mergeWithExistingSecurity(const SecurityLookup& securityLookup, SecurityVec& allSecurities, SecurityVec& newSecurities, const std::string& uuid, 
										const std::string& cfigiId, const std::string& figiId, const std::string& isinId, const std::string& permId, const std::string& perfId, unsigned int& secIndex, bool matchAll = false);
	};
} /// namespace pesa
