/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Quandl_AdjFactor.cpp
///
/// Created on: 15 Nov 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Poco/String.h"
#include "Framework/Helper/FileDataLoader.h"
#include "PesaImpl/Components/SP/SpGeneric.h"
#include "PesaImpl/Components/SP/SpRest.h"
#include "PesaImpl/Components/Quandl/Quandl_JsonParser.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Quandl_AdjFactor data
	////////////////////////////////////////////////////////////////////////////
	class Quandl_AdjFactor : public IDataLoader {
	private:
		IntDate					m_updateDate;				/// The last date for which to use the Sharadar data
		IntDay					m_maxDaysToUse = 5;			/// Max days to use the EOD prices to calculate adjustment factor

	public:
								Quandl_AdjFactor(const ConfigSection& config);
		virtual 				~Quandl_AdjFactor();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {}
		virtual void 			buildAdjFactor(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);
		virtual void 			buildAdjVolumeFactor(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
	};

	Quandl_AdjFactor::Quandl_AdjFactor(const ConfigSection& config) : IDataLoader(config, "QNDL:ADJ") {
		m_updateDate			= config.getInt("updateDate", 0, true);
		m_maxDaysToUse			= config.getInt("maxDaysToUse", m_maxDaysToUse);
	}

	Quandl_AdjFactor::~Quandl_AdjFactor() {
		CTrace("Deleting Quandl_AdjFactor!");
	}

	void Quandl_AdjFactor::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		std::string datasetName = config().getRequired("datasetName");

		datasets.add(Dataset(datasetName, {
			DataValue("f_adj", DataType::kFloat, DataType::size(DataType::kFloat), THIS_DATA_FUNC(Quandl_AdjFactor::buildAdjFactor), DataFrequency::kDaily, 0),
			DataValue("f_adjVolume", DataType::kFloat, DataType::size(DataType::kFloat), THIS_DATA_FUNC(Quandl_AdjFactor::buildAdjVolumeFactor), DataFrequency::kDaily, 0),
		}));
	}

	void Quandl_AdjFactor::buildAdjFactor(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		/// We only use this for the last 5 days
		auto numStocks = dci.universe->size(dci.di);
		auto* adjFactor = new FloatMatrixData(dci.universe, dci.def, 1, numStocks);

		if (dci.di >= dci.diEnd - m_maxDaysToUse) {
			auto* closePrice = dr.floatData(dci.universe, "quandlWikiEOD.closePrice", true);
			auto* adjClosePrice = dr.floatData(dci.universe, "quandlWikiEOD.prelim_adjClosePrice", true);

			adjFactor->row(0) = adjClosePrice->row(dci.di) / closePrice->row(dci.di);
			CDebug("[%u] f_adj: closePrice / adjClosePrice", dci.rt.dates[dci.di]);
		}
		else {
			adjFactor->setMemory(1.0f);
			CDebug("[%u] f_adj: 1.0", dci.rt.dates[dci.di]);
		}

		frame.data = FloatMatrixDataPtr(adjFactor);
		frame.dataOffset = 0;
		frame.dataSize = adjFactor->dataSize();
	}

	void Quandl_AdjFactor::buildAdjVolumeFactor(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		/// We only use this for the last 5 days
		auto numStocks = dci.universe->size(dci.di);
		auto* adjFactor = new FloatMatrixData(dci.universe, dci.def, 1, numStocks);

		if (dci.di >= dci.diEnd - m_maxDaysToUse) {
			auto* volume = dr.floatData(dci.universe, "quandlWikiEOD.volume", true);
			auto* adjVolume = dr.floatData(dci.universe, "quandlWikiEOD.prelim_adjVolume", true);

			adjFactor->row(0) = volume->row(dci.di) / adjVolume->row(dci.di);

			CDebug("[%u] f_adjVolume: volume / adjVolume", dci.rt.dates[dci.di]);
		}
		else {
			adjFactor->setMemory(1.0f);
			CDebug("[%u] f_adjVolume: 1.0", dci.rt.dates[dci.di]);
		}

		frame.data = FloatMatrixDataPtr(adjFactor);
		frame.dataOffset = 0;
		frame.dataSize = adjFactor->dataSize();
	}

	//void Quandl_AdjFactor::initData(IDataRegistry& dr, const pesa::DataCacheInfo& dci) {
	//	ASSERT(dci.universe, "Invalid universe");
	//	m_diCacheStart = dci.di;

	//	m_diUpdateDate = dci.rt.getDateIndex(m_updateDate);
	//	size_t numDays = m_diUpdateDate - m_diCacheStart;
	//	size_t numStocks = dci.universe->size(dci.diEnd);

	//	for (const auto& name : m_datasetNames) {
	//		auto data = FloatMatrixDataPtr(new FloatMatrixData(nullptr, DataDefinition(name), numDays, numStocks));
	//		//data->setMemory(std::nanf(""));
	//		m_dataCache[name] = std::move(data);
	//	}
	//}

	//void Quandl_AdjFactor::buildEODData(IDataRegistry& dr, const pesa::DataCacheInfo& dci) {
	//	//IntDay diStart = m_diUpdateDate + 1;
	//	//IntDate startDate = dci.rt.dates[diStart];
	//	//IntDate endDate = dci.rt.dates[dci.diEnd];

	//	//if (diStart > dci.diEnd)
	//	//	return;

	//	///// We DON'T WANT to use this data if it's more than 10 days stale
	//	//ASSERT(dci.diEnd - diStart < s_maxEODData, "We do not use Quandl EOD data for more than 10 days in history since it's not point in time. Currently required: " << dci.diEnd - diStart);

	//	///// If we have already exceeded the start date then we don't need to do anything else here 
	//	///// from the Sharadar data file
	//	//if (startDate > endDate)
	//	//	return;

	//	//CDebug("Building Quandl EOD WIKI prices for dates: [%u, %u]", startDate, endDate);

	//	//std::string serverUrl(config().getResolvedRequiredString("serverUrl"));

	//	///// Now we make a series of rest requests
	//	//SpRestBatch batch(config(), nullptr, nullptr, m_minfo);
	//	//std::string keyExtra;

	//	//for (IntDay di = diStart; di <= dci.diEnd; di++) {
	//	//	//SpGeneric::preCacheDay(dr, dci, di, keyExtra);
	//	//	//IntDate date = dci.rt.dates[di];
	//	//	//CDebug("WIKI EOD Precaching date: %u", date);

	//	//	//StringMap params;
	//	//	//params["date"] = Util::cast(date);
	//	//	//params["api_key"] = parentConfig()->macros().getMacro("QUANDL_API_KEY");

	//	//	//minfo.jsonParser = new Quandl_JsonParser();

	//	//	//batch.getDataAsync(dci, params, nullptr, &minfo);
	//	//}

	//	////batch.wait();
	//	////SpRest::ResultVec results = batch.results();

	//	////for (SpRest::Result& result : results) {
	//	////}
	//}

	//void Quandl_AdjFactor::buildSharadarData(IDataRegistry& dr, const pesa::DataCacheInfo& dci) {
	//	IntDay diStart = m_diCacheStart;
	//	IntDate startDate = dci.rt.dates[diStart];
	//	IntDate endDate = m_updateDate;

	//	/// If we have already exceeded the start date then we don't need to do anything else here 
	//	/// from the Sharadar data file
	//	if (startDate > endDate)
	//		return;

	//	CDebug("Building Sharadar prices for dates: [%u, %u]", startDate, endDate);

	//	helper::FileDataLoader loader(m_pricesFilename.toString(), ",", false, true);
	//	size_t lineNumber = 0;
	//	std::string ignoreTicker;
	//	const Security* psec = nullptr;
	//	unsigned int numHandled = 0;
	//	IntDate handlingStartDate = 0;
	//	IntDate handlingEndDate = 0;

	//	while (!loader.eof()) {
	//		std::string line(loader.readLine());

	//		/// This ticker needs to be ignored
	//		if (!ignoreTicker.empty() && ignoreTicker == line.substr(0, ignoreTicker.size()))
	//			continue;

	//		/// OK, now the ticker matches, we can parse it 
	//		StringVec info(Util::split(line, ","));
	//		const auto& ticker = info[0]; 
	//		const auto* sec = dci.universe->mapUuid(ticker);

	//		/// We couldn't find this security, so we're just going to ignore it
	//		if (!sec) {
	//			CWarning("Ignoring ticker: %s", ticker);
	//			ignoreTicker = ticker;
	//			numHandled = handlingStartDate = handlingEndDate = 0;
	//			psec = nullptr;
	//			continue;
	//		} 
	//		else {
	//			if (sec != psec && psec) {
	//				CDebug("[%s/%u] Num handled: %u [%u, %u]", psec->tickerStr(), psec->index, numHandled, handlingStartDate, handlingEndDate);
	//				numHandled = handlingStartDate = handlingEndDate = 0;
	//			}

	//			psec = sec;
	//			ignoreTicker = "";
	//		}

	//		IntDate date = Util::strDateToIntDate(std::move(info[1]));

	//		if (date < startDate || date > endDate) 
	//			continue;

	//		if (!handlingStartDate)
	//			handlingStartDate = date;
	//		handlingEndDate = date;
	//		numHandled++;

	//		IntDay diDate = dci.rt.getDateIndex(date);
	//		IntDay di = diDate - diStart;

	//		(*m_dataCache["openPrice"])(di, sec->index) 	= Util::cast<float>(info[2]);
	//		(*m_dataCache["highPrice"])(di, sec->index) 	= Util::cast<float>(info[3]);
	//		(*m_dataCache["lowPrice"])(di, sec->index) 		= Util::cast<float>(info[4]);
	//		(*m_dataCache["closePrice"])(di, sec->index) 	= Util::cast<float>(info[5]);
	//		(*m_dataCache["volume"])(di, sec->index) 		= Util::cast<float>(info[6]);
	//		(*m_dataCache["f_adj"])(di, sec->index) 		= 1.0f;
	//		(*m_dataCache["f_adjVolume"])(di, sec->index) 	= 1.0f;
	//	}
	//}

	//void Quandl_AdjFactor::buildData(IDataRegistry& dr, const pesa::DataCacheInfo& dci) {
	//	if (!m_dataCache.empty())
	//		return;

	//	/// Init the data cache
	//	initData(dr, dci);

	//	/// OK, we now build the sharadar data ...
	//	buildSharadarData(dr, dci);

	//	/// And then we build the EOD WIKI data ...
	//	buildEODData(dr, dci);
	//}

	//void Quandl_AdjFactor::buildDaily(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
	//	//buildData(dr, dci);

	//	if (dci.rt.datesdci.di <= m_diUpdateDate) {
	//		ASSERT(m_diCacheStart <= dci.di, "Invalid data requested. Initially we deduced the start of di to be: " << m_diCacheStart << ", but then the system requested data for index: " << dci.di);

	//		/// Ok, now we just serve the data from the cache ...
	//		std::string dataName = dci.def.name;
	//		IntDay diOffset = dci.di - m_diCacheStart;
	//		auto cacheIter = m_dataCache.find(dataName);

	//		ASSERT(cacheIter != m_dataCache.end(), "Unknown data requested: " << dataName);

	//		FloatMatrixDataPtr data = cacheIter->second;
	//		ASSERT(diOffset >= 0 && diOffset < (IntDay)data->rows(), "Invalid data request for di: " << dci.di << ". The data was prepared for date range: [" << m_diCacheStart << ", " << dci.diEnd << "]");

	//		// Ok, now we give the data back ...
	//		frame.data			= data;
	//		frame.dataOffset	= diOffset * data->rowSize();
	//		frame.dataSize		= data->rowSize();
	//		frame.noDataUpdate	= false;
	//	}
	//	else 
	//		SpGeneric::buildDaily(dr, dci, frame);
	//}

	//void Quandl_AdjFactor::postDataBuild(IDataRegistry& dr, const pesa::DataCacheInfo& dci) {
	//	m_dataCache.clear();
	//}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createQuandl_AdjFactor(const pesa::ConfigSection& config) {
	return new pesa::Quandl_AdjFactor((const pesa::ConfigSection&)config);
}

