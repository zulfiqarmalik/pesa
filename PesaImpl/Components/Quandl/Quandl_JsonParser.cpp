/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Quandl_JsonParser.cpp
///
/// Created on: 22 Nov 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Quandl_JsonParser.h"

namespace pesa {
	static Data* createDataPtr(const std::string& name, const std::string& type, size_t count) {
		if (type == "float" || type.find("BigDecimal") == 0 || type == "double" || type == "int" || type == "Integer") 
			return new FloatMatrixData(nullptr, DataDefinition(name, DataType::kFloat, 4, DataFrequency::kDaily, 0), 1, count);
		else if (type == "Date" || type == "DateTime")
			return new Int64MatrixData(nullptr, DataDefinition(name, DataType::kDateTime, 4, DataFrequency::kDaily, 0), 1, count);

		return new StringData(nullptr, count);
	}

	int Quandl_JsonParser::parseResults(SpRest* rcall, SpRest::Result& result) {
		gason::JsonValue jsonResult = rcall->jsonResult();
		StringVec ctypes, cnames;
		std::vector<Data*> dptrs;
		auto datatable	= jsonResult.child("datatable");
		auto data		= datatable.child("data");
		auto columns	= datatable.child("columns");

		/// Need to optimise this!
		size_t count	= 0;
		for (gason::JsonIterator iter = gason::begin(data); iter != gason::end(data); iter++) 
			count++;

		for (gason::JsonIterator iter = gason::begin(columns); iter != gason::end(columns); iter++) {
			std::string cname(iter->value.child("name").toString());
			std::string ctype(iter->value.child("type").toString());

			result.columnDefs[cname] = ctype;
			Data* dptr = createDataPtr(cname, ctype, count);

			result.data[cname] = DataPtr(dptr);

			dptrs.push_back(dptr);
			cnames.push_back(std::move(cname));
			ctypes.push_back(std::move(ctype));
		}

		/// Now we do the conversion ...
		size_t ii = 0;

		for (gason::JsonIterator iter = gason::begin(data); iter != gason::end(data); iter++, ii++) {
			auto item				= iter->value;
			size_t jj				= 0;

			for (gason::JsonIterator itemIter = begin(item); itemIter != end(item); itemIter++, jj++) {
				const auto& cname	= cnames[jj];
				const auto& ctype	= ctypes[jj];
				Data* dptr			= dptrs[jj];
				auto value			= itemIter->value;
				const auto& def		= dptr->def();

				if (def.isFloat()) 
					(*static_cast<FloatMatrixData*>(dptr))(0, ii) = SpRest::jsonFloatValue(value);
				else if (def.itemType == DataType::kDateTime) {
					auto strDate = SpRest::jsonStringValue(value);
					auto dateTime = Util::strDateTimeToUTCDateTime(strDate);
					(static_cast<Int64MatrixData*>(dptr))->setDTValue(0, ii, dateTime);
				}
				else if (def.itemType == DataType::kString)
					(static_cast<StringData*>(dptr))->setValue(0, ii, SpRest::jsonStringValue(value));
				else
					ASSERT(false, "Currently unsupported: " << DataType::toString(def.itemType));
			}
		}

		return 0;
	}
} /// namespace pesa
