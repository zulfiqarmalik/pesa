/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Quandl_JsonParser.h
///
/// Created on: 22 Nov 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "PesaImpl/Components/SP/SpRest.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Quandl JSON parser
	////////////////////////////////////////////////////////////////////////////
	class Quandl_JsonParser : public SpRest::IParser {
	public:
		int						parseResults(SpRest* rcall, SpRest::Result& result);
	};
} /// namespace pesa

