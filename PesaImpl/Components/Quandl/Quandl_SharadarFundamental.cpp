/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Quandl_SharadarFundamental.cpp
///
/// Created on: 03 Feb 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Poco/String.h"
#include "Framework/Helper/FileDataLoader.h"
#include "PesaImpl/Components/SP/SpGeneric.h"
#include "PesaImpl/Components/SP/SpRest.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Quandl_SharadarFundamental data
	////////////////////////////////////////////////////////////////////////////
	class Quandl_SharadarFundamental : public SpGeneric {
	private:
		virtual std::string		makeKey(IntDate date, const std::string& keyExtra) const;

	public:
								Quandl_SharadarFundamental(const ConfigSection& config);
		virtual 				~Quandl_SharadarFundamental();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
	};

	Quandl_SharadarFundamental::Quandl_SharadarFundamental(const ConfigSection& config) 
		: SpGeneric(config) {
		SpGeneric::setLogChannel("QNDL:SF1");

		SpGeneric::m_symbolColName = "ticker";
		SpGeneric::m_timestampColName = "datekey";
		SpGeneric::m_maxCacheDaysToKeep = 120;
	}

	Quandl_SharadarFundamental::~Quandl_SharadarFundamental() {
		CTrace("Deleting Quandl_SharadarFundamental!");
	}

	std::string Quandl_SharadarFundamental::makeKey(IntDate date, const std::string& keyExtra) const {
		IntDate lastFiscalQuarterEndDate = SpGeneric::getLastFiscalQuarterDate(date);
		return SpGeneric::makeKeyDirect(lastFiscalQuarterEndDate, keyExtra);
	}

} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createQuandl_SharadarFundamental(const pesa::ConfigSection& config) {
	return new pesa::Quandl_SharadarFundamental((const pesa::ConfigSection&)config);
}

