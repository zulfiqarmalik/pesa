/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Quandl_SharadarNextED.cpp
///
/// Created on: 05 Feb 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Poco/String.h"
#include "Framework/Helper/FileDataLoader.h"
#include "PesaImpl/Components/SP/SpGeneric.h"
#include "PesaImpl/Components/SP/SpRest.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Quandl_SharadarNextED data
	////////////////////////////////////////////////////////////////////////////
	class Quandl_SharadarNextED : public IDataLoader {
	public:
								Quandl_SharadarNextED(const ConfigSection& config);
		virtual 				~Quandl_SharadarNextED();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, pesa::Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);
	};

	Quandl_SharadarNextED::Quandl_SharadarNextED(const ConfigSection& config) 
		: IDataLoader(config, "QNDL:SF1") {
	}

	Quandl_SharadarNextED::~Quandl_SharadarNextED() {
		CTrace("Deleting Quandl_SharadarNextED!");
	}

	void Quandl_SharadarNextED::initDatasets(IDataRegistry& dr, pesa::Datasets& datasets) {
		std::string datasetName = config().getString("datasetName", "SF1");

		datasets.add(Dataset(datasetName, {
			DataValue("Next_ED", DataType::kIndexedString, 0, nullptr, DataFrequency::kDaily, 0)
		}));
	}

	void Quandl_SharadarNextED::build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		const std::string dataName = "SF1.datekey";
		Int64MatrixData* dates = dr.int64Data(dr.getSecurityMaster(), dataName);
		ASSERT(dates, "Unable to load source data: " << dataName);

		/// Now we just convert it to a string
		size_t numStocks = dci.universe->size(dci.di);
		StringDataPtr nextED = std::make_shared<ShortIndexedStringData>(dci.universe, numStocks);

		for (size_t ii = 0; ii < numStocks; ii++) {
			/// TODO: Fix why this std::max needs to be there!
			auto date = dates->getDTValue(std::min(dci.di, (IntDay)dates->rows() - 1), ii);
			std::string dateStr = Util::dateToStr(date, "-");
			nextED->setValue(0, ii, dateStr);
		}

		CInfo("Updated data: [%-8u] - %s", dci.rt.dates[dci.di], dci.def.toString(dci.ds));

		frame.data = nextED;
		frame.dataOffset = 0;
		frame.dataSize = nextED->dataSize();
		frame.noDataUpdate = false;
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createQuandl_SharadarNextED(const pesa::ConfigSection& config) {
	return new pesa::Quandl_SharadarNextED((const pesa::ConfigSection&)config);
}

