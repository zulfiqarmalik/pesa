/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Reuters_PermId.cpp
///
/// Created on: 02 Jul 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "PesaImpl/Components/SP/SpRest.h"
#include "Framework/Helper/TurtleReader.h"

#include "sparsepp/spp.h"

using namespace pesa::helper;

namespace pesa {

	////////////////////////////////////////////////////////////////////////////

	typedef std::vector<uint64_t>							UInt64Vec;
	typedef std::unordered_map<std::string, bool>			ExchangeMap;
	typedef spp::sparse_hash_map<std::string, SizeVec>		TickerMap;
	typedef spp::sparse_hash_map<std::string, std::string>	TickerNameMap;
	typedef std::unordered_map<uint64_t, TurtleReader::CNodeP> PermIdMap;
	typedef std::unordered_map<std::string, UInt64Vec>		TickerPermIdMap;
	typedef std::map<std::string, DataPtr>					StringDataMap;

	////////////////////////////////////////////////////////////////////////////
	/// Reuters_PermId: PermID parser for securities
	////////////////////////////////////////////////////////////////////////////
	class Reuters_PermId : public IDataLoader {
	private:
		struct QuoteData {
			uint64_t					quoteId = 0;
			TurtleReader::CNodeP		quote = nullptr;
			std::string					quoteName;

			uint64_t					currencyId = 0;
			TurtleReader::CNodeP		currency = nullptr;

			uint64_t					instrumentId = 0;
			TurtleReader::CNodeP		instrument = nullptr;
			std::string					instrumentName;

			uint64_t					assetClassId = 0;
			TurtleReader::CNodeP		assetClass = nullptr;

			uint64_t					organisationId = 0;
			TurtleReader::CNodeP		organisation = nullptr;
			std::string					organisationName;

			uint64_t					sectorId = 0;
			TurtleReader::CNodeP		sector = nullptr;

			uint64_t					industryGroupId = 0;
			TurtleReader::CNodeP		industryGroup = nullptr;

			uint64_t					industryId = 0;
			TurtleReader::CNodeP		industry = nullptr;

			uint64_t					subIndustryId = 0;
			TurtleReader::CNodeP		subIndustry = nullptr;

			std::string					ticker;
		};

		//////////////////////////////////////////////////////////////////////////

		static const unsigned char		s_hasNameId = 1;
		static const unsigned char		s_hasExchangeCodeId = 2;
		static const unsigned char		s_hasExchangeTickerId = 3;
		static const unsigned char		s_hasRicId = 4;
		static const unsigned char		s_isQuoteOfId = 5;
		static const unsigned char		s_isQuotedInId = 6;
		static const unsigned char		s_HeadquartersAddressId = 7;
		static const unsigned char		s_hasActivityStatusId = 8;
		static const unsigned char		s_isDomiciledInId = 9;
		static const unsigned char		s_nameId = 10;
		static const unsigned char		s_hasAssetClassId = 11;
		static const unsigned char		s_hasInstrumentStatusId = 12;
		static const unsigned char		s_hasPrimaryQuoteId = 13;
		static const unsigned char		s_isIssuedById = 14;
		static const unsigned char		s_decimalPlacesId = 15;
		static const unsigned char		s_iso4217Id = 16;
		static const unsigned char		s_iso4217NumericId = 17;
		static const unsigned char		s_prefLabelId = 18;
		static const unsigned char		s_BusinessClassificationCodeId = 19;
		static const unsigned char		s_broaderId = 20;
		static const unsigned char		s_labelId = 21;
		static const unsigned char		s_organisationNameId = 22;
		static const unsigned char		s_hasMicId = 23;
		static const unsigned char		s_RegisteredAddressId = 24;
		static const unsigned char		s_isIncorporatedInId = 25;
		static const unsigned char		s_hasIPODateId = 26;
		static const unsigned char		s_hasInactiveDateId = 27;
		static const unsigned char		s_hasPrimaryBusinessSectorId = 28;
		static const unsigned char		s_hasPrimaryEconomicSectorId = 29;
		static const unsigned char		s_hasPrimaryIndustryGroupId = 30;

		ExchangeMap						m_exchanges;					/// The exchanges that we're dealing with

		TurtleReader::FileDetailsPtr 	m_organisations;				/// The organisations that we've loaded
		TurtleReader::FileDetailsPtr 	m_assetClasses;					/// The asset classes that we've loaded
		TurtleReader::FileDetailsPtr 	m_currencies;					/// The currencies that we've loaded
		TurtleReader::FileDetailsPtr 	m_industries;					/// The industries that we've loaded

		TurtleReader::FileDetailsPtr 	m_instruments;					/// The instruments that we've loaded

		TurtleReader::FileDetailsPtr 	m_quotes;						/// The quotes that we've loaded
		TickerPermIdMap					m_quoteTickersLUT;				/// Quotes tickers lookup table

		//TurtleReaderPtrVec				m_readers;						/// The readers that we have

		BinTree							m_tree;							/// Binary search tree

		const StringData*				m_tickers;						/// The input tickers
		const StringData*				m_names;						/// The input tickers names
		const StringData*				m_figis;						/// The figis

		StringDataMap					m_data;							/// The data cache that we've built up

		std::vector<DataDefinition>		m_dataDefs = {
			/// General information
			DataDefinition("id", DataType::kString, 0, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo),

			DataDefinition("sector", DataType::kIndexedString, 0, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo),
			DataDefinition("sectorId", DataType::kIndexedString, 0, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo),
			DataDefinition("industryGroup", DataType::kIndexedString, 0, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo),
			DataDefinition("industryGroupId", DataType::kIndexedString, 0, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo),
			DataDefinition("industry", DataType::kIndexedString, 0, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo),
			DataDefinition("industryId", DataType::kIndexedString, 0, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo),
			DataDefinition("subIndustry", DataType::kIndexedString, 0, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo),
			DataDefinition("subIndustryId", DataType::kIndexedString, 0, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo),

			DataDefinition("assetClassName", DataType::kIndexedString, 0, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo),
			DataDefinition("assetClassDesc", DataType::kIndexedString, 0, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo),
			DataDefinition("assetClassId", DataType::kIndexedString, 0, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo),

			/// Quotes
			DataDefinition("quoteId", DataType::kString, 0, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo),
			DataDefinition("quoteName", DataType::kString, 0, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo),
			DataDefinition("quoteRIC", DataType::kString, 0, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo),
			DataDefinition("quoteCcy", DataType::kIndexedString, 0, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo),
			DataDefinition("quoteExchange", DataType::kIndexedString, 0, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo),
			DataDefinition("quoteMIC", DataType::kIndexedString, 0, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo),

			/// Instrument
			DataDefinition("instrumentId", DataType::kString, 0, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo),
			DataDefinition("instrumentName", DataType::kString, 0, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo),
			DataDefinition("instrumentIssuerId", DataType::kString, 0, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo),
			DataDefinition("instrumentStatus", DataType::kIndexedString, 0, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo),

			/// Organisation
			DataDefinition("organisationId", DataType::kString, 0, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo),
			DataDefinition("organisationHeadquatersAddress", DataType::kString, 0, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo),
			DataDefinition("organisationRegisteredAddress", DataType::kString, 0, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo),
			DataDefinition("organisationStatus", DataType::kIndexedString, 0, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo),
			DataDefinition("organisationName", DataType::kString, 0, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo),
			//DataDefinition("organisationIncorporatedRegionCode", DataType::kIndexedString, 0, DataFrequency::kUniversal, DataFlags::kUpdatesInOneGo),
			//DataDefinition("organisationDomiciledRegionCode", DataType::kIndexedString, 0, DataFrequency::kUniversal, DataFlags::kUpdatesInOneGo),
			DataDefinition("organisationIPODate", DataType::kDateTime, 0, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo),
			DataDefinition("organisationInactiveDate", DataType::kDateTime, 0, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo),
		};

		void							loadDatabase(IDataRegistry& dr, const pesa::DataCacheInfo& dci);
		void							useQuote(size_t ii, QuoteData& qd);
		bool							resolveQuote(size_t ii, const UInt64Vec& quoteIds, QuoteData& qd, size_t& numMultiple);
		bool							getQuoteData(size_t ii, uint64_t quoteId, QuoteData& qd);

		TurtleReader::FileDetailsPtr	loadFile(IDataRegistry& dr, const pesa::DataCacheInfo& dci, std::string filename, size_t numThreads, TurtleReader::CreateNode createNode = nullptr);
		void							loadTickers(IDataRegistry& dr);

		void							consolidateInfo(IDataRegistry& dr);

	public:
										Reuters_PermId(const ConfigSection& config);
		virtual 						~Reuters_PermId();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 					initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 					build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);
	};

	Reuters_PermId::Reuters_PermId(const ConfigSection& config) : IDataLoader(config, "Reuters_PermId") {
	    auto exchangesStr = config.getResolvedString("exchanges", "");

		if (!exchangesStr.empty()) {
			StringVec countries, exchanges;
			FrameworkUtil::parseExchanges(exchangesStr, exchanges, countries);

			for (auto exchange : exchanges)
				m_exchanges[exchange] = true;
		}

		auto micExchangesStr = config.getResolvedString("micExchanges", "");

		if (!micExchangesStr.empty()) {
			StringVec countries, micExchanges;
			FrameworkUtil::parseExchanges(micExchangesStr, micExchanges, countries);

			for (auto micExchange : micExchanges)
				m_exchanges[micExchange] = true;
		}

		m_tree.add("hasName", s_hasNameId);
		m_tree.add("hasExchangeCode", s_hasExchangeCodeId);
		m_tree.add("hasExchangeTicker", s_hasExchangeTickerId);
		m_tree.add("hasRic", s_hasRicId);
		m_tree.add("isQuoteOf", s_isQuoteOfId);
		m_tree.add("isQuotedIn", s_isQuotedInId);
		m_tree.add("HeadquartersAddress", s_HeadquartersAddressId);
		m_tree.add("hasActivityStatus", s_hasActivityStatusId);
		m_tree.add("isDomiciledIn", s_isDomiciledInId);
		m_tree.add("name", s_nameId);
		m_tree.add("hasAssetClass", s_hasAssetClassId);
		m_tree.add("hasInstrumentStatus", s_hasInstrumentStatusId);
		m_tree.add("hasPrimaryQuote", s_hasPrimaryQuoteId);
		m_tree.add("isIssuedBy", s_isIssuedById);
		m_tree.add("decimalPlaces", s_decimalPlacesId);
		m_tree.add("iso4217", s_iso4217Id);
		m_tree.add("iso4217Numeric", s_iso4217NumericId);
		m_tree.add("prefLabel", s_prefLabelId);
		m_tree.add("BusinessClassificationCode", s_BusinessClassificationCodeId);
		m_tree.add("broader", s_broaderId);
		m_tree.add("label", s_labelId);
		m_tree.add("organization-name", s_organisationNameId);
		m_tree.add("hasMic", s_hasMicId);
		m_tree.add("RegisteredAddress", s_RegisteredAddressId);
		m_tree.add("isIncorporatedIn", s_isIncorporatedInId);
		m_tree.add("hasIPODate", s_hasIPODateId);
		m_tree.add("hasInactiveDate", s_hasInactiveDateId);
		m_tree.add("hasPrimaryBusinessSector", s_hasPrimaryBusinessSectorId);
		m_tree.add("hasPrimaryEconomicSector", s_hasPrimaryEconomicSectorId);
		m_tree.add("hasPrimaryIndustryGroup", s_hasPrimaryIndustryGroupId);
	}

	Reuters_PermId::~Reuters_PermId() {
		CTrace("Deleting Reuters_PermId");
	}

	void Reuters_PermId::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		auto datasetName = config().getResolvedRequiredString("datasetName");

		datasets.add(Dataset(datasetName, [this]() -> DataValueVec {
			DataValueVec dvv;

			for (const auto& def : m_dataDefs)
				dvv.push_back(DataValue(def.name, def.itemType, def.itemSize, nullptr, def.frequency, def.flags));

			return dvv;
		}()));
	}

	void Reuters_PermId::build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
	    std::string dataName = dci.def.name;
	  
	    try {
		    loadDatabase(dr, dci);
			consolidateInfo(dr);

			//	std::string dataName		= dci.def.name;
			auto iter					= m_data.find(dataName);

			CDebug("Building: %s", dataName);
			ASSERT(iter != m_data.end(), "Unable to find data: " << dataName);

			/// Now that we have the data, all we need to do is just send the information back in the frame
			frame.noDataUpdate			= false;
			frame.data					= iter->second;
			frame.dataSize				= frame.data->dataSize();
			frame.dataOffset			= 0;

			CInfo("Built: %s [Data Size: %z]", dataName, frame.data->dataSize());
		}
		catch (const std::exception& e) {
		    Util::reportException(e);
		}
		catch (...) {
		    CError("Unknown exception while trying to build data: %s. Aborting ...", dataName);
			Util::abort();
		}
	}

	inline std::thread constructLUT(TurtleReader::CNodePVec& values, PermIdMap& lut) {
		lut.reserve(values.size());

		return std::thread(([&]() {
			for (auto value : values)
				lut[value->id] = value;
		}));
	}

	//////////////////////////////////////////////////////////////////////////
	void Reuters_PermId::loadDatabase(IDataRegistry& dr, const pesa::DataCacheInfo& dci) {
		if (m_data.size())
			return;

		CInfo("=== Loading PermID Database ===");

		loadTickers(dr);

		m_assetClasses 	= loadFile(dr, dci, config().getResolvedRequiredString("assetClasses"), 1); //, [](TurtleReader::CNode** node) -> void { *node = new AssetClass(); });
		m_currencies 	= loadFile(dr, dci, config().getResolvedRequiredString("currencies"), 16); //, [](TurtleReader::CNode** node) -> void { *node = new Currency(); });
		m_industries 	= loadFile(dr, dci, config().getResolvedRequiredString("industries"), 16); //, [](TurtleReader::CNode** node) -> void { *node = new Industry(); });

		CDebug("Constructing LUT for asset classes!");
		std::thread assetClassesLUTThread = constructLUT(m_assetClasses->values, m_assetClasses->lut);

		CDebug("Constructing LUT for currencies!");
		std::thread currenciesLUTThread = constructLUT(m_currencies->values, m_currencies->lut);

		CDebug("Constructing LUT for industries!");
		std::thread industriesLUTThread = constructLUT(m_industries->values, m_industries->lut);

		m_quotes = loadFile(dr, dci, config().getResolvedRequiredString("quotes"), 64);

		/// Construct quote map in a spearate thread
		m_quotes->lut.reserve(m_quotes->values.size());

		/// For quotes, we only want the values from our exchange
		CDebug("Constructing LUT for quotes!");

		std::thread quotesLUTThread([&]() {
			for (TurtleReader::CNodeP node : m_quotes->values) {
				auto exchange = node->valueStr(s_hasExchangeCodeId);
				auto micExchange = node->valueStr(s_hasMicId);
				ExchangeMap::const_iterator iter = m_exchanges.end();

				/// Prefer the MIC exchange map ...
				if (!micExchange.empty())
					iter = m_exchanges.find(exchange);

				if (iter == m_exchanges.end() && !exchange.empty())
					iter = m_exchanges.find(exchange);

				if (iter != m_exchanges.end()) {
					m_quotes->lut[node->id] = node;

					auto ticker = node->valueStr(s_hasExchangeTickerId);

					/// If there is no ticker, then try to guess the ticker from RIC
					if (ticker.empty()) {
						auto ricId = node->valueStr(s_hasRicId);
						if (!ricId.empty()) {
							ticker = Util::split(ricId, "_")[0];
							ticker = Util::split(ricId, ".")[0];
						}
					}

					if (!ticker.empty()) 
						m_quoteTickersLUT[ticker].push_back(node->id);
				}
			}

			CDebug("Quotes finsihed!");
		});

		m_organisations = loadFile(dr, dci, config().getResolvedRequiredString("organisations"), 64);

		CDebug("Constructing LUT for orgranisations!");
		std::thread organisationsLUTThread = constructLUT(m_organisations->values, m_organisations->lut);

		m_instruments 	= loadFile(dr, dci, config().getResolvedRequiredString("instruments"), 64); //, [](TurtleReader::CNode** node) -> void { *node = new Instrument(); });

		/// Construct an instument map in a spearate thread
		CDebug("Constructing LUT for instruments!");
		std::thread instumentsLUTThread = constructLUT(m_instruments->values, m_instruments->lut);

		/// Ensure that the threads have finished
		assetClassesLUTThread.join();
		currenciesLUTThread.join();
		industriesLUTThread.join();

		organisationsLUTThread.join();
		instumentsLUTThread.join();
		quotesLUTThread.join();

		CInfo("=== PermID Database LOADED! ===");

		CDebug("Num asset classes : %z", m_assetClasses->lut.size());
		CDebug("Num industries    : %z", m_industries->lut.size());
		CDebug("Num currencies    : %z", m_currencies->lut.size());
		CDebug("Num quotes        : %z / %z", m_quotes->lut.size(), m_quotes->values.size());
		CDebug("Num instruments   : %z", m_instruments->lut.size());
		CDebug("Num organisations : %z", m_organisations->lut.size());
	}

	bool Reuters_PermId::getQuoteData(size_t ii, uint64_t quoteId, Reuters_PermId::QuoteData& qd) {
#define QD_ASSERT(cond, message)		if (!cond) { \
											std::ostringstream ss; \
											ss << "Ticker: " << qd.ticker << " - " << message; \
											pesa::Util::warning("QUOTE ASSERT", "%s", ss.str()); \
											return false; \
										}

		qd.ticker				= m_tickers->getValue(0, ii);

		qd.quoteId				= quoteId;
		qd.quote 				= m_quotes->get(qd.quoteId);

		qd.currencyId 			= qd.quote->valueId(s_isQuotedInId);
		QD_ASSERT(qd.currencyId, "Unable to find currency id for quote: " << qd.quoteId);

		qd.currency 			= m_currencies->get(qd.currencyId);
		QD_ASSERT(qd.currency, "Unable to find currency: " << qd.currencyId);

		qd.instrumentId 		= qd.quote->valueId(s_isQuoteOfId);
		QD_ASSERT(qd.instrumentId, "Unable to find instrument for quote: " << qd.quoteId);

		qd.instrument 			= m_instruments->get(qd.instrumentId);
		QD_ASSERT(qd.instrument, "Unable to find instrument: " << qd.instrumentId);

		qd.assetClassId 		= qd.instrument->valueId(s_hasAssetClassId);
		QD_ASSERT(qd.assetClassId, "Unable to find asset class for instrument: " << qd.instrumentId);

		qd.assetClass 			= m_assetClasses->get(qd.assetClassId);
		QD_ASSERT(qd.assetClass, "Unable to find asset class: " << qd.assetClassId);

		qd.organisationId 		= qd.instrument->valueId(s_isIssuedById);
		QD_ASSERT(qd.organisationId, "Unable to find issuer/organistaion id from instrument: " << qd.instrumentId);

		qd.organisation 		= m_organisations->get(qd.organisationId);
		QD_ASSERT(qd.organisation, "Unable to find organisation: " << qd.organisationId);

		qd.subIndustryId		= qd.organisation->valueId(s_hasPrimaryBusinessSectorId);
		qd.subIndustry			= qd.subIndustryId ? m_industries->get(qd.subIndustryId) : nullptr;

		if (qd.subIndustry) {
			qd.industryId		= qd.subIndustry->valueId(s_broaderId);
			qd.industry			= qd.industryId ? m_industries->get(qd.industryId) : nullptr;
		}

		if (qd.industry) {
			qd.industryGroupId	= qd.industry->valueId(s_broaderId);
			qd.industryGroup	= qd.industryGroupId ? m_industries->get(qd.industryGroupId) : nullptr;
		}

		if (qd.industryGroup) {
			qd.sectorId			= qd.industryGroup->valueId(s_broaderId);
			qd.sector			= qd.sectorId ? m_industries->get(qd.sectorId) : nullptr;
		}

		//qd.industryId 			= qd.organisation->valueId(s_hasPrimaryBusinessSectorId);
		//qd.industry 			= qd.industryId ? m_industries->get(qd.industryId) : nullptr;

		//qd.subIndustryId 		= qd.organisation->valueId(s_hasPrimaryEconomicSectorId);
		//qd.subIndustryGroup 		= qd.subIndustryId ? m_industries->get(qd.subIndustryId) : nullptr;

		//qd.sectorId 			= qd.organisation->valueId(s_hasPrimaryIndustryGroupId);
		//qd.sector 				= qd.sectorId ? m_industries->get(qd.sectorId) : nullptr;

		/// Fill in some of the extra information ...
		qd.quoteName			= qd.quote->valueStr(s_hasNameId);
		qd.instrumentName		= qd.instrument->valueStr(s_hasNameId);
		qd.organisationName		= qd.organisation->valueStr(s_organisationNameId);

#undef QD_ASSERT

		return true;
	}

	void Reuters_PermId::useQuote(size_t ii, QuoteData& qd) { 
		/// General
		m_data["id"]->setString(0, ii, 0, qd.quote->idStr());

		/// Sector Information
		if (qd.industry) {
			m_data["industry"]->setString(0, ii, 0, qd.industry->valueStr(s_labelId));
			m_data["industryId"]->setString(0, ii, 0, qd.industry->valueStr(s_BusinessClassificationCodeId));
		}

		if (qd.industryGroup) {
			m_data["industryGroup"]->setString(0, ii, 0, qd.industryGroup->valueStr(s_labelId));
			m_data["industryGroupId"]->setString(0, ii, 0, qd.industryGroup->valueStr(s_BusinessClassificationCodeId));
		}

		if (qd.subIndustry) {
			m_data["subIndustry"]->setString(0, ii, 0, qd.subIndustry->valueStr(s_labelId));
			m_data["subIndustryId"]->setString(0, ii, 0, qd.subIndustry->valueStr(s_BusinessClassificationCodeId));
		}

		auto sector = qd.sector;

		if (qd.sector) {
			m_data["sector"]->setString(0, ii, 0, qd.sector->valueStr(s_labelId));
			m_data["sectorId"]->setString(0, ii, 0, qd.sector->valueStr(s_BusinessClassificationCodeId));
		}
		//else if (qd.industry) {
		//	m_data["sector"]->setString(0, ii, 0, qd.industry->valueStr(s_labelId));
		//	m_data["sectorId"]->setString(0, ii, 0, qd.sector->valueStr(s_BusinessClassificationCodeId));
		//}

		/// Asset Class
		m_data["assetClassName"]->setString(0, ii, 0, qd.quote->valueStr(s_hasExchangeCodeId));
		m_data["assetClassDesc"]->setString(0, ii, 0, qd.quote->valueStr(s_hasExchangeCodeId));
		m_data["assetClassId"]->setString(0, ii, 0, qd.quote->valueStr(s_hasExchangeCodeId));

		/// Quotes
		m_data["quoteId"]->setString(0, ii, 0, qd.quote->idStr());
		m_data["quoteName"]->setString(0, ii, 0, qd.quoteName);
		m_data["quoteRIC"]->setString(0, ii, 0, qd.quote->valueStr(s_hasRicId));
		m_data["quoteCcy"]->setString(0, ii, 0, qd.currency->valueStr(s_iso4217Id));
		m_data["quoteExchange"]->setString(0, ii, 0, qd.quote->valueStr(s_hasExchangeCodeId));
		m_data["quoteMIC"]->setString(0, ii, 0, qd.quote->valueStr(s_hasMicId));

		/// Instrument
		m_data["instrumentId"]->setString(0, ii, 0, qd.instrument->idStr());
		m_data["instrumentName"]->setString(0, ii, 0, qd.instrumentName);
		m_data["instrumentIssuerId"]->setString(0, ii, 0, qd.organisation->idStr());
		m_data["instrumentStatus"]->setString(0, ii, 0, qd.instrument->valueStr(s_hasInstrumentStatusId));

		/// Organisation
		m_data["organisationId"]->setString(0, ii, 0, qd.organisation->idStr());
		m_data["organisationHeadquatersAddress"]->setString(0, ii, 0, qd.organisation->valueStr(s_HeadquartersAddressId));
		m_data["organisationRegisteredAddress"]->setString(0, ii, 0, qd.organisation->valueStr(s_RegisteredAddressId));
		m_data["organisationStatus"]->setString(0, ii, 0, qd.organisation->valueStr(s_hasActivityStatusId));
		m_data["organisationName"]->setString(0, ii, 0, qd.organisationName);
		//m_data["organisationIncorporatedRegionCode"]->setString(0, ii, 0, qd.organisation->valueStr(s_isIncorporatedInId));
		//m_data["organisationDomiciledRegionCode"]->setString(0, ii, 0, qd.organisation->valueStr(s_isDomiciledInId));
		m_data["organisationIPODate"]->setString(0, ii, 0, qd.organisation->valueStr(s_hasIPODateId));
		m_data["organisationInactiveDate"]->setString(0, ii, 0, qd.organisation->valueStr(s_hasInactiveDateId));
	}

	bool Reuters_PermId::resolveQuote(size_t ii, const UInt64Vec& quoteIds, Reuters_PermId::QuoteData& qd, size_t& numMultiple) {
		std::vector<QuoteData> qdd;

		auto name = m_names->getValue(0, ii);
		auto ticker = m_tickers->getValue(0, ii);
		auto figi = m_figis->getValue(0, ii);

		for (size_t i = 0; i < quoteIds.size(); i++) {
			QuoteData qd;
			if (getQuoteData(ii, quoteIds[i], qd))
				qdd.push_back(qd);
		}

		if (!qdd.size())
			return false;

		/// First of all we need to construct the unique quotes from this ...
		std::vector<QuoteData*> uqdd;
		uqdd.push_back(&qdd[0]);

		for (size_t i = 1; i < qdd.size(); i++) {
			bool isUnique = true;

			for (size_t j = 0; j < uqdd.size() && isUnique; j++) {
				if (uqdd[j]->organisationName == qdd[i].organisationName || uqdd[j]->quoteName == qdd[i].quoteName || uqdd[j]->instrumentName == qdd[i].instrumentName) {
					isUnique = false;

					/// TODO: We need to ensure that the quote that we're using is the latest one 
				}
			}

			if (isUnique)
				uqdd.push_back(&qdd[i]);
		}

		/// If we're left with just one quote at the end of it, then we just use the one that's still active
		if (uqdd.size() == 1) {
			qd = *(uqdd[0]);
			return true;
		}

		size_t bestIndex = 0;

		CDebug("Conflict for ticker: %s [%s] [%s]", name, figi, ticker);

		/// Now we try to make sure we pick the right quote
		if (!name.empty()) {
			int bestMatch = 100000;

			for (size_t i = 0; i < uqdd.size(); i++) {
				QuoteData& qd = *(uqdd[i]);
				int orgNameScore = 0;

				if (!qd.organisationName.empty())
					orgNameScore = Util::levenshteinDistance(name, qd.organisationName);

				int quoteNameScore = 0;

				if (!qd.quoteName.empty())
					quoteNameScore = Util::levenshteinDistance(name, qd.quoteName);

				int instNameScore = 0;

				if (!qd.instrumentName.empty())
					instNameScore = Util::levenshteinDistance(name, qd.instrumentName);

				int totalScore = (int)((float)orgNameScore * 0.5f + (float)instNameScore * 0.4f + (float)quoteNameScore * 0.1f);
				if (totalScore == bestMatch) {
					CWarning("Possible multiple matches for security. Please check!");
				}
				//ASSERT(totalScore != bestMatch, "Total score matches the best match. Conflict!");

				CDebug("   - %z. %s / %s / %z [Inst = %s / %s / %z] [Quote = %s] = %d", i, qd.organisationName, qd.organisation->valueStr(s_hasActivityStatusId), qd.organisationId, 
					qd.instrumentName, qd.instrument->valueStr(s_hasInstrumentStatusId), qd.instrumentId, qd.quoteName, totalScore);

				/// If either of the scores match
				if (totalScore < bestMatch) {
					bestMatch = totalScore;
					bestIndex = i;
				}
			}
		}

		numMultiple++;
		qd = *uqdd[bestIndex];
		CDebug("MATCH: %z. %s", bestIndex, qd.organisationName);

		return true;
	}

	void Reuters_PermId::consolidateInfo(IDataRegistry& dr) {
		if (m_data.size())
			return;

		CInfo("Consolidating the information of all securities ...");

		size_t numSecurities = m_tickers->cols();
		TurtleReader::CNodePVec quotes(numSecurities);

		memset(&quotes[0], 0, sizeof(TurtleReader::CNodeP) * numSecurities);

		CDebug("Num tickers to consolidate: %z", numSecurities);

		size_t numMultiple = 0;

		for (const auto& def : m_dataDefs) {
			auto data = dr.createDataBuffer(nullptr, def);
			data->ensureSize(1, numSecurities, false);
			m_data[def.name] = data;
		}

		size_t total = 0;
		for (size_t ii = 0; ii < numSecurities; ii++) {
			std::string ticker = m_tickers->getValue(0, ii);

			if (ticker.empty())
				continue;

			std::string figi = m_figis->getValue(0, ii);
			std::string name = m_names->getValue(0, ii);
			auto iter = m_quoteTickersLUT.find(ticker);

			if (iter != m_quoteTickersLUT.end()) {
				auto& quoteIds = iter->second;
				auto numQuotes = quoteIds.size();

				if (numQuotes) {
					total++;

					/// If there is just a single quote, then we use it
					if (numQuotes == 1) {
						QuoteData qd;

						if (getQuoteData(ii, quoteIds[0], qd))
							useQuote(ii, qd);
					}
					else {
						QuoteData qd;

						if (resolveQuote(ii, quoteIds, qd, numMultiple))
							useQuote(ii, qd);
					}
				}
			}
			else {
				CTrace("Unable to find ticker: %s [TKR: %s, FIGI: %s]", name, ticker, figi);
			}
		}

		CInfo("Consolidated: %z / %z [Num Multiple: %z]", total, numSecurities, numMultiple);

		/// Free the memory early
		m_assetClasses		= nullptr;
		m_currencies		= nullptr;
		m_industries		= nullptr;
		m_quotes			= nullptr;
		m_instruments		= nullptr;
		m_organisations		= nullptr;
	}

	void Reuters_PermId::loadTickers(IDataRegistry& dr) {
		std::string tickersId = config().getResolvedRequiredString("tickers");
		std::string namesId = config().getResolvedRequiredString("names");
		std::string figisId = config().getResolvedRequiredString("figis");

		m_tickers = dr.stringData(nullptr, tickersId, true);
		m_names = dr.stringData(nullptr, namesId, true);
		m_figis = dr.stringData(nullptr, figisId, true);
	}

	TurtleReader::FileDetailsPtr Reuters_PermId::loadFile(IDataRegistry& dr, const pesa::DataCacheInfo& dci, std::string filename, size_t numThreads, TurtleReader::CreateNode createNode) {
		try {
			CInfo("Loading: %s ...", filename);

			TurtleReader* reader = new TurtleReader(filename);
		
			if (createNode)
				reader->createNodeCallback() = createNode;

			reader->tree() = &m_tree;

			auto details = reader->readAll(numThreads);
			details->reader = reader;

			//m_readers.push_back(treader);

			CInfo("DONE: %s!", filename);

			return details;
		}
		catch (const std::exception& e) {
			Util::reportException(e);
		}
		catch (...) {
			CError("Unknown exception while trying to load file: %s. Aborting ...", filename);
			Util::abort();
		}

		return nullptr;
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createReuters_PermId(const pesa::ConfigSection& config) {
	return new pesa::Reuters_PermId((const pesa::ConfigSection&)config);
}

