/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Reuters_SecMaster.cpp
///
/// Created on: 02 May 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Poco/Timestamp.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// MorningStar Security Master
	////////////////////////////////////////////////////////////////////////////
	class Reuters_SecMaster : public IDataLoader {
	private:
		typedef std::unordered_map<IntDate, UIntVec>    DailySecMap;
        typedef std::unordered_map<std::string, size_t> SymbolLookupMap;
        typedef std::map<std::string, Security::Index>  SecurityLookup;
        typedef std::unordered_map<std::string, bool>   ExchangeBoolMap;

		SecurityLookup			m_securityLookup;		/// Which securities do we already have? 
		SecurityVec				m_allSecurities;		/// All the securities within the system

		void 					buildSecurities(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities);
		void 					buildDetails(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities);

	public:
								Reuters_SecMaster(const ConfigSection& config);
		virtual 				~Reuters_SecMaster();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			buildSecurityMaster(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);

		virtual void 			preDataBuild(IDataRegistry& dr, const DataCacheInfo& dci);
		virtual void 			postDataBuild(IDataRegistry& dr, const DataCacheInfo& dci);
	};

	Reuters_SecMaster::Reuters_SecMaster(const ConfigSection& config) : IDataLoader(config, "Reuters_SecMaster") {
	}

	Reuters_SecMaster::~Reuters_SecMaster() {
		CTrace("Deleting Reuters_SecMaster");
	}

	void Reuters_SecMaster::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		std::string name = config().getRequired("name");
		std::string prefix = config().getString("prefix", "");

		datasets.add(Dataset(name, {
			DataValue(prefix + "Master", DataType::kSecurityData, Security::s_version, THIS_DATA_FUNC(Reuters_SecMaster::buildSecurityMaster), 
				DataFrequency::kStatic, DataFlags::kAlwaysLoad | DataFlags::kUpdatesInOneGo | DataFlags::kSecurityMaster),
		}));
	}

	void Reuters_SecMaster::postDataBuild(IDataRegistry& dr, const DataCacheInfo& dci) {
		/// We do not need this data anymore ...
		m_securityLookup.clear();
		m_allSecurities.clear();
	}

	void Reuters_SecMaster::preDataBuild(IDataRegistry& dr, const DataCacheInfo& dci) {
		std::string name = config().getRequired("name");
		std::string prefix = config().getString("prefix", "");

		const SecurityData* secData = dr.get<SecurityData>(nullptr, name + "." + prefix + "Master", true);

		if (secData) {
			size_t numSecurities = secData->numSecurities();
			m_allSecurities.resize(numSecurities);
			CInfo("Existing number of securities: %z", numSecurities);

			for (size_t i = 0; i < numSecurities; i++) {
				const Security& sec = secData->security(i);
				m_securityLookup[sec.uuidStr()] = sec.index;
				m_allSecurities[i] = sec;
			}
		}
	}

	void Reuters_SecMaster::build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
	}

	void Reuters_SecMaster::buildSecurities(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities) {
		DataInfo dinfo;
		dinfo.noCacheUpdate				= true;
		dinfo.noAssertOnMissingDataLoader = true;

		const auto* issuePermId			= dr.stringData(nullptr, "preprocess.issuePermId", (DataInfo*)&dinfo);
		const auto* issuerPermId		= dr.stringData(nullptr, "preprocess.issuerPermId", (DataInfo*)&dinfo);
		const auto* quotePermId			= dr.stringData(nullptr, "preprocess.quotePermId", (DataInfo*)&dinfo);
		const auto* ticker				= dr.stringData(nullptr, "preprocess.ticker", (DataInfo*)&dinfo);
		const auto* tradingStatus		= dr.stringData(nullptr, "preprocess.tradingStatus", (DataInfo*)&dinfo);
		const auto* tradingSymbol		= dr.stringData(nullptr, "preprocess.tradingSymbol", (DataInfo*)&dinfo);
		const auto* exchangeCode		= dr.stringData(nullptr, "preprocess.exchangeCode", (DataInfo*)&dinfo);
		const auto* currency			= dr.stringData(nullptr, "preprocess.currency", (DataInfo*)&dinfo);

		bool ignoreMissingUuid			= config().getBool("ignoreMissingUuid", false);

		size_t count					= issuerPermId->cols();

		for (size_t ii = 0; ii < count; ii++) {
			auto issueUuid				= issuePermId->getValue(0, ii);
			auto quoteUuid				= quotePermId->getValue(0, ii);
			auto parentUuid				= issuerPermId->getValue(0, ii);
			auto instrument				= tradingSymbol->getValue(0, ii);

			auto uuid					= !issueUuid.empty() ? issueUuid : quoteUuid;

			/// We don't wanna deal with securities that don't have any real UUID associated with them
			if (uuid.empty()) 
				continue;

			/// If this CFIGI is already part of the all securities table then we just ignore it
			auto iter					= m_securityLookup.find(uuid);

			if (iter != m_securityLookup.end())
				continue;

			Security sec;

			sec.setInstrumentId(instrument);
			sec.setTicker(ticker->getValue(0, ii));
			sec.setBBTicker(ticker->getValue(0, ii)); //bbTicker->getValue(0, ii) + " " + exchCode->getValue(0, ii));
			sec.setUuid(uuid);
			sec.setName(ticker->getValue(0, ii)); 
			sec.setExchange(exchangeCode->getValue(0, ii));
			//sec.setIssueClass(issueClass->getValue(0, ii));
			//sec.setParentUuid(parentUuid->getValue(0, ii));

			//bool adr = isForeign->getValue(0, ii) == "Y" || name->getValue(0, ii).find("(Foreign)") != std::string::npos ? true : false;
			//sec.setBBSecurityType(!adr ? secType->getValue(0, ii) : "ADR");
			sec.setCurrency(currency->getValue(0, ii));
			//sec.setCountryCode2(country->getValue(0, ii));
			//sec.setISIN(isin->getValue(0, ii));
			//sec.setIssueType("Common Stock");

			//sec.isInactive = delistedInfo->getValue(0, ii) == "Y" ? true : false;

			newSecurities.push_back(sec);

			m_securityLookup[uuid] = (unsigned int)(m_allSecurities.size() + ii);
		}
	}

    void Reuters_SecMaster::buildSecurityMaster(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
        SecurityVec newSecurities;
        buildSecurities(dr, dci, newSecurities);

        CDebug("Number of new securities found: %z", newSecurities.size());

        size_t numExisting = m_allSecurities.size();
        size_t numNew = newSecurities.size();
        size_t numFiltered = numNew;

        /// Assign the correct indexes to the securities
        for (auto& sec : newSecurities) {
            sec.index = (Security::Index)m_allSecurities.size();
            m_allSecurities.push_back(sec);
        }

        try {
            CDebug("Number of new securities: %z - After filteration: %z [Existing: %z, DataSize: %z]", numNew, numFiltered, numExisting, m_allSecurities.size() * sizeof(Security));

            /// otherwise we create a new data for these
            frame.data = std::make_shared<SecurityData>(dci.universe, m_allSecurities, dci.def);
            frame.dataOffset = 0;
            frame.dataSize = frame.data->dataSize();
            frame.noDataUpdate = false;
        }
        catch (const std::exception& e) {
            Util::reportException(e);
            Util::exit(1);
        }
    }

    void Reuters_SecMaster::buildDetails(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities) {
        CDebug("Getting detailed information about the securities!");
    }
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createReuters_SecMaster(const pesa::ConfigSection& config) {
	return new pesa::Reuters_SecMaster((const pesa::ConfigSection&)config);
}

