/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// AxiomaFactorCov.cpp
///
/// Created on: 17 Nov 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "SpGeneric.h"
#include "Poco/String.h"

namespace pesa {
	class AxiomaFactorCov : public SpGeneric {
	private:
		int						m_numFactors = 0;
		int						m_minFactor = -1;
		int						m_maxFactor = -1;

	public:
								AxiomaFactorCov(const ConfigSection& config);
		virtual 				~AxiomaFactorCov();

		////////////////////////////////////////////////////////////////////////////
		/// SpGeneric overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			buildDaily(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
	};

	AxiomaFactorCov::AxiomaFactorCov(const ConfigSection& config) 
		: SpGeneric(config) {
		setLogChannel("AxiomaFactorCov");
		m_minfo.specifiedFieldsOnly = true;
		SpGeneric::m_passSymbols = false;
	}

	AxiomaFactorCov::~AxiomaFactorCov() {
		CTrace("Deleting AxiomaFactorCov");
	}

	void AxiomaFactorCov::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		ASSERT(m_datasets.size() == 1, "Invalid datasets: " << config().getString("datasets", ""));

		std::string dsName = m_datasets[0];
		datasets.add(Dataset(m_datasetName, {
			DataValue(dsName + "_Cov", DataType::kFloat, sizeof(float), THIS_DATA_FUNC(SpGeneric::buildDaily), DataFrequency::kDaily, 0),
		}));

		m_dataRequired.push_back("COVARIANCE");
		m_requiredDataTypes.push_back(DataType::kFloat);
		m_dataRequired.push_back("ROWFACTORID");
		m_requiredDataTypes.push_back(DataType::kInt);
		m_dataRequired.push_back("COLUMNFACTORID");
		m_requiredDataTypes.push_back(DataType::kInt);
	}

	void AxiomaFactorCov::buildDaily(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		std::string rmappedName;
		IntDate date = dci.rt.dates[dci.di];
		auto result = waitForResult(dr, dci, frame, rmappedName, "");

		if (!result.data.size()) {
			frame.noDataUpdate = true;
			return;
		}

		DataPtr covData				= result.data["COVARIANCE"];
		DataPtr rowIdData			= result.data["ROWFACTORID"];
		DataPtr colIdData			= result.data["COLUMNFACTORID"];

		if (!covData) {
			frame.noDataUpdate		= true;
			return;
		}

		ASSERT(covData, "Unable to get Covariance data!");
		FloatMatrixData& cov		= *dynamic_cast<FloatMatrixData*>(covData.get());
		IntMatrixData& rowIds		= *dynamic_cast<IntMatrixData*>(rowIdData.get());
		IntMatrixData& colIds		= *dynamic_cast<IntMatrixData*>(colIdData.get());

		int size = (int)cov.cols();
		int numFactors = (int)std::sqrt((float)size);

		int minFactor				= m_minFactor;
		int maxFactor				= m_maxFactor;

		if (minFactor < 0 && maxFactor < 0) {
			std::unordered_map<int, bool> factorsDiscovered;
			int i = 0;

			while (factorsDiscovered.size() < (size_t)numFactors && i++ < size) {
				int factorId			= rowIds(0, i);
				if (minFactor < 0 || factorId < minFactor)
					minFactor			= factorId;
				if (maxFactor < 0 || factorId > maxFactor)
					maxFactor			= factorId;
				if (factorsDiscovered.find(factorId) == factorsDiscovered.end())
					factorsDiscovered[factorId] = true;
			}
		}

		ASSERT(maxFactor > 0 && minFactor > 0 && maxFactor > minFactor, "Invalid minFactor: " << minFactor << " - maxFactor: " << maxFactor);

		numFactors = maxFactor - minFactor + 1;

		std::vector<int> colIndexes(numFactors);
		std::vector<int> rowIndexes(numFactors);

		memset(&colIndexes[0], 0, sizeof(int) * colIndexes.size());
		memset(&rowIndexes[0], 0, sizeof(int) * rowIndexes.size());

		if (!m_numFactors)
			m_numFactors = numFactors;
		else {
			ASSERT(m_numFactors >= numFactors, "Inconsistent number of factors. Previously we had: " << m_numFactors << " - now we have: " << numFactors);
			numFactors = m_numFactors;
		}

		FloatMatrixData* newData = new FloatMatrixData(nullptr, 1, numFactors * numFactors);
		//ASSERT(numFactors * numFactors == size, "Covariance matrix must be square. Total size: " << numFactors << " - SQRT: " << numFactors);

		for (int ii = 0; ii < size; ii++) {
			int rowFactor			= rowIds(0, ii);
			int colFactor			= colIds(0, ii);

			ASSERT(rowFactor >= minFactor && rowFactor <= maxFactor, "Invalid factorId in row: " << rowFactor << " at index: " << ii << ". Expecting factor ids to be in range [" << minFactor << ", " << maxFactor << "]");
			ASSERT(colFactor >= minFactor && colFactor <= maxFactor, "Invalid factorId in row: " << colFactor << " at index: " << ii << ". Expecting factor ids to be in range [" << minFactor << ", " << maxFactor << "]");

			int rowIndex			= rowFactor - minFactor;
			int colIndex			= colFactor - minFactor;

			//if (rowIndex < 0 || rowIndex >= numFactors) {
			//	CWarning("Ignoring factor: %d [Max: %d]", rowFactor, minFactor + numFactors);
			//	continue;
			//}

			//if (rowIndex < 0 || rowIndex >= numFactors) {
			//	CWarning("Ignoring factor: %d [Max: %d]", rowFactor, minFactor + numFactors);
			//	continue;
			//}
			ASSERT(rowIndex >= 0 && rowIndex < numFactors, "Invalid factor id in row: " << rowFactor);
			ASSERT(colIndex >= 0 && colIndex < numFactors, "Invalid factor id in col: " << colFactor);

			int outRowIndex			= rowIndexes[rowIndex];
			int outColIndex			= colIndexes[colIndex];

			ASSERT(outRowIndex >= 0 && rowIndex < numFactors, "Invalid out factor id in row: " << outRowIndex);
			ASSERT(outColIndex >= 0 && outColIndex < numFactors, "Invalid out factor id in col: " << outColIndex);

			rowIndexes[rowIndex]++;
			colIndexes[colIndex]++;

			//ASSERT(rowIndex < numFactors, "Row index overflow! Expecting max: " << numFactors << " - However current index is: " << rowIndex);
			//ASSERT(colIndex < numFactors, "Col index overflow! Expecting max: " << numFactors << " - However current index is: " << colIndex);

			float covValue			= cov(0, ii);
			(*newData)(0, outRowIndex * numFactors + outColIndex) = covValue;
		}

		frame.data = DataPtr(newData);
		frame.dataOffset = 0;
		frame.dataSize = frame.data->dataSize();

		CInfo("[%-8u]-Factor covariance count: %d [%d, %d]", date, size, minFactor, maxFactor);
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createAxiomaFactorCov(const pesa::ConfigSection& config) {
	return new pesa::AxiomaFactorCov((const pesa::ConfigSection&)config);
}

