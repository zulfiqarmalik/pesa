/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// BBAdjFactor.cpp
///
/// Created on: 18 Oct 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "SpGeneric.h"
#include "Poco/String.h"

namespace pesa {
	class BBAdjFactor : public SpGeneric {
	private:
		FloatVec				m_adjFactors;				/// The adjustment factors
		IntVec					m_diAdjFactors;				/// The day of the last valid adjustment factor
		FloatMatrixData*		m_dailyFactors = nullptr;	/// The daily PRICE factors
		FloatMatrixData*		m_dailyVolFactors = nullptr;/// The daily VOLUME factors
		FloatMatrixData*		m_dailyFactorsVerbatim = nullptr;	/// The daily PRICE factors (verbatim i.e. not propagated historically)
		FloatMatrixData*		m_dailyVolFactorsVerbatim = nullptr;	/// The daily VOLUME factors (verbatim i.e. not propagated historically)
		FloatMatrixData*		m_srcVolFactors = nullptr;	/// The daily VOLUME factors source
		IntMatrixData*			m_diDailyFactors = nullptr;	/// The di for the daily factors
		bool					m_cacheBuilt = false;		/// Whether the cache has been built or not

	protected:
		virtual void 			buildData(IDataRegistry& dr, const DataCacheInfo& dci);

	public:
								BBAdjFactor(const ConfigSection& config);
		virtual 				~BBAdjFactor();

		////////////////////////////////////////////////////////////////////////////
		/// BBAdjFactor overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			buildFactor(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void 			buildDiFactor(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void 			buildDailyFactors(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void 			buildDailyFactorsVerbatim(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void 			buildDailyVolFactorsVerbatim(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void 			buildDailyVolFactors(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void 			buildDiDailyFactors(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void 			postDataBuild(IDataRegistry& dr, const DataCacheInfo& dci);
		virtual void			preCache(IDataRegistry& dr, const DataCacheInfo& dci, const std::string& keyExtra);
	};

	BBAdjFactor::BBAdjFactor(const ConfigSection& config) 
		: SpGeneric(config) {
		setLogChannel("BBAdjFactor");
		m_minfo.specifiedFieldsOnly = true;
	}

	BBAdjFactor::~BBAdjFactor() {
		CTrace("Deleting BBAdjFactor");
	}

	void BBAdjFactor::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		datasets.add(Dataset(m_datasetName, {
			DataValue("f_adj", DataType::kFloat, sizeof(float), THIS_DATA_FUNC(BBAdjFactor::buildDailyFactors), DataFrequency::kDaily, DataFlags::kAlwaysOverwrite),
			DataValue("f_adjVolume", DataType::kFloat, sizeof(float), THIS_DATA_FUNC(BBAdjFactor::buildDailyVolFactors), DataFrequency::kDaily, DataFlags::kAlwaysOverwrite),

			DataValue("f_adjFactorVerbatim", DataType::kFloat, sizeof(float), THIS_DATA_FUNC(BBAdjFactor::buildDailyFactorsVerbatim), DataFrequency::kDaily, DataFlags::kAlwaysOverwrite),
			DataValue("f_adjVolFactorVerbatim", DataType::kFloat, sizeof(float), THIS_DATA_FUNC(BBAdjFactor::buildDailyVolFactorsVerbatim), DataFrequency::kDaily, DataFlags::kAlwaysOverwrite),
		}));

		m_dataRequired.push_back("SYMBOL");
		m_dataRequired.push_back("FACTOR");
		m_dataRequired.push_back("FLAG");
		m_dataRequired.push_back("TYPE");
		m_dataRequired.push_back("TIMESTAMP");

		m_requiredDataTypes.push_back(DataType::kString);
		m_requiredDataTypes.push_back(DataType::kFloat);
		m_requiredDataTypes.push_back(DataType::kInt);
		m_requiredDataTypes.push_back(DataType::kInt);
		m_requiredDataTypes.push_back(DataType::kTimestamp);
	}

	void BBAdjFactor::postDataBuild(IDataRegistry& dr, const pesa::DataCacheInfo& dci) {
		m_adjFactors.clear();
		m_diAdjFactors.clear();

		delete m_dailyFactors; 
		delete m_srcVolFactors;
		delete m_dailyVolFactors; 
		delete m_dailyFactorsVerbatim;
		delete m_dailyVolFactorsVerbatim;
		delete m_diDailyFactors;
	}

	void BBAdjFactor::preCache(IDataRegistry& dr, const DataCacheInfo& dci, const std::string& keyExtra) {
		if (m_cacheBuilt)
			return;

		preCacheDay(dr, dci, 0, keyExtra);
		m_cacheBuilt = true;
	}

	void BBAdjFactor::buildDailyFactors(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		buildData(dr, dci);

		size_t numCols = m_dailyFactors->cols();
		FloatMatrixData* ddata = new FloatMatrixData(dci.universe, 1, numCols);
		memcpy(ddata->rowPtr(0), m_dailyFactors->rowPtr(dci.di), sizeof(float) * numCols);

		frame.data = DataPtr(ddata);
		frame.dataSize = ddata->dataSize();
		frame.dataOffset = 0;

		CDebug("[%u] - %s.%s", dci.rt.dates[dci.di], m_datasetName, std::string(dci.def.name));
	}

	void BBAdjFactor::buildDailyFactorsVerbatim(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		buildData(dr, dci);

		size_t numCols = m_dailyFactorsVerbatim->cols();
		FloatMatrixData* ddata = new FloatMatrixData(dci.universe, 1, numCols);
		memcpy(ddata->rowPtr(0), m_dailyFactorsVerbatim->rowPtr(dci.di), sizeof(float) * numCols);

		frame.data = DataPtr(ddata);
		frame.dataSize = ddata->dataSize();
		frame.dataOffset = 0;

		CDebug("[%u] - %s.%s", dci.rt.dates[dci.di], m_datasetName, std::string(dci.def.name));
	}

	void BBAdjFactor::buildDailyVolFactorsVerbatim(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		buildData(dr, dci);

		size_t numCols = m_dailyVolFactorsVerbatim->cols();
		FloatMatrixData* ddata = new FloatMatrixData(dci.universe, 1, numCols);
		memcpy(ddata->rowPtr(0), m_dailyVolFactorsVerbatim->rowPtr(dci.di), sizeof(float) * numCols);

		frame.data = DataPtr(ddata);
		frame.dataSize = ddata->dataSize();
		frame.dataOffset = 0;

		CDebug("[%u] - %s.%s", dci.rt.dates[dci.di], m_datasetName, std::string(dci.def.name));
	}

	void BBAdjFactor::buildDailyVolFactors(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		buildData(dr, dci);

		size_t numCols = m_dailyVolFactors->cols();
		FloatMatrixData* ddata = new FloatMatrixData(dci.universe, 1, numCols);
		memcpy(ddata->rowPtr(0), m_dailyVolFactors->rowPtr(dci.di), sizeof(float) * numCols);

		frame.data = DataPtr(ddata);
		frame.dataSize = ddata->dataSize();
		frame.dataOffset = 0;

		CDebug("[%u] - %s.%s", dci.rt.dates[dci.di], m_datasetName, std::string(dci.def.name));
	}

	void BBAdjFactor::buildDiDailyFactors(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		buildData(dr, dci);

		frame.data = DataPtr(m_diDailyFactors);
		frame.dataSize = m_diDailyFactors->dataSize();
		frame.dataOffset = 0;
		m_diDailyFactors = nullptr;
	}

	void BBAdjFactor::buildFactor(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		buildData(dr, dci);

		ASSERT(m_adjFactors.size(), "Unable to load adjustment factors!");

		FloatMatrixData* adjFactors = new FloatMatrixData(nullptr, m_adjFactors);

		frame.data = DataPtr(adjFactors);
		frame.dataSize = adjFactors->dataSize();
		frame.dataOffset = 0;
	}

	void BBAdjFactor::buildDiFactor(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		buildData(dr, dci);

		ASSERT(m_diAdjFactors.size(), "Unable to load adjustment factors indexes!");

		IntMatrixData* diAdjFactors = new IntMatrixData(nullptr, m_diAdjFactors);

		frame.data = DataPtr(diAdjFactors);
		frame.dataSize = diAdjFactors->dataSize();
		frame.dataOffset = 0;
	}

	void BBAdjFactor::buildData(IDataRegistry& dr, const DataCacheInfo& dci) {
		/// If the factors have already been loaded
		if (m_adjFactors.size())
			return; 

		std::string rmappedName;
		IntDate date = dci.rt.dates[dci.di];
		auto secMaster = dr.getSecurityMaster();
		size_t size = secMaster->size(0);

		m_adjFactors.resize(size);
		m_diAdjFactors.resize(size);

		memset(&m_adjFactors[0], 0, sizeof(float) * m_adjFactors.size());
		memset(&m_diAdjFactors[0], 0, sizeof(int) * m_diAdjFactors.size());

		m_dailyFactors					= new FloatMatrixData(nullptr, dci.rt.dates.size(), size);
		m_dailyFactorsVerbatim			= new FloatMatrixData(nullptr, dci.rt.dates.size(), size);
		m_dailyVolFactorsVerbatim		= new FloatMatrixData(nullptr, dci.rt.dates.size(), size);
		m_srcVolFactors					= new FloatMatrixData(nullptr, dci.rt.dates.size(), size);
		m_dailyVolFactors				= new FloatMatrixData(nullptr, dci.rt.dates.size(), size);
		m_diDailyFactors				= new IntMatrixData(nullptr, dci.rt.dates.size(), size);

		/// By default, we set the adjustment factor to 1.0f
		m_dailyFactorsVerbatim->setMemory(1.0f);
		m_dailyVolFactorsVerbatim->setMemory(1.0f);

		DataFrame tmpFrame;
		auto results = waitForResults(dr, dci, tmpFrame, rmappedName); 

		ASSERT(results.size(), "Unable to get any sizes!");
		size_t numValidFactors = 0;
		std::unordered_map<std::string, Poco::Timestamp> tsMap;

		int numResults = (int)results.size();
		 
		for (int resultIndex = numResults - 1; resultIndex >= 0; resultIndex--) {
			auto& result				= results[resultIndex];
			auto figiData				= result.data["SYMBOL"];
			auto factorData				= result.data["FACTOR"]; 
			auto factorTypeData			= result.data["TYPE"]; 
			auto factorFlagData			= result.data["FLAG"]; 
			auto timestampData			= result.data["TIMESTAMP"];

			if (!figiData)
				continue;

			const std::string* figis	= (dynamic_cast<StringData*>(figiData.get()))->sptr();
			const auto* factors			= (dynamic_cast<FloatMatrixData*>(factorData.get()));
			const auto* factorFlags		= (dynamic_cast<IntMatrixData*>(factorFlagData.get()));
			const auto* factorTypes		= (dynamic_cast<IntMatrixData*>(factorTypeData.get()));
			const auto* timestamps		= (dynamic_cast<Int64MatrixData*>(timestampData.get()));

			auto length					= figiData->cols();

			for (size_t ii = 0; ii < length; ii++) {
				const auto& figi		= figis[ii];
				if (figi.empty())
					continue;

				const auto*	sec			= secMaster->mapUuid(figi);

				if (!sec || !sec->isValid() || sec->index == Security::s_invalidIndex)
					continue;

				float factor			= (*factors)(0, ii);
				int operatorType		= (*factorTypes)(0, ii);
				int factorFlag			= (*factorFlags)(0, ii);

				/// If there is no factor then we just continue
				if (std::isnan(factor))
					continue; 

				auto timestamp			= timestamps->getTSValue(0, ii);
				int64_t rawTimestamp	= timestamps->getValue(0, ii);
				auto iter				= tsMap.find(figi);

				IntDate tsDate = Util::timestampToIntDate(timestamp);
				IntDate diFactor = dci.rt.getDateIndex(tsDate);

				if (diFactor == Constants::s_invalidDay) {
					Poco::DateTime dtEnd = Util::intToDate(dci.rt.dates[dci.rt.diEnd]);
					Poco::DateTime dtTimestamp(timestamp);

					CWarning("Date not in backtest moving ahead: %u", tsDate);
					while (dtTimestamp < dtEnd && diFactor == Constants::s_invalidDay) {
						dtTimestamp += Util::daysSpan(1);
						tsDate = Util::dateToInt(dtTimestamp);
						diFactor = dci.rt.getDateIndex(tsDate);
					}

					/// If we still do not have a factor then we just ignore this ...
					if (diFactor == Constants::s_invalidDay)
						continue;
					else {
						CInfo("New date: %u", tsDate);
					}
				}

				float volumeFactor							= factor;
				float existingFactor						= m_dailyFactors->isValid(diFactor, sec->index) ? (*m_dailyFactors)(diFactor, sec->index) : 1.0f;
				float existingVolFactor						= m_srcVolFactors->isValid(diFactor, sec->index) ? (*m_srcVolFactors)(diFactor, sec->index) : 1.0f;
				bool addFactor								= false;

				/*
				Column 3 - Operator Type (1=div, 2=mult, 3=add. Opposite for Volume)
				Column 4 - Flag (1=prices only, 2=volumes only, 3=prices and volumes)
				*/
				switch (operatorType) {
				case 1: /// DIVISION
					if (factorFlag == 1) {		/// Prices ONLY
						factor								= 1.0f / factor;
						volumeFactor						= 1.0f;
					}
					else if (factorFlag == 2) { /// Volumes ONLY
						factor								= 1.0f;
						volumeFactor						= volumeFactor;
					}
					else if (factorFlag == 3) { /// Prices AND Volumes
						factor								= 1.0f / factor;
						volumeFactor						= volumeFactor;
					}
					else {
						ASSERT(false, "Unsupported factor flag: " << factorFlag);
					}
					break;
				case 2: /// MULTIPLICATION 
					if (factorFlag == 1) {		/// Prices ONLY
						factor								= factor;
						volumeFactor						= 1.0f;
					}
					else if (factorFlag == 2) { /// Volumes ONLY
						factor								= 1.0f;
						volumeFactor						= 1.0f / volumeFactor;
					}
					else if (factorFlag == 3) { /// Prices AND Volumes
						factor								= factor;
						volumeFactor						= 1.0f / volumeFactor;
					}
					else {
						ASSERT(false, "Unsupported factor flag: " << factorFlag);
					}
					break;
				//case 3: /// ADDITION
				//	if (factorFlag == 1) {		/// Prices ONLY
				//		factor								= factor;
				//		volumeFactor						= 1.0f;
				//	}
				//	else if (factorFlag == 2) { /// Volumes ONLY
				//		factor								= 1.0f;
				//		volumeFactor						= 1.0 / volumeFactor;
				//	}
				//	else if (factorFlag == 3) { /// Prices AND Volumes
				//		factor								= factor;
				//		volumeFactor						= 1.0 / volumeFactor;
				//	}
				//	else {
				//		ASSERT(false, "Unsupported factor flag: %d", factorFlag);
				//	}

				//	addFactor = true;
					//break;
				}
				/// Stock SPLIT and R-SPLIT
				//if (operatorType == 1 && factorFlag == 3) {
				//	volumeFactor							= factor;
				//	factor									= 1.0f / factor;
				//}
				//else
				//	volumeFactor							= 1.0f;

				(*m_dailyFactorsVerbatim)(diFactor, sec->index) *= factor;
				(*m_dailyVolFactorsVerbatim)(diFactor, sec->index) *= volumeFactor;

				volumeFactor								*= existingVolFactor;
				factor										*= existingFactor;

				(*m_dailyFactors)(diFactor, sec->index)		= factor;
				(*m_srcVolFactors)(diFactor, sec->index)	= volumeFactor;
				(*m_diDailyFactors)(diFactor, sec->index)	= diFactor;

				/// If we already have this value then we keep the latest timestamp
				if (iter != tsMap.end()) {
					auto prevTimestamp	= iter->second;
					if (prevTimestamp >= timestamp)
						continue;
				}

				/// Otherwise we just write the value
				m_adjFactors[sec->index] = factor;
				m_diAdjFactors[sec->index] = diFactor;

				tsMap[figi]				= timestamp;
			}
		}

		/// Backfill the matrix
		int rows = (int)m_dailyFactors->rows();
		int cols = (int)m_dailyFactors->cols();

		for (int ii = 0; ii < cols; ii++) {
			IntDay di = rows - 1;
			IntDay diFactor = di;
			float factor = (*m_dailyFactors)(di, ii);
			float volFactor = (*m_srcVolFactors)(di, ii);

			auto sec = secMaster->security(di, ii);

			if (std::isnan(factor)) {
				factor = 1.0f;
				volFactor = 1.0f;
			}

			while (di >= 0) {
				while (--di >= 0 && !m_dailyFactors->isValid(di, ii)) {
					(*m_dailyFactors)(di, ii) = factor;
					(*m_dailyVolFactors)(di, ii) = volFactor;
					(*m_diDailyFactors)(di, ii) = diFactor;
				}

				/// The day on which the factor changes also needs to use the FUTURE factor
				IntDay diDifferent = std::max(di, 0);

				float newFactor = (*m_dailyFactors)(diDifferent, ii); 
				float newVolFactor = (*m_srcVolFactors)(diDifferent, ii);

				(*m_dailyFactors)(diDifferent, ii) = factor;
				(*m_dailyVolFactors)(diDifferent, ii) = volFactor;
				(*m_diDailyFactors)(diDifferent, ii) = diFactor;

				if (di >= 0) {
					ASSERT(!std::isnan(newFactor), "Invalid factor!");
					diFactor = diDifferent;
					volFactor *= newVolFactor;
					factor *= newFactor;
				}
			}
		}

		CInfo("Updated BBAdjFactor: [%-8u]- Num Factors: %z, Num securities: %z", date, numValidFactors, size);
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createBBAdjFactor(const pesa::ConfigSection& config) {
	return new pesa::BBAdjFactor((const pesa::ConfigSection&)config);
}

