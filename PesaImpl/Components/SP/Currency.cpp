/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Currency.cpp
///
/// Created on: 18 Oct 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "SpGeneric.h"
#include "Poco/String.h"

namespace pesa {
	class Currency : public SpGeneric {
	private:
		StringVec				m_currencies;			/// The currencies that we have 
		std::string				m_datasetName = "ccy";	/// What is the name of the dataset

	protected:
		virtual std::string		buildSymbolsStr(StringVec& uuids, const DataCacheInfo& dci, IntDay di, size_t startUuidIndex, size_t endUuidIndex);

	public:
								Currency(const ConfigSection& config);
		virtual 				~Currency();

		////////////////////////////////////////////////////////////////////////////
		/// Currency overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			buildDaily(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual bool			optimiseForUniverse() { return false; }
	};

	Currency::Currency(const ConfigSection& config) 
		: SpGeneric(config) {
		setLogChannel("Currency");
		m_minfo.specifiedFieldsOnly = false;
		m_datasetName			= config.getString("datasetName", m_datasetName);
		std::string currencies	= parentConfig()->defs().resolve(config.getRequired("currency"), (Poco::DateTime*)nullptr);
		m_currencies			= Util::split(currencies, "|");
		ASSERT(m_currencies.size(), "Invalid currency: " << currencies);
	}

	Currency::~Currency() {
		CTrace("Deleting Currency");
	}

	void Currency::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		for (auto& currency : m_currencies) {
			datasets.add(Dataset(m_datasetName, {
				DataValue(currency + "_open", DataType::kFloat, sizeof(float), THIS_DATA_FUNC(SpGeneric::buildDaily), DataFrequency::kDaily, 0, reinterpret_cast<void*>(&currency)),
				DataValue(currency + "_close", DataType::kFloat, sizeof(float), THIS_DATA_FUNC(SpGeneric::buildDaily), DataFrequency::kDaily, 0, reinterpret_cast<void*>(&currency)),
				DataValue(currency + "_low", DataType::kFloat, sizeof(float), THIS_DATA_FUNC(SpGeneric::buildDaily), DataFrequency::kDaily, 0, reinterpret_cast<void*>(&currency)),
				DataValue(currency + "_high", DataType::kFloat, sizeof(float), THIS_DATA_FUNC(SpGeneric::buildDaily), DataFrequency::kDaily, 0, reinterpret_cast<void*>(&currency)),
			}));
		}
	}

	std::string Currency::buildSymbolsStr(StringVec& uuids, const DataCacheInfo& dci, IntDay di, size_t, size_t) {
		std::string symbolsStr = "dict(Symbology='BTKR',IDs=(";

		for (const auto& currency : m_currencies) {
			std::string ccyStr = currency + " Curncy";
			uuids.push_back(ccyStr);
			symbolsStr += PESA_QSTR(ccyStr) + ",";
		}

		symbolsStr += "))";
		return symbolsStr;
	}

	void Currency::buildDaily(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		try { 
			std::string rmappedName;
			IntDate date = dci.rt.dates[dci.di];
			SpRest::Result result = waitForResult(dr, dci, frame, rmappedName);

			if (!result.data.size()) {
				frame.noDataUpdate = true;
				return;
			}

			auto ccysData = result.data["SYMBOL"];

			if (!ccysData || !ccysData->cols()) {
				frame.noDataUpdate = true;
				return;
			}

			size_t ccyCount = ccysData->cols();
			const std::string* ccys = (dynamic_cast<StringData*>(ccysData.get()))->sptr();
			int index = -1;
			std::string* srcCcy = reinterpret_cast<std::string*>(dci.dv.customInfo);
			std::string ccyRequired = *srcCcy + " Curncy";

			ASSERT(ccyCount, "No currencies were received!");

			for (size_t i = 0; i < ccyCount && index < 0; i++) {
				const auto& ccy = ccys[i];
				if (ccy == ccyRequired) 
					index = (int)i;
			}


			FloatMatrixData* data = new FloatMatrixData(nullptr, dci.def, 1, 1);
			std::string dataName = dci.def.name;

			if (index >= 0 && index < (int)ccyCount) {
				/// OK now that we have the currency we can find the latest data ...
				std::string srcDataName;

				if (dataName == *srcCcy + "_open")
					srcDataName = "PX_OPEN";
				else if (dataName == *srcCcy + "_close")
					srcDataName = "PX_LAST";
				else if (dataName == *srcCcy + "_low")
					srcDataName = "PX_LOW";
				else if (dataName == *srcCcy + "_high")
					srcDataName = "PX_HIGH";
				else
					ASSERT(false, "Unrecognised data name: " << dataName);

				const DoubleMatrixData* values = dynamic_cast<const DoubleMatrixData*>(result.data[srcDataName].get());

				/// If we get the value for this currency back then we just use that 
				/// Otherwise, we set it to NaN so that 
				if (values) {
					double value = (*values)(0, index);
					(*data)(0, 0) = (float)value;
				}
				else {
					CWarning("[%u]-[%s] - No data on the date. Using last day's data ...", date, dataName);
					(*data)(0, 0) = std::nanf("");
				}
			}
			else {
				CWarning("[%u]-[%s] - No data on the date. Using last day's data ...", date, dataName);
				(*data)(0, 0) = std::nanf("");
			}

			//ASSERT(index >= 0 && index < (int)ccyCount, "Unable to find currency: " << ccyRequired);

			frame.data = FloatMatrixDataPtr(data);
			frame.dataOffset = 0;
			frame.dataSize = data->dataSize();

			CInfo("Updated currency: [%-8u]-[%s] - %s", date, *srcCcy, dataName);
		}
		catch (const std::exception& e) {
			Util::reportException(e);
		}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createCurrency(const pesa::ConfigSection& config) {
	return new pesa::Currency((const pesa::ConfigSection&)config);
}

