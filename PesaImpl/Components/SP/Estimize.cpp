/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Estimize.cpp
///
/// Created on: 23 Sep 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "SpGeneric.h"
#include "Poco/String.h"

namespace pesa {
	class Estimize : public SpGeneric {
	private:
		struct SecDataCache {
			int					fiscalYear;				/// The year that this is targetting
			int					fiscalQuarter;			/// The fiscal quarter that we're targetting
			StringVec			securities;				/// The securities
			SpRestBatchPtr		restData;				/// The REST data pointer
		};

		struct DailyData {
			int					fiscalYear, fiscalQuarter;
		};

		typedef std::shared_ptr<SecDataCache> SecDataCachePtr;

		typedef std::unordered_map<std::string, SecDataCachePtr> BamRestBatchPtrMap;
		typedef std::vector<SecDataCachePtr> SecDataCachePtrVec;
		typedef std::unordered_map<std::string, int> IndexMap;
		typedef std::unordered_map<std::string, DailyData> DailyDataMap;

		BamRestBatchPtrMap		m_secDataCache;			/// Data cache for each of the securities
		BamRestBatchPtrMap		m_globalSecDataCache;	/// All the data caches we sorted on fiscal years and quarters
		DailyDataMap			m_secDailyLUT;			/// The daily lookup table for each of the securities
		IndexMap				m_secIndexes;			/// Resolved indexes so that we don't have to iterate again ...
		std::string				m_fiscalYearDataId = "estimize.fiscal_year";
		std::string				m_fiscalQuarterDataId = "estimize.fiscal_quarter";
		std::string				m_reportsAtDataId = "estimize.c_reports_at";

		void					buildSecDataCache(const Security& sec, IDataRegistry& dr, const DataCacheInfo& dci, IntDay di, SecDataCachePtrVec& newItems);

		static inline std::string makeKey(const std::string secUuid, int fiscalYear, int fiscalQuarter) {
			return secUuid + "-" + Util::cast(fiscalYear) + "-" + Util::cast(fiscalQuarter);
		}

		static inline std::string makeKey(int fiscalYear, int fiscalQuarter) {
			return Util::cast(fiscalYear) + "-" + Util::cast(fiscalQuarter);
		}

	public:
								Estimize(const ConfigSection& config);
		virtual 				~Estimize();

		////////////////////////////////////////////////////////////////////////////
		/// Estimize overrides
		////////////////////////////////////////////////////////////////////////////
		SpGeneric::CacheInfoPtr preCacheAll(IDataRegistry& dr, const DataCacheInfo& dci, const std::string& keyExtra);
		virtual void			preCache(IDataRegistry& dr, const DataCacheInfo& dci, const std::string& keyExtra);
		virtual void			postDataBuild(IDataRegistry& dr, const pesa::DataCacheInfo& dci);
		virtual void 			buildDaily(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
	};

	Estimize::Estimize(const ConfigSection& config) 
		: SpGeneric(config) {
		setLogChannel("Estimize");
		m_minfo.specifiedFieldsOnly = false;
		m_fiscalYearDataId = config.getString("fiscalYearDataId", m_fiscalYearDataId);
		m_fiscalQuarterDataId = config.getString("fiscalQuarterDataId", m_fiscalQuarterDataId);
		m_reportsAtDataId = config.getString("reportsAtDataId", m_reportsAtDataId);
	}

	Estimize::~Estimize() {
		CTrace("Deleting Estimize");
	}

	void Estimize::postDataBuild(IDataRegistry& dr, const pesa::DataCacheInfo& dci) {
		SpGeneric::postDataBuild(dr, dci);
		m_globalSecDataCache.clear();
		m_secDataCache.clear();
		m_secIndexes.clear();
		m_secDailyLUT.clear();
	}

	void Estimize::buildSecDataCache(const Security& sec, IDataRegistry& dr, const DataCacheInfo& dci, IntDay di, SecDataCachePtrVec& newItems) {
		IntVarMatrixData* fiscalYears = dr.intVarData(dci.universe, m_fiscalYearDataId);
		ASSERT(fiscalYears, "Unable to load data: " << m_fiscalYearDataId);
			
		IntVarMatrixData* fiscalQuarters = dr.intVarData(dci.universe, m_fiscalQuarterDataId);
		ASSERT(fiscalQuarters, "Unable to load data: " << m_fiscalQuarterDataId);

		Int64VarMatrixData* reportsAt = dr.int64VarData(dci.universe, m_reportsAtDataId);
		ASSERT(reportsAt, "Unable to load data: " << m_reportsAtDataId);

		Security::Index ii = sec.index;
		int fiscalYear = 0, fiscalQuarter = 0;

		if (fiscalYears->getSize(di, ii))
			fiscalYear = fiscalYears->getValue(di, ii);

		if (fiscalQuarters->getSize(di, ii))
			fiscalQuarter = fiscalQuarters->getValue(di, ii);

		/// We don't have data for this security
		if (!fiscalYear || !fiscalQuarter)
			return;

		/// Over here we check what the report date for this particular security is. 
		/// If we are on the day of the report date then we need to get the data for the 
		/// next item in the fiscal quarter and fiscal year array. This is just the way
		/// the estimize data is organised. 
		IntDate currDate = dci.rt.dates[di];
		Poco::DateTime reportDateTime = reportsAt->getDTValue(di, ii);
		IntDate reportDate = Util::dateToInt(reportDateTime);

		if (di > 0 && currDate >= reportDate) {
			/// In this particular case we don't do anything
			fiscalYear = fiscalYear; /// JUST SO WE CAN PUT A BREAKPOINT
		}
		else {
			fiscalQuarter -= 1;

			if (fiscalQuarter == 0) {
				fiscalQuarter = 4;
				fiscalYear--;
			}
		}

		m_secDailyLUT[makeKey(sec.uuidStr(), (int)di, (int)sec.index)] = { fiscalYear, fiscalQuarter };

		std::string secUuid = sec.uuidStr();
		std::string key = Estimize::makeKey(secUuid, fiscalYear, fiscalQuarter);
		auto iter = m_secDataCache.find(key);

		if (iter != m_secDataCache.end())
			return;

		/// Otherwise search the global data cache ...
		std::string gkey = Estimize::makeKey(fiscalYear, fiscalQuarter);
		auto giter = m_globalSecDataCache.find(gkey);

		if (giter != m_globalSecDataCache.end()) {
			giter->second->securities.push_back(secUuid);
			m_secDataCache[key] = giter->second;
			return;
		}

		/// Otherwise we create a new one ...
		SecDataCachePtr newItem = std::make_shared<SecDataCache>();
		newItem->fiscalYear = fiscalYear;
		newItem->fiscalQuarter = fiscalQuarter;
		newItem->restData = SpRestBatchPtr(new SpRestBatch(config(), &m_dataRequired, &m_requiredDataTypes, m_minfo));
		newItem->securities.push_back(secUuid);

		m_globalSecDataCache[gkey] = newItem;
		m_secDataCache[key] = newItem;

		newItems.push_back(newItem);
	}

	SpGeneric::CacheInfoPtr Estimize::preCacheAll(IDataRegistry& dr, const DataCacheInfo& dci, const std::string& keyExtra) {
		SecDataCachePtrVec newItems;

		for (IntDay di = dci.di; di < dci.diEnd; di++) {
			size_t numSecurities = dci.universe->size(di);
			IntDate date = dci.rt.dates[di];
			ASSERT(numSecurities, "Universe has no securities on date: " << date);

			CDebug("[%u] [di = %d] Pre-calculating fiscal data", dci.rt.dates[di], di);

			for (size_t ii = 0; ii < numSecurities; ii++) {
				auto sec = dci.universe->security(di, (Security::Index)ii);
				buildSecDataCache(sec, dr, dci, di, newItems);
			}
		}

		for (auto newItem : newItems) {
			StringMap args;
			int fiscalQuarter = newItem->fiscalQuarter;
			int fiscalYear = newItem->fiscalYear;

			CDebug("Precaching: Fiscal Year: %d - Fiscal Quarter: %d - Num Securities: %z", fiscalYear, fiscalQuarter, newItem->securities.size());

			ASSERT(newItem->securities.size(), "Invalid! No securities in the batch!?");
			std::string symbolsStr = "dict(Symbology='CFIGI',IDs=(";

			for (auto& secId : newItem->securities) {
				symbolsStr += PESA_QSTR(secId) + ",";
			}

			symbolsStr += "))";

			ASSERT(fiscalQuarter >= 1 && fiscalQuarter <= 4, "Invalid fiscal quarter: " << fiscalQuarter);
			args["fiscal_year"] = Util::cast(fiscalYear);
			args["fiscal_quarter"] = Util::cast(fiscalQuarter);
			args["Symbols"] = symbolsStr;

			/// fire off the async request ...
			newItem->restData->getDataAsync(dci, args);
		}

		return nullptr;
	}

	void Estimize::preCache(IDataRegistry& dr, const DataCacheInfo& dci, const std::string& keyExtra) {
		if (m_diLast > dci.diEnd)
			return;

		//for (IntDay di = dci.di; di <= dci.diEnd; di++)
		preCacheAll(dr, dci, keyExtra);

		m_diLast = dci.diEnd + 1;
	}

	void Estimize::buildDaily(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		preCache(dr, dci, "");

		IntDay di = dci.di;
		IntDate date = dci.rt.dates[di];
		size_t numSecurities = dci.universe->size(di);
		ASSERT(numSecurities, "Universe has no securities on date: " << date);
		IntVarMatrixData* fiscalYears = dr.intVarData(dci.universe, m_fiscalYearDataId);
		ASSERT(fiscalYears, "Unable to load data: " << m_fiscalYearDataId);

		IntVarMatrixData* fiscalQuarters = dr.intVarData(dci.universe, m_fiscalQuarterDataId);
		ASSERT(fiscalQuarters, "Unable to load data: " << m_fiscalQuarterDataId);

		DataPtr newData = nullptr;
		std::vector<size_t> invalid;
		std::string dataName = dci.def.name;
		std::string rmappedName = SpGeneric::getRMappedName(dataName);
		size_t numInvalid = 0, numRemapped = 0;
		size_t numFound = 0;

		for (size_t ii = 0; ii < numSecurities; ii++) {
			auto sec = dci.universe->security(di, (Security::Index)ii);
			auto secUuid = sec.uuidStr();
			//int fiscalYear = 0, fiscalQuarter = 0;

			//if (fiscalYears->getSize(di, ii))
			//	fiscalYear = fiscalYears->getValue(di, ii);

			//if (fiscalQuarters->getSize(di, ii))
			//	fiscalQuarter = fiscalQuarters->getValue(di, ii);

			auto ddInfo = m_secDailyLUT[makeKey(secUuid, (int)dci.di, (int)sec.index)];
			auto key = makeKey(secUuid, ddInfo.fiscalYear, ddInfo.fiscalQuarter);
			auto iter = m_secDataCache.find(key);

			/// If we don't have this with us ...
			if (iter == m_secDataCache.end()) {
				/// The reason why we push it into an array is that newData might not be a valid pointer
				/// until we encounter a valid value for a UUID. Hence we fill newData with invalid values
				/// at the very end. When we're done with this loop ...
				invalid.push_back(ii);
				continue;
			}

			/// Otherwise we just get the results ...
			SecDataCachePtr item = iter->second;
			item->restData->wait();
			auto results = item->restData->results();
			ASSERT(results.size() == 1, "Expecting results to be of size 1 but found: " << results.size());
			SpRest::Result& result = results[0];

			auto figiData = result.data[s_symbolColName];

			if (!figiData) {
				invalid.push_back(ii);
				continue;
			}

			size_t figiCount = figiData->cols();
			const std::string* figis = (dynamic_cast<StringData*>(figiData.get()))->sptr();
			DataPtr data = result.data[rmappedName];

			ASSERT(data, "Unable to find data: " << rmappedName << " - for security: " << secUuid);

			if (!newData) {
				newData = DataPtr(data->clone(data->universe(), true));
				newData->ensureSize(1, numSecurities);
			}

			std::string secIndexKey = key + "-" + rmappedName;
			auto secIndexIter = m_secIndexes.find(secIndexKey);
			int srcIndex = -1;

			if (secIndexIter == m_secIndexes.end()) {
				for (size_t fid = 0; fid < figiCount && srcIndex ; fid++) {
					const auto& figi = figis[fid];
					if (figi == secUuid) 
						srcIndex = (int)fid;
				}

				m_secIndexes[secIndexKey] = srcIndex;
			}
			else
				srcIndex = secIndexIter->second;

			if (srcIndex < 0) {
				invalid.push_back(ii);
				continue;
			}

			numFound++;
			const auto& figi = figis[srcIndex];
			SpGeneric::copyElement(dci.universe, figi, data, (size_t)srcIndex, newData.get(), numInvalid, numRemapped);
		}

		if (!newData) {
			frame.noDataUpdate = true;
			return;
		}

		ASSERT(newData, "Unable to create data: " << rmappedName << " [" << dataName << "]");

		///// Now we set the invalid values ...
		//for (auto ii : invalid)
		//	newData->makeInvalid(0, ii);

		frame.data = DataPtr(newData);
		frame.dataOffset = 0;
		frame.dataSize = frame.data->dataSize();

		CInfo("Updated data: [%-8u]-[Found: %5z / %5z - Invalid: %5z] - %s", date, numFound, numSecurities, numInvalid, dci.def.toString(dci.ds));
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createEstimize(const pesa::ConfigSection& config) {
	return new pesa::Estimize((const pesa::ConfigSection&)config);
}

