/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// IBES.cpp
///
/// Created on: 14 Mar 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "MultiCodeDataTables.h"
#include "Poco/String.h"

namespace pesa {
	class IBES : public MultiCodeDataTables {
	protected:
		typedef std::unordered_map<IntDay, CacheInfoPtr> DataCacheLookup;
		bool					m_needsArgs = true;
		StringVec				m_detailedIndexes;

		SizeVec					getMultiColumnMatchInfo(IDataRegistry& dr, const DataCacheInfo& dci, SpRest::Result& result);
		virtual CacheInfoPtr	preCacheDay(IDataRegistry& dr, const DataCacheInfo& dci, IntDay di, const std::string& keyExtra);

		virtual std::string		getTableName() const;
		virtual std::string		getKeyExtra(IDataRegistry& dr, const DataCacheInfo& dci, IntDate date) const { return m_needsArgs ? MultiCodeDataTables::getKeyExtra(dr, dci, date) : ""; }
		virtual bool			hasMultiColumnMatchInfo() const { return true; }

	public:
								IBES(const ConfigSection& config);
		virtual 				~IBES();

		////////////////////////////////////////////////////////////////////////////
		/// IBES overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
	};

	IBES::IBES(const ConfigSection& config) : MultiCodeDataTables(config) {
		m_needsArgs				= config.getBool("needsArgs", m_needsArgs);
	}

	IBES::~IBES() {
		CTrace("Deleting IBES");
	}

	std::string IBES::getTableName() const {
		return config().getRequired("tableName");
	}

	struct PCodeInfo {
		int periodType = -1;
		int periodCode = -1;
		std::string periodDesc;
	};

	typedef std::vector<PCodeInfo> PCodeInfoVec;

	inline std::string pkey(int periodType, int periodCode) {
		return Util::cast(periodType) + "_" + Util::cast(periodCode);
	}

	void IBES::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		MultiCodeDataTables::initCodes(dr);

		std::string keyword = getTableName();
		StringData* periodDesc = dr.stringData(nullptr, m_datasetName + ".periodDesc");
		IntMatrixData* periodCode = dr.intData(nullptr, m_datasetName + ".periodCode");
		IntMatrixData* periodType = dr.intData(nullptr, m_datasetName + ".periodType");
		std::map<int, PCodeInfoVec> periodTypeToCodeLookup;
		std::string fixedPeriodsStr = config().getString("periods", "");
		size_t numCodes = m_codes.size();

		if (fixedPeriodsStr.empty()) {
			for (size_t ii = 0; ii < periodType->cols(); ii++) {
				PCodeInfo pci;
				pci.periodType = (*periodType)(0, ii);
				pci.periodCode = (*periodCode)(0, ii);
				pci.periodDesc = periodDesc->getValue(0, ii);

				std::string key = pkey(pci.periodType, pci.periodCode);

				periodTypeToCodeLookup[pci.periodType].push_back(pci);
			}
		}
		else {
			StringVec fixedPeriods = Util::split(fixedPeriodsStr, "|");
			std::map<int, PCodeInfoVec> pciLookup;
			PCodeInfoVec baseVec;
			std::map<std::string, std::string> periodCodeToDescLookup;

			for (size_t pti = 0; pti < periodType->cols(); pti++) {
				int pt = (*periodType)(0, pti);

				for (size_t ii = 0; ii < periodCode->cols(); ii++) {
					int pc = (*periodCode)(0, ii);
					std::string pd = periodDesc->getValue(0, ii);
					std::string key = pkey(pt, pc);
					periodCodeToDescLookup[key] = pd;
				}
			}

			for (size_t i = 0; i < fixedPeriods.size(); i++) {
				const std::string& periodInfoStr = fixedPeriods[i];
				auto periodInfoTuple = Util::split(periodInfoStr, "=");
				ASSERT(periodInfoTuple.size() == 2, "Invalid period info tuple: " << periodInfoStr);

				std::string lhs = periodInfoTuple[0];
				auto rhs = Util::split(periodInfoTuple[1], ",");

				PCodeInfoVec pciVec;

				for (size_t i = 0; i < rhs.size(); i++) {
					PCodeInfo pci;
					pci.periodCode = Util::cast<int>(rhs[i]);
					pciVec.push_back(pci);
				}

				if (lhs != "*")
					pciLookup[Util::cast<int>(lhs)] = pciVec;
				else
					baseVec = pciVec;
			}

			for (size_t ci = 0; ci < numCodes; ci++) {
				const Code& code = m_codes[ci];
				auto citer = pciLookup.find(code.code1);
				PCodeInfoVec pciVec = baseVec;

				if (citer != pciLookup.end()) {
					PCodeInfoVec& vec = citer->second;
					pciVec.insert(pciVec.end(), vec.begin(), vec.end());
				}

				for (size_t i = 0; i < pciVec.size(); i++) {
					std::string key = pkey(code.code1, pciVec[i].periodCode);

					pciVec[i].periodDesc = periodCodeToDescLookup[key];
					ASSERT(!pciVec[i].periodDesc.empty(), "Unable to find desc for period type: " << pciVec[i].periodType << " and period code: " << pciVec[i].periodCode);
					pciVec[i].periodType = code.code1;
				}

				periodTypeToCodeLookup[code.code1] = pciVec;
			}
		}


		/// Now for each of the codes, we will see get the corresponding list of Periods
		size_t numDatasetDefs = m_datasetDefs.size();
		Dataset ds(m_datasetName);

		for (size_t ci = 0; ci < numCodes; ci++) {
			const Code& code = m_codes[ci];
			std::string code1Id = code.code1 >= 0 ? m_code1Lookup[Util::cast(code.code1)] : "";
			std::string code2Id = code.code2 >= 0 ? m_code2Lookup[Util::cast(code.code2)] : "";
			auto pcIter = periodTypeToCodeLookup.find(code.code1);

			if (pcIter != periodTypeToCodeLookup.end()) {
				PCodeInfoVec pciVec = pcIter->second;

				for (size_t pcii = 0; pcii < pciVec.size(); pcii++) {
					const PCodeInfo& pci = pciVec[pcii];

					std::string index = Util::cast(code.code2) + "," + Util::cast(code.code1);
					std::string detailedIndex = index + "," + Util::cast(pci.periodCode);

					m_indexes.push_back(index);
					m_detailedIndexes.push_back(detailedIndex);

					for (size_t dsi = 0; dsi < numDatasetDefs; dsi++) {
						const DatasetDef& dsDef = m_datasetDefs[dsi];

						std::string datasetName = MultiCodeDataTables::makeName(dsDef.name, keyword, code.code2, code2Id, code.code1, code1Id) + "-p" + Util::cast(pci.periodCode) + "-" + pci.periodDesc;
						DataValue dv(datasetName, dsDef.type, DataType::size(dsDef.type), THIS_DATA_FUNC(IBES::buildDaily), code.frequency, m_alwaysOverwrite ? DataFlags::kAlwaysOverwrite : 0);
						dv.customInfo = reinterpret_cast<void*>(m_indexes.size());

						m_rmappings[datasetName] = dsDef.name;

						ds.add(dv);

						m_dataRequired.push_back(dsDef.name);
						m_requiredDataTypes.push_back(dv.definition.itemType);
						m_requiredDataFrequencies.push_back((DataFrequency::Type)dv.definition.frequency);
					}
				}
			}
		}

		m_dataRequired.push_back(m_symbolColName);
		m_requiredDataTypes.push_back(DataType::kString); /// For the CFIGI

		datasets.add(ds);

		m_dataRequired.push_back("Measure");
		m_requiredDataTypes.push_back(DataType::kInt); 

		m_dataRequired.push_back("PerType");
		m_requiredDataTypes.push_back(DataType::kInt);

		m_dataRequired.push_back("Period");
		m_requiredDataTypes.push_back(DataType::kInt);
	}

	SpGeneric::CacheInfoPtr IBES::preCacheDay(IDataRegistry& dr, const DataCacheInfo& dci, IntDay di, const std::string& keyExtra) {
		if (m_needsArgs)
			SpGeneric::preCacheDay(dr, dci, di, keyExtra);

		return SpGeneric::preCacheDay(dr, dci, di, "");

	}

	SizeVec IBES::getMultiColumnMatchInfo(IDataRegistry& dr, const DataCacheInfo& dci, SpRest::Result& result) {
		ASSERT(dci.dv.customInfo, "Invalid string as part of the data value custom info: " << dci.dv.toString(dci.ds));
		size_t indexId = reinterpret_cast<size_t>(dci.dv.customInfo) - 1;

		ASSERT(indexId < m_detailedIndexes.size(), "Invalid custom info index: " << indexId);
		std::string& index = m_detailedIndexes[indexId];

		auto parts = Util::split(index, ",");
		auto measure = Util::cast<int>(parts[0]);
		auto perType = Util::cast<int>(parts[1]);
		auto period = Util::cast<int>(parts[2]);

		DataPtr perTypeDataPtr = result.data["PerType"];
		DataPtr measureDataPtr = result.data["Measure"];
		DataPtr periodDataPtr = result.data["Period"];

		ASSERT(perTypeDataPtr && measureDataPtr && periodDataPtr, "Unable to get PerType|Measure|Period data!"); 
		const IntMatrixData* perTypeData = dynamic_cast<IntMatrixData*>(perTypeDataPtr.get());
		const IntMatrixData* measureData = dynamic_cast<IntMatrixData*>(measureDataPtr.get());
		const IntMatrixData* periodData = dynamic_cast<IntMatrixData*>(periodDataPtr.get());

		ASSERT(perTypeData && measureData && periodData, "Unable to cast PerType|Measure|Period data to integer matrix!");
		SizeVec colInfo;
		size_t count = std::min<size_t>(perTypeData->cols(), measureData->cols()); /// These will be the same but I'm paranoid

		for (size_t ii = 0; ii < count; ii++) {
			auto perTypeInfo = perTypeData->getValue(0, ii);
			auto measureInfo = measureData->getValue(0, ii);
			auto periodInfo = periodData->getValue(0, ii);

			if (perTypeInfo == perType && measureInfo == measure && periodInfo == period)
				colInfo.push_back(ii);
		}

		return colInfo;
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createIBES(const pesa::ConfigSection& config) {
	return new pesa::IBES((const pesa::ConfigSection&)config);
}

