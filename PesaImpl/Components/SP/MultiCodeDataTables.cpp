/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// MultiCodeDataTables.cpp
///
/// Created on: 14 Mar 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "MultiCodeDataTables.h"
#include "Poco/String.h"

namespace pesa {
	MultiCodeDataTables::MultiCodeDataTables(const ConfigSection& config) 
		: SpGeneric(config) {
		setLogChannel("MultiCodeDataTables");
		m_maxItems = config.get<size_t>("maxItems", m_maxItems);
		m_minfo.specifiedFieldsOnly = false;

		m_code1Name			= config.getString("code1Name", m_code1Name);
		m_code1Prefix		= config.getString("code1Prefix", m_code1Prefix);
		m_code1IdName		= config.getString("code1IdName", m_code1IdName);
		m_code1QueryId		= config.getString("code1QueryId", m_code1QueryId);

		m_code2Name			= config.getString("code2Name", m_code2Name);
		m_code2Prefix		= config.getString("code2Prefix", m_code2Prefix);
		m_code2IdName		= config.getString("code2IdName", m_code2IdName);
		m_code2QueryId		= config.getString("code2QueryId", m_code2QueryId);

		m_codeForFrequency	= config.getString("codeForFrequency", m_codeForFrequency);
		auto frequencyMapping = config.getString("frequencyMapping", "");

		if (!frequencyMapping.empty()) {
			auto fmaps		= Util::split(frequencyMapping, ",");
			for (const auto& fmap : fmaps) {
				auto mapping= Util::split(fmap, "=");
				ASSERT(mapping.size() == 2, "Invalid frequency mapping: " << fmap);

				DataFrequency::Type frequency = DataFrequency::parse(mapping[1]);
				m_frequencyMap[mapping[0]] = frequency;
			}
		}

		initCodesAndDefs();
	}

	MultiCodeDataTables::~MultiCodeDataTables() {
		CTrace("Deleting MultiCodeDataTables");
	}

	std::string MultiCodeDataTables::makeName(const std::string& dvName, const std::string& tableName, int code2, const std::string& code2Id, int code1, const std::string& code1Id) const {
		std::string name = tableName + "_" + dvName;

		if (!code2Id.empty())
			name += "_" + code2Id + "-mc" + Util::cast(code2);

		if (!code1Id.empty())
			name += "_" + code1Id + "-pc" + Util::cast(code1);

		return name;
	}

	void MultiCodeDataTables::initCodesAndDefs() {
		auto frequency = config().getString("frequency", "daily");
		m_frequency = DataFrequency::parse(frequency);

		m_code1Data = config().getString("code1Data", "");
		m_code2Data = config().getString("code2Data", "");

		if (m_code2Data.empty() && m_code2Data.empty()) {
			auto codesStr = config().getRequired("codes");
			auto codes = Util::split(codesStr, "|");

			for (auto code : codes) {
				Code c;
				char frequencyStr[128];

				sscanf(code.c_str(), "(%d, %d).%s", &c.code1, &c.code2, frequencyStr);

				std::string str = frequencyStr;
				c.frequency = DataFrequency::parse(str);

				m_codes.push_back(c);
			}
		}

		auto datasetsStr = config().getRequired("datasets");
		auto datasets = Util::split(datasetsStr, ",");

		for (auto dataset : datasets) {
			DatasetDef dsDef;
			DataFrequency::Type frequency;

			DataDefinition::parseDefString(dataset, dsDef.name, dsDef.type, frequency);

			m_datasetDefs.push_back(dsDef);
		}
	}

	std::string MultiCodeDataTables::getKeyExtra(IDataRegistry& dr, const DataCacheInfo& dci, IntDate date) const {
		ASSERT(dci.dv.customInfo, "Invalid string as part of the data value custom info: " << dci.dv.toString(dci.ds));
		size_t indexId = reinterpret_cast<size_t>(dci.dv.customInfo) - 1;
		ASSERT(indexId < m_indexes.size(), "Invalid custom info index: " << indexId);
		return m_indexes[indexId];
	}

	SpGeneric::CacheInfoPtr MultiCodeDataTables::preCacheDay(IDataRegistry& dr, const DataCacheInfo& dci, IntDay di, const std::string& keyExtra) {
		ASSERT(dci.dv.customInfo, "Invalid string as part of the data value custom info: " << dci.dv.toString(dci.ds));
		size_t indexId = reinterpret_cast<size_t>(dci.dv.customInfo) - 1;

		ASSERT(indexId < m_indexes.size(), "Invalid custom info index: " << indexId);
		std::string& index = m_indexes[indexId];

		auto parts = Util::split(index, ",");
		auto code2 = Util::cast<int>(parts[0]);
		auto code1 = Util::cast<int>(parts[1]);

		if (code2 >= 0)
			m_additionalArgs[m_code2QueryId] = "(" + parts[0]+ ",)";

		if (code1 >= 0)
			m_additionalArgs[m_code1QueryId] = "(" + parts[1] + ",)";

		/// The keyExtra parameter is our items string. These are the items that we're fetching in the current iteration!
		return SpGeneric::preCacheDay(dr, dci, di, index);
	}

	static IntMatrixDataPtr makeTmpData(const std::string& codeListStr) {
		auto codeList = Util::split(codeListStr, ",");
		IntMatrixDataPtr tmpData = std::make_shared<IntMatrixData>((IUniverse*)nullptr, DataDefinition(), 1, codeList.size());
		for (size_t i = 0; i < codeList.size(); i++)
			(*tmpData)(0, i) = Util::cast<int>(codeList[i]);

		return tmpData;
	}

	void MultiCodeDataTables::initCodesFromDataIds(IDataRegistry& dr) {
		DataInfo dinfo;
		dinfo.noAssertOnMissingDataLoader = true;

		DataPtr code1Data = nullptr, code2Data = nullptr;

		if (!m_code1Data.empty())
			code1Data = dr.getDataPtr(nullptr, m_datasetName + "." + m_code1Data, &dinfo);
		else {
			std::string code1ListStr = config().getString("code1List", "");
			if (!code1ListStr.empty()) 
				code1Data = makeTmpData(code1ListStr);
		}

		if (!m_code2Data.empty())
			code2Data = dr.getDataPtr(nullptr, m_datasetName + "." + m_code2Data, &dinfo);
		else {
			std::string code2ListStr = config().getString("code2List", "");
			if (!code2ListStr.empty())
				code2Data = makeTmpData(code2ListStr);
		}

		size_t numCode1Cols = code1Data ? code1Data->cols() : 0;
		size_t numCode2Cols = code2Data ? code2Data->cols() : 0;

		if (numCode1Cols && numCode2Cols) {
			for (size_t i = 0; i < numCode1Cols; i++) {
				Code c;
				c.code1 = Util::cast<int>(code1Data->getString(0, i, 0));

				for (size_t j = 0; j < numCode2Cols; j++) {
					c.code2 = Util::cast<int>(code2Data->getString(0, j, 0));
					c.frequency = m_frequency;
					m_codes.push_back(c);
				}
			}
		}
		else if (numCode1Cols) {
			for (size_t i = 0; i < numCode1Cols; i++) {
				Code c;
				c.code1 = Util::cast<int>(code1Data->getString(0, i, 0));
				c.code2 = -1;
				c.frequency = m_frequency;
				m_codes.push_back(c);
			}
		}
		else if (numCode2Cols) {
			for (size_t j = 0; j < numCode2Cols; j++) {
				Code c;
				c.code1 = -1;
				c.code2 = Util::cast<int>(code2Data->getString(0, j, 0));
				c.frequency = m_frequency;
				m_codes.push_back(c);
			}
		}
		else {
			/// Otherwise it doesn't use any codes at all
			Code c;
			c.code1 = -1;
			c.code2 = -1;
			c.frequency = m_frequency;
			m_codes.push_back(c);
		}
	}

	std::string MultiCodeDataTables::getTableName() const {
		return config().getRequired("keyword");
	}

	void MultiCodeDataTables::initCodes(IDataRegistry& dr) {
		/// Here is where we'll fix the mappings

		if (!m_code1Data.empty() || !m_code2Data.empty()) {
			initCodesFromDataIds(dr);
		}

		if (!m_code2Lookup.size() && !m_code1Lookup.size()) {
			DataPtr code2Ids	= dr.getDataPtr(nullptr, m_datasetName + "." + m_code2IdName, (DataInfo*)nullptr);
			DataPtr code2Codes	= dr.getDataPtr(nullptr, m_datasetName + "." + m_code2Name, (DataInfo*)nullptr);
			DataPtr code1Ids	= dr.getDataPtr(nullptr, m_datasetName + "." + m_code1IdName, (DataInfo*)nullptr);
			DataPtr code1Codes	= dr.getDataPtr(nullptr, m_datasetName + "." + m_code1Name, (DataInfo*)nullptr);
			typedef std::map<std::string, std::string> StrMap;

			ASSERT(code2Ids->cols() == code2Codes->cols(), "Invalid measure code data!");
			ASSERT(code1Ids->cols() == code1Codes->cols(), "Invalid period code data!");

			for (size_t ii = 0; ii < code1Codes->cols(); ii++) {
				std::string code1 = code1Codes->getString(0, ii, 0);
				std::string code1Id = code1Ids->getString(0, ii, 0);
				m_code1Lookup[code1] = code1Id;
			}

			for (size_t ii = 0; ii < code2Codes->cols(); ii++) {
				std::string code2 = code2Codes->getString(0, ii, 0);
				std::string code2Id = code2Ids->getString(0, ii, 0);
				m_code2Lookup[code2] = code2Id;
			}
		}
	}

	void MultiCodeDataTables::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		initCodes(dr);

		std::string keyword = getTableName();
		size_t numCodes = m_codes.size();
		size_t numDatasetDefs = m_datasetDefs.size();
		Dataset ds(m_datasetName);

		for (size_t ci = 0; ci < numCodes; ci++) {
			const Code& code = m_codes[ci];
			std::string code1Id = code.code1 >= 0 ? m_code1Lookup[Util::cast(code.code1)] : "";
			std::string code2Id = code.code2 >=0 ? m_code2Lookup[Util::cast(code.code2)] : "";

			std::string index = Util::cast(code.code2) + "," + Util::cast(code.code1);
			m_indexes.push_back(index);

			for (size_t dsi = 0; dsi < numDatasetDefs; dsi++) {
				const DatasetDef& dsDef = m_datasetDefs[dsi];

				std::string datasetName = makeName(dsDef.name, keyword, code.code2, code2Id, code.code1, code1Id);
				DataValue dv(datasetName, dsDef.type, DataType::size(dsDef.type), THIS_DATA_FUNC(SpGeneric::buildDaily), code.frequency, m_alwaysOverwrite ? DataFlags::kAlwaysOverwrite : 0);
				dv.customInfo = reinterpret_cast<void*>(m_indexes.size());

				m_rmappings[datasetName] = dsDef.name;

				ds.add(dv);

				m_dataRequired.push_back(dsDef.name);
				m_requiredDataTypes.push_back(dv.definition.itemType);
				m_requiredDataFrequencies.push_back((DataFrequency::Type)dv.definition.frequency);
			}
		}

		m_dataRequired.push_back(m_symbolColName);
		m_requiredDataTypes.push_back(DataType::kString); /// For the CFIGI

		datasets.add(ds);
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createMultiCodeDataTables(const pesa::ConfigSection& config) {
	return new pesa::MultiCodeDataTables((const pesa::ConfigSection&)config);
}

