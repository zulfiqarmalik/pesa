/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// MultiCodeDataTables.h
///
/// Created on: 29 Aug 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "SpGeneric.h"
#include "Poco/String.h"

namespace pesa {
	class MultiCodeDataTables : public SpGeneric {
	protected:
		struct Code {
			int					code1;					/// The first code
			int					code2;					/// The second code
			DataFrequency::Type frequency;				/// What is the frequency of this pair
		};

		struct DatasetDef {
			DataType::Type		type;					/// What is the type of the data value
			std::string			name;					/// What is the name of the data value
		};

		typedef std::map<std::string, std::string> StrMap;
		typedef std::map<std::string, DataFrequency::Type> FrequencyMap;

		size_t					m_maxItems = 50;
		StringVec				m_indexes;				/// The indexes that we will read back from the customInfo
		StrMap					m_code1Lookup;			/// Code1 code to ID lookup (for better variable names)
		StrMap					m_code2Lookup;			/// Code2 code to ID lookup (for better variable names)
		std::vector<DatasetDef> m_datasetDefs;			/// The definition of the datasets
		std::vector<Code>		m_codes;				/// What are the codes
		std::string				m_code1Data;
		std::string				m_code2Data;
		DataFrequency::Type		m_frequency = DataFrequency::kDaily;

		std::string				m_code1Name		= "periodType";
		std::string				m_code1Prefix	= "pt";
		std::string				m_code1IdName	= "periodDesc";
		std::string				m_code1QueryId	= "PerTypes";

		std::string				m_code2Name		= "measureCode";
		std::string				m_code2Prefix	= "mc";
		std::string				m_code2IdName	= "measureId";
		std::string				m_code2QueryId	= "Measures";

		std::string				m_codeForFrequency = "";
		FrequencyMap			m_frequencyMap;

		virtual std::string		getTableName() const;
		virtual void 			initCodes(IDataRegistry& dr);

	protected:
		virtual std::string		getKeyExtra(IDataRegistry& dr, const DataCacheInfo& dci, IntDate date) const;
		virtual CacheInfoPtr	preCacheDay(IDataRegistry& dr, const DataCacheInfo& dci, IntDay di, const std::string& keyExtra);

		std::string				makeName(const std::string& dvName, const std::string& tableName, int code2, const std::string& code2Id, int code1, const std::string& code1Id) const;

		void					initCodesAndDefs();
		void 					initCodesFromDataIds(IDataRegistry& dr);

	public:
								MultiCodeDataTables(const ConfigSection& config);
		virtual 				~MultiCodeDataTables();

		////////////////////////////////////////////////////////////////////////////
		/// MultiCodeDataTables overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
	};
} /// namespace pesa
