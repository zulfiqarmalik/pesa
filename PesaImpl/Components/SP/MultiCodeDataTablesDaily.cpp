/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// MultiCodeDataTablesDaily.cpp
///
/// Created on: 14 Mar 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "MultiCodeDataTables.h"
#include "Poco/String.h"

namespace pesa {
	class MultiCodeDataTablesDaily : public MultiCodeDataTables {
	protected:
		typedef std::unordered_map<IntDay, CacheInfoPtr> DataCacheLookup;

		SizeVec					getMultiColumnMatchInfo(IDataRegistry& dr, const DataCacheInfo& dci, SpRest::Result& result);
		virtual CacheInfoPtr	preCacheDay(IDataRegistry& dr, const DataCacheInfo& dci, IntDay di, const std::string& keyExtra);

		virtual std::string		getTableName() const;
		virtual std::string		getKeyExtra(IDataRegistry& dr, const DataCacheInfo& dci, IntDate date) const { return SpGeneric::getKeyExtra(dr, dci, date); }
		virtual bool			hasMultiColumnMatchInfo() const { return true; }

	public:
								MultiCodeDataTablesDaily(const ConfigSection& config);
		virtual 				~MultiCodeDataTablesDaily();

		////////////////////////////////////////////////////////////////////////////
		/// MultiCodeDataTablesDaily overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
	};

	MultiCodeDataTablesDaily::MultiCodeDataTablesDaily(const ConfigSection& config) 
		: MultiCodeDataTables(config) {
	}

	MultiCodeDataTablesDaily::~MultiCodeDataTablesDaily() {
		CTrace("Deleting MultiCodeDataTablesDaily");
	}

	std::string MultiCodeDataTablesDaily::getTableName() const {
		return config().getRequired("tableName");
	}

	void MultiCodeDataTablesDaily::initDatasets(IDataRegistry& dr, Datasets& dss) {
		MultiCodeDataTables::initDatasets(dr, dss);

		for (Dataset& ds : dss.datasets) {
			for (DataValue& dv : ds.values) {
				dv.callback = THIS_DATA_FUNC(MultiCodeDataTablesDaily::buildDaily);
			}
		}

		m_dataRequired.push_back("Measure");
		m_requiredDataTypes.push_back(DataType::kInt); 

		m_dataRequired.push_back("PerType");
		m_requiredDataTypes.push_back(DataType::kInt);
	}

	SpGeneric::CacheInfoPtr MultiCodeDataTablesDaily::preCacheDay(IDataRegistry& dr, const DataCacheInfo& dci, IntDay di, const std::string& keyExtra) {
		return SpGeneric::preCacheDay(dr, dci, di, "");
	}

	SizeVec MultiCodeDataTablesDaily::getMultiColumnMatchInfo(IDataRegistry& dr, const DataCacheInfo& dci, SpRest::Result& result) {
		ASSERT(dci.dv.customInfo, "Invalid string as part of the data value custom info: " << dci.dv.toString(dci.ds));
		size_t indexId = reinterpret_cast<size_t>(dci.dv.customInfo) - 1;

		ASSERT(indexId < m_indexes.size(), "Invalid custom info index: " << indexId);
		std::string& index = m_indexes[indexId];

		auto parts = Util::split(index, ",");
		auto code2 = Util::cast<int>(parts[0]);
		auto code1 = Util::cast<int>(parts[1]);

		DataPtr code1DataPtr = result.data["PerType"];
		DataPtr code2DataPtr = result.data["Measure"];

		ASSERT(code1DataPtr && code2DataPtr, "Unable to get code1 and code2 data!"); 
		const IntMatrixData* code1Data = dynamic_cast<IntMatrixData*>(code1DataPtr.get());
		const IntMatrixData* code2Data = dynamic_cast<IntMatrixData*>(code2DataPtr.get());

		ASSERT(code1Data && code2Data, "Unable to cast code1 and code2 data to integer matrix!");
		SizeVec colInfo;
		size_t count = std::min<size_t>(code1Data->cols(), code2Data->cols()); /// These will be the same but I'm paranoid

		for (size_t ii = 0; ii < count; ii++) {
			auto code1Info = code1Data->getValue(0, ii);
			auto code2Info = code2Data->getValue(0, ii);

			if (code1Info == code1 && code2Info == code2)
				colInfo.push_back(ii);
		}

		return colInfo;
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createMultiCodeDataTablesDaily(const pesa::ConfigSection& config) {
	return new pesa::MultiCodeDataTablesDaily((const pesa::ConfigSection&)config);
}

