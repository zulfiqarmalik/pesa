/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// PricesAdjFactor.cpp
///
/// Created on: 07 May 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "SpGeneric.h"
#include "Poco/String.h"

namespace pesa {
	class PricesAdjFactor : public IDataLoader { 
	private:
		FloatMatrixDataPtr		m_cumFactor;			/// Cumulative factor

		void					buildDailyFactor(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		void					buildCumulativeFactor(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);

	public:
								PricesAdjFactor(const ConfigSection& config);
		virtual 				~PricesAdjFactor();

		////////////////////////////////////////////////////////////////////////////
		/// PricesAdjFactor overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {}
	};

	PricesAdjFactor::PricesAdjFactor(const ConfigSection& config) 
		: IDataLoader(config, "PricesAdjFactor") {
	}

	PricesAdjFactor::~PricesAdjFactor() {
		CTrace("Deleting PricesAdjFactor");
	}

	void PricesAdjFactor::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		std::string datasetName			= config().getRequired("datasetName");
		std::string dataName			= config().getRequired("dataName");
		bool dailyFactor				= config().getBool("dailyFactor", true);

		if (dailyFactor) {
			datasets.add(Dataset(datasetName, {
				DataValue(dataName + "Daily", DataType::kFloat, sizeof(float), THIS_DATA_FUNC(PricesAdjFactor::buildDailyFactor), DataFrequency::kDaily, 0),
			}));
		}
		else {
			datasets.add(Dataset(datasetName, {
				DataValue(dataName, DataType::kFloat, sizeof(float), THIS_DATA_FUNC(PricesAdjFactor::buildCumulativeFactor), DataFrequency::kDaily, DataFlags::kAlwaysOverwrite),
			}));
		}
	}

	void PricesAdjFactor::buildCumulativeFactor(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		if (!m_cumFactor) {
			std::string datasetName		= config().getRequired("datasetName");
			std::string dataName		= config().getRequired("dataName");
			const auto* dailyFactor		= dr.floatData(dci.universe, datasetName + "." + dataName + "Daily");

			IntDay rows					= (IntDay)dailyFactor->rows();
			size_t cols					= dailyFactor->cols();
			m_cumFactor					= std::make_shared<FloatMatrixData>(dci.universe, dci.def, rows, cols);

			int di						= (int)rows - 1;
			(*m_cumFactor)[rows - 1]	= (*dailyFactor)[rows - 1];

			while (di > 0) {
				(*m_cumFactor)[di - 1]	= (*m_cumFactor)[di] * (*dailyFactor)[di - 1];
				di--;
			}
		}

		size_t cols						= dci.universe->size(dci.di);
		FloatMatrixData* data			= new FloatMatrixData(dci.universe, dci.def, 1, cols);

		(*data)[0]						= (*m_cumFactor)[dci.di];
		frame.noDataUpdate				= false;
		frame.data						= DataPtr(data);
		frame.dataOffset				= 0;
		frame.dataSize					= data->dataSize();
	}

	void PricesAdjFactor::buildDailyFactor(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		auto pricesData					= config().getRequired("pricesData");
		auto adjPricesData				= config().getRequired("adjPricesData");
		const FloatMatrixData* prices	= dr.floatData(dci.universe, pricesData);
		const FloatMatrixData* adjPrices= dr.floatData(dci.universe, adjPricesData);

		IntDay rows						= (IntDay)prices->rows();
		size_t cols						= prices->cols();
		FloatMatrixData* data			= new FloatMatrixData(dci.universe, dci.def, 1, cols);

		//if (!m_zero || !m_one) {
		//	m_zero						= std::make_shared<FloatMatrixData>(dci.universe, dci.def, 1, cols);
		//	m_one						= std::make_shared<FloatMatrixData>(dci.universe, dci.def, 1, cols);

		//	m_zero->setMemory(0.0f);
		//	m_one->setMemory(1.0f);
		//}

		if (dci.di < rows - 1) {
			(*data)[0] = (
				(prices->middleRows(dci.di + 1, 1) - adjPrices->middleRows(dci.di + 1, 1)) == 0.0 && 
				(prices->middleRows(dci.di + 0, 1) - adjPrices->middleRows(dci.di + 0, 1)) != 0.0)
				.select(
					adjPrices->middleRows(dci.di, 1) / prices->middleRows(dci.di, 1),
					1.0f
				);
		}
		else {
			data->setMemory(1.0f);
		}

		frame.noDataUpdate				= false;
		frame.data						= DataPtr(data);
		frame.dataOffset				= 0;
		frame.dataSize					= data->dataSize();

		CInfo("[%u]: Built: %s", dci.rt.dates[dci.di], std::string(dci.def.name));
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createPricesAdjFactor(const pesa::ConfigSection& config) {
	return new pesa::PricesAdjFactor((const pesa::ConfigSection&)config);
}

