/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Revere.cpp
///
/// Created on: 12 Oct 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "SpGeneric.h"
#include "Poco/String.h"

namespace pesa {
	class Revere : public SpGeneric {
	private:
		struct FigiInfo {
			StringMap			values; 
		};
		typedef std::unordered_map<std::string, FigiInfo> FigiInfoMap;
		FigiInfoMap				m_infoCache;		/// Info cache made for per date FIGI with all the values

		bool					m_dataConstructed = false;

	public:
								Revere(const ConfigSection& config);
		virtual 				~Revere();

		////////////////////////////////////////////////////////////////////////////
		/// SpGeneric overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			buildDaily(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void 			postDataBuild(IDataRegistry& dr, const pesa::DataCacheInfo& dci);
		//virtual std::string		buildSymbolsStr(StringVec& uuids, const DataCacheInfo& dci, IntDay di, size_t startUuidIndex, size_t endUuidIndex);
	};

	Revere::Revere(const ConfigSection& config) 
		: SpGeneric(config) {
		setLogChannel("Revere");
		m_minfo.specifiedFieldsOnly = false;
	}

	Revere::~Revere() {
		CTrace("Deleting Revere");
	}

	void Revere::postDataBuild(IDataRegistry& dr, const pesa::DataCacheInfo& dci) {
		m_infoCache.clear();
	}

	//std::string Revere::buildSymbolsStr(StringVec& uuids, const DataCacheInfo& dci, IntDay di, size_t startUuidIndex, size_t endUuidIndex) {
	//	std::string symbolsStr = "dict(Symbology='CFIGI',IDs=(";

	//	std::string uuid = "BBG000B9XRY4";
	//	symbolsStr += PESA_QSTR(uuid) + ",";

	//	symbolsStr += "))";

	//	return symbolsStr;
	//}

	void Revere::buildDaily(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		if (!m_dataConstructed) {
			if (!m_dataCache.size()) {
				SpGeneric::preCacheDay(dr, dci, 0, "");
			}

			ASSERT(m_dataCache.size() == 1, "Invalid data cache size should be 1 but is: " << m_dataCache.size());
			CacheInfoPtr ci = m_dataCache.begin()->second;
			ci->errCode = ci->restData->wait();

			if (ci->errCode != 0) {
				frame.noDataUpdate = true;
				return;
			}

			SpRest::ResultVec results = ci->restData->results();

			if (!results.size()) {
				frame.noDataUpdate = true;
				return;
			}

			//ASSERT(results.size() == 1, "Expecting a single result but found: " << results.size());
			Poco::Timespan oneDay = Poco::Timespan(1, 0, 0, 0, 0);
			StringMap invalidFigis;

			for (size_t batchIndex = 0; batchIndex < results.size(); batchIndex++) {
				SpRest::Result& result = results[batchIndex];
				StringData* figis = dynamic_cast<StringData*>(result.data["SYMBOL"].get());
				StringData* companyStartDate = dynamic_cast<StringData*>(result.data["CompanyStartDate"].get());
				StringData* companyEndDate = dynamic_cast<StringData*>(result.data["CompanyEndDate"].get());

				size_t numFigis = figis->cols();

				for (auto figiIndex = 0; figiIndex < numFigis; figiIndex++) {
					auto figi = figis->getValue(0, figiIndex);

					if (invalidFigis.find(figi) != invalidFigis.end())
						continue;

					auto sec = dci.universe->mapUuid(figi);

					/// This particular security isn't eve part of our universe ... we just ignore it 
					if (!sec || !sec->isValid()) {
						invalidFigis[figi] = figi;
						continue;
					}

					CDebug("Handling: %s [%z]", figi, figiIndex);

					size_t numDates = companyStartDate->cols();
					int srcIndex = -1;

					for (size_t idt = 0; idt < numDates; idt++) {
						std::string startDateStr = companyStartDate->getValue(0, idt);
						std::string endDateStr = companyEndDate->getValue(0, idt);
						IntDate startDate = Util::strDateTimeToIntDate(startDateStr);
						IntDate endDate = Util::strDateTimeToIntDate(endDateStr);

						if (endDate < dci.rt.getStartDate())
							continue;

						Poco::DateTime dtStart = Util::intToDate(startDate);
						Poco::DateTime dtEnd = Util::intToDate(endDate);
						Poco::DateTime simEndDate = Util::intToDate(dci.rt.getEndDate());

						/// Now we get all the data for this entity and save it 
						Poco::DateTime dtCurr = dtStart;

						while (dtCurr <= dtEnd && dtCurr <= simEndDate) {
							dtCurr = dtCurr + oneDay;
							IntDate currDate = Util::dateToInt(dtCurr);
							if (currDate < dci.rt.getStartDate())
								continue;

							std::string fid = figi + "_" + Util::cast(currDate);

							for (auto iter = m_mappings.begin(); iter != m_mappings.end(); iter++) {
								auto lhs = iter->first;
								auto rhs = iter->second;
								StringData* srcData = dynamic_cast<StringData*>(result.data[lhs].get());
								ASSERT(srcData, "Unable to read data from returned payload: " << lhs);
								std::string dataValue = srcData->getValue(0, figiIndex);
								m_infoCache[fid].values[rhs] = dataValue;
							}
						}
					}
				}
			}

			/// Clear the data
			m_dataConstructed = true;
			m_dataCache.clear();
			results.clear();
		}

		StringData* newData = new StringData(dci.universe, dci.def);
		size_t size = dci.universe->size(dci.di);
		std::string dateStr = std::string("_") + Util::cast(dci.rt.dates[dci.di]);
		std::string dataName = dci.def.name;
		size_t total = 0, numInvalid = 0;

		newData->ensureSize(1, size);

		for (size_t ii = 0; ii < size; ii++) {
			auto sec = dci.universe->security(dci.di, (Security::Index)ii);
			if (!sec.index == Security::s_invalidIndex)
				continue;
			auto fid = sec.uuidStr() + dateStr;
			auto iter = m_infoCache.find(fid);

			if (iter == m_infoCache.end()) {
				numInvalid++;
				continue;
			}

			std::string value = iter->second.values[dataName];
			newData->setValue(0, ii, value);
		}

		/// We don't need this data again ...
		frame.data = DataPtr(newData);
		frame.dataOffset = 0;
		frame.dataSize = frame.data->dataSize();

		//CInfo("Updated data: [%-8u]-[Found: %5z / %5z - Invalid: %5z] - %s", today, hdata.figis.size(), ci->uuids.size(), numInvalid, dci.def.toString(dci.ds));
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createRevere(const pesa::ConfigSection& config) {
	return new pesa::Revere((const pesa::ConfigSection&)config);
}

