/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Snapshot.cpp
///
/// Created on: 22 Jan 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "SpGeneric.h"
#include "Poco/String.h"

namespace pesa {
	class Snapshot : public SpGeneric {
	private:
		typedef std::map<int, std::string> TimestampStrMap;
		TimestampStrMap			m_tsMap;						/// The time stamp map
		IntVec					m_times;						/// The times
		std::string				m_d0Update;						/// What data to update NOW!
		std::map<std::string, std::string> m_d0Mappings;		/// The mappings

	public:
								Snapshot(const ConfigSection& config);
		virtual 				~Snapshot();

		////////////////////////////////////////////////////////////////////////////
		/// Snapshot overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			buildDaily(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void 			buildToday(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
	};

	Snapshot::Snapshot(const ConfigSection& config)
		: SpGeneric(config) {
		setLogChannel("Snapshot");

		m_d0Update = config.getString("d0Update", "");

		if (!m_d0Update.empty()) {
			FrameworkUtil::parseMappings(m_d0Update, &m_d0Mappings);
		}

		auto stimes = config.getRequired("times");
		auto times = Util::split(stimes, ",");
		for (const auto& time : times) {
			auto mapping = Util::split(time, "=");
			ASSERT(mapping.size() == 2, "Invalid time mapping: " << time);
			int offsetInMinutes = Util::cast<int>(mapping[0]);
			m_times.push_back(offsetInMinutes);
			std::string suffix = mapping[1];
			m_tsMap[offsetInMinutes] = suffix;
		}
	}

	Snapshot::~Snapshot() {
		CTrace("Deleting Snapshot");
	}

	void Snapshot::initDatasets(IDataRegistry& dr, Datasets& dss) {
		Dataset ds(m_datasetName);
		StringVec dataRequired;

		m_rmappings.clear();

		for (std::string dataset : m_datasets) {
			DataFrequency::Type frequency;
			DataType::Type type;
			std::string dataName;
			bool addToRequiredList = true;

			DataDefinition::parseDefString(dataset, dataName, type, frequency);

			std::string orgDataName = dataName;
			auto mappedIter = m_mappings.find(dataName);
			if (mappedIter != m_mappings.end())
				dataName = mappedIter->second;

			for (int& time : m_times) {
				auto tsiter = m_tsMap.find(time);
				ASSERT(tsiter != m_tsMap.end(), "Unable to find: " << time);
				int offsetInMinutes = tsiter->first;
				std::string suffix = tsiter->second;
				std::string fullName = dataName + suffix;
				m_rmappings[fullName] = orgDataName;
				DataValue dv(dataName + suffix, type, DataType::size(type), THIS_DATA_FUNC(SpGeneric::buildDaily), frequency, 0, reinterpret_cast<void*>(&time));
				ds.add(dv);
			}

			dataRequired.push_back(orgDataName);
			m_requiredDataTypes.push_back(type);
			m_requiredDataFrequencies.push_back(frequency);
		}

		dss.add(ds);

		dataRequired.push_back(s_symbolColName);
		m_requiredDataTypes.push_back(DataType::kString); /// For the CFIGI 

		dataRequired.push_back("TIMESTAMP");
		m_requiredDataTypes.push_back(DataType::kTimestamp); /// For the TIMESTAMP

		//for (size_t i = 0; i < m_dataIndexes.size(); i++) {
		//	const auto& dataIndex = m_dataIndexes[i];
		//	const auto& dataValue = m_dataValues[i];

		//	DataFrequency::Type frequency;
		//	DataType::Type type;
		//	std::string dataName;

		//	DataDefinition::parseDefString(dataIndex, dataName, type, frequency);
		//	dataRequired.push_back(dataName);
		//	m_requiredDataTypes.push_back(type);
		//	m_resolvedDataIndexes[i] = dataName;

		//	ASSERT(type == DataType::kString, "Only string." << dataName << " is supported for the time being!");

		//	DataDefinition::parseDefString(dataValue, dataName, type, frequency);
		//	dataRequired.push_back(dataName);
		//	m_requiredDataTypes.push_back(type);
		//	m_dataValues[i] = dataName;
		//}

		m_dataRequired = dataRequired;
	} 

	void Snapshot::buildToday(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		frame.noDataUpdate = true;
		return;
		// if (m_d0Update.empty())
		// 	return;

		/// Get this data
		// DataPtr data = dr.getDataPtr(dci.universe, m_d0Update, true);
	}

	void Snapshot::buildDaily(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		try { 
			/// If its the current day
			Poco::DateTime dtToday;
			IntDate date = dci.rt.dates[dci.di];
			IntDate todayDate = Util::dateToInt(dtToday);

			if (date == todayDate) {
				CDebug("Building today's data using snapshot: %u", date);
				buildToday(dr, dci, frame);
				return;
			}

			std::string rmappedName;
			auto results = waitForResults(dr, dci, frame, rmappedName, "");

			if (!results.size()) {
				frame.noDataUpdate = true;
				return;
			}

			std::string cacheKey = SpGeneric::makeKey(date, "");

			CacheInfoPtr ci = m_dataCache[cacheKey];
			Data* newData = nullptr;
			size_t numRemapped = 0;
			int offsetInMinutes = *(reinterpret_cast<int*>(dci.dv.customInfo));

			ASSERT(offsetInMinutes > 0, "Invalid offset: " << offsetInMinutes);

			auto tradeStart = dci.rt.utcTradingStartTime_DT[dci.di];

			/// Zero out any rounding errors that might have crept in the seconds and milliseconds section
			Poco::DateTime dtStart(tradeStart.year(), tradeStart.month(), tradeStart.day(), tradeStart.hour(), tradeStart.minute());
			Poco::DateTime dtSnap = (dtStart + Poco::Timespan(0, 0, offsetInMinutes, 0, 0));
			IUniverse* secMaster = dr.getSecurityMaster();

			//CDebug("%s - %s", rmappedName, Util::dateTimeToStr(dtSnap));

			for (size_t batchIndex = 0; batchIndex < results.size(); batchIndex++) {
				SpRest::Result& result = results[batchIndex];
				DataPtr figiData, data;

				if (!extractData(dci, frame, date, result, figiData, data, rmappedName))
					return;

				size_t figiCount = figiData->cols();
				DataPtr timestampData = result.data["TIMESTAMP"];
				ASSERT(timestampData, "Invalid timestamp data in the result!");
				const std::string* figis = (dynamic_cast<StringData*>(figiData.get()))->sptr();
				const Int64MatrixData* timestamps = dynamic_cast<Int64MatrixData*>(timestampData.get());

				/// Make sure we have enough size over here ...
				if (!newData) {
					newData = data->clone(data->universe(), true);
					newData->ensureSize(1, secMaster->size(dci.di), false);
					ASSERT(newData->itemSize() == 0 || newData->dataSize() % newData->itemSize() == 0, "Invalid data size: " << newData->dataSize() << " - not aligned with item size: " << newData->itemSize());
				}

				/// The order of the returned data is not guaranteed to be in the same order in which we 
				/// sent, hence we will need to remap stuff over here
				for (size_t ii = 0; ii < figiCount; ii++) {
					const auto& figi = figis[ii];
					int64_t timestamp = (*timestamps)(0, ii);
					Poco::DateTime dt = timestamps->getDTValue(0, ii);
					Poco::DateTime dtRounded(dt.year(), dt.month(), dt.day(), dt.hour(), dt.minute());
					Poco::Timespan span = dt - dtSnap;
					int totalMinutes = std::abs(span.totalMinutes());

					/// If there is less than a 2 minute difference then we use the data ...
					if (totalMinutes < 1) {
						/// Now we need to map this uuid
						const Security* sec = secMaster->mapUuid(figi);

						numRemapped++;
						if (sec && sec->isValid()) 
							newData->copyElement(data.get(), 0, ii, 0, (size_t)sec->index);
					}
				}
			}

			if (!numRemapped) {
				CWarning("No mapping on date: %u [di = %d, SnapDT = %s, Data = %s]", date, dci.rt.di, Util::dateTimeToStr(dtSnap), dci.def.toString(dci.ds));
			}

			if (!newData) {
				frame.noDataUpdate = true;
				return;
			}

			frame.data = DataPtr(newData);
			frame.dataOffset = 0;
			frame.dataSize = frame.data->dataSize();

			CInfo("Updated data: [%-8u]-[Found: %5z / %5z] - %s", date, numRemapped, secMaster->size(dci.di), dci.def.toString(dci.ds));
		}
		catch (const std::exception& e) {
			Util::reportException(e);
		}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createSnapshot(const pesa::ConfigSection& config) {
	return new pesa::Snapshot((const pesa::ConfigSection&)config);
}

