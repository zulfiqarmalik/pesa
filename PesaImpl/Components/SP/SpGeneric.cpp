/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// SpGeneric.cpp
///
/// Created on: 20 Aug 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "SpGeneric.h"
#include "Poco/String.h"

#include "Framework/Data/VarMatrixData.h"
#include "PesaImpl/Components/Quandl/Quandl_JsonParser.h"
#include "PesaImpl/Components/MorningStar/MS_JsonParser.h"

namespace pesa {
	const std::string SpGeneric::s_symbolColName = "ticker";

	SpGeneric::SpGeneric(const ConfigSection& config) 
		: IDataLoader(config, std::string("SpGeneric-") + config.getRequired("datasetName")) {
		m_datasetName			= config.getRequired("datasetName");
		std::string datasets	= config.getString("datasets", "");
		m_alwaysOverwrite		= config.getBool("alwaysOverwrite", m_alwaysOverwrite);
		m_useValidValue			= config.getBool("useValidValue", m_useValidValue);
		m_uuidMapping			= config.getBool("uuidMapping", m_uuidMapping);
		m_maxPrecache			= config.get<IntDay>("maxPrecache", s_maxPrecache);
		m_maxRequestUuids		= config.get<IntDay>("maxRequestUuids", s_maxRequestUuids);
		m_requestDelay			= config.getInt("requestDelay", m_requestDelay);
		m_symbolColName			= config.getString("symbolColName", s_symbolColName);
		m_isSymbolFIGI			= config.getBool("isSymbolFIGI", m_isSymbolFIGI);
		m_noEmptyArguments		= config.getBool("noEmptyArguments", m_noEmptyArguments);
		m_uuidData				= config.getString("uuidData", m_uuidData);
		m_uuidType				= config.getString("uuidType", m_uuidType);
		m_exactTimestamp		= config.getBool("exactTimestamp", m_exactTimestamp);
		m_dateFormat			= config.getString("dateFormat", m_dateFormat);
		m_fetchesAllHistory		= config.getBool("fetchesAllHistory", m_fetchesAllHistory);
		m_backfill				= config.getBool("backfill", m_backfill);
		m_backfillBase			= config.getString("backfillBase", "");
		m_timestampColName		= config.getString("timestampColName", m_timestampColName);
		m_exactSymbolList		= config.getBool("exactSymbolList", m_exactSymbolList);
		m_noMappingOnCopy		= config.getBool("noMappingOnCopy", m_noMappingOnCopy);

		m_deliveryDelay			= config.get<IntDay>("deliveryDelay", m_deliveryDelay);

		if (!datasets.empty())
			m_datasets			= Util::split(datasets, ",");

		m_passSymbols			= config.getBool("passSymbols", m_passSymbols);
		m_useSymbolList			= config.getBool("useSymbolList", m_useSymbolList);

		std::string mappings	= config.get("mappings");

		if (!mappings.empty()) {
			StringVec mappingsVec = Util::split(mappings, ",");
			for (auto& mapping : mappingsVec) {
				StringVec parts = Util::split(mapping, "=");
				ASSERT(parts.size() == 2, "Invalid mapping: " << mapping);
				m_mappings[parts[0]] = parts[1];
				m_rmappings[parts[1]] = parts[0];
			}
		}

		auto& children = config.children();
		if (children.size()) {
			for (auto* child : children) {
				if (child->name() == "Args") {
					const auto& map = child->map();
					for (auto iter = map.begin(); iter != map.end(); iter++) {
						const auto& name = iter->first;
						const auto& value = iter->second;
						m_additionalArgs[name] = value;
					}
				}
			}
		}

		std::string dataIndex = config.get("dataIndex");
		if (!dataIndex.empty()) {
			m_dataIndexes = Util::split(dataIndex, ",");
			m_resolvedDataIndexes.resize(m_dataIndexes.size());
		}

		std::string dataValue = config.get("dataValue");
		if (!dataValue.empty())
			m_dataValues = Util::split(dataValue, ",");

		ASSERT(m_dataIndexes.size() == m_dataValues.size(), "dataIndex and dataValue sizes do not match up. dataIndex: " << m_dataIndexes.size() << " - dataValue: " << m_dataValues.size());
	}

	SpGeneric::~SpGeneric() {
		CTrace("Deleting SpGeneric");
	}

	void SpGeneric::buildUuidLookupTable(IDataRegistry& dr, const std::string& universeId) {
		if (m_uuidData.empty() || !m_uuidLookup.empty())
			return;

		IUniverse* universe = !universeId.empty() ? dr.getUniverse(universeId, true) : nullptr;
		const StringData* uuidData = dr.stringData(universe, m_uuidData);
		size_t numCols = uuidData->cols();

		for (size_t ii = 0; ii < numCols; ii++) {
			const std::string& uuid = uuidData->getValue(0, ii);
			if (!uuid.empty()) 
				m_uuidLookup[uuid] = (Security::Index)ii;
		} 
	}

	void SpGeneric::buildUuidLookupTableForType(IDataRegistry& dr, const std::string& universeId) {
		if (m_uuidType.empty() || !m_uuidLookup.empty())
			return;

		ASSERT(!universeId.empty(), "Invalid universe with uuidType argument: " << m_uuidType);

		SecId::Type type = SecId::parse(m_uuidType);
		StringVec ids;
		IUniverse* universe = dr.getUniverse(universeId, true);

		universe->idsOfType(0, ids, type); 

		size_t numSecurities = universe->size(0);

		ASSERT(ids.size() == numSecurities, "Invalid number of ids returned. Expecting: " << numSecurities << " - Found: " << ids.size());

		for (size_t ii = 0; ii < ids.size(); ii++) {
			const std::string& uuid = ids[ii];
			if (!uuid.empty()) 
				m_uuidLookup[uuid] = (Security::Index)ii;
		}
	}

	std::string SpGeneric::getServerUrl(IDataRegistry& dr, const DataCacheInfo& dci, IntDay di) const {
		return config().getServerUrl("", dci.rt.getDate(di));
	}

	SpRest::IParser* SpGeneric::createJsonParser() { 
		auto jsonParser = config().getString("jsonParser", "");

		if (jsonParser.empty())
			return nullptr; 

		if (jsonParser == "Quandl_JsonParser")
			return new Quandl_JsonParser();
		else if (jsonParser == "MS_JsonParser" || jsonParser == "MorningStar_JsonParser")
			return new MS_JsonParser(this);

		return nullptr;
	}

	void SpGeneric::initDatasets(IDataRegistry& dr, Datasets& dss) {
		Dataset ds(m_datasetName);
		StringVec dataRequired;

		if (m_uuidLookup.empty()) {
			std::string universeId = config().getString("universeId", "");

			if (!m_uuidData.empty()) 
				buildUuidLookupTable(dr, universeId);
			else if (!m_uuidType.empty()) 
				buildUuidLookupTableForType(dr, universeId);
		}

		for (std::string dataset : m_datasets) {
			DataFrequency::Type frequency;
			DataType::Type type;
			std::string dataName;
			bool addToRequiredList = true;

			DataDefinition::parseDefString(dataset, dataName, type, frequency);

			std::string orgDataName = dataName;
			auto mappedIter = m_mappings.find(dataName);
			if (mappedIter != m_mappings.end()) 
				dataName = mappedIter->second;

			m_datasetNames.push_back(dataName);

			if (frequency != DataFrequency::kUniversal) {
				if (!m_dataIndexes.size()) {
					DataValue dv(dataName, type, DataType::size(type), THIS_DATA_FUNC(SpGeneric::buildDaily), frequency, m_alwaysOverwrite ? DataFlags::kAlwaysOverwrite : 0);
					ds.add(dv);
				}
				else {
					if (m_uuidMapping) {
						DataValue dv(dataName, type, DataType::size(type), THIS_DATA_FUNC(SpGeneric::buildDailyMultiIndexed), frequency, m_alwaysOverwrite ? DataFlags::kAlwaysOverwrite : 0);
						ds.add(dv);
						addToRequiredList = false; /// These values are indexed differently!
					}
					else {
						DataValue dv(dataName, type, DataType::size(type), THIS_DATA_FUNC(SpGeneric::buildDailyMultiIndexed_NonUUID), frequency, m_alwaysOverwrite ? DataFlags::kAlwaysOverwrite : 0);
						ds.add(dv);
						addToRequiredList = false; /// These values are indexed differently!
					}
				}
			}
			else {
				DataValue dv(dataName, type, DataType::size(type), THIS_DATA_FUNC(SpGeneric::buildUniversal), frequency, DataFlags::kUpdatesInOneGo | DataFlags::kAlwaysOverwrite);
				ds.add(dv);
			}

			if (addToRequiredList) {
				dataRequired.push_back(orgDataName);
				m_requiredDataTypes.push_back(type);
				m_requiredDataFrequencies.push_back(frequency);
			}
		}

		dss.add(ds);

		dataRequired.push_back(m_symbolColName);
		m_requiredDataTypes.push_back(DataType::kString); /// For the CFIGI

		if (!m_timestampColName.empty()) {
			dataRequired.push_back(m_timestampColName);
			m_requiredDataTypes.push_back(DataType::kDateTime); /// For the CFIGI
		}

		/// Make sure that we get these ...
		for (size_t i = 0; i < m_dataIndexes.size(); i++) {
			const auto& dataIndex = m_dataIndexes[i];
			const auto& dataValue = m_dataValues[i];

			DataFrequency::Type frequency;
			DataType::Type type;
			std::string dataName;

			DataDefinition::parseDefString(dataIndex, dataName, type, frequency);
			dataRequired.push_back(dataName);
			m_requiredDataTypes.push_back(type);
			m_resolvedDataIndexes[i] = dataName;

			//ASSERT(type == DataType::kString, "Only string." << dataName << " is supported for the time being!");

			DataDefinition::parseDefString(dataValue, dataName, type, frequency);
			dataRequired.push_back(dataName);
			m_requiredDataTypes.push_back(type);
			m_dataValues[i] = dataName;
		}

		m_dataRequired = dataRequired;
	}

	void SpGeneric::preDataBuild(IDataRegistry& dr, const DataCacheInfo& dci) {
	}

	void SpGeneric::postDataBuild(IDataRegistry& dr, const pesa::DataCacheInfo& dci) {
		m_dataCache.clear();
	}

	void SpGeneric::preBuild(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		preCache(dr, dci, "");
	}

	void SpGeneric::buildUniversal(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		buildDaily(dr, dci, frame);
	}

	std::string& SpGeneric::resolveArg(std::string& value) const {
		value = parentConfig()->defs().resolve(value, (unsigned int)0);
		return value;
	}

	void SpGeneric::resolveArgs(StrMap& args, const DataCacheInfo& dci, IntDay di) {
		Poco::DateTime date						= Util::intToDate(dci.rt.dates[di]);
		std::string diDate						= Util::intDateToStr(m_dateFormat, dci.rt.dates[di]);
		std::string diStart						= Util::intDateToStr(m_dateFormat, dci.rt.dates[dci.rt.diStart]);
		std::string diEnd						= Util::intDateToStr(m_dateFormat, dci.rt.dates[dci.rt.diEnd]);
		std::string diFirst						= Util::intDateToStr(m_dateFormat, dci.rt.dates[0]);
		std::string dciStart					= Util::intDateToStr(m_dateFormat, dci.rt.dates[dci.diStart]);
		std::string dciEnd						= Util::intDateToStr(m_dateFormat, dci.rt.dates[dci.diEnd]);

		std::string year						= Util::cast(date.year());
		std::string month						= Util::castPadded(date.month(), 2);
		std::string day							= Util::castPadded(date.day(), 2);
		std::string quarter						= Util::cast(date.month() / 3);
		std::string q_quarter					= Util::cast(date.month() / 3 + 1);
		std::string di0_SlowDate				= diDate;
		std::string diEndIncludingHolidayDate	= diDate;

		std::string diDate_FrequencyOffset;
		std::string lastFiscalQuarterEndDate;

		int holidaysAhead = dci.rt.holidaysAhead[di];
		if (holidaysAhead > 1 && di < (IntDay)dci.rt.dates.size() - 1) {
			Poco::DateTime diEndDate = Util::intToDate(dci.rt.dates[di + 1]) - Util::daysSpan(1);
			diEndIncludingHolidayDate = Util::cast(Util::dateToInt(diEndDate));
		}

		if (dci.def.frequency >= DataFrequency::kMonthly) {
			int frequency = (int)((dci.def.frequency * 2) / 3);
			int diFrequencyDateEnd = std::min(di + frequency, dci.rt.diEnd);

			int holidaysAhead = dci.rt.holidaysAhead[diFrequencyDateEnd];
			if (holidaysAhead > 1 && diFrequencyDateEnd < (IntDay)dci.rt.dates.size() - 1) {
				Poco::DateTime diEndDate = Util::intToDate(dci.rt.dates[diFrequencyDateEnd + 1]) - Util::daysSpan(1);
				diDate_FrequencyOffset = Util::cast(Util::dateToInt(diEndDate));
			}
			else {
				IntDate dateEnd = dci.rt.getDate((size_t)diFrequencyDateEnd);
				diDate_FrequencyOffset = Util::intDateToStr(m_dateFormat, dateEnd);
			}
		}
		else {
			/// Otherwise we just use the daily date logic!
			diDate_FrequencyOffset = diEndIncludingHolidayDate;
		}

		/// On the very first day, we give a bit of a delta
		if (di == 0) {
			std::string slowDateDef = config().getString("di0_SlowDate", "");

			/// If no definition of slow date is given then just use the default logic of a 180 days behind the start
			if (slowDateDef.empty()) {
				Poco::DateTime deltaDate = date - Poco::Timespan(180, 0, 0, 0, 0);
				di0_SlowDate = Util::intDateToStr(m_dateFormat, Util::dateToInt(deltaDate));
			}
			else {
				/// Otherwise we just use the explicid definition of the slow date
				di0_SlowDate = slowDateDef;
			}
		}

		for (auto iter = args.begin(); iter != args.end(); iter++) {
			auto& value = iter->second;

			//if (value[0] != '@') {
			value = Poco::replace(value, "{$diDate}", diDate.c_str());
			value = Poco::replace(value, "{$di0_SlowDate}", di0_SlowDate.c_str());
			value = Poco::replace(value, "{$diEndIncludingHolidayDate}", diEndIncludingHolidayDate.c_str());
			value = Poco::replace(value, "{$diFirst}", diFirst.c_str());
			value = Poco::replace(value, "{$diStart}", diStart.c_str());
			value = Poco::replace(value, "{$diEnd}", diEnd.c_str());
			value = Poco::replace(value, "{$YYYY}", year.c_str());
			value = Poco::replace(value, "{$MM}", month.c_str());
			value = Poco::replace(value, "{$DD}", day.c_str());
			value = Poco::replace(value, "{$q}", quarter.c_str());
			value = Poco::replace(value, "{$Q}", q_quarter.c_str());
			value = Poco::replace(value, "{$market}", parentConfig()->defs().market().c_str());
			value = Poco::replace(value, "{$diDate_FrequencyOffset}", diDate_FrequencyOffset.c_str());
			value = Poco::replace(value, "{$dciStart}", dciStart.c_str());
			value = Poco::replace(value, "{$dciEnd}", dciEnd.c_str());

			if (value.find("{$lastFiscalQuarterEndDate}") != std::string::npos && lastFiscalQuarterEndDate.empty()) {
				lastFiscalQuarterEndDate = Util::cast(getLastFiscalQuarterDate(dci.rt.dates[di]));
			}

			value = Poco::replace(value, "{$lastFiscalQuarterEndDate}", lastFiscalQuarterEndDate.c_str());

			//}
			//else {
			//	/// This is actually a data variable ...
			//	std::string dataId = value;
			//	dataId.erase(0, 1);
			//	DataPtr data = dci.rt.dataRegistry->getDataPtr(nullptr, dataId, true);
			//	ASSERT(data, "Unable to find data: " << dataId);

			//}

			/// In the end we are going to use the generic replace ...
			value = resolveArg(value);
		}
	}

	IntDate SpGeneric::getLastFiscalQuarterDate(IntDate date) {
		IntDate year, month, day;

		Util::splitIntDate(date, year, month, day);
		std::string syear = Util::cast(year);

		if (month < 4)
			return Util::strDateToIntDate(Util::cast(year - 1) + "-12-31");
		else if (month >= 4 && month < 7)
			return Util::strDateToIntDate(syear + "-03-31");
		else if (month >= 7 && month < 10)
			return Util::strDateToIntDate(syear + "-06-30");

		/// The final else case!
		return Util::strDateToIntDate(syear + "-09-30");
	}

	std::string SpGeneric::makeKeyDirect(IntDate date, const std::string& keyExtra) {
		std::string key = Util::cast(date);
		if (keyExtra.empty())
			return key;
		return key + "-" + keyExtra;
	}

	std::string SpGeneric::makeKey(IntDate date, const std::string& keyExtra) const {
		if (m_fetchesAllHistory)
			return "ALL_HISTORY";

		return SpGeneric::makeKeyDirect(date, keyExtra);
	}

	std::string SpGeneric::buildSymbolsStr(StringVec& uuids, const DataCacheInfo& dci, IntDay di, size_t startUuidIndex, size_t endUuidIndex) {
		if (m_passSymbols) {
			IntDate date = dci.rt.dates[di];

			//if (m_useSymbolList || uuids.size() < (size_t)s_minUuids) {
			if (m_useSymbolList) {
				std::string symbolsStr = "";

				/// Get the first empty string ...
                for (size_t uuidIndex = startUuidIndex; uuidIndex < endUuidIndex && symbolsStr.empty(); uuidIndex++) {
					auto& uuid = uuids[uuidIndex];
					if (!uuid.empty())
						symbolsStr = uuid;
				}

				/// Then fill up the next with a comma ...
				for (size_t uuidIndex = startUuidIndex; uuidIndex < endUuidIndex; uuidIndex++) {
					auto& uuid = uuids[uuidIndex];
					if (!uuid.empty())
						symbolsStr += std::string(",") + uuid;
				}

				return symbolsStr;
			}
			else {
				return config().getRequired("defaultSymbolFormat");
			}
		}

		return "";
	}

	void SpGeneric::getAllSymbols(IDataRegistry& dr, const DataCacheInfo& dci, IntDay di, std::vector<std::string>& ids) const {
		/// If there is no symbol column name specified then we just get the UUIDs from the universe
		if (m_uuidData.empty() && m_uuidType.empty()) {
			dci.secSrcUniverse->uuids(di, ids);
			return;
		}
		else if (!m_uuidData.empty()) {
			/// Otherwise, we get the data ...
			StringData* uuidData = dr.stringData(dci.universe, m_uuidData);
			di = std::min((IntDay)uuidData->rows() - 1, di);

			size_t numCols = uuidData->cols();

			ids.resize(numCols);

			for (size_t ii = 0; ii < numCols; ii++) {
				std::string symbol = uuidData->getValue(di, ii);
				ASSERT(!symbol.empty(), "Symbol empty at: " << ii << ". Dataset: " << uuidData);
				ids[ii] = symbol;
			}
		}
		else if (!m_uuidType.empty()) {
			SecId::Type idType = SecId::parse(m_uuidType);
			dci.universe->idsOfType(0, ids, idType);
		}
	}

	SpGeneric::CacheInfoPtr SpGeneric::preCacheDay(IDataRegistry& dr, const DataCacheInfo& dci, IntDay di, const std::string& keyExtra) {
		CacheInfoPtr ci			= std::make_shared<CacheInfo>();
		IntDate date			= dci.rt.dates[di];
		std::string cacheKey	= makeKey(date, keyExtra);
		auto iter				= m_dataCache.find(cacheKey);

		/// We already have this in the cache ...
		if (iter != m_dataCache.end())
			return iter->second;

		SpRest::StringMap args;
		IntDay diToUseForDates = di;
		
		if (m_deliveryDelay)
			diToUseForDates = std::max(di - m_deliveryDelay, 0);

		if (m_additionalArgs.size()) {
			args = m_additionalArgs;
			resolveArgs(args, dci, diToUseForDates);

			if (m_noEmptyArguments) {
				SpRest::StringMap tmpArgs = args;
				args.clear();

				for (auto iter : tmpArgs) {
					std::string key = iter.first;
					std::string value = iter.second;
					std::string tmpValue = Poco::remove(value, '\'');

					if (!tmpValue.empty())
						args[key] = value;
				}
			}
		}

		ci->restData = SpRestBatchPtr(new SpRestBatch(config(), &m_dataRequired, &m_requiredDataTypes, m_minfo));

		/// This might not be valid if we're building dates for instance, which happens before any of the universes are loaded!
		if (dci.secSrcUniverse && (m_passSymbols || m_useSymbolList)) {
			getAllSymbols(dr, dci, di, ci->uuids);
		}

		SpRest::MoreInfo minfo	= m_minfo;
		minfo.jsonParser		= createJsonParser();
		minfo.serverUrl			= getServerUrl(dr, dci, di);
		minfo.useHttpGet		= config().getBool("useHttpGet", false);

		/// If no uuids were retrieved from the getAllSymbols function and the config
		/// is setup to use the exact symbol list then just return
		if (!ci->uuids.size() && m_exactSymbolList)
			return nullptr;

		ci->numSymbols			= ci->uuids.size() ? ci->uuids.size() : dci.universe->size(dci.di);

		if (m_passSymbols && m_useSymbolList) {
			size_t startUuidIndex = 0, endUuidIndex = 0; 

			if (!m_maxRequestUuids)
				m_maxRequestUuids = (int)ci->numSymbols;

			while (endUuidIndex < ci->numSymbols && startUuidIndex < ci->numSymbols) {
				startUuidIndex = endUuidIndex;
				endUuidIndex = std::min(startUuidIndex + (size_t)m_maxRequestUuids, ci->numSymbols);

				if (m_requestDelay) {
					Poco::Thread::sleep((long)m_requestDelay);
				}

				if (m_fetchesAllHistory) {
				    CDebug("[%u, %u] Querying symbol range: [%z, %z) / %z", dci.rt.dates[dci.diStart], dci.rt.dates[dci.diEnd], startUuidIndex, endUuidIndex, ci->uuids.size()); 
				}

				if (startUuidIndex != endUuidIndex) {
					std::string symbolsStr = buildSymbolsStr(ci->uuids, dci, di, startUuidIndex, endUuidIndex);
					ASSERT(!symbolsStr.empty(), "Invalid symbols returned for symbol range: " << startUuidIndex << ", " << endUuidIndex);
					args[config().getRequired("symbolArg")] = symbolsStr;

					ci->di = di;
					ci->date = date;

					/// Request for data asynchronously ...
					ci->restData->getDataAsync(dci, args, nullptr, &minfo);
				}
			}
		}
		else {
			std::string symbolsStr = buildSymbolsStr(ci->uuids, dci, di, 0, 0);
			if (!symbolsStr.empty())
				args[config().getRequired("symbolArg")] = symbolsStr;

			ci->di = di;
			ci->date = date;

			/// Request for data asynchronously ...
			ci->restData->getDataAsync(dci, args, nullptr, &minfo);
		}

		m_diLast = di;
		m_dataCache[cacheKey] = ci;

		if (dci.def.frequency >= DataFrequency::kMonthly) {
			auto startIter = args.find("StartDate");
			auto endIter = args.find("EndDate");

			if (startIter != args.end() && endIter != args.end()) {
				std::string startDateStr = Poco::replace(Poco::replace(startIter->second, "-", ""), "'", "");
				IntDate startDate = Util::cast<IntDate>(startDateStr);

				std::string endDateStr = Poco::replace(Poco::replace(endIter->second, "-", ""), "'", "");
				IntDate endDate = Util::cast<IntDate>(endDateStr);

				IntDay diStart = dci.rt.getDateIndex(startDate);
				IntDay diEnd = dci.rt.getDateIndex(endDate);

				if (diStart == di && (diEnd - diStart) > 5) {
					for (IntDay ddi = diStart + 1; ddi <= diEnd; ddi++) {
						IntDate date = dci.rt.dates[ddi];
						std::string cacheKey = makeKey(date, keyExtra);
						m_dataCache[cacheKey] = ci; /// Same batch data is going to get used for all the dates in the rameg
					}

					m_diLast = diEnd;
				}

				CDebug("Precaching Complete: %s.%s [%u - %u] [Num Securities: %z, Key-X: %s]", m_datasetName, std::string(dci.def.name), startDate, endDate, ci->uuids.size(), keyExtra);
			}

		}
		else {
			CDebug("Precaching Complete: %s.%s [%u (%d/%d)] [Num Securities: %z, Key-X: %s]", m_datasetName, std::string(dci.def.name), date, di, diToUseForDates, ci->uuids.size(), keyExtra);
		}

		return ci;
	}

	IntDay SpGeneric::getMaxPrecache() const {
		return m_maxPrecache;
	}

	IntDay SpGeneric::getUsageCount() const {
		IntDay usage = 0;
		CacheInfoPtr ciPrev = nullptr;
		for (auto iter = m_dataCache.begin(); iter != m_dataCache.end(); iter++) {
			CacheInfoPtr ci = iter->second;

			if (!ci->consumed) {
				if (ciPrev != ci) /// This is to handle the case where longer results need to be get (monthly and quarterly etc) in one go!
					usage += (IntDay)ci->restData->numCalls();
			}

			ciPrev = ci;
		}

		return usage;
	}

	void SpGeneric::preCache(IDataRegistry& dr, const DataCacheInfo& dci, const std::string& keyExtra) {
		IntDay maxPrecache = getMaxPrecache(); 
		IntDay usageCount = getUsageCount();
		IntDay cacheCount = maxPrecache - usageCount;
		IntDate date = dci.rt.dates[dci.di];

		if (m_diLast < dci.di || m_diLast < 0)
			m_diLast = std::max(dci.di, 0);

		/// cache is already been full
		while (cacheCount > 0 && m_diLast <= dci.diEnd) {
			auto ci = preCacheDay(dr, dci, m_diLast, keyExtra);
			if (ci) 
				cacheCount -= (IntDay)ci->restData->numCalls();
			else
				break;

			m_diLast++;
		}

		//m_diLast = std::min(dci.di + cacheCount, dci.diEnd);

		//for (IntDay di = dci.di; di <= m_diLast; di++) 
		//	preCacheDay(dr, dci, di, keyExtra);
	}

	bool SpGeneric::clearOldCache(IDataRegistry& dr, const DataCacheInfo& dci, const std::string& keyExtra) {
		/// We clear two day old cache OR if this data loader is setup to fetch the entire history!
		if (dci.di < m_maxCacheDaysToKeep || m_fetchesAllHistory)
			return false;

		IntDay di			= dci.di - m_maxCacheDaysToKeep;
		IntDate date		= dci.rt.dates[di];
		std::string cacheKey= makeKey(date, keyExtra);
		auto iter			= m_dataCache.find(cacheKey);

        CTrace("Trying to clear old cache: %s [Total items: %z]", cacheKey, m_dataCache.size());

		if (iter != m_dataCache.end()) {
			CTrace("- CLEARED cache key: %s", cacheKey);
			for (auto iter = m_dataCache.begin(); iter != m_dataCache.end(); iter++) {
				CTrace("    => KEY: %s", iter->first);
			}

			m_dataCache[cacheKey] = nullptr;
			m_dataCache.erase(iter);
		}

		return true;
	}

	SpRest::Result SpGeneric::waitForResult(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame, std::string& rmappedName, std::string keyExtra) {
		SpRest::ResultVec results = waitForResults(dr, dci, frame, rmappedName, keyExtra);
		if (!results.size())
			return SpRest::Result();

		ASSERT(results.size() == 1, "Invalid use of the REST results. Expecting 1 result but found: " << results.size());
		return results[0];
	}

	SpRest::ResultVec SpGeneric::waitForResults(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame, std::string& rmappedName, std::string keyExtra) {
		clearOldCache(dr, dci, keyExtra);
		preCache(dr, dci, keyExtra);

		IntDate date = dci.rt.dates[dci.di];
		std::string cacheKey = makeKey(date, keyExtra);

		/// Sometimes it can happen that if we have a large dataset and they all don't fully update
		/// their end dates might not be the same in the cache header. In this case we can reach 
		/// a situation where the precache logic above might not have cached the correct date
		/// in which case we simply instruct it to do that ...
		if (m_dataCache.find(cacheKey) == m_dataCache.end())
			preCacheDay(dr, dci, dci.di, keyExtra);

		/// If there is no key in the cache then just return
		if (m_dataCache.find(cacheKey) == m_dataCache.end())
			return SpRest::ResultVec();

		/// OK this should technically NEVER happen!
		ASSERT(m_dataCache.find(cacheKey) != m_dataCache.end(), "Date was not in the pre-cache: " << date);

		CacheInfoPtr ci = m_dataCache[cacheKey];

		/// wait for it to complete ...
		ci->errCode = ci->restData->wait();

		if (ci->errCode != 0) {
			frame.noDataUpdate = true;
			return SpRest::ResultVec();
		}

		std::string dataName = dci.def.name;
		rmappedName = dataName;

		auto rmappingIter = m_rmappings.find(dataName);
		if (rmappingIter != m_rmappings.end())
			rmappedName = rmappingIter->second;

		ci->consumed = true;

		auto results = ci->restData->results();
		return results;
	}

	std::string SpGeneric::getRMappedName(const std::string& dataName) const {
		std::string rmappedName = dataName;

		auto rmappingIter = m_rmappings.find(dataName);
		if (rmappingIter != m_rmappings.end())
			rmappedName = rmappingIter->second;

		return rmappedName;
	}

	bool SpGeneric::extractData(const DataCacheInfo& dci, DataFrame& frame, IntDate date, SpRest::Result& result, DataPtr& figiData, DataPtr& data, const std::string& rmappedName) {
		figiData = result.data[m_symbolColName];
		data = result.data[rmappedName];

		//if (dci.def.frequency >= DataFrequency::kMonthly) {
		//	auto timestampsPtr = result.data[m_timestampColName];
		//	if (!timestampsPtr)
		//		return false;

		//	Int64MatrixDataPtr timestamps = std::static_pointer_cast<Int64MatrixData>(timestampsPtr);

		//	/// Must have some data at least
		//	if (!timestamps->cols())
		//		return false;

		//	IntDate dtValue = Util::dateToInt(timestamps->getDTValue(0, 0));
		//	if (dtValue != date)
		//		return false;
		//}

		if (!data) {
			CTrace("Unable to find field in JSON: [%u] - %s", date, rmappedName);
			frame.noDataUpdate = true;
			return false;
		}

		/// No data received for this particular day ...
		if (!figiData->cols()) {
			CWarning("No data received for date: %u", date);
			frame.noDataUpdate = true;
			return false;
		}

		size_t figiCount = figiData->cols();
		ASSERT(figiCount == data->cols(), "Mismatched number of entries between CFIGI and " << rmappedName << ". CFIGI: " << figiData->cols() << " - " << rmappedName << ": " << data->cols());

		return true;
	}

	std::string SpGeneric::getKeyExtra(IDataRegistry& dr, const DataCacheInfo& dci, IntDate date) const {
		return "";
	}

	void SpGeneric::buildDailyMultiIndexed_NonUUID(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		std::string rmappedName;
		IntDate date = dci.rt.dates[dci.di];
		Data* newData = nullptr;
		std::string keyExtra = getKeyExtra(dr, dci, date);
		auto results = waitForResults(dr, dci, frame, rmappedName, keyExtra);

		if (!results.size()) {
			frame.noDataUpdate = true;
			return;
		}

		std::string cacheKey = makeKey(date, keyExtra);
		CacheInfoPtr ci = m_dataCache[cacheKey];
		size_t numInvalid = 0, numRemapped = 0;

		for (size_t batchIndex = 0; batchIndex < results.size(); batchIndex++) {
			SpRest::Result& result = results[batchIndex];

			auto parts = Util::split(rmappedName, "$");
			ASSERT(parts.size() == 2, "Invalid mapping. Mapping must be of format <dataIndex>$<valueName> where <dataIndex> can be any value specified in the dataIndex field of the config. Value given: " << rmappedName);
			const auto& dataIndexName = parts[0];
			const auto& valueToMatch = parts[1];
			DataPtr dataIndexPtr = result.data[dataIndexName];

			if (!dataIndexPtr) {
				CWarning("Unable to find field in JSON: [%u] - %s", date, dataIndexName);
				frame.noDataUpdate = true;
				return;
			}

			const std::string* dataIndexes = (dynamic_cast<StringData*>(dataIndexPtr.get()))->sptr();
			ASSERT(dataIndexes, "dataIndex MUST be of type string!");
			size_t numDataIndexes = dataIndexPtr->cols();
			int dataIndexInArray = -1;

			for (size_t ii = 0; ii < numDataIndexes && dataIndexInArray < 0; ii++) {
				if (dataIndexes[ii] == valueToMatch) 
					dataIndexInArray = (int)ii;
			}

			ASSERT(dataIndexInArray >= 0, "Unable to find data value: " << valueToMatch << " inside the array: " << dataIndexName);

			std::string dataValueName;

			for (size_t kk = 0; kk < m_resolvedDataIndexes.size() && dataValueName.empty(); kk++) {
				if (m_resolvedDataIndexes[kk] == dataIndexName) 
					dataValueName = m_dataValues[kk];
			}

			const auto data = result.data[dataValueName];
			ASSERT(data, "Unable to find data: " << dataValueName);

			if (!newData) {
				newData = data->clone(data->universe(), true);
				newData->ensureSize(1, 1);
				newData->setMetadata(dci.customMetadata);
			}

			newData->copyElement(data.get(), 0, (size_t)dataIndexInArray, 0, 0);
		}

		if (!newData) {
			frame.noDataUpdate = true;
			return;
		}

		frame.data = DataPtr(newData);
		frame.dataOffset = 0;
		frame.dataSize = frame.data->dataSize();

		CInfo("[%-8u] - %s", date, dci.def.toString(dci.ds));
	}

	void SpGeneric::buildDailyMultiIndexed(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		std::string rmappedName;
		IntDate date = dci.rt.dates[dci.di];
		Data* newData = nullptr;
		std::string keyExtra = getKeyExtra(dr, dci, date);
		auto results = waitForResults(dr, dci, frame, rmappedName, keyExtra);

		if (!results.size()) {
			frame.noDataUpdate = true;
			return;
		}

		std::string cacheKey = makeKey(date, keyExtra);
		CacheInfoPtr ci = m_dataCache[cacheKey];
		auto numUuids = ci->numSymbols;
		size_t totalFigiCount = 0;

		size_t dataCountPerFigi = 0;
		size_t numInvalid = 0, numRemapped = 0;

		for (size_t batchIndex = 0; batchIndex < results.size(); batchIndex++) {
			SpRest::Result& result = results[batchIndex];

			auto figiData = result.data[m_symbolColName];
			if (!figiData || !figiData->cols()) {
				CWarning("No data received for date: %u", date);
				frame.noDataUpdate = true;
				return;
			}

			size_t figiCount = figiData->cols();
			const std::string* figis = (dynamic_cast<StringData*>(figiData.get()))->sptr();
			const auto& testFigi = figis[0];
			totalFigiCount += figiCount;

			auto dcPerFigiIter = m_initialDataCountPerFigi.find(rmappedName);
			if (dcPerFigiIter == m_initialDataCountPerFigi.end()) {
				if (dataCountPerFigi == 0) {
					dataCountPerFigi = 1;

					/// TODO: This can be removed at a later stage (and moved to a test) ...
					for (size_t i = 1; i < figiCount; i++) {
						if (figis[i] == testFigi)
							dataCountPerFigi++;
					}

					CTrace("Number of data points: %z", dataCountPerFigi);
				}

				m_initialDataCountPerFigi[rmappedName] = dataCountPerFigi;
			}
			else {
				dataCountPerFigi = dcPerFigiIter->second;
			}

			/// If there is just one data point per figi then its just the normaly buildDaily process
			if (dataCountPerFigi == 1) {
				buildDaily(dr, dci, frame);
				return;
			}

			//ASSERT(figiCount % dataCountPerFigi == 0, "Invalid FIGI count returned: " << figiCount << " - Expecting it to be an exact multiple of: " << dataCountPerFigi);

			auto parts = Util::split(rmappedName, "$");
			ASSERT(parts.size() == 2, "Invalid mapping. Mapping must be of format <dataIndex>$<valueName> where <dataIndex> can be any value specified in the dataIndex field of the config. Value given: " << rmappedName);
			const auto& dataIndexName = parts[0];
			const auto& valueToMatch = parts[1];
			DataPtr dataIndexPtr = result.data[dataIndexName];

			if (!dataIndexPtr) {
				CWarning("Unable to find field in JSON: [%u] - %s", date, dataIndexName);
				frame.noDataUpdate = true;
				return;
			}

			//const std::string* dataIndexes = (dynamic_cast<StringData*>(dataIndexPtr.get()))->sptr();
			const Data* dataIndexes = dataIndexPtr.get();
			//ASSERT(dataIndexes, "dataIndex MUST be of type string!");
			//int dataIndexInArray = -1;

			//for (size_t ii = 0; ii < dataCountPerFigi && dataIndexInArray < 0; ii++) {
			//	if (dataIndexes[ii] == valueToMatch) 
			//		dataIndexInArray = (int)ii;
			//}

			//ASSERT(dataIndexInArray >= 0 && dataIndexInArray < (int)dataCountPerFigi, "Unable to find data value: " << valueToMatch << " inside the array: " << dataIndexName);

			std::string dataValueName;

			for (size_t kk = 0; kk < m_resolvedDataIndexes.size() && dataValueName.empty(); kk++) {
				if (m_resolvedDataIndexes[kk] == dataIndexName)
					dataValueName = m_dataValues[kk];
			}

			const auto data = result.data[dataValueName];
			ASSERT(data, "Unable to find data: " << dataValueName);

			if (!newData) {
				newData = data->clone(data->universe(), true);
				newData->ensureSize(1, numUuids, false);
				newData->setMetadata(dci.customMetadata);
				ASSERT(newData->itemSize() == 0 || newData->dataSize() % newData->itemSize() == 0, "Invalid data size: " << newData->dataSize() << " - not aligned with item size: " << newData->itemSize());
			}

			size_t numInvalidInResult = 0;
			size_t numRemappedInResult = 0;

			for (size_t ii = 0; ii < figiCount; ii++) {
				std::string dataIndexStr = dataIndexes->getString(0, ii, 0);
				if (dataIndexStr == valueToMatch) {
					const auto& figi = figis[ii];
					SpGeneric::copyElement(dci.universe, figi, data, newData, numInvalidInResult, numRemappedInResult, ii);
				}
			}

			/// We don't need this data again (unless its the symbol column) ...
			if (rmappedName != m_symbolColName)
				result.data[rmappedName] = nullptr;
		}

		if (!newData) {
			frame.noDataUpdate = true;
			return;
		}

		frame.data = DataPtr(newData);
		frame.dataOffset = 0;
		frame.dataSize = frame.data->dataSize();

		CInfo("Updated data: [%-8u]-[Found: %5z / %5z - Invalid: %5z] - %s", date, totalFigiCount / dataCountPerFigi, ci->numSymbols, numInvalid, dci.def.toString(dci.ds));
	}

	SizeVec SpGeneric::getMultiColumnMatchInfo(IDataRegistry& dr, const DataCacheInfo& dci, SpRest::Result& result) {
		return SizeVec();
	}

	bool SpGeneric::matchTimestamp(Int64MatrixDataPtr timestamps, size_t ii, IntDate date) {
		ASSERT(timestamps, "Invalid timestamps pointer (NULL)");
		ASSERT(timestamps->cols() > ii, "Insufficient number of rows in the timestamps data: " << timestamps->cols() << " - ii: " << ii);

		IntDate dtValue = Util::dateToInt(timestamps->getDTValue(0, ii));

		if (m_exactTimestamp)
			return dtValue == date ? true : false;

		if (dtValue <= date)
			return true;
			
		return false;
	}

	struct DIInfo {
		IntDay					diSrc = 0;
		IntDay					diDst = 0;

		bool operator() (DIInfo lhs, DIInfo rhs) { return lhs.diDst < rhs.diDst; }
	};

	void SpGeneric::buildBackFilledData(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame, const std::string& rmappedName, SpRest::ResultVec& results, const StringVec& uuids) {
		size_t numUuids			= uuids.size();
		IntDate date			= dci.rt.dates[dci.di];
		size_t totalFigiCount	= 0;
		std::string dataName	= dci.def.name;
		DataPtr backfillBase	= dr.getDataPtr(dci.universe, m_backfillBase);
		IUniverse* secMaster	= dr.getSecurityMaster();
		size_t numSecurities	= secMaster->size(dci.di);
		IntDate startDate		= dci.rt.dates[dci.diStart];
		IntDate endDate			= dci.rt.dates[dci.diEnd];

		ASSERT(numUuids == numSecurities, "Invalid number of uuids which is not equal to the count in security master. Expecting: " << numSecurities << " - Found: " << numUuids);

		if (!m_readyDataStore.size() && results.size()) {
			CInfo("Compiling ready store for num batches: %z ...", results.size());

			for (size_t batchIndex = 0; batchIndex < results.size(); batchIndex++) {
				SpRest::Result& result = results[batchIndex];

				CTrace("Extracting the timestamps column: %s", m_timestampColName);
				auto timestampsPtr = result.data[m_timestampColName];
				if (!timestampsPtr) {
					CInfo("Unable to find timestamps column for batch: %z [Timestamp column: %s] - Backfill Count: %z", batchIndex, m_timestampColName, backfillBase->cols());
					continue;
				}

				Int64MatrixDataPtr timestamps = std::static_pointer_cast<Int64MatrixData>(timestampsPtr);
				if (timestamps) {
					CTrace("Got the timestamps column: %zx%z", timestamps->rows(), timestamps->cols());
				}

				IntDay numTSRows = (IntDay)timestamps->rows();
				DataPtr figiData = result.data[m_symbolColName];
				size_t figiCount = figiData->cols();

				CTrace("Extracing data for date: %u", date);
				const std::string* figis = (dynamic_cast<StringData*>(figiData.get()))->sptr();
				CTrace("Total FIGI Count in batch-%z: %z", batchIndex, figiCount);

				for (auto iter = result.data.begin(); iter != result.data.end(); iter++) {
					std::string orgDataName = iter->first;
					DataPtr data = iter->second;

					if (orgDataName == m_symbolColName || orgDataName == m_timestampColName)
						continue;

					CDebug("Processing ready data store: [Batch: %z / %z]-[UUIDS: %z / Num secs: %z]  %s / %s", batchIndex, results.size(), numUuids, numSecurities, orgDataName, rmappedName);

					DataPtr newData = nullptr;
					auto rsIter = m_readyDataStore.find(orgDataName);

					if (rsIter != m_readyDataStore.end())
						newData = rsIter->second;

					size_t numInvalidInResult = 0, numRemappedInResult = 0;

					if (!newData) {
						IntDay numRows = dci.diEnd - dci.diStart + 1;
						newData = DataPtr(data->createInstance());
						newData->ensureSize(numRows, numUuids, false);
						newData->setMetadata(dci.customMetadata);

						m_readyDataStore[orgDataName] = newData;

						/// Make one row
						for (size_t ii = 0; ii < numUuids; ii++) {
							//copyElementRange(dci.universe, uuids[ii], backfillBase, newData.get(), numInvalidInResult, numRemappedInResult, ii, 0, 0, numRows);
							copyElement(dci.universe, uuids[ii], backfillBase, newData.get(), numInvalidInResult, numRemappedInResult, ii, 0, 0);
						}

						/// And then copy over all the other rows
						for (IntDay di = 1; di < numRows; di++) {
							newData->copyRow(newData.get(), 0, di);
						}
					}

					IntDay diDst = dci.diStart;

					for (size_t ii = 0; ii < figiCount; ii++) {
						const std::string& figi = figis[ii];
						std::vector<DIInfo> diIndices;

						for (IntDay diSrc = 0; diSrc < numTSRows; diSrc++) {
							int64_t ivalue = timestamps->getValue(diSrc, ii);

							if (ivalue == 0)
								continue;

							IntDate tsDate = Util::dateToInt(timestamps->getDTValue(diSrc, ii));

							if (tsDate < startDate && tsDate > endDate)
								continue;

							IntDay diTS = dci.rt.getDateIndex(tsDate);
							IntDay diDst = std::max(diTS - dci.diStart - 1, 0); /// subtract 1 because the day is exclusive
							diIndices.push_back({ diSrc, diDst });
						}

						std::sort(diIndices.begin(), diIndices.end(), DIInfo());
						IntDay diPrevDst = dci.diStart;

						for (IntDay diIndex = 0; diIndex < diIndices.size(); diIndex++) {
							DIInfo& dii = diIndices[diIndex];
							copyElementRange(dci.universe, figi, data, newData.get(), numInvalidInResult, numRemappedInResult, ii, dii.diSrc, diPrevDst, dii.diDst);
							diPrevDst = dii.diDst;
						}
					}
				}
			}

			CInfo("... Ready store compiled!");

			/// We don't need the results vector anymore ...
			results.clear();

			//dataIter			= m_readyDataStore.find(rmappedName);
		}

		//ASSERT(dataIter != m_readyDataStore.end(), "Unable to find data in the ready data store: " << rmappedName << " [" << dataName << "]");
		auto dataIter = m_readyDataStore.find(rmappedName);

		IntDay di = dci.di - dci.diStart;
		DataPtr data = dataIter != m_readyDataStore.end() ? dataIter->second : nullptr;
		DataPtr dataRow = data ? data->extractRow(di) : nullptr;

		if (dataRow) {
			/// Now we actually construct the data ...
			frame.data = dataRow;
			frame.dataOffset = 0;
			frame.dataSize = dataRow->dataSize();
			frame.noDataUpdate = false;
		}
		else {
			CInfo("No data compiled from the webservice. Using the data from the backfill base instead: %s", dci.def.toString(dci.ds));

			DataPtr data = dr.createDataBuffer(nullptr, dci.def);
			size_t numCols = uuids.size();

			ASSERT(numCols == backfillBase->cols(), "Number of UUIDs mismatch. Expecting: " << numCols << " - Found: " << backfillBase->cols());

			data->ensureSize(1, numCols, false);

			for (size_t ii = 0; ii < numCols; ii++)
				data->copyElement(backfillBase.get(), std::min(di, (IntDay)backfillBase->rows() - 1), ii, 0, ii);

			frame.data = DataPtr(data);
			frame.dataOffset = 0;
			frame.dataSize = data->dataSize();
			frame.customMetadata = data->getMetadata();
			frame.noDataUpdate = false;
		}

		CInfo("Updated data: [%-8u]-[Count: %z] - %s", date, frame.data->cols(), dci.def.toString(dci.ds));
	}

	bool SpGeneric::doesProvideUUIDMapping() const {
		return false;
	}

	size_t SpGeneric::mapUuid(const std::string& uuid) const {
		ASSERT(false, "Invalid call to mapUuid! This should be provided by the implementation");
		return std::numeric_limits<size_t>::max();
	}

	void SpGeneric::buildReadyDataStore(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame, const std::string& rmappedName, SpRest::ResultVec& results, const StringVec& uuids) {
		if (m_backfill) {
			buildBackFilledData(dr, dci, frame, rmappedName, results, uuids);
			return;
		}

		size_t numUuids			= uuids.size();
		IntDate date			= dci.rt.dates[dci.di];
		size_t totalFigiCount	= 0;
		IUniverse* secMaster	= dr.getSecurityMaster();
		std::string dataName	= dci.def.name;
		size_t numSecurities	= secMaster->size(dci.di);

		//ASSERT(numUuids == numSecurities, "Invalid number of uuids which is not equal to the count in security master. Expecting: " << numSecurities << " - Found: " << numUuids);

		IntDate startDate		= dci.rt.dates[dci.diStart];
		IntDate endDate			= dci.rt.dates[dci.diEnd];
		std::unordered_map<std::string, size_t> uuidMap;

		if ((m_noMappingOnCopy || uuids.size() != numSecurities) && !doesProvideUUIDMapping()) {
			for (size_t ii = 0; ii < numUuids; ii++) 
				uuidMap[uuids[ii]] = ii;
		}

		if (!m_readyDataStore.size() && results.size()) {
			CInfo("Compiling ready store for num batches: %z ...", results.size());

			for (size_t batchIndex = 0; batchIndex < results.size(); batchIndex++) {
				SpRest::Result& result = results[batchIndex];

				CTrace("Extracting the timestamps column: %s", m_timestampColName);
				auto timestampsPtr = result.data[m_timestampColName];
				if (!timestampsPtr) {
					CInfo("Unable to find timestamps column for batch: %z", batchIndex);
					continue;
				}

				Int64MatrixDataPtr timestamps = std::static_pointer_cast<Int64MatrixData>(timestampsPtr);
				if (timestamps) {
					CTrace("Got the timestamps column: %zx%z", timestamps->rows(), timestamps->cols());
				}

				DataPtr figiData = result.data[m_symbolColName];
				size_t figiCount = figiData->cols();

				CTrace("Extracing data for date: %u", date);
				const std::string* figis = (dynamic_cast<StringData*>(figiData.get()))->sptr();
				CTrace("Total FIGI Count in batch-%z: %z", batchIndex, figiCount);

				for (auto iter = result.data.begin(); iter != result.data.end(); iter++) {
					std::string orgDataName = iter->first;
					DataPtr data = iter->second;

					CDebug("Processing ready data store: [Batch: %z / %z]-[UUIDS: %z / Num secs: %z] %s / %s", batchIndex, results.size(), numUuids, numSecurities, orgDataName, rmappedName);

					for (auto diter = result.dateMap.begin(); diter != result.dateMap.end(); diter++) {
						IntDate tsDate = diter->first; ///Util::dateToInt(timestamps->getDTValue(di, tsi));

						if (tsDate < startDate || tsDate > endDate)
							continue;

						IntDay diTS = dci.rt.getDateIndex(tsDate);

						/// If we couldn't find this date then we just ignore it for the time being
						if (diTS == Constants::s_invalidDay || diTS > dci.diEnd)
							continue;

						IntDay diDst = diTS - dci.diStart;
						IntDay diSrc = 0;
						if (!result.dateMap.size())
							constructDateMap(result);

						//auto dateIter = result.dateMap.find(tsDate);
						//if (dateIter == result.dateMap.end()) {
						//	//CWarning("Unable to find date: %u", date);
						//	continue;
						//}

						diSrc = diter->second;

						if (!data) {
							/// Create a NULL entry
							m_readyDataStore[orgDataName] = nullptr;
							continue;
						}

						if (orgDataName == m_timestampColName || orgDataName == m_symbolColName)
							continue;

						totalFigiCount += figiCount;

						if (figiCount > numUuids && m_passSymbols && m_useSymbolList == true)
							CDebug("More data returned than was expecting. Expected: %z - Returned: %z (This could possibly be a problem)", numUuids, figiCount);
						//ASSERT(figiCount <= numUuids, "More data returned than was expecting. Expected: " << numUuids << " - Returned: " << figiCount);

						DataPtr newData = nullptr;
						auto rsIter = m_readyDataStore.find(orgDataName);

						if (rsIter != m_readyDataStore.end())
							newData = rsIter->second;

						if (!newData) {
							IntDay numRows = dci.diEnd - dci.diStart + 1;
							newData = DataPtr(data->createInstance());
							newData->ensureSize(numRows, numUuids, false);
							newData->invalidateAll();
							newData->setMetadata(dci.customMetadata);

							m_readyDataStore[orgDataName] = newData;
						}

						size_t numInvalidInResult = 0, numRemappedInResult = 0;

						if (!m_noMappingOnCopy || uuids.size() == numSecurities) {
							for (size_t ii = 0; ii < figiCount; ii++) {
								const auto& figi = figis[ii];
								SpGeneric::copyElement(dci.universe, figi, data, newData.get(), numInvalidInResult, numRemappedInResult, ii, diSrc, diDst);
							}
						}
						else {
							size_t invalidIndex = std::numeric_limits<size_t>::max();

							for (size_t ii = 0; ii < figiCount; ii++) {
								const auto& figi = figis[ii];
								size_t figiIndex = invalidIndex;

								if (!doesProvideUUIDMapping()) {
									auto uuidIter = uuidMap.find(figi);
									ASSERT(uuidIter != uuidMap.end(), "Unable to map uuid: " << figi << std::endl);
									figiIndex = uuidIter->second;
								}
								else
									figiIndex = mapUuid(figi);

								ASSERT(figiIndex < newData->cols(), "Invalid UUID index provided: " << figiIndex << ", when the total number of UUIDs are: " << newData->cols());

								newData->copyElement(data.get(), diSrc, ii, diDst, figiIndex);
							}
						}

						/// We don't need this data again (unless its the SYMBOL column) ...
						result.data[orgDataName] = nullptr;
					}
				}
			}

			CInfo("... Ready store compiled!");

			/// We don't need the results vector anymore ...
			results.clear();

			//dataIter			= m_readyDataStore.find(rmappedName);
		}

		//ASSERT(dataIter != m_readyDataStore.end(), "Unable to find data in the ready data store: " << rmappedName << " [" << dataName << "]");
		auto dataIter			= m_readyDataStore.find(rmappedName);

		IntDay di				= dci.di - dci.diStart;
		DataPtr data			= dataIter != m_readyDataStore.end() ? dataIter->second : nullptr;
		DataPtr dataRow			= data ? data->extractRow(di) : nullptr;

		if (dataRow) {
			/// Now we actually construct the data ...
			frame.data			= dataRow;
			frame.dataOffset	= 0;
			frame.dataSize		= dataRow->dataSize();
			frame.noDataUpdate	= false;
		}
		else {
			CInfo("No data compiled from the webservice. Using EMPTY data instead: %s", dci.def.toString(dci.ds));

			DataPtr data = dr.createDataBuffer(nullptr, dci.def);
			size_t numCols = uuids.size();

			data->ensureSize(1, numCols, false);
			data->invalidateAll();

			frame.data = DataPtr(data);
			frame.dataOffset = 0;
			frame.dataSize = data->dataSize();
			frame.customMetadata = data->getMetadata();
			frame.noDataUpdate = false;
		}

		CInfo("Updated data: [%-8u]-[Count: %z / %z] - %s", date, dataRow ? dataRow->cols() : 0, numSecurities, dci.def.toString(dci.ds));
	}

	void SpGeneric::buildDaily(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		std::string rmappedName;
		IntDate date = dci.rt.dates[dci.di];
		std::string keyExtra = getKeyExtra(dr, dci, date);

		auto results = waitForResults(dr, dci, frame, rmappedName, keyExtra);

		if (!results.size()) {
			frame.noDataUpdate = true;
			return;
		}

		std::string cacheKey = makeKey(date, keyExtra);

		CacheInfoPtr ci			= m_dataCache[cacheKey];
		auto numUuids			= ci->numSymbols;
		DataPtr newData			= nullptr;
		size_t totalFigiCount	= 0;
		size_t dataCountPerFigi = 0;
		size_t numInvalid		= 0;
		size_t numRemapped		= 0;
		size_t numFoundCount	= 0;
		bool isSparseData		= dci.def.frequency > DataFrequency::kDaily ? true : false; 

		if (m_fetchesAllHistory) {
			buildReadyDataStore(dr, dci, frame, rmappedName, results, ci->uuids);
			return;
		}
		
		for (size_t batchIndex = 0; batchIndex < results.size(); batchIndex++) {
			SpRest::Result& result = results[batchIndex];
			DataPtr figiData, data;

			CTrace("Extracing data for date: %u", date);

			if (!extractData(dci, frame, date, result, figiData, data, rmappedName)) 
				continue;

			CTrace("Extracting the timestamps column: %s", m_timestampColName);
			auto timestampsPtr = result.data[m_timestampColName];
			if (!timestampsPtr && (isSparseData || m_fetchesAllHistory))
				continue;

			Int64MatrixDataPtr timestamps = std::static_pointer_cast<Int64MatrixData>(timestampsPtr);
			if (timestamps) {
				CTrace("Got the timestamps column: %zx%z", timestamps->rows(), timestamps->cols());
			}

			bool hasColInfo = hasMultiColumnMatchInfo();
			SizeVec colInfo;

			if (hasColInfo) {
				colInfo = getMultiColumnMatchInfo(dr, dci, result);

				/// If there is no column info given then there is no point in proceeding
				if (!colInfo.size())
					continue;
			}

			size_t figiCount = figiData->cols();
			const std::string* figis = (dynamic_cast<StringData*>(figiData.get()))->sptr();

			CTrace("Total FIGI Count: %z", figiCount);

			totalFigiCount += figiCount;

			if (figiCount > numUuids && m_passSymbols && m_useSymbolList == true)
              CDebug("More data returned than was expecting. Expected: %z - Returned: %z (This could possibly be a problem)", numUuids, figiCount);
			//ASSERT(figiCount <= numUuids, "More data returned than was expecting. Expected: " << numUuids << " - Returned: " << figiCount);

			/// Make sure we have enough size over here ...
			if (!newData) {
				newData = dr.createDataBuffer(data->universe(), dci.def);

				//if (dci.def.itemType == DataType::kVarFloat)
				//	newData = new FloatVarMatrixData(data->universe(), dci.def);
				//else if (dci.def.itemType == DataType::kVarInt)
				//	newData = new IntVarMatrixData(data->universe(), dci.def);
				//else if (dci.def.itemType == DataType::kVarDouble)
				//	newData = new DoubleVarMatrixData(data->universe(), dci.def);
				//else if (dci.def.itemType == DataType::kVarDateTime)
				//	newData = new Int64VarMatrixData(data->universe(), dci.def);
				//else if (dci.def.itemType == DataType::kVarTimestamp)
				//	newData = new Int64VarMatrixData(data->universe(), dci.def);
				//else if (dci.def.itemType == DataType::kVarString) {
				//	auto strData = new StringData(data->universe(), dci.def);
				//	newData = strData;
				//	strData->csvSep() = "!&!";
				//}
				//else
				//	newData = data->clone(data->universe(), true);

				if (m_symbolColName == s_symbolColName || m_isSymbolFIGI) {
					newData->ensureSize(1, numUuids, false);
					ASSERT(newData->itemSize() == 0 || newData->dataSize() % newData->itemSize() == 0, "Data: " << dci.def.name << " - Invalid data size: " << newData->dataSize() << " - not aligned with item size: " << newData->itemSize());
				}
				else if (figiCount) {
					newData->ensureSize(1, figiCount, false);
				}

				newData->setMetadata(dci.customMetadata);
			}

			size_t numInvalidInResult = 0;
			size_t numRemappedInResult = 0;

#define CHECK_SPARSE_DATA_DATE()	if (isSparseData) { \
										if (!matchTimestamp(timestamps, ii, date)) \
											continue; \
									}

			IntDay diSrc = 0;
			if (m_fetchesAllHistory) {
				if (!result.dateMap.size())
					constructDateMap(result);

				auto iter = result.dateMap.find(date);
				if (iter == result.dateMap.end()) {
					//CWarning("Unable to find date: %u", date);
					continue;
				}

				//ASSERT(iter != result.dateMap.end(), "Unable to find date: " << date << " in the result date map!");
				diSrc = iter->second;
			}

			/// The order of the returned data is not guaranteed to be in the same order in which we 
			/// sent, hence we will need to remap stuff over here
			if (!hasColInfo) {
				if (m_symbolColName == s_symbolColName || m_isSymbolFIGI) {
					for (size_t ii = 0; ii < figiCount; ii++) {

						CHECK_SPARSE_DATA_DATE();

						const auto& figi = figis[ii];
						numFoundCount++;

						SpGeneric::copyElement(dci.universe, figi, data, newData.get(), numInvalidInResult, numRemappedInResult, ii, diSrc);
					}
				}
				else {
					for (size_t ii = 0; ii < figiCount; ii++) {
						CHECK_SPARSE_DATA_DATE();
						numFoundCount++;
						newData->copyElement(data.get(), diSrc, ii, 0, ii);
					}
				}
			}
			else {
				for (size_t colIndex = 0; colIndex < colInfo.size(); colIndex++) {
					size_t ii = colInfo[colIndex];
					CHECK_SPARSE_DATA_DATE();
					const auto& figi = figis[ii];
					numFoundCount++;
					SpGeneric::copyElement(dci.universe, figi, data, newData.get(), numInvalidInResult, numRemappedInResult, ii, diSrc);
				}
			}

            CTrace("Data constructed for date: %d", date);

#undef CHECK_SPARSE_DATA_DATE

			/// We don't need this data again (unless its the SYMBOL column) ...
			if (rmappedName != m_symbolColName)
				result.data[rmappedName] = nullptr;
		}

		if (!newData || !numFoundCount) {
			newData = nullptr;
			frame.noDataUpdate = true;
			return;
		}

        CTrace("Got data of size: %z", newData->dataSize());

		frame.data = newData;
		frame.dataOffset = 0;
		frame.dataSize = frame.data->dataSize();

		if (frame.dataSize == 0)
			frame.noDataUpdate = true;

		if (keyExtra.empty()) {
			CInfo("Updated data: [%-8u]-[Found: %5z / %5z (%z) - Invalid: %5z] - %s", date, totalFigiCount, numFoundCount, newData->cols(), numInvalid, dci.def.toString(dci.ds));
		}
		else {
			CInfo("Updated data: [%-8u]-[Key: %s]-[Found: %5z / %5z (%z) - Invalid: %5z] - %s", date, keyExtra, totalFigiCount, numFoundCount, newData->cols(), numInvalid, dci.def.toString(dci.ds));
		}
	}

	void SpGeneric::constructDateMap(SpRest::Result& result) {
		ASSERT(false, "TODO!");
	}

	void SpGeneric::copyElementRange(IUniverse* universe, const std::string& uuid, DataPtr srcData, Data* dstData, size_t& numInvalid, size_t& numRemapped, size_t iiSrc, IntDay diSrc, IntDay diDstStart, IntDay diDstEnd) {
		for (IntDay di = diDstStart; di < diDstEnd; di++)
			copyElement(universe, uuid, srcData, dstData, numInvalid, numRemapped, iiSrc, diSrc, di);
	}

	void SpGeneric::copyElement(IUniverse* universe, const std::string& uuid, DataPtr srcData, Data* dstData, size_t& numInvalid, size_t& numRemapped, 
		size_t iiSrc, IntDay diSrc /* = 0 */, IntDay diDst /* = 0 */) {
		Security sec;

		if (m_uuidLookup.empty()) {
			const Security* psec = universe->mapUuid(uuid);
			if (!psec || !psec->isValid())
				return;

			sec = *psec;
		}
		else {
			ASSERT(universe == this->dataRegistry()->getSecurityMaster(), "UUID Lookups only work with security master for the time being!");
			auto iter = m_uuidLookup.find(uuid);
			if (iter == m_uuidLookup.end())
				return;

			Security::Index iiFigi = iter->second;
			sec = universe->security(0, iiFigi);

			if (!sec.isValid())
				return;
		}

		Security::Index index = sec.index;

		if (m_useValidValue && !srcData->isValid(diSrc, iiSrc)) {
			numInvalid++;
			return;
		}

		dstData->copyElement(srcData.get(), diSrc, iiSrc, diDst, (size_t)index);

		if (!srcData->isValid(diSrc, iiSrc)) {
			//CTrace("Invalid security: %s - %s", sec->uuidStr(), sec->tickerStr());
			numInvalid++;
		}

		/// This is the index where this figi should have been
		if (index != (Security::Index)iiSrc)
			numRemapped++;
	}

	void SpGeneric::build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		/// NO-OP: see buildDaily, buildUniversal etc.
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createSpGeneric(const pesa::ConfigSection& config) {
	return new pesa::SpGeneric((const pesa::ConfigSection&)config);
}

