/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// SpGeneric.cpp
///
/// Created on: 24 Aug 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/Framework.h"
#include "SpRest.h"
#include "Poco/Timestamp.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// SpGeneric
	////////////////////////////////////////////////////////////////////////////
	class SpGeneric : public IDataLoader {
	protected:
		struct CacheInfo {
			SpRestBatchPtr		restData;
			IntDay				di;
			IntDate				date;
			StringVec			uuids;
			size_t				numSymbols = 0;
			int					errCode = -1;
			bool				consumed = false;
		};

		typedef std::shared_ptr<CacheInfo> CacheInfoPtr;
		typedef std::unordered_map<std::string, CacheInfoPtr> DataCache;
		typedef std::vector<DataFrequency::Type> DataFrequencyVec;
		typedef std::unordered_map<std::string, DataPtr> DataPtrMap;
		typedef std::unordered_map<std::string, std::string> StrMap;
		typedef std::unordered_map<std::string, size_t> SizeMap;
		typedef std::unordered_map<std::string, Security::Index> DataToSecurityIndexMap;
		typedef std::unordered_map<std::string, DataPtr>	ReadyDataStore;

		static const int		s_maxRequestUuids = 8000;	/// How manu UUIDs to send in a single request
		static const int		s_minUuids = 3000;			/// If the total number of UUIDs is less than this then we always send the symbol list and never do IDs=None!

		#ifndef EIGEN_DONT_VECTORIZE
			static const IntDay	s_maxPrecache = 8;			/// Precache upto 30 items
		#else
			static const IntDay	s_maxPrecache = 4;			/// Precache upto 30 items
		#endif 

		static const std::string s_symbolColName;			/// Name of the symbol column

		std::string				m_datasetName;				/// The name of this dataset
		StringVec 				m_datasets; 				/// What datasets are we supposed to load
		StringVec				m_datasetNames;				/// Resolved and cleaned dataset names
		SpRest::StringMap		m_mappings;					/// Mappings from the original name to our custom name
		SpRest::StringMap		m_rmappings;				/// Reverse mappings
		DataCache				m_dataCache;				/// Data cache
		ReadyDataStore			m_readyDataStore;			/// Read data store
		IntDay					m_diLast = -1;				/// The last day that we cached ...
		StringVec				m_dataRequired;				/// What data do we require 
		SpRest::DataTypeVec		m_requiredDataTypes;		/// The types of the data that we need
		DataFrequencyVec		m_requiredDataFrequencies;	/// The frequencies of the required data sets
		bool					m_passSymbols = false;		/// Do we need to pass symbols to this end point?
		bool					m_useSymbolList = false;	/// Whether to pass the entire symbol list to the Data API
		bool					m_exactSymbolList = false;	/// Use the EXACT symbol list and never fallback to all the symbols in the unvirse
		bool					m_alwaysOverwrite = false;	/// Always overwrite the data
		bool					m_useValidValue = false;	/// Only overwrite if the data is valid
		bool					m_uuidMapping = true;		/// Whether we want to do uuid mapping
		bool					m_noMappingOnCopy = false;	/// Don't map while copying
		int						m_maxRequestUuids = s_maxRequestUuids; /// Max uuids per request
		int						m_requestDelay = 0;			/// Delay between each request payload because of data limits etc. 
		std::string				m_symbolColName;			/// The name of the symbol column
		std::string				m_timestampColName = "";	/// The name of the TIMESTAMP column
		bool					m_exactTimestamp = true;	/// Match exact timestamp or not
		IntDay					m_maxPrecache;				/// What is the max size of the precache ring buffer
		bool					m_isSymbolFIGI = false;		/// Only used in case of m_symbolColName != "SYMBOL". This tells the system to still treat the data as a FIGI
		std::string				m_dateFormat = "{$YYYY}-{$MM}-{$DD}"; /// What is the dateFormat to use
		bool					m_fetchesAllHistory = false;/// Whether this config fetches all the history in one go?

		bool					m_backfill = false;			/// Back fill the data
		std::string				m_backfillBase;				/// The base data to use for backfil

		IntDay					m_deliveryDelay = 0;		/// Is there some sort of delivery delay in the data? This is used to get rid of the forward bias that may accumulate in historical data

		std::string				m_uuidData = "";			/// The UUID data to pass through for mapping
		std::string				m_uuidType = "";			/// A specific type of uuid being used (e.g. sedol, cusip, ticker etc.)?
		DataToSecurityIndexMap	m_uuidLookup;				/// The UUID to security index lookup table

		SizeMap					m_initialDataCountPerFigi;	/// What's the best guess data count per figi

		StringVec				m_resolvedDataIndexes;		/// Resolved names of the data indexes
		StringVec				m_dataIndexes;				/// Data indexes for indexed data
		StringVec				m_dataValues;				/// Data values for indexed data

		SpRest::MoreInfo		m_minfo;					/// More info about caching
		StrMap					m_additionalArgs;			/// The additional args that can be specified with the dataset

		bool					m_slowMoving = false;		/// Whether this is slow moving data ...
		bool					m_noEmptyArguments = false;	/// Don't pass empty arguments (defaults to false for backwards compatibility reasons!)

		IntDay					m_maxCacheDaysToKeep = 2;	/// Maximum number of cache days to keep

		static std::string		makeKeyDirect(IntDate date, const std::string& keyExtra);
		static IntDate			getLastFiscalQuarterDate(IntDate date);

		bool					matchTimestamp(Int64MatrixDataPtr timestamps, size_t ii, IntDate date);
		virtual std::string		makeKey(IntDate date, const std::string& keyExtra) const;
		virtual bool			hasMultiColumnMatchInfo() const { return false; }
		virtual SizeVec			getMultiColumnMatchInfo(IDataRegistry& dr, const DataCacheInfo& dci, SpRest::Result& result);
		void					buildUuidLookupTable(IDataRegistry& dr, const std::string& universeId);
		void					buildUuidLookupTableForType(IDataRegistry& dr, const std::string& universeId);
		virtual bool			clearOldCache(IDataRegistry& dr, const DataCacheInfo& dci, const std::string& keyExtra);
		virtual void 			preCache(IDataRegistry& dr, const DataCacheInfo& dci, const std::string& keyExtra);
		virtual CacheInfoPtr	preCacheDay(IDataRegistry& dr, const DataCacheInfo& dci, IntDay di, const std::string& keyExtra);
		void					resolveArgs(StrMap& args, const DataCacheInfo& dci, IntDay di);
		virtual std::string& 	resolveArg(std::string& value) const;
		virtual std::string		buildSymbolsStr(StringVec& uuids, const DataCacheInfo& dci, IntDay di, size_t startUuidIndex, size_t endUuidIndex);
		virtual bool			doesProvideUUIDMapping() const;
		virtual size_t			mapUuid(const std::string& uuid) const;

		//virtual void			splitResults(IDataRegistry& dr, const DataCacheInfo& dci, SpRest::ResultVec& results);
		//virtual void			splitResult(IDataRegistry& dr, const DataCacheInfo& dci, SpRest::Result& result);

		virtual SpRest::ResultVec waitForResults(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame, std::string& rmappedName, std::string keyExtra = "");
		virtual SpRest::Result	waitForResult(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame, std::string& rmappedName, std::string keyExtra = "");
		bool					extractData(const DataCacheInfo& dci, DataFrame& frame, IntDate date, SpRest::Result& result, DataPtr& figiData, DataPtr& data, const std::string& dataName);
		virtual std::string		getKeyExtra(IDataRegistry& dr, const DataCacheInfo& dci, IntDate date) const;
		 
		void					copyElement(IUniverse* universe, const std::string& uuid, DataPtr srcData, Data* dstData, size_t& numInvalid, size_t& numRemapped, size_t iiSrc, IntDay diSrc = 0, IntDay diDst = 0);
		void					copyElementRange(IUniverse* universe, const std::string& uuid, DataPtr srcData, Data* dstData, size_t& numInvalid, size_t& numRemapped, size_t iiSrc, IntDay diSrc, IntDay diDstStart, IntDay diDstEnd);
		std::string				getRMappedName(const std::string& dataName) const;
		virtual IntDay			getMaxPrecache() const;
		IntDay					getUsageCount() const;
		virtual SpRest::IParser* createJsonParser();
		virtual std::string		getServerUrl(IDataRegistry& dr, const DataCacheInfo& dci, IntDay di) const;
		virtual void			getAllSymbols(IDataRegistry& dr, const DataCacheInfo& dci, IntDay di, std::vector<std::string>& ids) const;

		virtual void 			buildBackFilledData(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame, const std::string& rmappedName, SpRest::ResultVec& results, const StringVec& uuids);
		virtual void 			buildReadyDataStore(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame, const std::string& rmappedName, SpRest::ResultVec& results, const StringVec& uuids);

		void					constructDateMap(SpRest::Result& result);

	public:
								SpGeneric(const ConfigSection& config);
		virtual 				~SpGeneric();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			buildUniversal(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void 			buildDaily(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void 			buildDailyMultiIndexed(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void 			buildDailyMultiIndexed_NonUUID(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void 			preBuild(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);

		virtual void			preDataBuild(IDataRegistry& dr, const DataCacheInfo& dci);
		virtual void 			postDataBuild(IDataRegistry& dr, const pesa::DataCacheInfo& dci);

		virtual std::string 	id() const {
			return IComponent::id() + "." + m_datasetName;
		}

		////////////////////////////////////////////////////////////////////////////
		/// Inline functions
		////////////////////////////////////////////////////////////////////////////
		const StringVec&		dataRequired() const { return m_dataRequired; }
		const SpRest::DataTypeVec& requiredDataTypes() const { return m_requiredDataTypes; }
		const SpRest::StringMap& mapping() const { return m_mappings; }
		const SpRest::StringMap& rmapping() const { return m_rmappings; }
	};
} /// namespace pesa

