/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// SpRest.cpp
///
/// Created on: 20 Aug 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "curl/curl.h"
#include "curl/easy.h"

#include "Poco/String.h"
#include "Poco/DateTimeParser.h"

#include "Helper/DB/MongoStream.h"
#include "SpRest.h"

#include "Poco/Environment.h"

#ifndef __WINDOWS__
	#define stricmp strcasecmp
	#define strnicmp strncasecmp
#endif

using namespace pesa::io;
using namespace bsoncxx;
using namespace mongocxx;

namespace pesa {
	std::string SpRest::s_dbName = "api_cache";
	const int SpRest::s_defResponseBufferLen = 2 * 1024 * 1024;

	SpRest::SpRest(const ConfigSection& config, IntDate date /* = 0 */) {
		initConfig(config, date);
	}

	SpRest::SpRest(const ConfigSection& config, const StringVec& requiredFields, IntDate date /* = 0 */)
		: m_requiredFields(requiredFields) {
		initConfig(config, date);
	}

	SpRest::~SpRest() {
		clearResponse();
		delete m_promise;
		delete m_thread;
	}

	void SpRest::constructHierarchicalData(Result& result, HierarchicalData& hdata, StringVec& fields) {
		Trace("SpRest", "Constructing hierarchical data!");
		DataPtr figiData = result.data["SYMBOL"];

		/// If there is no figi data ...
		if (!figiData) {
			hdata.figis.clear();
			return;
		}

		const std::string* figis = (dynamic_cast<StringData*>(figiData.get()))->sptr();
		size_t figiCount = figiData->cols();
		ASSERT(figiCount, "No figis in the result!");
		DataMap cloneables;

		if (!fields.size()) {
			for (auto riter : result.data) {
				const std::string& dataName = riter.first;
				fields.push_back(dataName);
			}
		}

		for (auto& dataName : fields) {
			auto riter = result.data.find(dataName);
			ASSERT(riter != result.data.end(), "Unable to find field: " << dataName);
			DataPtr srcData = riter->second;
			auto cloneable = DataPtr(srcData->clone(nullptr, true));
			cloneable->ensureSize(1, 1);
			cloneables[dataName] = cloneable;
		}

		/// first of all we construct a list of unique figi
		size_t numUniqueFigis = 0;
		for (size_t ii = 0; ii < figiCount; ii++) {
			const std::string& figi = figis[ii];
			auto iter = hdata.dmap.find(figi);
			if (iter == hdata.dmap.end()) {
				FigiData data;
				data.figi = figi;
				data.numValues = 1;
				hdata.dmap[figi] = data;
				numUniqueFigis++;
				hdata.figis.push_back(figi);
			}
			else {
				iter->second.numValues++;
			}
		}

		Trace("SpRest", "Number of unique figis: %z", numUniqueFigis);

		/// OK, now we can go and construct the hierarchical data now ...
		for (size_t ii = 0; ii < figiCount; ii++) {
			const std::string& figi = figis[ii];
			auto hiter = hdata.dmap.find(figi);
			ASSERT(hiter != hdata.dmap.end(), "Unable to find figi (in dmap): " << figi);

			FigiData& fdata = hiter->second;

			for (auto dataName : fields) {
				if (dataName == "SYMBOL")
					continue;

				DataPtr srcData = result.data[dataName];
				DataPtr cloneable = cloneables[dataName];
				DataPtr dstData;
				auto diter = fdata.data.find(dataName);

				if (diter == fdata.data.end()) {
					/// now we wanna clone this data
					dstData = DataPtr(cloneable->clone(nullptr, true));
					dstData->ensureSize(1, fdata.numValues);
					fdata.data[dataName] = dstData;
				}
				else 
					dstData = diter->second;

				size_t srcIndex = ii;
				size_t dstIndex = fdata.numCopied;
				dstData->copyElement(srcData.get(), 0, srcIndex, 0, dstIndex);
			}

			fdata.numCopied++;
		}
	}

	void SpRest::initConfig(const ConfigSection& config, IntDate date /* = 0 */) {
		m_responseText = nullptr;
		clearResponse();

		m_serverUrl = config.getServerUrl();
		m_proxyServer = config.getMacro("PROXY_SERVER", true);

		if (m_proxyServer.empty())
		    m_proxyServer = Poco::Environment::get("PROXY_SERVER", "");

		if (m_dbCache) {
			std::string indexFields = config.getString("indexFields", "");
			if (indexFields.empty()) {
				m_indexFields = {
					"StartDate", "EndDate", "Keyword"
				};
			}
			else {
				m_indexFields = Util::split(indexFields, ",");
			}
		}
	}

	void SpRest::clearResponse() {
		//fprintf(stderr, "Clearing response: %p\n", (void*)this);
		Trace("SpRest", "Clearing response: %d", m_responseBufferLen);

		delete[] m_responseText;
		m_responseText = nullptr;

		/// Clear the JSON allocator too
		delete m_jsonAllocator;
		m_jsonAllocator = nullptr;

		m_responseText = nullptr;
		m_responseTextLen = 0;
		m_responseBufferLen = 0;
		m_expectedResponseLen = 0;
		m_responseBufferLen = 0;
	}

	size_t SpRest::curlHeaderCallback(char* ptr, size_t size, size_t nmemb, void* userdata) {
		SpRest* self = reinterpret_cast<SpRest*>(userdata);
		return self->headerCallback(ptr, size, nmemb);
	}

	size_t SpRest::headerCallback(char* contents, size_t size, size_t nmemb) {
		std::string contentStr = contents;
		auto realSize = size * nmemb;
		auto parts = Util::split(contentStr, ":");

		if (parts.size() == 2 && Poco::toLower(parts[0]) == "content-length") {
			m_expectedResponseLen = Util::cast<int>(parts[1]);
			m_responseBufferLen = m_expectedResponseLen + 1;
			m_responseText = new char [m_responseBufferLen];
		}

		return realSize;
	}

	size_t SpRest::curlCallback(char* ptr, size_t size, size_t nmemb, void* userdata) {
		SpRest* self = reinterpret_cast<SpRest*>(userdata);
		return self->callback(ptr, size, nmemb);
	}

	size_t SpRest::callback(char* contents, size_t size, size_t nmemb) {
		int realSize = (int)(size * nmemb);
		int existingSpace = m_responseBufferLen - m_responseTextLen;

        //fprintf(stderr, "Got data: %p (size = %d, R-Buffer = %d, R-Len = %d)\n", (void*)this, realSize, m_responseBufferLen, m_responseTextLen);

        ASSERT(existingSpace >= 0, "Invalid buffer state!");
        ASSERT(realSize >= 0 && realSize < 1000 * 1024 * 1024, "Invalid input size is too big!");
    
        if (!m_responseText) {
			m_responseBufferLen = s_defResponseBufferLen;
			m_responseText = new char [s_defResponseBufferLen];
			memset(m_responseText, 0, m_responseBufferLen);
		}
		else if (realSize >= existingSpace) {
			auto existingBufferLen = m_responseBufferLen;
			auto* existingText = m_responseText;

			/// We add s_defResponseBufferLen bytes each time
            int rb = m_responseBufferLen + std::max(s_defResponseBufferLen, realSize - existingSpace + 1); /// 1 for the NULL terminator
            ASSERT(rb >= 0 && rb < 1000 * 1024 * 1024, "Invalid buffer state!?");
            m_responseBufferLen = rb;
			m_responseText = new char [m_responseBufferLen];

			if (existingText && existingBufferLen) {
				memcpy(m_responseText, existingText, existingBufferLen);
				delete[] existingText;
				existingText = nullptr;
			}
		}

        ASSERT((realSize < (m_responseBufferLen - m_responseTextLen)), "Buffer overflow!");
		ASSERT(m_responseText, "Unable to allocate enough memory to keep response!");

        ASSERT(m_responseTextLen < 1000 * 1024 * 1024, "What AGAIN!?");

		ASSERT(!m_expectedResponseLen || m_responseBufferLen == m_expectedResponseLen + 1, "Invalid response buffer length: " << m_responseBufferLen << " - Content size was: " << m_expectedResponseLen);

		strncpy(m_responseText + m_responseTextLen, contents, realSize);
		m_responseTextLen += realSize;
		m_responseText[m_responseTextLen] = '\0';

		return realSize;
	}

	std::string SpRest::toPostString(const StringMap& params) {
		std::string postStr = "";
		std::string prefix = "";

		for (auto iter = params.begin(); iter != params.end(); iter++) {
			const std::string& name = iter->first;
			const std::string& value = iter->second;

			postStr += prefix + name + "=" + value;
			prefix = "&";
		}

		return postStr;
	}

	std::string SpRest::toJson(const StringMap& params) {
		std::string jsonStr = "";

		for (auto iter = params.begin(); iter != params.end(); iter++) {
			const std::string& name = iter->first;
			const std::string& value = iter->second;

			jsonStr += "\"" + name + "\":\"" + value + "\"";
		}

		return "{" + jsonStr + "}";
	}

	float SpRest::jsonFloatValue(gason::JsonValue value) {
		auto tag = value.getTag();
		if (tag == gason::JSON_NULL || (tag == gason::JSON_STRING && !stricmp(value.toString(), "nan")))
			return std::nanf("");
		else if (tag == gason::JSON_STRING)
			return Util::cast<float>(value.toString());
		return (float)value.toNumber();
	}

	std::string SpRest::jsonStringValue(gason::JsonValue value) {
		auto tag = value.getTag();
		if (tag == gason::JSON_NULL)
			return std::string();
		if (tag == gason::JSON_STRING)
			return std::string(value.toString());
		else if (tag == gason::JSON_NUMBER)
			return Util::cast(value.toInt());

		ASSERT(false, "Unsupported convertion to string from tag: " << tag);
		return std::string();
	}

	double SpRest::jsonDoubleValue(gason::JsonValue value) {
		auto tag = value.getTag();
		if (tag == gason::JSON_STRING && !stricmp(value.toString(), "nan"))
			return std::nanf("");
		else if (tag == gason::JSON_STRING)
			return Util::cast<double>(value.toString());
		return value.toNumber();
	}

	int SpRest::jsonIntValue(gason::JsonValue value) {
		auto tag = value.getTag();
		if (tag == gason::JSON_NULL || (tag == gason::JSON_STRING && !stricmp(value.toString(), "nan")))
			return 0;
		else if (tag == gason::JSON_STRING)
			return Util::cast<int>(value.toString());
		return value.toInt();
	}

	bool SpRest::jsonArray(SpRest* self, const std::string& field, gason::JsonValue value, StringVec& result, StringVec* keys) {
		return jsonObjectGeneric<std::string>(value, result, keys, jsonStringValue);
	}

	bool SpRest::jsonArray(SpRest* self, const std::string& field, gason::JsonValue value, FloatVec& result, StringVec* keys) {
		return jsonObjectGeneric<float>(value, result, keys, jsonFloatValue);
	}

	bool SpRest::jsonArray(SpRest* self, const std::string& field, gason::JsonValue value, DoubleVec& result, StringVec* keys) {
		return jsonObjectGeneric<double>(value, result, keys, jsonDoubleValue);
	}

	bool SpRest::jsonArray(SpRest* self, const std::string& field, gason::JsonValue value, IntVec& result, StringVec* keys) {
		return jsonObjectGeneric<int>(value, result, keys, jsonIntValue);
	}

	bool SpRest::jsonArray(SpRest* self, const std::string& field, gason::JsonValue value, UIntVec& result, StringVec* keys) {
		return jsonObjectGeneric<unsigned int>(value, result, keys, [](gason::JsonValue value)->unsigned int {
			return (unsigned int)value.toInt();
		});
	}

	bool SpRest::jsonArray(SpRest* self, const std::string& field, gason::JsonValue value, Int64Vec& result, StringVec* keys) {
		return jsonObjectGeneric<int64_t>(value, result, keys, [](gason::JsonValue value)->int64_t {
			auto tag = value.getTag();
			if (tag == gason::JSON_NULL || (tag == gason::JSON_STRING && !stricmp(value.toString(), "nan")))
				return 0;
			return (int64_t)(value.fval);
		});
	}

	template <typename t_type, typename t_dataType>
	static inline DataPtr jsonArray(SpRest* self, const std::string& field, gason::JsonValue value, StringVec* keys) {
		std::vector<t_type> data;

		if (SpRest::jsonArray(self, field, value, data, keys))
			return std::shared_ptr<t_dataType>(new t_dataType(nullptr, data));

		return nullptr;
	}

	DataPtr SpRest::jsonArray(SpRest*self, const std::string& field, gason::JsonValue value, DataType::Type type, StringVec* keys) {
		DataPtr result = nullptr;

		switch (type) {
		case DataType::kFloat: 
		case DataType::kFloatArray:
		case DataType::kVarFloat:
			return pesa::jsonArray<float, FloatMatrixData>(self, field, value, keys);

		case DataType::kVarDouble:
		case DataType::kDouble:
			return pesa::jsonArray<double, DoubleMatrixData>(self, field, value, keys);

		case DataType::kInt:
		case DataType::kIntArray:
		case DataType::kVarInt:
			return pesa::jsonArray<int, IntMatrixData>(self, field, value, keys);

		case DataType::kInt64:
			return pesa::jsonArray<int64_t, Int64MatrixData>(self, field, value, keys);

		case DataType::kUInt:
		case DataType::kUIntArray:
			return pesa::jsonArray<unsigned int, UIntMatrixData>(self, field, value, keys);

		case DataType::kString:
		case DataType::kStringArray:
		case DataType::kVarString:
			return pesa::jsonArray<std::string, StringData>(self, field, value, keys);

		case DataType::kTimestamp:
		case DataType::kVarTimestamp:
			return pesa::jsonArray<int64_t, Int64MatrixData>(self, field, value, keys);

		case DataType::kVarDateTime:
		case DataType::kDateTime: 
			{
				std::vector<std::string> data;
				if (SpRest::jsonArray(self, field, value, data, keys)) {
					Int64MatrixData* dtData = new Int64MatrixData(nullptr, 1, data.size());

					for (size_t ii = 0; ii < data.size(); ii++) {
						int tz = 0;
						auto dt = Util::strDateTimeToUTCDateTime(data[ii], &tz);

						if (tz != 0)
							Trace("SpRest", "BAD date time which is NOT UTC!: %s - Field: %s", data[ii], field);

						dtData->setDTValue(0, ii, dt);
					}

					return Int64MatrixDataPtr(dtData);
				}

				return nullptr;
			}

		default:
			break;
		}

		return result;
	}

	int SpRest::parseResult() {
		/// If there is a custom JSON parser given then just use that ...
		if (m_minfo.jsonParser) 
			return m_minfo.jsonParser->parseResults(this, m_result);

		StringVec ctypes, cnames;
		SpRest::jsonArray(this, "column_types", m_jsonResult.child("column_types"), ctypes, &cnames);

		ASSERT(ctypes.size() == cnames.size(), "Mismatchend number of columns. How did this happen!");
		for (size_t i = 0; i < ctypes.size(); i++)
			m_result.columnDefs[cnames[i]] = ctypes[i];

		bool useJsonForTypeDefs = false;
		auto requiredFields = m_requiredFields;

		/// If no column names are given, then we just get all the values ...
		/// This is not going to be the common scenario ...
		if (m_requiredFields.empty() || !m_minfo.specifiedFieldsOnly) {
			requiredFields = cnames;
			useJsonForTypeDefs = true;
		}

		auto resultData = m_jsonResult.child("data");

		/// Now we do the conversion ...
		for (size_t i = 0; i < requiredFields.size(); i++) {
			const std::string& field = requiredFields[i];
			if (m_result.columnDefs.find(field) == m_result.columnDefs.end()) 
				continue;

			DataType::Type dataType = DataType::kUnknown;

			/// If the user has explicity set a field to something then use that ... otherwise, deduce from the JSON payload
			if (!useJsonForTypeDefs && i < m_requiredFieldTypes.size())
				dataType = m_requiredFieldTypes[i];
			else {
				/// We first try to match what we have asked for the data to be. This is because the BAM REST API
				/// reports the type of some data incorrectly as 'str'! Hence we only use it as a last resort
				for (size_t ii = 0; ii < m_requiredFieldTypes.size() && ii < m_requiredFields.size() && dataType == DataType::kUnknown; ii++) {
					if (m_requiredFields[ii] == field) 
						dataType = m_requiredFieldTypes[ii];
				}
			}

			/// Only use this as a last resort
			if (dataType == DataType::kUnknown)
				dataType = DataType::parse(m_result.columnDefs[field]);

			DataPtr data = SpRest::jsonArray(this, field, resultData.child(field.c_str()), dataType, nullptr);
			if (!data) {
                Trace("SpRest", "Unable to retrieve data for: %s", field);
			}
			// ASSERT(data, "Unable to retrieve data for: " << field);
			m_result.data[field] = data;
		}

		return 0;
	}

	int debugCallback(CURL *handle, curl_infotype type, char *data, size_t size, void *userptr) {
		const char *text;

		switch (type) {
		case CURLINFO_TEXT:
			Trace("SpRest", "%s", std::string(data, size));
			return 0;
		//case CURLINFO_HEADER_OUT:
		//	text = "=> Send header";
		//	break;
		//case CURLINFO_DATA_OUT:
		//	text = "=> Send data";
		//	break;
		//case CURLINFO_SSL_DATA_OUT:
		//	text = "=> Send SSL data";
		//	break;
		//case CURLINFO_HEADER_IN:
		//	text = "<= Recv header";
		//	break;
		//case CURLINFO_DATA_IN:
		//	text = "<= Recv data";
		//	break;
		//case CURLINFO_SSL_DATA_IN:
		//	text = "<= Recv SSL data";
		//	break;
		default:
			return 0;
		}

		Trace("SpRest", "%s", std::string(text));
		return 0;
	}

	void SpRest::createIndexes(const std::string& collName) {
		//if (!m_dbCache || !MongoClient::isAvailable())
		//	return;

		//BsonDocumentBuilder doc{};

		//for (auto iter = m_params.begin(); iter != m_params.end(); iter++) {
		//	std::string indexName = iter->first;
		//	doc << indexName << 1;
		//}

		////doc << finalize;

		//MongoClient mongoCl;
		//auto coll = mongoCl.coll(collName, s_dbName.c_str());
		//options::index indexOpt;
		//indexOpt.background(true);

		//coll.create_index(doc.view(), indexOpt);
	}

	int SpRest::saveDataDB() {
		if (!m_dbCache || !MongoClient::isAvailable())
			return 0;

		try {
			auto collName = m_params["Keyword"];
			auto id = SpRest::makeId(m_params);

			/// No ID means we can't do anything with this dataset
			if (id.empty())
				return 0;

			OutputMongoStream os(collName, id, nullptr, s_dbName.c_str());

			for (auto iter = m_params.begin(); iter != m_params.end(); iter++) {
				std::string key = iter->first;
				std::string value = iter->second;

				os.write(key.c_str(), value);
			}

			std::string responseText = m_responseText;

			//os.replace() = true;
			os.write("responseText", responseText);

			os.flush();
		}
		catch (const std::exception& e) {
			Util::reportException(e);
		}


		return 0;
	}

	std::string SpRest::loadDataDB() {
		if (!m_dbCache || !MongoClient::isAvailable())
			return "";

		auto collName = m_params["Keyword"];
		createIndexes(collName);

		std::string id = SpRest::makeId(m_params);

		/// No ID means we can't do anything with this dataset
		if (id.empty())
			return "";

		BsonDocumentBuilder find;

		//for (auto iter = m_params.begin(); iter != m_params.end(); iter++) {
		//	find << iter->first << iter->second;
		//}

		//MongoClient mongoCl;
		//auto coll = mongoCl.coll(collName, s_dbName.c_str());
		//auto result = coll.find_one(find.view());
		//if (!result) {
		//	Debug("JOB", "No more pending jobs within the system!");
		//	return "";
		//}

		/// OK now that we have a job ... load it ...
		InputMongoStream is(collName, id, s_dbName.c_str());

		if (!is.isValid())
			return "";

		std::string responseText;
		std::string fields;

		is.read("Fields", &fields);
		auto itFields = m_params.find("Fields");

		if (!fields.empty() && itFields != m_params.end() && fields != itFields->second)
			return "";

		is.read("responseText", &responseText);

		return responseText;
	}

	std::string SpRest::makeId(const StringMap& params) {
		std::string id;

		for (auto indexField : m_indexFields) {
			auto iter = params.find(indexField);
			if (iter != params.end())
				id += iter->second + "-";
		}

		return id;
	}

	int SpRest::makeRequest(int numRetries) {
		std::string dbResponse;

		/// If the dbCache is enabled and we find data in the db
		if (m_dbCache) 
			dbResponse = loadDataDB();
		 
		/// If we received nothing from the database
		if (dbResponse.empty()) {
			m_result.columnDefs.clear();
			m_result.data.clear();

			m_userAuth = !m_username.empty() && !m_password.empty() ? m_username + ":" + m_password : "";

			m_curlStatus = -1;
			m_httpStatus = 200;

			std::string baseServerUrl = !m_minfo.serverUrl.empty() ? m_minfo.serverUrl : m_serverUrl;
			std::string serverUrl = m_serverUrl;
			
			if (m_minfo.useHttpGet && !m_postString.empty()) {
				serverUrl = baseServerUrl;
				if (baseServerUrl.find('?') != std::string::npos)
					serverUrl += "&";
				else
					serverUrl += "?"; 
				serverUrl += m_postString;
			}

			while (m_curlStatus != CURLE_OK && numRetries) {
				/// We start off by clearing whatever response buffers we currently have. This will ensure
				/// that during the retries, we always have a clean buffer

				clearResponse();

				CURL* c = curl_easy_init();

				if (!m_userAuth.empty()) {
					curl_easy_setopt(c, CURLOPT_HTTPAUTH, CURLAUTH_NTLM);
					curl_easy_setopt(c, CURLOPT_USERPWD, m_userAuth.c_str());
				}

				curl_easy_setopt(c, CURLOPT_NOSIGNAL, 1);
				curl_easy_setopt(c, CURLOPT_URL, serverUrl.c_str());
				if (!m_minfo.useHttpGet && !m_postString.empty())
					curl_easy_setopt(c, CURLOPT_POSTFIELDS, m_postString.c_str());

				curl_easy_setopt(c, CURLOPT_WRITEDATA, reinterpret_cast<void*>(this));
				curl_easy_setopt(c, CURLOPT_WRITEFUNCTION, SpRest::curlCallback);
				curl_easy_setopt(c, CURLOPT_HEADERDATA, reinterpret_cast<void*>(this));
				curl_easy_setopt(c, CURLOPT_HEADERFUNCTION, SpRest::curlHeaderCallback);
				curl_easy_setopt(c, CURLOPT_TIMEOUT, (long)s_timeout);

				curl_easy_setopt(c, CURLOPT_VERBOSE, 1);
				curl_easy_setopt(c, CURLOPT_DEBUGFUNCTION, debugCallback);

				if (!m_proxyServer.empty())
					curl_easy_setopt(c, CURLOPT_PROXY, m_proxyServer.c_str());

				Poco::Timestamp startTime;

				m_curlStatus = curl_easy_perform(c);
				
				/// Measure the time it takes for the request to go through
				float seconds = (float)startTime.elapsed() * 0.001f * 0.001f;

				curl_easy_getinfo(c, CURLINFO_RESPONSE_CODE, &m_httpStatus);

				curl_easy_cleanup(c);

				if (seconds > 15.0f) {
					Debug("SpRest", "HTTP request took: %0.2hf seconds", seconds);
				}

				/// Call was successful
				if (m_curlStatus == CURLE_OK && m_httpStatus >= 200 && m_httpStatus < 300)
					break;

                ///clearResponse();

				Warning("SpRest", "REST call failed! CURL Status: %d - HTTP Status: %d - Num tries left: %d", m_curlStatus, m_httpStatus, numRetries - 1);
				numRetries--;
				m_curlStatus = -1;
			}

			//Debug("SpRest", "%s", std::string(m_responseText));
			//Debug("SpRest", "Successfully executed REST call");

			if (m_curlStatus != CURLE_OK) {
				Error("SpRest", "CURL Error: %d", m_curlStatus);

				if (m_responseText)
					Error("SpRest", "Response Text: %s", std::string(m_responseText));

				clearResponse();
				Error("SpRest", "Post String: %s", m_postString);

				return m_curlStatus;
			}

			if (m_httpStatus < 200 || m_httpStatus >= 300) {
				Error("SpRest", "HTTP Error: %d", m_httpStatus);

				if (m_responseText)
					Error("SpRest", "Response Text: %s", std::string(m_responseText));

				clearResponse();
				Error("SpRest", "Post String: %s", m_postString);

				return m_httpStatus;
			}

			/// Ok, now that we are here and db cache is enabled then we can just save the data in the database
			saveDataDB();
		}
		else {
			/// We just use the data that was part of the database
			m_curlStatus = CURLE_OK;
			m_responseTextLen = (int)dbResponse.length() + 1;
			m_responseBufferLen = m_responseTextLen;
			m_responseText = new char [m_responseTextLen];
			memcpy((char*)m_responseText, dbResponse.c_str(), dbResponse.length());
			m_responseText[dbResponse.length()] = '\0';
		}

		//Debug("SpRest", "Response: %s", std::string(m_responseText));

		char* startPtr = m_responseText;
		char* endPtr = nullptr;

		//Info("REST", "%s", m_postString);

		//std::string rtext = m_responseText;
		//Debug("SpRest", "Response: %s", rtext);

		if (m_minfo.parseJson) {
			m_jsonAllocator = new gason::JsonAllocator();
			m_jsonStatus = gason::Json::parse(startPtr, &endPtr, &m_jsonResult, *m_jsonAllocator);

			/// We're unable to parse the JSON correctly ...
			if (m_jsonStatus != gason::JSON_PARSE_OK) {
				Error("SpRest", "Unable to parse JSON of length: %z", m_responseTextLen);
				Error("SpRest", "Post string: %s", m_postString);
				clearResponse();
				return m_jsonStatus;
			}
		}

		//std::vector<float> data;
		//auto value = m_jsonResult.child("data").child("Price_momentum");

		//SpRest::jsonArray(value, data, nullptr);

		return 0;
	}

	int SpRest::getData(int numRetries) {
		int errCode = -1;

		while (numRetries > 0 && errCode != 0) {
			int requestErrCode = makeRequest(m_numRetries);
			if (requestErrCode)
				return requestErrCode;

			errCode = parseResult();
			if (errCode) {
				Error("SpRest", "parseResult returned error: %d [Num Retries Left: %d]", errCode, numRetries);
				numRetries--;

				/// Sleep for a bit and then retry again ...
				Poco::Thread::sleep((long)1000);

				ASSERT(numRetries > 0, "Max retries exceeded. ServerUrl: " << m_serverUrl << " - Post string: " << m_postString);
			}
		}

		/// We CANNOT keep the response if json parsing is requested. This is because the JSON parsing modifies
		/// the source buffer inline so the integrity of the source buffer is compromised!
		if (!m_minfo.keepResponse || m_minfo.parseJson)
			clearResponse();

		return 0;
	}

	void SpRest::setup(StringMap& params) {
		const auto& appOptions = Application::instance()->appOptions();
		setup(appOptions.username, appOptions.password, params, nullptr);
	}

	void SpRest::setup(const std::string& username, const std::string& password, StringMap& params, const char* serverUrl) {
		if (serverUrl)
			m_serverUrl = serverUrl;

		m_username = username;
		m_password = password;
		m_params = params;

		//Warning("SpRest", "StartDate = %s, EndDate = %s", params["StartDate"], params["EndDate"]);

		m_postString = toPostString(params);
	}

	void SpRest::setup(const DataCacheInfo& dci, StringMap& params, const char* serverUrl) {
		setup(dci.rt.appOptions.username, dci.rt.appOptions.password, params, serverUrl);
	}
		
	int SpRest::getDataSync(const DataCacheInfo& dci, StringMap& params, const char* serverUrl /* = nullptr */) {
		setup(dci, params, serverUrl);
		return getData(m_numRetries);
	}

	void SpRest::run() {
		int errCode = getData(m_numRetries);
		m_promise->set_value(errCode);
	}

	std::future<int> SpRest::getDataAsync(const DataCacheInfo& dci, StringMap& params, const char* serverUrl /* = nullptr */) {
		setup(dci, params, serverUrl);
		//return std::async(std::launch::async, &SpRest::getData, this, dci, params, serverUrl);

		//std::promise<int> promise;
		//std::future<int> future = promise.get_future();
  //      SpRest* self = this;

		//m_thread = new std::thread([=](std::promise<int> promise) {
		//	int errCode = self->getData();
		//	promise.set_value(errCode);
		//}, std::move(promise));

		//return future;

		delete m_promise;
		delete m_thread;

		m_promise = new std::promise<int>;
		m_thread = new Poco::Thread();
		m_thread->start(*this);

		return m_promise->get_future();
	}

	std::string SpRest::buildSymbolsStr(const StringVec& figis) {
		std::string symbolsStr = "dict(Symbology='CFIGI',IDs=(";

		for (auto& figi : figis) {
			symbolsStr += PESA_QSTR(figi) + ",";
		}

		symbolsStr += "))";
		return symbolsStr;
	}

	SpRest::Result& SpRest::getData(const StringVec& dataNames, IntDate startDate, IntDate endDate, std::string figi) {
		StringVec figis;		figis.push_back(figi);
		return getData(dataNames, startDate, endDate, figis);
	}

	SpRest::Result& SpRest::getData(const StringVec& dataNames, IntDate startDate, IntDate endDate, const StringVec& figis) {
		return getData(dataNames, startDate, endDate, figis, nullptr);
	}

	SpRest::Result& SpRest::getData(const StringVec& dataNames, IntDate startDate, IntDate endDate, const StringVec& figis, const StringMap* args_) {
		StringMap args;
		if (args_)
			args = *args_;

		//args["ultrajson"]	= "'True'";
		//args["yml"]			= m_yml;
		//args["Keyword"]		= m_keyword;
		//args["StartDate"]	= PESA_QSTR(Util::intDateToStr(startDate, "-"));
		//args["EndDate"]		= PESA_QSTR(Util::intDateToStr(endDate, "-"));
		//args["Symbols"]		= buildSymbolsStr(figis);

		//m_requiredFields.push_back("SYMBOL");
		//m_requiredFieldTypes.push_back(DataType::kString);

		//for (auto& dataName : dataNames) {
		//	auto parts = Util::split(dataName, ".");
		//	if (parts.size() == 1) {
		//		m_requiredFields.push_back(dataName);
		//		m_requiredFieldTypes.push_back(DataType::kUnknown);
		//	}
		//	else {
		//		auto type = DataType::parse(parts[0]);
		//		m_requiredFields.push_back(parts[1]);
		//		m_requiredFieldTypes.push_back(type);
		//	}
		//}

		setup(args);

		int errCode = getData(m_numRetries);
		return m_result;
	}

	//////////////////////////////////////////////////////////////////////////
	/// SpRestBatch
	//////////////////////////////////////////////////////////////////////////
	SpRestBatch::SpRestBatch(const ConfigSection& config, const StringVec* requiredFields, SpRest::DataTypeVec* requiredFieldsTypes, SpRest::MoreInfo minfo)
		: m_config(config) 
		, m_minfo(std::move(minfo)) {
		if (requiredFields)
			m_requiredFields = *requiredFields;
		if (requiredFieldsTypes)
			m_requiredFieldTypes = *requiredFieldsTypes;
	}

	SpRestBatch::~SpRestBatch() {
		Trace("SpRestBatch", "Clearing Batch: %z", m_calls.size());
		m_calls.clear();
	}

	SpRestPtr SpRestBatch::getDataAsync(const DataCacheInfo& dci, StringMap& params, const char* serverUrl /* = nullptr */, SpRest::MoreInfo* minfo /* = nullptr */) {
		/// Setup the call here ...
		RestCallPtr rcall = std::make_shared<RestCall>();
		rcall->call = std::make_shared<SpRest>(m_config, m_requiredFields);
		rcall->call->minfo() = minfo ? *minfo : m_minfo;
		rcall->call->requiredFieldTypes() = m_requiredFieldTypes;

		/// Then make the actual call ...
		rcall->future = rcall->call->getDataAsync(dci, params, serverUrl);
		m_calls.push_back(rcall);

		return rcall->call;
	}

	int SpRestBatch::wait() {
		size_t numFinished = 0;

		while (numFinished != m_calls.size()) {
			for (auto rcall : m_calls) {
				if (rcall->future.valid()) {
					/// Increase the priority of the thread, since this is the one we're waiting for right now ...
					//rcall->call->thread()->setOSPriority(Poco::Thread::getMaxOSPriority());

                    auto futureStatus = rcall->future.wait_for(std::chrono::seconds(SpRest::s_timeout * 3));
					ASSERT(futureStatus != std::future_status::timeout, "REST call timed out. Post string: " << rcall->call->postString());
                    
					rcall->errCode = rcall->future.get();

					numFinished++;

					if (rcall->errCode != 0)
						return rcall->errCode;
				}
				else
					numFinished++;
			}
		}

		return 0;
	}

	SpRest::ResultVec SpRestBatch::results() {
		SpRest::ResultVec results;

		for (auto rcall : m_calls) {
			SpRest::Result result = rcall->call->result();
			results.push_back(result);
		}

		return results;
	}
} /// namespace pesa
