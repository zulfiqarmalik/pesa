/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// SpRest.h
///
/// Created on: 20 Aug 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Helper/JSON/gason.hpp"
#include "Framework/Framework.h"

#include "Poco/Thread.h"
#include <future>

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	typedef std::future<int> IntFuture;

	class SpRest : public Poco::Runnable {
	public:
		static const int		s_timeout = 1200; /// 20 minutes (temporary)

		typedef std::unordered_map<std::string, std::string>	StringMap;
		typedef std::unordered_map<std::string, DataPtr>		DataMap;
		typedef std::function<void(SpRest*)>					Callback;
		typedef std::vector<DataType::Type>						DataTypeVec;
		typedef std::unordered_map<IntDate, IntDay>				DateMap;

		struct FigiData {
			std::string			figi;				/// The figi 
			size_t				numValues = 0;		/// Number of valid values against this figi
			size_t				numCopied = 0;		/// The number of values we've copied so far
			SpRest::DataMap		data;				/// The actual data for the figi
		};
		typedef std::vector<FigiData> FigiDataVec;
		typedef std::unordered_map<std::string, FigiData> FigiDataMap;

		struct HierarchicalData {
			StringVec			figis;				/// The figis for the entire set (all the unique ones that is)
			FigiDataMap			dmap;				/// The figi data map
		};

		struct Result {
			StringMap			columnDefs;			/// What are the column definitions
			DataMap				data;				/// The actual data
			HierarchicalData	hdata;				/// The hierarchical data
			DateMap				dateMap;			/// Mapping of date to index. This is used when there are multiple rows of data returned
		};

		typedef std::vector<Result> ResultVec;

		class IParser;
		struct MoreInfo {
			bool				specifiedFieldsOnly = true;
			bool				parseJson			= true;
			bool				keepResponse		= false;
			bool				useHttpGet			= false;
			IParser*			jsonParser			= nullptr;
			std::string			serverUrl; 
		};

		//////////////////////////////////////////////////////////////////////////
		/// IJsonParser: JSON parser. Provide custom implementation
		//////////////////////////////////////////////////////////////////////////
		class IParser {
		public:
			virtual int			parseResults(SpRest* rcall, Result& result) = 0;
		};

		friend class IParser;

		//////////////////////////////////////////////////////////////////////////
		static void				constructHierarchicalData(Result& result, HierarchicalData& hdata, StringVec& fields);

		int						parseResult();

		template <typename T>
		static bool				jsonObjectGeneric(gason::JsonValue value, std::vector<T>& result, StringVec* keys, std::function<T(gason::JsonValue)> callback) {
			for (gason::JsonIterator iter = gason::begin(value); iter != gason::end(value); iter++) {
				T item = callback(iter->value);
				if (keys)
					keys->push_back(iter->key);
				result.push_back(item);
			}

			return true;
		}

		static float			jsonFloatValue(gason::JsonValue value);
		static double			jsonDoubleValue(gason::JsonValue value);
		static int				jsonIntValue(gason::JsonValue value);
		static std::string		jsonStringValue(gason::JsonValue value);

		static bool				jsonArray(SpRest* self, const std::string& field, gason::JsonValue value, StringVec& result, StringVec* keys = nullptr);
		static bool				jsonArray(SpRest* self, const std::string& field, gason::JsonValue value, FloatVec& result, StringVec* keys = nullptr);
		static bool				jsonArray(SpRest* self, const std::string& field, gason::JsonValue value, DoubleVec& result, StringVec* keys = nullptr);
		static bool				jsonArray(SpRest* self, const std::string& field, gason::JsonValue value, IntVec& result, StringVec* keys = nullptr);
		static bool				jsonArray(SpRest* self, const std::string& field, gason::JsonValue value, UIntVec& result, StringVec* keys = nullptr);
		static bool				jsonArray(SpRest* self, const std::string& field, gason::JsonValue value, Int64Vec& result, StringVec* keys = nullptr);

		static DataPtr			jsonArray(SpRest* self, const std::string& field, gason::JsonValue value, DataType::Type type, StringVec* keys = nullptr);

		static std::string		toPostString(const StringMap& params);
		static std::string		toJson(const StringMap& params);

	private:
		static std::string		s_dbName;

		static size_t			curlCallback(char* ptr, size_t size, size_t nmemb, void* userdata);
		size_t					callback(char* ptr, size_t size, size_t nmemb);

		static size_t			curlHeaderCallback(char* ptr, size_t size, size_t nmemb, void* userdata);
		size_t					headerCallback(char* contents, size_t size, size_t nmemb);

		std::string 			m_serverUrl;					/// The server URL (endpoint) that we're going to contact

		std::string				m_postString;					/// We need to save this for CURL
		StringVec				m_requiredFields;				/// Optional - the required data
		DataTypeVec				m_requiredFieldTypes;			/// Optional - the expected types of the required fields
		SpRest::Result			m_result;						/// The final result after parsing

		StringMap				m_params;						/// The params that were passed!
		bool					m_dbCache = false;				/// Whether to cache the results of the query to the database

		static const int		s_defResponseBufferLen;
		char*					m_responseText = nullptr;		/// The response buffer
		int						m_responseBufferLen = 0;		/// The length of the response buffer
		int						m_responseTextLen = 0;			/// The length of the response text
		int						m_expectedResponseLen = 0;		/// What is the expected response size. This is read from the response header

		gason::JsonParseStatus	m_jsonStatus = gason::JSON_PARSE_OK; /// The status of the JSON parsing
		gason::JsonAllocator*	m_jsonAllocator = nullptr;		/// The JSON allocator for GASON
		gason::JsonValue		m_jsonResult;					/// The JSON result 

		std::string				m_username;						/// The username (this is used and then immediately emptied!)
		std::string				m_password;						/// The password (this is used and then immediately emptied!)
		std::string 			m_userAuth; 					/// The user auth string

		int						m_numRetries = 3;				/// How many times to retry
		MoreInfo				m_minfo;						/// More info

		Poco::Thread*			m_thread = nullptr;				/// The thread that we use for async (used in getDataAsync only)
		std::promise<int>*		m_promise = nullptr;			/// The async promise

		int						m_curlStatus = -1;				/// Last CURL status
		int						m_httpStatus = 200;				/// Last HTTP status

		StringVec				m_indexFields;					/// What fields to index on for the purpose of making an intermediate cache in the mongo database

		std::string 			m_proxyServer;					/// The proxy server

		void					initConfig(const ConfigSection& config, IntDate date = 0);
		void					clearResponse();

		std::string				loadDataDB();
		int						saveDataDB();
		void					createIndexes(const std::string& collName);

		std::string				makeId(const StringMap& params);

		int						getData(int numRetries);
		void					setup(const DataCacheInfo& dci, StringMap& params, const char* serverUrl);
		void					setup(const std::string& username, const std::string& password, StringMap& params, const char* serverUrl);
		void					setup(StringMap& params);
		int						makeRequest(int numRetries = 3);

		std::string				buildSymbolsStr(const StringVec& figis);

	public:
								SpRest(const ConfigSection& config, IntDate date = 0);
								SpRest(const ConfigSection& config, const StringVec& requiredFields, IntDate date = 0);
								SpRest(const SpRest&) = delete;
		virtual 				~SpRest();

		virtual void			run();

		int						getDataSync(const DataCacheInfo& dci, StringMap& params, const char* serverUrl = nullptr);
		IntFuture				getDataAsync(const DataCacheInfo& dci, StringMap& params, const char* serverUrl = nullptr);

		SpRest::Result&			getData(const StringVec& dataNames, IntDate startDate, IntDate endDate, std::string figi);
		SpRest::Result&			getData(const StringVec& dataNames, IntDate startDate, IntDate endDate, const StringVec& figis);
		SpRest::Result&			getData(const StringVec& dataNames, IntDate startDate, IntDate endDate, const StringVec& figis, const StringMap* args);

		//////////////////////////////////////////////////////////////////////////
		/// Inline functions
		//////////////////////////////////////////////////////////////////////////
		inline Poco::Thread*	thread() { return m_thread; }
		inline SpRest::Result	result() { return m_result; }
		inline StringVec&		requiredFields() { return m_requiredFields; }
		inline DataTypeVec&		requiredFieldTypes() { return m_requiredFieldTypes; }
		inline gason::JsonParseStatus parseStatus() const { return m_jsonStatus; }
		inline const std::string& postString() const { return m_postString; }
		inline int				httpStatus() const { return m_httpStatus; }
		inline int				curlStatus() const { return m_curlStatus; }
		inline MoreInfo&		minfo()	{ return m_minfo; }
		inline bool&			dbCache() { return m_dbCache; }
		inline bool				dbCache() const { return m_dbCache; }
		inline size_t			responseTextLen() const { return m_responseTextLen; }
		inline const char*		responseText() const { return m_responseText; }
		inline gason::JsonValue jsonResult() { return m_jsonResult; }
		inline std::string& 	proxyServer() { return m_proxyServer; }
	};

	typedef std::shared_ptr<SpRest>	SpRestPtr;
	typedef std::unique_ptr<SpRest>	SpRestUPtr;
	typedef std::vector<SpRestPtr>	SpRestPtrVec;

	//////////////////////////////////////////////////////////////////////////
	/// SpRestBatch - A batch of SpRest calls
	//////////////////////////////////////////////////////////////////////////
	class SpRestBatch {
	public:
		struct RestCall {
			SpRestPtr			call;
			IntFuture			future;
			int					errCode = -1;
		};

		typedef std::shared_ptr<RestCall> RestCallPtr;
		typedef std::vector<RestCallPtr> RestCallPtrVec;

	private:
		const ConfigSection&	m_config;				/// The config
		RestCallPtrVec			m_calls;				/// The REST calls that we have made so far
		StringVec				m_requiredFields;		/// The fields that we require
		SpRest::DataTypeVec		m_requiredFieldTypes;	/// The required data types
		SpRest::MoreInfo		m_minfo;				/// More info

	public:
								SpRestBatch(const ConfigSection& config, const StringVec* requiredFields, SpRest::DataTypeVec* requiredDataTypes, SpRest::MoreInfo minfo);
								~SpRestBatch();

		SpRestPtr				getDataAsync(const DataCacheInfo& dci, StringMap& params, const char* serverUrl = nullptr, SpRest::MoreInfo* minfo = nullptr);
		int						wait();
		SpRest::ResultVec		results();

		inline RestCallPtrVec&	calls() { return m_calls; }
		inline size_t			numCalls() const { return m_calls.size(); }
	};

	typedef std::shared_ptr<SpRestBatch> SpRestBatchPtr;

#define PESA_QSTR(arg)				"'" + arg + "'"
} /// namespace pesa

