/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// SpSecMaster.cpp
///
/// Created on: 20 Aug 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "SpRest.h"
#include "Poco/Timestamp.h"
//
//namespace pesa {
//	////////////////////////////////////////////////////////////////////////////
//	/// Security Master
//	////////////////////////////////////////////////////////////////////////////
//	class SpSecMaster : public IDataLoader {
//	private:
//		struct CacheInfo {
//			std::future<int>	future;
//			SpRestPtr			restData;
//			std::string			exchange;
//			size_t				diStart;
//			size_t				diEnd;
//		};
//
//		typedef std::unordered_map<IntDate, UIntVec> DailySecMap;
//		typedef std::map<std::string, Security::Index> SecurityLookup;
//		typedef std::shared_ptr<CacheInfo> CacheInfoPtr;
//		typedef std::vector<CacheInfoPtr> CacheInfoPtrVec;
//
//		StringVec				m_countries;			/// The country that the exchanges belong to
//		StringVec				m_exchanges;			/// The list of exchanges that we're supposed to handle
//		StringMap				m_filters;				/// What are the filters that we need to apply
//		StringVec				m_filterIssueDesc;		/// Issue desc filters
//		bool					m_applyFilters = false;	/// Should we apply filters
//		bool					m_mergedExchangeCode = false; /// Use the composite exchange code (merge country and exchange code)
//		std::string				m_tickerField = "TDEQ";	/// The name of the ticker field
//		SecurityLookup			m_securityLookup;		/// Which securities do we already have? 
//		DailySecMap				m_dailySecurities;		/// Daily securities ...
//
//		IntDay					m_preCacheIter = -1;	/// The precache iterator
//		size_t					m_numRequested = 0;		/// The number of requests that we've sent
//		size_t					m_numResponses = 0;		/// The number of responses that we've received
//
//		bool					m_getPrimaryExch = true;/// Get the primary exchange always for a security
//
//		SecurityVec				m_allSecurities;		/// All the securities within the system
//
//		CacheInfoPtrVec			m_cinfo;				/// Caching pointer info ...
//
//		static const size_t		s_maxRequests;
//
//		bool					filterNewSecurities(const SecurityVec& newSecurities, SecurityVec& filtered);
//		void					cacheDailySecurities(IntDate date, const UIntVec& dailySecurities, const std::string& exchange);
//		void 					buildSecurities(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities);
//		void 					buildDetails(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities);
//		void					preCache(CacheInfoPtrVec& cinfo, IDataRegistry& dr, const DataCacheInfo& dci, bool singleDate = false);
//		void					preCache(CacheInfoPtrVec& cinfo, IDataRegistry& dr, const DataCacheInfo& dci, const std::string& serverUrl, 
//								const std::string& keyword, const std::string& yml, StringVec& requiredFields, bool singleDate = false, 
//								SpRest::StringMap* addArgs = nullptr);
//
//	public:
//								SpSecMaster(const ConfigSection& config);
//		virtual 				~SpSecMaster();
//
//		////////////////////////////////////////////////////////////////////////////
//		/// IDataLoader overrides
//		////////////////////////////////////////////////////////////////////////////
//		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
//		virtual void 			buildSecurityMaster(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
//		virtual void 			buildDailySecurities(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
//		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
//
//		virtual void 			preDataBuild(IDataRegistry& dr, const DataCacheInfo& dci);
//		virtual void 			postDataBuild(IDataRegistry& dr, const DataCacheInfo& dci);
//	};
//
//	const size_t				SpSecMaster::s_maxRequests = 16;
//
//	SpSecMaster::SpSecMaster(const ConfigSection& config) 
//		: IDataLoader(config, "SpSecMaster") {
//		std::string exchangesStr = config.getRequired("exchanges");
//		exchangesStr = config.config()->defs().resolve(exchangesStr, (Poco::DateTime*)nullptr);
//		m_getPrimaryExch = config.getBool("getPrimaryExch", m_getPrimaryExch);
//
//		FrameworkUtil::parseExchanges(exchangesStr, m_exchanges, m_countries);
//
//		m_mergedExchangeCode = config.getBool("mergedExchangeCode", m_mergedExchangeCode);
//		m_tickerField = config.getString("tickerField", m_tickerField);
//
//		auto filter = config.getString("filter", "");
//		if (!filter.empty()) {
//			auto filters = Util::split(filter, "|");
//			if (filters.size()) {
//				m_applyFilters = true;
//				for (auto& f : filters)
//					m_filters[f] = f;
//			}
//		}
//
//		auto filterIssueDesc = config.getString("filterIssueDesc", "");
//		if (!filterIssueDesc.empty()) {
//			auto filters = Util::split(filterIssueDesc, "|");
//			if (filters.size()) {
//				m_applyFilters = true;
//				for (auto& f : filters)
//					m_filterIssueDesc.push_back(f);
//			}
//		}
//	}
//
//	SpSecMaster::~SpSecMaster() {
//		CTrace("Deleting SpSecMaster");
//	}
//
//	void SpSecMaster::initDatasets(IDataRegistry& dr, Datasets& datasets) {
//		std::string name = config().getRequired("name");
//
//		datasets.add(Dataset(name, {
//			DataValue("Master", DataType::kSecurityData, Security::s_version, THIS_DATA_FUNC(SpSecMaster::buildSecurityMaster),
//				DataFrequency::kUniversal, DataFlags::kAlwaysLoad | DataFlags::kUpdatesInOneGo | DataFlags::kSecurityMaster),
//		}));
//	}
//
//	void SpSecMaster::postDataBuild(IDataRegistry& dr, const DataCacheInfo& dci) {
//		/// We do not need this data anymore ...
//		m_securityLookup.clear();
//		m_dailySecurities.clear();
//		m_allSecurities.clear();
//	}
//
//	void SpSecMaster::preDataBuild(IDataRegistry& dr, const DataCacheInfo& dci) {
//		std::string name = config().getRequired("name");
//
//		const SecurityData* secData = dr.get<SecurityData>(nullptr, name + ".Master", true);
//
//		if (secData) {
//			size_t numSecurities = secData->numSecurities();
//			m_allSecurities.resize(numSecurities);
//			CInfo("Existing number of securities: %z", numSecurities);
//
//			for (size_t i = 0; i < numSecurities; i++) {
//				const Security& sec = secData->security(i);
//				m_securityLookup[sec.uuidStr()] = sec.index;
//				m_allSecurities[i] = sec;
//
//				//auto str = sec.debugString();
//				//CDebug("\n%z - %s", i, str);
//			}
//		}
//	}
//
//	void SpSecMaster::buildDailySecurities(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
//		if (!m_dailySecurities.size()) {
//			std::vector<Security> newSecurities;
//			buildSecurities(dr, dci, newSecurities);
//		}
//
//		IntDate date = dci.rt.dates[dci.di];
//		auto iter = m_dailySecurities.find(date);
//
//		if (iter == m_dailySecurities.end()) {
//			frame.noDataUpdate = true;
//			return;
//		}
//
//		CInfo("Number of securities [%u]: %z", date, iter->second.size());
//
//		frame.data = std::make_shared<UIntMatrixData>(dci.universe, iter->second);
//		frame.dataOffset = 0;
//		frame.dataSize = frame.data->dataSize();
//	}
//
//	void SpSecMaster::cacheDailySecurities(IntDate date, const UIntVec& dailySecurities, const std::string& exchange) {
//		return;
//		ASSERT(dailySecurities.size(), "No securities for date: " << date);
//
//		CDebug("[%u - %s]: Adding %z securities", date, exchange, dailySecurities.size());
//
//		auto iter = m_dailySecurities.find(date);
//		if (iter != m_dailySecurities.end()) {
//			/// append it to the existing securities ...
//			iter->second.insert(iter->second.end(), dailySecurities.begin(), dailySecurities.end());
//			return;
//		}
//
//		m_dailySecurities[date] = dailySecurities;
//	}
//
//	struct SecCmp {
//		bool operator() (const Security& lhs, const Security& rhs) {
//			return lhs.uuidStr() < rhs.uuidStr();
//		}
//	};
//
//	void SpSecMaster::buildSecurityMaster(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
//		SecurityVec newSecurities;
//		buildSecurities(dr, dci, newSecurities);
//
//		CDebug("Number of new securities found: %z", newSecurities.size());
//		//std::sort(newSecurities.begin(), newSecurities.end(), SecCmp());
//		//CDebug("... Sorting DONE!");
//
//		size_t numExisting = m_allSecurities.size();
//		size_t numNew = newSecurities.size();
//		size_t numFiltered = numNew;
//		SecurityVec filtered;
//
//		if (!m_applyFilters || (!m_filters.size() && !m_filterIssueDesc.size())) {
//			m_allSecurities.insert(m_allSecurities.end(), newSecurities.begin(), newSecurities.end());
//			buildDetails(dr, dci, m_allSecurities);
//		}
//		else {
//			/// Get the details of the new securities ...
//			buildDetails(dr, dci, newSecurities);
//
//			/// Then update the details of the existing securities as well
//			CDebug("Updating the details of existing securities. Count: %z", m_allSecurities.size());
//			buildDetails(dr, dci, m_allSecurities);
//
//			filterNewSecurities(newSecurities, filtered);
//			numFiltered = filtered.size();
//
//			/// Assign the correct indexes to the securities
//			for (auto& sec : filtered) {
//				sec.index = (Security::Index)m_allSecurities.size();
//				m_allSecurities.push_back(sec);
//			}
//		}
//
//		//if (numExisting) {
//		//	/// Now we keep the latest information for all securities!
//		//	CInfo("Keeping the latest security information as part of the Security Master!");
//		//	buildDetails(dr, dci, m_allSecurities);
//		//}
//
//		///// Add the new securities to all securities ...
//		//if (newSecurities.size()) {
//		//	if (filterNewSecurities(newSecurities, filtered)) {
//		//		numFiltered = filtered.size();
//		//		m_allSecurities.insert(m_allSecurities.end(), filtered.begin(), filtered.end());
//		//	}
//		//	else {
//		//	}
//		//}
//
//        try {
//			CDebug("Number of new securities: %z - After filteration: %z [Existing: %z, DataSize: %z]", numNew, numFiltered, numExisting, m_allSecurities.size() * sizeof(Security));
//
//			/// otherwise we create a new data for these
//			frame.data = std::make_shared<SecurityData>(dci.universe, m_allSecurities, dci.def);
//			frame.dataOffset = 0;
//			frame.dataSize = frame.data->dataSize();
//			frame.noDataUpdate = false;
//        }
//        catch (const std::exception& e) {
//			Util::reportException(e);
//			Util::exit(1);
//        }
//        //		CDebug("Number of new securities: %z - After filteration: %z [Existing: %z, DataSize: %z]", numNew, numFiltered, numExisting, m_allSecurities.size() * sizeof(Security));
//
//		/// otherwise we create a new data for these 
//		//frame.data = std::make_shared<SecurityData>(dci.universe, m_allSecurities, dci.def);
//		//frame.dataOffset = 0;
//		//frame.dataSize = frame.data->dataSize();
//		//frame.noDataUpdate = false;
//	}
//
//	void SpSecMaster::preCache(CacheInfoPtrVec& cinfo, IDataRegistry& dr, const DataCacheInfo& dci, const std::string& serverUrl,
//		const std::string& keyword, const std::string& yml, StringVec& requiredFields, bool singleDate /* = false */, 
//		SpRest::StringMap* addArgs /* = nullptr */) {
//		const IntDay diDelta = !singleDate ? 7 : 1;
//
//		if (m_preCacheIter < 0)
//			m_preCacheIter = std::max(dci.diStart - 1, 0);
//
//		/// We got nothing left to do ...
//		if (m_preCacheIter > dci.diEnd)
//			return;
//
//		ASSERT(m_numResponses <= m_numRequested, "More responses than requests: " << m_numResponses << " > " << m_numRequested);
//		size_t pendingRequests = m_numRequested - m_numResponses;
//
//		if (pendingRequests >= s_maxRequests)
//			return;
//
//		size_t slotsAvailable = s_maxRequests - pendingRequests;
//
//		while (slotsAvailable > 0 && m_preCacheIter <= dci.diEnd) {
//			IntDate diIterStart = m_preCacheIter;
//			IntDate istartDate = dci.rt.dates[diIterStart];
//			std::string startDate = Util::intDateToStr(istartDate, "-");
//
//			m_preCacheIter += diDelta;
//
//			if (m_preCacheIter > dci.diEnd + 1)
//				m_preCacheIter = dci.diEnd + 1;
//
//			IntDate iendDate = dci.rt.dates[m_preCacheIter - 1]; /// END DATE is inclusive
//			std::string endDate = Util::intDateToStr(iendDate, "-");
//
//			SpRest::StringMap args;
//
//			if (addArgs)
//				args = *addArgs;
//
//			if (!singleDate) {
//				args["StartDate"] = PESA_QSTR(startDate);
//				args["EndDate"] = PESA_QSTR(endDate);
//			}
//			else {
//				args["Date"] = PESA_QSTR(startDate);
//			}
//
//			CDebug("Precaching SecurityMaster: [%u - %u]", istartDate, iendDate);
//
//			for (size_t i = 0; i < m_exchanges.size(); i++) {
//				const auto& exchange = m_exchanges[i];
//				const auto& country = m_countries[i];
//
//				CTrace("Building exchange: %s", exchange);
//
//				if (!m_mergedExchangeCode) {
//					args["EXCH"] = PESA_QSTR(exchange);
//				}
//				else {
//					args["EXCH"] = PESA_QSTR(country + exchange);
//				}
//
//				CacheInfoPtr ci = std::make_shared<CacheInfo>();
//
//				ci->restData = SpRestPtr(new SpRest(config(), requiredFields));
//				ci->diStart = diIterStart;
//				ci->diEnd = m_preCacheIter;
//				ci->exchange = !m_mergedExchangeCode ? exchange : (country + exchange);
//				ci->future = ci->restData->getDataAsync(dci, args);
//
//				cinfo.push_back(ci);
//				m_numRequested++;
//			}
//
//			slotsAvailable--;
//		}
//	}
//
//	bool SpSecMaster::filterNewSecurities(const SecurityVec& newSecurities, SecurityVec& filtered) {
//		if (!m_applyFilters || (!m_filters.size() && !m_filterIssueDesc.size()))
//			return false;
//
//		for (const auto& security : newSecurities) {
//			auto issueClass = security.bbSecurityTypeStr(); 
//			auto iter = m_filters.find(issueClass);
//
//			if (iter == m_filters.end()) {
//				CTrace("Filtering out ticker: %s [class = %s]", security.tickerStr(), issueClass);
//			}
//			else { 
//				bool useSecurity = true;
//				auto issueDesc = security.issueDescStr();
//				
//				for (const auto& fdFilter : m_filterIssueDesc) {
//					if (issueDesc.find(fdFilter) != std::string::npos) {
//						CTrace("Filtering out issue desc: %s [desc = %s, filter = %s]", security.tickerStr(), issueDesc, fdFilter);
//						useSecurity = false;
//						break;
//					}
//				}
//
//				if (useSecurity)
//					filtered.push_back(security);
//			}
//		}
//
//		return true;
//	}
//
//	void SpSecMaster::buildDetails(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities) {
//		CDebug("Getting detailed information about the securities!");
//		size_t numSecurities = newSecurities.size();
//
//		/// No securities at all ... nothing to do over here!
//		if (!numSecurities)
//			return;
//
//		//const auto& c	= config();
//		//auto serverUrl	= c.getRequired("serverUrl");
//		//auto yml		= c.getYml("secYml");
//		//auto keyword	= c.getRequired("secKeyword");
//
//		std::unordered_map<std::string, size_t> uuidMap;
//		StringVec uuids(numSecurities);
//		size_t secIndex = 0;
//		StringVec requiredFields = {
//			"SYMBOL", "CUSIP", "SEDOL", "NAME", "ISSUE_DESC", "ISSUE_TYPE", "ISSUE_CLASS", "CURRENCY", "PRIMARY_EXCHANGE", 
//			"ISO_COUNTRY_2", "ISO_COUNTRY_3", "PARENT_CO_ID", "SECURITY_TYP", "SECURITY_TYP2", "BTKR", "ISIN"
//		};
//
//		if (!m_tickerField.empty()) 
//			requiredFields.push_back(m_tickerField);
//
//		SpRest::MoreInfo minfo;
//		std::string startDate = Util::intDateToStr(dci.rt.dates[0], "-");
//		std::string endDate = Util::intDateToStr(dci.rt.dates[dci.rt.dates.size() - 2], "-");
//		ConfigSection dconfig(config());
//
//		dconfig.set("yml", config().get("secYml"));
//		dconfig.set("keyword", config().getKeyword("secKeyword"));
//
//		SpRestBatch restData(dconfig, &requiredFields, nullptr, minfo);
//
//		while (secIndex < numSecurities) {
//			SpRest::StringMap args;
//			size_t secEndIndex = std::min(secIndex + 10000, numSecurities);
//			std::string symbolsStr = "dict(Symbology='CFIGI',IDs=(";
//
//			for (size_t i = secIndex; i < secEndIndex; i++) {
//				const auto& sec = newSecurities[i];
//				std::string uuid = sec.uuidStr();
//				uuids[i] = uuid;
//				uuidMap[uuid] = i;
//				symbolsStr += PESA_QSTR(uuid) + ",";
//			}
//			symbolsStr += "))";
//
//			secIndex = secEndIndex;
//
//			args["Symbols"] = symbolsStr;
//			args["Date"] = PESA_QSTR(endDate);
//			//args["StartDate"] = PESA_QSTR(startDate);
//			//args["EndDate"] = PESA_QSTR(endDate);
//
//			restData.getDataAsync(dci, args);
//		}
//
//		/// Now we wait for the results!
//		restData.wait();
//
//		auto results = restData.results();
//		ASSERT(results.size(), "Unable to get detailed information about the securities!");
//
//		for (auto& result : results) {
//			auto figiData 					= result.data["SYMBOL"];
//			auto cusipData 					= result.data["CUSIP"];
//			auto sedolData 					= result.data["SEDOL"];
//			auto nameData 					= result.data["NAME"];
//			auto issueDescData 				= result.data["ISSUE_DESC"];
//			auto issueTypeData 				= result.data["ISSUE_TYPE"];
//			auto issueClassData 			= result.data["ISSUE_CLASS"];
//			auto ccyData					= result.data["CURRENCY"];
//			auto primaryExchangeData		= result.data["PRIMARY_EXCHANGE"];
//			auto countryCode3Data			= result.data["ISO_COUNTRY_3"];
//			auto countryCode2Data			= result.data["ISO_COUNTRY_2"];
//			auto parentIdData				= result.data["PARENT_CO_ID"];
//			auto bbSecurityTypeData			= result.data["SECURITY_TYP"];
//			auto bbSecurityDescData			= result.data["SECURITY_TYP2"];
//			auto bbTickerData				= result.data["BTKR"];
//			auto isinData					= result.data["ISIN"];
//			DataPtr tickerData				= nullptr;
//
//			if (!m_tickerField.empty())
//				tickerData					= result.data[m_tickerField];
//
//			/// No data was returned ...
//			if (!figiData || !figiData->cols()) 
//				continue;
//
//			ASSERT(figiData->cols()			== cusipData->cols(), "Mismatched number of entries between SYMBOL and CUSIP. SYMBOL: " << figiData->cols() << " - CUSIP: " << cusipData->cols());
//			ASSERT(figiData->cols()			== sedolData->cols(), "Mismatched number of entries between SYMBOL and SEDOL. SYMBOL: " << figiData->cols() << " - SEDOL: " << sedolData->cols());
//			ASSERT(figiData->cols()			== nameData->cols(), "Mismatched number of entries between SYMBOL and NAME. SYMBOL: " << figiData->cols() << " - NAME: " << nameData->cols());
//
//			const std::string* figis		= (dynamic_cast<StringData*>(figiData.get()))->sptr();
//			const std::string* cusips		= (dynamic_cast<StringData*>(cusipData.get()))->sptr();
//			const std::string* sedols		= (dynamic_cast<StringData*>(sedolData.get()))->sptr();
//			const std::string* names		= (dynamic_cast<StringData*>(nameData.get()))->sptr();
//			const std::string* issueDesc	= (dynamic_cast<StringData*>(issueDescData.get()))->sptr();
//			const std::string* issueType	= (dynamic_cast<StringData*>(issueTypeData.get()))->sptr();
//			const std::string* issueClass	= (dynamic_cast<StringData*>(issueClassData.get()))->sptr();
//			const std::string* ccy			= (dynamic_cast<StringData*>(ccyData.get()))->sptr();
//			const std::string* pexch		= (dynamic_cast<StringData*>(primaryExchangeData.get()))->sptr();
//			const std::string* ccode3		= (dynamic_cast<StringData*>(countryCode3Data.get()))->sptr();
//			const std::string* ccode2		= (dynamic_cast<StringData*>(countryCode2Data.get()))->sptr();
//			const std::string* parentId		= (dynamic_cast<StringData*>(parentIdData.get()))->sptr();
//			const std::string* bbSecTypes	= (dynamic_cast<StringData*>(bbSecurityTypeData.get()))->sptr();
//			const std::string* bbSecDescs	= (dynamic_cast<StringData*>(bbSecurityDescData.get()))->sptr();
//			const std::string* bbTickers	= (dynamic_cast<StringData*>(bbTickerData.get()))->sptr();
//			const std::string* isins		= (dynamic_cast<StringData*>(isinData.get()))->sptr();
//			const std::string* tickers		= tickerData ? (dynamic_cast<StringData*>(tickerData.get()))->sptr() : nullptr;
//			size_t length					= figiData->cols();
//
//			for (size_t i = 0; i < length; i++) {
//				const std::string& figi		= figis[i];
//				const std::string& cusip	= cusips[i];
//				const std::string& sedol	= sedols[i];
//				std::string name			= names[i];
//				auto iter					= uuidMap.find(figi);
//
//				if (iter == uuidMap.end())
//					continue;
//
//				if (name.empty()) {
//					CTrace("No name for figi: %s", figi);
//					name = figi;
//				}
//
//				//ASSERT(!name.empty(), "Invalid name received for FIGI: " << figi);
//
//				CTrace("Mapped FIGI: %s => Name = %s, Cusip = %s, Sedol = %s", figi, name, cusip, sedol);
//
//				size_t index					= iter->second;
//				newSecurities[index].setName(name);
//				newSecurities[index].setCusip(cusip);
//				newSecurities[index].setSedol(sedol);
//				newSecurities[index].setIssueDesc(issueDesc[i]);
//				newSecurities[index].setIssueType(issueType[i]);
//				newSecurities[index].setIssueClass(issueClass[i]);
//				newSecurities[index].setCurrency(ccy[i]);
//				newSecurities[index].setCountryCode3(ccode3[i]);
//				newSecurities[index].setCountryCode2(ccode2[i]);
//				newSecurities[index].setParentUuid(parentId[i]);
//				newSecurities[index].setBBSecurityType(bbSecTypes[i]);
//				newSecurities[index].setBBSecurityDesc(bbSecDescs[i]);
//				newSecurities[index].setBBSecurityDesc(isins[i]);
//
//				if (m_getPrimaryExch)
//					newSecurities[index].setExchange(pexch[i]);
//
//				if (!bbTickers[i].empty()) 
//					newSecurities[index].setBBTicker(bbTickers[i]);
//
//				if (tickers && !tickers[i].empty())
//					newSecurities[index].setTicker(tickers[i]);
//			}
//		}
//			//SpRest restData(serverUrl, keyword, yml);
//
//			//int errCode = restData.getDataSync(dci, args);
//
//			//int httpStatus = restData.httpStatus();
//
//			//if (errCode != 0 || restData.curlStatus() != 0 || httpStatus < 200 || httpStatus >= 300) {
//			//	CError("Fatal error updating Security Master! ABORTING!!!");
//			//	Util::exit(1);
//			//}
//
//
//	}
//
//	void SpSecMaster::preCache(CacheInfoPtrVec& cinfo, IDataRegistry& dr, const DataCacheInfo& dci, bool singleDate /* = false */) {
//		const auto& c	= config();
//		auto serverUrl	= c.getServerUrl();
//
//		ASSERT(!serverUrl.empty(), "Invalid serverUrl which is empty!");
//		auto yml		= c.getYml();
//		auto keyword	= c.getKeyword("secKeyword");
//		StringVec fields= { "SYMBOL", "BTKR", "TIMESTAMP" };
//
//		if (!m_tickerField.empty())
//			fields.push_back(m_tickerField);
//
//		preCache(cinfo, dr, dci, serverUrl, yml, keyword, fields);
//	}
//
//	void SpSecMaster::buildSecurities(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities) {
//		const IntDay diDelta = 30;
//
//		IntDate diIter = dci.diStart;
//
//		CDebug("Security master precaching ...");
//		preCache(m_cinfo, dr, dci);
//		CDebug("Precaching complete. Waiting for results ...");
//
//		size_t numLeft = m_cinfo.size();
//
//		while (m_cinfo.size()) {
//			auto iter = m_cinfo.begin();
//
//			preCache(m_cinfo, dr, dci);
//
//			auto ci = *iter;
//
//			/// erase this now ...
//			m_cinfo.erase(iter);
//
//			/// wait a little while and check whether the future is ready or not 
//			//auto futureState = ci->future.wait_for(std::chrono::milliseconds(10));
//			//if (futureState != std::future_status::ready)
//			//	continue;
//			ci->future.wait();
//
//			m_numResponses++;
//			int errCode = ci->future.get();
//
//			std::string exchange = ci->exchange;
//
//			CInfo("Updating data for exchange: %s [%u - %u]", ci->exchange, dci.rt.dates[ci->diStart], dci.rt.dates[ci->diEnd - 1]);
//
//			int httpStatus = ci->restData->httpStatus();
//
//			if (errCode != 0 || ci->restData->curlStatus() != 0 || httpStatus < 200 || httpStatus >= 300) {
//				CError("Fatal error updating Security Master! ABORTING!!!");
//				Util::exit(1);
//			}
//
//			//if (errCode != 0) {
//			//	CWarning("No data generated. Ignoring ...");
//			//	ci->restData = nullptr;
//			//	continue;
//			//}
//
//			SpRest::Result result = ci->restData->result();
//			auto figiData = result.data["SYMBOL"];
//			auto bbTickerData = result.data["BTKR"];
//			DataPtr tickerData = nullptr;
//			auto timestampData = result.data["TIMESTAMP"];
//
//			if (!m_tickerField.empty())
//				tickerData = result.data[m_tickerField];
//
//			/// No data was returned ...
//			if (!figiData || !figiData->cols()) {
//				ci->restData = nullptr;
//				continue;
//			}
//
//			ASSERT(figiData->cols() == bbTickerData->cols(), "Mismatched number of entries between SYMBOL and BTKR. SYMBOL: " << figiData->cols() << " - BTKR: " << bbTickerData->cols());
//			ASSERT(figiData->cols() == timestampData->cols(), "Mismatched number of entries between SYMBOL and TIMESTAMP. SYMBOL: " << figiData->cols() << " - TIMESTAMP: " << timestampData->cols());
//
//			if (tickerData) {
//				ASSERT(figiData->cols() == tickerData->cols(), "Mismatched number of entries between SYMBOL and " << m_tickerField << ". SYMBOL: " << figiData->cols() << " - " << m_tickerField << " : " << tickerData->cols());
//			}
//
//			const std::string* figis = (dynamic_cast<StringData*>(figiData.get()))->sptr();
//			const std::string* bbTickers = (dynamic_cast<StringData*>(bbTickerData.get()))->sptr();
//			const std::string* tickers = tickerData ? (dynamic_cast<StringData*>(tickerData.get()))->sptr() : nullptr;
//			const int64_t* timestamps = (dynamic_cast<Int64MatrixData*>(timestampData.get()))->fdata();
//			size_t count = figiData->cols(); /// They're all the same (we just checked above)
//			IntDate dsStart = Util::py_timestampToIntDate(timestamps[0]);
//			size_t dsStartIndex = 0;
//			UIntVec dailySecurities;
//			size_t numDays = ci->diEnd - ci->diStart + 1; /// End date is inclusive
//
//			dailySecurities.reserve(count / numDays);
//
//			for (size_t i = 0; i < count; i++) {
//				const auto& figi = figis[i]; 
//				IntDate currDate = Util::py_timestampToIntDate(timestamps[i]);
//
//				ASSERT(currDate >= dsStart, "Invalid timestamp discovered. Timestamp values must be in increasing order. Current timestamp: " << currDate << " - previous: " << dsStart);
//
//				if (currDate != dsStart) {
//					cacheDailySecurities(dsStart, dailySecurities, ci->exchange);
//
//					dailySecurities.clear();
//					dailySecurities.reserve(count / numDays);
//					dsStart = currDate;
//					dsStartIndex = i;
//				}
//
//				auto findIter = m_securityLookup.find(figi);
//				Security::Index currIndex = Security::s_invalidIndex;
//				std::string ticker = tickers ? tickers[i] : "";
//
//				if (ticker.empty())
//					continue;
//
//				if (findIter == m_securityLookup.end()) {
//					CTrace("Got new security: %s", figi);
//
//					Security newSecurity(figi, bbTickers[i], bbTickers[i]);
//					newSecurity.index = (Security::Index)m_securityLookup.size();
//					newSecurity.isInactive = false;
//
//					newSecurity.setExchange(exchange);
//					newSecurity.setBBTicker(bbTickers[i]);
//					if (ticker.empty())
//						newSecurity.setTicker(ticker);
//					newSecurity.setName(figi);
//
//					m_securityLookup[figi] = newSecurity.index;
//					newSecurities.push_back(newSecurity);
//
//					currIndex = newSecurity.index;
//				}
//				else {
//					currIndex = findIter->second;
//					/// Update the ticker and BBTicker
//					Security* security = nullptr;
//					if (currIndex < (Security::Index)m_allSecurities.size())
//						security = &(m_allSecurities[currIndex]);
//					else {
//						for (size_t nsi = 0; nsi < newSecurities.size() && !security; nsi++) {
//							Security* newSecurity = &(newSecurities[nsi]);
//							if (newSecurity->index == currIndex) {
//								security = newSecurity;
//								ASSERT(security->uuidStr() == figi, "Security mismatch at index: " << currIndex << ". Expecting: " << figi << " - Found: " << newSecurity->toString());
//							}
//						}
//					}
//					//else if (currIndex < newSecurities.size())
//					//	security = &(newSecurities[currIndex]);
//
//					if (security && !bbTickers[i].empty())
//						security->setBBTicker(bbTickers[i]);
//
//					if (security && !ticker.empty())
//						security->setTicker(ticker);
//
//					if (!m_getPrimaryExch)
//						security->setExchange(exchange);
//				}
//
//				//ASSERT(currIndex != Security::s_invalidIndex, "Invalid index for FIGI: " << figi);
//				//dailySecurities.push_back((unsigned int)currIndex);
//			}
//
//			//cacheDailySecurities(dsStart, dailySecurities, ci->exchange);
//			CDebug("Security Master Total: %z", m_securityLookup.size());
//
//			/// sanity check that we received something for all the dates that we handled in this chunk
//			//for (size_t ii = ci->diStart; ii < ci->diEnd; ii++) {
//			//	IntDate date = dci.rt.dates[ii];
//			//	auto findIter = m_dailySecurities.find(date);
//			//	if (findIter == m_dailySecurities.end()) {
//			//		CWarning("No securities received for date: %u [%s]", date, ci->exchange);
//			//	}
//			//}
//
//			/// No need for this anymore, free up the memory ...
//			ci->restData = nullptr;
//		}
//	}
//
//	void SpSecMaster::build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
//	}
//} /// namespace pesa
//
//////////////////////////////////////////////////////////////////////////////
///// To be called from the simulator
//////////////////////////////////////////////////////////////////////////////
//EXPORT_FUNCTION pesa::IComponent* createSpSecMaster(const pesa::ConfigSection& config) {
//	return new pesa::SpSecMaster((const pesa::ConfigSection&)config);
//}
//
