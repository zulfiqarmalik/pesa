/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// SpTradingTimes.cpp
///
/// Created on: 12 Oct 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "SpGeneric.h"
#include "Poco/String.h"
#include "Core/Math/Stats.h"

namespace pesa {
	class SpTradingTimes : public SpGeneric {
	private:
		StringVec				m_exchanges;
		StringVec				m_countries;
		size_t					m_exchangeIndex = 0;
		StringMap				m_activityMap;
		std::string				m_market;

		static int64_t			getTradingTime(const StringVec& activities, const Int64MatrixData& dateTime, const std::string& activityName, 
								const StringVec& markets, const std::string& market);

	protected:
		//virtual void 			preCache(IDataRegistry& dr, const DataCacheInfo& dci, const std::string& keyExtra);

	public:
								SpTradingTimes(const ConfigSection& config);
		virtual 				~SpTradingTimes();

		////////////////////////////////////////////////////////////////////////////
		/// SpGeneric overrides
		////////////////////////////////////////////////////////////////////////////
		virtual std::string& 	resolveArg(std::string& value) const;
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			buildDaily(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void			preCache(IDataRegistry& dr, const DataCacheInfo& dci, const std::string& keyExtra);
		virtual IntDay			getMaxPrecache() const { return 4; }
	};

	SpTradingTimes::SpTradingTimes(const ConfigSection& config) 
		: SpGeneric(config) {
		setLogChannel("SpTradingTimes");
		m_minfo.specifiedFieldsOnly = true;

		std::string exchangesStr = config.getRequired("exchanges");
		FrameworkUtil::parseExchanges(exchangesStr, m_exchanges, m_countries);

		ASSERT(m_countries.size() == m_exchanges.size(), "Countries and exchanges do not match. Number of countries: " << m_countries.size() << " - Number of exchanges: " << m_exchanges.size());

		std::string activitiesStr = config.getRequired("activities");
		auto parts = Util::split(activitiesStr, ",");
		for (auto& part : parts) {
			auto activity = Util::split(part, "=");
			ASSERT(activity.size() == 2, "Invalid activity: " << part);
			m_activityMap[activity[0]] = activity[1];
		}

		m_market = config.getString("market", "");
	}

	SpTradingTimes::~SpTradingTimes() {
		CTrace("Deleting SpTradingTimes");
	}

	std::string& SpTradingTimes::resolveArg(std::string& value) const {
		std::string country = m_countries[m_exchangeIndex];
		std::string exchange = m_exchanges[m_exchangeIndex];

		value = Poco::replace(value, "{$country}", country.c_str());
		value = Poco::replace(value, "{$exchange}", exchange.c_str());

		return value;
	}

	//void SpTradingTimes::preCache(IDataRegistry& dr, const DataCacheInfo& dci, const std::string& keyExtra) {
	//	IntDay cacheCount = SpGeneric::s_maxPrecache - std::max(m_diLast - dci.di, 0);
	//	IntDate date = dci.rt.dates[dci.di];

	//	/// cache is already been full
	//	if (cacheCount <= 0)
	//		return;

	//	m_diLast = std::min(dci.di + cacheCount, dci.diEnd);

	//	for (IntDay di = dci.di; di <= m_diLast; di++)
	//		preCacheDay(dr, dci, di, keyExtra);
	//}

	void SpTradingTimes::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		Dataset ds("calendar");

		for (size_t i = 0; i < m_exchanges.size(); i++) {
			const std::string& exchange = m_exchanges[i];
			std::string lexchange = Poco::toLower(exchange);
			std::string gmtOpenId = "gmt_open_" + lexchange;
			std::string gmtCloseId = "gmt_close_" + lexchange;
			ds.add(DataValue(gmtOpenId, DataType::kTimestamp, sizeof(int64_t), THIS_DATA_FUNC(SpTradingTimes::buildDaily), DataFrequency::kDaily, 0, (void*)i));
			ds.add(DataValue(gmtCloseId, DataType::kTimestamp, sizeof(int64_t), THIS_DATA_FUNC(SpTradingTimes::buildDaily), DataFrequency::kDaily, 0, (void*)i));

			m_mappings[gmtOpenId] = "GMT_OPEN";
			m_mappings[gmtCloseId] = "GMT_CLOSE";
			//ds.add(DataValue("local_open_" + lexchange, DataType::kTimestamp, sizeof(int64_t), THIS_DATA_FUNC(SpTradingTimes::buildDaily), DataFrequency::kDaily, 0, (void*)i));
			//ds.add(DataValue("local_close_" + lexchange, DataType::kTimestamp, sizeof(int64_t), THIS_DATA_FUNC(SpTradingTimes::buildDaily), DataFrequency::kDaily, 0, (void*)i));
		}

		SpGeneric::m_dataRequired.push_back("GMT_OPEN");
		SpGeneric::m_requiredDataTypes.push_back(DataType::kTimestamp);

		SpGeneric::m_dataRequired.push_back("GMT_CLOSE");
		SpGeneric::m_requiredDataTypes.push_back(DataType::kTimestamp);

		//SpGeneric::m_dataRequired.push_back("LOCALTIME_OPEN");
		//SpGeneric::m_requiredDataTypes.push_back(DataType::kTimestamp);

		//SpGeneric::m_dataRequired.push_back("LOCALTIME_CLOSE");
		//SpGeneric::m_requiredDataTypes.push_back(DataType::kTimestamp);

		SpGeneric::m_dataRequired.push_back("ACTIVITY");
		SpGeneric::m_requiredDataTypes.push_back(DataType::kString);

		if (!m_market.empty()) {
			SpGeneric::m_dataRequired.push_back("MARKET");
			SpGeneric::m_requiredDataTypes.push_back(DataType::kString);
		}

		datasets.add(ds);
	}

	int64_t SpTradingTimes::getTradingTime(const StringVec& activities, const Int64MatrixData& dateTime, const std::string& activityName, 
		const StringVec& markets, const std::string& market) {
		FloatVec times;
		for (size_t i = 0; i < activities.size(); i++) { 
			if (activities[i] == activityName) {
				/// Try to match the market if one is given
				if (markets.size() && !market.empty() && markets[i] != market) {
					continue;
				}
				int64_t time = dateTime(0, i);
				return time;
			}
		}

		return 0;
	}

	void SpTradingTimes::preCache(IDataRegistry& dr, const DataCacheInfo& dci, const std::string& keyExtra) {
		m_exchangeIndex = 0;
		while (m_exchangeIndex < m_exchanges.size()) {
			std::string exchange = m_exchanges[m_exchangeIndex];
			std::string country = m_countries[m_exchangeIndex];
			std::string key = country + "-" + exchange;
			SpGeneric::preCache(dr, dci, key);
			m_exchangeIndex++;
		}
	}

	void SpTradingTimes::buildDaily(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		m_exchangeIndex = 0;
		std::string rmappedName;
		size_t exchangeIndex = (size_t)(dci.dv.customInfo);
		std::string exchange = m_exchanges[exchangeIndex];
		std::string country = m_countries[exchangeIndex];
		std::string key = country + "-" + exchange;
		IntDate today = dci.rt.dates[dci.di];
		SpRest::Result result = waitForResult(dr, dci, frame, rmappedName, key);

		std::string dataName = m_mappings[rmappedName];
		ASSERT(!dataName.empty(), "Unable to find data: " << rmappedName << " on date: " << today);

		std::string activityName = m_activityMap[dataName];
		ASSERT(!activityName.empty(), "Unable to find activity: " << dataName << " on date: " << today);

		const StringData* marketPtr = nullptr;
		const StringData* activityPtr = dynamic_cast<const StringData*>(result.data["ACTIVITY"].get());

		if (!activityPtr) {
			CWarning("No data for exchange: %s [%u]", key, today);
			frame.noDataUpdate = true;
			return;
		}

		const StringVec& activities = activityPtr->vector();
		const Int64MatrixData& times = *dynamic_cast<const Int64MatrixData*>(result.data[dataName].get());
		StringVec markets;

		if (!m_market.empty())
			marketPtr = dynamic_cast<const StringData*>(result.data["MARKET"].get());

		int64_t time = getTradingTime(activities, times, activityName, marketPtr ? marketPtr->vector() : markets, m_market);

		if (!time) {
			/// If we don't get any data then we set it to 11PM GMT (this is avoid data sets that 
			/// are sensetive to trading times from getting any trading day data (the DataRegistry
			/// will take care of that)
			Poco::DateTime dtToday = Util::intToDate(today);
			Poco::DateTime dtNew(dtToday.year(), dtToday.month(), dtToday.day(), 23);
			CInfo("No trading times on date: %u - Using: %s", today, Util::dateTimeToStr(dtNew));
			time = dtNew.timestamp().epochMicroseconds() * 1000; /// We're looking for Epoch NANO-seconds (Python)
		}

		ASSERT(time, "Unable to get time for activity: " << activityName << " on date: " << today);
		Int64MatrixData* data = new Int64MatrixData(dci.universe, dci.def, 1, 1);
		data->setValue(0, 0, time);

		/// We don't need this data again ...
		result.data[dataName] = nullptr;

		frame.data = DataPtr(data);
		frame.dataOffset = 0;
		frame.dataSize = frame.data->dataSize();

		CInfo("Updated data: [%-8u]-[%s]: %s", today, key, rmappedName);
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createSpTradingTimes(const pesa::ConfigSection& config) {
	return new pesa::SpTradingTimes((const pesa::ConfigSection&)config);
}

