/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Starmine.cpp
///
/// Created on: 02 Feb 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "SpGeneric.h"
#include "Poco/String.h"

namespace pesa {
	class Starmine : public SpGeneric {
	private:
		size_t					m_maxItems = 50;
		StringVec				m_allItemsStr;			/// We keep these so that we can pass pointer to this ...
		std::map<std::string, bool> m_dsIgnore;

	protected:
		virtual std::string		getKeyExtra(IDataRegistry& dr, const DataCacheInfo& dci, IntDate date) const;
		virtual CacheInfoPtr	preCacheDay(IDataRegistry& dr, const DataCacheInfo& dci, IntDay di, const std::string& keyExtra);

	public:
								Starmine(const ConfigSection& config);
		virtual 				~Starmine();

		////////////////////////////////////////////////////////////////////////////
		/// Starmine overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
	};

	Starmine::Starmine(const ConfigSection& config) 
		: SpGeneric(config) {
		setLogChannel("Starmine");
		m_maxItems = config.get<size_t>("maxItems", m_maxItems);

		auto ignoreStr = config.getString("ignore", "");
		auto ignoreList = Util::split(ignoreStr, ",");

		for (const auto& dsIgnore : ignoreList)
			m_dsIgnore[dsIgnore] = true;
	}

	Starmine::~Starmine() {
		CTrace("Deleting Starmine");
	}

	std::string Starmine::getKeyExtra(IDataRegistry& dr, const DataCacheInfo& dci, IntDate date) const {
		ASSERT(dci.dv.customInfo, "Invalid string as part of the data value custom info: " << dci.dv.toString(dci.ds));
		size_t indexId = reinterpret_cast<size_t>(dci.dv.customInfo) - 1;
		ASSERT(indexId < m_allItemsStr.size(), "Invalid custom info index: " << indexId);
		return m_allItemsStr[indexId];
	}

	SpGeneric::CacheInfoPtr Starmine::preCacheDay(IDataRegistry& dr, const DataCacheInfo& dci, IntDay di, const std::string& keyExtra) {
		ASSERT(dci.dv.customInfo, "Invalid string as part of the data value custom info: " << dci.dv.toString(dci.ds));
		size_t indexId = reinterpret_cast<size_t>(dci.dv.customInfo) - 1;
		ASSERT(indexId < m_allItemsStr.size(), "Invalid custom info index: " << indexId);

		std::string itemsStr = m_allItemsStr[indexId];
		m_additionalArgs["Items"] = itemsStr;

		/// The keyExtra parameter is our items string. These are the items that we're fetching in the current iteration!
		return SpGeneric::preCacheDay(dr, dci, di, itemsStr);
	}

	void Starmine::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		/// Here is where we'll fix the mappings
		std::string keyword = config().getRequired("keyword");
		IntMatrixData* tableItems = dr.intData(nullptr, m_datasetName + "." + keyword);
		ASSERT(tableItems, "Invalid table items!");

		auto numCols = tableItems->cols();

		size_t itemIter = 0;
		size_t dsIndex = 0;

		while (itemIter < numCols) {
			size_t iterEnd = std::min(itemIter + m_maxItems, numCols);
			std::string allItemsStr = "(";

			m_datasets.clear();

			for (auto ii = itemIter; ii < iterEnd; ii++) {
				auto itemId = (*tableItems)(0, ii);
				auto itemIdStr = "Item$" + Util::cast(itemId);
				auto iter = m_mappings.find(itemIdStr);

				if (iter == m_mappings.end()) {
					CWarning("[Starmine] Item: %d not mapped. Ignoring ...", itemId);
					continue;
				}

				//ASSERT(iter != m_mappings.end(), "Invalid mapping: " << itemIdStr);
				std::string dsName = iter->second;

				auto dsIgnoreIter = m_dsIgnore.find(dsName);

				if (dsIgnoreIter != m_dsIgnore.end()) {
					// CWarning("Ignoring dataset: %s", dsName);
					continue;
				}

				m_datasets.push_back(dsName);
				allItemsStr += Util::cast(itemId) + ",";
			}

			allItemsStr += ")";
			itemIter = iterEnd;
			m_allItemsStr.push_back(allItemsStr);
			//m_additionalArgs["Items"] = allItemsStr;
			SpGeneric::initDatasets(dr, datasets);

			//for (auto& ds : datasets.datasets) {
			for (size_t dsi = dsIndex; dsi < datasets.datasets.size(); dsi++) {
				auto& ds = datasets.datasets[dsi];

				for (auto& dv : ds.values) 
					dv.customInfo = reinterpret_cast<void*>(m_allItemsStr.size());
			}

			dsIndex = datasets.datasets.size();
		}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createStarmine(const pesa::ConfigSection& config) {
	return new pesa::Starmine((const pesa::ConfigSection&)config);
}

