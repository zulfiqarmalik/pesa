/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// WorldScope.cpp
///
/// Created on: 14 Mar 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "SpGeneric.h"
#include "Poco/String.h"

namespace pesa {
	class WorldScope : public SpGeneric {
	private:
		typedef std::map<std::string, std::string> StrMap;

		size_t					m_maxItems = 50;
		StringVec				m_indexes;				/// The indexes that we will read back from the customInfo
		StrMap					m_measureLookup;		/// Measure code to ID lookup (for better variable names)
		StrMap					m_periodLookup;			/// The period type to period id lookup (for better variable names)

	protected:
		virtual std::string		getKeyExtra(IDataRegistry& dr, const DataCacheInfo& dci, IntDate date) const;
		virtual CacheInfoPtr	preCacheDay(IDataRegistry& dr, const DataCacheInfo& dci, IntDay di, const std::string& keyExtra);
		virtual SpRest::ResultVec waitForResults(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame, std::string& rmappedName, std::string keyExtra = "");

	public:
								WorldScope(const ConfigSection& config);
		virtual 				~WorldScope();

		////////////////////////////////////////////////////////////////////////////
		/// WorldScope overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
	};

	WorldScope::WorldScope(const ConfigSection& config) 
		: SpGeneric(config) {
		setLogChannel("WorldScope");
		m_maxItems = config.get<size_t>("maxItems", m_maxItems);
	}

	WorldScope::~WorldScope() {
		CTrace("Deleting WorldScope");
	}

	inline std::string makeName(const std::string& dvName, const std::string& tableName, const std::string& measureId, const std::string& periodId) {
		return tableName + "_" + dvName + "_" + measureId + "_" + periodId;
	}

	SpRest::ResultVec WorldScope::waitForResults(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame, std::string& rmappedName, std::string keyExtra) {
		auto ret = SpGeneric::waitForResults(dr, dci, frame, rmappedName, keyExtra);

		//const std::string* index = reinterpret_cast<std::string*>(dci.dv.customInfo);
		//ASSERT(index, "Invalid string as part of the data value custom info: " << dci.dv.toString(dci.ds));
		//std::string keyword = config().getRequired("keyword");
		//std::string dvName = dci.dv.definition.name;
		//auto parts = Util::split(*index, ",");
		//auto measureCode = parts[0];
		//auto perType = parts[1];
		//std::string measureId = m_measureLookup[measureCode];
		//std::string periodId = m_periodLookup[perType];

		rmappedName = dci.dv.definition.name; /// makeName(dvName, keyword, measureId, periodId);

		return ret;
	}

	std::string WorldScope::getKeyExtra(IDataRegistry& dr, const DataCacheInfo& dci, IntDate date) const {
		ASSERT(dci.dv.customInfo, "Invalid string as part of the data value custom info: " << dci.dv.toString(dci.ds));
		size_t indexId = reinterpret_cast<size_t>(dci.dv.customInfo) - 1;
		ASSERT(indexId < m_indexes.size(), "Invalid custom info index: " << indexId);
		return m_indexes[indexId];
	}

	SpGeneric::CacheInfoPtr WorldScope::preCacheDay(IDataRegistry& dr, const DataCacheInfo& dci, IntDay di, const std::string& keyExtra) {
		ASSERT(dci.dv.customInfo, "Invalid string as part of the data value custom info: " << dci.dv.toString(dci.ds));
		size_t indexId = reinterpret_cast<size_t>(dci.dv.customInfo) - 1;
		ASSERT(indexId < m_indexes.size(), "Invalid custom info index: " << indexId);
		std::string& index = m_indexes[indexId];

		auto parts = Util::split(index, ",");
		auto measureCode = parts[0];
		auto perType = parts[1];

		m_additionalArgs["Measures"] = "(" + measureCode + ",)";
		m_additionalArgs["PerTypes"] = "(" + perType + ",)";

		/// The keyExtra parameter is our items string. These are the items that we're fetching in the current iteration!
		return SpGeneric::preCacheDay(dr, dci, di, index);
	}

	void WorldScope::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		/// Here is where we'll fix the mappings
		std::string keyword = config().getRequired("keyword");
		const StringData* tableMeasureCodes = dr.stringData(nullptr, m_datasetName + ".Measure_" + keyword);
		ASSERT(tableMeasureCodes, "Unable to load table measure codes: " << keyword);

		const StringData* tablePeriodTypes = dr.stringData(nullptr, m_datasetName + ".PerType_" + keyword);
		ASSERT(tablePeriodTypes, "Unable to load table PerType: " << keyword);

		if (!m_measureLookup.size() && !m_periodLookup.size()) {
			const StringData* measureIds	= dr.stringData(nullptr, m_datasetName + ".measureId");
			const StringData* measureCodes	= dr.stringData(nullptr, m_datasetName + ".measureCode");
			const StringData* periodIds		= dr.stringData(nullptr, m_datasetName + ".periodId");
			const StringData* periodCodes	= dr.stringData(nullptr, m_datasetName + ".periodCode");
			typedef std::map<std::string, std::string> StrMap;

			StrMap measureLookup, periodLookup;

			ASSERT(measureIds->cols() == measureCodes->cols(), "Invalid measure code data!");
			ASSERT(periodIds->cols() == periodCodes->cols(), "Invalid period code data!");

			for (size_t ii = 0; ii < periodCodes->cols(); ii++) {
				std::string periodCode = periodCodes->getValue(0, ii);
				std::string periodId = periodIds->getValue(0, ii);
				m_periodLookup[periodCode] = periodId;
			}

			for (size_t ii = 0; ii < measureCodes->cols(); ii++) {
				std::string measureCode = measureCodes->getValue(0, ii);
				std::string measureId = measureIds->getValue(0, ii);
				m_measureLookup[measureCode] = measureId;
			}
		}

		size_t numMeasureCodes = tableMeasureCodes->cols();
		size_t numPeriodTypes = tablePeriodTypes->cols();

		ASSERT(numMeasureCodes, "Invalid number of measure codes for table: " << keyword);
		ASSERT(numPeriodTypes, "Invalid number of period types for table: " << keyword);

		size_t dsIndex = 0;

		for (size_t mc = 0; mc < numMeasureCodes; mc++) {
			std::string measureCode = tableMeasureCodes->getValue(0, mc);
			ASSERT(m_measureLookup.find(measureCode) != m_measureLookup.end(), "Unable to find measure code: " << measureCode);
			std::string measureId = m_measureLookup[measureCode];

			for (size_t pt = 0; pt < numPeriodTypes; pt++) {
				std::string perType = tablePeriodTypes->getValue(0, pt);
				ASSERT(m_periodLookup.find(perType) != m_periodLookup.end(), "Unable to find period type: " << perType);
				std::string periodId = m_periodLookup[perType];

				std::string index = measureCode + "," + perType;

				m_indexes.push_back(index);

				//m_additionalArgs["Items"] = allItemsStr;
				SpGeneric::initDatasets(dr, datasets);

				for (auto dsi = dsIndex; dsi < datasets.datasets.size(); dsi++) {
					auto& ds = datasets.datasets[dsi];

					for (size_t dvi = 0; dvi < ds.values.size(); dvi++) {
						auto& dv = ds.values[dvi];
						std::string dvName = dv.definition.name;
						dvName = makeName(dvName, keyword, measureId, periodId);
						dv.definition.setName(dvName);
						dv.customInfo = reinterpret_cast<void*>(m_indexes.size());
					}
				}

				dsIndex = datasets.datasets.size();
			}
		}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createWorldScope(const pesa::ConfigSection& config) {
	return new pesa::WorldScope((const pesa::ConfigSection&)config);
}

