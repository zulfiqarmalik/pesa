/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// BasicSecMaster.cpp
///
/// Created on: 16 Nov 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "PesaImpl/Components/SP/SpRest.h"
#include "Poco/Timestamp.h"
#include "Framework/Helper/FileDataLoader.h"

#include "Poco/String.h"
#include "Poco/StringTokenizer.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Basic Security Master: Used with Sharadar Equity data on Quandl
	////////////////////////////////////////////////////////////////////////////
	class BasicSecMaster : public IDataLoader {
	private:
		typedef std::unordered_map<IntDate, UIntVec>    DailySecMap;
        typedef std::unordered_map<std::string, size_t> SymbolLookupMap;
        typedef std::map<std::string, Security::Index>  SecurityLookup;
        typedef std::unordered_map<std::string, bool>   ExchangeBoolMap;

		bool					m_applyFilters = false;	/// Should we apply filters
		StringMap				m_filters;				/// What are the filters that we need to apply
		StringVec				m_filterIssueDesc;		/// Issue desc filters

		ExchangeBoolMap         m_exchanges;            /// The exchanges that we're supposed to handle
		SecurityLookup			m_securityLookup;		/// Which securities do we already have? 
		DailySecMap				m_dailySecurities;		/// Daily securities ...

		SecurityVec				m_allSecurities;		/// All the securities within the system

		StringMap				m_secDataMap = {
			{ "sd_startDate", 	"preprocess.csi_StartDate" },
			{ "sd_endDate", 	"preprocess.csi_EndDate" },
			{ "sd_exchange", 	"preprocess.csi_Exchange" },
			{ "sd_isActive", 	"preprocess.csi_IsActive" },
			{ "sd_figi", 		"preprocess.openfigi_figi" },
			{ "sd_cfigi", 		"preprocess.openfigi_cfigi" },
			{ "sd_priorTickers","preprocess.quandl_priorTickers" },
			{ "sd_sector", 		"preprocess.quandl_sector" },
			{ "sd_industry", 	"preprocess.quandl_industry" },
			{ "sd_famaIndustry","preprocess.quandl_famaIndustry" },
		};

		bool					filterNewSecurities(const SecurityVec& newSecurities, SecurityVec& filtered);
		void 					buildSecurities(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities);
		void 					buildDetails(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities);

	public:
								BasicSecMaster(const ConfigSection& config);
		virtual 				~BasicSecMaster();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			buildSecurityMaster(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);

		virtual void 			preDataBuild(IDataRegistry& dr, const DataCacheInfo& dci);
		virtual void 			postDataBuild(IDataRegistry& dr, const DataCacheInfo& dci);
	};

	BasicSecMaster::BasicSecMaster(const ConfigSection& config) : IDataLoader(config, "BasicSecMaster") {
        std::string exchangesStr = config.getResolvedRequiredString("exchanges");
        StringVec exchanges, countries;
        FrameworkUtil::parseExchanges(exchangesStr, exchanges, countries);

        for (const auto& exchange : exchanges)
            m_exchanges[Poco::toUpper(exchange)] = true;

		auto filter = config.getString("filter", "");
		if (!filter.empty()) {
			auto filters = Util::split(filter, "|");
			if (filters.size()) {
				m_applyFilters = true;
				for (auto& f : filters)
					m_filters[f] = f;
			}
		}

		auto filterIssueDesc = config.getString("filterIssueDesc", "");
		if (!filterIssueDesc.empty()) {
			auto filters = Util::split(filterIssueDesc, "|");
			if (filters.size()) {
				m_applyFilters = true;
				for (auto& f : filters)
					m_filterIssueDesc.push_back(f);
			}
		}
	}

	BasicSecMaster::~BasicSecMaster() {
		CTrace("Deleting BasicSecMaster");
	}

	void BasicSecMaster::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		std::string name = config().getRequired("name");

		datasets.add(Dataset(name, {
			DataValue("Master", DataType::kSecurityData, Security::s_version, THIS_DATA_FUNC(BasicSecMaster::buildSecurityMaster), 
				DataFrequency::kStatic, DataFlags::kAlwaysLoad | DataFlags::kUpdatesInOneGo | DataFlags::kSecurityMaster),
		}));

		Dataset secDataDS = Dataset("preprocess");

		for (auto iter = m_secDataMap.begin(); iter != m_secDataMap.end(); ++iter) {
			secDataDS.add(DataValue(iter->first, DataType::kString, 0, nullptr, DataFrequency::kStatic, DataFlags::kUpdatesInOneGo | DataFlags::kAlwaysOverwrite));
		}

		datasets.add(secDataDS);
		//datasets.add(Dataset("secData", {
		//	DataValue("startDate", DataType::kString, 0, nullptr, DataFrequency::kUniversal, DataFlags::kUpdatesInOneGo),
		//	DataValue("endDate", DataType::kString, 0, nullptr, DataFrequency::kUniversal, DataFlags::kUpdatesInOneGo),
		//	DataValue("exchange", DataType::kString, 0, nullptr, DataFrequency::kUniversal, DataFlags::kUpdatesInOneGo),
		//	DataValue("isActive", DataType::kString, 0, nullptr, DataFrequency::kUniversal, DataFlags::kUpdatesInOneGo),
		//	DataValue("figi", DataType::kString, 0, nullptr, DataFrequency::kUniversal, DataFlags::kUpdatesInOneGo),
		//	DataValue("cfigi", DataType::kString, 0, nullptr, DataFrequency::kUniversal, DataFlags::kUpdatesInOneGo),
		//	DataValue("priorTickers", DataType::kString, 0, nullptr, DataFrequency::kUniversal, DataFlags::kUpdatesInOneGo),
		//	DataValue("sector", DataType::kString, 0, nullptr, DataFrequency::kUniversal, DataFlags::kUpdatesInOneGo),
		//	DataValue("industry", DataType::kString, 0, nullptr, DataFrequency::kUniversal, DataFlags::kUpdatesInOneGo),
		//	DataValue("famaIndustry", DataType::kString, 0, nullptr, DataFrequency::kUniversal, DataFlags::kUpdatesInOneGo),
		//}));
	}

	void BasicSecMaster::postDataBuild(IDataRegistry& dr, const DataCacheInfo& dci) {
		/// We do not need this data anymore ...
		m_securityLookup.clear();
		m_dailySecurities.clear();
		m_allSecurities.clear();
	}

	void BasicSecMaster::preDataBuild(IDataRegistry& dr, const DataCacheInfo& dci) {
		std::string name = config().getRequired("name");

		const SecurityData* secData = dr.get<SecurityData>(nullptr, name + ".Master", true);

		if (secData) {
			size_t numSecurities = secData->numSecurities();
			m_allSecurities.resize(numSecurities);
			CInfo("Existing number of securities: %z", numSecurities);

			for (size_t i = 0; i < numSecurities; i++) {
				const Security& sec = secData->security(i);
				m_securityLookup[sec.uuidStr()] = sec.index;
				m_allSecurities[i] = sec;

				//auto str = sec.debugString();
				//CDebug("\n%z - %s", i, str);
			}
		}
	}

	bool BasicSecMaster::filterNewSecurities(const SecurityVec& newSecurities, SecurityVec& filtered) {
		if (!m_applyFilters || (!m_filters.size()))
			return false;

		for (const auto& security : newSecurities) {
			auto issueClass = security.bbSecurityTypeStr(); 
			auto iter = m_filters.find(issueClass);

			if (iter == m_filters.end()) {
				CTrace("Filtering out ticker: %s [class = %s]", security.tickerStr(), issueClass);
			}
			else { 
				bool useSecurity = true;
				auto issueDesc = security.issueDescStr();
				
				for (const auto& fdFilter : m_filterIssueDesc) {
					if (issueDesc.find(fdFilter) != std::string::npos) {
						CTrace("Filtering out issue desc: %s [desc = %s, filter = %s]", security.tickerStr(), issueDesc, fdFilter);
						useSecurity = false;
						break;
					}
				}

				if (useSecurity)
					filtered.push_back(security);
			}
		}

		return true;
	}

	void BasicSecMaster::build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		std::string dataName = dci.def.name;
		std::string srcDataId = m_secDataMap[dataName];

		ASSERT(!srcDataId.empty(), "Unable to find the source data for: " << dataName);

		DataInfo dinfo;
		dinfo.noCacheUpdate = true;

		const StringData* cfigi			= dr.stringData(nullptr, "preprocess.openfigi_cfigi", (DataInfo*)&dinfo);
		const StringData* srcData		= dr.stringData(nullptr, srcDataId, (DataInfo*)&dinfo);
		size_t numStocks				= m_allSecurities.size();
		size_t count					= cfigi->cols();
		StringData* data				= new StringData(nullptr, 1, numStocks);
		size_t numCopied				= 0;

		for (size_t ii = 0; ii < count; ii++) {
			const auto& uuid = cfigi->getValue(0, ii);
			auto iter = m_securityLookup.find(uuid);

			if (iter != m_securityLookup.end()) {
				const auto& value = srcData->getValue(0, ii);
				Security::Index ii = iter->second;
				data->setValue(0, ii, value);
				numCopied++;
			}
		}

		CInfo("Updated data: [%-8u]-[Copied: %5z / %5z] - %s", dci.rt.dates[dci.di], numCopied, numStocks, dci.def.toString(dci.ds));
		frame.data = StringDataPtr(data);
		frame.dataSize = frame.data->dataSize();
		frame.dataOffset = 0;
	}

	void BasicSecMaster::buildSecurities(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities) {
		DataInfo dinfo;
		dinfo.noCacheUpdate = true;
		dinfo.noAssertOnMissingDataLoader = true;

		const StringData* ticker		= dr.stringData(nullptr, "preprocess.quandl_ticker", (DataInfo*)&dinfo);
		const StringData* cfigi			= dr.stringData(nullptr, "preprocess.openfigi_cfigi", (DataInfo*)&dinfo);
		const StringData* name			= dr.stringData(nullptr, "preprocess.quandl_name", (DataInfo*)&dinfo);
		const StringData* cusip			= dr.stringData(nullptr, "preprocess.quandl_cusip", (DataInfo*)&dinfo);
		const StringData* exchange		= dr.stringData(nullptr, "preprocess.quandl_exchange", (DataInfo*)&dinfo);
		const StringData* secType		= dr.stringData(nullptr, "preprocess.openfigi_securityType", (DataInfo*)&dinfo);
		const StringData* currency		= dr.stringData(nullptr, "preprocess.quandl_currency", (DataInfo*)&dinfo);
		const StringData* delistedInfo	= dr.stringData(nullptr, "preprocess.quandl_delistedFrom", (DataInfo*)&dinfo);
		const StringData* exchCode		= dr.stringData(nullptr, "preprocess.openfigi_exchCode", (DataInfo*)&dinfo);
		const StringData* isForeign		= dr.stringData(nullptr, "preprocess.quandl_isForeign", (DataInfo*)&dinfo);
		const StringData* bbTicker		= dr.stringData(nullptr, "preprocess.openfigi_ticker", (DataInfo*)&dinfo);
		const StringData* parentUuid	= dr.stringData(nullptr, "preprocess.parentUuid", (DataInfo*)&dinfo);

		size_t count					= ticker->cols();

		for (size_t ii = 0; ii < count; ii++) {
			auto uuid					= cfigi->getValue(0, ii);

			ASSERT(!uuid.empty(), "Empty UUID for ticker: " << ticker->getValue(0, ii));

			/// If this CFIGI is already part of the all securities table then we just ignore it
			auto iter					= m_securityLookup.find(uuid);

			if (iter != m_securityLookup.end())
				continue;

			Security sec;

			sec.setTicker(ticker->getValue(0, ii));
			sec.setBBTicker(bbTicker->getValue(0, ii) + " " + exchCode->getValue(0, ii));
			sec.setUuid(uuid);
			sec.setTicker(ticker->getValue(0, ii));
			sec.setName(name->getValue(0, ii));
			//sec.setCusip(cusip->getValue(0, ii));
			sec.setExchange(exchange->getValue(0, ii));
			sec.setParentUuid(parentUuid->getValue(0, ii));

			bool adr = isForeign->getValue(0, ii) == "Y" || name->getValue(0, ii).find("(Foreign)") != std::string::npos ? true : false;
			sec.setBBSecurityType(!adr ? secType->getValue(0, ii) : "ADR");
			sec.setCurrency(currency->getValue(0, ii));

			sec.isInactive = delistedInfo->getValue(0, ii) == "Y" ? true : false;

			newSecurities.push_back(sec);
		}
	}

    void BasicSecMaster::buildSecurityMaster(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
        SecurityVec newSecurities;
        buildSecurities(dr, dci, newSecurities);

        CDebug("Number of new securities found: %z", newSecurities.size());

        size_t numExisting = m_allSecurities.size();
        size_t numNew = newSecurities.size();
        size_t numFiltered = numNew;
        SecurityVec filtered;

        if (!m_applyFilters || (!m_filters.size() && !m_filterIssueDesc.size())) {
            m_allSecurities.insert(m_allSecurities.end(), newSecurities.begin(), newSecurities.end());
            buildDetails(dr, dci, m_allSecurities);
        }
        else {
            /// Get the details of the new securities ...
            buildDetails(dr, dci, newSecurities);

            /// Then update the details of the existing securities as well
            CDebug("Updating the details of existing securities. Count: %z", m_allSecurities.size());
            buildDetails(dr, dci, m_allSecurities);

            filterNewSecurities(newSecurities, filtered);
            numFiltered = filtered.size();

            /// Assign the correct indexes to the securities
            for (auto& sec : filtered) {
                sec.index = (Security::Index)m_allSecurities.size();
                m_allSecurities.push_back(sec);
            }
        }

        try {
            CDebug("Number of new securities: %z - After filteration: %z [Existing: %z, DataSize: %z]", numNew, numFiltered, numExisting, m_allSecurities.size() * sizeof(Security));

            /// otherwise we create a new data for these
            frame.data = std::make_shared<SecurityData>(dci.universe, m_allSecurities, dci.def);
            frame.dataOffset = 0;
            frame.dataSize = frame.data->dataSize();
            frame.noDataUpdate = false;
        }
        catch (const std::exception& e) {
            Util::reportException(e);
            Util::exit(1);
        }
    }

    void BasicSecMaster::buildDetails(IDataRegistry& dr, const DataCacheInfo& dci, std::vector<Security>& newSecurities) {
        CDebug("Getting detailed information about the securities!");
    }
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createBasicSecMaster(const pesa::ConfigSection& config) {
	return new pesa::BasicSecMaster((const pesa::ConfigSection&)config);
}

