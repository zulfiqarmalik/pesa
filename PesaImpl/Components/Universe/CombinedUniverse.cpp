/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// CombinedUniverse.cpp
///
/// Created on: 16 Sep 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include <Poco/String.h>

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// CombinedUniverse - Combines one or more universes
	////////////////////////////////////////////////////////////////////////////
	class CombinedUniverse : public helper::BasicUniverseBuilder {
	private:
		typedef std::unordered_map<Security::Index, size_t> SecMap;
		StringVec				m_universeIds;		/// The ids of the other universes that are used to make this one
		SecMap					m_prevDay;					/// What securities did we use the previous day

	public:
								CombinedUniverse(const ConfigSection& config);
		virtual 				~CombinedUniverse();

		static inline IntDay	delay() { return 1; }

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);

		virtual std::string 	id() const {
			return IComponent::id() + "." + name();
		}
	};

	CombinedUniverse::CombinedUniverse(const ConfigSection& config) 
		: helper::BasicUniverseBuilder(config, "COMBINED_UNIVERSE") {
		std::string ids = config.getRequired("universes");
		m_universeIds = Util::split(ids, ",");
		ASSERT(m_universeIds.size(), "Invalid universes to combine: " << ids);
	}

	CombinedUniverse::~CombinedUniverse() {
		CTrace("Deleting CombinedUniverse!");
	}

	void CombinedUniverse::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		datasets.add(Dataset(name(), {
			DataValue("Universe", DataType::kUInt, sizeof(Security::Index), nullptr, DataFrequency::kStatic, 0),
		}));
	}

	void CombinedUniverse::build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		typedef std::vector<const UIntMatrixData*> UniverseVec;
		typedef std::vector<SecMap> SecMapVec;

		SecMapVec dailySecurities;

		for (const auto& universeId : m_universeIds) {
			const auto* universeData = dr.get<UIntMatrixData>(nullptr, universeId + ".Universe", false);
			size_t cols = universeData->cols();
			dailySecurities.push_back(SecMap());
			SecMap& map = dailySecurities[dailySecurities.size() - 1];

			for (size_t ii = 0; ii < cols; ii++) {
				uint32_t secId = (*universeData)(dci.rt.di, ii);
				map[secId] = ii;
			}
		}

		/// Now we perform the operation
		auto orgOp = config().getRequired("op");
		auto op = Poco::toLower(orgOp);
		SecMap result;

		if (op == "add") {
			for (auto& map : dailySecurities) {
				for (auto iter = map.begin(); iter != map.end(); iter++) {
					uint32_t secId = iter->first;
					result[secId] = result.size();
				}
			}
		}
		else if (op == "subtract") {
			result = dailySecurities[0];

			for (size_t i = 1; i < dailySecurities.size(); i++) {
				auto& map = dailySecurities[i];

				for (auto iter = map.begin(); iter != map.end(); iter++) {
					uint32_t secId = iter->first;
					auto riter = result.find(secId);
					if (riter != result.end())
						result.erase(riter);
				}
			}
		}
		else {
			ASSERT(false, "Invalid operation: " << orgOp);
			return;
		}

		/// OK now we can construct our matrix
		UIntMatrixData* data = new UIntMatrixData(dci.universe, dci.def, 1, result.size(), false);
		size_t index = 0;
		size_t numCommon = 0;

		for (auto iter = result.begin(); iter != result.end(); iter++) {
			uint32_t secId = iter->first;
			if (m_prevDay.find(secId) != m_prevDay.end())
				numCommon++;
			(*data)(0, index++) = secId;
		}

		float commonPercent = 0.0f;
		if (m_prevDay.size())
			commonPercent = ((float)numCommon / (float)m_prevDay.size()) * 100.0f;

		CInfo("%s - %u [di = %d]: Added %z securities (%0.2hf%% overlap with previous day)", name(), dci.rt.dates[dci.di], dci.di, index, commonPercent);

		frame.data = UIntMatrixDataPtr(data);
		frame.dataOffset = 0;
		frame.dataSize = data->dataSize();
		frame.noDataUpdate = false;
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createCombinedUniverse(const pesa::ConfigSection& config) {
	return new pesa::CombinedUniverse((const pesa::ConfigSection&)config);
}

