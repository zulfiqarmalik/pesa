/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// DerivedSecMaster.cpp
///
/// Created on: 08 Mar 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Poco/Timestamp.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class DerivedSecMaster : public IDataLoader {
	private:
		typedef std::map<std::string, Security::Index> SecurityLookup;

		SecurityLookup			m_securityLookup;		/// Which securities do we already have? 
		SecurityVec				m_allSecurities;		/// All the securities within the system
		std::string				m_srcUniverseId;		/// What is the source universe id

	public:
								DerivedSecMaster(const ConfigSection& config);
		virtual 				~DerivedSecMaster();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			buildSecurityMaster(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);

		virtual void 			preDataBuild(IDataRegistry& dr, const DataCacheInfo& dci);
		virtual void 			postDataBuild(IDataRegistry& dr, const DataCacheInfo& dci);
	};

	DerivedSecMaster::DerivedSecMaster(const ConfigSection& config) 
		: IDataLoader(config, "DerivedSecMaster") {
		m_srcUniverseId = config.getRequired("srcUniverse");
	}

	DerivedSecMaster::~DerivedSecMaster() {
		CTrace("Deleting DerivedSecMaster");
	}

	void DerivedSecMaster::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		std::string name = config().getRequired("name");

		datasets.add(Dataset(name, {
			DataValue("Master", DataType::kSecurityData, Security::s_version, THIS_DATA_FUNC(DerivedSecMaster::buildSecurityMaster),
				DataFrequency::kStatic, DataFlags::kAlwaysLoad | DataFlags::kUpdatesInOneGo | DataFlags::kSecurityMaster),
		}));
	}

	void DerivedSecMaster::postDataBuild(IDataRegistry& dr, const DataCacheInfo& dci) {
		/// We do not need this data anymore ...
		m_securityLookup.clear();
		m_allSecurities.clear();
	}

	void DerivedSecMaster::preDataBuild(IDataRegistry& dr, const DataCacheInfo& dci) {
		std::string name = config().getRequired("name");

		const SecurityData* secData = dr.get<SecurityData>(nullptr, name + ".Master", true);

		if (secData) {
			size_t numSecurities = secData->numSecurities();
			m_allSecurities.resize(numSecurities);
			CInfo("Existing number of securities: %z", numSecurities);

			for (size_t i = 0; i < numSecurities; i++) {
				const Security& sec = secData->security(i);
				ASSERT(sec.isValid(), "Invalid security at index: " << i);
				std::string secUuid = sec.uuidStr();

				auto secIter = m_securityLookup.find(secUuid);
				ASSERT(secIter == m_securityLookup.end(), "Invalid state. Security exists more than once in the master: " << sec.toString());

				m_securityLookup[secUuid] = (Security::Index)i;
				m_allSecurities[i] = sec;
			}
		}
	}

	void DerivedSecMaster::buildSecurityMaster(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
        try {
			IUniverse* srcUniverse = dr.getUniverse(m_srcUniverseId);
			ASSERT(srcUniverse, "Unable to load universe: " << m_srcUniverseId);

			/// We always iterate the entire universe ... since it may have been updated for a lot of days ...
			size_t size = srcUniverse->size(dci.rt.diEnd);
			size_t numExisting = m_allSecurities.size();
			IUniverse* mainMaster = dr.getSecurityMaster();

			for (size_t ii = 0; ii < size; ii++) {
				Security sec = srcUniverse->security(dci.rt.diEnd, (Security::Index)ii);
				std::string secUuid = sec.uuidStr();

				/// Get the security info from the main master, which is always up to date!
				const Security* mainSec = mainMaster->mapUuid(secUuid);
				ASSERT(mainSec, "Unable to find security: " << secUuid << " - in the PRE_SecurityMaster!");

				/// Merge the two securities together
				sec = *mainSec;
//				sec = *mainSec;

				auto iter = m_securityLookup.find(secUuid);
				auto iter2 = m_securityLookup.find(sec.uuidStr());

				if (iter == m_securityLookup.end() && iter2 == m_securityLookup.end()) {
					sec.index = (Security::Index)m_allSecurities.size();
					m_allSecurities.push_back(sec);
				}
				else {
					/// Update the existing information!
					if (iter != m_securityLookup.end())
						sec.index = iter->second;
					else if (iter2 != m_securityLookup.end())
						sec.index = iter2->second;

					m_allSecurities[sec.index].merge(sec);
				}

                m_securityLookup[secUuid] = sec.index;
                m_securityLookup[sec.uuidStr()] = sec.index;
			}

			CDebug("Total securities: %z [Num Existing: %z]", m_allSecurities.size(), numExisting);

			/// otherwise we create a new data for these
			frame.data = std::make_shared<SecurityData>(dci.universe, m_allSecurities, dci.def);
			frame.dataOffset = 0;
			frame.dataSize = frame.data->dataSize();
			frame.noDataUpdate = false;
        }
        catch (const std::exception& e) {
			Util::reportException(e);
			Util::exit(1);
        }
	}

	void DerivedSecMaster::build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createDerivedSecMaster(const pesa::ConfigSection& config) {
	return new pesa::DerivedSecMaster((const pesa::ConfigSection&)config);
}

