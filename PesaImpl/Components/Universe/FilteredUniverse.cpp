/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// FilteredUniverse.cpp
///
/// Created on: 06 Oct 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include <Poco/String.h>

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// FilteredUniverse - Takes a source universe and then filters it using 
	/// some criteria
	////////////////////////////////////////////////////////////////////////////
	class FilteredUniverse : public helper::BasicUniverseBuilder {
	private:
		std::string				m_sourceUniverseId;			/// Source universe identifier
		StringVec				m_filters;					/// Data filters
		std::string				m_operation;				/// What is the operation

	public:
								FilteredUniverse(const ConfigSection& config);
		virtual 				~FilteredUniverse();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);

		virtual std::string 	id() const {
			return IComponent::id() + "." + name();
		}
	};

	FilteredUniverse::FilteredUniverse(const ConfigSection& config) 
		: helper::BasicUniverseBuilder(config, "FILTERED_UNIVERSE") {
		m_sourceUniverseId	= config.getRequired("source");
		std::string filters = config.getRequired("filters");
		m_filters			= Util::split(filters, " ");
		m_operation			= config.getRequired("operation");

		const ConfigSection* suConfig = parentConfig()->modules().findUnique("name", m_sourceUniverseId);
		ASSERT(suConfig, "Unable to find config for a universe with name: " << m_sourceUniverseId);
	}

	FilteredUniverse::~FilteredUniverse() {
		CTrace("Deleting FilteredUniverse!");
	}

	void FilteredUniverse::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		datasets.add(Dataset(name(), {
			DataValue("Universe", DataType::kUInt, sizeof(Security::Index), nullptr, DataFrequency::kStatic, 0),
		}));
	}

	void subtract(SecurityIndexVec& finalUniverse, const DataCacheInfo& dci, IntDay di, const UIntMatrixData* universeData, const std::vector<const IntMatrixData*>& filters) {
		size_t cols = universeData->cols();
		auto* secMaster = dci.rt.dataRegistry->getSecurityMaster();

		for (size_t ii = 0; ii < cols; ii++) {
			Security::Index secId = (*universeData)(di, ii);

			if (secId != Security::s_invalidIndex) {
				Security sec = secMaster->security(0, secId);
				bool filterOut = false;
				for (const IntMatrixData* filter : filters) {
					int value = (*filter)(di, secId);

					if (value == 0) {
						filterOut = true;
						break;
					}
				}

				if (!filterOut)
					finalUniverse.push_back(secId);
			}
		}
	}

	void FilteredUniverse::build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		const UIntMatrixData* universeData = dr.get<UIntMatrixData>(nullptr, m_sourceUniverseId + ".Universe", false);
		std::vector<Security::Index> finalUniverse;
		std::vector<const IntMatrixData*> filters;
		IntDay di = dci.di;

		for (const auto& filterName : m_filters) {
			const IntMatrixData* filter = dr.get<IntMatrixData>(nullptr, filterName, false);
			ASSERT(filter, "Unable to load data: " << filterName);
			filters.push_back(filter);
		}

		ASSERT(filters.size(), "Must specify some valid filters!");

		if (m_operation == "subtract")
			subtract(finalUniverse, dci, di, universeData, filters);

		ASSERT(finalUniverse.size(), "Filtered all elements from the universe!");

		/// OK now we can construct our matrix
		UIntMatrixData* data = new UIntMatrixData(dci.universe, dci.def, 1, finalUniverse.size(), false);

		for (size_t ii = 0; ii < finalUniverse.size(); ii++) {
			Security::Index secId = (Security::Index)finalUniverse[ii];
			(*data)(0, ii) = secId;
		}

		CInfo("%s - %u [di = %d]: Filtered from %z => %z", name(), dci.rt.dates[dci.di], dci.di, universeData->cols(), finalUniverse.size());

		frame.data = UIntMatrixDataPtr(data);
		frame.dataOffset = 0;
		frame.dataSize = data->dataSize();
		frame.noDataUpdate = false;
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createFilteredUniverse(const pesa::ConfigSection& config) {
	return new pesa::FilteredUniverse((const pesa::ConfigSection&)config);
}

