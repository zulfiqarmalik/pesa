/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// IndexUniverse.cpp
///
/// Created on: 06 Oct 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Poco/String.h"
#include "Poco/File.h"
#include "Helper/JSON/gason.hpp"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// IndexUniverse - Constructs a universe from an existing published 
	/// index coming from Bloomberg
	////////////////////////////////////////////////////////////////////////////
	class IndexUniverse : public helper::BasicUniverseBuilder {
	private:
		typedef std::map<std::string, StringVec> ExchangeTickerMap;
		std::string				m_tickerDataId = "secData.tickers"; /// The ticker data to use ...
		std::string				m_figiDataId = "secData.figis"; /// The FIGIs to use ...
		Poco::Path				m_basePath;						/// The base path of the index
		ExchangeTickerMap		m_exchangeTickerMap;			/// Exchange code map
		std::string				m_source;						/// The source of this universe
		std::string				m_indexFormat = "BB";			/// Index is in the Bloomberg proprietary format

		gason::JsonValue		getIndicesList(gason::JsonValue& jsonResult);

	public:
								IndexUniverse(const ConfigSection& config);
		virtual 				~IndexUniverse();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);

		virtual std::string 	id() const {
			return IComponent::id() + "." + name();
		}
	};

	IndexUniverse::IndexUniverse(const ConfigSection& config) 
		: helper::BasicUniverseBuilder(config, "FILTERED_UNIVERSE") {
		m_tickerDataId = config.getString("tickerDataId", m_tickerDataId);
		m_figiDataId = config.getString("figiDataId", m_figiDataId);
		m_source = config.getRequired("source");
		m_indexFormat = config.getString("indexFormat", m_indexFormat);

		setLogChannel("IndexUniverse : " + m_source);

		auto basePath = config.getRequired("basePath");
		basePath = parentConfig()->defs().resolve(basePath, (unsigned int)0);

		Poco::Path path(basePath);
		path.append(m_source);
		m_basePath = path.makeAbsolute();

		auto exchangeTickerMap = config.getString("exchangeTickerMap", "");
		exchangeTickerMap = parentConfig()->defs().resolve(exchangeTickerMap, (unsigned int)0);

		if (!exchangeTickerMap.empty()) {
			auto exchangeInfos = Util::split(exchangeTickerMap, ",");
			for (const auto& exchangeInfo : exchangeInfos) {
				auto parts = Util::split(exchangeInfo, ":");
				auto exchange = parts[0];
				auto tickerMaps = Util::split(parts[1], "|");
				for (const auto& tickerMap : tickerMaps)
					m_exchangeTickerMap[exchange].push_back(tickerMap);
			}
		}
	}

	IndexUniverse::~IndexUniverse() {
		CTrace("Deleting IndexUniverse!");
	}

	gason::JsonValue IndexUniverse::getIndicesList(gason::JsonValue& jsonResult) {
		if (m_indexFormat == "QV" || m_indexFormat == "MS")
			return jsonResult.child("results");

		auto fieldName = m_source + " Index";
		return jsonResult.child("INDX_MWEIGHT_HIST").child(fieldName.c_str()).child("Index Member");
	}

	void IndexUniverse::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		datasets.add(Dataset(name(), {
			DataValue("Universe", DataType::kUInt, sizeof(Security::Index), nullptr, DataFrequency::kStatic, DataFlags::kCollectAllBeforeWriting),
			DataValue("IMap", DataType::kUInt, sizeof(unsigned int), THIS_DATA_FUNC(BasicUniverseBuilder::buildIMap), DataFrequency::kStatic,
			DataFlags::kAlwaysOverwrite | DataFlags::kUpdatesInOneGo),
			DataValue("DMap", DataType::kFloat, sizeof(float), THIS_DATA_FUNC(BasicUniverseBuilder::buildDMap), DataFrequency::kStatic, 0),
		}));
	}

	void IndexUniverse::build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		BasicUniverseBuilder::build(dr, dci, frame);
		IntDay di = std::max(dci.di - 1, 0);
		IntDate date = dci.rt.dates[di]; 
		std::string filename = Poco::Path(m_basePath).append(Util::cast(date) + ".json").toString();
		Poco::File file(filename);

		ASSERT(file.exists(), "Index file missing: " << filename);

		/// Read the contents of the JSON. 
		std::string indexDataStr = Util::readEntireTextFile(filename);
		size_t jsonDataLen = indexDataStr.length() + 1;
		char* jsonData = new char[jsonDataLen];
		memcpy(jsonData, indexDataStr.c_str(), indexDataStr.length());
		jsonData[jsonDataLen - 1] = '\0';

		char* startPtr = jsonData;
		char* endPtr = nullptr;
		gason::JsonAllocator jsonAllocator;
		gason::JsonValue jsonResult;

		gason::JsonParseStatus jsonStatus = gason::Json::parse(startPtr, &endPtr, &jsonResult, jsonAllocator);

		ASSERT(jsonStatus == gason::JSON_PARSE_OK, "Unable to parse json in file: " << filename);
		auto indicesList = getIndicesList(jsonResult); ///jsonResult.child("INDX_MWEIGHT_HIST").child(fieldName.c_str()).child("Index Member");
		auto fieldName = m_source + " Index";
		auto iter = gason::begin(indicesList);

		if (!iter.isValid())
			indicesList = jsonResult.child(fieldName.c_str());

		StringVec indices;

		for (gason::JsonIterator iter = gason::begin(indicesList); iter != gason::end(indicesList); iter++) {
			auto value = iter->value;
			auto tag = value.getTag();
			std::string svalue;
			if (tag == gason::JSON_NULL)
				svalue = std::string();
			if (tag == gason::JSON_STRING)
				svalue = std::string(value.toString());
			else if (tag == gason::JSON_NUMBER)
				svalue = Util::cast(value.toInt());
			else {
				ASSERT(false, "Unsupported convertion to string from tag: " << tag);
			}

			indices.push_back(svalue);
		}

		delete[] jsonData;

		if (!indices.size()) {
			frame.noDataUpdate = true;
			return;
		}

		auto* secMaster = dr.getSecurityMaster();
		const StringData* tickerData = dr.stringData(secMaster, m_tickerDataId, true);
		ASSERT(tickerData, "Unable to load data: " << m_tickerDataId);

		const StringData* figiData = dr.stringData(secMaster, m_figiDataId, true);
		ASSERT(figiData, "Unable to load data: " << m_figiDataId);

		/// Now we construct and index of the securities for this particular day ...
		size_t numCols = tickerData->cols();
		StringMap tickerToFigiMap;

		for (size_t ii = 0; ii < numCols; ii++) {
			std::string ticker = tickerData->getValue(dci.di, ii);
			std::string yesterdayTicker = tickerData->getValue(std::max(dci.di - 1, 0), ii);
			std::string figi = figiData->getValue(dci.di, ii);
			const Security* sec = secMaster->mapUuid(figi);
			auto exchange = sec->exchangeStr();
			auto tickerMapIter = m_exchangeTickerMap.find(exchange);
			StringVec usedTickers { ticker };
			tickerToFigiMap[ticker] = figi;

			if (yesterdayTicker != ticker) {
				tickerToFigiMap[yesterdayTicker] = figi;
				usedTickers.push_back(yesterdayTicker);
			}

			if (tickerMapIter != m_exchangeTickerMap.end()) {
				const StringVec& tickerMaps = tickerMapIter->second;

				for (const std::string& tickerMap : tickerMaps) {
					for (const std::string& usedTicker : usedTickers) {
						std::string altTicker = usedTicker;
						auto spos = altTicker.rfind(' ');

						if (spos != std::string::npos) {
							altTicker = altTicker.substr(0, spos + 1);
							altTicker += tickerMap;
							tickerToFigiMap[altTicker] = figi;
						}
					}
				}
			}
		}

		size_t numMapped = 0;
		std::vector<Security::Index> udata;
		for (size_t ii = 0; ii < indices.size(); ii++) {
			const auto& ticker = indices[ii]; 
			auto iter = tickerToFigiMap.find(ticker);

			if (iter == tickerToFigiMap.end()) {
				CInfo("Unable to find ticker [date = %u]: %s", date, ticker);
				//(*data)(0, ii) = Security::s_invalidIndex;
				continue;
			}

			auto figi = iter->second;
			/// Now we look up this figi 
			const Security* sec = secMaster->mapUuid(figi);
			ASSERT(sec && sec->isValid(), "Unable to find figi: " << figi << " in security master. This was mapped from ticker: " << ticker << " on date: " << date);
			udata.push_back(sec->index);
			//(*data)(0, ii) = sec->index;
			BasicUniverseBuilder::addToAlphaIndexMap(dci.di, sec->index);
			numMapped++;
		}

		CInfo("[di = %d - %u]: Index count: %z => %z", dci.di, dci.rt.dates[dci.di], indices.size(), numMapped);

		UIntMatrixDataPtr data = std::make_shared<UIntMatrixData>(secMaster, udata);
		frame.data = data;
		frame.dataOffset = 0;
		frame.dataSize = data->dataSize();
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createIndexUniverse(const pesa::ConfigSection& config) {
	return new pesa::IndexUniverse((const pesa::ConfigSection&)config);
}

