/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// MainUniverse.cpp
///
/// Created on: 7 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"

namespace pesa {

	////////////////////////////////////////////////////////////////////////////
	/// Main Universe
	////////////////////////////////////////////////////////////////////////////
	class MainUniverse : public helper::BasicUniverseBuilder {
	private:
		const UIntMatrixData*	m_data = nullptr; 			/// The underlying universe
		Metadata 				m_metadata;	 				/// The metadata for the underlying universe
		size_t					m_imapCount = 0;			/// IMap count

	public:
								MainUniverse(const ConfigSection& config);
		virtual 				~MainUniverse();

		////////////////////////////////////////////////////////////////////////////
		/// IComponent overrides
		////////////////////////////////////////////////////////////////////////////
//		virtual void 			run(const RuntimeData& rtd);

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);

		////////////////////////////////////////////////////////////////////////////
		/// IUniverse overrides
		////////////////////////////////////////////////////////////////////////////
		virtual Security 		security(IntDay di, Security::Index ii) const;
		virtual const Security* mapUuid(const std::string& uuid) const;
		virtual const UIntMatrixData* universeData() const;

		virtual std::string 	id() const {
			return IComponent::id() + "." + config().getString("name", "<unnamed>");
		}
	};

	MainUniverse::MainUniverse(const ConfigSection& config) 
		: helper::BasicUniverseBuilder(config, config.get("name")) {
	}

	MainUniverse::~MainUniverse() {
		CTrace("Deleting MainUniverse!");
	}

	void MainUniverse::initDatasets(IDataRegistry& dr, Datasets& datasets) {
	}

	void MainUniverse::build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		std::string name = this->name();
		m_data = dr.get<UIntMatrixData>(nullptr, name + ".Universe", false, &m_metadata);
		ASSERT(m_data, "Unable to load universe: " << name << ".Universe");
		BasicUniverseBuilder::build(dr, dci, frame);

		DataInfo dinfo;
		dinfo.noCacheUpdate = true;
		dinfo.rowHeaders = true;

		const UIntMatrixData* imap = dr.get<UIntMatrixData>(nullptr, name + ".IMap", &dinfo);
		const FloatMatrixData* dmap = dr.get<FloatMatrixData>(nullptr, name + ".DMap", &dinfo);

		if (imap && dmap) {
			m_imapCount = imap->cols();
			BasicUniverseBuilder::constructAlphaIndexMap(dci.rt, imap, dmap);
		}
	}

	Security MainUniverse::security(IntDay di, Security::Index ai) const { 
		/// Get it from sec master which does not have a concept of di (but we pass it nonetheless)
		Security::Index ii = index(di, ai);

		/// Return an invalid security
		if (ii == Security::s_invalidIndex)
			return Security();

		auto* secMaster = const_cast<MainUniverse*>(this)->dataRegistry()->getSecurityMaster();
		ASSERT(secMaster, "Unable to load security master from the data registry!");

		return secMaster->security(di, ii);
	}

	const Security* MainUniverse::mapUuid(const std::string& uuid) const {
		if (m_securities.size())
			return helper::BasicUniverseBuilder::mapUuid(uuid);

		auto* self = const_cast<MainUniverse*>(this);
		auto* secMaster = self->dataRegistry()->getSecurityMaster();
		ASSERT(secMaster, "Unable to load security master from the data registry!");

		if (m_imapCount) {
			(const_cast<MainUniverse*>(this)->m_securitiesList).resize(m_imapCount);

			for (auto iter : m_aiMap) {
				Security::Index secId = iter.first;
				size_t ii = iter.second;

				ASSERT(ii < m_securitiesList.size(), "Invalid aiMap index: " << ii << " - Max: " << m_securitiesList.size());

				Security sec = secMaster->security(0, secId);

				self->m_securitiesList[ii] = sec;
				self->mapSecuritySymbol(sec, ii);
				//(const_cast<MainUniverse*>(this))->add(sec);
			}
		}
		else {
			for (auto iter : m_aiMap) {
				Security::Index secId = iter.first;
				Security sec = secMaster->security(0, secId);
				(const_cast<MainUniverse*>(this))->add(sec);
			}
		}

		return helper::BasicUniverseBuilder::mapUuid(uuid);
	}

	const UIntMatrixData* MainUniverse::universeData() const {
		return m_data;
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createMainUniverse(const pesa::ConfigSection& config) {
	return new pesa::MainUniverse((const pesa::ConfigSection&)config);
}

