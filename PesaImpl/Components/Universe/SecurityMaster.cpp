/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// SecurityMaster.cpp
///
/// Created on: 21 Jun 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Security Master
	////////////////////////////////////////////////////////////////////////////
	class SecurityMaster : public helper::BasicUniverseBuilder {
	private:
	protected:
		virtual Security::Index	universeDataIndex(IntDay di, size_t ii) const;

	public:
								SecurityMaster(const ConfigSection& config);
		virtual 				~SecurityMaster();

		////////////////////////////////////////////////////////////////////////////
		/// IComponent overrides
		////////////////////////////////////////////////////////////////////////////
//		virtual void 			run(const RuntimeData& rtd);

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame);
		virtual Security::Index	index(IntDay di, size_t ii) const;
		virtual size_t			size(IntDay di) const;
		virtual Security 		securityInfo(pesa::IntDay di, Security::Index ii);
		bool					isValid(pesa::IntDay di, size_t ii) const;
		virtual size_t			alphaIndex(pesa::IntDay di, size_t ii) { return ii; }

		////////////////////////////////////////////////////////////////////////////
		/// IUniverse overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			combine(FloatMatrixData& thisAlpha, IUniversePtr other, FloatMatrixData& otherAlpha) {}
	};

	SecurityMaster::SecurityMaster(const ConfigSection& config) : helper::BasicUniverseBuilder(config, "SEC_MASTER") {
	}

	SecurityMaster::~SecurityMaster() {
		CTrace("Deleting SecurityMaster!");
	}

	bool SecurityMaster::isValid(pesa::IntDay di, size_t ii) const {
		return true;
	}

	void SecurityMaster::initDatasets(IDataRegistry& dr, Datasets& datasets) {
	}

	size_t SecurityMaster::size(IntDay) const {
		return m_securitiesList.size();
	}

	Security SecurityMaster::securityInfo(pesa::IntDay di, Security::Index ii) {
		IDataRegistry& dr 				= *dataRegistry();
		Security sec					= security(di, ii);
		DataInfo dinfo;
		dinfo.noCacheUpdate				= true;
		dinfo.noAssertOnMissingDataLoader= true; 
		//const StringData* figi 			= dynamic_cast<const StringData*>(dr.getData(this, "secData.figi", true));
		//const StringData* name 			= dynamic_cast<const StringData*>(dr.getData(this, "secData.name", true));
		//const StringData* cusip 		= dynamic_cast<const StringData*>(dr.getData(this, "secData.cusip", true));
		//const StringData* sedol 		= dynamic_cast<const StringData*>(dr.getData(this, "secData.sedol", true));
		//const StringData* ticker 		= dynamic_cast<const StringData*>(dr.getData(this, "secData.ticker", true));
		//const StringData* issueDesc 	= dynamic_cast<const StringData*>(dr.getData(this, "secData.issueDesc", true));
		//const StringData* issueType 	= dynamic_cast<const StringData*>(dr.getData(this, "secData.issueType", true));
		//const StringData* issueClass 	= dynamic_cast<const StringData*>(dr.getData(this, "secData.issueClass", true));
		//const StringData* currency 		= dynamic_cast<const StringData*>(dr.getData(this, "secData.currency", true));
		//const StringData* exchange 		= dynamic_cast<const StringData*>(dr.getData(this, "secData.exchange", true));
		//const StringData* countryCode2 	= dynamic_cast<const StringData*>(dr.getData(this, "secData.countryCode2", true));
		//const StringData* countryCode3 	= dynamic_cast<const StringData*>(dr.getData(this, "secData.countryCode3", true));
		//const StringData* parentUuid 	= dynamic_cast<const StringData*>(dr.getData(this, "secData.parentUuid", true));
		const StringData* tickerId		= dr.get<StringData>(this, "secData.tickerId", &dinfo);

		if (!tickerId)
			return sec; 

		sec.setTicker(tickerId->getValue(di, ii));

		//const StringData* bbTicker		= dynamic_cast<const StringData*>(bbTickerPtr.get());

		//sec.setUuid(figi->getValue(di, ii));
		//sec.setName(name->getValue(di, ii));
		//sec.setCusip(cusip->getValue(di, ii));
		//sec.setSedol(sedol->getValue(di, ii));
		//sec.setTicker(ticker->getValue(di, ii));
		//sec.setIssueDesc(issueDesc->getValue(di, ii));
		//sec.setIssueType(issueType->getValue(di, ii));
		//sec.setIssueClass(issueClass->getValue(di, ii));
		//sec.setCurrency(currency->getValue(di, ii));
		//sec.setExchange(exchange->getValue(di, ii));
		//sec.setCountryCode2(countryCode2->getValue(di, ii));
		//sec.setCountryCode3(countryCode3->getValue(di, ii));
		//sec.setParentUuid(parentUuid->getValue(di, ii));
		//sec.setBBTicker(bbTicker->getValue(di, ii));

		return sec;
	}

	Security::Index	SecurityMaster::universeDataIndex(IntDay, size_t ii) const {
		ASSERT(ii >= 0 && ii < m_securitiesList.size(), "Invalid index: " << ii << " - Securities List Size: " << m_securitiesList.size());
		return m_securitiesList[ii].index;
	}

	Security::Index	SecurityMaster::index(IntDay di, size_t ii) const {
		return universeDataIndex(di, ii);
	}

	void SecurityMaster::build(IDataRegistry& dr, const pesa::DataCacheInfo& dci, pesa::DataFrame& frame) {
		/// We do not want a cache update at this stage ... caching should've been done by now!
		std::string name = config().getRequired("name");
		const SecurityData* securitiesPtr = dr.get<SecurityData>(nullptr, name + ".Master", true);

		/// If we're dumping something, then just ignore
		if (!securitiesPtr && !dci.rt.appOptions.dumpData.empty())
			return;

		ASSERT(securitiesPtr, "Unable to get data: " << name << ".Master!");

		const SecurityVecPtr securities = securitiesPtr->vector();
		BasicUniverseBuilder::buildSecurities(securities);
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createSecurityMaster(const pesa::ConfigSection& config) {
	return new pesa::SecurityMaster((const pesa::ConfigSection&)config);
}

