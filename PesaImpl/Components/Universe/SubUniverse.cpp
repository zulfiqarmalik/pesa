/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// SubUniverse.cpp
///
/// Created on: 20 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Framework/Helper/FileDataLoader.h"
#include "Poco/String.h"
#include <stdio.h>
#include <ctype.h>

namespace pesa {
	class SubUniverse;
	using namespace helper;

	//////////////////////////////////////////////////////////////////////////
	/// Conditions for universe construction
	class UniverseCondition {
	public:
		virtual bool 			check(const RuntimeData& rt, IDataRegistry& dr, IntDay di, Security::Index ii, const DataCacheInfo* dci) = 0;  
		virtual std::string		desc() const = 0;
		virtual bool 			preCheck(const RuntimeData& rt, IDataRegistry& dr, IntDay di, Security::Index ii, const DataCacheInfo* dci) { return true; }
		virtual void			reset() {}
		virtual bool			needsPreCheck() const { return false; }
	};

	typedef std::shared_ptr<UniverseCondition> UniverseConditionPtr;

	//////////////////////////////////////////////////////////////////////////
	class ApprovedCountry : public UniverseCondition {
	private:
		typedef std::unordered_map<std::string, std::string> StrStrMap;
		std::string				m_secCountryNamesId;
		std::string				m_secSymbolsId;
		std::string				m_approvedCountriesId;
		std::string				m_parentCountryExceptionsId;
		std::string				m_excludedStocksId;

		//////////////////////////////////////////////////////////////////////////
		StrStrMap				m_approvedCountries;
		StrStrMap				m_exceptions;
		StrStrMap				m_excluded;

	public:
		ApprovedCountry(const std::string& secCountryNamesId, const std::string& secSymbolsId, const std::string& approvedCountriesId, 
			const std::string& parentCountryExceptionsId, const std::string& excludedStocksId)
			: m_secCountryNamesId(secCountryNamesId)
			, m_secSymbolsId(secSymbolsId)
			, m_approvedCountriesId(approvedCountriesId)
			, m_parentCountryExceptionsId(parentCountryExceptionsId)
			, m_excludedStocksId(excludedStocksId) {
		}

		std::string				desc() const {
			return "ApprovedCountry";
		}

		bool					isExcluded(const RuntimeData& rt, IDataRegistry& dr, IntDay di, Security::Index ii, const Security& sec, const std::string& symbol) {
			auto exIter = m_excluded.find(symbol);
			if (exIter != m_excluded.end()) {
				Debug("SUB_UNIVERSE", "Excluded --: %8s [FIGI: %s] --", symbol, sec.uuidStr());
				return true;
			}

			return false;
		}

		bool					isIncluded(const RuntimeData& rt, IDataRegistry& dr, IntDay di, Security::Index ii, const Security& sec, const std::string& symbol) {
			auto excIter = m_exceptions.find(symbol);
			if (excIter != m_exceptions.end()) {
				Debug("SUB_UNIVERSE", "Included ++: %8s [FIGI: %s] ++", symbol, sec.uuidStr());
				return true;
			}

			return false;
		}

		virtual bool			check(const RuntimeData& rt, IDataRegistry& dr, IntDay di, Security::Index ii, const DataCacheInfo* dci) {
			IUniverse* secMaster = dr.getSecurityMaster();
			StringData* countryNames = dr.stringData(secMaster, m_secCountryNamesId);
			ASSERT(countryNames, "Unable to load securities country names data: " << m_secCountryNamesId);

			StringData* symbols = dr.stringData(secMaster, m_secSymbolsId);
			ASSERT(symbols, "Unable to load securities symbol data: " << m_secSymbolsId);

			if (m_approvedCountries.empty()) {
				StringData* approvedCountries = dr.stringData(secMaster, m_approvedCountriesId);
				ASSERT(approvedCountries, "Unable to load approved countries data: " << m_approvedCountriesId);
				size_t numCols = approvedCountries->cols();

				for (size_t ii = 0; ii < numCols; ii++) {
					std::string approvedCountry = Poco::toUpper(approvedCountries->getValue(0, ii));
					m_approvedCountries[approvedCountry] = "";
				}
			}

			if (m_exceptions.empty()) {
				StringData* exceptions = dr.stringData(secMaster, m_parentCountryExceptionsId);
				ASSERT(exceptions, "Unable to load exceptions data: " << m_parentCountryExceptionsId);
				size_t numCols = exceptions->cols();

				for (size_t ii = 0; ii < numCols; ii++) {
					std::string except = Poco::toUpper(exceptions->getValue(0, ii));
					except = Poco::replace(except, "\"", "");
					m_exceptions[except] = except;
				}
			}

			if (m_excluded.empty()) {
				StringData* excluded = dr.stringData(secMaster, m_excludedStocksId);
				ASSERT(excluded, "Unable to load exceptions data: " << m_excludedStocksId);
				size_t numCols = excluded->cols();

				for (size_t ii = 0; ii < numCols; ii++) {
					std::string ex = Poco::toUpper(excluded->getValue(0, ii));
					ex = Poco::replace(ex, "\"", "");
					m_excluded[ex] = ex;
				}
			}

			Security sec = secMaster->security(di, ii);

			/// Check whether the couexcludedStocksntry is an approved country
			std::string countryName = countryNames->getValue(0, ii);
			std::string symbol = Poco::toUpper(symbols->getValue(0, ii));

			auto appIter = m_approvedCountries.find(countryName);
			if (!countryName.empty() && appIter != m_approvedCountries.end())
				return true;

			/// If we get to here then the country is not approved. In this case we just 
			/// need to make sure that its in the exceptions list

			if (isIncluded(rt, dr, di, ii, sec, symbol))
				return true;

			/// Is it explicitly excluded from the universe ...
			if (isExcluded(rt, dr, di, ii, sec, symbol))
				return false;

			/// We get here we include the stock. Even if its empty ...
			if (countryName.empty())
				return true;

			return false;
		}
	};

	//////////////////////////////////////////////////////////////////////////
	class SectorCondition : public UniverseCondition {
	private:
		StringMap				m_targetSectorMap;
		std::string				m_targetSector;
		std::string				m_sectorDataId = "classification.bbSector";

	public:
								SectorCondition(const std::string& targetSector, const std::string& sectorDataId)
									: m_targetSector(targetSector)
									, m_sectorDataId(sectorDataId) {
									auto targetSectors = Util::split(targetSector, "|");
									for (const auto& targetSector : targetSectors)
										m_targetSectorMap[targetSector] = "";
								}

		virtual bool			check(const RuntimeData& rt, IDataRegistry& dr, IntDay di, Security::Index ii, const DataCacheInfo* dci) {
			IUniverse* secMaster = dr.getSecurityMaster();
			DataPtr data = dr.getDataPtr(secMaster, m_sectorDataId);
			ASSERT(data, "Unable to load data: " << m_sectorDataId);
			StringData* sdata = dynamic_cast<StringData*>(data.get());

			const auto& sector = sdata->getValue(0, ii);

			auto iter = m_targetSectorMap.find(sector);
			if (iter == m_targetSectorMap.end())
				return false;

			return true;
		}

		std::string				desc() const {
			return Poco::format("[%s] SectorCondition - not part of sector(s): %s", m_sectorDataId, m_targetSector);
		}

	};

	//////////////////////////////////////////////////////////////////////////
	class SectorFilterCondition : public UniverseCondition {
	private:
		StringMap				m_targetSectorMap;
		std::string				m_sectorDataId = "secData.sector";
		bool					m_exclude = true;

	public:
								SectorFilterCondition(const std::string& targetSector, const std::string& sectorDataId, bool exclude)
									: m_sectorDataId(sectorDataId)
									, m_exclude(exclude) {
									auto targetSectors = Util::split(targetSector, "|");
									for (const auto& targetSector : targetSectors) 
										m_targetSectorMap[targetSector] = "";
								}

		virtual bool			check(const RuntimeData& rt, IDataRegistry& dr, IntDay di, Security::Index ii, const DataCacheInfo* dci) {
			IUniverse* secMaster = dr.getSecurityMaster();
			DataPtr data = dr.getDataPtr(secMaster, m_sectorDataId);
			ASSERT(data, "Unable to load data: " << m_sectorDataId);
			StringData* sdata = dynamic_cast<StringData*>(data.get());

			const auto& sector = sdata->getValue(0, ii);

			/// If there is no sector then we just ignore that security!
			if (sector.empty())
				return false;

			auto iter = m_targetSectorMap.find(sector);

			if (m_exclude) {
				if (iter != m_targetSectorMap.end())
					return false;
			}
			else {
				if (iter == m_targetSectorMap.end())
					return false;
			}

			return true;
		}

		std::string desc() const {
			return "SectorFilterCondition";
		}
	};

	//////////////////////////////////////////////////////////////////////////
	class IssueClassFilter : public UniverseCondition {
	private:
		StringVec				m_issueClasses;			/// The issue classes that we use

	public:
								IssueClassFilter(const StringVec& issuesClasses)
									: m_issueClasses(issuesClasses) {}

		virtual bool			check(const RuntimeData& rt, IDataRegistry& dr, IntDay di, Security::Index ii, const DataCacheInfo* dci) {
			IUniverse* secMaster = dr.getSecurityMaster();
			Security sec = secMaster->security(di, ii);
			std::string secIssueClass = sec.bbSecurityTypeStr();

			if (secIssueClass.empty())
				return false;

			for (const auto& issueClass : m_issueClasses) {
				if (secIssueClass == issueClass)
					return true;
			}

			return false;
		}

		std::string desc() const {
			return "IssueClassFilter";
		}
	};

	//////////////////////////////////////////////////////////////////////////
	class DuplicateFilter : public UniverseCondition {
	private:
		struct DupInfo {
			Security::Index		secId = Security::s_invalidIndex;
			size_t				index = 0;
			std::string			issueClass;

			DupInfo() {}
			DupInfo(Security::Index secId_, size_t index_, const std::string& issueClass_) : secId(secId_), index(index_), issueClass(issueClass_) {}
		};

		typedef std::map<Security::Index, bool> DiscardMap;
		typedef std::unordered_map<std::string, DupInfo> SecDupInfoMap;

		DiscardMap				m_discardMap;				/// Securities that have been discarded based on duplicate logic
		SecDupInfoMap			m_dupMap;					/// Duplicate security map
		std::string				m_volumeDataId;
		std::string				m_priceDataId;

	public:
								DuplicateFilter(const std::string& volumeDataId, const std::string& priceDataId) 
									: m_volumeDataId(volumeDataId)
									, m_priceDataId(priceDataId) {
								}

		virtual void			reset() {
			/// We reset this for each iteration ...
			m_dupMap.clear();
			m_discardMap.clear();
		}

		std::string desc() const {
			return "DuplicateFilter";
		}

		static size_t			matchIssueClass(const std::string& lhs, const std::string& rhs, std::string& lhsIssueType, std::string& rhsIssueType) {
			/// If the strings have nothing in common then we don't even attempt to match ...
			if (lhs.length() != rhs.length()) {
				lhsIssueType = lhs;
				rhsIssueType = rhs;
				return std::max(lhs.length(), rhs.length());
			}

			size_t diffCount = 0;
			for (size_t i = 0; i < lhs.length(); i++) {
				char lc = lhs[i];
				char rc = rhs[i];

				if (lc != rc) {
					lhsIssueType.append(1, lc);
					rhsIssueType.append(1, rc);
					diffCount++;
				}
			}

			return diffCount;
		}

		static bool				alphaNum(char c) {
			return ::isalnum(c) ? true : false;
		}

		static size_t			matchBBTicker(const std::string& lhs_, const std::string& rhs_) { 
			std::string lhs = lhs_;
			std::string rhs = rhs_;

			lhs.erase(std::remove_if(lhs.begin(), lhs.end(), alphaNum), lhs.end());
			rhs.erase(std::remove_if(rhs.begin(), rhs.end(), alphaNum), rhs.end());

			Poco::removeInPlace(lhs, '/');
			Poco::removeInPlace(rhs, '/');

			auto lhsLength = lhs.length();
			auto rhsLength = rhs.length();

			size_t diffCount = (size_t)std::abs((int)lhsLength - (int)rhsLength);
			auto iterLen = std::min(lhsLength, rhsLength);
			for (size_t i = 0; i < iterLen; i++) {
				char lc = lhs[i];
				char rc = rhs[i];

				if (lc != rc) 
					diffCount++;
			}


			return diffCount;
		}

		virtual bool			needsPreCheck() const { return true; }

		virtual bool			check(const RuntimeData& rt, IDataRegistry& dr, IntDay di, Security::Index ii, const DataCacheInfo* dci) {
			IUniverse* secMaster = dr.getSecurityMaster();
			auto iter = m_discardMap.find(ii); 
			auto sec = secMaster->security(di, ii);

			if (iter == m_discardMap.end() || iter->second == false)
				return true;

			return false;
		}

		virtual bool			preCheck(const RuntimeData& rt, IDataRegistry& dr, IntDay di, Security::Index ii, const DataCacheInfo* dci) {
			IUniverse* secMaster = dr.getSecurityMaster();
			//auto sec = secMaster->security(di, ii);
			auto sec = secMaster->securityInfo(di, ii);
			auto parentUuid = sec.parentUuidStr();

			if (parentUuid.empty())
				return true;

			auto secId = ii;
			auto issueClass = sec.issueDescStr();
			auto iter = m_dupMap.find(parentUuid);

			if (iter != m_dupMap.end()) {
				auto discardIter = m_discardMap.find(secId);
				bool passThrough = false;

				if (discardIter != m_discardMap.end()) {
					if (discardIter->second == true) {
						//CDebug("[%s - %s] Listing has been marked to be superseded by another listing from the same company", sec.nameStr(), issueClass);
						return false;
					}
					else if (discardIter->second == false)
						return true;
				}

				DupInfo& dupInfo = iter->second;
				std::string currIssueType, newIssueType;
				//auto existingSec = secMaster->security(di, dupInfo.secId);
				auto existingSec = secMaster->securityInfo(di, dupInfo.secId);

				size_t issueDescDiffCount = DuplicateFilter::matchIssueClass(dupInfo.issueClass, issueClass, currIssueType, newIssueType);
				size_t tickerDiffCount = DuplicateFilter::matchBBTicker(existingSec.bbTickerStr(), sec.bbTickerStr());

				/// We are expecting that there should not be more than 1 character difference (the issueType) 
				/// in this particular case. If there is more than 1 then the securities are probably too 
				/// different and we count them as separate securities!
				if (issueDescDiffCount > 1 && tickerDiffCount > 1) {
					Debug("SUB_UNIVERSE", "[%s] - Issue classes are too different: %s / %s - TREATING AS DIFFERENT SECURITIES", sec.nameStr(), dupInfo.issueClass, issueClass);
					m_discardMap[dupInfo.secId] = false;
					m_discardMap[secId] = false;
					return true;
				}

				const FloatMatrixData* volumeData = dr.floatData(secMaster, m_volumeDataId);
				const FloatMatrixData* priceData = dr.floatData(secMaster, m_priceDataId);

				ASSERT(volumeData, "Unable to load volume data: " << m_volumeDataId);
				ASSERT(priceData, "Unable to load price data: " << m_priceDataId);

				bool chooseNewSec = false;

				float currVolume = (*volumeData)(di, dupInfo.secId);
				float newVolume = (*volumeData)(di, secId);

				if (std::isnan(newVolume) || newVolume < 0.0001f)
					return false;

				float volumeDiff = (newVolume - currVolume) / currVolume;

				/// If the differential of the volume is more than 50% of the existing volume then 
				/// we just choose 
				if (volumeDiff > 0.5f) /// If the new security has a 50%+ volunme advantage we CHOOSE IT
					chooseNewSec = true;
				else if (volumeDiff < -0.5f)  /// If the new security has a 50%+ volunme advantage we DISCARD IT
					chooseNewSec = false;
				else {
					float currPrice = (*priceData)(di, dupInfo.secId);
					float newPrice = (*priceData)(di, secId);

					if (std::isnan(newPrice) || newPrice < 0.0001f)
						chooseNewSec = false;
					/// This is slightly complicated since the volumes are in a very close range (GOOG/GOOGL for instance)
					/// In this particular case we just select the security with the lower price
					else if (newPrice < currPrice)
						chooseNewSec = true;

					//if (newPrice < currPrice)
					//	chooseNewSec = true;
				}

				/// Check whether the new security has the preferred issue type ...
				if (chooseNewSec) {
					Trace("SUB_UNIVERSE", "[%-30s] - [%s - '%s'] => [%s - '%s']", sec.nameStr(), existingSec.bbTickerStr(), existingSec.issueDescStr(), 
						sec.bbTickerStr(), sec.issueDescStr());

					m_discardMap[dupInfo.secId] = true;
					m_discardMap[secId] = false;

					/// Replace the information with the new information!
					dupInfo.issueClass = issueClass;
					dupInfo.secId = secId;
				}
				else {
					m_discardMap[secId] = true;
					m_discardMap[dupInfo.secId] = false;
					Trace("SUB_UNIVERSE", "[%-30s] - [%s - '%s'] <= [%s - '%s']", sec.nameStr(), existingSec.bbTickerStr(), existingSec.issueDescStr(),
						sec.bbTickerStr(), sec.issueDescStr());
					return false;
				}
			}

			DupInfo dupInfo(sec.index, ii, sec.issueDescStr());
			m_dupMap[parentUuid] = dupInfo;

			return true;
		}
	};

	//////////////////////////////////////////////////////////////////////////
	class RestrictedListFilter : public UniverseCondition {
	private:
		std::string				m_startTimeId;			/// The id of the starting time data
		std::string				m_endTimeId;			/// The id of the ending time data
		const Int64MatrixData*	m_startTime = nullptr;	/// The starting time
		const Int64MatrixData*	m_endTime = nullptr;	/// The ending time

	public:
								RestrictedListFilter(const std::string& startTime, const std::string& endTime)
									: m_startTimeId(startTime), m_endTimeId(endTime) {}

		virtual bool			check(const RuntimeData& rt, IDataRegistry& dr, IntDay di, Security::Index ii, const DataCacheInfo* dci) {
			IUniverse* secMaster = dr.getSecurityMaster();

			if (!m_startTime)
				m_startTime = dr.get<Int64MatrixData>(secMaster, m_startTimeId, true);

			if (!m_endTime)
				m_endTime = dr.get<Int64MatrixData>(secMaster, m_endTimeId, true);

			Poco::DateTime startTime = m_startTime->getDTValue(di, ii);
			Poco::DateTime endTime = m_endTime->getDTValue(di, ii);

			IntDate startDate = Util::dateToInt(startTime);
			IntDate endDate = Util::dateToInt(endTime);
			IntDate currDate = rt.dates[di];

			if (startDate <= currDate && endDate >= currDate)
				return false; 

			return true;
		}

		std::string desc() const {
			return "RestrictedListFilter";
		}
	};

	//////////////////////////////////////////////////////////////////////////
	class RestrictedSecurities : public UniverseCondition {
	private:
		std::unordered_map<std::string, size_t> m_restrictedSecs;		/// Restricted securities
		std::string				m_dataId;				/// The data id
		bool					m_dataHandled = false;	/// Has the data been handled or not

	public:
		RestrictedSecurities(const std::string& dataId)
			: m_dataId(dataId) {}

		virtual bool 			preCheck(const RuntimeData& rt, IDataRegistry& dr, IntDay di, Security::Index, const DataCacheInfo* dci) {
			if (m_dataHandled)
				return true;

			IntDate date = rt.dates[dci->di];
			Debug("SUB_UNIVERSE", "[%u] - Building restricted securities from: %s", date, m_dataId);
			auto dataIds = Util::split(m_dataId, "|");
			auto* secMaster = dr.getSecurityMaster();

			for (const auto& dataId : dataIds) {
				const FloatMatrixData* data = dr.floatData(secMaster, dataId);
				size_t numCols = data->cols();
				di = dci->di - data->delay();

				for (size_t ci = 0; ci < numCols; ci++) {
					float fvalue = 0.0f;

					if (!data->def().isStatic())
						fvalue = (*data)(di, ci);
					else
						fvalue = (*data)(0, ci);

					if (std::abs(fvalue - 1.0f) < 0.0001f) {
						auto sec = secMaster->security(di, (Security::Index)ci);
						std::string uuid = Poco::trim(sec.uuidStr());
						if (!uuid.empty()) {
							m_restrictedSecs[uuid] = 1;
						}
					}
				}
			}

			Debug("SUB_UNIVERSE", "[%u] - Number of restricted securities: %z", date, m_restrictedSecs.size());

			m_dataHandled = true;

			return true;
		}
		virtual void			reset() {
			m_restrictedSecs.clear();
			m_dataHandled = false;
		}

		virtual bool			needsPreCheck() const { return true; }

		virtual bool check(const RuntimeData& rt, IDataRegistry& dr, IntDay di, Security::Index ii, const DataCacheInfo* dci) {
			IUniverse* secMaster = dr.getSecurityMaster();

			auto sec = secMaster->security(di, ii);
			auto uuid = sec.uuidStr();
			auto iter = m_restrictedSecs.find(uuid);

			if (iter != m_restrictedSecs.end()) {
				Info("SUB_UNIVERSE", "Restricted security: %s [%s - %s]", uuid, sec.tickerStr(), sec.nameStr());
				return false;
			}

			return true;
		}

		std::string desc() const {
			return "RestrictedSecurities";
		}
	};

	////////////////////////////////////////////////////////////////////////// 
	class SubtractFilter : public UniverseCondition {
	private:
		std::string				m_filterDataIds;			/// The data id that we use for filteration
		std::vector
		<const IntMatrixData*>	m_filters;					/// The matrices that will be used for filteration

	public:
								SubtractFilter(const std::string& filterDataId)
									: m_filterDataIds(filterDataId) {}

		virtual bool			check(const RuntimeData& rt, IDataRegistry& dr, IntDay di, Security::Index ii, const DataCacheInfo* dci) {
			IUniverse* secMaster = dr.getSecurityMaster();
			auto sec = secMaster->security(dci->di, ii);

			if (!m_filters.size()) {
				StringVec filterIds = Util::split(m_filterDataIds, "|");

				for (auto& filterId : filterIds) {
					const auto* filter = dr.get<IntMatrixData>(secMaster, filterId, true);
					ASSERT(filter, "Unable to load data: " << filterId);
					m_filters.push_back(filter);
				}
			}

			for (const IntMatrixData* filter : m_filters) {
				int value = (*filter)(dci->di, ii);
				if (value == 0) 
					return false;
			}

			return true;
		}

		std::string desc() const {
			return "SubtractFilter";
		}
	};

	////////////////////////////////////////////////////////////////////////// 
	class MarketStatusFilter : public UniverseCondition {
	private:
		std::string				m_marketStatusDataId = "secData.marketStatus";

	public:
								MarketStatusFilter(const std::string& marketStatusDataId)
									: m_marketStatusDataId(marketStatusDataId) {}

		virtual bool			check(const RuntimeData& rt, IDataRegistry& dr, IntDay di, Security::Index ii, const DataCacheInfo* dci) {
			if (dci->di != dci->rt.diEnd)
				return true;

			IUniverse* secMaster = dr.getSecurityMaster();
			auto sec = secMaster->security(dci->di, ii);
			const StringData* marketStatus = dr.stringData(secMaster, m_marketStatusDataId);

			ASSERT(marketStatus, "Unable to load market status data: " << m_marketStatusDataId);

			std::string securityStatus = marketStatus->getValue(0, ii);
			if (securityStatus != "ACTV") {
				Debug("Security not ACTV: %s [%s, %s]", sec.uuidStr(), sec.tickerStr(), sec.nameStr());
				return false;
			}

			return true;
		}

		std::string desc() const {
			return "MarketStatusFilter";
		}
	};

	////////////////////////////////////////////////////////////////////////// 
	class AddFilter : public UniverseCondition {
	private:
		std::string				m_uuidsStr;				/// The list of FIGIs as a single string
		std::unordered_map<std::string, Security::Index> m_uuidMap;

	public:
								AddFilter(const std::string& figiList) : m_uuidsStr(figiList) {}

		void					loadFigiList(const RuntimeData& rt, IDataRegistry& dr, const std::string& filename) {
			helper::FileDataLoader fdl(filename);
			StringVec info;
			StringVec figis;

			do {
				info = fdl.read();
				if (info.size()) {
					figis.push_back(info[0]);
				}
			} while (info.size());

			buildFigiMap(rt, dr, figis);
		}

		void					buildFigiMap(const RuntimeData& rt, IDataRegistry& dr, const StringVec& figis) {
			IUniverse* secMaster = dr.getSecurityMaster();

			for (const std::string& figi : figis) {
				const Security* sec = secMaster->mapUuid(figi);

				if (sec) {
					m_uuidMap[figi] = sec->index;
					Info("SUB_UNIVERSE", "Got security: %u - %s => %s (%s-%s)", sec->index, sec->nameStr(), sec->symbolStr(), sec->tickerStr(), sec->exchangeStr());
				}
				else {
					Warning("SUB_UNIVERSE", "Unable to find figi: %s", figi);
				}
			}
		}

		virtual bool			check(const RuntimeData& rt, IDataRegistry& dr, IntDay di, Security::Index ii, const DataCacheInfo* dci) {
			IUniverse* secMaster = dr.getSecurityMaster();

			if (m_uuidMap.empty()) {
				if (m_uuidsStr[0] != '@') 
					buildFigiMap(rt, dr, Util::split(m_uuidsStr, ","));
				else
					loadFigiList(rt, dr, dr.parentConfig()->defs().resolve(m_uuidsStr.substr(1, m_uuidsStr.length() - 1), nullptr));
			}

			auto sec = secMaster->security(di, ii);
			auto iter = m_uuidMap.find(sec.uuidStr());

			if (iter == m_uuidMap.end())
				iter = m_uuidMap.find(sec.tickerStr());

			if (iter == m_uuidMap.end())
				iter = m_uuidMap.find(sec.instrumentId());

			if (iter == m_uuidMap.end() || iter->second != ii)
				return false;

			return true;
		}

		std::string desc() const {
			return "AddFilter";
		}
	};

	////////////////////////////////////////////////////////////////////////// 
	class ParentUniverseFilter : public UniverseCondition {
	private:
		std::vector<IUniverse*>	m_srcUniverses;
		std::string				m_strUniverses;

	public:
								ParentUniverseFilter(const std::vector<IUniverse*>& srcUniverses) : m_srcUniverses(srcUniverses) {
									m_strUniverses = m_srcUniverses.size() ? m_srcUniverses[0]->name() : "";
									for (size_t i = 1; i < m_srcUniverses.size(); i++) 
										m_strUniverses += ", " + m_srcUniverses[i]->name();
								}

		virtual bool			check(const RuntimeData& rt, IDataRegistry& dr, IntDay, Security::Index iiGlobal, const DataCacheInfo* dci) {
			if (!m_srcUniverses.size())
				return true;

			auto* secMaster = dr.getSecurityMaster();

			for (size_t uid = 0; uid < m_srcUniverses.size(); uid++) {
				IUniverse* srcUniverse = m_srcUniverses[uid];

				/// Map from a global security index to a local one
				auto iiSrc = srcUniverse->alphaIndex(dci->di, iiGlobal);
				auto remappedGlobal = srcUniverse->index(dci->di, iiSrc);

				/// Just sanity check that the mapping back is fine as wellU
				ASSERT(remappedGlobal == iiGlobal, "Invalid local index returned: " << iiSrc << " for global index: " << iiGlobal << " (reverse mapping: " << remappedGlobal << ")");
				if (srcUniverse->isValid(dci->di, iiSrc))
					return true;
			}

			return false;
		}

		std::string desc() const {
			return Poco::format("ParentUniverseFilter: %s", m_strUniverses);
		}
	};

	//////////////////////////////////////////////////////////////////////////

	class FloatCondition : public UniverseCondition {
	protected:
		float					m_cmpValue;			/// Value to compare against
		std::string				m_dataId;			/// What data id should we use?
		IntDay					m_stickyDays = 0;	/// In case we need averaged value over the last N days
		IntDay					m_passThreshold = 0;/// What is the threshold of passing. By default its the same as the sticky days
		const FloatMatrixData*	m_data = nullptr;	/// What data are we going to use?

		virtual bool			check(float value, float cmpValue) = 0;
		virtual std::string		conditionDesc() const = 0;

	public:
								FloatCondition(float cmpValue, const std::string& dataId, IntDay stickyDays = 0, IntDay passThreshold = 0, bool immediateDiscardInvalid = true)
									: m_cmpValue(cmpValue), m_dataId(dataId), m_stickyDays(stickyDays), m_passThreshold(passThreshold) {
									/// If no threshold is specified, we just assume the same as sticky days
									if (m_passThreshold == 0)
										m_passThreshold = m_stickyDays;

									ASSERT(m_passThreshold <= m_stickyDays, "Passing threshold more than the total number of days to check. Total days to check: " << 
										m_stickyDays << " - Pass threshold: " << m_passThreshold);
								}

		void printInvalidSec(IntDate date, const Security& sec) {
			//Info("COND", "[%u] - %s - %s, %s, %s", date, m_dataId, sec.nameStr(), sec.uuidStr(), sec.bbTickerStr());
		}

		virtual bool			check(const RuntimeData& rt, IDataRegistry& dr, IntDay di, Security::Index ii, const DataCacheInfo* dci) {
			IUniverse* secMaster = dr.getSecurityMaster();
			if (!m_data)
				m_data = dr.get<FloatMatrixData>(secMaster, m_dataId, true);

			const FloatMatrixData* data = m_data;

			if (di >= data->rows())
				return false;

			ASSERT(data, "Unable to retrieve data: " << m_dataId);

			const Security& sec = secMaster->security(di, ii);

			IntDate date = rt.dates[di];
			float value = (*data)(di, ii);
			float prevValue = (*data)(std::max(di - 1, 0), ii);
			float nextValue = (*data)(std::min(di + 1, (IntDay)data->rows() - 1), ii);

			/// If there is no data available, we kind of assume that the stock has been delisted
			if (std::isnan(value)) {
				printInvalidSec(rt.dates[di], sec);
				return false;
			}

			bool passed = check(value, m_cmpValue);

			/// If no sticky days is specified then we just make the single test and that is for today 
			if (m_stickyDays == 0)
				return passed;

			/// if sticky days are specified then all the days need to pass the condition
			IntDay days = std::min(di, m_stickyDays); /// We don't wanna go past the first available date now do we?
			IntDay minPassDays = std::min(di, m_passThreshold);
			IntDay numPassed = passed ? 1 : 0;

			for (IntDay ddi = di - days + 1; ddi < di; ddi++) { /// We've already done the check for di
				float ddiValue = (*data)(ddi, ii);

				/// If value on any of the days is NaN then we just fail the security!
				if (std::isnan(ddiValue)) {
					printInvalidSec(rt.dates[ddi], sec);
					return false;
				}

				//if (!std::isnan(ddiValue)) {
				//	value += ddiValue;
				//	numValidDays++;
				//}
				passed = check(ddiValue, m_cmpValue);
				if (passed) 
					numPassed++;
			}

			/// It has passed the condidion on every day 
			bool didPass = numPassed >= minPassDays;
			return didPass;
		}

		virtual std::string desc() const {
			return Poco::format("[%s] Data: %s - CmpValue: %0.2hf [Sticky Days: %d / Pass thershold: %d", conditionDesc(), m_dataId, m_cmpValue, m_stickyDays, m_passThreshold);
		}
	};

	class GTCondition : public FloatCondition {
	protected:
		virtual bool			check(float value, float cmpValue) { return value > cmpValue; }
		virtual std::string		conditionDesc() const { return "GTCondition"; }

	public:
		using					FloatCondition::FloatCondition;
	};

	class GTECondition : public FloatCondition {
	protected:
		virtual bool			check(float value, float cmpValue) { return value >= cmpValue; }
		virtual std::string		conditionDesc() const { return "GTECondition"; }

	public:
		using					FloatCondition::FloatCondition;
	};

	class LTCondition : public FloatCondition {
	protected:
		virtual bool			check(float value, float cmpValue) { return value < cmpValue; }
		virtual std::string		conditionDesc() const { return "LTCondition"; }

	public:
		using					FloatCondition::FloatCondition;
	};

	class LTECondition : public FloatCondition {
	protected:
		virtual bool			check(float value, float cmpValue) { return value <= cmpValue; }
		virtual std::string		conditionDesc() const { return "LTECondition"; }

	public:
		using					FloatCondition::FloatCondition;
	};

	//////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////
	/// SubUniverse - Used to construct smaller sized universes based on a 
	/// certain number of conditions
	////////////////////////////////////////////////////////////////////////////
	class SubUniverse : public helper::BasicUniverseBuilder {
	private:

		typedef std::vector<UniverseConditionPtr> UniverseConditionPtrVec;
		typedef std::unordered_map<Security::Index, size_t> SecMap;

		UniverseConditionPtrVec m_conditions;				/// The conditions of this universe
		size_t					m_limit;					/// Limit of the universe
		std::string				m_sortBy;					/// What to finally sort it by

		IntDay					m_delay = 1;				/// The delay

		bool					m_allowDuplicates = true;	/// Whether to allow duplicates within the universe or not
		bool					m_cheapest = false;			/// Prefer the cheapest in case of duplicates
		std::string				m_priceDataId = Constants::s_defClosePrice; /// What ID to use in case of checking for the cheapest
		std::string				m_volumeDataId = Constants::s_defVolume;	/// What volume data id to use
		std::string				m_duplicateVolumeCheckId = Constants::s_defVolume; /// What is the data id we use to check duplicates

		SecurityVecPtr			m_prevDaySecurities;		/// Securities from the previous day 
		SecMap					m_prevDaySecMap;			/// Security map for the last day (for easier lookup of securities used the last day)

		float					m_exitPassRate = 1.0f;
		float					m_entryPassRate = 1.0f;
		bool					m_stickyUniverse = false;	/// Use the last day's data or not

		/// Track big/drmatic changes to the universe
		IntDay					m_bigChangeStart = 15;		/// How many days in should we start checking for big changes
		float					m_bigChangeThershold = 90.0f; /// If the universe changes more than this percentage from the previous day, then throw an error

		void 					parseLimits(const ConfigSection& config);
		void 					parseLimit(const ConfigSection& config, const std::string& limit);

		void					filter(const DataCacheInfo* dci, IDataRegistry& dr, UniverseConditionPtr condition, IntDay di, const SecurityVec& input,
								SecurityVec& output, const SecurityVecPtr& prevUniverse, const SecMap& prevUniverseMap);

	public:
								SubUniverse(const ConfigSection& config);
		virtual 				~SubUniverse();

		inline IntDay			delay() { return m_delay; }

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void 			postDataBuild(IDataRegistry& dr, const pesa::DataCacheInfo& dci);

		virtual std::string 	id() const {
			return IComponent::id() + "." + name();
		}
	};

	SubUniverse::SubUniverse(const ConfigSection& config) 
		: helper::BasicUniverseBuilder(config, config.getString("name", "SUB_UNIVERSE")) {
		m_limit					= (size_t)config.getInt("limit", 0, true);
		m_sortBy				= config.getString("sortBy", "");
		m_allowDuplicates		= config.getBool("allowDuplicates", m_allowDuplicates);
		m_delay					= config.getInt("delay", m_delay, true);

		m_priceDataId			= config.getString("priceDataId", m_priceDataId);
		m_volumeDataId			= config.getString("volumeDataId", m_volumeDataId);
		m_duplicateVolumeCheckId= config.getString("duplicateVolumeCheckId", m_duplicateVolumeCheckId);
		m_cheapest				= config.getBool("cheapest", m_cheapest);

		m_bigChangeStart		= config.getInt("bigChangeStart", m_bigChangeStart);
		m_bigChangeThershold	= config.getFloat("bigChangeThershold", m_bigChangeThershold);

		auto preferIssueClass	= config.getString("dupPreference", ""); 
		auto exitPassRate		= config.getString("exitPassRate", "");
		auto entryPassRate		= config.getString("entryPassRate", "");

		m_stickyUniverse		= config.getBool("stickyUniverse", m_stickyUniverse);

		if (!exitPassRate.empty()) 
			m_exitPassRate		= Util::percent(exitPassRate) * 0.01f;

		if (!entryPassRate.empty())
			m_entryPassRate = Util::percent(entryPassRate) * 0.01f;

		std::string filter = config.getString("filter", "");
		if (!filter.empty()) {
			auto issueClasses = Util::split(filter, "|");
			m_conditions.push_back(std::make_shared<IssueClassFilter>(issueClasses));
		}

		std::string sector = config.getString("sector", "");
		if (!sector.empty()) {
			std::string sectorDataId = config.getString("sectorDataId", "");
			m_conditions.push_back(std::make_shared<SectorCondition>(sector, sectorDataId));
		}

		std::string excludeSector = config.getString("excludeSector", "");
		if (!excludeSector.empty()) {
			std::string excludeSectorId = config.getString("excludeSectorId", "");
			if (!excludeSectorId.empty())
				m_conditions.push_back(std::make_shared<SectorFilterCondition>(excludeSector, excludeSectorId, true));
		}

		std::string includeSector = config.getString("includeSector", "");
		if (!includeSector.empty()) {
			std::string includeSectorId = config.getString("includeSectorId", "");
			if (!includeSectorId.empty())
				m_conditions.push_back(std::make_shared<SectorFilterCondition>(includeSector, includeSectorId, false));
		}

		/// load all the conditions
		float minVolume			= 0.0f;
		if (config.getAmount("minVolume", minVolume)) {
			IntDay avgDays		= config.get("d_minVolume", 0);
			auto volumeDataId	= config.getString("volumeDataId", Constants::s_defVolume);
			m_conditions.push_back(std::make_shared<GTECondition>(minVolume, volumeDataId, avgDays));
		}

		float minMarketCap		= 0.0f;
		if (config.getAmount("minMarketCap", minMarketCap)) {
			IntDay avgDays		= config.get("d_minMarketCap", 0);
			auto mcapDataId		= config.getString("marketCapDataId", Constants::s_defMarketCap);
			m_conditions.push_back(std::make_shared<GTECondition>(minMarketCap, mcapDataId, avgDays));
		}

		std::string subtract	= config.getString("subtract", "");
		if (!subtract.empty()) {
			m_conditions.push_back(std::make_shared<SubtractFilter>(subtract));
		}

		std::string addSecurities = config.getString("addSecurities", "");
		if (!addSecurities.empty()) {
			m_conditions.push_back(std::make_shared<AddFilter>(addSecurities));
		}

		std::string marketStatusDataId = config.getString("marketStatusDataId", "");
		if (!marketStatusDataId.empty()) {
			m_conditions.push_back(std::make_shared<MarketStatusFilter>(marketStatusDataId));
		}

		if (!m_allowDuplicates) {
			m_conditions.push_back(std::make_shared<DuplicateFilter>(m_duplicateVolumeCheckId, m_priceDataId));
		}

		auto restrictStart		= config.getString("restrictStartId", "");
		auto restrictEnd		= config.getString("restrictEndId", "");

		if (!restrictStart.empty() && !restrictEnd.empty()) 
			m_conditions.push_back(std::make_shared<RestrictedListFilter>(restrictStart, restrictEnd));

		std::string approvedCountriesId = config.getString("approvedParentCountries", "");
		if (!approvedCountriesId.empty()) {
			m_conditions.push_back(std::make_shared<ApprovedCountry>(
				config.getRequired("secCountryNames"),
				config.getRequired("secSymbols"),
				config.getRequired("approvedParentCountries"),
				config.getRequired("parentCountryExceptions"),
				config.getRequired("excludedStocks")
			));
		}

		std::string restrictedSecuritiesId = config.getString("restrictedSecurities", "");
		if (!restrictedSecuritiesId.empty()) 
			m_conditions.push_back(std::make_shared<RestrictedSecurities>(restrictedSecuritiesId));

		parseLimits(config);

		//std::string suName = config.getString("source", "");
		//if (!suName.empty()) {
		//	const ConfigSection* suConfig = parentConfig()->modules().findUnique("name", suName);
		//	ASSERT(suConfig, "Unable to find config for a universe with name: " << suName);
		//	IntDay suDelay = suConfig->delay();
		//	ASSERT(m_delay == suDelay, "Cannot use universe with delay: " << suDelay << " as the source of a universe with delay: " << m_delay);
		//	m_delay = 0; /// Source universe is already delayed ... we don't need this anymore
		//}
	}

	SubUniverse::~SubUniverse() {
		CTrace("Deleting SubUniverse!");
	}

	void SubUniverse::parseLimit(const pesa::ConfigSection &config, const std::string& limit) {
		auto parts = Util::split(limit, "@");
		auto lhs = parts[0];
		IntDay avgDays = parts.size() == 2 ? Util::cast<IntDay>(parts[1]) : 0;

		parts = Util::split(lhs, "[");
		ASSERT(parts.size() == 2, "Invalid limit: " << limit << ". The '[' isn't formatted correctly. Should be of the format <dataId>[<optional lower limit>, <optional higher limit>]@<optional average days]");

		auto dataId = parts[0];
		parts = Util::split(parts[1], "]");
		ASSERT(parts.size() == 2, "Invalid limit: " << limit << ". The ']' isn't formatted correctly. Should be of the format <dataId>[<optional lower limit>, <optional higher limit>]@<optional average days]");

		parts = Util::split(parts[0], ",");
		ASSERT(parts.size() == 2, "Invalid limit: " << limit << ". The range specified inside the brackets '[]' is incorrect. Should be of the format <dataId>[<optional lower limit>, <optional higher limit>]@<optional average days]");

		auto minLimit = parts[0];
		auto maxLimit = parts[1];

		if (!minLimit.empty()) {
			float minValue = Util::amount(minLimit);
			CInfo("Condition: %s >= %0.2hf [days = %d]", dataId, minValue, avgDays);
			m_conditions.push_back(std::make_shared<GTECondition>(minValue, dataId, avgDays));
		}

		if (!maxLimit.empty()) {
			float maxValue = Util::amount(maxLimit);
			CInfo("Condition: %s <= %0.2hf [days = %d]", dataId, maxValue, avgDays);
			m_conditions.push_back(std::make_shared<LTECondition>(maxValue, dataId, avgDays));
		}
	}

	void SubUniverse::parseLimits(const pesa::ConfigSection &config) {
		std::string limitsStr = config.getResolvedString("limits", "");

		if (limitsStr.empty())
			return;

		auto limits = Util::split(limitsStr, "|");

		for (const auto& limit : limits)
			parseLimit(config, limit);
	}

	void SubUniverse::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		auto name = this->name();

		datasets.add(Dataset(name, {
			DataValue("Universe", DataType::kUInt, sizeof(Security::Index), nullptr, DataFrequency::kStatic, DataFlags::kCollectAllBeforeWriting),
			DataValue("IMap", DataType::kUInt, sizeof(unsigned int), THIS_DATA_FUNC(BasicUniverseBuilder::buildIMap), DataFrequency::kStatic, 
				DataFlags::kAlwaysOverwrite | DataFlags::kUpdatesInOneGo),
			DataValue("DMap", DataType::kFloat, sizeof(float), THIS_DATA_FUNC(BasicUniverseBuilder::buildDMap), DataFrequency::kStatic, 0),
		}));
	}

	void SubUniverse::filter(const DataCacheInfo* dci, IDataRegistry& dr, UniverseConditionPtr condition, IntDay di, 
		const SecurityVec& input, SecurityVec& output, const SecurityVecPtr& prevUniverse, const SecMap& prevUniverseMap) {
		output.clear(); 

		if (condition->needsPreCheck()) {
			for (auto& sec : input) {
				condition->preCheck(dci->rt, dr, di, sec.index, dci);
			}
		}

		IntDate date = dci->rt.dates[di];
		for (auto& sec : input) {
			auto uuid = sec.uuidStr();

			bool didSelect = condition->check(dci->rt, dr, di, sec.index, dci);
			if (didSelect) {
				output.push_back(sec);
			}
			/// See why the reason for the drop was? (only on a sticky universe)
			else if (m_stickyUniverse) {
				auto piter = prevUniverseMap.find(sec.index);
				if (piter != prevUniverseMap.end()) {
					CWarning("[%u] Dropped security: %s [Ticker: %s, Name: %s] - Reason: %s", date, sec.uuidStr(), sec.tickerStr(), sec.nameStr(), condition->desc());
				}
			}
		}
	}

	void SubUniverse::postDataBuild(IDataRegistry& dr, const pesa::DataCacheInfo& dci) {	
		m_prevDaySecurities = nullptr;
		m_prevDaySecMap.clear();
		m_conditions.clear();

		BasicUniverseBuilder::postDataBuild(dr, dci);
	}

	//////////////////////////////////////////////////////////////////////////
	struct CustomCmp {
		IDataRegistry&	dr;
		std::string		dataId;
		IntDay			di;

		CustomCmp(IDataRegistry& dr_, const std::string& dataId_, IntDay di_) : dr(dr_), dataId(dataId_), di(di_) {
		}

		bool operator() (const Security& lhs, const Security& rhs) {
			const FloatMatrixData* data = dr.get<FloatMatrixData>(dr.getSecurityMaster(), dataId);

			ASSERT(data, "Unable to get data: " << dataId);
			ASSERT(lhs.isValid() && rhs.isValid(), "Invalid set of securities passed!");

			float lhsValue = (*data)(di, lhs.index);
			float rhsValue = (*data)(di, rhs.index);

			//ASSERT(!std::isnan(lhsValue) && !std::isnan(rhsValue), "NaN value should have been filtered out!");

			return lhsValue > rhsValue;
		}
	};

	//////////////////////////////////////////////////////////////////////////
	void SubUniverse::build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		BasicUniverseBuilder::build(dr, dci, frame);

		std::vector<IUniverse*>	srcUniverses;
		std::string sourcesStr	= config().getString("sources", "");
		SecurityVecPtr input	= std::make_shared<SecurityVec>();
		SecurityVecPtr output	= std::make_shared<SecurityVec>();
		IntDay di				= std::max(dci.di - delay(), 0);
		IUniverse* secMaster	= dr.getSecurityMaster();
		SecurityVecPtr prevUniverse;
		SecMap prevUniverseMap;

		/// Cache our source universes
		if (!sourcesStr.empty()) {
			StringVec sources = Util::split(sourcesStr, "|");
			for (auto& source : sources) {
				IUniverse* srcUniverse = dr.getUniverse(source, true);
				ASSERT(srcUniverse, "Unable to find source universe: " << source);
				srcUniverses.push_back(srcUniverse);
			}
		}
		else
			srcUniverses.push_back(secMaster);

		if (m_prevDaySecurities) {
			prevUniverse = m_prevDaySecurities;
			prevUniverseMap = m_prevDaySecMap;
		}
		else /// If we didn't get any previous universe then we just create an empty one ...
			prevUniverse		= std::make_shared<SecurityVec>();
		

		if (dci.lastFrame.data && !prevUniverse->size() && dci.lastFrame.data->cols() > 1) {
			UIntMatrixData& prevData = *(dynamic_cast<UIntMatrixData*>(dci.lastFrame.data.get()));
			auto cols = prevData.cols();
			ASSERT(prevData.rows() == 1, "Invalid number of rows. Expecting 1, found: " << prevData.rows());

			for (auto ii = 0; ii < cols; ii++) {
				Security::Index secId = prevData(0, ii);
				if (secId == Security::s_invalidIndex)
					continue;

				/// Also check whether this security is actually valid on this on all of the source universe
				Security sec = secMaster->security(dci.di, secId);
				prevUniverseMap[secId] = ii;
				prevUniverse->push_back(sec);
			}
		}

		size_t prevUniverseCount = prevUniverse->size();

		if (prevUniverse->size() && m_stickyUniverse) {
			UniverseConditionPtrVec conditions;

			/// First of all we try to filter out the securities from the universe from previous day 
			if (!sourcesStr.empty()) 
				conditions.push_back(std::make_shared<ParentUniverseFilter>(srcUniverses));

			float minVolume = 0.0f;
			auto& config = this->config();
			config.getAmount("minVolume", minVolume);

			if (config.getAmount("minVolume", minVolume)) {
				IntDay stickyDays = config.get("d_minVolume", 0);
				IntDay passThreshold = (IntDay)std::round(stickyDays * m_exitPassRate);
				auto volumeDataId = config.getString("volumeDataId", Constants::s_defVolume);
				conditions.push_back(std::make_shared<GTECondition>(minVolume, volumeDataId, stickyDays, passThreshold));
			}

			float minMarketCap = 0.0f;
			config.getAmount("minMarketCap", minMarketCap);

			if (config.getAmount("minMarketCap", minMarketCap)) {
				IntDay stickyDays = config.get("d_minMarketCap", 0);
				IntDay passThreshold = (IntDay)std::round(stickyDays * m_exitPassRate);
				auto mcapDataId = config.getString("marketCapDataId", Constants::s_defMarketCap);
				conditions.push_back(std::make_shared<GTECondition>(minMarketCap, mcapDataId, stickyDays, passThreshold));
			}

			if (conditions.size()) {
				for (UniverseConditionPtr condition : conditions) {
					condition->reset();
					filter(&dci, dr, condition, di, *prevUniverse, *output, prevUniverse, prevUniverseMap);
					/// Swap to make the output as the input for the next filteration function 
					*prevUniverse = *output;
				}
			}

			/// If we have fulfilled the limit of our universe with previous day's data 
			/// then we don't need to check for anything else
			if (m_limit != 0 && prevUniverse->size() >= m_limit) {
				ASSERT(prevUniverse->size() == prevUniverseCount, "Invalid state. Expecting number of passed items to be: " << prevUniverseCount << ", but instead found: " << prevUniverse->size());
				m_prevDaySecurities = prevUniverse;
				frame = dci.lastFrame;
				CInfo("[%u]-[di = %d]: Added %z securities [%0.2hf%% overlap with previous day] *NO_CHANGE*", dci.rt.dates[dci.di], dci.di, prevUniverse->size(), 100.0f);
				return;
			}

			output->clear();
		}

		SecurityVec				srcSecurities;

		/// Universes are ALREADY delayed which is why we pass dci.di instead of the di we calculated above
		size_t minSize			= srcUniverses[0]->size(dci.di); /// Universe will be AT LEAST this big
		input->reserve(minSize);
		output->reserve(minSize);

		/// A bit of an optimisation for the most common scenario
		if (srcUniverses.size() == 1) {
			for (size_t ii = 0; ii < minSize; ii++) {
				Security sec = srcUniverses[0]->security(dci.di, (Security::Index)ii);

				if (!sec.isValid())
					continue;

				/// Only add the valid securities
				if (srcUniverses[0]->isValid(dci.di, ii)) {
					if (m_stickyUniverse) {
						auto piter = prevUniverseMap.find(sec.index);

						if (piter == prevUniverseMap.end())
							input->push_back(sec);
					}
					else
						input->push_back(sec);
				}
			}
		}
		else {
			/// Universes are ALREADY delayed which is why we pass dci.di instead of the di we calculated above
			std::unordered_map<Security::Index, bool> secLookup;

			for (IUniverse* srcUniverse : srcUniverses) {
				size_t size = srcUniverse->size(dci.di);

				for (size_t ii = 0; ii < size; ii++) {
					Security sec = srcUniverse->security(dci.di, (Security::Index)ii);

					if (srcUniverse->isValid(dci.di, ii)) {
						if (m_stickyUniverse) {
							auto piter = prevUniverseMap.find(sec.index);
							if (piter != prevUniverseMap.end())
								continue;
						}

						auto iter = secLookup.find(sec.index);

						/// If we haven't used that security so far in the day then we use it and mark it as used
						if (iter == secLookup.end()) {
							input->push_back(sec);
							secLookup[sec.index] = true;
						}
						/// else {} Otherwise we ignore this security as it's already in the universe!
					}
				}
			}
		}

		/// If we've filtered out all securties, then this is ok ONLY if we have some data 
		/// from the last day
		//if (!input->size() && dci.lastFrame.data) {
		//	frame = dci.lastFrame;
		//	return;
		//}

		if (input->size()) {
			ASSERT(input->size(), "Input array is empty!");

			for (UniverseConditionPtr condition : m_conditions) {
				condition->reset();
				filter(&dci, dr, condition, di, *input, *output, prevUniverse, prevUniverseMap);

				//ASSERT(output->size(), "Filtered out every element!");

				/// Swap to make the output as the input for the next filteration function 
				*input = *output;
			}
		}

		/// The last swap would have given us the wrong pointer in output. Fix it!
		//if (m_conditions.size())
			output = input;

		/// sort it ...
		if (!m_sortBy.empty() && output->size()) {
			std::sort(output->begin(), output->end(), [&](auto&& lhs, auto&& rhs) -> bool {
				//CustomCmp(dr, m_sortBy, di)
				auto dataId = this->m_sortBy;
				const FloatMatrixData* data = dr.get<FloatMatrixData>(dr.getSecurityMaster(), dataId);

				ASSERT(data, "Unable to get data: " << dataId);
				ASSERT(lhs.isValid() && rhs.isValid(), "Invalid set of securities passed!");

				float lhsValue = (*data)(di, lhs.index);
				float rhsValue = (*data)(di, rhs.index);

				//ASSERT(!std::isnan(lhsValue) && !std::isnan(rhsValue), "NaN value should have been filtered out!");

				return lhsValue > rhsValue;
			});
		}

		SecurityVecPtr currUniverse = m_stickyUniverse ? prevUniverse : std::make_shared<SecurityVec>();
		size_t numCommon = 0;

		/// If there is no limit, we just add everything in the current securities
		if (!m_limit)
			currUniverse->insert(currUniverse->begin(), output->begin(), output->end());
		else {
			size_t numToAdd = std::max((int)m_limit - (int)currUniverse->size(), 0);
			currUniverse->insert(currUniverse->begin(), output->begin(), output->size() > numToAdd ? output->begin() + numToAdd : output->end());
		}

		if (!currUniverse->size()) {
			CWarning("[%u] - Nothing constructed probably because the data for the date isn't available", dci.rt.dates[dci.di]);
			frame.noDataUpdate = true;
			return;
		}

		//for (size_t i = 0; i < currUniverse->size(); i++) {
		//	auto& sec = currUniverse->at(i);
		//	auto iter = prevUniverseMap.find(sec.index);
		//	if (iter != prevUniverseMap.end())
		//		numCommon++;
		//}

		//ASSERT(output->size(), "Output array is empty!");

		SecMap currDay;

		/// Initialise as invalid securities ...
		size_t limit = m_limit;

		if (!limit || limit > currUniverse->size())
			limit = currUniverse->size();

		UIntMatrixData* data = new UIntMatrixData(dci.universe, dci.def, 1, limit, false);
		data->invalidateMemory();
		//for (size_t ii = 0; ii < limit; ii++)
		//	(*data)(0, ii) = Security::s_invalidIndex;

		size_t numSecurities = 0;

		m_prevDaySecMap.clear();

		IntDate date = dci.rt.dates[dci.di];
		SecMap indicesInCurrDay;

		for (size_t ii = 0; ii < currUniverse->size(); ii++) {
			auto& sec = currUniverse->at(ii);
			ASSERT(sec.index != Security::s_invalidIndex, "Invalid security index");
			m_prevDaySecMap[sec.index] = ii;
			(*data)(0, (Security::Index)ii) = sec.index;

			auto iter = prevUniverseMap.find(sec.index);
			if (iter != prevUniverseMap.end()) {
				indicesInCurrDay[iter->first] = iter->second;
				numCommon++;
			}

			BasicUniverseBuilder::addToAlphaIndexMap(dci.di, sec.index);
		}

		float commonPercent = 0.0f;
		if (prevUniverseCount)
			commonPercent	= ((float)numCommon / (float)prevUniverseCount) * 100.0f;

		CInfo("[%u]-[Overlap: %0.2hf%%]-[Size: %z]: %z securities", dci.rt.dates[dci.di], commonPercent, data->dataSize(), currUniverse->size());

		/// If we're not within the first few days of setting the universe up
		if (prevUniverseCount && m_bigChangeThershold > 0.0f && m_bigChangeStart >= 0 && dci.di > m_bigChangeStart) {
			/// Something is wrong, too many securities have been kicked out since yesterday. Dump the securities that
			/// have been kicked out!

			if (commonPercent < m_bigChangeThershold) {
				//for (size_t ii = 0; ii < m_prevDaySecurities->size(); ii++) {
				//	const Security& sec = m_prevDaySecurities->at(ii);
				//	auto iter = currDaySecMap.find(sec.index);

				//	if (iter == currDaySecMap.end()) {
				//		CWarning("[%u] Missing from yesterday to today: %s [ISIN: %s, TKR: %s, INST: %s, NM: %s", date, sec.uuidStr(), sec.isinStr(), sec.tickerStr(), sec.instrumentId(), sec.nameStr());
				//	}
				//}
				for (const auto iter : prevUniverseMap) {
					auto cdayIter = indicesInCurrDay.find(iter.first);

					if (cdayIter == indicesInCurrDay.end()) {
						size_t ii = iter.second;
						const Security& sec = m_prevDaySecurities->at(ii);
						CWarning("[%u] Missing from yesterday to today: %s [ISIN: %s, TKR: %s, INST: %s, NM: %s", date, sec.uuidStr(), sec.isinStr(), sec.tickerStr(), sec.instrumentId(), sec.nameStr());
					}
				}

				ASSERT(false, Poco::format("%u => %u Too big a change in the universe from the previous date. Percent common: %0.2hf [Threshold: %0.2hf]", dci.rt.dates[dci.di - 1], dci.rt.dates[dci.di], commonPercent, m_bigChangeThershold));
			}
		}

		/// Save it in memory for processing the next day
		m_prevDaySecurities = currUniverse;

		/// Setup the daily DataFrame
		frame.data			= UIntMatrixDataPtr(data);
		frame.dataOffset	= 0;
		frame.dataSize		= data->dataSize();
		frame.noDataUpdate	= false;
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createSubUniverse(const pesa::ConfigSection& config) {
	return new pesa::SubUniverse((const pesa::ConfigSection&)config);
}

