/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// AlphaExec.cpp
///
/// Created on: 7 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Helper/Email.h"
#include "Framework/Framework.h"
#include "Framework/Helper/FileDataLoader.h"
#include "Framework/Helper/MergedUniverse.h"

#include "Poco/Thread.h"
#include "Poco/Process.h"
#include "Poco/String.h"
#include "Poco/File.h"

#include <unordered_map>
#include <future>

#include "Core/Lib/Profiler.h"
#include "Helper/DB/MongoStream.h"
#include "Core/IO/BasicStream.h" 

namespace pesa {
	class AlphaExec;
	enum ChildStratErrorCodes {
		kOk						= 0, 
		kNoRun					= -99,
		kRunException			= -98,
	};

	struct AlphaStrategyInfo {
		std::string				uuid;
		std::string				filename;
		std::string				workingDir;
		std::string				logFilename;		/// Full log filename
		std::string				posDir;
		std::string				pnlDir;
		std::string				posFilename = "MyPort";
		std::string				universeId;
		float					bookSize = 0.0f;
		ConfigSection*			config;
		Poco::ProcessHandle*	proc = nullptr;
		StringVec				procArgs;			/// The arguments for the process
		int						exitCode = (int)kOk;

		StringVec				attachments;
		StringVec				inlineAttachments;

		std::string				plotString;			/// What to plot for this particular strategy
	};

	struct ParentAlphaInfo {
		IUniverse*				universe;
		FloatMatrixDataPtr		alphaData;
	};

	////////////////////////////////////////////////////////////////////////////
	/// AlphaExec - Executes all the alphas
	////////////////////////////////////////////////////////////////////////////
	class AlphaExec : public IPipelineComponent {
	private:
		typedef std::unordered_map<std::string, IUniverse*> IUniverseMap;
		typedef std::unordered_map<std::string, IOperationPtr> OperationMap;
		typedef std::unordered_map<std::string, ICombinationPtr> CombinationMap;
		typedef std::unordered_map<std::string, size_t> AlphaIndexMap;
		typedef std::unordered_map<std::string, std::shared_ptr<AlphaIndexMap> > AlphasIndexMap;
		typedef std::unordered_map<std::string, IAlphaPtr> IAlphaMap;
		typedef std::map<std::string, AlphaStrategyInfo> ProcMap;
		typedef std::map<std::string, ParentAlphaInfo> ParentAlphaInfoMap;

		static const size_t		s_minDays;
		//////////////////////////////////////////////////////////////////////////
		/// Multithreading
		//////////////////////////////////////////////////////////////////////////
		IntDate					m_lastDi = 0;						/// The last date that we pushed ...
		std::mutex 				m_asyncMutex;						/// Data mutex
		OperationMap			m_operationMap;						/// The operations that we have cached for each of the Alphas
		CombinationMap			m_combinationMap;					/// The combination map!
		AlphasIndexMap			m_aindexMap;						/// The alpha index map. This ensures that alphas created in a certain order, keep that order

		IAlphaMap				m_lastAlpha;						/// The last alpha value that was calculated

		IUniverseMap			m_comboUniverses;					/// Combo universes

		ProcMap					m_childStrategies;					/// The child strategies that we have within this config ...
		int						m_numThreads = 1;					/// How many threads to use to execute the alphas
		ParentAlphaInfoMap		m_parentAlphas;						/// Alphas that serve as a parent to other alphas
		int						m_maxConcurrentChildStrats = 0;
		bool					m_ignoreAlphaDates = true;			/// Whether to ignore alpha dates or not
		bool					m_aggregateBookSize = false;		/// Whether to aggregate the book size or not
		bool					m_aggregateDaily = false;			/// Aggregate the book size daily or not
		float					m_alphaAggregateBookSize = 0.0f;	/// Aggergated book size
		io::MongoClient*		m_mongoCl = nullptr;				/// The mongo client
		std::string				m_firstAlphaUniverse;				/// What is the first alpha universe that we encoutered

		bool					m_statelessAlpha = false;			/// Should we have stateless alphas or not

		//////////////////////////////////////////////////////////////////////////

		void					targetDeployStrategy(AlphaStrategyInfo& asInfo);
		void					emailStrategyInfo();
		std::string				copyFile(const AlphaStrategyInfo& asInfo, const std::string& dir, const std::string& targetDir, const std::string& filenameSuffix, const std::string& ext);
		bool					plot(const StringVec& filenames, const std::string& dstFilename) const;

		IAlphaPtr				findAlpha(const RuntimeData& rt, const AlphaResult* ar, const std::string& uuid) const;

		bool					saveCheckpoint(const RuntimeData& rt, const ConfigSection& comboConfig, const AlphaResult& ar);
		bool					loadCheckpoint(const RuntimeData& rt, const ConfigSection& comboConfig, AlphaResult& ar);
		std::string				getCheckpointFilename(const RuntimeData& rt, const ConfigSection& comboConfig);

		//////////////////////////////////////////////////////////////////////////
		void					checkParentIds(const RuntimeData& rt, const ConfigSection& alphasConfig);
		void					getChildStrategies(const RuntimeData& rt, const ConfigSection& alphasConfig);
		void					execChildStrategies(const RuntimeData& rt);
		void					execLimitedChildStrategies(const RuntimeData& rt, bool noRunStrategies);
		void					waitForChildStrategies(const RuntimeData& rt, bool noRunStrategies);

		void					execAlphas(const RuntimeData& rt, ConfigSection& alphasConfig, AlphaResult& result);
		void					execAllAlphas(const RuntimeData& rt, ConfigSection& alphasConfig, AlphaResult& result);
		int						execAlpha(const RuntimeData* rt, ConfigSection* alphaConfig, AlphaResult* result, IAlphaPtr alpha);
		int						execAlphaDirect(const RuntimeData* rt, ConfigSection* alphaConfig, AlphaResult* result, IAlphaPtr alpha = nullptr);
		void					execOperation(const RuntimeData& rt, ConfigSection& opConfig, IAlphaPtr alpha);
		void					execCombination(const RuntimeData& rt, const ConfigSection& comboConfig, AlphaResult& alphas, const std::string& firstAlphaUniverse);

		void					finishAlpha(const RuntimeData& rt);
		void					clearInvalid(const RuntimeData&rt, FloatMatrixDataPtr alphaData, IUniverse* universe);

		static FloatMatrixData* deriveAlpha(const RuntimeData& rt, FloatMatrixData* srcData, IUniverse* srcUniverse, FloatMatrixData* dstData, IUniverse* dstUniverse);

		std::string				getIdFromChildren(const std::vector<AlphaResult>& arList);

		inline std::string		getTargetDir() const {
			auto name = parentConfig()->defs().getString("name", "");
			auto stratsDir = parentConfig()->defs().getString("stratsDir", "");
			stratsDir = parentConfig()->defs().resolve(stratsDir, nullptr);
			if (stratsDir.empty() || name.empty())
				return "";
			Poco::Path targetPath(stratsDir);
			targetPath.append(name);
			return targetPath.toString();
		}

	public:
								AlphaExec(const ConfigSection& config);
		virtual 				~AlphaExec();

		////////////////////////////////////////////////////////////////////////////
		/// IPipelineComponent overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			prepare(RuntimeData& rt);
		virtual void 			tick(const RuntimeData& rt);
		virtual void 			finalise(RuntimeData& rt);
	};

	const size_t AlphaExec::s_minDays = 10;

	AlphaExec::AlphaExec(const ConfigSection& config) : IPipelineComponent(config, "ALPHA_EXEC") {
		PTrace("Created AlphaExec!");

		m_numThreads = std::max(Application::instance()->appOptions().numThreads, parentConfig()->defs().get<int>("numThreads", 1));
		if (m_numThreads <= 0)
			m_numThreads = 1;

		m_maxConcurrentChildStrats = parentConfig()->defs().get<int>("maxConcurrentChildStrategies", m_maxConcurrentChildStrats);

		if (Application::instance()->appOptions().maxChildProcesses || m_maxConcurrentChildStrats == 0)
			m_maxConcurrentChildStrats = Application::instance()->appOptions().maxChildProcesses;

		m_ignoreAlphaDates = parentConfig()->defs().getBool("ignoreAlphaDates", m_ignoreAlphaDates);
		m_aggregateBookSize = parentConfig()->portfolio().getBool("aggregateBookSize", m_aggregateBookSize);
		m_aggregateDaily = parentConfig()->portfolio().getBool("aggregateDaily", m_aggregateDaily);
		m_statelessAlpha = config.getBool("statelessAlpha", m_statelessAlpha);

		/// Can't have more than 1 thread for non-stateless alphas
		if (!m_statelessAlpha)
			m_numThreads = 1;
	}

	AlphaExec::~AlphaExec() {
		PTrace("Deleting AlphaExec!");
	}

	std::string AlphaExec::getIdFromChildren(const std::vector<AlphaResult>& arList) {
		std::string id = "COMBO-";

		for (const AlphaResult& car : arList) 
			id += car.alpha->uuid() + "-";

		id += "AUTO_ID";

		return id;
	}

	IAlphaPtr AlphaExec::findAlpha(const RuntimeData& rt, const AlphaResult* ar, const std::string& uuid) const {
		if (!ar) {
			if (rt.di == 0)
				return nullptr;

			auto dataId = IAlpha::genId(rt.di - 1);

			/// Finally we set the data in the DataRegistry ...
			DataInfo dinfo;
			dinfo.noAssertOnMissingDataLoader = true;

			const AlphaResultData* resultData = rt.dataRegistry->get<AlphaResultData>(nullptr, dataId, &dinfo);
			if (!resultData)
				return nullptr;

			ar = resultData->dataPtr().get();
		}

		if (ar->alpha && ar->alpha->uuid() == uuid)
			return ar->alpha;

		for (size_t ci = 0; ci < ar->childAlphas.size(); ci++) {
			const AlphaResult& car = ar->childAlphas[ci];
			IAlphaPtr matchedAlpha = findAlpha(rt, &car, uuid);
			if (matchedAlpha)
				return matchedAlpha;
		}

		return nullptr;
	}

	void AlphaExec::clearInvalid(const RuntimeData&rt, FloatMatrixDataPtr alphaData, IUniverse* universe) {
		PROFILE_SCOPED();

		auto* dmap = universe->dailyValidMap();
		if (dmap) {
			size_t numRows = alphaData->rows(); 

			for (IntDay di = 0; di < (IntDay)numRows; di++) {
				alphaData->row(di) = (alphaData->row(di) * dmap->row(rt.di)).infClear(std::nanf(""));
			}
		}
		else {
			size_t size = universe->size(rt.di);
			float* fdata = alphaData->fdata();

			for (size_t ii = 0; ii < size; ii++) {
				float alphaValue = (*alphaData)(0, ii);
				if (!universe->isValid(rt.di, ii))
					alphaData->makeInvalid(0, (UIntCount)ii);
				else if (std::isinf(alphaValue))
					alphaData->makeInvalid(0, (UIntCount)ii);
			}
		}
	}

	void AlphaExec::execAlphas(const RuntimeData& rt, ConfigSection& alphasConfig, AlphaResult& ar) {
		PROFILE_SCOPED();

		PTrace("Executing alphas block: %s", alphasConfig.toString());

		/// first we see whether we have a child alphas section ...
		auto childAlphasConfigs = alphasConfig.getAllChildrenOfType("Alphas");
		if (childAlphasConfigs.size()) {
			for (auto i = 0; i < childAlphasConfigs.size(); i++) {
				AlphaResult car;
				execAlphas(rt, *(childAlphasConfigs[i]), car);

				/// Now add this result to the list ...
				ar.childAlphas.push_back(car);
			}
		}

		/// We then continue on and evalute the normal alpha within this alphas section ...
		execAllAlphas(rt, alphasConfig, ar);
	}

	void AlphaExec::execAllAlphas(const RuntimeData& rt, ConfigSection& alphasConfig, AlphaResult& ar) {
		/// If the load checkpoint is successful ... then just return!
		if (loadCheckpoint(rt, alphasConfig, ar))
			return;

		auto alphas = alphasConfig.alphas();
		auto strats = alphasConfig.getAllChildrenOfType("Strategy");
		auto date = rt.dates[rt.di];

		alphas.insert(alphas.end(), strats.begin(), strats.end());

		std::vector<int> alphaDidExec(alphas.size());
		std::vector<AlphaResult> alphaResults(alphas.size());

		if (alphas.size()) {
			typedef std::future<int> Future;
			typedef std::list<Future> FutureList;

			FutureList asyncJobs;

			memset(&alphaDidExec[0], 0, sizeof(int) * alphaDidExec.size());

			for (auto i = 0; i < alphas.size(); i++) {
				auto* alphaConfig = alphas[i];

				if (!alphaConfig->getBool("_finalised_", false)) {
					const ConfigSection* moduleConfig = parentConfig()->getModuleFromModuleId(*alphaConfig);
					ASSERT(moduleConfig, "Unable to find moduleId: " << alphaConfig->moduleId());

					/// Merge the module and portfolio configs
					alphaConfig->mergeProperties(*moduleConfig, nullptr, true);
					//alphaConfig.set("checkpoint", alphasConfig.getString("checkpoint", ""));
					//alphaConfig.set("checkpointDir", alphasConfig.getString("checkpointDir", ""));
					alphaConfig->mergeProperties(parentConfig()->portfolio(), nullptr, true);

					alphaConfig->set("_finalised_", "true");
				}

				if (m_firstAlphaUniverse.empty())
					m_firstAlphaUniverse = alphaConfig->getRequired("universeId");

				/// OK here we try to see if there is a start date in the alpha that 
				/// overrides the startDate of the entire config ...
				if (!m_ignoreAlphaDates) {
					IntDate alphaStartDate = alphaConfig->get<int>("startDate", 0);
					IntDate alphaEndDate = alphaConfig->get<int>("endDate", 0);

					/// Then we ignore the alpha completely if its start date has not 
					/// been reached as yet in the backtest ...
					if (alphaStartDate != 0 && alphaStartDate > date) 
						continue;

					/// If an end date has been specified and it has past then we don't run the alpha any
					if (alphaEndDate != 0 && date > alphaEndDate)
						continue;
				}

				alphaDidExec[i] = 1;

				auto& alphaResult = alphaResults[i];

				/// If no threads are being used then just don't do anything
				if (m_numThreads <= 1) {
					execAlphaDirect(&rt, alphaConfig, &alphaResult);
				}
				else {
					/// Otherwise we wait for a slot to become available and then launch an async process
					/// Wait for one of the threads to finish
					while (asyncJobs.size() >= (size_t)m_numThreads) {
						auto& future = asyncJobs.front();
						future.wait();
						asyncJobs.pop_front();
					}

					IAlphaPtr alpha = nullptr;
					//if (alphaConfig->getRequired("moduleId") == "PyImpl") {
					//	alpha = helper::ShLib::global().createComponent<IAlpha>(*alphaConfig, rt.dataRegistry);
					//}

					auto future = std::async(std::launch::async, &AlphaExec::execAlphaDirect, this, &rt, alphaConfig, &alphaResult, alpha);
					asyncJobs.push_back(std::move(future));
				}
			}

			/// Clear the async job cache so that all the threads are finished ...
			asyncJobs.clear();
		}


		std::string comboUuid = alphasConfig.getRequired("uuid");
		auto aiter = m_aindexMap.find(comboUuid);
		AlphaIndexMap* aindexMap = nullptr;
		if (aiter == m_aindexMap.end()) {
			aindexMap = new AlphaIndexMap();
			m_aindexMap[comboUuid] = std::shared_ptr<AlphaIndexMap>(aindexMap);
		}
		else {
			aindexMap = aiter->second.get();
		}

		ASSERT(aindexMap, "Unable to get alpha index map for combo uuid: " << comboUuid);

		for (auto i = 0; i < alphas.size(); i++) {
			/// only do all of this if the alpha has been executed
			if (alphaDidExec[i]) {
				auto* alphaConfig = alphas[i];
				auto uuid = alphaConfig->getRequired("uuid");
				ASSERT(!uuid.empty(), "Invalid alpha with no uuid!");
				size_t alphaIndex = aindexMap->size();
				auto iter = aindexMap->find(uuid);

				if (iter != aindexMap->end())
					alphaIndex = iter->second;
				else
					(*aindexMap)[uuid] = alphaIndex;

				if (ar.childAlphas.size() < alphaIndex + 1)
					ar.childAlphas.resize(alphaIndex + 1);

				/// See whether this has more child alphas
				ar.childAlphas[alphaIndex] = alphaResults[i];
			}
		}

		/// add the combination at the end
		ConfigSection* comboConfig = &alphasConfig;
		ASSERT(comboConfig, "Invalid Alphas section without any combination!");

		/// Merge the module and portfolio configs
		if (!comboConfig->getBool("_finalised_", false)) {
			comboConfig->mergeProperties(*parentConfig()->getModuleFromModuleId(*comboConfig), nullptr, true);
			comboConfig->mergeProperties(parentConfig()->portfolio(), nullptr, true);
			comboConfig->set("_finalised_", "true");
		}

		/// Run the combination after that ...
		execCombination(rt, *comboConfig, ar, m_firstAlphaUniverse);

		/// invalidate the entries that are not part of the universe AFTER the Combination
		clearInvalid(rt, ar.alpha->data().alpha, ar.alpha->data().universe);

		/// And in the end we exec the operations (at Alphas level)
		auto operations = alphasConfig.operations();

		for (auto* opConfig : operations) {
			execOperation(rt, *opConfig, ar.alpha);
			/// invalidate the entries that are not part of the universe AFTER each operation
			clearInvalid(rt, ar.alpha->data().alpha, ar.alpha->data().universe);
		}
	}

	FloatMatrixData* AlphaExec::deriveAlpha(const RuntimeData& rt, FloatMatrixData* srcData, IUniverse* srcUniverse, FloatMatrixData* dstData, IUniverse* dstUniverse) {
		IUniverse* secMaster = rt.dataRegistry->getSecurityMaster();
		auto numRows = (IntDay)srcData->rows();
		auto numCols = (Security::Index)dstData->cols();

		if (dstData->rows() != numRows)
			dstData->ensureSize((size_t)numRows, dstData->cols(), false);

		for (IntDay di = 0; di < numRows; di++) {
			for (Security::Index ii = 0; ii < numCols; ii++) {
				Security sec = dstUniverse->security(di, ii);
				Security::Index iiSrc = (Security::Index)srcUniverse->alphaIndex(di, (size_t)sec.index);

				Security secSrc = srcUniverse->security(di, iiSrc);
				ASSERT(secSrc.index == sec.index, "Invalid srcUniverse to dstUniverse mapping!");

				float srcValue = (*srcData)(di, iiSrc);
				(*dstData)(di, ii) = srcValue;
			}
		}

		return dstData;
	}

	int AlphaExec::execAlpha(const RuntimeData* rt_, ConfigSection* alphaConfig_, AlphaResult* result_, IAlphaPtr alpha) {
		const RuntimeData& rt		= *rt_;
		ConfigSection& alphaConfig	= *alphaConfig_;
		AlphaResult& result			= *result_;
		std::string universeId		= alphaConfig.universeId();
		IUniverse* universe			= rt.dataRegistry->getUniverse(universeId);
		alpha->weight()				= alphaConfig.weight();

		float weight				= alpha->weight();
		float absWeight				= std::abs(weight);

		ASSERT(universe, "Unable to get universe: " << universeId);

		/// Set the universe for the alpha
		alpha->universe() = universe;

		size_t size = universe->size(rt.di);
		ASSERT(size, "Universe-%s" << universe->name() << " invalid size returned on di: " << rt.di);

		auto parentUuid = alphaConfig.getString("parentUuid", "");
		auto alphaUuid = alpha->uuid();

		//IAlphaPtr prevAlpha = nullptr;

		//{
		//	AUTO_LOCK(m_asyncMutex);

		//	ASSERT(false, "TODO!");

		//	/// Make yesterday's data available to the Alpha as well
		//	auto piter = m_lastAlpha.find(alphaUuid);
		//	if (piter != m_lastAlpha.end())
		//		prevAlpha = piter->second;
		//	else
		//		prevAlpha = findAlpha(rt, nullptr, alphaUuid);

		//	/// Set the stats for yesterday
		//	if (prevAlpha)
		//		alpha->yesterdayDataRef() = prevAlpha->dataPtr();

		//	m_lastAlpha[alpha->uuid()] = alpha;
		//}

		/// Create the buffer to contain the results of the alpha
		alpha->data().ensureAlphaSize(1, size);
		FloatMatrixDataPtr alphaData = alpha->data().alpha;
		//alphaData->invalidateMemory();

		/// run the alpha if it's not derived from any parent alpha ...
		if (!std::isnan(weight) && absWeight > 0.0001f) {
			if (parentUuid.empty()) {
				alpha->run(rt);
			} 
			else {
				/// Otherwise we map the alpha from the parent to the current
				auto iter = m_parentAlphas.find(parentUuid);

				if (iter == m_parentAlphas.end()) {
					PDebug("Parent alpha [UUID = %s] was not generated on date: %u. Generating in full: %s", parentUuid, rt.dates[rt.di], alphaUuid);
					alpha->run(rt);
				}
				else {
					ParentAlphaInfo pai = iter->second;
					AlphaExec::deriveAlpha(rt, pai.alphaData.get(), pai.universe, alpha->data().alpha.get(), universe);
				}
				//ASSERT(iter != m_parentAlphas.end(), "Unable to find alpha data for parentUuid: " << parentUuid);

			}
		}
		else {
			/// Set to 0
			alphaData->setMemory(0.0f);
		}

		/// Get the alpha book size
		m_alphaAggregateBookSize += alpha->bookSize();

		/// invalidate the entries that are not part of the universe BEFORE the Operation!
		clearInvalid(rt, alphaData, universe);

		/// If this alpha is being used as a parent (we detected that in the prepare part of the config).
		/// Then we have to save the stat of the alpha BEFORE anything else is done to this alpha vector
		bool asParent = alphaConfig.getBool("asParent", false);
		if (asParent) {
			FloatMatrixData* parentAlpha = dynamic_cast<FloatMatrixData*>(alphaData->clone(nullptr, false));
			ParentAlphaInfo pai;
			pai.alphaData = FloatMatrixDataPtr(parentAlpha);
			pai.universe = universe;
			m_parentAlphas[alphaUuid] = pai;
		}

		/// run all the operations only if the alpha has valid weights ...
		if (!std::isnan(weight) && absWeight > 0.0001f) {
			auto operations = alphaConfig.operations();

			for (auto* opConfig : operations) {
				execOperation(rt, *opConfig, alpha);
				clearInvalid(rt, alpha->data().alpha, alpha->data().universe);
			}
		}

		result.alpha = alpha;
		result.data = alpha->dataPtr();

		return 0;
	}

	int AlphaExec::execAlphaDirect(const RuntimeData* rt, ConfigSection* alphaConfig, AlphaResult* result, IAlphaPtr alpha /* = nullptr */) {
		PROFILE_SCOPED();

		if (!alpha) {
			bool loadAlpha = m_statelessAlpha;
			IAlphaPtr prevAlpha = nullptr;

			/// Otherwise we load it the first time around!
			auto alphaUuid(alphaConfig->getRequired("uuid"));
			auto iter = m_lastAlpha.find(alphaUuid);

			if (iter != m_lastAlpha.end()) {
				prevAlpha = iter->second;

				if (!prevAlpha || prevAlpha->needsCleanState())
					loadAlpha = true;
			}

			if (loadAlpha || !prevAlpha) {
				alpha = helper::ShLib::global().createComponent<IAlpha>(*alphaConfig, rt->dataRegistry);

				if (prevAlpha)
					alpha->yesterdayDataPtr() = prevAlpha->dataPtr();

				m_lastAlpha[alphaUuid] = alpha;
			}
			else { 
				alpha = prevAlpha;
				alpha->resetState();
			}
		}

		ASSERT(alpha, "Unable to create alpha from config: " << alphaConfig->toString());
        
		return execAlpha(rt, alphaConfig, result, alpha);
	}

	void AlphaExec::execOperation(const RuntimeData& rt, ConfigSection& opConfig, IAlphaPtr alpha) {
		PROFILE_SCOPED();
		 
		int64_t confAddr = (int64_t)&opConfig;
		auto alphaLTUuid = alpha->uuid() + Util::cast(confAddr);
		ASSERT(!alphaLTUuid.empty(), "Alpha does not have a valid uuid!");
		IOperationPtr operation = nullptr;

		{
			AUTO_LOCK(m_asyncMutex);

			auto iter = m_operationMap.find(alphaLTUuid); 

			if (iter != m_operationMap.end()) 
				operation = iter->second;
			else {
				if (!opConfig.getBool("_finalised_", false)) {
					const ConfigSection* moduleConfig = parentConfig()->getModuleFromModuleId(opConfig);
					opConfig.mergeProperties(*moduleConfig, nullptr, true);
					opConfig.mergeProperties(parentConfig()->portfolio(), nullptr, true);
					opConfig.set("_finalised_", "true");
				}

				operation = helper::ShLib::global().createComponent<IOperation>(opConfig, rt.dataRegistry);
				ASSERT(operation, "Unable to create operation from config: " << opConfig.toString());

				m_operationMap[alphaLTUuid] = operation;
			}
		}

		/// Copy the alpha data over to the operation ...
		operation->dataPtr() = alpha->dataPtr();

		if (m_statelessAlpha) {
			IAlphaPtr prevAlpha = nullptr;
			auto alphaUuid = alpha->uuid();

			{
				AUTO_LOCK(m_asyncMutex);

				/// Set the alpha stats to previous day's stats ... so as to make them available in the 
				/// operations and then reset them at the end
				auto piter = m_lastAlpha.find(alphaUuid);
				if (piter != m_lastAlpha.end())
					prevAlpha = piter->second;
				else
					prevAlpha = findAlpha(rt, nullptr, alphaUuid);
			}

			/// Set the stats for yesterday
			if (prevAlpha) 
				operation->yesterdayDataRef() = prevAlpha->yesterdayData();
		}
		else {
			operation->yesterdayDataRef() = alpha->yesterdayData();
		}

		operation->run(rt);

		/// Now copy the results back to the alpha vector
		ASSERT(operation->alpha().cols() == alpha->alpha().cols(), "Invalid operation that modified the alpha vector. Expected Size: "
				<< alpha->alpha().cols() << ", Found Size: " << operation->alpha().cols());

		//alpha->alpha() = operation->alpha();
		if (m_statelessAlpha)
			operation->data().alpha = nullptr;
	}

	std::string AlphaExec::getCheckpointFilename(const RuntimeData& rt, const ConfigSection& comboConfig) {
		std::string checkpointDir = comboConfig.getString("checkpointDir", "./acp");

		ASSERT(!checkpointDir.empty(), "Invalid checkpointDir specified in alphas block: " << comboConfig.getRequired("uuid"));

		Poco::Path cpPath(parentConfig()->filename());
		std::string fname = cpPath.getBaseName();

		cpPath.makeParent();
		cpPath.append(checkpointDir);
		cpPath.append(comboConfig.getRequired("uuid"));
		cpPath.append(fname + "_" + comboConfig.getRequired("uuid") + "_" + Util::cast(rt.di));
		cpPath.setExtension("acp");

		Util::ensureDirExists(cpPath);

		return cpPath.toString();
	}

	bool AlphaExec::saveCheckpoint(const RuntimeData& rt, const ConfigSection& alphaConfig, const AlphaResult& ar) {
		if (!rt.appOptions.useComboCheckpoint)
			return false; 

		bool checkpoint = alphaConfig.getBool("checkpoint", false);

		if (!checkpoint)
			return false; 

		std::string cpFilename = getCheckpointFilename(rt, alphaConfig);
		io::BasicOutputStreamPtr stream = io::BasicOutputStream::createBinaryWriter(cpFilename);

		stream->write("di", rt.di);

		const FloatMatrixData& calpha = ar.alpha->alpha();
		IUniverse* alphaUniverse = ar.alpha->data().universe;
		size_t numRows = calpha.rows();
		size_t numCols = calpha.cols();
		size_t depth = calpha.depth();

		stream->write("universeName", alphaUniverse->name());
		stream->write("rows", numRows);
		stream->write("cols", numCols);
		stream->write("depth", depth);

		for (size_t kk = 0; kk < depth; kk++) {
			stream->write("ptr", reinterpret_cast<const char*>(calpha.fdata(kk)), numRows * numCols * calpha.itemSize());
		}

		return true;
	}

	bool AlphaExec::loadCheckpoint(const RuntimeData& rt, const ConfigSection& alphaConfig, AlphaResult& ar) {
		if (!rt.appOptions.useComboCheckpoint)
			return false;

		std::string cpFilename;

		try {
			/// If we're running with the force option then don't load any checkpoint ... let it run normally
			if (rt.appOptions.force)
				return false;

			bool checkpoint = alphaConfig.getBool("checkpoint", false);

			if (!checkpoint)
				return false;

			int checkpointDay = alphaConfig.get<int>("checkpointDay", 0);

			/// If we're checkpointDay on either side of the starting date, we don't use the cached alpha ...
			if (checkpointDay && std::abs(rt.di - rt.diStart) <= checkpointDay)
				return false;

			cpFilename = getCheckpointFilename(rt, alphaConfig);
			Poco::File file(cpFilename);

			if (!file.exists() || file.getSize() == 0)
				return false;

			if (!ar.alpha) 
				ar.alpha = std::make_shared<ComboResult>(alphaConfig.getRequired("uuid"), alphaConfig);

			io::BasicInputStreamPtr stream = io::BasicInputStream::createBinaryReader(cpFilename);

			IntDay di = 0;
			stream->read("di", &di);

			if (di != rt.di) {
				PWarning("Invalid checkpoint on di: %d [Date = %u, Filename = %s]. Expecting: %d, Found: %d. NOT USING ...", rt.di, rt.dates[rt.di], cpFilename, rt.di, di);
				return false;
			}

			size_t numRows = 0;
			size_t numCols = 0;
			size_t depth = 0;
			std::string universeName;

			stream->read("universeName", &universeName);
			stream->read("rows", &numRows);
			stream->read("cols", &numCols);
			stream->read("depth", &depth);

			/// Since these many columns were written (and the universe may have changed which will change the size below)
			/// we just read this many columns to avoid buffer overflow.
			size_t numColsToRead = numCols;

			if (!ar.alpha->data().universe) {
				ar.alpha->data().universe = rt.dataRegistry->getUniverse(universeName);

				/// Get the latest size of the universe
				numCols = std::max(ar.alpha->data().universe->size(rt.diEnd), numCols);

				ar.alpha->data().ensureAlphaSize(numRows, numCols);
			}

			FloatMatrixData& calpha = ar.alpha->alpha();
			calpha.ensureSizeAndDepth(numRows, numCols, depth);

			for (size_t kk = 0; kk < depth; kk++) {
				stream->read("ptr", reinterpret_cast<char*>(calpha.fdata(kk)), numColsToRead * numCols * calpha.itemSize());
			}

			clearInvalid(rt, ar.alpha->data().alpha, ar.alpha->data().universe);

			return true;
		}
		catch (const std::exception& e) {
			Error("While saving checkpoint: %s - Got exception: %s", cpFilename, std::string(e.what()));
		}
		catch (...) {
		}

		return false;
	}

	void AlphaExec::execCombination(const RuntimeData& rt, const ConfigSection& comboConfig, AlphaResult& ar, const std::string& firstAlphaUniverse) {
		PROFILE_SCOPED();

		//if (!ar.childAlphas.size())
		//	return;

		std::string comboUuid = comboConfig.getRequired("uuid");

		if (!ar.alpha) {
			bool calcPortfolio = parentConfig()->portfolio().getBool("calcPortfolio", true);
			std::string comboId = comboConfig.getRequired("id");

			bool isDummy = !calcPortfolio || comboId == "DummyCombination";

			ar.alpha = std::make_shared<ComboResult>(comboUuid, comboConfig);
			if(isDummy)
				(const_cast<ConfigSection&>(ar.alpha->config())).set("calculatePnl", "false");

			/// If we have just one alpha (or its a dummy)
			if (ar.childAlphas.size() == 1 || (isDummy && ar.childAlphas.size() > 0)) {
				ar.alpha->universe() = ar.childAlphas[0].alpha->universe();
			}
			else if (ar.childAlphas.size() == 0 || isDummy) {
				ASSERT(!firstAlphaUniverse.empty(), "No valid alphas under the Alphas block!");
				PTrace("No alphas on date: %u. Using universe: %s", rt.dates[rt.di], firstAlphaUniverse);
				IUniverse* universe = rt.dataRegistry->getUniverse(firstAlphaUniverse);
				ar.alpha->universe() = universe;
				ar.alpha->data().ensureAlphaSize(1, ar.alpha->universe()->size(rt.di));
			}
			else {
				auto iter = m_comboUniverses.find(comboUuid);
				bool isMergedUniverse = false;

				if (iter == m_comboUniverses.end()) {
					/// otherwise we check whether they all have the same universe
					std::vector<IUniverse*> universes;
					IUniverseMap uniqueUniverses;

					for (size_t aidx = 0; aidx < ar.childAlphas.size(); aidx++) {
						IUniverse* caUniverse = ar.childAlphas[aidx].alpha->universe();
						std::string uname = caUniverse->name();
						auto uiter = uniqueUniverses.find(uname);

						if (uiter == uniqueUniverses.end()) {
							universes.push_back(caUniverse);
							uniqueUniverses[uname] = caUniverse;
						}
					}

					/// If the universe match then the universe of the combo result is going to be the same
					if (universes.size() == 1) 
						ar.alpha->universe() = universes[0];
					else {
						isMergedUniverse = true;

						/// See if the combo defines a universeId
						std::string comboUniverseId = comboConfig.getString("universeId", "");

						if (comboUniverseId.empty()) {
							/// OK, the universe do not match ... We now need to combine the universes together
							/// and save the result in the DataRegistry
							/// Lets merge the universes
							helper::MergedUniversePtr mergedUniverse = nullptr;
							std::string name = helper::MergedUniverse::makeName(universes);
							ar.alpha->universe() = rt.dataRegistry->getUniverse(name);
							m_comboUniverses[comboUuid] = ar.alpha->universe();
						}
						else {
							ar.alpha->universe() = rt.dataRegistry->getUniverse(comboUniverseId);
						}
					}

					//m_comboUniverses[comboUuid] = ar.alpha->universe();
				}
				else
					ar.alpha->universe() = iter->second;

				if (ar.alpha->universe()->isMerged() || isMergedUniverse) {
					/// Now what we're going to do is remap the existing alphas to this universe 
					for (auto& car : ar.childAlphas) {
						car.alpha->data().remappedUniverse = ar.alpha->universe();
						car.alpha->data().remappedAlpha = std::make_shared<FloatMatrixData>(nullptr, 0, 0);
						ar.alpha->universe()->remap(rt.di, car.alpha->data().alpha.get(), car.alpha->data().universe, *car.alpha->data().remappedAlpha.get());
					}
				}
			}
		}
		else
			ar.alpha->resetState();

		float comboWeight = comboConfig.get<float>("weight", 1.0f);
		ar.alpha->weight() = comboWeight;

		//IAlphaPtr prevAlpha = nullptr;

		//{
		//	AUTO_LOCK(m_asyncMutex);

		//	/// Make yesterday's data available to the Alpha as well
		//	auto piter = m_lastAlpha.find(comboUuid);
		//	if (piter != m_lastAlpha.end())
		//		prevAlpha = piter->second;
		//	else
		//		prevAlpha = findAlpha(rt, nullptr, comboUuid);

		//	/// Set the stats for yesterday
		//	if (prevAlpha)
		//		ar.alpha->yesterdayDataRef() = prevAlpha->dataPtr();

		//	m_lastAlpha[ar.alpha->uuid()] = ar.alpha;
		//}

		auto combinationIter = m_combinationMap.find(comboUuid);
		ICombinationPtr combination = nullptr;

		if (combinationIter != m_combinationMap.end()) {
			combination = combinationIter->second;
		}
		else {
			combination = helper::ShLib::global().createComponent<ICombination>(comboConfig, rt.dataRegistry);
			ASSERT(combination, "Unable to create combination from config: " << comboConfig.toString());
			m_combinationMap[comboUuid] = combination;
		}

		/// Combos are already called in the correct order
		/// This needs to be tracked from bottom up, so we calculate children 
		/// first before calculating the parent!
		//for (auto& car : ar.childAlphas)
		//	execCombination(rt, comboConfig, car);

		combination->alphas() = ar;
		combination->run(rt);

		/// Put the combination in the list as well
		m_lastAlpha[ar.alpha->uuid()] = ar.alpha;

		ar.data = ar.alpha->dataPtr();

		saveCheckpoint(rt, comboConfig, ar);
	}

	void AlphaExec::finishAlpha(const RuntimeData& rt) {
		PROFILE_SCOPED();

		m_alphaAggregateBookSize = 0.0f;
		AlphaResultPtr result = nullptr;

		/// Just finish normally ...
		auto* alphasConfig = const_cast<Config*>(parentConfig())->portfolio().alphasSec();

		/// Ok we have a valid alphas section, we just execute all the child alphas
		if (alphasConfig) {
			result = AlphaResultPtr(new AlphaResult());
			execAlphas(rt, *alphasConfig, *result);
		}
		else {
			ConfigSectionPVec allAlphas(const_cast<Config*>(parentConfig())->portfolio().alphas());
			ASSERT(allAlphas.size() == 1, "If there is no 'Alphas' section under the 'Portfolio' section then the 'Portfolio' section MUST contain ONLY 1 'Alpha'! Num found: " << allAlphas.size());
			auto* alphaConfig = allAlphas[0];
			AlphaResult* ar = new AlphaResult();
			execAlphaDirect(&rt, alphaConfig, ar);

			result = AlphaResultPtr(ar);
		}


		auto dataId = IAlpha::genId(rt.di);

		/// Finally we set the data in the DataRegistry ...
		AlphaResultData* resultData = nullptr;
		if (result->alpha)
			resultData = new AlphaResultData(nullptr, result, 0, DataDefinition());

		PTrace("Setting alpha data: %s", dataId);
		rt.dataRegistry->setData(dataId, resultData);

		if (m_aggregateDaily && m_aggregateBookSize) {
			auto& port = const_cast<ConfigSection&>(parentConfig()->portfolio());
			float existingBookSize = port.bookSize();
			if (existingBookSize != m_alphaAggregateBookSize) {
				PInfo("[%u] Changing portfolio bookSize from: %0.2hf => %0.2hf", rt.dates[rt.di], existingBookSize, m_alphaAggregateBookSize);
				port.set("bookSize", m_alphaAggregateBookSize);
			}
		}
	}

	void AlphaExec::tick(const RuntimeData& rt) {
		PROFILE_SCOPED();

		PTrace("AlphaExec::exec");

		/// Clear data from 2 days ago ... we don't need it anymore ...
		rt.dataRegistry->clearData(IAlpha::genId(rt.di - 2));
		finishAlpha(rt);
	}

	void AlphaExec::prepare(RuntimeData& rt) {
		/// create empty data for all the days of the simulation ...
		PTrace("Preparing empty alpha buffers");

		m_lastDi = rt.diStart;

		for (IntDay di = 0; di <= rt.diEnd; di++)
			rt.dataRegistry->setData(IAlpha::genId(di), nullptr);

		getChildStrategies(rt, parentConfig()->portfolio());
		execChildStrategies(rt);

		checkParentIds(rt, parentConfig()->portfolio());
	}

	void AlphaExec::checkParentIds(const RuntimeData& rt, const ConfigSection& alphasConfig) {
		auto childAlphasConfigs = alphasConfig.getAllChildrenOfType("Alphas");
		if (childAlphasConfigs.size()) {
			for (auto i = 0; i < childAlphasConfigs.size(); i++)
				checkParentIds(rt, *(childAlphasConfigs[i]));
		}

		auto alphas = alphasConfig.alphas();
		std::map<std::string, ConfigSection*> alphasMap;

		for (auto i = 0; i < alphas.size(); i++) {
			auto* alphaConfig = alphas[i];
			auto uuid = alphaConfig->getRequired("uuid");
			alphasMap[uuid] = alphaConfig;

			auto parentUuid = alphaConfig->getString("parentUuid", "");

			if (m_numThreads > 1) {
				auto moduleId = alphaConfig->getString("moduleId", "");
				if (!moduleId.empty() && moduleId == "PyImpl") {
					PWarning("Python alphas cannot have threads. DISABLING!");
					m_numThreads = 1;
				}
			}

			if (!m_ignoreAlphaDates) {
				auto startDate = alphaConfig->getString("startDate", "");

				if (!startDate.empty() && startDate == "{$DB_ENDDATE}") {
					/// Read the end date from the database
					if (!m_mongoCl) 
						m_mongoCl = new io::MongoClient(parentConfig()->dbName().c_str());

					io::InputMongoStream readStream(*m_mongoCl, "Stats", "_id", uuid);
					if (readStream.isValid()) {
						int dbEndDate = 0;
						readStream.read("endDate", &dbEndDate);
						ASSERT(dbEndDate, "Invalid start date in the DB for alpha uuid: " << uuid);

						Poco::DateTime tendDate = Util::intToDate(dbEndDate);
						Poco::Timespan maxDaysBehind = Poco::Timespan(s_minDays, 0, 0, 0, 0);
						tendDate = tendDate - maxDaysBehind;

						auto newStartDate = Util::cast(Util::dateToInt(tendDate)); 
						PDebug("Changing alpha: %s start date to: %s", uuid, newStartDate);

						alphaConfig->set("startDate", newStartDate);
					}
				}
			}

			if (!parentUuid.empty()) {
				auto parentAlphaConfigIter = alphasMap.find(parentUuid);

				/// If we manage to find the parent then we use it ... 
				if (parentAlphaConfigIter != alphasMap.end()) {
					auto* parentAlphaConfig = parentAlphaConfigIter->second;
					parentAlphaConfig->set("asParent", "true");
				}
				else {
					/// Otherwise we just unset the parentUuid for this section ...
					alphaConfig->set("parentUuid", "");
				}
				//ASSERT(parentAlphaConfigIter != alphasMap.end(), "Invalid parentUuid: " << parentUuid << ". The parent MUST be defined before the children and MUST be in the same <Alphas/> section!");
			}
		}
	}

	void AlphaExec::finalise(RuntimeData& rt) {
		PTrace("Clearing all alpha buffers");

		for (IntDay di = 0; di <= rt.diEnd; di++)
			rt.dataRegistry->clearData(IAlpha::genId(di));

		/// Email the strategy information at the end of a successful execution
		if (m_childStrategies.size())
			emailStrategyInfo();

		delete m_mongoCl;
		m_mongoCl = nullptr;
	}

	void AlphaExec::getChildStrategies(const RuntimeData& rt, const ConfigSection& alphasConfig) {
		ConfigSectionPVec childStrats;
		alphasConfig.getChildStrategies(childStrats, true);

		for (auto i = 0; i < childStrats.size(); i++) {
			auto* stratConfig = childStrats[i];
			auto uuid = stratConfig->getRequired("uuid");
			AlphaStrategyInfo asInfo;

			asInfo.uuid = uuid;
			asInfo.filename = stratConfig->getString("filename", "config.xml");
			asInfo.posDir = stratConfig->getString("posDir", "pos");
			asInfo.pnlDir = stratConfig->getString("pnlDir", "pnl");
			asInfo.posFilename = stratConfig->getString("posFilename", "MyPort");
			asInfo.universeId = stratConfig->getString("universeId", "");
			asInfo.config = stratConfig;

			m_childStrategies[uuid] = asInfo;
		}
	}

	void AlphaExec::waitForChildStrategies(const RuntimeData& rt, bool noRunStrategies) {
		float aggregateBookSize = 0.0f;
		/// Now we wait for the processes to terminate
		for (auto iter = m_childStrategies.begin(); iter != m_childStrategies.end(); iter++) {
			AlphaStrategyInfo& asInfo = iter->second;

			if (asInfo.proc) {
				PDebug("Waiting for process: %s", asInfo.uuid);
				asInfo.exitCode = asInfo.proc->wait();

				delete asInfo.proc;
				asInfo.proc = nullptr;
			}
			else {
				if (asInfo.exitCode == 0) {
					if (!noRunStrategies)
						asInfo.exitCode = (int)kNoRun;
				}
			}

			const auto& defs = parentConfig()->defs();

			if (asInfo.exitCode != 0 && asInfo.exitCode != (int)kNoRun) {
				try {
					PInfo("Process %s failed with exit code: %d", asInfo.uuid, asInfo.exitCode);

					std::string host = defs.getString("smtpHost", "");
					std::string sender = defs.getString("sender", "");
					std::string recipients = defs.getString("recipients", "");
					int port = defs.get<int>("smtpPort", 25);

					if (!host.empty() && !sender.empty() && !recipients.empty()) {
						PInfo("Sending failure email for child strategy: %s", asInfo.uuid);
						helper::Email mail(host, port);
						mail.setSender(sender).addRecipients(recipients).setSubject(asInfo.uuid + " FAILED!");

						std::string message;
						Poco::format(message, "Strategy failed while being run as a child process by AlphaExec. See attached log for details.\nName: %s\nDirectory: %s\nExit code: %d",
							asInfo.uuid, asInfo.workingDir, asInfo.exitCode);
						mail.addMessage(message);

						Poco::Path fullLogPath(asInfo.workingDir);
						fullLogPath.append(asInfo.logFilename);
						mail.addTextAttachment(asInfo.uuid + ".log", fullLogPath.toString());
						mail.send();
					}
				}
				catch (const std::exception& e) {
					Util::reportException(e);
				}
			}

			PDebug("Process exit code: %d", asInfo.exitCode);
			targetDeployStrategy(asInfo);

			/// OK success here! 
			Poco::Path posPath(asInfo.workingDir);
			posPath.append(asInfo.posDir);
			posPath.append(asInfo.posFilename);
			posPath.setExtension("pos");

			/// Open one of the pos files ...
			std::string posFilename = posPath.toString();
			PDebug("Reading universe from pos file: %s", posFilename);

			Poco::FileInputStream posStream(posFilename);
			static const size_t maxLine = 8192;
			char cline[maxLine];

			while (asInfo.universeId.empty() || asInfo.bookSize < 0.0001f) {
				posStream.getline(cline, maxLine);
				ASSERT(cline[0] == '#', "Invalid pos file with no universe id: " << posFilename);
				std::string line((char*)cline + 1); /// We remove the leading # 
				Poco::trimInPlace(line);
				auto parts = Util::split(line, ":");

				if (parts.size() == 2) {
					if (parts[0] == "Universe")
						asInfo.universeId = Poco::trim(parts[1]);
					else if (parts[0] == "BookSize")
						asInfo.bookSize = Util::cast<float>(Poco::trim(parts[1]));
				}
			}

			ASSERT(!asInfo.universeId.empty(), "Invalid strategy with no universeId in the positions: " << asInfo.uuid);
			ASSERT(asInfo.bookSize > 0.0f, "Invalid strategy with no book size in the positions: " << asInfo.uuid);

			aggregateBookSize += asInfo.bookSize;

			auto& stratConfig = *asInfo.config;
			if (stratConfig.getString("id", "").empty())
				stratConfig.set("id", "PosAlpha");

			stratConfig.set("workingDir", asInfo.workingDir);
			stratConfig.set("posDir", asInfo.posDir);
			stratConfig.set("posFilename", asInfo.posFilename);
			stratConfig.set("filename", asInfo.filename);
			stratConfig.set("universeId", asInfo.universeId);
		}

		if (parentConfig()->portfolio().getBool("aggregateBookSize", false) == true) {
			auto& port = const_cast<ConfigSection&>(parentConfig()->portfolio());
			float existingBookSize = port.bookSize();
			PInfo("Changing portfolio bookSize from: %0.2hf => %0.2hf", existingBookSize, aggregateBookSize);
			port.set("bookSize", aggregateBookSize);
		}
	}

	void AlphaExec::execLimitedChildStrategies(const RuntimeData& rt, bool noRunStrategies) {
		if (m_maxConcurrentChildStrats == 0 || noRunStrategies)
			return;

		PDebug("Executing limited parallel strategies: %d", m_maxConcurrentChildStrats);
		auto iter = m_childStrategies.begin();
		size_t maxConcurrentChildStrats = (size_t)m_maxConcurrentChildStrats;
		std::vector<AlphaStrategyInfo*> runningStrats;
		auto appPath = Application::instance()->cmdPath();
		auto command = appPath.toString();

		while (iter != m_childStrategies.end()) {
			/// We fill up the max concurrent slots ...
			while (runningStrats.size() < maxConcurrentChildStrats && iter != m_childStrategies.end()) {
				AlphaStrategyInfo* asInfo = &(iter->second);
				asInfo->proc = new Poco::ProcessHandle(Poco::Process::launch(command, asInfo->procArgs, asInfo->workingDir));
				PDebug("Strategy: %s - PID: %d [Dir = %s, Filename = %s]", asInfo->uuid, (int)asInfo->proc->id(), asInfo->workingDir, asInfo->filename);

				runningStrats.push_back(asInfo);

				/// Move the iterator now ...
				iter++;
			}

			/// We do not have a timed wait in Poco library hence we wait for the batch to finish in its entirity
			for (AlphaStrategyInfo* asInfo : runningStrats) {
				/// Asynchronously check whether the process is still running ...
				PDebug("Waiting for process: %s", asInfo->uuid);

				/// Properly call wait ...
				asInfo->exitCode = asInfo->proc->wait();

				PDebug("Process %s exit code: %d", asInfo->uuid, asInfo->exitCode);

				/// Delete 
				delete asInfo->proc;
				asInfo->proc = nullptr;
			}

            /// Clear this array now ...
            runningStrats.clear();
		}
	}
 
	void AlphaExec::execChildStrategies(const RuntimeData& rt) {
		if (!m_childStrategies.size())
			return;

		PDebug("Number of child strategies: %z [Max Concurrent: %d]", m_childStrategies.size(), m_maxConcurrentChildStrats);
		auto appPath = Application::instance()->cmdPath();
		auto command = appPath.toString();
		auto stratsDir = parentConfig()->defs().getRequired("stratsDir");
		auto appOptions = Application::instance()->appOptions();
		bool noRunStrategies = parentConfig()->portfolio().getBool("noRunStrategies", false) || Application::instance()->appOptions().noRunChildStrats;
		stratsDir = parentConfig()->defs().resolve(stratsDir, nullptr);

		PDebug("App path: %s", appPath.toString());

		size_t runCount = 0;

		for (auto iter = m_childStrategies.begin(); iter != m_childStrategies.end(); iter++) {
			AlphaStrategyInfo& asInfo = iter->second;

			try {
				Poco::Path cmdDir = stratsDir;
				cmdDir.append(asInfo.uuid);

				Poco::Path configFilePath = cmdDir;
				configFilePath.append(asInfo.filename);

				Poco::File configFile(configFilePath);
				std::string logFilename = "logs/child_strat.log";

				if (!configFile.exists()) {
					PWarning("Unable to find: %s. Trying alternative paths ...", configFilePath.toString());
					cmdDir = Poco::Path(stratsDir);
					configFilePath = cmdDir;
					configFilePath.append(asInfo.uuid);
					configFilePath.setExtension("xml");

					Poco::File configFile(configFilePath);
					ASSERT(configFile.exists(), "Unable to find config at: " << configFilePath.toString());

					asInfo.filename = asInfo.uuid + ".xml";
					logFilename = "./" + asInfo.uuid + ".log";
				}

				asInfo.workingDir = cmdDir.toString();
				asInfo.logFilename = logFilename;

				if (!noRunStrategies) {
					std::vector<std::string> args;
					args.push_back("-c" + asInfo.filename);
					args.push_back("-l" + asInfo.logFilename);
					args.push_back("-O");

					if (appOptions.quiet)
						args.push_back("-Q");

					args.push_back("-Q");
					args.push_back("-N" + asInfo.uuid);
					args.push_back("-j" + Util::cast(m_numThreads));
					args.push_back("-W1000"); /// Wait for the cache timestamp

					if (!rt.appOptions.ovrStartDate.empty())
						args.push_back("-s" + rt.appOptions.ovrStartDate);

					if (!rt.appOptions.ovrEndDate.empty())
						args.push_back("-e" + rt.appOptions.ovrEndDate);

					if (!rt.appOptions.configOverrides.empty())
						args.push_back("-o" + rt.appOptions.configOverrides);

					asInfo.procArgs = args;

					/// If no concurrent
					if (m_maxConcurrentChildStrats == 0) {
						asInfo.proc = new Poco::ProcessHandle(Poco::Process::launch(command, args, asInfo.workingDir));
						PDebug("Strategy: %s - PID: %d [Dir = %s, Filename = %s]", asInfo.uuid, (int)asInfo.proc->id(), asInfo.workingDir, asInfo.filename);
						runCount++;
					}

					//if (runCount % 4 == 0) {
					//	int sleepInterval = 0;
					//	PDebug("Sleeping for %d seconds to allow existing child strategies to spawn!", sleepInterval);
					//	Poco::Thread::sleep((long)(sleepInterval * 1000));
					//}
				}
			}
			catch (...) {
				asInfo.exitCode = (int)kRunException;
			}
		}

		execLimitedChildStrategies(rt, noRunStrategies);
		waitForChildStrategies(rt, noRunStrategies);
	}

	void AlphaExec::emailStrategyInfo() {
		try {
			const auto& defs = parentConfig()->defs();
			std::string name = defs.getString("name", "");

			if (name.empty())
				return;

			std::string host = defs.getString("smtpHost", "");
			std::string sender = defs.getString("sender", "");
			std::string recipients = defs.getString("recipients", "");
			int port = defs.get<int>("smtpPort", 25);

			if (host.empty() || sender.empty() || recipients.empty()) 
				return;

			helper::Email mail(host, port);
			std::string subject = Application::instance()->appOptions().simInstanceName + " Synopsis: " + parentConfig()->defs().getString("mailSubject", name);
			mail.setSender(sender).addRecipients(recipients).setSubject(subject);

			std::string htmlMessage = "<html><body>";
			StringVec plotFilenames;

			for (auto iter = m_childStrategies.begin(); iter != m_childStrategies.end(); iter++) {
				const AlphaStrategyInfo& asInfo = iter->second;

				if (!asInfo.plotString.empty())
					plotFilenames.push_back(asInfo.plotString);

				for (const auto& attachment : asInfo.attachments) 
					mail.addTextAttachment("", attachment);

				for (const auto& inlineAttachment : asInfo.inlineAttachments) 
					mail.addImageAttachment("", inlineAttachment, false);
			}

			/// Now we add the details from the entire strategy
			Poco::Path stratDir = Poco::Path(parentConfig()->filename()).makeParent();
			std::string posFilename = Poco::Path(stratDir).append("pos").append("MyPort.pos").toString();
			std::string pnlFilename = Poco::Path(stratDir).append("pnl").append("MyPort.pnl").toString();
			std::string pnlRawFilename = Poco::Path(stratDir).append("pnl").append("MyPort_pre_opt.pnl").toString();
			//std::string pnlRawFilename = Poco::Path(stratDir).append("pnl").append("MyPort_pre_opt.pnl").toString();

			std::string dstFName = name + "_Report.pdf";
			std::string dstFilename = Poco::Path("./").append(dstFName).toString();

			plotFilenames.push_back(pnlFilename + "@" + pnlRawFilename);

			plot(plotFilenames, dstFilename);

			mail.addTextAttachment("", posFilename);
			mail.addTextAttachment("", pnlFilename);
			//mail.addTextAttachment("", pnlRawFilename);

			mail.addPDFAttachment("", dstFilename);

			mail.send();
		}
		catch (const std::exception& e) {
			Util::reportException(e);
		}
	}

	std::string AlphaExec::copyFile(const AlphaStrategyInfo& asInfo, const std::string& dir, const std::string& targetDir, const std::string& filenameSuffix, const std::string& ext) {
		Poco::Path srcFilePath(asInfo.workingDir);
		Poco::Path dstFilePath(targetDir);

		srcFilePath.append(dir);
		srcFilePath.append(asInfo.posFilename + filenameSuffix);
		srcFilePath.setExtension(ext);

		dstFilePath.append(dir);
		dstFilePath.append(asInfo.uuid + filenameSuffix);
		dstFilePath.setExtension(ext);

		std::string srcFilename = srcFilePath.toString();
		std::string dstFilename = dstFilePath.toString();

		PDebug("Copying: %s => %s", srcFilename, dstFilename);

		Poco::File srcFile(srcFilename);
		Poco::File dstDir(dstFilePath.makeParent().toString());

		dstDir.createDirectories();
		srcFile.copyTo(dstFilename);

		return dstFilename;
	}

	bool AlphaExec::plot(const StringVec& plotFilenames, const std::string& dstFilename) const {
		const auto& defs = parentConfig()->defs();
		auto plotExe = defs.getString("plot", "");
		if (plotExe.empty())
			return false;	

		//auto plotExeArgs = defs.getString("plotArgs", "");
		//plotExeArgs = defs.resolve(plotExeArgs, nullptr);
        auto plotScript = defs.resolve("{$APP}/Tools/plot.py", nullptr);

        StringVec plotArgs;

        plotArgs.push_back(plotScript);
		for (const auto& plotFilename : plotFilenames) {
            ///			inputStr += "\"" + plotFilename + "\" ";
            plotArgs.push_back(plotFilename);
		}

        //		plotArgs.push_back(inputStr);
		plotArgs.push_back("--pdf");
        plotArgs.push_back("-f" + dstFilename);

		//Poco::replaceInPlace(plotExeArgs, std::string("$$Inputs"), inputStr);
		//Poco::replaceInPlace(plotExeArgs, std::string("$$Output"), dstFilename);
		//auto args = Util::split(plotExeArgs, " ");

		try {
			PDebug("Launching plot process: %s, Args: %s [Dst: %s]", plotExe, Util::combine(plotArgs, " "), dstFilename);
			Poco::ProcessHandle plotProc(Poco::Process::launch(plotExe, plotArgs));
			int errCode = plotProc.wait();

			if (errCode == 0) 
				return true;
			else 
				PInfo("Plot failed with error code: %d", errCode);
		}
		catch (const std::exception& e) {
			Util::reportException(e);
		}

		return false;
	}

	void AlphaExec::targetDeployStrategy(AlphaStrategyInfo& asInfo) {
		/// Copy the positions and pnl files to the targetDir
		const auto& defs = parentConfig()->defs();
		bool targetDeploy = defs.getBool("targetDeploy", false);

		if (!targetDeploy)
			return;

		std::string targetDir = getTargetDir();
		if (targetDir.empty())
			return;

		targetDir = defs.resolve(targetDir, nullptr);

		auto posFilename = copyFile(asInfo, asInfo.posDir, targetDir, "", "pos");
		//auto pnlFilename = copyFile(asInfo, asInfo.pnlDir, targetDir, "", "pnl");
		//auto pnlRawFilename = copyFile(asInfo, asInfo.pnlDir, targetDir, "_raw", "pnl");

        Poco::Path pnlFilePath(asInfo.workingDir), pnlFilePathRaw(asInfo.workingDir);
        pnlFilePath.append(asInfo.pnlDir);
        pnlFilePath.append(asInfo.posFilename);
        pnlFilePath.setExtension("pnl");
 
        pnlFilePathRaw.append(asInfo.pnlDir);
        pnlFilePathRaw.append(asInfo.posFilename + "_pre_opt");
        pnlFilePathRaw.setExtension("pnl");

        asInfo.plotString = pnlFilePath.toString(); /// + "@" + pnlFilePathRaw.toString();

        //		asInfo.plotString = pnlFilename + "@" + pnlRawFilename;

		//asInfo.attachments.push_back(posFilename);
		//asInfo.attachments.push_back(pnlRawFilename);
		//asInfo.attachments.push_back(pnlFilename);



		//Poco::Path dstFilePath(targetDir);
		//dstFilePath.append(asInfo.uuid);
		//dstFilePath.setExtension("png");

		//std::string dstFilename = dstFilePath.toString();

		//if (plot(pnlFilename, pnlRawFilename, dstFilename))
		//	asInfo.inlineAttachments.push_back(dstFilename);
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IPipelineComponent* createAlphaExec(const pesa::ConfigSection& config) {
	return new pesa::AlphaExec((const pesa::ConfigSection&)config);
}
