/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// GenericDataDumper.cpp
///
/// Created on: 20 Jul 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Framework/Helper/FileDataLoader.h"
#include <unordered_map>

#include "Poco/File.h"
#include "Poco/FileStream.h"
#include "Poco/DateTime.h"
#include "Poco/Timestamp.h"
#include "PesaImpl/Components/SP/SpRest.h"

#include "Framework/Helper/FileDataLoader.h"

#include <Poco/Exception.h>
#include <Poco/Zip/Compress.h>
#include <Poco/DateTime.h>
#include <Poco/MemoryStream.h>
#include "Poco/Thread.h"

#include <iostream>
#include <fstream>

#include "Core/IO/BasicStream.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// DumpDayRange: Dumps a series of days
	////////////////////////////////////////////////////////////////////////////
	class DumpDayRange : public Poco::Runnable {
	private: 
		const RuntimeData&		m_rt;
		IDataRegistry&			m_dr;
		const ConfigSection*	m_sec;
		IntDay					m_diStart;
		IntDay					m_diEnd;
		std::string				m_separator = "|";

	public:
								DumpDayRange(const RuntimeData& rt, IDataRegistry& dr, const ConfigSection* sec, IntDay diStart, IntDay diEnd, const std::string& separator);
		static bool				duplicate(const RuntimeData& rt, IDataRegistry& dr, const ConfigSection* sec, IntDay di, const std::string& srcFilename);
		static bool				dump(const RuntimeData& rt, IDataRegistry& dr, const ConfigSection* sec, IntDay di, const std::string& separator, bool deleteFile = true, std::string* outFilename = nullptr);
		static std::string		getDataValue(const RuntimeData& rt, IDataRegistry& dr, IUniverse* secMaster, const std::string& dataId, IntDay di, Security::Index ii);

		void					run();
	};

	DumpDayRange::DumpDayRange(const RuntimeData& rt, IDataRegistry& dr, const ConfigSection* sec, IntDay diStart, IntDay diEnd, const std::string& separator)
		: m_rt(rt), m_dr(dr), m_sec(sec), m_diStart(diStart), m_diEnd(diEnd), m_separator(separator) {
	}

	void DumpDayRange::run() {
		for (IntDay di = m_diStart; di <= m_diEnd; di++)
			DumpDayRange::dump(m_rt, m_dr, m_sec, di, m_separator);
	}

	std::string DumpDayRange::getDataValue(const RuntimeData& rt, IDataRegistry& dr, IUniverse* secMaster, const std::string& dataId, IntDay di, Security::Index ii) {
		DataPtr data = dr.getDataPtr(secMaster, dataId, true);
		ASSERT(data, "Unable to load data: " << dataId);

		IntDay numRows = (IntDay)data->rows();
		auto dii = std::min(di, numRows - 1);
		std::string value;

		if (data->isValid(dii, ii))
			value = data->getString(dii, ii, 0);

		return std::move(value);
	}

	bool DumpDayRange::duplicate(const pesa::RuntimeData &rt, pesa::IDataRegistry &dr, const pesa::ConfigSection *sec, pesa::IntDay di,
                                 const std::string &srcFilename) {

        IntDate date = rt.dates[di];
        auto fieldLabels = sec->getResolvedRequiredString("fieldLabels", date);
        auto name = sec->getResolvedRequiredString("name", date);
        auto fname = Poco::format("%s_%u.csv", name, date);
        auto outputDir = sec->getResolvedRequiredString("outputDir", date);

        Poco::Path filePath(outputDir);

        auto fields = Util::split(sec->getResolvedRequiredString("fields"), ",");

        //filePath.append(name);

        Poco::Path fileDir = filePath;
        Poco::File dir(filePath.toString());
        dir.createDirectories();

        filePath.append(fname);

        std::string filename = filePath.toString();
        std::string zipFilename = filename + ".zip";

        Poco::File file(filename);
        Poco::File zfile(zipFilename);

        /// If the file already exists then don't create a new one (unless it's the last day of the simulation) ...
        if (zfile.exists() && !rt.isLastDi(di))
            return false;

        Poco::File srcFile(srcFilename);
        srcFile.copyTo(filename);

        std::ofstream ostr(zipFilename, std::ios::binary);
        Poco::Zip::Compress compress(ostr, true);

        compress.addFile(filePath, fname);

        compress.close();
        ostr.close();

        file.remove(false);

        Util::info("DDMP", "[%s] Copied: %s => %s", name, srcFilename, zipFilename);

	}

	bool DumpDayRange::dump(const RuntimeData& rt, IDataRegistry& dr, const ConfigSection* sec, IntDay di, const std::string& separator, bool deleteFile /* = true */, std::string* outFilename /* = nullptr */) {
		IntDate date = rt.dates[di];
		auto fieldLabels = sec->getResolvedRequiredString("fieldLabels", date);
		auto name = sec->getResolvedRequiredString("name", date);
		auto fname = Poco::format("%s_%u.csv", name, date);
		auto emptyCheck = sec->getResolvedString("emptyCheck", "");
		auto outputDir = sec->getResolvedRequiredString("outputDir", date);

		Poco::Path filePath(outputDir);

		auto fields = Util::split(sec->getResolvedRequiredString("fields"), ",");

		//filePath.append(name);

		Poco::Path fileDir = filePath;
		Poco::File dir(filePath.toString());
		dir.createDirectories();

		filePath.append(fname);

		std::string filename = filePath.toString();
		std::string zipFilename = filename + ".zip";

		Poco::File file(filename);
		Poco::File zfile(zipFilename);

		/// If the file already exists then don't create a new one (unless it's the last day of the simulation) ...
		if (zfile.exists() && !rt.isLastDi(di))
			return false;

		/// Otherwise we create the file
		auto secMaster = dr.getSecurityMaster();
		auto size = secMaster->size(di);

		FILE* f = fopen(filename.c_str(), "w");

		fprintf(f, "%s\n", fieldLabels.c_str());

		for (auto ii = 0; ii < size; ii++) {
			auto sec = secMaster->security(di, ii);

			if (!emptyCheck.empty()) {
				auto data = dr.getDataPtr(secMaster, emptyCheck, true);
				auto dii = std::min(di, (IntDay)data->rows() - 1);
				ASSERT(data, "Unable to load data: " << emptyCheck);

				auto svalue = data->getString(dii, ii, 0);

				if (!data->isValid(dii, ii) || svalue.empty())
					continue;
			}

			std::string row = getDataValue(rt, dr, secMaster, fields[0], di, ii);  // Poco::format("%u %s %u", date, separator, sec.index);

			for (size_t dataIndex = 1; dataIndex < fields.size(); dataIndex++) {
				auto value = getDataValue(rt, dr, secMaster, fields[dataIndex], di, ii);
				row += Poco::format(" %s %s", separator, value);
			}

			//for (const auto& dataId : fields) {
			//	value
			//	DataPtr data = dr.getDataPtr(secMaster, dataId, true);
			//	ASSERT(data, "Unable to load data: " << dataId);

			//	IntDay numRows = (IntDay)data->rows();
			//	auto dii = std::min(di, numRows - 1);
			//	std::string value;

			//	if (data->isValid(dii, ii))
			//		value = data->getString(dii, ii, 0);

			//	row +=  Poco::format(" %s %s", separator, value);
			//}

			fprintf(f, "%s\n", row.c_str());
		}

		fclose(f);

		std::ofstream ostr(zipFilename, std::ios::binary);
		Poco::Zip::Compress compress(ostr, true);

		compress.addFile(filePath, fname);

		compress.close();
		ostr.close();

        if (deleteFile)
		    file.remove(false);

        if (outFilename)
            *outFilename = filename;

		Util::info("DDMP", "[%s] Dumped: %s", name, zipFilename);

		return true;
	}



	////////////////////////////////////////////////////////////////////////////
	/// GenericDataDumper: Dumps the data into a CSV at the end of the data update process
	////////////////////////////////////////////////////////////////////////////
	class GenericDataDumper : public IPipelineComponent {
	private:
		std::string				m_separator = "|";
		bool					dumpSection(const RuntimeData& rt, const ConfigSection* sec);

	public:
		GenericDataDumper(const ConfigSection& config);
		virtual 				~GenericDataDumper();

		////////////////////////////////////////////////////////////////////////////
		/// IPipelineComponent overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			prepare(RuntimeData& rt);
		virtual void 			tick(const RuntimeData& rt) {}
	};

	GenericDataDumper::GenericDataDumper(const ConfigSection& config) : IPipelineComponent(config, "DDMP") {
		m_separator				= config.getString("separator", m_separator);
	}

	GenericDataDumper::~GenericDataDumper() {
		trace("Deleting GenericDataDumper!");
	}

	void GenericDataDumper::prepare(RuntimeData& rt) {
		if (!rt.appOptions.updateDataOnly) {
			PInfo("GenericDataDumper only runs during data updates!");
			return;
		}

		auto children = config().children();

		for (const auto* configSec : children) {
			if (configSec->name() == "Dump" || configSec->name() == "dump")
				dumpSection(rt, configSec);
		}
	}

	bool GenericDataDumper::dumpSection(const RuntimeData& rt, const ConfigSection* sec) {
		IDataRegistry& dr = *rt.dataRegistry;

		//PInfo("Dumping: %s", name);

		/// Checkpoint the DataRegistry
		auto cp = dr.createDataCheckpoint();

		int delay = sec->getInt("delay", 1);
		int numThreads = sec->getInt("threads", 1);
		bool replicate = sec->getBool("replicate", false);
		IntDay diStart = 0;
		IntDay diEnd = rt.diEnd - delay;
		IntDay numDays = diEnd - diStart;
		IntDay numDaysPerThread = (IntDay)((float)numDays / (float)numThreads);

		/// If the config is set to NOT replicate the same file over and over again, then just follow the normal process ...
		/// OR if we have too few days then follow the normal process as well. The replicate optimisation isn't really helpful in thsi scenario
		if (!replicate || (replicate && numDays < 10)) {
            /// If there is just one thread OR days per thread is something small then we just use one thread
            if (numThreads <= 1 || numDays < 10) {
                for (IntDay di = diStart; di <= diEnd; di++)
                    DumpDayRange::dump(rt, dr, sec, di, m_separator);
            }
            else {
                std::vector<DumpDayRange*> ddrs(numThreads);
                std::vector<Poco::Thread*> dumpThreads(numThreads);
                int t = 0;

                for (int t = 0; t < numThreads - 1; t++) {
                    DumpDayRange* ddr = new DumpDayRange(rt, dr, sec, diStart, diStart + numDaysPerThread, m_separator);
                    ddrs[t] = ddr;
                    dumpThreads[t] = new Poco::Thread();
                    dumpThreads[t]->start(*ddr);
                    diStart += numDaysPerThread + 1;
                }

                ddrs[numThreads - 1] = new DumpDayRange(rt, dr, sec, diStart, diEnd, m_separator);
                dumpThreads[numThreads - 1] = new Poco::Thread();
                dumpThreads[numThreads - 1]->start(*ddrs[numThreads - 1]);

                for (size_t t = 0; t < dumpThreads.size(); t++) {
                    dumpThreads[t]->join();
                    delete ddrs[t];
                    delete dumpThreads[t];
                }
            }
        }
        else {
		    /// Otherwise, we write one file and then copy that across
		    std::string srcFilename;

            DumpDayRange::dump(rt, dr, sec, diStart, m_separator, true, &srcFilename);

            /// Now copy this file across
            for (IntDay di = diStart + 1; di <= diEnd; di++)
                DumpDayRange::duplicate(rt, dr, sec, di, srcFilename);

            /// In the end we delete the original source file!
            Poco::File srcFile(srcFilename);
            srcFile.remove(false);
		}


		/// Delete the checkpoint and free up the memory
		dr.deleteDataCheckpoint(cp);

		return true;
	}
} /// namespace pesa

  ////////////////////////////////////////////////////////////////////////////
  /// To be called from the simulator
  ////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IPipelineComponent* createGenericDataDumper(const pesa::ConfigSection& config) {
	return new pesa::GenericDataDumper((const pesa::ConfigSection&)config);
}
