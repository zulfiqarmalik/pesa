/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// MS_PreprocessDump.cpp
///
/// Created on: 20 Jul 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Framework/Helper/FileDataLoader.h"
#include <unordered_map>

#include "Poco/File.h"
#include "Poco/FileStream.h"
#include "Poco/DateTime.h"
#include "Poco/Timestamp.h"
#include "PesaImpl/Components/SP/SpRest.h"

#include "Framework/Helper/FileDataLoader.h"

#include <Poco/Exception.h>
#include <Poco/Zip/Compress.h>
#include <Poco/DateTime.h>
#include <Poco/MemoryStream.h>

#include <iostream>
#include <fstream>

#include "Core/IO/BasicStream.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// MS_PreprocessDump: Dumps the data into a CSV at the end of the data update process
	////////////////////////////////////////////////////////////////////////////
	class MS_PreprocessDump : public IPipelineComponent {
	private:
		std::string				m_baseDir;				/// The base directory for the output
		std::string				m_fieldLabels;			/// The field labels
		StringVec				m_fields;				/// The actual fields to output in addition to some of the basic ones

		bool					dump(const RuntimeData& rt, IDataRegistry& dr, IUniverse* secMaster, IntDay di, Poco::Path baseDir);

	public:
								MS_PreprocessDump(const ConfigSection& config);
		virtual 				~MS_PreprocessDump();

		////////////////////////////////////////////////////////////////////////////
		/// IPipelineComponent overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			prepare(RuntimeData& rt);
		virtual void 			tick(const RuntimeData& rt) {}
	};

	MS_PreprocessDump::MS_PreprocessDump(const ConfigSection& config) : IPipelineComponent(config, "DDMP") {
		m_baseDir				= config.getResolvedRequiredString("outputDir");
		m_fieldLabels			= config.getResolvedRequiredString("fieldLabels");
		
		auto fields				= config.getResolvedRequiredString("fields");
		m_fields				= Util::split(fields, ",");
	}

	MS_PreprocessDump::~MS_PreprocessDump() {
		trace("Deleting MS_PreprocessDump!");
	}

	void MS_PreprocessDump::prepare(RuntimeData& rt) {
		if (!rt.appOptions.updateDataOnly) {
			PInfo("MS_PreprocessDump only runs during data updates!");
			return;
		}

		Poco::Path baseDir(m_baseDir);
		IDataRegistry& dr		= *rt.dataRegistry;
		IUniverse* secMaster	= dr.getSecurityMaster();

		baseDir.append(secMaster->name());

		for (IntDay di = 0; di != rt.diEnd; di++) 
			dump(rt, dr, secMaster, di, baseDir);
	}

	bool MS_PreprocessDump::dump(const RuntimeData& rt, IDataRegistry& dr, IUniverse* secMaster, IntDay di, Poco::Path filePath) {
		IntDate date			= rt.dates[di];
		std::string fname		= Poco::format("%s_%u.csv", secMaster->name(), date);

		Poco::Path fileDir = filePath;
		Poco::File dir(filePath.toString());
		dir.createDirectories();
		
		filePath.append(fname);

		std::string filename	= filePath.toString();
		std::string zipFilename = filename + ".zip";

		Poco::File file(filename);
		Poco::File zfile(zipFilename);

		/// If the file already exists then don't create a new one (unless it's the last day of the simulation) ...
		if (zfile.exists() && !rt.isLastDi(di))
			return false;

		/// Otherwise we create the file
		auto size				= secMaster->size(di);

		FILE* f					= fopen(filename.c_str(), "w");
		auto header				= Poco::format("Date, SID, %s", m_fieldLabels);

		fprintf(f, "Number, Date, %s\n", m_fieldLabels.c_str());

		for (auto ii = 0; ii < size; ii++) {
			auto sec = secMaster->security(di, ii);
			std::string row		= Poco::format("%u, %u", date, sec.index);

			for (const auto& dataId : m_fields) {
				DataPtr data	= dr.getDataPtr(secMaster, dataId, true);
				ASSERT(data, "Unable to load data: " << dataId);

				IntDay numRows	= (IntDay)data->rows();
				auto value		= data->getString(std::min(di, numRows - 1), ii, 0);
				row += ", " + value;
			}

			fprintf(f, "%s\n", row.c_str());
		}

		fclose(f);

		std::ofstream ostr(zipFilename, std::ios::binary);
		Poco::Zip::Compress compress(ostr, true);

		compress.addFile(filePath, fname);

		compress.close();
		ostr.close();

		file.remove(false);
		PInfo("Wrote: %s", zipFilename);

		return true;
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IPipelineComponent* createMS_PreprocessDump(const pesa::ConfigSection& config) {
	return new pesa::MS_PreprocessDump((const pesa::ConfigSection&)config);
}
