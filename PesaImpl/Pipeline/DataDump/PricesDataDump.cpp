/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// PricesDataDump.cpp
///
/// Created on: 17 Sep 2018
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Framework/Helper/FileDataLoader.h"
#include <unordered_map>

#include "Poco/File.h"
#include "Poco/FileStream.h"
#include "Poco/DateTime.h"
#include "Poco/Timestamp.h"
#include "PesaImpl/Components/SP/SpRest.h"

#include "Framework/Helper/FileDataLoader.h"

#include <Poco/Exception.h>
#include <Poco/Zip/Compress.h>
#include <Poco/DateTime.h>
#include <Poco/MemoryStream.h>

#include <iostream>
#include <fstream>

#include "Core/IO/BasicStream.h"

namespace pesa {
	struct BBSecInfo {
		std::string				ticker;					/// The generic ticker
		std::string				bbTicker;				/// The bloomberg ticker
		std::string				figi;					/// The figi
		std::string				cfigi;					/// The composite figi
		std::string				parentFigi;				/// The figi of the parent
		std::string				isin;					/// The isin
		std::string				name;					/// The name of the security
		std::string				marketStatus;			/// The market status
		std::string				country;				/// The country
		std::string				sector;					/// The sector of this security
		std::string				industry;				/// The industry of this security
		std::string				subIndustry;			/// The sub-industry of this security
		std::string				group;					/// The industry group of this security
	};
	////////////////////////////////////////////////////////////////////////////
	/// PricesDataDump: Dumps the data into a CSV at the end of the data update process
	////////////////////////////////////////////////////////////////////////////
	class PricesDataDump : public IPipelineComponent {
	private:
		typedef std::unordered_map<std::string, size_t> BBIndexMap;

		std::string				m_separator = "|";		/// The separator that we're going to use
		std::vector<BBSecInfo>	m_bbSecurities;			/// The BB securities that we've loaded
		BBIndexMap				m_bbLUT;				/// The BB securities lookup table

		void					dumpDay(const RuntimeData& rt, IDataRegistry& dr, IntDay di);
		void					loadBBPrices(const RuntimeData& rt, IDataRegistry& dr, IntDay di);
		void					loadIdMap(const RuntimeData& rt, IDataRegistry& dr);

	public:
								PricesDataDump(const ConfigSection& config);
		virtual 				~PricesDataDump();

		////////////////////////////////////////////////////////////////////////////
		/// IPipelineComponent overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			prepare(RuntimeData& rt);
		virtual void 			tick(const RuntimeData& rt) {}
	};

	PricesDataDump::PricesDataDump(const ConfigSection& config) : IPipelineComponent(config, "DDMP") {
		m_separator				= config.getString("separator", m_separator);
	}

	PricesDataDump::~PricesDataDump() {
		trace("Deleting PricesDataDump!");
	}

	void PricesDataDump::prepare(RuntimeData& rt) {
		if (!rt.appOptions.updateDataOnly) {
			PInfo("PricesDataDump only runs during data updates!");
			return;
		}


		IDataRegistry& dr = *rt.dataRegistry;

		loadIdMap(rt, dr);

		/// Checkpoint the DataRegistry
		auto cp = dr.createDataCheckpoint();

		int delay = sec->getInt("delay", 1);

		for (IntDay di = 0; di <= rt.diEnd - delay; di++)
			dumpDay(rt, dr, di);

		/// Delete the checkpoint and free up the memory
		dr.deleteDataCheckpoint(cp);

		return true;
	}

	void PricesDataDump::loadIdMap(const RuntimeData& rt, IDataRegistry& dr) {
		std::string filename	= config().getResolvedRequiredString("idMap");
		helper::FileDataLoader reader(filename, m_separator.c_str(), false, true);
		StringVec values		= reader.read();

		while (values.size()) {
			BBSecInfo bsi;

			bsi.ticker			= Util::split(values[0], " ")[0];
			bsi.bbTicker		= Poco::replace(values[1], " Equity", "");
			bsi.name			= values[2];
			bsi.marketStatus	= values[3];
			bsi.country			= values[4];
			bsi.figi			= values[5];
			bsi.cfigi			= values[6];
			bsi.parentFigi		= values[7];
			bsi.isin			= values[8];
			bsi.sector			= values[9];
			bsi.industry		= values[10];
			bsi.subIndustry		= values[11];
			bsi.group			= values[12];

			/// Read the next line. Lots of conditionals below
			values				= reader.read();

			if (bsi.cfigi.empty())
				continue;

			/// This happens with Bloomberg
			if (bsi.isin == "nan")
				bsi.isin		= "";

			auto iter			= m_bbLUT.find(bsi.cfigi);

			if (iter != m_bbLUT.end())
				continue;

			size_t index		= m_bbSecurities.size();

			m_bbLUT[bsi.cfigi]	= index;
			m_bbLUT[bsi.figi]	= index;

			if (!bsi.isin.empty())
				m_bbLUT[bsi.isin]= index;

			m_bbSecurities.push_back(bsi);
		}
	}

	bool PricesDataDump::dumpDay(const RuntimeData& rt, IDataRegistry& dr, IntDay di) {
		auto* sec			= config();

		IntDate date		= rt.dates[di];
		auto fieldLabels	= sec->getResolvedRequiredString("fieldLabels", date);
		auto name			= sec->getResolvedRequiredString("name", date);
		auto fname			= Poco::format("%s_%u.csv", name, date);
		auto emptyCheck		= sec->getResolvedString("emptyCheck", "");
		auto outputDir		= sec->getResolvedRequiredString("outputDir", date);

		Poco::Path filePath(outputDir);

		auto fields			= Util::split(sec->getResolvedRequiredString("fields"), ",");

		//filePath.append(name);

		Poco::Path fileDir	= filePath;
		Poco::File dir(filePath.toString());
		dir.createDirectories();

		filePath.append(fname);

		std::string filename = filePath.toString();
		std::string zipFilename = filename + ".zip";

		Poco::File file(filename);
		Poco::File zfile(zipFilename);

		/// If the file already exists then don't create a new one (unless it's the last day of the simulation) ...
		if (zfile.exists() && !rt.isLastDi(di))
			return false;

		/// Otherwise we create the file
		auto secMaster = dr.getSecurityMaster();
		auto size = secMaster->size(di);

		FILE* f = fopen(filename.c_str(), "w");
		auto header = Poco::format("Date, SID, Ticker, UUID, %s", fieldLabels);

		fprintf(f, "Date %s Index %s %s\n", m_separator.c_str(), m_separator.c_str(), fieldLabels.c_str());

		for (auto ii = 0; ii < size; ii++) {
			auto sec = secMaster->security(di, ii);

			if (!emptyCheck.empty()) {
				auto data = dr.getDataPtr(secMaster, emptyCheck, true);
				auto dii = std::min(di, (IntDay)data->rows() - 1);
				ASSERT(data, "Unable to load data: " << emptyCheck);

				auto svalue = data->getString(dii, ii, 0);
				
				if (!data->isValid(dii, ii) || svalue.empty())
					continue;
			}

			std::string row = Poco::format("%u %s %u", date, m_separator, sec.index);

			for (const auto& dataId : fields) {
				DataPtr data = dr.getDataPtr(secMaster, dataId, true);
				ASSERT(data, "Unable to load data: " << dataId);

				IntDay numRows = (IntDay)data->rows();
				auto dii = std::min(di, numRows - 1);
				std::string value;

				if (data->isValid(dii, ii))
					value = data->getString(dii, ii, 0);

				row +=  Poco::format(" %s %s", m_separator, value);
			}

			fprintf(f, "%s\n", row.c_str());
		}

		fclose(f);

		std::ofstream ostr(zipFilename, std::ios::binary);
		Poco::Zip::Compress compress(ostr, true);

		compress.addFile(filePath, fname);

		compress.close();
		ostr.close();

		file.remove(false);
		PInfo("[%s] Dumped: %s", name, zipFilename);

		return true;
	}
} /// namespace pesa

  ////////////////////////////////////////////////////////////////////////////
  /// To be called from the simulator
  ////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IPipelineComponent* createPricesDataDump(const pesa::ConfigSection& config) {
	return new pesa::PricesDataDump((const pesa::ConfigSection&)config);
}
