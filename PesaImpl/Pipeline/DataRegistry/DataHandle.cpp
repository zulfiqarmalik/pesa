/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// DataHandle.cpp
///
/// Created on: 28 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "DataHandle.h"
#include "DataHeader.h"
#include "DataRegistry.h"

#include "Poco/Util/OptionException.h"

#include "Poco/File.h"
#include "Poco/FileStream.h"
#include "Poco/String.h"
#include "Poco/Glob.h"
#include "Poco/NamedMutex.h"

#include "Core/IO/BasicStream.h"

#include <memory>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#ifdef __MACOS__
	#include <sys/uio.h>
#elif __LINUX__
        #include <sys/io.h>
#else
	#include <io.h>
#endif 

#include <stdio.h>

#include "Core/Lib/Profiler.h"

#define AUTO_NAMED_MUTEX_LOCK(m) Poco::ScopedLock __sc__(m);

namespace pesa {

	DataHandle::DataHandle(IUniverse* universe, DataRegistry& dr_, IDataLoaderPtr dl_, const Dataset& ds_, const DataValue& dv_, 
		const std::string& rootPath, const Poco::Path& dataCachePath)
		: m_universe(universe), m_dr(dr_), m_dl(dl_), m_ds(ds_), m_dv(dv_)
		, m_rootPath(rootPath)
		, m_dataCachePath(dataCachePath) {
		m_hdr.version = 0;
	}

	DataHandle::~DataHandle() {
		/// We unlock the file (this will check whether its been locked at all)
		unlock();
	}

	void DataHandle::lock() {
		/// If we already have the lock then don't do anything ...
		//if (m_lock)
		//	return;

		//m_lockPath = m_dataCachePath;
		//m_lockPath.setExtension("lock");
		//std::string lockPath = m_lockPath.toString();
		//Poco::File file(lockPath);
		//file.createFile();

		//Trace("DataHandle", "Locking: %s", lockPath);
		//m_lock = new boost::interprocess::file_lock(lockPath.c_str());
		//m_lock->lock();
	}

	void DataHandle::unlock() {
		//if (m_lock) {
		//	try {
		//		m_lock->unlock();
		//		delete m_lock;
		//		m_lock = nullptr;
		//		Poco::File file(m_lockPath.toString());
		//		file.remove();
		//	}
		//	catch (...) {
		//		/// We just ignore the exception. This would normally certainly be because
		//		/// someone else might have acquired the file lock for the file that we're 
		//		/// trying to delete
		//	}
		//}
	}

	void DataHandle::shLock() {
		/// If we already have the lock then don't do anything ...
		if (m_lock)
			return;

		m_lockPath = m_dataCachePath;
		m_lockPath.setExtension("lock");
		std::string lockPath = m_lockPath.toString();
		Poco::File file(lockPath);
		file.createFile();

		Info("DataHandle", "SH Lock: %s", lockPath);
		m_lock = new boost::interprocess::file_lock(lockPath.c_str());
		m_lock->lock();
	}

	void DataHandle::shUnlock() {
		if (m_lock) {
			try {
				std::string lockPath = m_lockPath.toString();
				Info("DataHandle", "SH Unlock: %s", lockPath);

				m_lock->unlock();
				delete m_lock;
				m_lock = nullptr;
				Poco::File file(lockPath);
				file.remove();
			}
			catch (...) {
				/// We just ignore the exception. This would normally certainly be because
				/// someone else might have acquired the file lock for the file that we're 
				/// trying to delete
			}
		}
	}

	std::string DataHandle::dataCacheFilename(std::string dataCacheRoot /* = "" */) const {
		std::string dcPath;

		if (dataCacheRoot.empty())
			dcPath = m_dataCachePath.toString();
		else {
			Poco::Path cachePath;
			
			cachePath.parseDirectory(dataCacheRoot); ///, m_dataCachePath.getFileName());
			auto overrideDepth = cachePath.depth();
			auto depth = m_dataCachePath.depth();

			for (auto i = overrideDepth; i < depth; i++) {
				cachePath.append(m_dataCachePath.directory(i));
				cachePath.makeDirectory();
			}

			cachePath.setFileName(m_dataCachePath.getFileName());
			dcPath = cachePath.toString();
		}

		std::string itemTypeStr = DataType::toString(m_dv.definition.itemType);
		std::string frequencyStr = DataFrequency::toString((DataFrequency::Type)m_dv.definition.frequency);

		return dcPath + "," + itemTypeStr + "," + frequencyStr;
	}

	std::string DataHandle::metadataFilename() const {
		return dataCacheFilename() + ".meta";
	}

	void DataHandle::closeReadStream() {
		m_readStream = nullptr;
	}

	bool DataHandle::openReadStream(bool noRetry /* = false */, bool noThrow /* = true */) {
		if (m_readStream)
			return true;

		std::string dcPath = dataCacheFilename();

		try {
			ASSERT(strlen(m_dv.definition.name), "Invalid data definition: " << m_dv.toString(m_ds));

			const Config* config = m_dl->parentConfig();
			Poco::File file(dcPath);
			//Poco::Path parentDirPath = Poco::Path(m_dataCachePath).makeParent();
			//Poco::File parentDir(parentDirPath);

			//if (!parentDir.exists()) {
			//	Info("DataHandle", "Creating data cache dir: %s", parentDirPath.toString());
			//	parentDir.createDirectories();
			//}

			m_hdr.version = 0;

			/// Check if the file really exists ...
			Trace("DataHandle", "Loading header path: %s", dcPath);

			m_readStream = io::BasicInputStream::createBinaryReader(dcPath);
			m_readStream->stream()->read((char*)&m_hdr, sizeof(m_hdr));

			readFooter();

			return true;
		}
		catch (const Poco::PathNotFoundException& e) {
			if (noRetry)
				return false;

			if (!noThrow)
				throw e;

			// Oopes! The data cache path does not exist. Lets create it!
			//Info("DataHandle", "Path dose not exist: %s. Creating ...", dcPath);
			//Poco::File file(dcPath);
			//file.createDirectories();

			if (!tryCopyFromPrevVersion())
				return false;

			/// OK here we have successfully copied it now ... read the header again
			m_copyFromPrevVersion = false;

			/// Run it again ...
			return openReadStream(noRetry, false);
		}
		catch (const Poco::FileNotFoundException& e) {
			if (noRetry)
				return false;

			if (!noThrow)
				throw e;

			if (!tryCopyFromPrevVersion())
				throw e;

			m_copyFromPrevVersion = false;
			return openReadStream(noRetry, false);
		}

		return false;
	}

	/// Loads the header of a particular dataset
	void DataHandle::readHeader() {
		PROFILE_SCOPED();

		std::string dcPath = dataCacheFilename();

		if (!openReadStream()) {
			throw Poco::FileNotFoundException("File not found: " + dcPath);
		}

		std::string loaderId = m_dl->config().get("id");

		Trace("DataHandle", "DataHeader: %s", m_hdr.toString());

		if (m_hdr.version != DataHeader::s_version) {
			m_dr.info("Version mismatch in DataHeader. File version: %z, Current version: %z. Recaching ...", m_hdr.version, DataHeader::s_version);
			m_hdr.version = 0;
			throw Poco::Util::IncompatibleOptionsException("Version mismatch!");
		}
		else if (!m_hdr.isOfType(loaderId)) {
			m_dr.info("DataLoader id mismatch in header. File loader: %z, Current loader: %z. Recaching ...", m_hdr.loaderId, loaderId);
			m_hdr.version = 0;
			throw Poco::Util::IncompatibleOptionsException("Version mismatch!");
		}

		m_maxRowSize = m_hdr.rowSize;
		/// now that we have a file loaded successfully, we just wanna do an update
		m_doUpdate = true;
	}

	void DataHandle::readFooter() {
		std::string metaFilename = metadataFilename();

		try {
			Poco::File metaFile(metaFilename);

			/// Check if the file exists. If it doesn't then there is no metadata associted with this file
			if (!metaFile.exists())
				return;

			auto metaStream = io::BasicInputStream::createBinaryReader(metaFilename);

			metaStream->stream()->read((char*)&m_ftr.size, sizeof(m_ftr.size));

			if (m_ftr.size) {
				metaStream->stream()->read((char*)&m_ftr.version, sizeof(m_ftr.version)); 
				metaStream->stream()->read((char*)&m_ftr.def, sizeof(m_ftr.def));

				/// Read the meta header. This takes
				/// precedence over the actual file header. This is because writing the data
				/// and writing the metadata are NOT atomic operations. Metadata is written 
				/// after the original data is written. In the event of an issue between these 
				/// two operations, we wanna trust the information coming from the latter
				/// write rather than the former one!

				DataHeader hdr;
				metaStream->stream()->read((char*)&hdr, sizeof(hdr));

				m_hdr = hdr;

				/// Create the buffer
				m_ftr.customMetadata = DataRegistry::createDataBuffer(nullptr, m_ftr.def, nullptr);

				m_ftr.customMetadata->read(*metaStream);
			}
		}
		catch (const Poco::PathNotFoundException&) {
		}
		catch (const Poco::FileNotFoundException&) {
		}
		catch (...) {
		}
	}

	bool DataHandle::tryCopyFromPrevVersion() {
		if (!m_copyFromPrevVersion)
			return false; 

		/// We don't even attempt to copy an always overwrite file
		//if (m_dv.definition.isAlwaysOverwrite()) 
		//	return false;

		/// Now we try to copy the data from the previous version if its allowed in the config ...
		bool copyFromPrevVersion = m_dr.parentConfig()->defs().getBool("copyFromPrevVersion", false);

		/// Not allowed so we just return as usual
		if (!copyFromPrevVersion)
			return false;

		/// OK the config has allowed to copyFromPrevVersion ... now we wanna check the individual data loader
		if (m_dl) {
			copyFromPrevVersion = m_dl->config().getBool("copyFromPrevVersion", copyFromPrevVersion) && !m_dl->config().getBool("alwaysOverwrite", false);
			if (!copyFromPrevVersion)
				return false;
		}

		copyFromPrevVersion = !m_dv.definition.isNoCopyFromPrevVersion();
		if (!copyFromPrevVersion)
			return false;

		std::string prevVersion = Application::instance()->prevVersion();
		if (prevVersion.empty())
			return false;

		std::string prevDataCachePath = m_dr.parentConfig()->defs().getString("prevDataCachePath", "");
		prevDataCachePath = m_dr.parentConfig()->defs().resolve(prevDataCachePath, nullptr);

		if (prevDataCachePath.empty())
			return false;

		try {
			auto currFilename = dataCacheFilename();
			auto prevFilename = dataCacheFilename(prevDataCachePath);

			Info("DataHandle", "[%s] => [%s] ...", prevFilename, currFilename);

			Poco::File src(prevFilename);
			if (!src.exists()) {
				Info("DataHandle", "File does not exist: %s", prevFilename);
				return false;
			}

			/// Make sure that the directory exists over here, before copying!
			Util::ensureDirExists(currFilename);

			src.copyTo(currFilename);
			Info("DataHandle", "[DONE!]");

			return true;
		}
		catch (const std::exception&) {
			return false;
		}

		return false;
	}

	DataPtr DataHandle::read(const RuntimeData& rt, DataInfo* dinfo, size_t startRow /* = 0 */, size_t endRow /* = 0 */) {
		ASSERT(m_readStream, "Invalid read stream!");
		Trace("DataHandle", "Creating buffer for reading ...");

		DataPtr data = m_dr.createDataBuffer(m_universe, m_dl, m_ds, m_dv.definition);
		ASSERT(data, "Unable to create data for definition: " << m_dv.definition.toString(m_ds));

		/// Set the metadata on the data ...
		data->setMetadata(m_ftr.customMetadata);

		data->delay() = m_delay;

		read(m_readStream, m_hdr, data, rt, dinfo, startRow, endRow);

		return data;
	}

	static const int g_shMemHdrVersion = 1;
	static const int g_shMemHdrMagic = 0xDeadBeef;

	//struct ShMemHdr {
	//	int version			= g_shMemHdrVersion;
	//	int magic			= g_shMemHdrMagic;
	//	int isInit			= 0;
	//	size_t rows			= 0;
	//	size_t cols			= 0;
	//};

	void DataHandle::read(io::BasicInputStreamPtr stream, const DataHeader& hdr, DataPtr data, const RuntimeData& rt, 
		DataInfo* dinfo, size_t startRow /* = 0 */, size_t endRow /* = 0 */) {
		size_t dataSize = getStreamSize(*stream->stream()) - sizeof(DataHeader);
		size_t itemSize = data->itemSize();
		size_t diOffset = startRow;
		size_t numRows = hdr.numRows;
		size_t maxRows = endRow - startRow;
		bool loadEntire = true; //!dinfo ? false : dinfo->loadEntire;
		bool customRW = data->doesCustomReadWrite();

		std::string shMemId = Poco::replace(m_cacheId, '.', '_');
		SharedMemoryPtr shmem = nullptr;
		size_t shmemRows = 0, shmemCols = 0;

		bool useShMem = false;

		/// Only enable it on Linux for the time being
#if defined(__LINUX__) || defined(__MACOS__)
		auto appOptions = Application::instance()->appOptions();
		useShMem = ((dinfo && dinfo->useShMem) || appOptions.useShMem || appOptions.runServer) && data->doesSupportSharedMemory() && !shMemId.empty() && !rt.appOptions.updateDataOnly;
#endif 

		/// Now that we are here we try to see whether the SharedMemory has been created

		if (maxRows > 0 && numRows > maxRows)
			numRows = maxRows;

		if (numRows > rt.dates.size()) {
			numRows = rt.dates.size();
			numRows -= m_delay;
		}

		/// For fixed sized data ...
		if (!customRW) {
			if (itemSize != 0) {
				ASSERT(m_maxRowSize, "Invalid max row size: " << m_cacheId);
				ASSERT(m_maxRowSize % itemSize == 0, "Invalid max row size: " << m_maxRowSize << " - Item size: " << itemSize << ": " << m_cacheId);

				if (!hdr.def.doesUpdateInOneGo() && !loadEntire) {
					numRows = (size_t)std::max((int)((rt.diEnd - rt.diStart) + rt.backdays + 1), 0); /// 1 is because inclusive range [diStart, diEnd]
					numRows = std::min(numRows, hdr.numRows);
					diOffset = rt.diEnd - numRows + 1;
				}

				data->diOffset() = (IntDay)diOffset;

				if (!useShMem)
					data->ensureSize(numRows, m_maxRowSize / itemSize);
				else {
					shmemRows = numRows;
					shmemCols = m_maxRowSize / itemSize;
				}
			}
			else {
				Trace("DataHandle", "Reading data (%z): %z x %z (ItemSize: dynamic)", m_maxRowSize);
				numRows = 1;
				/// we treat it as a dynamic sized blob and let it handle the memory layout in postMemcpy ...
				if (useShMem)
					data->ensureSize(1, m_maxRowSize, false);
				else {
					shmemRows = 1;
					shmemCols = m_maxRowSize;
				}
			}
		}
		else {
			data->ensureSize(numRows, 1, false);
			useShMem = false;
		}

		/// version contains the size of the actual DataHeader that was written to the file
		stream->stream()->seekg(hdr.version, stream->stream()->beg);

		char* dataPtr = nullptr;

		if (!useShMem)
			dataPtr = (char*)data->data();
		else {
			size_t shmemSize = shmemRows * shmemCols * itemSize;
			Debug("DataHandle", "Creating shared memory object of size: %z = %zx%z [%s]", shmemSize, shmemRows, shmemCols, shMemId);

			ASSERT(shmemSize, "Invalid shared memory size!");

			//shmemSize += sizeof(ShMemHdr);
			if (useShMem && !dinfo->shmemAddress) {
				shmem = SharedMemoryPtr(new SharedMemory(shMemId, shmemSize, SharedMemory::AM_WRITE, true));
				if (!shmem) {
					Error("DataHandle", "Unable to open shared memory for writing data: %s", shMemId);
					return;
				}
			}
			else 
				shmem = SharedMemoryPtr(new SharedMemory(shMemId, shmemSize, SharedMemory::AM_READ, false));
			
			if (shmem) {
				ASSERT(shmem, "Unable to create shared memory of size: " << shmemSize << " - Cache ID: " << shMemId);
				data->shmemPtr() = shmem;

				//ShMemHdr* shHdr = reinterpret_cast<ShMemHdr*>(shmem->begin());

				Trace("DataHandle", "Loading from shared memory ...");
				dataPtr = shmem->begin(); // + sizeof(ShMemHdr);
                ASSERT(dataPtr, "Unable to get the underlying shared memory pointer!");

                Debug("DataHandle", "Pointer: %z [%zx%z]", reinterpret_cast<size_t>(dataPtr), shmemRows, shmemCols);
				data->fromSharedMemory(0, dataPtr, shmemRows, shmemCols);

				Debug("DataHandle", "%s (0, 0) = %s", shMemId, data->getString(0, 0, 0));

				//if (shHdr->version == g_shMemHdrVersion && shHdr->magic == g_shMemHdrMagic && 
				//	shHdr->isInit == 1 && shHdr->rows == shmemRows && shHdr->cols == shmemCols) 
				if (dinfo->shmemAddress && dinfo->useShMem) {
                    Debug("DataHandle", "ShMem object already initialised! Using directly: %s [%zx%z = %z]", shMemId, shmemRows, shmemCols, shmemSize);
					return;
				}

				//if (shHdr->version == g_shMemHdrVersion && shHdr->magic == g_shMemHdrMagic &&
				//	shHdr->isInit == 1 && shHdr->rows == shmemRows && shHdr->cols == shmemCols) 
				//{
				//	Debug("DataHandle", "ShMem object already initialised! Using directly: %s", shMemId);
				//	dataPtr = shmem->begin() + sizeof(ShMemHdr);
				//	data->fromSharedMemory(0, dataPtr, shmemRows, shmemCols);
				//	return;
				//}
			}
			else { 
				Warning("DataHandle", "Unable to load shared memory data: %s - Loading a fresh local copy ...", shMemId);

				/// Recursively call without shmem flag
				bool prevUseShmem = dinfo->useShMem;
				dinfo->useShMem = false;
				this->read(stream, hdr, data, rt, dinfo, startRow, endRow);
				dinfo->useShMem = prevUseShmem;
				return;
			}
		}

		if (!hdr.def.doesUpdateInOneGo()) {
			Trace("DataHandle", "Data: %s - Num rows to read: %z [Buffer size: %z]", std::string(hdr.def.name), numRows, data->dataSize());

			if (m_rowHeaders)
				data->rowInfos().resize(numRows);

			///// Lock the file (this will essentially lock the shared memory section) ...
			//if (useShMem)
			//	this->shLock();
				/// Once we have the lock we need to check again whether this is usable because

			for (size_t di = 0; di < numRows; di++) {
				DataRowHeader rhdr;
				size_t index = di + diOffset;
				IntDate dateToRead = rt.dates[index];

				/// If we manage to read something
				if (!customRW) {
					if (readRow(dateToRead, dataPtr, m_maxRowSize, 0, &rhdr)) {
						ASSERT(rhdr.date == dateToRead, "Dates mismatch. Expecting: " << dateToRead << " - Found: " << rhdr.date << ": " << m_cacheId);
						dataPtr += m_maxRowSize;
					}
					else {
					}

					if (m_rowHeaders)
						data->rowInfos()[di] = rhdr;
				}
				else {
					DataRowHeader rhdrRead;
					bool didSeek = readSeek(dateToRead) != 0;

					if (dateToRead >= 20181011)
						int a = 10;

					/// In case we're dumping debug data, then we just continue ...
					if (!didSeek && !rt.appOptions.dumpData.empty())
						continue;

					ASSERT(didSeek, "Unable to seek to date: " << dateToRead);
					m_readStream->stream()->read((char*)&rhdrRead, sizeof(rhdrRead));
					ASSERT(rhdrRead.date == dateToRead, "Dates mismatch. Expecting: " << dateToRead << " - Found: " << rhdr.date << ": " << m_cacheId);

					for (size_t kk = 0; kk < rhdr.depth; kk++)
						data->readRow(*m_readStream, kk, di);

					if (m_rowHeaders)
						data->rowInfos()[di] = rhdrRead;
				}
			}

			///// We need to unlock here!
			//if (useShMem)
			//	this->shUnlock();

			/// We've done a memcpy to the data [
			if (!customRW)
				data->postMemcpy();
		}
		else {
			ASSERT(numRows == 1, "Invalid number of rows for universal data: " << numRows << ": " << m_cacheId);

			if (!customRW) {
				ASSERT(dataPtr, "Invalid data pointer given while reading a non custom stream!");
				stream->stream()->read(dataPtr, m_maxRowSize);
			}
			else
				data->read(*stream);
		}

		//if (useShMem) {
		//	Trace("DataHandle", "Finalising shared memory ...!");
			//ShMemHdr shHdr;
			//shHdr.isInit = 1;
			//shHdr.rows = shmemRows;
			//shHdr.cols = shmemCols;

			//memcpy(shmem->begin(), &shHdr, sizeof(shHdr));
			//data->fromSharedMemory(0, dataPtr, shmemRows, shmemCols);

			///// Unlock the shared memory now ...
			//this->shUnlock();
		//}
	}

	bool DataHandle::readFrame(const RuntimeData& rt, DataFrame& frame, IntDay di) {
		if (di < 0)
			return false;

		if (m_dv.definition.doesUpdateInOneGo() || m_dv.definition.isSecurityMaster())
			return false;

		ASSERT(m_readStream, "Read stream manually closed but not reopened again! Call DataHandle::openReadStream to open it again!");

		Trace("DataHandle", "Creating buffer for reading frame: %d [Date = %u]", di, rt.dates[di]);
		DataPtr data = m_dr.createDataBuffer(m_universe, m_dl, m_ds, m_dv.definition);
		IntDate dateToRead = rt.dates[di];
		DataRowHeader rhdr;
		bool customRW = data->doesCustomReadWrite();

		data->setMetadata(m_ftr.customMetadata);
		data->delay() = m_delay;

		if (!customRW) {
			auto itemSize = data->itemSize();
			auto numCols = std::max(m_maxRowSize / itemSize, (size_t)1);
			data->ensureSizeAndDepth(1, numCols, 1);
			char* dataPtr = (char*)data->data();
            Trace("DataHandle", "Allocated buffer: %z", data->dataSize());

			if (readRow(dateToRead, dataPtr, m_maxRowSize, 0, &rhdr)) {
				ASSERT(rhdr.date == dateToRead, "Dates mismatch. Expecting: " << dateToRead << " - Found: " << rhdr.date << ": " << m_cacheId);
			}
			else {
			}
		}
		else {
			DataRowHeader rhdrRead;
			bool didSeek = readSeek(dateToRead) != 0;
			ASSERT(didSeek, "Unable to seek to date: " << dateToRead << ": " << m_cacheId);

			m_readStream->stream()->read((char*)&rhdrRead, sizeof(rhdrRead));
			ASSERT(rhdrRead.date == dateToRead, "Dates mismatch. Expecting: " << dateToRead << " - Found: " << rhdr.date << ": " << m_cacheId);

			data->ensureSize(1, 1, false);

			for (size_t kk = 0; kk < rhdr.depth; kk++)
				data->readRow(*m_readStream, kk, 0); /// We pass 0 in di because we're loading just one row!
		}

		/// This isalways in the corr
		data->clearUniverse();
		frame.data = data;
		frame.dataSize = data->dataSize();
		frame.dataOffset = 0;
		frame.di = di;

		return true;
	}

	void DataHandle::beginWrite(bool updateExisting, size_t rowSize) {
		PROFILE_SCOPED();

		/// Make a seek map of the existing data and then close the read stream ...
		if (updateExisting && m_needSeekMap && !m_didTruncate) {
			constructSeekMap();
			m_readStream = nullptr;
		}

		std::string path = dataCacheFilename();

		ASSERT(!path.empty(), "Empty path given for opening write stream!");

		if (updateExisting) {
			Trace("DataHandle", "Opening file for updating: %s", path);
			m_writeStream = io::BasicOutputStream::createBinaryWriter(path, std::ios::in | std::ios::out);

			// go to the end of the stream
			m_writeStream->stream()->seekp(0, m_writeStream->stream()->end);
		}
		else {
			try {
				Trace("DataHandle", "Opening new file for writing: %s", path);
				Util::ensureDirExists(path);
			 
				/// create a new write stream
				m_writeStream = io::BasicOutputStream::createBinaryWriter(path, std::ios::trunc);

				/// and write a dummy header in there ... the header will be updated at the very end
				DataHeader hdr;
				memset(&hdr, 0, sizeof(hdr));
				writeHeader(hdr);

				/// Go to the end of the stream ...
				m_writeStream->stream()->seekp(0, m_writeStream->stream()->end);

				m_doUpdate = true;
			}
			catch (const Poco::Exception& e) {
				Error("DataHandle", "Error opening file: %s", path);
				Util::reportException(e);
			}
			catch (const std::exception& e) {
				Error("DataHandle", "Error opening file: %s", path);
				Util::reportException(e);
			}
		}

		setRowSize(rowSize);
		m_writePos = m_writeStream->stream()->tellp();
	}

	void DataHandle::endWrite() {
		PROFILE_SCOPED();

		ASSERT(m_writeStream, "Cannot endWrite when there isn't a writeStream! Did you call beginWrite?");
		m_writeStream->flush();
		m_writeStream = nullptr;
		m_seekMap.clear();
	}

	void DataHandle::setRowSize(size_t rowSize) {
		ASSERT(rowSize >= m_hdr.rowSize, "Data row size cannot shrink! Previous: " << m_hdr.rowSize << " - New: " << rowSize);
		m_hdr.rowSize = rowSize;
	}

	void DataHandle::setNumRows(size_t numRows) {
		ASSERT(numRows >= m_hdr.numRows, "Number of rows in data cannot decrease! Previous: " << m_hdr.numRows << " - New: " << numRows);
		m_hdr.numRows = numRows;
	}

	bool DataHandle::readRowHeader(IntDate date, DataRowHeader& rhdr) {
		if (!readSeek(date))
			return false;
		m_readStream->stream()->read((char*)&rhdr, sizeof(rhdr));
		return true;
	}

	bool DataHandle::readRow(IntDate date, void* data, size_t dataLength, size_t offset /* = 0 */, DataRowHeader* rhdr /* = nullptr */, bool noSeek /* = true */) {
		size_t dataLengthToRead = dataLength;

		if (!noSeek || rhdr) {
			if (!readSeek(date))
				return false;

			DataRowHeader rhdrRead;
			m_readStream->stream()->read((char*)&rhdrRead, sizeof(rhdrRead));
			if (rhdr)
				*rhdr = rhdrRead;

			dataLengthToRead = rhdrRead.size;
		}

		m_readStream->stream()->read((char*)data, dataLengthToRead);

		return true;
	}

	IntDate DataHandle::mapDate(IntDay di) {
		ASSERT(m_dates, "No dates were set on the data handle!");
		return (*m_dates)[di];
	}

	Poco::Timestamp DataHandle::getUpdateTimestamp() const {
		IntDate updateDate = m_hdr.updateDate;
		IntTime updateTime = m_hdr.updateTime;

		return Util::intToTimestamp(updateDate, updateTime);
	}

	void DataHandle::writeHeader(const DataCacheInfo& dci, IntDay diLast, IntDay diLastValidData, size_t rowSize, size_t numRows) {
		DataHeader hdr;

		ASSERT(dci.di >= 0, "Invalid di: " << dci.di);

		hdr.version = DataHeader::s_version;
		hdr.def = dci.def;

		IntDate startDate = dci.rt.getStartDate();

		/// If this data was written then it could be that the start date was changed
		/// in the config, but that doesn't mean that the data in the dataset has 
		/// shrunk now.
		if (m_hdr.startDate == 0 || startDate < m_hdr.startDate)
			hdr.startDate = startDate;
		else
			hdr.startDate = m_hdr.startDate;

		if (diLast < 0)
			diLast = 0;

		/// Same logic for the end date as well. A bit of mix in it are OneGo data sets
		/// which are always updated from the start to the very end. ALWAYS!
		IntDate endDate = dci.rt.dates[diLast];
		IntDate lastValidDataDate = dci.rt.dates[diLastValidData];

		if (hdr.def.doesUpdateInOneGo()) {
			endDate = dci.rt.getEndDate();
			lastValidDataDate = dci.rt.getEndDate();
		}

		if (m_hdr.endDate == 0 || m_hdr.endDate < endDate) 
			hdr.endDate = endDate;
		else
			hdr.endDate = m_hdr.endDate;

		if (m_hdr.lastValidDataDate == 0 || m_hdr.lastValidDataDate < lastValidDataDate)
			hdr.lastValidDataDate = lastValidDataDate;
		else
			hdr.lastValidDataDate = m_hdr.lastValidDataDate;

		ASSERT(dci.def.itemSize == 0 || rowSize % dci.def.itemSize == 0, "Data: " << dci.def.name << " - Invalid row size: " << rowSize << " - not aligned with item size: " << dci.def.itemSize << ": " << m_cacheId);

		Trace("DataHandle", "Dataset: %s - Row size: %z", dci.def.toString(dci.ds), rowSize);

		/// Save the current position of the stream. Get the footer offset and then go back
		hdr.rowSize = rowSize;
		hdr.numRows = numRows;
		hdr.metadata = dci.metadata;

		Poco::DateTime utcNow; 
		hdr.updateDate = Util::dateToInt(utcNow);
		hdr.updateTime = Util::dateToTime(utcNow);

		strncpy(hdr.loaderId, dci.config.get("id").c_str(), DataHeader::s_maxId - 1);

		/// Update the copy of the locally kept header with the new one 
		m_hdr = hdr;

		writeHeader(hdr);
	}

	void DataHandle::writeHeader(const DataHeader& hdr) {
		ASSERT(m_writeStream, "Invalid write stream handle given!");

		/// To avoid negative value "overflow" for an unsigned number!
		ASSERT(hdr.numRows < 50000, "Invalid number of rows: " << hdr.numRows);

		/// save the current position of the stream
		std::streampos currentPos = m_writeStream->stream()->tellp();

		/// go to the start and update/write the header
		m_writeStream->stream()->seekp(0, m_writeStream->stream()->beg);
		m_writeStream->stream()->write((const char*)&hdr, sizeof(hdr));

		/// then return to the previous position
		m_writeStream->stream()->seekp(currentPos, m_writeStream->stream()->beg);
	}

	void DataHandle::writeFooter(DataPtr customMetadata) {
		/// If there is no metadata given then we don't write anything at all
		if (!customMetadata)
			return;

		std::string metaFilename = metadataFilename();

		try {
			m_ftr.customMetadata = customMetadata;
			m_ftr.def = customMetadata->def();

			auto metaStream = io::BasicOutputStream::createBinaryWriter(metaFilename);

			/// Write various bits of info. The ftr.size is updated at the END
			metaStream->stream()->write((char*)&m_ftr.size, sizeof(m_ftr.size));
			metaStream->stream()->write((char*)&m_ftr.version, sizeof(m_ftr.version));
			metaStream->stream()->write((char*)&m_ftr.def, sizeof(m_ftr.def));
			
			/// We duplicate the header over here as well. The metadata header takes 
			/// precedence over the actual file header. This is because writing the data
			/// and writing the metadata are NOT atomic operations. Metadata is written 
			/// after the original data is written. In the event of an issue between these 
			/// two operations, we wanna trust the information coming from the latter
			/// write rather than the former one!
			metaStream->stream()->write((const char*)&m_hdr, sizeof(m_hdr));

			/// Write the actual data to disk
			customMetadata->write(*metaStream);

			/// Get the size of the stream
			m_ftr.size = metaStream->stream()->tellp();

			/// Go to the start of the stream
			metaStream->stream()->seekp(0, metaStream->stream()->beg);
			metaStream->stream()->write((char*)&m_ftr.size, sizeof(m_ftr.size));

			metaStream->flush();
			metaStream = nullptr;
		}
		catch (const Poco::PathNotFoundException&) {
		}
		catch (const Poco::FileNotFoundException&) {
		}
		catch (...) {
		}
	}

	size_t DataHandle::getStreamSize(std::istream& stream) const {
		return Util::getStreamSize(*m_readStream->stream());

		//if (!m_ftr.size)
		//	return streamSize - sizeof(size_t);

		//return streamSize - m_ftr.size;
	}

	void DataHandle::constructSeekMap() {
		PROFILE_SCOPED();

		/// This has already been constructed ...
		if (m_seekMap.size())
			return;

		//ASSERT(m_readStream, "Invalid read stream!");

		/// If its a universal data then we cannot have this ...
		if (m_hdr.def.doesUpdateInOneGo())
			return;

		/// Open the read stream if it isn't open already!
		if (!m_readStream) {
			// If we're still unable to open the read stream then quietly return; we've got nothing else to offer!
			if (!openReadStream(true, true))
				return;
		}

		/// Move to just after the header now
		size_t pos = sizeof(DataHeader);
		m_readStream->stream()->seekg(pos, m_readStream->stream()->beg);

		size_t streamSize = getStreamSize(*m_readStream->stream());
		size_t numRows = m_hdr.numRows;
		size_t startRow = 0;
		DataRowHeader lastRHdr;

		while (startRow++ < numRows && pos < streamSize) {
			DataRowHeader rhdr;
			m_readStream->stream()->read((char*)&rhdr, sizeof(DataRowHeader));
			ASSERT(rhdr.date != 0, "Invalid date in row header: " << rhdr.date << ": " << m_cacheId);

			size_t rpos = pos;
			pos += rhdr.writtenBytes + sizeof(DataRowHeader);
			m_readStream->stream()->seekg(pos, m_readStream->stream()->beg);

			ASSERT(m_seekMap.find(rhdr.date) == m_seekMap.end(), "[" << m_dv.toString(m_ds) << "] - Corrupt Data! Date was found twice in the file: " << rhdr.date << ". Report this to zmalik@quanvolve.com and give access to the file in question!");

			m_seekMap[rhdr.date] = rpos;
			lastRHdr = rhdr;
		}

		//if (lastRHdr.date) {
		//	const auto& rt = m_dr.pipelineHandler()->runtimeData();
		//	m_lastDate = lastRHdr.date;
		//	m_lastDi = rt.getDateIndex(m_lastDate);
		//	ASSERT(m_lastDi >= 0 && m_lastDi < (IntDay)rt.dates.size(), "Invalid date index: " << m_lastDi << " - for date: " << m_lastDate);
		//}
	}

	size_t DataHandle::getPos(IntDate date) {
		if (!m_seekMap.size() && m_needSeekMap)
			constructSeekMap();

		auto iter = m_seekMap.find(date);
		if (iter != m_seekMap.end())
			return iter->second;

		return 0;
	}

	size_t DataHandle::writeSeek(IntDate date) {
		size_t pos = getPos(date);
		if (!pos) {
			m_writeStream->stream()->seekp(0, m_writeStream->stream()->end);
			return 0;
		}
		//ASSERT(pos, "Unable to seek to date: " << date);
		m_writeStream->stream()->seekp(pos, m_writeStream->stream()->beg);
		return pos;
	}

	size_t DataHandle::readSeek(IntDate date) {
		size_t pos = getPos(date);
		if (!pos) {
			m_readStream->stream()->seekg(0, m_readStream->stream()->end);
			return 0;
		}
		//ASSERT(pos, "Unable to seek to date: " << date);
		m_readStream->stream()->seekg(pos, m_readStream->stream()->beg);
		return pos;
	}

	void DataHandle::truncateWriteStream(size_t* pos_) {
		PROFILE_SCOPED();

		ASSERT(!m_didTruncate, "Truncation was already done on this file!");
		ASSERT(m_doUpdate, "Why truncate on a file that's not being appended to?!");

		size_t pos;

		if (pos_)
			pos = *pos_;
		else
			pos = m_writeStream->stream()->tellp();

		/// OK ... close the write stream first ... 
		m_writeStream = nullptr;

		std::string path = dataCacheFilename();
		int fd = open(path.c_str(), O_RDWR);
		ASSERT(fd >= 0, "While trying to truncate, invalid file descriptor returned: " << path);

		/// Open up a C file stream
#ifdef __WINDOWS__
		_chsize(fd, (long)pos);
#else
		int tret = ftruncate(fd, pos);
		ASSERT(tret == 0, "Unable to truncate filename: " << path << " - ftruncate returned: " << tret);
#endif
		close(fd);

		m_didTruncate = true;

		/// Open the file again 
		m_writeStream = io::BasicOutputStream::createBinaryWriter(path, std::ios::in | std::ios::out);
		m_writeStream->stream()->seekp(pos, m_writeStream->stream()->beg);
	}

	void DataHandle::writeDi(DataFrame& frame, IntDate date) {
		PROFILE_SCOPED();

		bool didSeek = false;
		bool customRW = frame.data->doesCustomReadWrite();

		if (date != 0 && m_needSeekMap && m_seekMap.size()) {
			/// If we seek once when we MUST write contiguously after that
			/// this is a fixed assumption. We now clear the seek map!
			if (writeSeek(date)) {
				if (m_seekMap.size()) {
					/// Find the date that we're truncating to and then get the last date in the seek map
					auto iter = m_seekMap.find(date);
					if (iter != m_seekMap.end()) {
						iter--;
						if (iter != m_seekMap.end()) {
							m_lastDate = (IntDate)iter->first;
							m_lastDi = m_dr.pipelineHandler()->runtimeData().getDateIndex(m_lastDate);
						}
					}
				}

				didSeek = true;
				m_seekMap.clear();
				m_needSeekMap = false;
			}
		}


		if (frame.di > 0 && m_lastDi > 0) {
			const auto& rt = m_dr.pipelineHandler()->runtimeData();
			ASSERT(m_lastDi == frame.di - 1 && rt.dates[m_lastDi] == rt.dates[frame.di - 1], "Invalid frame being write at date: " << date << " [di=" << frame.di
				<< "] - the last frame was written at date: " << m_lastDate << " [di=" << m_lastDi << "]");
		}

		m_lastDate = date;
		m_lastDi = frame.di;

		//if (!didSeek) {
		//	m_writeStream->stream()->seekp(m_writePos, m_writeStream->stream()->beg);
		//}

		/// If we are writing for the first time to an update file then we truncate it first 
		/// This is because the C++ file IO API is shit and I should've known better and 
		/// stuck with the C API instead. A bitter lesson learnt then. Anyway, lets just get
		/// on with it and truncate the darn thing!
		if (!m_didTruncate && m_doUpdate)
			truncateWriteStream(nullptr);

		if (customRW) {
			/// This is custom data ... its handled slightly differently!
			DataRowHeader rhdr;
			rhdr.date = date;
			rhdr.size = frame.dataSize;
			rhdr.cols = frame.data->cols();
			rhdr.depth = frame.data->depth();
			rhdr.writtenBytes = rhdr.size;

			/// With custom read and write, we have the additional problem that the DataRowHeader size 
			/// might not represent the correct size of what was actually written. In that case we will
			/// just calculate what was written from the difference in stream positions before and 
			/// after the actual write. 
			/// We'll then update the DataRowHeader to point to the correct size that was actually 
			/// written, only if the two are different!
			size_t rhdrPos = m_writeStream->stream()->tellp();
			m_writeStream->stream()->write((char*)&rhdr, sizeof(rhdr));

			for (size_t kk = 0; kk < rhdr.depth; kk++)
				frame.data->writeRow(*m_writeStream, kk, 0);

			size_t currPos = m_writeStream->stream()->tellp();
			size_t posDelta = currPos - rhdrPos;
			ASSERT(posDelta >= rhdr.size, "Invalid number of bytes written. Expecting at least: " << rhdr.size << " - Actually written: " << posDelta);

			/// If the posDelta is larger than what we assumed to be then we 
			/// have to re-write the DataRowHeader
			if (posDelta > rhdr.writtenBytes) {
				rhdr.writtenBytes = posDelta - sizeof(DataRowHeader);
				m_writeStream->stream()->seekp(rhdrPos, m_writeStream->stream()->beg);
				m_writeStream->stream()->write((char*)&rhdr, sizeof(rhdr));

				/// Go back to the end of the stream
				m_writeStream->stream()->seekp(0, m_writeStream->stream()->end);
			}

			return;
		}

		ASSERT(m_seekMap.find(date) == m_seekMap.end(), "Date has already been written: " << date);

		DataRowHeader rhdr;
		rhdr.date = date;
		rhdr.cols = frame.dataSize / frame.data->itemSize();
		rhdr.size = frame.dataSize;
		rhdr.writtenBytes = rhdr.size;
		rhdr.depth = 1;

		size_t pos = m_writeStream->stream()->tellp();

		m_writeStream->stream()->write((char*)&rhdr, sizeof(rhdr));
		write(frame);

		m_writePos = m_writeStream->stream()->tellp();
		m_seekMap[date] = pos;
	}

	void DataHandle::write(DataFrame& frame, size_t orgFileOffset /* = 0 */, bool truncate /* = false */) {
		PROFILE_SCOPED();

		size_t fileOffset = orgFileOffset + sizeof(DataHeader);
		if (orgFileOffset)
			m_writeStream->stream()->seekp(fileOffset, m_writeStream->stream()->beg);

		/// We need to truncate the stream here!
		if (truncate && !m_didTruncate && m_doUpdate)
			truncateWriteStream(&fileOffset);

		if (!frame.data->doesCustomReadWrite()) {
			const char* dataPtr = reinterpret_cast<const char*>(frame.data->data());
			m_writeStream->stream()->write(dataPtr + frame.dataOffset, frame.dataSize);
		}
		else {
			frame.data->write(*m_writeStream);
		}
	}

	std::string DataHandle::toString() const {
		return m_dv.definition.toString(m_ds);
	}

	bool DataHandle::doesNeedUpdate(IntDate startDate, IntDate endDate) const {
		if (m_hdr.endDate >= startDate && m_hdr.endDate >= endDate)
			return false;
		return true;
	}  

	void DataHandle::rawDump(const RuntimeData& rt, const DumpInfo& dumpInfo) {
		try {
			std::string filename = dumpInfo.dataId.substr(1);
			Poco::File file(filename);

			m_readStream = io::BasicInputStream::createBinaryReader(filename);
			m_readStream->stream()->read((char*)&m_hdr, sizeof(m_hdr));
			auto maxRowSize = m_hdr.rowSize;

			Info("DataHandle", "Dumping file : %s", filename);
			Info("DataHandle", "%s", m_hdr.debugString());

			IntDay diStart = 0;
			IntDay diEnd = rt.diEnd - 1;

			if (dumpInfo.startDate != 0)
				diStart = rt.getDateIndex(dumpInfo.startDate);
			if (dumpInfo.endDate != 0)
				diEnd = rt.getDateIndex(dumpInfo.endDate);

			//for (IntDay di = diStart; di <= diEnd; di++) {
			//	DataRowHeader rhdr;
			//	IntDate date = rt.dates[di];

			//	readRowHeader(date, rhdr);

			//	Info("DataHandle", "DATE: %u", date);
			//	Info("DataHandle", "\tR-Hdr Date                 : %z", rhdr.date);
			//	Info("DataHandle", "\tR-Hdr Num Cols             : %z", rhdr.cols);
			//	Info("DataHandle", "\tR-Hdr Depth                : %z", rhdr.depth);
			//	Info("DataHandle", "\tR-Hdr Size in Bytes        : %z", rhdr.size);
			//	Info("DataHandle", "\tR-Hdr Written Size in Bytes: %z", rhdr.writtenBytes);
			//}
			size_t streamSize = getStreamSize(*m_readStream->stream());
			size_t numCountedRows = 0;
			auto pos = m_readStream->stream()->tellg();

			while (pos < streamSize) {
				DataRowHeader rhdr;
				m_readStream->stream()->read((char*)&rhdr, sizeof(DataRowHeader));
				ASSERT(rhdr.date != 0, "Invalid date in row header: " << rhdr.date);
				size_t rpos = pos;
				pos += rhdr.writtenBytes + sizeof(DataRowHeader);
				m_readStream->stream()->seekg(pos, m_readStream->stream()->beg);
				IntDate date;

				if (numCountedRows < rt.dates.size())
					date = rt.dates[numCountedRows];
				else
					date = 0;

				Info("DataHandle", "DATE: %u [%u]", rhdr.date, date);
				//Info("DataHandle", "\tR-Hdr Date                 : %z", rhdr.date);
				Info("DataHandle", "\tR-Hdr Num Cols             : %z", rhdr.cols);
				Info("DataHandle", "\tR-Hdr Depth                : %z", rhdr.depth);
				Info("DataHandle", "\tR-Hdr Size in Bytes        : %z", rhdr.size);
				Info("DataHandle", "\tR-Hdr Written Size in Bytes: %z", rhdr.writtenBytes);

				numCountedRows++;
			}

			Info("DataHandle", "Counted Rows: %z", numCountedRows);
			Info("DataHandle", "Hdr Rows: %z", m_hdr.numRows);
		}
		catch (const Poco::PathNotFoundException&) {
			// Oopes! The data cache path does not exist. Lets create it!
		}
	}
} /// namespace pesa 

#undef AUTO_NAMED_MUTEX_LOCK
