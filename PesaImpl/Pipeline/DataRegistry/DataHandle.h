/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// DataHandle.h
///
/// Created on: 28 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#ifndef PESAIMPL_PIPELINE_DATAREGISTRY_DATAHANDLE_H_
#define PESAIMPL_PIPELINE_DATAREGISTRY_DATAHANDLE_H_

#include "Framework/Framework.h"
#include "./DataHeader.h"

#include "Core/IO/BasicStream.h"

#define BOOST_DATE_TIME_NO_LIB 
#include <boost/interprocess/sync/file_lock.hpp>

namespace pesa {
	class DataRegistry;

	class DataHandle : public IDataHandle {
	private:
		typedef std::map<IntDate, size_t> SeekMap;

		IUniverse*				m_universe = nullptr;			/// What is the universe
		DataRegistry& 			m_dr;							/// The DataRegistry handle
		IDataLoaderPtr 			m_dl; 							/// The data loader
		const Dataset& 			m_ds;							/// The dataset info
		const DataValue& 		m_dv;							/// The data definition info
		std::string 			m_rootPath; 					/// The root path of the data
		Poco::Path 				m_dataCachePath; 				/// Path to where things should be written
		bool 					m_doUpdate = false; 			/// Do an update instead
		DataHeader 				m_hdr; 							/// Header that was previously loaded
		DataFooter				m_ftr;							/// The footer that was loaded
		SeekMap					m_seekMap;						/// Seek map while writing
		bool					m_needSeekMap = true;			/// Do we need the seek map?
		const IntDateVec*		m_dates = nullptr;				/// The dates pointer that can be used to lookup dates
		IntDay					m_delay = 1;					/// The delay of the data loader
		size_t					m_maxRowSize = 0;				/// Max row size in bytes

		IntDay					m_lastDi = 0;					/// The last DI in the file
		IntDate					m_lastDate = 0;					/// The last date that was written to the file

		io::BasicInputStreamPtr m_readStream = nullptr;			/// The (optional) read stream
		io::BasicOutputStreamPtr m_writeStream = nullptr;		/// The (optional) write stream
		size_t					m_writePos = 0;					/// The writing position!
		bool					m_didTruncate = false;			/// Did we truncate the file?
		bool					m_copyFromPrevVersion = true;	/// Copy from previous version or not (this is used in combination with the boolean flag in defs)
		bool					m_rowHeaders = false;			/// Load the row headers into the data or not
		std::string				m_cacheId;						/// The unique cache id

		Poco::Path				m_lockPath;						/// File lock path
		boost::interprocess::file_lock* m_lock = nullptr;		/// The file lock

		size_t					getPos(IntDate date);
		size_t					writeSeek(IntDate date);
		size_t					readSeek(IntDate date);

		std::string				dataCacheFilename(std::string dataCacheRoot = "") const;
		std::string				metadataFilename() const;
		void					truncateWriteStream(size_t* pos);

		bool					tryCopyFromPrevVersion();
		size_t					getStreamSize(std::istream& stream) const;

	public:

								DataHandle(IUniverse* universe, DataRegistry& dr_, IDataLoaderPtr dl_, const Dataset& ds_, const DataValue& dv_, 
									const std::string& rootPath, const Poco::Path& dataCachePath);

								~DataHandle();

		void 					readHeader();
		void					readFooter();

		bool					openReadStream(bool noRetry = false, bool noThrow = true);
		void					closeReadStream();

		DataPtr 				read(const RuntimeData& rt, DataInfo* dinfo, size_t startRow = 0, size_t endRow = 0);
		void 					read(io::BasicInputStreamPtr stream, const DataHeader& hdr, DataPtr data, const RuntimeData& rt, DataInfo* dinfo, size_t startRow = 0, size_t endRow = 0);

		void 					lock();
		void 					unlock();

		void					shLock();
		void					shUnlock();

		void 					beginWrite(bool updateExisting, size_t rowSize);
		void 					endWrite();

		void	 				writeHeader(const DataCacheInfo& dci, IntDay diLast, IntDay diLastValidData, size_t rowSize, size_t numRows);
		void	 				writeHeader(const DataHeader& hdr);
		void					writeFooter(DataPtr customMetadata);

		void					setRowSize(size_t rowSize);
		void					setNumRows(size_t numRows);

		bool					doesNeedUpdate(IntDate startDate, IntDate endDate) const;
		void	 				writeDi(DataFrame& frame, IntDate date);
		void	 				write(DataFrame& frame, size_t fileOffset = 0, bool truncate = false);

		bool					readFrame(const RuntimeData&rt, DataFrame& frame, IntDay di); 

		std::string 			toString() const;
		void					constructSeekMap();
		Poco::Timestamp			getUpdateTimestamp() const;

		void					rawDump(const RuntimeData& rt, const DumpInfo& dumpInfo);

		//////////////////////////////////////////////////////////////////////////
		/// IDataHandle overrides
		//////////////////////////////////////////////////////////////////////////
		virtual bool			readRowHeader(IntDate date, DataRowHeader& rhdr);
		virtual bool			readRow(IntDate date, void* data, size_t dataLength, size_t offset = 0, DataRowHeader* rhdr = nullptr, bool noSeek = true);
		virtual IntDate			mapDate(IntDay di);

		////////////////////////////////////////////////////////////////////////////
		/// Inline functions
		////////////////////////////////////////////////////////////////////////////
		inline const DataHeader& hdr() const 	{ return m_hdr;	}
		inline const DataFooter& ftr() const	{ return m_ftr; }
		inline const Dataset& 	ds() const 		{ return m_ds; }
		inline const DataValue& dv() const 		{ return m_dv; }
		inline DataRegistry& 	dr() 			{ return m_dr; }
		inline IDataLoaderPtr 	dl() 			{ return m_dl; }
		inline Poco::Path& 		dataCachePath()	{ return m_dataCachePath; }
		inline bool 			doUpdate() const{ return m_doUpdate; }
		inline bool& 			doUpdate()		{ return m_doUpdate; }
		inline std::string& 	rootPath() 		{ return m_rootPath; }
		inline bool				canWrite() const{ return m_writeStream ? true : false; }
		inline const IntDateVec*& dates()		{ return m_dates; }
		inline const IntDateVec& dates() const  { return *m_dates; }
		inline const SeekMap&	seekMap() const { return m_seekMap; }
		inline bool				copyFromPrevVersion() const { return m_copyFromPrevVersion; }
		inline bool&			copyFromPrevVersion() { return m_copyFromPrevVersion; }
		inline IntDay&			delay()			{ return m_delay; }
		inline const IntDay&	delay() const	{ return m_delay; }
		inline std::string&		cacheId()		{ return m_cacheId; } 
		inline const std::string& cacheId() const { return m_cacheId; }
		inline bool&			rowHeaders()	{ return m_rowHeaders; }
		inline bool				rowHeaders() const { return m_rowHeaders; }
	};

	typedef std::shared_ptr<DataHandle> DataHandlePtr;
} /// namespace pesa 

#endif /// PESAIMPL_PIPELINE_DATAREGISTRY_DATAHANDLE_H_ 
