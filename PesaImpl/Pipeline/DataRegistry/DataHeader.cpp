/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// DataHeader.cpp
///
/// Created on: 28 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "./DataHeader.h"

namespace pesa {

	const size_t DataHeader::s_version = sizeof(DataHeader);
	const size_t DataFooter::s_version = sizeof(DataFooter) - sizeof(DataPtr);

	DataHeader::DataHeader() {
	}

	bool DataHeader::isValid() const {
		return version > 0;
	}

	bool DataHeader::isOfType(const std::string& loaderId) const {
		return std::string(loaderId) == loaderId;
	}

	std::string DataHeader::toString() const {
		std::ostringstream ss;
		ss << "\n" <<
			"\tStartDate: " << startDate << "\n"
			"\tEndDate  : " << endDate << "\n"
			"\tUpDate   : " << updateDate << "\n"
			"\tUpTime   : " << updateTime << "\n"
			"\tRowSize  : " << rowSize << "\n"
			"\tNumRows  : " << numRows << "\n"
			"\tDefName  : " << def.name;

		return ss.str();
	}

	std::string DataHeader::debugString() const {
		std::ostringstream ss;
		ss << "\n" <<
			"\tStartDate: " << startDate << "\n"
			"\tEndDate  : " << endDate << "\n"
			"\tUpDate   : " << updateDate << "\n"
			"\tUpTime   : " << updateTime << "\n"
			"\tRowSize  : " << rowSize << "\n"
			"\tNumRows  : " << numRows << "\n"
			"\tDefName  : " << def.name;

		return ss.str();
	}


} /// namespace pesa 
	