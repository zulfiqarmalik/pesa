/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// DataHeader.h
///
/// Created on: 28 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/Framework.h"

namespace pesa {

	//////////////////////////////////////////////////////////////////////////
	/// DataHeader: The header embedded within each data file. Note that 
	/// the structure is FIXED size!
	//////////////////////////////////////////////////////////////////////////
	struct DataHeader {
		static const size_t 	s_maxId = 128;			/// Maximum number of chars for the name
		static const size_t 	s_version;

		size_t 					version = s_version;	/// Versioning information
		char 					loaderId[s_maxId];		/// The id of the data loader
		IntDate 				startDate = 0;			/// The starting date
		IntDate 				lastValidDataDate = 0;	/// The date for the last entry (on which a valid data was provided)
		IntDate 				endDate = 0; 			/// The date for the last entry
		IntDate					updateDate = 0;			/// The date of the last update
		IntTime					updateTime = 0;			/// The last time when this was updated
		size_t 					rowSize = 0;			/// Size of each row of data in bytes
		size_t 					numRows = 0; 			/// How many rows of data is it

		Metadata 				metadata; 				/// The metadata that was associated with this data

		/// The DataDefinition has to be the LAST element here!
		DataDefinition 			def;					/// More information on the dataset

								DataHeader();
		bool 					isValid() const;
		bool 					isOfType(const std::string& loaderId) const;

		std::string 			toString() const;
		std::string				debugString() const;
	};

	//////////////////////////////////////////////////////////////////////////
	/// DataFooter: The footer embedded within each data file. This can have 
	/// variable size and this is where the CustomMetadata is embedded
	//////////////////////////////////////////////////////////////////////////
	struct DataFooter {
		static const size_t		s_version;				/// The footer version

		size_t					size = 0;				/// The size in bytes of the footer
		size_t					version = s_version;	/// The footer version
		DataDefinition 			def;					/// The definition of the metadata that's embedded in here
		DataPtr					customMetadata = nullptr;/// The custom metadata that's embedded within the document
	};
} /// namespace pesa 

