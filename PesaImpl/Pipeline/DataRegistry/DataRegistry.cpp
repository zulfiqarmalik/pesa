/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// DataRegistry.cpp
///
/// Created on: 7 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "DataRegistry_Server.h"
#include "DataRegistry_Client.h"

#include "./DataRegistry.h"
#include "./DataHandle.h"
#include "./DataHeader.h"

#include "Core/IO/BasicStream.h"

#include "Poco/File.h"
#include "Poco/FileStream.h"
#include "Poco/Util/OptionException.h"

#include "Framework/Data/MatrixDataWindow.h"
#include "Framework/Data/VarMatrixData.h"
#include "Framework/Helper/MergedUniverse.h"
#include "Framework/Helper/ShLib.h"

#include <iomanip>
#include <future>
#include <regex>

#include "Core/Lib/Profiler.h"
#include "Poco/Thread.h"

#include "Core/Lib/Application.h"

namespace pesa {
	#define AUTO_REC_READ_LOCK(m)	std::lock_guard<std::recursive_mutex> __read_rec_guard__(m);
	#define AUTO_REC_WRITE_LOCK(m)	std::lock_guard<std::recursive_mutex> __write_rec_guard__(m);

	#define DR_READ_LOCK()			AUTO_REC_READ_LOCK(m_dataReadMutex);
	#define DR_WRITE_LOCK()			AUTO_REC_WRITE_LOCK(m_dataWriteMutex);
	#define DR_READ_WRITE_LOCK()	DR_READ_LOCK(); DR_WRITE_LOCK();

	#ifndef EIGEN_DONT_VECTORIZE
		const size_t DataRegistry::s_maxThreads = 16;
	#else
		const size_t DataRegistry::s_maxThreads = 4;
	#endif 

	DataRegistry::UpdateSet::~UpdateSet() {
		delete hData;
		hData = nullptr;
	}

	const char* DataRegistry::s_secMasterName	= "SecurityMaster";
	const char* DataRegistry::s_timestamp		= "TIMESTAMP";
	const char* DataRegistry::s_optTimestamp	= "TIMESTAMP_OPT";
	const char* DataRegistry::s_preTimestamp	= "TIMESTAMP_UPDATE_START";
	const int DataRegistry::s_waitForStart		= 10;

	DataRegistry::DataRegistry(const ConfigSection& config, std::string logChannel /* = "" */) : IDataRegistry(config, logChannel.empty() ? "DR" : logChannel) {
		PTrace("Created DataRegistry!");
		m_readOnly = config.getBool("readOnly", m_readOnly);

		/// Read the aliases
		std::string aliases = parentConfig()->defs().getString("aliases", "");
		if (!aliases.empty()) {
			aliases = parentConfig()->defs().resolve(aliases, nullptr);
			FrameworkUtil::parseMappings(aliases, &m_aliases);
		}
	}

	DataRegistry::~DataRegistry() {
		PTrace("Deleting DataRegistry!");
	}

	void DataRegistry::parseDataName(const std::string& fullName, std::string& dsName, std::string& defName) {
        StringVec parts = Util::split(fullName, ".");
		ASSERT(parts.size() == 2, "Invalid full data id: " << fullName);
		dsName = parts[0];
		defName = parts[1];
	}

	DataPtr DataRegistry::getMatrixWindow(IUniverse* universe, IDataHandlePtr hData, const DataDefinition& def, size_t backdays, size_t maxRows) {
		if (def.doesUpdateInOneGo())
			return nullptr;

		switch (def.itemType) {
		case DataType::kFloat:
		case DataType::kFloatArray:
			return FloatMatrixDataWindowPtr(new FloatMatrixDataWindow(universe, hData, def, backdays, maxRows));

		case DataType::kInt:
		case DataType::kIntArray:
			return IntMatrixDataWindowPtr(new IntMatrixDataWindow(universe, hData, def, backdays, maxRows));

		case DataType::kUInt:
		case DataType::kUIntArray:
			return UIntMatrixDataWindowPtr(new UIntMatrixDataWindow(universe, hData, def, backdays, maxRows));

		case DataType::kLong:
		case DataType::kLongArray:
			return LongMatrixDataWindowPtr(new LongMatrixDataWindow(universe, hData, def, backdays, maxRows));

		case DataType::kULong:
		case DataType::kULongArray:
			return ULongMatrixDataWindowPtr(new ULongMatrixDataWindow(universe, hData, def, backdays, maxRows));

		default:
			break;
		}

		return nullptr;
	}

	DataPtr DataRegistry::createDataBuffer(IUniverse* universe, const DataDefinition& def) {
		return DataRegistry::createDataBuffer(universe, def, nullptr);
	}

	DataPtr DataRegistry::createDataBuffer(IUniverse* universe, const DataDefinition& def, DataPtr metadata) {
		DataPtr data = nullptr;

		switch (def.itemType) {
		case DataType::kFloat:
		case DataType::kFloatArray:
			data = FloatMatrixDataPtr(new FloatMatrixData(universe, nullptr, def));
			break;

		case DataType::kDouble:
			data = DoubleMatrixDataPtr(new DoubleMatrixData(universe, nullptr, def));
			break;

		case DataType::kInt:
		case DataType::kIntArray:
			data = IntMatrixDataPtr(new IntMatrixData(universe, nullptr, def));
			break;

		case DataType::kUInt16:
			data = UShortMatrixDataPtr(new UShortMatrixData(universe, nullptr, def));
			break;

		case DataType::kUInt:
		case DataType::kUIntArray:
			data = UIntMatrixDataPtr(new UIntMatrixData(universe, nullptr, def));
			break;

		case DataType::kLong:
		case DataType::kLongArray:
			data = LongMatrixDataPtr(new LongMatrixData(universe, nullptr, def));
			break;

		case DataType::kULong:
		case DataType::kULongArray:
			data = ULongMatrixDataPtr(new ULongMatrixData(universe, nullptr, def));
			break;

		case DataType::kString:
		case DataType::kVarString:
			data = std::shared_ptr<StringData>(new StringData(universe, 0, def));
			break;

		case DataType::kBlob:
			data = BinaryDataPtr(new BinaryData(universe, nullptr, 0, def));
			break;

		case DataType::kSecurityData:
			data = SecurityDataPtr(new SecurityData(universe, nullptr, def));
			break;

		case DataType::kFloatConstant:
			data = ConstantFloatPtr(new ConstantFloat(0.0f, def));
			break;

		case DataType::kInt64:
		case DataType::kTimestamp:
		case DataType::kDateTime:
			data = Int64MatrixDataPtr(new Int64MatrixData(universe, nullptr, def));
			break;

		case DataType::kVarFloat:
			data = FloatVarMatrixDataPtr(new FloatVarMatrixData(universe, def));
			break;

		case DataType::kVarInt:
			data = IntVarMatrixDataPtr(new IntVarMatrixData(universe, def));
			break;

		case DataType::kVarDouble:
			data = DoubleVarMatrixDataPtr(new DoubleVarMatrixData(universe, def));
			break;

		case DataType::kVarDateTime:
			data = Int64VarMatrixDataPtr(new Int64VarMatrixData(universe, def));
			break;

		case DataType::kVarTimestamp:
			data = Int64VarMatrixDataPtr(new Int64VarMatrixData(universe, def));
			break;

		case DataType::kIndexedStringMetadata:
			data = ShortIndexedStringMetadataPtr(new ShortIndexedStringMetadata(universe, def));
			break;

		case DataType::kIndexedString:
			data = ShortIndexedStringDataPtr(new ShortIndexedStringData(universe, def));
			break;

		case DataType::kLongIndexedStringMetadata:
			data = LongIndexedStringMetadataPtr(new LongIndexedStringMetadata(universe, def));
			break;

		case DataType::kLongIndexedString:
			data = LongIndexedStringDataPtr(new LongIndexedStringData(universe, def));
			break;

		case DataType::kVarLongIndexedString:
			data = VarLongIndexedStringDataPtr(new VarLongIndexedStringData(universe, def));
			break;

		case DataType::kCustom:
		case DataType::kUnknown:
		default:
			break;
		}

		if (data && metadata)
			data->setMetadata(metadata);

		return data;
	}

	DataPtr DataRegistry::createCustomData(IUniverse* universe, const std::string& dsName, const DataDefinition& def, DataPtr metadata) {
		ASSERT(def.itemType == DataType::kCustom || def.itemType == DataType::kUnknown, "Invalid DataDefinition passed for createCustomData: " << def.itemType);

		const ConfigSection* configSec = nullptr;
		auto typeName = std::string(def.typeName);

		if (!typeName.empty())
			configSec = parentConfig()->modules().getChildSectionWithNameAndId("CustomData", typeName);
		else if (dsName.empty())
			configSec = parentConfig()->modules().getChildSectionWithNameAndProperty("CustomData", "data", dsName + "." + def.name);

		/// First of all we try to find the relevant config
		ASSERT(configSec, "Unable to find config section for data: " << def.name);

		/// Now we create DataPtr
		auto data = pesa::helper::ShLib::global().createData(this, universe, def, *configSec); 
		ASSERT(data, "Unable to create custom data with typename: " << typeName);

		return data;
	}

	DataPtr DataRegistry::createDataBuffer(IUniverse* universe, IDataLoaderPtr dl, const Dataset& ds, const DataDefinition& def, DataPtr metadata) {
		PTrace("Creating data buffer: %s (%d)", def.toString(ds), (int)def.itemType);
		DataPtr data = DataRegistry::createDataBuffer(universe, def, metadata);

		if (data)
			return data; 

		if (def.itemType == DataType::kCustom || def.itemType == DataType::kUnknown)
			return createCustomData(universe, ds.name, def, metadata);

		return dl->create(*this, ds, def);
	}

	DataPtr DataRegistry::createDataBuffer(IUniverse* universe, IDataLoaderPtr dl, const Dataset& ds, const DataDefinition& def) {
		return createDataBuffer(universe, dl, ds, def, nullptr);
	}

	std::string DataRegistry::getBaseDataCachePath() const {
		const Config* config = parentConfig();
		std::string rootDir = config->defs().get("dataCachePath");
		rootDir = config->defs().resolve(rootDir, 0U);
		ASSERT(!rootDir.empty(), "Invalid dataCachePath specified in the Defs section!");
		return rootDir;
	}

#define TS_FILE(name)				std::string rootDir = getBaseDataCachePath(); \
									Poco::Path rootPath(rootDir); \
									rootPath.append(name);


	void DataRegistry::waitForTimestamp(const char* name, int intervalMilliseconds /* = 100 */) {
		try {
			/// This is deliberately setup to be a spin-lock/busy-wait loop. 
			/// The reason for this is that we want the CPU usage to stay above a certain level
			/// during the time when the data cache is being updated. The rationale for this is 
			/// that we don't want the Amazon AWS auto-scale logic to start bringing our instances
			/// down. The default wait interval is small to prevent this from happening (i.e. the loop
			/// will spin more). If you want to change this behaviour and reduce CPU usage then you 
			/// can give a longer interval (5-10 seconds for instance) and that will bring the CPU
			/// usage down
			while (true) {
				TS_FILE(name);

				Poco::File tsFile(rootPath);

				/// OK, the reason why we have this stupid logic instead of just having this condition as 
				/// part of the while condition is because, the Poco::File stores state so if the file, 
				/// for some reason, does not exist at the start then the state is saved. I've seen conditions
				/// where this loop is spinning even when the file exists. This is the reason we need to create 
				/// the object every iteration of the loop. 
				if (tsFile.exists())
					break;

				/// Less than 100 milliseconds we just wanna keep the CPU usage for reasons explained above!
				if (intervalMilliseconds > 100) {
					Poco::Thread::sleep((long)intervalMilliseconds);
					PInfo("[%s] Waiting for TIMESTAMP: %s [Interval = %d ms]", Application::instance()->appOptions().simInstanceName, rootPath.toString(), intervalMilliseconds);
				}
			}
		}
		catch (const std::exception& e) {
			Util::reportException(e);
			Util::exit(-9999);
		}
	}

	bool DataRegistry::getTimestamp(const char* name, Poco::Timestamp& ts) {
		try {
			TS_FILE(name);

			Poco::File tsFile(rootPath);
			if (!tsFile.exists())
				return false;

			ts = tsFile.getLastModified();
			return true;
		}
		catch (const std::exception& e) {
			Util::reportException(e);
			return false;
		}

		return false;
	}

	bool DataRegistry::removeTimestamp(const char* name) {
		try {
			TS_FILE(name);

			Poco::File tsFile(rootPath);
			if (!tsFile.exists())
				return true;

			tsFile.remove();
			return true;
		}
		catch (const std::exception& e) {
			Util::reportException(e);
			return false;
		}

		return false;
	}

	bool DataRegistry::createTimestamp(const char* name) {
		try {
			TS_FILE(name);

			PInfo("Creating timestamp: %s", rootPath.toString());

			Poco::File tsFile(rootPath);
			if (tsFile.exists())
				return false;

			return tsFile.createFile();
		}
		catch (const std::exception& e) {
			Util::reportException(e);
			Util::exit(-9999);
		}

		return false;
	}

#undef TS_FILE

	/// Run all the DataLoaders defined in the config
	Poco::Path DataRegistry::getDataPath(IUniverse* universe, const ConfigSection& config, const Dataset& ds, const DataDefinition& def) const {
		/// First of all we try to get from the local config section of the data ...
		std::string rootDir = config.getString("dataCachePath", "");

		/// If nothing comes up then we get it from the global defs section
		if (rootDir.empty()) 
			rootDir = config.config()->defs().get("dataCachePath");

		ASSERT(!rootDir.empty(), "Unable to find dataCachePath. Either have it in the data config section or have it as path of the Defs section!");

		auto path = def.path(universe, rootDir, ds.name);
		return config.config()->defs().resolve(path.toString(), 0U);
	}

	void DataRegistry::cacheUniverse(const ConfigSection& config) {
	}

	void DataRegistry::cacheUniverses() {
	}

	void DataRegistry::tickWindowData(const RuntimeData& rt) {
		for (auto& wdata : m_windowData) {
			IntDay wdi = rt.di - wdata.hData->delay();
			IntDate diDate = rt.dates[wdi];
			DataPtr data = wdata.data; 
			char* pdata = reinterpret_cast<char*>(data->data());
			size_t numRows = data->rows();
			size_t rowSize = numRows * data->itemSize();
			IntDay diDst = wdi % numRows;
			wdata.hData->readRow(diDate, pdata + (diDst * rowSize), rowSize);
		}
	}

	void DataRegistry::tick(const RuntimeData& rt) {
		m_tickDataCache.clear();

		for (auto iter = m_dlMap.begin(); iter != m_dlMap.end(); iter++)
			iter->second->dataTick(rt, *this);

		//size_t memUsage = 0;

		//for (auto iter = m_dataCache.begin(); iter != m_dataCache.end(); iter++) {
		//	auto& citem = iter->second;
		//	if (citem.data) {
		//		size_t numItems = citem.data->rows() * citem.data->cols();
		//		size_t dataSize = numItems * citem.data->itemSize();
		//		memUsage += dataSize;
		//	}
		//}

		//PInfo("Total memory usage: %z", memUsage);

		tickWindowData(rt);
	}

	void DataRegistry::finalise(RuntimeData& rt) {
		m_windowData.clear();
		m_tickDataCache.clear();
		m_dataCache.clear();
		m_dlMap.clear();
		m_dataDefMap.clear();
		m_secMaster = nullptr;
		m_universes.clear();
	}

	void DataRegistry::prepare(RuntimeData& rt) {
		PTrace("DataRegistry::exec");
		rt.dataRegistry = this;

		if (rt.appOptions.updateDataOnly && rt.appOptions.waitForTimestamp) {
			PDebug("Removing TIMESTAMPs ....");
			removeTimestamp(s_timestamp);
			removeTimestamp(s_optTimestamp);

			int waitForStart = config().get<int>("waitForStart", s_waitForStart);
			while (waitForStart-- > 0) {
				PDebug("Starting data update in: %d seconds", waitForStart);
				Poco::Thread::sleep(1000L);
			}

			/// Create a pre-liminary TIMESTAMP
			createTimestamp(s_preTimestamp);
		}

		/// IMPORTANT: We keep the size since we add items to this vector 
		/// but DO NOT want to iterate over them
		auto modules = m_config.config()->modules().modules();
		size_t numModules = modules.size();
		std::string market = parentConfig()->defs().market();

		PTrace("Num modules: %z", modules.size());

		for (size_t i = 0; i < numModules; i++) {
			auto module = modules[i];

			auto name = module->name();
			if (name[0] == '_')
				continue;

			auto handler = module->getString("handler", "");

			/// We are looking for data handlers
			if (handler.empty())
				continue;
			else if (handler == "DataAlias") {
				defineAlias(module);
				continue;
			}
			else if (handler != "DataLoader")
				continue;

			auto ifMarketsStr			= module->getString("_IF_MARKET_", "");
			auto ifNotMarketsStr		= module->getString("_IF_NOT_MARKET_", "");

			if (!ifMarketsStr.empty()) {
				auto ifMarkets			= Util::split(ifMarketsStr, "|");
				bool relevantToMarket	= Util::isInVector(ifMarkets, market);

				if (!relevantToMarket) {
					//PInfo("Not relevant to market: %s Module: %s", ifNotMarketsStr, module->toString(true));
					continue;
				}
			}

			if (!ifNotMarketsStr.empty()) {
				auto ifNotMarkets		= Util::split(ifNotMarketsStr, "|");
				bool irrelevantToMarket = Util::isInVector(ifNotMarkets, market);

				if (irrelevantToMarket) {
					//PInfo("Irrelevant to market: %s Module: %s", ifMarketsStr, module->toString(true));
					continue;
				}
			}

			PTrace("Preparing: %s", module->toString(true));

			if (module->isSecMaster()) {
			  	DataFrame emptyFrame;
				m_secMaster = helper::ShLib::global().createComponent<IUniverse>(*module, this);
                DataCacheInfo dci(rt, Dataset(), DataValue(DataDefinition()), DataDefinition(), Constants::s_invalidDay, Constants::s_invalidDay, Constants::s_invalidDay, m_secMaster->config(),
                	nullptr, false, nullptr);
				m_secMaster->build(*this, dci, emptyFrame);
				m_dlMap[DataRegistry::s_secMasterName] = m_secMaster;

				std::string name = m_secMaster->name();
				if (!name.empty() && name != DataRegistry::s_secMasterName)
					m_dlMap[name] = m_secMaster;
				continue;
			}

			if (module->getBool("managed", false)) {
				PInfo("Ignoring managed module: %s", module->getRequired("name"));
				continue;
			}

			IDataLoaderPtr dl = helper::ShLib::global().createComponent<IDataLoader>(*module, this);
			ASSERT(dl != nullptr, "Unable to load module: " << module->toString());

			runForData(rt, dl);

			/// If we're updating the data only then we cache the universes, so that they 
			/// can be updated and optimised and eventually, cached!
			if (module->isUniverse() && rt.appOptions.updateDataOnly) {
				ConfigSection* universeConfig = new ConfigSection(*module);
				std::string wrapper;

				if (universeConfig->getBool("autoLoad", true) == false) {
					PDebug("Not auto-loading universe: %s", universeConfig->id());
					continue;
				}

				if (universeConfig->get("wrapper", wrapper)) {
					universeConfig->set("id", wrapper);
					universeConfig->set("path", universeConfig->get("wrapperPath"));
				}
				else {
					universeConfig->set("id", "MainUniverse");
					universeConfig->set("path", "");
				}

				modules.push_back(universeConfig);

				IUniversePtr universe = helper::ShLib::global().createComponent<IUniverse>(*universeConfig, this);
				DataCacheInfo dci(rt, Dataset(), DataValue(DataDefinition()), DataDefinition(), Constants::s_invalidDay, Constants::s_invalidDay, Constants::s_invalidDay, *module,
					m_secMaster ? m_secMaster.get() : nullptr, false, nullptr);
				DataFrame emptyFrame;

				PInfo("Precaching universe: %s", universe->name());

				universe->build(*this, dci, emptyFrame);
				m_universes.push_back(universe);
				m_dlMap[universe->name()] = universe;
			}
		}

		if (rt.appOptions.updateDataOnly && rt.appOptions.waitForTimestamp) {
			/// Create the normal data timestamp (and remove the pre-liminary one)
			removeTimestamp(s_preTimestamp);
			createTimestamp(s_timestamp);
		}

		optimiseDatasets(rt);

		if (rt.appOptions.updateDataOnly && rt.appOptions.waitForTimestamp)
			createTimestamp(s_optTimestamp);

		//const std::string& universeId = parentConfig()->portfolio().getRequired("universeId");
		//auto iter = m_universes.begin();
		///// OK now we need to remove all universes from the cache other than this one
		//while (iter != m_universes.end()) {
		//	auto universe = *iter;
		//	const auto& uname = universe->name();

		//	if (uname != universeId) {
		//		Debug("Removing universe from cache: %s", uname);
		//		auto cacheIter = m_dataCache.find(uname + ".Universe");
		//		if (cacheIter != m_dataCache.end())
		//			m_dataCache.erase(cacheIter);
		//		iter = m_universes.erase(iter);
		//	}
		//	else 
		//		iter++;
		//}

		PInfo("Data cache update complete!");

		//m_dataCache.clear();

		cacheUniverses();
	}

	void DataRegistry::mergeFrames(IUniverse* secMaster, const RuntimeData& rt, IntDay di, DataFrame& currFrame, DataFrame& lastFrame, bool mergeOnHoliday) {
		if (!lastFrame.data)
			return;

		if (mergeOnHoliday && currFrame.data->def().isStatic())
			return;

		IntDay rows = (IntDay)std::min(currFrame.data->rows(), lastFrame.data->rows());
		size_t cols = std::min(currFrame.data->cols(), lastFrame.data->cols());
		size_t depth = std::min(currFrame.data->depth(), lastFrame.data->depth()); 


		if (!mergeOnHoliday) {
			for (size_t kk = 0; kk < depth; kk++) {
				for (IntDay di = 0; di < rows; di++) {
					for (size_t ii = 0; ii < cols; ii++) {
						if (!currFrame.data->isValid(di, ii) && lastFrame.data->isValid(di, ii)) 
							currFrame.data->copyElement(lastFrame.data.get(), di, ii, di, ii, kk, kk);
					}
				}
			}
		}
		else if (secMaster) {
			IntDate date = rt.dates[di];
			std::unordered_map<std::string, bool> holidayExchanges;
			for (auto eiter = rt.exchangeHolidays.begin(); eiter != rt.exchangeHolidays.end(); eiter++) {
				const IntDateBoolMap& holidayMap = eiter->second; 
				auto iter = holidayMap.find(date);
				if (iter != holidayMap.end())
					holidayExchanges[eiter->first] = true;
			}

			if (!holidayExchanges.size())
				return;

			for (size_t kk = 0; kk < depth; kk++) {
				for (IntDay di = 0; di < rows; di++) {
					for (size_t ii = 0; ii < cols; ii++) {
						/// If the data in the current frame is invalid and we have valid data in the 
						/// last frame then we copy it over from the last good frame
						//if (!currFrame.data->isValid(di, ii) && lastFrame.data->isValid(di, ii)) {
							/// Get the source security 
						Security sec = secMaster->security(di, (Security::Index)ii);

						if (!sec.isValid())
							continue;

						auto secExchange = sec.exchangeStr();
						auto iter = holidayExchanges.find(secExchange);
						//ASSERT(iter != holidayExchanges.end(), "Unable to find holiday calendar for exchange: " << secExchange << ". Perhaps the exchange is incorrect?");

						if (iter == holidayExchanges.end()) {
							/// See whether this has a valid exchange at all
							auto eiter = rt.exchangeHolidays.find(secExchange);

							/// We have 
							if (eiter != rt.exchangeHolidays.end() || currFrame.data->isValid(di, ii))
								continue;
						}

						/// OK we have ascertained by now that its a holiday/Unknown exchange and we can copy from the previous frame
						currFrame.data->copyElement(lastFrame.data.get(), di, ii, di, ii, kk, kk);
					}
				}
			}
		}
	}

	bool DataRegistry::getNextFrameData(IUniverse* secMaster, const RuntimeData& rt, const DataCacheInfo& dci, const UpdateSet& uset, DataFrame& frame) {
		//////////////////////////////////////////////////////////////////////////
		/// !!IMPORTANT!!!
		///
		/// THIS FUNCTION CAN BE CALLED FROM A THREAD. 'const UpdateSet& uset' is passed as a
		/// const for this reason. DO NOT use any const_cast or any other 
		/// malarkey! 'uset' is NOT meant to be written to in this function 
		/// call. All of that is handleg in the DataRegistry::updateFrameDataState
		/// function below. Any 'uset' writes should be done there! 
		//////////////////////////////////////////////////////////////////////////
		ASSERT(dci.di <= uset.diEnd, "Invalid di: " << dci.di << " - diEnd: " << uset.diEnd);

		auto* universe = uset.universe;
		auto& ds = uset.hData->ds();
		auto& def = uset.hData->dv().definition;
		auto dl = uset.hData->dl();
		bool universeSensitive = dl->config().getBool("universeSensitive", false);

		/// If a source data has been given as input then that is the first thing that we use
		if (uset.srcData != nullptr) {
			ASSERT((size_t)dci.di >= uset.srcDataIndex, "Insufficient data provided for caching optimised dataset. di: " << dci.di << " - srcDataIndex: "
				<< uset.srcDataIndex << " - Dataset: " << def.toString(ds));

			size_t offset = dci.di - uset.srcDataIndex;
			size_t offsetInBytes = offset * uset.srcData->cols() * uset.srcData->itemSize();
			size_t dataSize = uset.srcData->cols() * uset.srcData->itemSize();

			/// If we cross the limit then we don't have enough data ...
			if (offsetInBytes + dataSize > uset.srcData->dataSize()) {
				frame.noDataUpdate = true;
				return false;
			}

			frame.data = uset.srcData;
			frame.dataOffset = offsetInBytes;
			frame.dataSize = dataSize;
			frame.noDataUpdate = false;
		}
		else if (uset.hData->dv().callback)
			uset.hData->dv().callback(*rt.dataRegistry, dci, frame);
		else
			dl->build(*rt.dataRegistry, dci, frame);

		/// as soon as we get an empty frame we stop collection ...
		if (!frame.data || (frame.data && !frame.data->doesCustomReadWrite())) {
			if (!frame.data || !frame.dataSize) {
				ASSERT(frame.noDataUpdate, Poco::format("Fatal Error: No data update for day: %d. You must set the noDataUpdate flag if no data is being provided in the frame!", (int)(rt.dates[dci.di])));
				//if (!frame.noDataUpdate)
				//	throw Poco::Exception("Fatal Error: No data update for day: %d", (int)(rt.dates[dci.di]));
				return false;
			}
		}

		ASSERT(frame.data->def().itemType == DataType::kCustom || frame.data->itemSize() == 0 || frame.dataSize % frame.data->itemSize() == 0,
			"Invalid data size: " << frame.dataSize << " - not aligned with item size: " << frame.data->itemSize());

		return true;
	}

	void DataRegistry::updateFrameDataState(IUniverse* secMaster, const RuntimeData& rt, const DataCacheInfo& dci, UpdateSet& uset, DataFrame& frame) {
		if (uset.existingFtr.customMetadata) {
			frame.data->setMetadata(uset.existingFtr.customMetadata);
			frame.customMetadata = uset.existingFtr.customMetadata;
		}
		else {
			frame.customMetadata = frame.data->getMetadata();
			uset.existingFtr.customMetadata = frame.customMetadata;
		}

		if (frame.dataSize > uset.maxRowSize || uset.maxRowSize == 0)
			uset.maxRowSize = frame.dataSize;

		if (frame.dataSize < uset.minRowSize || uset.minRowSize == 0)
			uset.minRowSize = frame.dataSize;

		/// over here we merge the frames and make sure that we have the last valid data
		/// for all the stocks
		if ((uset.useLastDayDataOnHoliday || uset.useLastDayData) && uset.lastGoodFrame.data) {
			uset.lastGoodFrame.data->clearUniverse();
			mergeFrames(secMaster, rt, dci.di, frame, uset.lastGoodFrame, uset.useLastDayDataOnHoliday);
		}

		frame.di = dci.di;
	}

	void DataRegistry::getNextFrame(IUniverse* secMaster, const RuntimeData& rt, const DataCacheInfo& dci, UpdateSet& uset, DataFrame& frame) {
		//if (DataRegistry::getNextFrameData(secMaster, rt, dci, uset, frame)) {
		//	DataRegistry::updateFrameDataState(secMaster, rt, dci, uset, frame);
		//}
		ASSERT(dci.di <= uset.diEnd, "Invalid di: " << dci.di << " - diEnd: " << uset.diEnd);

		auto* universe	= uset.universe;
		auto& ds 		= uset.hData->ds();
		auto& def 		= uset.hData->dv().definition;
		auto dl			= uset.hData->dl();
		bool universeSensitive = dl->config().getBool("universeSensitive", false);
		IntDay di 		= dci.di;

		/// If a source data has been given as input then that is the first thing that we use
		if (uset.srcData != nullptr) {
			ASSERT((size_t)di >= uset.srcDataIndex, "Insufficient data provided for caching optimised dataset. di: " << di << " - srcDataIndex: " 
				<< uset.srcDataIndex << " - Dataset: " << def.toString(ds));

			size_t offset 			= di - uset.srcDataIndex;
			size_t offsetInBytes	= offset * uset.srcData->cols() * uset.srcData->itemSize();
			size_t dataSize			= uset.srcData->cols() * uset.srcData->itemSize();

			/// If we cross the limit then we don't have enough data ...
			if (offsetInBytes + dataSize > uset.srcData->dataSize()) {
				frame.noDataUpdate	= true;
				return;
			}

			frame.data 				= uset.srcData;
			frame.dataOffset 		= offsetInBytes;
			frame.dataSize 			= dataSize;
			frame.noDataUpdate 		= false;
		}
		else if (uset.hData->dv().callback)
			uset.hData->dv().callback(*rt.dataRegistry, dci, frame);
		else
			dl->build(*rt.dataRegistry, dci, frame);

		/// as soon as we get an empty frame we stop collection ...
		if (!frame.data || (frame.data && !frame.data->doesCustomReadWrite())) {
			if (!frame.data || !frame.dataSize) {
				ASSERT(frame.noDataUpdate, Poco::format("Fatal Error: No data update for day: %d. You must set the noDataUpdate flag if no data is being provided in the frame!", (int)(rt.dates[di])));
				//if (!frame.noDataUpdate)
				//	throw Poco::Exception("Fatal Error: No data update for day: %d", (int)(rt.dates[di]));
				return;
			}
		}

		ASSERT(frame.data->def().itemType == DataType::kCustom || frame.data->itemSize() == 0 || frame.dataSize % frame.data->itemSize() == 0,
			"Invalid data size: " << frame.dataSize << " - not aligned with item size: " << frame.data->itemSize());

		if (uset.existingFtr.customMetadata) {
			frame.data->setMetadata(uset.existingFtr.customMetadata);
			frame.customMetadata = uset.existingFtr.customMetadata;
		}
		else {
			frame.customMetadata = frame.data->getMetadata();
			uset.existingFtr.customMetadata = frame.customMetadata;
		}

		if (frame.dataSize > uset.maxRowSize || uset.maxRowSize == 0)
			uset.maxRowSize = frame.dataSize;

		if (frame.dataSize < uset.minRowSize || uset.minRowSize == 0)
			uset.minRowSize = frame.dataSize;

		/// over here we merge the frames and make sure that we have the last valid data
		/// for all the stocks
		if ((uset.useLastDayDataOnHoliday || uset.useLastDayData) && uset.lastGoodFrame.data) {
			uset.lastGoodFrame.data->clearUniverse();
			mergeFrames(secMaster, rt, di, frame, uset.lastGoodFrame, uset.useLastDayDataOnHoliday);
		}

		frame.di = di;	}

	IPipelineComponentPtr DataRegistry::prepareDownload(const RuntimeData& rt, ConfigSection* downloadSec, IntDay dciStart, IntDay dciEnd) {
		if (!downloadSec)
			return nullptr; 

		dciStart = rt.getDate(dciStart);

		if (!rt.isTodayHoliday || dciEnd < rt.dates.size() - 1)
			dciEnd = rt.getDate(dciEnd);
		else
			dciEnd = rt.getDate(dciEnd - 1);

		/// Set these values. These are going to be removed from the config in finaliseUpdateSets function!
		downloadSec->set("dciStart", Util::cast(dciStart));
		downloadSec->set("dciEnd", Util::cast(dciEnd));

		IPipelineComponentPtr downloader = helper::ShLib::global().createPipelineComponent<IPipelineComponent>(*downloadSec, this);
		PInfo("Preparing the downloader ...");
		downloader->prepare(const_cast<RuntimeData&>(rt));

		return downloader;
	}

	void DataRegistry::finaliseDownload(const RuntimeData& rt, IPipelineComponentPtr downloader, ConfigSection* downloadSec) {
		if (!downloader)
			return;

		PInfo("Finalising the downloader ...");
		downloader->finalise(const_cast<RuntimeData&>(rt));

		/// Clear these values in the end!
		downloadSec->unset("dciStart");
		downloadSec->unset("dciEnd");
	}

	void DataRegistry::prepareUpdateSets(const RuntimeData& rt, UpdateSetPtrVec& usets) {
		IntDay minDiStart			= -1;
		IntDay maxDiEnd				= -1;
		IntDay diSimEnd				= rt.diSimEnd();
		ConfigSection* downloadSec	= nullptr;

		for (auto uset : usets) {
			auto& def = uset->hData->dv().definition;

			if (!downloadSec)
				downloadSec = const_cast<ConfigSection*>(uset->hData->dl()->config().getChildSection("Download"));

			/// If its an always over-write then we just always start from the first date
			if (def.isAlwaysOverwrite()) {
				uset->diStart				= 0;
				uset->hData->doUpdate()		= false;
			}

			uset->orgDiStart				= uset->diStart;
			uset->diEnd						= std::min((IntDay)rt.dates.size() - 1 - uset->delay, diSimEnd);
			uset->historyOverlap			= 0;
			uset->existingHdr				= uset->hData->hdr();
			uset->existingFtr				= uset->hData->ftr();
			uset->maxRowSize				= uset->existingHdr.rowSize;
			uset->minRowSize				= uset->existingHdr.rowSize;

			/// Read the following from the config, but they do not apply if we are optimising!
			auto& dlConfig = uset->hData->dl()->config();

			uset->useLastDayData 			= dlConfig.getBool("useLastDayData", uset->useLastDayData, true) && !uset->isOptimising; 
			uset->useLastDayDataOnHoliday 	= dlConfig.getBool("useLastDayDataOnHoliday", uset->useLastDayDataOnHoliday, true) && !uset->isOptimising;
			uset->loadLastDayData 			= dlConfig.getBool("loadLastDayData", uset->loadLastDayData, true) && !uset->isOptimising;
			uset->useLastDayDataFrameIfEmpty= dlConfig.getBool("useLastDayDataFrameIfEmpty", uset->useLastDayDataFrameIfEmpty, true) && !uset->isOptimising;
			uset->useNullDataFrameIfEmpty 	= dlConfig.getBool("useNullDataFrameIfEmpty", uset->useNullDataFrameIfEmpty, true) && !uset->isOptimising;
			uset->minDaysToUpdate 			= dlConfig.getInt("minDaysToUpdate", rt.appOptions.days, true);
			uset->numFramesBeforeFlush 		= dlConfig.getInt("numFramesBeforeFlush", s_numFramesBeforeFlush, true);
			uset->minDate 					= dlConfig.getInt("minDate", uset->minDate, true);
			uset->maxDate 					= dlConfig.getInt("maxDate", uset->maxDate, true);

			uset->collectAllFramesBeforeWriting = def.collectAllBeforeWriting();

			/// We need to remove this data from the cache ...
			auto name = uset->hData->dv().toString(uset->hData->ds());
			rt.dataRegistry->clearData(name);

			/// For datasets that update in one go just run the loop once!
			if (def.doesUpdateInOneGo()) {
				uset->diStart = uset->diEnd;
				uset->diLast = uset->diEnd;
			}
			else {
				uset->diStart = std::max(uset->diStart - uset->minDaysToUpdate, 0); /// We always want to update the last 'rt.appOptions.days' day data by default

				/// If an update is forced and a start date is given for the data update then 
				/// we use the minimum of the two
				//if (rt.appOptions.force && rt.appOptions.startDate != 0) { 
				//	IntDay startDateIndex = rt.getDateIndex(rt.appOptions.startDate);
				//	uset->diStart = std::min(uset->diStart, startDateIndex);
				//}

				uset->historyOverlap = uset->orgDiStart - uset->diStart + 1;
				uset->historyOverlap = std::min(uset->historyOverlap, uset->existingHdr.numRows); /// Mustn't go past the number of rows that we already have!
			}

			/// start cannot go above diEnd
			uset->diStart = std::min(uset->diStart, uset->diEnd);

			if (minDiStart < 0 || uset->diStart < minDiStart)
				minDiStart = uset->diStart;
			if (maxDiEnd < 0 || uset->diEnd > maxDiEnd)
				maxDiEnd = uset->diEnd;

			/// We don't want to run this while we're optimising the dataset (or it's marked as download once only) ...
			if (!uset->srcData && downloadSec && !downloadSec->getBool("onlyOnce", true)) 
				prepareDownload(rt, downloadSec, uset->diStart, uset->diEnd);

			/// ACQUIRE LOCK HERE. THIS WILL BE RELEASED AUTOMATICALLY!
			uset->hData->lock();
		}

		if (minDiStart < 0)
			minDiStart = 0;

		/// If something is marked as download once, then we shall use the first update set to 
		/// download it ...
		if (usets.size() && !usets[0]->srcData && downloadSec && downloadSec->getBool("onlyOnce", true)) 
			usets[0]->downloader = prepareDownload(rt, downloadSec, minDiStart, maxDiEnd);

		for (auto uset : usets) {
			if (!uset->hData->dv().definition.doesUpdateInOneGo() && minDiStart != uset->diStart) {
				uset->diStart = minDiStart;
				uset->historyOverlap = uset->orgDiStart - uset->diStart + 1;
				uset->historyOverlap = std::min(uset->historyOverlap, uset->existingHdr.numRows); /// Mustn't go past the number of rows that we already have!
			}

			uset->diLast = uset->di;
			uset->di = uset->diStart;
			uset->diLastDataFlush = uset->di;

			/// OK over here we just try to get the last valid frame from the DataHandle
			if (uset->useLastDayData || uset->loadLastDayData || uset->useLastDayDataOnHoliday || uset->useLastDayDataFrameIfEmpty || uset->maxDate) {
				IntDay diPrevFrame = uset->diStart - 1;

				if (diPrevFrame >= 0) {
					/// Open the read stream again ... it might haven been closed by the checking stage ...
					uset->hData->openReadStream(true, true);

					uset->hData->readFrame(rt, uset->lastGoodFrame, uset->diStart - 1);

					/// We always close the read stream 
					uset->hData->closeReadStream();

					if (uset->lastGoodFrame.data)
						uset->lastGoodFrame.data->setMetadata(uset->existingFtr.customMetadata);
				}

				/// If there is no data available then we just make an dummy frame
				if (!uset->lastGoodFrame.data && (uset->useLastDayData || uset->useLastDayDataOnHoliday || uset->useLastDayDataFrameIfEmpty || uset->maxDate)) { /// We don't wanna do this for loadLastDayData
					PDebug("No last good frame found. Using empty frame!");
					uset->lastGoodFrame.data = createDataBuffer(uset->universe, uset->hData->dl(), uset->hData->ds(), uset->hData->dv().definition);
					uset->lastGoodFrame.data->ensureSize(1, 1);
					uset->lastGoodFrame.di = std::max(uset->di - 1, 0);
					uset->lastGoodFrame.dataSize = uset->lastGoodFrame.data->dataSize();
					uset->lastGoodFrame.dataOffset = 0;
				}
			}

			if (uset->minDate) {
				PDebug("Creating empty data frame for selective data with updates between dates: [%u, %u]", uset->minDate, uset->maxDate);
				size_t size = uset->universe ? uset->universe->size(uset->diEnd) : m_secMaster->size(uset->diEnd);
				uset->emptyFrame.data = createDataBuffer(uset->universe, uset->hData->dl(), uset->hData->ds(), uset->hData->dv().definition);
				uset->emptyFrame.data->ensureSize(1, size);
				uset->emptyFrame.di = 0;
				uset->emptyFrame.dataSize = uset->emptyFrame.data->dataSize();
				uset->emptyFrame.dataOffset = 0;
			}

			DataCacheInfo dci(rt, uset->hData->ds(), uset->hData->dv(), uset->hData->dv().definition, uset->di, uset->diStart,
				uset->diEnd, uset->hData->dl()->config(), uset->universe, false, (void*)&uset->hData, uset->secSrcUniverse);
			uset->hData->dl()->preBuild(*rt.dataRegistry, dci);
		}
	}

	void DataRegistry::finaliseUpdateSets(const RuntimeData& rt, UpdateSetPtrVec& usets) {
		ConfigSection* downloadSec = nullptr;

		for (auto uset : usets) {
			ASSERT(!uset->hDataOpen, "Cache Inconsistency! Data handle is still open! It should have been closed!");

			if (!downloadSec)
				downloadSec = const_cast<ConfigSection*>(uset->hData->dl()->config().getChildSection("Download"));

			DataCacheInfo dci(rt, uset->hData->ds(), uset->hData->dv(), uset->hData->dv().definition, uset->diStart, uset->diStart,
				uset->diEnd, uset->hData->dl()->config(), uset->universe, false, (void*)&uset->hData);
			dci.di = uset->di;
			uset->hData->dl()->postBuild(*rt.dataRegistry, dci);

			std::string dataId = uset->hData->dv().toString(uset->hData->ds());
			auto cacheId = getCacheId(uset->universe, dataId);

			{
				DR_WRITE_LOCK();
				m_updatedThisRun[cacheId] = true;
			}

			/// RELEASE THE LOCK HERE!
			uset->hData->unlock();

			if (downloadSec && uset->downloader) 
				finaliseDownload(rt, uset->downloader, downloadSec);

			uset->downloader = nullptr;
		}
	}

	void DataRegistry::invalidate(const RuntimeData& rt, UpdateSetPtrVec& usets) {
		if (m_readOnly && !rt.appOptions.updateDataOnly)
			return;

		PROFILE_SCOPED();

		size_t startIndex = 0;
		size_t maxUpdate = (size_t)s_maxSimultaneousUpdates;
		size_t endIndex = std::min(usets.size(), maxUpdate);
		size_t numIter = 0;

		while (startIndex < usets.size()) {
			invalidateRange(rt, usets, startIndex, endIndex);
			startIndex = endIndex;
			endIndex = std::min(usets.size(), startIndex + maxUpdate);
		}
	}

	void DataRegistry::invalidateRange(const RuntimeData& rt, UpdateSetPtrVec& usets_, size_t startIndex, size_t endIndex) {
		try {
			static const IntDay numUpdateInOneGo = 50;

			PROFILE_SCOPED();

			size_t maxUpdates = (size_t)s_maxSimultaneousUpdates;
			UpdateSetPtrVec usets;

			for (size_t i = startIndex; i < endIndex; i++)
				usets.push_back(usets_[i]);

			prepareUpdateSets(rt, usets);

			while (usets.size()) {
				auto iter = usets.begin();

				while (iter != usets.end()) {
					UpdateSet& uset = *(iter->get());

					/// This one has reached the end already so we erase it 
					if (uset.di > uset.diEnd) { /// diEnd is inclusive ...
						iter = usets.erase(iter);
						continue;
					}

					auto& hData 	= *uset.hData;
					auto dl 		= hData.dl();
					auto& ds 		= hData.ds();
					auto& def 		= hData.dv().definition;
					IntDay diStart	= def.doesUpdateInOneGo() ? uset.orgDiStart : uset.diStart;

					DataCacheInfo dci(rt, ds, hData.dv(), def, uset.di, diStart, uset.diEnd, dl->config(), uset.universe, (uset.di == uset.diEnd), 
						(void*)&uset.hData, uset.secSrcUniverse);

					/// If its a constant data then we do not need to update it and just return
					//if (hData.hdr().version != 0 && def.isConstant()) {
					//	Info("DR", "Constant data has already been updated: %s", def.toString(ds));
					//	hData.beginWrite(true, hData.hdr().rowSize);
					//	hData.writeHeader(dci, dci.di, dci.di, sizeof(float), 1);
					//	hData.endWrite();

					//	/// We're done with this now 
					//	iter = usets.erase(iter);
					//	continue;
					//}

					/// !!!DO NOT ERASE THIS ITERATOR AFTER THIS!!!
					iter++;

					std::vector<DataFrame> frames;
					IntDay diEnd = uset.di;

					if (uset.collectAllFramesBeforeWriting || usets.size() == 1)
						diEnd = uset.diEnd;
					else {
						/// TODO: We collect a certain number of frames before flushing them out
						//diEnd = std::min(uset.di + numUpdateInOneGo, uset.diEnd);
						diEnd = uset.di;
					}

					for (IntDay di = uset.di; di <= diEnd; di++) {
						DataFrame frame;
						IntDate diDate = rt.dates[di];
						bool tryGetNewFrame = (uset.minDate == 0 && uset.maxDate == 0 ? true : (diDate < uset.minDate && diDate > uset.maxDate));

						dci.di = di;
						dci.lastFrame = uset.lastGoodFrame;
						dci.customMetadata = uset.existingFtr.customMetadata;

						/// Over here we try to get the new frame!
						if (tryGetNewFrame) {
							DataRegistry::getNextFrame(m_secMaster.get(), rt, dci, uset, frame);

							if ((frame.data && frame.dataSize) || (frame.data && frame.data->doesCustomReadWrite())) {
								frames.push_back(frame);
								uset.diLast = di;
								uset.lastGoodFrame = frame;
							}
							else {
								bool useLastDataFrame = uset.useLastDayData || uset.useLastDayDataFrameIfEmpty;
								ASSERT(useLastDataFrame || frame.noDataUpdate, "Cannot have an empty frame without setting noDataUpdate to true (if its expected to be empty)");

								if (def.doesUpdateInOneGo()) {
									if (def.isSecurityMaster() && uset.orgDiStart < uset.diEnd) {
										ASSERT(false, "Fatal Error: Security master cannot be empty on any day other than the current running day!");
										return;
									}
									break;
								}

								int frequency = (int)def.frequency;
								/// Ok first we only allow last 2 days worth of data to be bad ...
								ASSERT(useLastDataFrame || di >= uset.diEnd - frequency, "Only Universal datasets are allowed to provide empty data frames. Dataset: " << hData.dv().toString(ds) << " - di: " << di << " - Date: " << rt.dates[di]);

								/// The ASSERT didn't hit so we're going to use the last good frame ...

								if (!uset.lastGoodFrame.data && useLastDataFrame) {
									//Warning("DR", "No good data available at all. Just ignoring ...");
									//continue;
									ASSERT(false, "Fatal error. No data available at all!");
								}

								ASSERT(uset.lastGoodFrame.data, "Invalid data with NO good frames at all!");

								DataFrame frame = uset.lastGoodFrame;

								if (uset.useNullDataFrameIfEmpty) {
									DataPtr lastData = uset.lastGoodFrame.data;

									size_t numCols = lastData->cols();

									if (dci.universe)
									    numCols = dci.universe->size(dci.di);

									/// Create a new frame and invalidate everything ...
									frame.data = DataPtr(lastData->createInstance());
									frame.data->ensureSizeAndDepth(lastData->rows(), numCols, lastData->depth(), false);
									frame.data->invalidateAll();

									Warning("DR", "[%u] - %s.%s: No good data available. USING <NULL> BUFFER!", rt.dates[di], uset.universe ? uset.universe->name() : std::string("<NONE>"), hData.dv().toString(ds));
								}
								else {
									Warning("DR", "[%u] - %s.%s: No good data available. Using the last day's data", rt.dates[di], uset.universe ? uset.universe->name() : std::string("<NONE>"), hData.dv().toString(ds));
								}

								frame.di = di;
								uset.diLast = uset.lastGoodFrame.di;

								frames.push_back(frame);

								if (frame.dataSize > uset.maxRowSize || uset.maxRowSize == 0)
									uset.maxRowSize = frame.dataSize;
							}
						}
						else {
							DataFrame* srcFrame = nullptr;

							/// Min dates a bit more tricky. We need to use an empty data frame for these dates. 
							/// However, for max dates (with NO min dates), we can just use the lastGoodFrame 
							if (uset.minDate && diDate < uset.minDate) {
								ASSERT(uset.emptyFrame.data, "Invalid empty frame with no data. Should have been created since this is a selective dataset!");

								PDebug("Using empty data frame because of minDate condition: %u < %u", diDate, uset.minDate);

								srcFrame = &uset.emptyFrame;
								uset.diLast = di;
							}
							else { /// There can be only one other case when this will hit and that is when diDate > uset.maxDate
								PDebug("Using empty data frame because of maxDate condition: %u > %u", diDate, uset.maxDate);

								srcFrame = &uset.lastGoodFrame;
								uset.diLast = uset.lastGoodFrame.di;
							}

							DataFrame frame = *srcFrame;
							frame.di = di;
							frames.push_back(frame);
						}

						uset.numNewRows++;
					}

					/// diEnd has already been handled!
					uset.di = diEnd + 1;

					if (!frames.size()) {
						if (def.doesUpdateInOneGo()) {
							hData.beginWrite(hData.doUpdate(), uset.maxRowSize);
							hData.writeHeader(dci, uset.di - 1, uset.diLast, uset.maxRowSize, 1);
							hData.writeFooter(uset.existingFtr.customMetadata);
							hData.endWrite();
						}
						continue;
					}

					if (uset.existingHdr.rowSize > uset.maxRowSize) {
						uset.maxRowSize = uset.existingHdr.rowSize;
					}

					bool doUpdate = hData.doUpdate();
					size_t numRows = uset.existingHdr.numRows;
					size_t offset = uset.existingHdr.rowSize;

					if (def.isAlwaysOverwrite() || def.isSecurityMaster())
						offset = 0;

					if (!uset.hDataOpen) {
						hData.beginWrite(doUpdate, uset.maxRowSize);
						uset.hDataOpen = true;
					}

					size_t numNewRows = 0;

					for (DataFrame& frame : frames) {
						ASSERT(frame.dataSize || frame.data->doesCustomReadWrite() , "Invalid datasize!");
						ASSERT(dci.def.itemSize == 0 || frame.dataSize % dci.def.itemSize == 0, "Data: " << dci.def.name << " - Invalid frame size: " << frame.dataSize << " - not aligned with item size: " << dci.def.itemSize);

						/// If the data does not update in one go then we just write it normally and incrementally
						if (!def.doesUpdateInOneGo()) {
							hData.writeDi(frame, rt.dates[frame.di]);
							numNewRows++;
						}
						/// For universal/one row data we need to update it slightly differently
						else {
							hData.write(frame, offset, true);
							offset += frame.dataSize;
							uset.maxRowSize = offset;
						}
					}

					frames.clear();

					if (def.doesUpdateInOneGo())
						numRows = 1;
					else {
						//numRows = uset.existingHdr.numRows + (uset.numNewRows - uset.historyOverlap);
						numRows = uset.diStart + uset.numNewRows;
					}

					ASSERT(numRows <= rt.dates.size(), "Invalid number of rows: " << numRows << " - max total: " << rt.dates.size());

					/// finalise the header at the end
					if ((uset.di - uset.diLastDataFlush > uset.numFramesBeforeFlush && uset.numFramesBeforeFlush > 0) || uset.di >= uset.diEnd) {
						hData.writeHeader(dci, uset.di - 1, uset.diLast, uset.maxRowSize, numRows); 

						/// Footer is only written at the end
						if (uset.di > uset.diEnd || def.doesUpdateInOneGo())
							hData.writeFooter(uset.existingFtr.customMetadata);

						hData.endWrite();
						uset.diLastDataFlush = uset.di;
						uset.hDataOpen = false;
					}
				}
			}

			/// Our copied array is empty now but it contained pointers from the source
			/// array so we just use that to finalise
			finaliseUpdateSets(rt, usets);
		}
		catch (const std::exception& e) {
			Util::reportException(e);
			Util::exit(1);
		}
	}

	void DataRegistry::defineAlias(const ConfigSection* module) {
		auto mappingsStr = module->getRequired("mappings");
		StrStr_Map mappings;

		FrameworkUtil::parseMappings(mappingsStr, &mappings);

		for (auto iter : mappings) {
			auto existingAlias = m_aliases.find(iter.first);
			if (existingAlias != m_aliases.end()) {
				PWarning("Alias is already defined to: %s = %s. Redefining to: %s = %s", existingAlias->first, existingAlias->second, iter.first, iter.second);
			}
			m_aliases[iter.first] = iter.second;
		}
	}

	IDataLoader* DataRegistry::getDataLoader(const std::string& dataId) {
		IDataLoaderPtrMap::iterator iter = m_dlMap.find(dataId);

		if (iter == m_dlMap.end())
			return nullptr;

		return iter->second.get();
	}

	void DataRegistry::clearData(const std::string& dataId) {
		DR_READ_WRITE_LOCK();

		DataCacheMap::iterator iter = m_dataCache.find(dataId);

		if (iter != m_dataCache.end()) {
			//iter->second = nullptr;
			m_dataCache.erase(iter);
            PTrace("Cleared data: %s [Cache items: %z]", dataId, m_dataCache.size());
		}
	}

	DataPtr DataRegistry::setData(const std::string& id, Data* data) {
		/// WE OWN IT NOW!
		return cacheData(id, DataRegistry::CacheItem(DataPtr(data)));
	}

	//DataPtr DataRegistry::setData(const std::string& id, DataPtr data) {
	//	ASSERT(!id.empty(), "Cannot setData for an empty id!");
	//	m_dataCache[id] = DataRegistry::CacheItem(DataPtr(data));
	//	return m_dataCache[id].data;
	//}

	DataPtr DataRegistry::cacheData(const std::string& id, DataRegistry::CacheItem cacheItem) {
		ASSERT(!id.empty(), "Cannot setData for an empty id!");
		m_dataCache[id] = cacheItem;

		/// Put it in all the checkpoints
		if (m_checkpoints.size()) { 
			for (CheckpointPtr cp : m_checkpoints) 
				cp->dataIds.push_back(id);
		}

		return m_dataCache[id].data;
	}

	pesa::Dataset DataRegistry::prepareForLoad(const std::string& id) {
		DataDefinition def;
		std::string dsName, defName;
		parseDataName(id, dsName, defName);

		{
			DR_READ_LOCK();

			ASSERT(m_dataDefMap.find(id) != m_dataDefMap.end(), "Unable to find definition for data: " << id);
			def = m_dataDefMap[id];
		}

		// if the data does not exist then we need to load it ...
		Dataset ds(dsName);
		ds.values.push_back(DataValue(def));

		return ds;
	}

	Metadata DataRegistry::getMetadata(IUniverse* universe, const std::string& id) {
		Dataset ds 		= prepareForLoad(id);
		DataValue& dv 	= ds.values[0];
		IDataLoaderPtr dl = m_dlMap[id];

		try {
			DataHandle hData(universe, *this, dl, ds, dv, parentConfig()->defs().dataCachePath(), getDataPath(universe, dl->config(), ds, dv.definition));

			DR_READ_LOCK();
			hData.readHeader();
			return hData.hdr().metadata;
		}
		catch (const Poco::FileNotFoundException&) {
		}
		catch (const Poco::Util::IncompatibleOptionsException&) {
		}

		return Metadata();
	}

	DataHeader DataRegistry::getDataHdr(IUniverse* universe, const std::string& id, std::map<IntDate, size_t>* seekMap, Metadata* metadata /* = nullptr */) {
		std::string cacheId = getCacheId(universe, id);
		ASSERT(m_dlMap.find(id) != m_dlMap.end(), "Unable to find DataLoader for: " << id);
		ASSERT(pipelineHandler(), "Invalid pipeline handler!");

		Dataset ds 				= prepareForLoad(id);
		DataValue& dv 			= ds.values[0];
		DataDefinition def 		= dv.definition;
		IDataLoaderPtr dl 		= m_dlMap[id];
		std::string rootPath 	= parentConfig()->defs().dataCachePath();

		DataHandle hData(universe, *this, dl, ds, dv, rootPath, getDataPath(universe, dl->config(), ds, dv.definition));
		hData.readHeader();

		if (seekMap) {
			hData.constructSeekMap();
			*seekMap = hData.seekMap();
		}

		return hData.hdr();
	}

	std::vector<IUniverse*> DataRegistry::getActiveUniverses() {
		std::vector<IUniverse*> universes;

		for (IUniversePtr universe : m_universes)
			universes.push_back(universe.get());

		return universes;
	}

	StringVec DataRegistry::regexDataLoaders(const std::string& search) {
		StringVec dlNames;
		std::regex regex(search);

        std::smatch match1;
        std::regex e("gmt_open_*");
        std::string tmp = "gmt_open_US";

        if (std::regex_search(tmp, match1, e))
            dlNames.push_back(tmp);

		for (auto iter = m_dlMap.begin(); iter != m_dlMap.end(); iter++) { 
			std::string dlName = iter->first;
			std::smatch match;

			if (std::regex_search(dlName, match, regex))
				dlNames.push_back(dlName);
		}

		return dlNames;
	}

	DataPtrVec DataRegistry::getAllDataPtr(IUniverse* universe, const std::string& dataId, DataInfo* dinfo) {
		StringVec dlNames = regexDataLoaders(dataId);
		DataPtrVec dataVec;

		for (std::string& dlName : dlNames) {
			DataPtr data = getDataPtr(universe, dlName, dinfo);
			dataVec.push_back(data);
		}

		return dataVec;
	}

	void* DataRegistry::createDataCheckpoint() {
		DR_READ_LOCK();
		CheckpointPtr cp = std::make_shared<Checkpoint>();
		m_checkpoints.push_back(cp);
		return reinterpret_cast<void*>(cp.get());
	}

	void DataRegistry::deleteDataCheckpoint(void* checkpoint) {
		DR_READ_LOCK();
		Checkpoint* cp = reinterpret_cast<Checkpoint*>(checkpoint);

		/// Find it and remove from the checkpoint list first!
		auto iter = m_checkpoints.begin();

		while (iter != m_checkpoints.end()) {
			CheckpointPtr iterPtr = *iter;
			if (cp == iterPtr.get())
				break;
			iter++;
		}

		ASSERT(iter != m_checkpoints.end(), "Unable to find checkpoint!");

		/// Now we clear the checkpoint ...
		for (const auto& dataId : cp->dataIds)
			this->clearData(dataId);

		cp->dataIds.clear();

		m_checkpoints.erase(iter);
	}

	std::string DataRegistry::fixDataId(const std::string& id_) {
		std::string id = id_;

		/// This is for backwards compatibility when we used to support aliases starting with @ only
		if (id[0] == '@') {
			id = id.substr(1);
			auto aliasIter = m_aliases.find(id);

			/// First of all we try to see whether it's a full alias
			if (aliasIter != m_aliases.end()) 
				id = aliasIter->second;
			/// Otherwise we check each of the parts as an alias
			else {
				auto idParts = Util::split(id, ".");
				bool didFind;

				for (auto i = 0; i < idParts.size(); i++) {
					aliasIter = m_aliases.find(idParts[i]);
					if (aliasIter != m_aliases.end()) {
						idParts[i] = aliasIter->second;
						didFind = true;
					}
				}

				if (didFind)
					return Util::combine(idParts);
			}
		}
		else {
			/// We now support aliases that get mapped even if you don't use the @ character
			auto aliasIter = m_aliases.find(id);
			if (aliasIter != m_aliases.end())
				id = aliasIter->second;
		}

		return id;
	}

	DataPtr DataRegistry::getDataPtr(IUniverse* universe, const std::string& id_, DataInfo* dinfo_) {

		PROFILE_SCOPED();

		std::string id			= fixDataId(id_);
		DataInfo dinfo			= dinfo_ ? *dinfo_ : DataInfo();
		auto noCacheUpdate		= dinfo.noCacheUpdate;
		auto onlyCheckCached	= dinfo.onlyCheckCached;
		auto noThrow			= dinfo.noAssertOnMissingDataLoader;
		auto* metadata			= dinfo.metadata;
		bool dataWindow			= dinfo.dataWindow;
		const RuntimeData& rt	= pipelineHandler()->runtimeData();
		std::string cacheId		= getCacheId(universe, id);

		/// We don't apply windowed data logic to security master data ...
		if (universe == m_secMaster.get() || !m_secMaster)
			dinfo.dataWindow	= false;

		/// If we're in the prepare stage, we load the entire content
		if (rt.simState == kSS_Prepare)
			dinfo.loadEntire	= true;

		{
			DR_READ_LOCK();

			DataCacheMap::iterator iter = m_dataCache.find(cacheId);

			// first we see whether the data has already been loaded
			if (iter != m_dataCache.end()) {
				iter->second.lastUse = rt.di;
				return iter->second.data;
			}

			/// If no cache update 
			if (onlyCheckCached)
				return nullptr;
		}

		auto iter = m_dlMap.find(id);
		if (noThrow && iter == m_dlMap.end())
			return nullptr;

		ASSERT(iter != m_dlMap.end(), "Unable to find DataLoader for: " << id);
		ASSERT(pipelineHandler(), "Invalid pipeline handler!");

		Dataset ds 				= prepareForLoad(id);
		DataValue& dv 			= ds.values[0];
		DataDefinition def 		= dv.definition;
		IntDate startDate 		= rt.getStartDate();
		IntDate endDate 		= rt.dates[rt.diSimEnd()];
		IDataLoaderPtr dl 		= iter->second;
		std::string rootPath 	= parentConfig()->defs().dataCachePath();
        bool getAgain 			= false;
		bool getFromSecMaster	= false;

		/// If the data is to be made available only during specific times then we don't return
		if (def.isUpdateAfterMarketOpen() && !rt.isAfterMarketOpen())
			return nullptr;
		else if (def.isUpdateBeforeMarketClose() && !rt.isBeforeMarketClose())
			return nullptr;
		else if (def.isUpdateDuringTradingHours() && !rt.isInTradingHours())
			return nullptr;

		/// If we've been asked to wait for the TIMESTAMP then we just wait indefinitely
		if (rt.appOptions.waitForTimestamp && !rt.appOptions.updateDataOnly) {
			waitForTimestamp(s_timestamp, rt.appOptions.waitInterval);
		}

		DataHandlePtr hData		= DataHandlePtr(new DataHandle(universe, *this, dl, ds, dv, rootPath, getDataPath(universe, dl->config(), ds, dv.definition)));
		IntDay delay			= getDelay(*(hData.get()));

		hData->dates()			= &rt.dates; 
		hData->delay()			= delay;
		hData->cacheId()		= cacheId;
		hData->rowHeaders()		= dinfo.rowHeaders;

		try {
			DR_READ_LOCK();

			hData->readHeader(); 

			/// If there is no data at all then we don't save anything ...
			//if (hData->hdr().rowSize == 0)
			//	return nullptr;

			/// If the data is marked as update after market open and it hasn't been updated after market open then 
			/// we cannot use this data anymore
			Poco::DateTime lastUpdateTime(hData->getUpdateTimestamp());
			if (def.isUpdateAfterMarketOpen() && !rt.isTimeAfterMarketOpen(lastUpdateTime))
				return nullptr;


			if (universe && universe != m_secMaster.get()) {
				Poco::Timestamp tsDataUpdate;
				bool isPrevSecMaster = universe->config().isSecMaster();

				/// Any previous sec masters always load from their own directory
				if (!isPrevSecMaster) {
					bool isValidTimestamp = getTimestamp(s_timestamp, tsDataUpdate);

					if (isValidTimestamp) {
						Poco::Timestamp srcDataUpdateTimestamp = hData->getUpdateTimestamp();

						/// If source data timestamp is less than the update timestamp then we force load from 
						/// the security master ...
						if (srcDataUpdateTimestamp < tsDataUpdate) {
							PDebug("Data is stale: %s. Force using security master data ...", cacheId);
							getFromSecMaster = true;
						}
					}
					else {
						/// If we cannot the TIMESTAMP at all then we ALWAYS get the data from the security master
						PDebug("Unable to get TIMESTAMP. Using security master copy for data: ", cacheId);
						getFromSecMaster = true;
					}
				}
			}

			if (!getFromSecMaster || !universe || universe == m_secMaster.get()) {
				/// We try to see if the number of rows match up to what we're expecting
				auto readHdr = hData->hdr();
				if (universe && m_secMaster && universe != m_secMaster.get() && readHdr.numRows < rt.dates.size() - 1) {
					if (universe->config().getBool("optimise", false))
						PWarning("[%s]-The optimised data is either not ready or invalid. If you are repeatedly getting this warning, please contact support. IGNORING ...", cacheId);
					throw Poco::Exception("Unusable optimised data. Going to use data from master ...");
				}

				/// Copy the metadata over
				if (metadata)
					*metadata = hData->hdr().metadata;

				if (DataRegistry::doesNeedUpdate(dl->config(), universe, rt, ds, dv, hData->hdr(), startDate, endDate, hData->delay(), m_readOnly) && !noCacheUpdate && !m_readOnly) {
					/// If an update was required and noCacheUpdate is not true then we update the cache ...
					runForData(rt, dl);

					/// get the data again
					if (!noCacheUpdate)
						getAgain = true;
				}
				else {
					DataPtr cachedData = nullptr; 
				
					if (!cachedData) {
						size_t startRow = 0, endRow = 0;

						/// Data window only applies to floating point data right now ...
						if (dinfo.dataWindow) {
							auto& def = hData->dv().definition;
							if (def.itemType != DataType::kFloat && def.itemType != DataType::kDouble)
								dinfo.dataWindow = false;
							else {
								size_t backdays = (size_t)(parentConfig()->defs().backdays());
								ASSERT(backdays < rt.diStart, "Invalid number of backdays: " << rt.diStart);
								startRow = (size_t)rt.diStart - backdays;
								endRow = (size_t)(rt.diStart);
							}
						}

						cachedData = hData->read(rt, &dinfo, startRow, endRow);

						if (dinfo.dataWindow) {
							WindowData wdata;
							wdata.data = cachedData;
							wdata.hData = hData;
							m_windowData.push_back(wdata);
						}
					}

					ASSERT(cachedData, "Unable to create data buffer!");

					/// This is already optimised for the universe passed in ... tell it as such!
					if (cachedData)
						cachedData->clearUniverse();

					/// data is upto date and so we can just return it ...
					if (!DataRegistry::doesNeedUpdate(dl->config(), universe, rt, ds, dv, hData->hdr(), startDate, endDate, hData->delay(), m_readOnly)) {
						cachedData = dl->dataLoaded(rt, *this, cachedData, cacheId, universe);
						return cacheData(cacheId, DataRegistry::CacheItem(hData->hdr(), cachedData, rt.di));
					}
					else { // if (!noCacheUpdate) {
						/// This becomes temporary data ... we need to clear it in the next tick cycle
						/// TODO: investigate the use of tick data cache
						cachedData = dl->dataLoaded(rt, *this, cachedData, cacheId, universe);
						return cacheData(cacheId, DataRegistry::CacheItem(hData->hdr(), cachedData, rt.di));
					}

					return cachedData;
				}
			}
		}
		catch (const Poco::FileNotFoundException&) {
			if (noCacheUpdate)
				return nullptr;
			getFromSecMaster = true;
		}
		catch (const Poco::Util::IncompatibleOptionsException&) {
			if (noCacheUpdate)
				return nullptr;
			getFromSecMaster = true;
		}
		catch (const Poco::Exception& e) {
			// Util::reportException(e);
			PWarning("%s", e.displayText());
			if (noCacheUpdate)
				return nullptr;
			getFromSecMaster = true;
		}
		catch (const std::exception& e) {
			// Util::reportException(e);
			PWarning("%s", std::string(e.what()));
			if (noCacheUpdate)
				return nullptr;
			getFromSecMaster = true;
		}

		if (getAgain) {
			dinfo.noCacheUpdate = true;
			return getDataPtr(universe, id, &dinfo);	/// We don't want to go into an infinite loop so second time around we force no cache update.
														/// The data MUST have loaded by now. If not then there is a problem
		}
		else if (getFromSecMaster) {
			return getSecurityMasterDataPtr(universe, id, dinfo, hData->hdr(), dl, cacheId, dv);
		}

		return nullptr;
	}

	DataPtr DataRegistry::getSecurityMasterDataPtr(IUniverse* universe, const std::string& id, DataInfo& dinfo, 
		const DataHeader& hdr, IDataLoaderPtr dl, const std::string& cacheId, DataValue& dv) {
		const RuntimeData& rt	= pipelineHandler()->runtimeData();
		DataDefinition def 		= dv.definition;

		/// If the file was not found then we still need to check that whether the data has been
		/// cached for a higher (super-set) universe of the current universe. Right now that 
		/// can only be the SecurityMaster
		if (universe && universe == m_secMaster.get()) {
			runForData(rt, dl);
		}
		else {
			DR_READ_LOCK();

			auto secMasterCacheId = getCacheId(m_secMaster.get(), id);
			DataInfo dinfo;
			dinfo.onlyCheckCached = true;

			/// First of all we check whether data is already in the cache or not ...
			DataPtr cachedData = getDataPtr(m_secMaster.get(), id, &dinfo);
			DataPtr data = cachedData;
			if (!data)
				data = getDataPtr(m_secMaster.get(), id, nullptr);

			if (data) {
				auto dl = getDataLoader(id);
				auto& config = dl->config();
				auto doOptimise = config.getBool("optimiseForUniverse", false);

				/// We always enable optimisation for floating type data ...
				if (def.itemType == DataType::kFloat || def.itemType == DataType::kDouble)
					doOptimise = true;

				doOptimise = doOptimise && !dinfo.noOptimise;

				if (universe && doOptimise && !rt.appOptions.disableOptimisation) {
					ASSERT(!config.getBool("universeSensitive"), "Data is universes sensitive and cannot be trivially optimised in memory: " << cacheId);

					DataPtr optimisedDataPtr = nullptr;
					optimisedDataPtr = optimiseDataForCrossUniverse(data, 0, id, m_secMaster.get(), universe);

					if (optimisedDataPtr) {
						optimisedDataPtr->universe() = nullptr;
						optimisedDataPtr = dl->dataLoaded(rt, *this, optimisedDataPtr, cacheId, universe);
						cacheData(cacheId, DataRegistry::CacheItem(hdr, optimisedDataPtr, rt.di));

						/// If the sec master data was not already in the cache then we just remove it from the cache 
						if (!cachedData)
							clearData(secMasterCacheId);

						return optimisedDataPtr;
					}
				}

				/// If we are unable to optimise for some reason ... then we just load the securitymaster data and pass that 
				/// back ...
				DataPtr uwData = DataPtr(data->clone(universe, false));
				uwData->universe() = universe;
				return cacheData(cacheId, DataRegistry::CacheItem(hdr, uwData, rt.di));
			}

			/// Map to a target universe 

			return data;
		}

		return nullptr;
	}

	void DataRegistry::optimiseDatasets(const RuntimeData& rt) {
		IUniversePVec universes;

		for (size_t i = 0; i < m_universes.size(); i++) {
			IUniverse* universe = m_universes[i].get();

			if (universe->config().getBool("optimise", false) == false) {
				PDebug("Optimisation disabled on universe: %s", universe->name());
				continue;
			}

			universes.push_back(universe);
		}

		optimiseDatasets(universes, rt);
	}

	IntDay DataRegistry::getDelay(DataHandle& hData) {
		/// Universes and universal datasets need to be update until TODAY
		const ConfigSection& config = hData.dl()->config();
		if (config.isUniverse() || hData.dv().definition.isStatic() || hData.dv().definition.doesUpdateInOneGo())
			return 0;

		/// While others will get updated to the Final backtest day MINUS the delay
		/// if today is a holiday
		return config.delay();
	}

	void DataRegistry::optimiseDatasets(IUniverse* universe, const RuntimeData& rt) {
		IUniversePVec universes = { universe };
		optimiseDatasets(universes, rt);
	}

	void DataRegistry::optimiseDatasets(IUniversePVec& universes, const RuntimeData& rt) {
		if (m_readOnly && !rt.appOptions.updateDataOnly)
			return; 

		/// Optimisation is disabled altogether
		if (rt.appOptions.disableOptimisation)
			return;

		PDebug("Number of datasets to optimise: %z", m_dsOptimal.size());

		std::string rootPath = parentConfig()->defs().dataCachePath();
		IntDate startDate = rt.getStartDate();
		IntDate endDate = rt.dates[rt.diSimEnd()];
		IUniverse* secMaster = m_secMaster.get();

		for (DataRegistry::OptimalDataset& dso : m_dsOptimal) {
			IDataLoaderPtr dl = dso.dl;
			IUniversePVec targetUniverses = universes;

			if (dso.optimiseTargets.size()) {
				targetUniverses.clear();

				for (const std::string& optimiseTarget : dso.optimiseTargets) {
					IUniverse* targetUniverse = getUniverse(optimiseTarget);
					ASSERT(targetUniverse, "Unable to load optimiseTarget universe: " << optimiseTarget);
					targetUniverses.push_back(targetUniverse);
				}
			}

			for (Dataset& ds : dso.dss.datasets) {

				for (DataValue& dv : ds.values) { 
					std::string srcDataId = dv.definition.toString(ds);
					DataPtr srcData = nullptr;

					for (IUniverse* universe : targetUniverses) {
						auto universeName = universe->name();

						/// If optimisation is disabled on the universe ...
						if (universe->config().getBool("optimise", false) == false) 
							continue;

						std::string optimiseDatasets = universe->config().getString("optimiseDatasets", "");
						if (!optimiseDatasets.empty()) {
							StringVec optDatasets = Util::split(optimiseDatasets, "|");
							bool shouldHandle = false;

							for (const std::string& optDataset : optDatasets) {
								if (optDataset == ds.name) {
									shouldHandle = true;
									break;
								}
							}

							if (!shouldHandle)
								continue;
						}

						IntDay diStart = 0;
						DataHandle hData(universe, *this, dl, ds, dv, rootPath, getDataPath(universe, dl->config(), ds, dv.definition));
						bool alwaysUpdate = dl->config().getBool("alwaysUpdate", false);

						auto uset = std::make_shared<UpdateSet>();

						/// We don't optimise string datasets!
						if (dv.definition.itemType == DataType::kString || 
							dv.definition.isVariableLengthData())
							continue;

						uset->universe = universe;
						uset->hData = new DataHandle(universe, *this, dl, ds, dv, rootPath, getDataPath(universe, dl->config(), ds, dv.definition));
						uset->delay = getDelay(*uset->hData);
						uset->minDaysToUpdate = uset->hData->dl()->config().get<int>("minDaysToUpdate", rt.appOptions.days);
						uset->isOptimising = true;

						/// For the purpose of optimisation, we always write out each dataset at once!
						//uset->collectAllFramesBeforeWriting = true;

						try {

							uset->hData->readHeader();
							ASSERT(uset->hData->hdr().isValid(), "Invalid header loaded! If for some reason dataset cannot be loaded then loadHeader must throw (FileNotFoundException)!");

							/// If the data does not need any update then just ignore it ...
							if (!(alwaysUpdate && rt.appOptions.updateDataOnly) && 
								!DataRegistry::doesNeedUpdate(dl->config(), universe, rt, ds, dv, uset->hData->hdr(), startDate, endDate, uset->delay, m_readOnly)) {
								dummyDataUpdate(dl, universe, rt, ds, dv, dv.definition, uset->hData);
								continue;
							}

							uset->hData->closeReadStream();

							/// otherwise we update the data
							IntDay startIndex = rt.getDateIndex(uset->hData->hdr().endDate);

							/// OK, if we get this then that most probably means that the dates have not been constructed so far
							/// in that case we can ONLY move forward if the data is one of those that updates in one go
							if (startIndex == Constants::s_invalidDay && dv.definition.doesUpdateInOneGo())
								startIndex = 0;
							else
								ASSERT(startIndex != Constants::s_invalidDay, "Invalid date index returned for date: " << uset->hData->hdr().endDate << " [Data: "
									<< dv.toString(ds) << " - Non-Universal]");

							/// If its an always overwrite set then just update everything ...
							if (dv.definition.isAlwaysOverwrite())
								uset->diStart = 0;
							else
								uset->diStart = startIndex + uset->delay;

							PDebug("Updating data: [%u - %u] - %s.%s", rt.dates[uset->diStart], endDate, universe->name(), dv.definition.toString(ds));
						}
						catch (const Poco::FileNotFoundException&) {
							/// now load the entire dataset
							PDebug("Loading dataset from scratch: %s.%s", universe->name(), dv.definition.toString(ds));
							uset->diStart = 0;
						}
						catch (const Poco::Util::IncompatibleOptionsException&) {
							/// now load the entire dataset
							PDebug("Loading dataset from scratch because of incompatible options: %s.%s", universe->name(), dv.definition.toString(ds));
							uset->diStart = 0;
						}
						catch (const Poco::Exception& e) {
							std::cout << "Reporting ..." << std::endl;
							Util::reportException(e);
						}

						/// We don't optimise string data (for the time being) ...
						if (dv.definition.itemType == DataType::kString)
							continue;

						bool noUpdate = uset->hData->dl()->config().getBool("noUpdate", false);
						if (noUpdate)
							continue;

						while (m_asyncFutures.size() >= s_maxThreads) {
							auto iter = m_asyncFutures.begin();
							iter->wait();
							m_asyncFutures.erase(iter);
						}

						if (!srcData) {
							DataInfo dinfo;
							dinfo.noCacheUpdate = true;
							dinfo.loadEntire = true;
							dinfo.noOptimise = true;

							srcData = getDataPtr(secMaster, srcDataId, &dinfo);

							ASSERT(srcData, "Unable to get data: " << srcDataId);

							/// We currently do not support optimising custom RW datasets
							if (srcData->doesCustomReadWrite())
								break;
						}

						/// Execute it in an async block
						bool universeSensitive = dl->config().getBool("universeSensitive", false);
						bool supportsAsync = dl->config().getBool("async", true);

						/// If it's not sensitive to universe and the data loader supports async operations, then we just update it asynchronously 
						if (!universeSensitive && supportsAsync) {
							auto future = std::async(std::launch::async, &DataRegistry::optimiseAsync, this, rt, uset, srcData);
							m_asyncFutures.push_back(std::move(future));
						}
						else {
							/// Otherwise we update it synchronously under the assumption that this data is 
							/// order sensitive! (or does not support async updates)
							optimiseAsync(rt, uset, srcData);
						}
					}

					/// clear this data from the cache ...
					if (srcData)
						clearData(getCacheId(secMaster, srcDataId));
				}
			}
		}

		m_asyncFutures.clear();
		Debug("DR", "Dataset optimisation DONE!");
	}

	int DataRegistry::optimiseAsync(const RuntimeData& rt, UpdateSetPtr uset, DataPtr srcData) {
		UpdateSetPtrVec usets;

		auto* secMaster = getSecurityMaster();
		IntDay diCacheStart = std::max(uset->diStart - uset->minDaysToUpdate, 0);
		std::string dataId = uset->hData->dv().definition.toString(uset->hData->ds());
		const auto& config = uset->hData->dl()->config();
		bool universeSensitive = config.getBool("universeSensitive", false);

		if (!universeSensitive && srcData) {
			DataPtr newData = optimiseDataForCrossUniverse(srcData, diCacheStart, dataId, secMaster, uset->universe, false);

			if (!newData)
				return 0;

			uset->srcData = newData;
			uset->srcDataIndex = (size_t)diCacheStart;
		}

		usets.push_back(uset);
		DataRegistry::invalidate(rt, usets);

		return 0;
	}

	DataPtr DataRegistry::optimiseDataForCrossUniverse(DataPtr data, IntDay diStart, const std::string& id, IUniverse* srcUniverse, IUniverse* dstUniverse, bool cacheData /* = true */) {
		ASSERT(data && srcUniverse && dstUniverse, "Invalid data passed for cross-universe data optimiser!");

		if (data->def().isConstant())
			return data;

		/// This is a kind of data that cannot be optimised (e.g. PIT baseData) ...
		if (data->cols() <= 1)
			return data;

		PDebug("Optimising data: %s.%s", dstUniverse->name(), id);
		Data* dstData = data->optimiseFor(diStart, srcUniverse, dstUniverse);

		if (!dstData) {
			PWarning("Unable to optimise data: %s", id);
			return nullptr;
		}

		ASSERT(dstData, "Unable to optimise data from universe " << srcUniverse->name() << " => " << dstUniverse->name());

		/// Make sure that the delay of the two datasets is exactly the same!
		dstData->delay() = data->delay();

		/// Cache the data for later retrieval if an id was given. The DataRegistry is going to handle
		/// the deletion of this pointer
		if (!id.empty() && cacheData) {
			std::string cacheId = getCacheId(srcUniverse, id);
			return setData(cacheId, dstData);
		}

		/// Otherwise ... Return a shared_pointer so that the caller is responsible for freeing the data
		return DataPtr(dstData);

		//ASSERT(def.isDaily(), "Optimisation only supported for daily data so far!");

		//PInfo("Optimise data: %s [%s => %s]", id, srcUniverse->name(), dstUniverse->name());

		//size_t maxCols = 0;

		//for (IntDay di = 0; di < (IntDay)data->rows(); di++) {
		//	size_t numCols = dstUniverse->size(di);

		//	if (numCols > maxCols)
		//		maxCols = numCols;
		//}

		//ASSERT(maxCols > 0, "Invalid maxCols for universe: " << dstUniverse->name());

		//FloatMatrixData* dstData = new FloatMatrixData(def, data->rows(), maxCols, false);

		//for (IntDay di = 0; di < (IntDay)data->rows(); di++) {
		//	for (Security::Index ii = 0; ii < (Security::Index)maxCols; ii++) {
		//		Security::Index iiSrc = srcUniverse->index(di, ii);

		//		ASSERT(iiSrc != Security::s_invalidIndex, "Unable to get security from universe " << dstUniverse->name() << " for (" << di << ", " << ii << ")");

		//		float srcValue = (*data)(di, iiSrc);
		//		(*dstData)(di, ii) = srcValue;
		//	}
		//}

	}

	void DataRegistry::runForData(const RuntimeData& rt, IDataLoaderPtr dl) {
		try {
			/// see what do we have for this dataset
			Datasets datasets;

			dl->initDatasets(*this, datasets);

			PTrace("Got num datasets: %z", datasets.size());

			std::string rootPath = parentConfig()->defs().dataCachePath();
			IntDate startDate = rt.getStartDate();
			IntDate endDate = rt.getEndDate();

			std::vector<IUniversePtr> universes;
			const ConfigSection& mconfig = dl->config<ConfigSection>();
			IUniverse* secSrcUniverse = m_secMaster.get();
			std::string secSrcUniverseId = mconfig.getString("secSrcUniverse", "");
			bool alwaysUpdate = mconfig.getBool("alwaysUpdate", false);

			if (!secSrcUniverseId.empty()) {
				secSrcUniverse = getUniverse(secSrcUniverseId);
			}

			if (mconfig.getBool("optimiseForUniverse") == true && dl->optimiseForUniverse()) {
				//universes = m_universes;
				/// We only collect data for the security master ... after that 
				/// we optimise it for other universes!
				universes.push_back(m_secMaster);
			}
			else {
				/// If its a universe!
				if (mconfig.isUniverse())
					universes.push_back(nullptr);
				else
					universes.push_back(m_secMaster);
			}

			{
				DR_READ_WRITE_LOCK();

				std::string id = dl->id();

				if (m_dlMap.find(id) == m_dlMap.end())
					m_dlMap[id] = dl;
				//else
				//	PWarning("DataLoader %s duplicate entry found. Is this the intention?", id);
				//ASSERT(m_dlMap.find(id) == m_dlMap.end(), "Duplicate ID for a data module: " << id);

                /// add the datasets to the map ...
				for (auto& ds : datasets.datasets) {
					for (auto& dv : ds.values) {
						std::string dataDefId = dv.definition.toString(ds);
						auto iter = m_dlMap.find(dataDefId);

						//if (iter != m_dlMap.end() && iter->second.get() != dl.get())
						//	PWarning("Dataset was redefined, previous definition will be overwritten: %s", dataDefId);

						//PTrace("Dataset: %s", dataDefId);

						m_dataDefMap[dataDefId] = dv.definition;
						m_dlMap[dataDefId] = dl;
					}
				}
            }

			if (!rt.appOptions.updateDataOnly)
				return;

			/// Now that we have the lock ... we need to check whether the data needs update again ...
			/// REASON for doing this: We don't want to acquire the lock unless we're absolutely 
			/// sure that we "might" need update. If we do then we lock and then check again. The reason 
			/// why we check again is that while we were trying to acquire the lock, some other process
			/// (which had the lock at the time) might have already updated the data. 
			/// The reason for this logic is efficiency. Read access will far outnumber write access
			/// hence we don't want to have write access to the file while we will just be reading 
			/// from it most of the time!
#define LK_CHK()	uset->hData->lock(); \
					if (!(alwaysUpdate && rt.appOptions.updateDataOnly) && \
						!DataRegistry::doesNeedUpdate(dl->config(), universe.get(), rt, ds, dv, uset->hData->hdr(), startDate, endDate, uset->delay, m_readOnly)) { \
						dummyDataUpdate(dl, universe.get(), rt, ds, dv, dv.definition, uset->hData); \
						uset->hData->unlock(); \
						continue; \
					} 

			for (auto universe : universes) {
				/// Create a checkpoint for the data ...
				void* checkpoint = nullptr;

				if (rt.appOptions.updateDataOnly)
					checkpoint = createDataCheckpoint();

				{
					DR_WRITE_LOCK();

					/// preDataBuild is called for each of the universes
					DataCacheInfo dci(rt, Dataset(), DataValue(DataDefinition()), DataDefinition(), Constants::s_invalidDay, Constants::s_invalidDay, Constants::s_invalidDay, dl->config(),
						m_secMaster.get(), false, nullptr, secSrcUniverse);
					dl->preDataBuild(*this, dci);
				}

				UpdateSetPtrVec usets;

				for (auto& ds : datasets.datasets) {
					if (mconfig.getBool("optimiseForUniverse") && dl->optimiseForUniverse()) {
						Datasets dss;
						dss.datasets.push_back(ds);
						DataRegistry::OptimalDataset dso(dl, dss);
						std::string optimiseTargets = mconfig.getString("optimiseTargets", "");

						if (!optimiseTargets.empty()) {
							dso.optimiseTargets = Util::split(optimiseTargets, "|");
						}
						m_dsOptimal.push_back(dso);
					}

					for (auto& dv : ds.values) {
						auto uset = std::make_shared<UpdateSet>();

						uset->universe = universe.get();
						uset->secSrcUniverse = secSrcUniverse;
						uset->hData = new DataHandle(universe.get(), *this, dl, ds, dv, rootPath, getDataPath(universe.get(), dl->config(), ds, dv.definition));
						uset->delay = getDelay(*uset->hData);

						/// Just double check over here ...
						if (!(alwaysUpdate && rt.appOptions.updateDataOnly) && 
							!DataRegistry::doesNeedUpdate(dl->config(), universe.get(), rt, ds, dv, uset->hData->hdr(), startDate, endDate, getDelay(*uset->hData), m_readOnly))
							continue;

						try {
							uset->hData->readHeader();
							ASSERT(uset->hData->hdr().isValid(), "Invalid header loaded! If for some reason dataset cannot be loaded then loadHeader must throw (FileNotFoundException)!");

							/// If the data does not need any update then just ignore it ...
							//if (!DataRegistry::doesNeedUpdate(dl->config(), universe.get(), rt, ds, dv, uset->hData->hdr(), startDate, endDate, uset->delay, m_readOnly)) {
							//	dummyDataUpdate(dl, universe.get(), rt, ds, dv, dv.definition, uset->hData);
							//	continue;
							//}

							LK_CHK();

							uset->hData->closeReadStream();

							/// otherwise we update the data
							IntDay startIndex = rt.getDateIndex(uset->hData->hdr().endDate);
							
							/// OK, if we get this then that most probably means that the dates have not been constructed so far
							/// in that case we can ONLY move forward if the data is one of those that updates in one go
							if (startIndex == Constants::s_invalidDay && dv.definition.doesUpdateInOneGo())
								startIndex = 0;
							else
								ASSERT(startIndex != Constants::s_invalidDay, "Invalid date index returned for date: " << uset->hData->hdr().endDate << " [Data: " 
									<< dv.toString(ds) << " - Non-Universal]");

							PDebug("Updating data: (%u - %u] - %s", uset->hData->hdr().endDate, endDate, dv.definition.toString(ds));
							uset->diStart = startIndex + 1;
						}
						catch (const Poco::FileNotFoundException&) {
							/// now load the entire dataset>	PesaImpl.dll!pesa::DataRegistry::runForData(const pesa::RuntimeData & rt, std::shared_ptr<pesa::IDataLoader> dl) Line 2181	C++	Symbols loaded.

							PDebug("Loading dataset from scratch: %s.%s", universe ? universe->name() : "<NONE>", dv.definition.toString(ds));
							uset->diStart = 0;
							LK_CHK();
						}
						catch (const Poco::Util::IncompatibleOptionsException&) {
							/// now load the entire dataset
							PDebug("Loading dataset from scratch because of incompatible options: %s.%s", universe ? universe->name() : "<NONE>", dv.definition.toString(ds));
							uset->diStart = 0;
							LK_CHK();
						}
						catch (const Poco::Exception& e) {
							std::cout << "Reporting ..." << std::endl;
							Util::reportException(e);
						}

						bool noUpdate = uset->hData->dl()->config().getBool("noUpdate", false);
						if (noUpdate)
							continue;

						usets.push_back(uset);
					}

					if (ds.flushDataset && usets.size()) {
						DR_WRITE_LOCK();
						invalidate(rt, usets);
						usets.clear();
					}
				}

#undef LK_CHK

				if (usets.size()) {
					DR_WRITE_LOCK();
					invalidate(rt, usets);
				}

				{
					DR_WRITE_LOCK();
					DataCacheInfo dci(rt, Dataset(), DataValue(DataDefinition()), DataDefinition(), Constants::s_invalidDay, Constants::s_invalidDay, Constants::s_invalidDay, dl->config(),
						m_secMaster.get(), false, nullptr, secSrcUniverse);
					dl->postDataBuild(*this, dci);
				}

				/// Clear the checkpoint now ....
				if (checkpoint)
					deleteDataCheckpoint(checkpoint);
			}
		}
		catch (const Poco::Exception& e) {
			Util::reportException(e);
			Util::exit(1);
		}
		catch (const std::exception& e) {
			Util::reportException(e);
			Util::exit(1);
		}
	}

	void DataRegistry::dummyDataUpdate(IDataLoaderPtr dl, IUniverse* universe, const RuntimeData& rt, const Dataset& ds, const DataValue& dv, const DataDefinition& def, DataHandle* hData) {
		DR_WRITE_LOCK();

		DataCacheInfo dci(rt, ds, dv, def, Constants::s_invalidDay, Constants::s_invalidDay, Constants::s_invalidDay, dl->config(),
			universe, false, (void*)hData);

		dl->preBuild(*this, dci);
		dl->postBuild(*this, dci);
	}

	bool DataRegistry::doesNeedUpdate(const ConfigSection& config, IUniverse* universe, const RuntimeData& rt, const Dataset& ds, const DataValue& dv, 
		const DataHeader& hdr, IntDate startDate, IntDate endDate, IntDay delay, bool readOnly) {

		/// We don't do anything if its a readOnly cache 
		if (readOnly && !rt.appOptions.updateDataOnly)
			return false;

		/// Delay 0 datasets don't update on the last date

		const auto& def = dv.definition;

		if (def.isUpdateAfterMarketOpen() && !rt.isAfterMarketOpen())
			return false;
		else if (def.isUpdateBeforeMarketClose() && !rt.isBeforeMarketClose())
			return false;
		else if (def.isUpdateDuringTradingHours() && !rt.isInTradingHours())
			return false;

		std::string dataId = dv.toString(ds);
		auto cacheId = getCacheId(universe, dataId);
		bool forceUpdate = config.getBool("alwaysUpdate", false);
		bool noUpdate = config.getBool("noUpdate", false);

		if (noUpdate)
			return false;

		/// If force is set to TRUE then we just run the update regardless (Only during preparation stage of the simulation)
		if ((rt.appOptions.force || forceUpdate) && rt.simState == kSS_Prepare) {
			auto iter = m_updatedThisRun.find(cacheId);
			if (iter == m_updatedThisRun.end()) 
				return true;

			return false;
		}

		if (hdr.updateDate >= startDate && hdr.updateDate >= endDate) {
			IntDay iDataEnd = rt.getDateIndex(hdr.endDate); /// The actual ending date does not really count
			IntDay iLastValidDataEnd = rt.getDateIndex(hdr.lastValidDataDate);

			if (iDataEnd == Constants::s_invalidDay || iLastValidDataEnd == Constants::s_invalidDay) {
				if (endDate < hdr.updateDate)
					return false;
				else if (endDate == hdr.updateDate && rt.isTodayHoliday)
					return false;
				return true;
			}
			/// Have we already updated on the holiday then we don't unless it's a forced update (which is handled above anyway!)
			else if (rt.isTodayHoliday && endDate <= hdr.updateDate)
				return false;

			IntDay iEnd = rt.getDateIndex(endDate);

			if (iEnd == Constants::s_invalidDay)
				iEnd = (IntDay)rt.dates.size();

			int daysSinceUpdate = iEnd - iDataEnd - delay;
			int daysSinceDataUpdate = 0; /// Deprecated: iEnd - iLastValidDataEnd;
			int updateFrequency = std::max((int)hdr.def.frequency, 0);

			if (updateFrequency == 0) /// For handling universal data
				updateFrequency = 1;

			if (daysSinceUpdate < updateFrequency && daysSinceDataUpdate <= (updateFrequency + 1)) {
				std::string name = (universe ? universe->name() : "<NONE>") + "." + dv.toString(ds);
				Trace("DR", "Data has been updated recently: %-35s [Config End Date: %u, Cache End Date: %u, Last Updated: %u@%06u]", name,
					endDate, hdr.endDate, hdr.updateDate, hdr.updateTime);
				return false;
			}
			else {
				Debug("DR", "Data updated on: %u:%06u but the last update entry is: %u, which is different than config end date: %u",
					hdr.updateDate, hdr.updateTime, hdr.endDate, endDate);
				return true;
				//return false;
			}
		}

		return true;
	}

	void DataRegistry::write(const DataCacheInfo& dci, const Data* data) {
//		ASSERT(dci.def == data->def(), "Data definitions do not match up!");
//
//		if (dci.def.frequency == kUniversal) {
//			writeUniversal(dci, data);
//			return;
//		}

		// otherwise we just normally write the data
	}

	IUniversePtr DataRegistry::loadUniverse(const std::string& name, bool noOptimise) {
		auto& modules = const_cast<ConfigSection&>(parentConfig()->modules());
		auto* module = modules.findUnique("name", name);
		const auto& rt = pipelineHandler()->runtimeData();

		if (!module) {
			PWarning("Unable to find universe: %s", name);
			return nullptr;
		}

		//ASSERT(module, "Unable to find universe: " << name);
		ASSERT(module->isUniverse(), "Module is not a universe: " << name);

		ConfigSection* universeConfig = new ConfigSection(*module);
		std::string wrapper;

		if (universeConfig->get("wrapper", wrapper)) {
			universeConfig->set("id", wrapper);
			universeConfig->set("path", universeConfig->get("wrapperPath"));
		}
		else {
			universeConfig->set("id", "MainUniverse");
			universeConfig->set("path", "");
		}

		modules.addChildSection(universeConfig);

		IUniversePtr universe = helper::ShLib::global().createComponent<IUniverse>(*universeConfig, this);
		DataCacheInfo dci(rt, Dataset(), DataValue(DataDefinition()), DataDefinition(), Constants::s_invalidDay, Constants::s_invalidDay, Constants::s_invalidDay, *module,
			m_secMaster ? m_secMaster.get() : nullptr, false, nullptr);
		DataFrame emptyFrame;

		universe->build(*this, dci, emptyFrame);
		m_universes.push_back(universe);

		if (noOptimise == false)
			optimiseDatasets(universe.get(), rt);

		m_dlMap[universe->name()] = universe;

		return universe;
	}

	IUniverse* DataRegistry::getUniverse(const std::string& universeId, bool noOptimise /* = false */) {
		if (universeId.empty() || universeId == "NONE" || universeId == "<NONE>")
			return nullptr;

		DR_READ_WRITE_LOCK();

		auto* dl = getDataLoader(universeId);
		if (dl)
			return dynamic_cast<IUniverse*>(dl);

		if (universeId.find('|') == std::string::npos)
			return loadUniverse(universeId, noOptimise).get();

		/// This is a multi-universe!
		PInfo("Creating merged universe: %s", universeId);
		auto universeIds = Util::split(universeId, "|");

		/// Make sure that the universeIds are sorted ...
		std::sort(universeIds.begin(), universeIds.end());
		std::string universeUuid = Util::combine(universeIds, "|");

		/// Then we try to find the dataloader again ...
		dl = getDataLoader(universeUuid);

		if (dl)
			return dynamic_cast<IUniverse*>(dl);

		std::vector<IUniverse*> universes;

		for (auto uid : universeIds) {
			auto* u = this->getUniverse(uid);
			ASSERT(u, "Unable to load universe: " << uid);
			universes.push_back(u);
		}

		helper::MergedUniversePtr mergedUniverse = nullptr;
		const RuntimeData& rt = pipelineHandler()->runtimeData();

		mergedUniverse = std::make_shared<helper::MergedUniverse>(rt, universes);
		mergedUniverse->dataRegistry() = this;

		m_universes.push_back(mergedUniverse);
		m_dlMap[universeId] = mergedUniverse;

		return mergedUniverse.get();
	}

	IUniverse* DataRegistry::getSecurityMaster() {
		if (m_secMaster)
			return m_secMaster.get();
		return getUniverse("SecurityMaster");
	}

	void DataRegistry::debugDump(const RuntimeData& rt, IUniverse* universe, const DumpInfo& dumpInfo, bool unique) {
		PInfo("Universe: %s", universe->name());

		typedef std::unordered_map<Security::Index, bool> SecMap;
		SecMap prev;
		SecMap total;
		unsigned int numCommon;
		float averageCommon = 0.0f;
		StringVec universeParts = Util::split(dumpInfo.dataId, "@");
		StringVec stockIds;

		if (universeParts.size() >= 2) {
			for (size_t i = 1; i < universeParts.size(); i++)
				stockIds.push_back(universeParts[i]);
		}

		IntDay diStart = 0;
		IntDay diEnd = rt.diEnd;

		if (dumpInfo.startDate != 0)
			diStart = rt.getDateIndex(dumpInfo.startDate);
		if (dumpInfo.endDate != 0)
			diEnd = rt.getDateIndex(dumpInfo.endDate);

		if (universe == getSecurityMaster()) {
			diEnd = rt.diEnd;
			diStart = diEnd - 1;
		}

		ASSERT(diStart != Constants::s_invalidDay && diStart <= rt.diEnd, "Start date outside of the bounds of simulation. Start date: " << dumpInfo.startDate << " [" << rt.dates[0] << ", " << rt.dates[rt.dates.size() - 1] << "]");
		ASSERT(diEnd != Constants::s_invalidDay && diEnd <= rt.diEnd, "Start date outside of the bounds of simulation. End date: " << dumpInfo.endDate << " [" << rt.dates[0] << ", " << rt.dates[rt.dates.size() - 1] << "]");
		ASSERT(diEnd >= diStart, "Cannot have end date less than the start date. Start date: " << dumpInfo.startDate << " - End date: " << dumpInfo.endDate);

		if (!unique) {
			for (IntDay di = diStart; di <= diEnd; di++) {
				IntDate date = rt.dates[di];
				SecMap curr;

				PInfo("Day = %u (%d)", date, di);
				PInfo("\tNum Securities: %z", universe->size(di));

				numCommon = 0;

				size_t size = universe->size(di);
				for (Security::Index ii = 0; ii < (Security::Index)size; ii++) {
					Security sec = universe->security(di, ii);
					PTrace("\t(%d, %u, %u) = %s [%s]", di, date, ii, sec.nameStr(), sec.symbolStr());

					if (prev.find(sec.index) != prev.end())
						numCommon++;

					if (total.find(sec.index) == total.end()) {
						PInfo(sec.debugString());
						total[sec.index] = true;
					}

					if (universe->isValid(di, ii))
						curr[sec.index] = true;
				}

				float commonPercent = 0.0f;

				if (prev.size())
					commonPercent = ((float)numCommon / (float)prev.size()) * 100.0f;
				else 
					commonPercent = 100.0f;

				averageCommon += commonPercent;

				PInfo("\tPrev day overlap: %0.2hf%% (%u)", commonPercent, numCommon);
				PInfo("\tTotal Unique: %z", total.size());

				prev = curr;
				curr.clear();
			}

			PInfo("Average common percent between consecutive days: %0.2hf\n", averageCommon / (float)(diEnd - diStart + 1));
		}
		else {
			std::map<Security::Index, Security> uniqueSecs;

			for (IntDay di = diStart; di <= diEnd; di++) {
				IntDate date = rt.dates[di];
				size_t size = universe->size(di);

				for (Security::Index ii = 0; ii < (Security::Index)size; ii++) {
					Security sec = universe->security(di, ii);

					if (uniqueSecs.find(sec.index) == uniqueSecs.end() && universe->isValid(di, ii) && sec.isValid())
						uniqueSecs[sec.index] = sec;
				}
			}

			for (auto iter = uniqueSecs.begin(); iter != uniqueSecs.end(); iter++) {
				auto& sec = iter->second;
				PInfo("%s, %s, %s, %s", sec.bbTickerStr(), sec.uuidStr(), sec.currencyStr(), sec.countryCode2Str());
			}
		}
	}

	void DataRegistry::debugDump(const RuntimeData& rt_, IUniverse* universe, const Data* data, const DumpInfo& dumpInfo, const StringVec& stockIds) {
		PInfo("Universe: %s", universe ? universe->name() : std::string("<NONE>"));
		PInfo("Data Name: %s", data->name());

		IntDay diStart = 0;
		IntDay diEnd = rt_.diEnd;

		if (dumpInfo.startDate != 0)
			diStart = rt_.getDateIndex(dumpInfo.startDate);
		if (dumpInfo.endDate != 0) {
			diEnd = rt_.getDateIndex(dumpInfo.endDate);
			if (diEnd == Constants::s_invalidDay && rt_.isTodayHoliday)
				diEnd = rt_.diEnd;
		}

		ASSERT(diStart != Constants::s_invalidDay && diStart <= rt_.diEnd, "Start date outside of the bounds of simulation. Start date: " << dumpInfo.startDate << " [" << rt_.dates[0] << ", " << rt_.dates[rt_.dates.size() - 1] << "]");
		ASSERT(diEnd != Constants::s_invalidDay && diEnd <= rt_.diEnd, "Start date outside of the bounds of simulation. End date: " << dumpInfo.endDate << " [" << rt_.dates[0] << ", " << rt_.dates[rt_.dates.size() - 1] << "]");
		ASSERT(diEnd >= diStart, "Cannot have end date less than the start date. Start date: " << dumpInfo.startDate << " - End date: " << dumpInfo.endDate);

		RuntimeData rt = rt_;
		//rt.diStart = diStart;
		//rt.diEnd = diEnd;

		SizeVec secIds;

		if (stockIds.size()) {
			/// print this stock information ...
			size_t numSecurities = universe->size(0);

			for (size_t sid = 0; sid < stockIds.size(); sid++) {
				auto stockId = stockIds[sid];

				const Security* sec = universe->mapUuid(stockId);

				ASSERT(sec && sec->index != Security::s_invalidIndex, "Invalid security: " << stockId);

///				PInfo("Stock ID: %s", stockId);
///				PInfo("Detailed Information: %s", sec->debugString());

				auto ii = universe->alphaIndex(0, sec->index);
				secIds.push_back(ii);
			}
		}

		if (!secIds.size() && stockIds.size()) {
			PInfo("Unable to find any matching stocks: %s", Util::combine(stockIds, ", "));
			return;
		}

		diStart = std::min(diStart, (IntDay)data->rows() - 1);
		diEnd = std::min(diEnd, (IntDay)data->rows() - 1);

		if (universe) {
			for (IntDay di = diStart; di <= diEnd; di++) {
				rt.di = di;
				/// Tick the data
				tick(rt);

				IntDate date = rt.dates[di];

				if (!rt.appOptions.onlyValid && !rt.appOptions.onlyInvalid && !rt.appOptions.ignoreDefault && !secIds.size()) {
					PInfo("Day = %u (%d)", date, di);
					PInfo("\tNum Securities: %z", universe->size(di));
				}

				size_t size = universe->size(di);
				Security::Index limit = (Security::Index)size;

				if (dumpInfo.limit > 0)
					limit = (Security::Index)dumpInfo.limit;

				if (data->cols() < limit)
					limit = (Security::Index)data->cols();

				if (!secIds.size()) {
					for (Security::Index ii = 0; ii < limit; ii++) {
						Security sec = universe->security(di, (Security::Index)ii);
                        size_t depth = data->depthAt(di, ii);

                        for (size_t kk = 0; kk < depth; kk++) {
                            std::string value = data->getString(di, ii, kk);

                            if (!rt.appOptions.ignoreDefault || !Util::isInVector(rt.appOptions.defaultValues, value)) {
                                if ((rt.appOptions.onlyValid && data->isValid(di, ii, kk)) ||
                                    (rt.appOptions.onlyInvalid && !data->isValid(di, ii, kk)) ||
                                    (!rt.appOptions.onlyValid && !rt.appOptions.onlyInvalid)) {
                                    std::string isValid = universe->isValid(di, (Security::Index)ii) ? "V" : "I";
                                    isValid += std::string("-") + std::string(sec.isHolidayInExchange(rt, di) ? "H" : "W");
                                    PInfo("\t(%d, %u, %u, %s [%s], %s [%s, %s, %s]) = %s", di, date, ii, sec.tickerStr(), sec.instrumentId(), sec.uuidStr(), sec.exchangeStr(), sec.currencyStr(),
                                          isValid, value);
                                }
                            }
                        }
					}
				}
				else {
					for (size_t sid = 0; sid < secIds.size(); sid++) {
						auto secId = secIds[sid];
						auto sec = universe->security(di, (Security::Index)secId);
                        size_t depth = data->depthAt(di, secId);

                        for (size_t kk = 0; kk < depth; kk++) {
                            std::string value = data->getString(di, secId, kk);

                            if (!rt.appOptions.ignoreDefault || !Util::isInVector(rt.appOptions.defaultValues, value)) {
                                if ((rt.appOptions.onlyValid && data->isValid(di, secId, kk)) ||
                                    (rt.appOptions.onlyInvalid && !data->isValid(di, secId, kk)) ||
                                    (!rt.appOptions.onlyValid && !rt.appOptions.onlyInvalid)) {
                                    std::string isValid = universe->isValid(di, (Security::Index)secId) ? "V" : "I";
                                    isValid += std::string("-") + std::string(sec.isHolidayInExchange(rt, di) ? "H" : "W");
                                    //float value = (*data)(di, secIds[sid]);
                                    PInfo("\t[%d, %u, %s, %s, %s] = %s", di, date, sec.tickerStr(), isValid, sec.currencyStr(), value);
                                }
                            }
                        }

					}
				}
			}
		}
		else {
			for (IntDay di = diStart; di <= diEnd; di++) {
				IntDate date = rt.dates[di];
				std::string value = data->getString(di, 0, 0);

				if (!rt.appOptions.ignoreDefault || !Util::isInVector(rt.appOptions.defaultValues, value)) {
					if ((rt.appOptions.onlyValid && data->isValid(di, 0, 0)) ||
						(rt.appOptions.onlyInvalid && !data->isValid(di, 0, 0)) ||
						(!rt.appOptions.onlyValid && !rt.appOptions.onlyInvalid)) {
						PInfo("Day = %u (%d) - %s", date, di, value);
					}
				}
			}
		}

        if (universe && secIds.size()) {
            for (size_t sid = 0; sid < secIds.size(); sid++) {
                auto stockId = stockIds[sid];
                auto sec = universe->security(0, (Security::Index)secIds[sid]);

                PInfo("\nStock ID: %s\nDetailedInformation: %s", stockIds[sid], sec.debugString());
//                PInfo("Detailed Information: %s", sec.debugString());
            }
        }
    }

	void DataRegistry::debugDump(const RuntimeData& rt_, const std::string& refId, const StringData* refData, const Data* data, const DumpInfo& dumpInfo, const StringVec& stockIds) {
		PInfo("Ref ID: %s", refId);
		PInfo("Data Name: %s", data->name());

		IntDay diStart = 0;
		IntDay diEnd = rt_.diEnd;

		if (dumpInfo.startDate != 0)
			diStart = rt_.getDateIndex(dumpInfo.startDate);
		if (dumpInfo.endDate != 0)
			diEnd = rt_.getDateIndex(dumpInfo.endDate);

		ASSERT(diStart != Constants::s_invalidDay && diStart <= rt_.diEnd, "Start date outside of the bounds of simulation. Start date: " << dumpInfo.startDate << " [" << rt_.dates[0] << ", " << rt_.dates[rt_.dates.size() - 1] << "]");
		ASSERT(diEnd != Constants::s_invalidDay && diEnd <= rt_.diEnd, "Start date outside of the bounds of simulation. End date: " << dumpInfo.endDate << " [" << rt_.dates[0] << ", " << rt_.dates[rt_.dates.size() - 1] << "]");
		ASSERT(diEnd >= diStart, "Cannot have end date less than the start date. Start date: " << dumpInfo.startDate << " - End date: " << dumpInfo.endDate);

		RuntimeData rt = rt_;

		if (stockIds.size()) {
			/// print this stock information ...
			size_t numSecurities = refData->cols();
			std::unordered_map<std::string, size_t> idMap;

			for (size_t ii = 0; ii < numSecurities; ii++) {
				auto id = refData->getValue(0, ii);
				idMap[id] = ii;
			}

			for (size_t sid = 0; sid < stockIds.size(); sid++) {
				auto stockId = stockIds[sid];

				auto iter = idMap.find(stockId);

				ASSERT(iter != idMap.end(), "[Ref] Unable to find security: " << stockId);

				auto ii = iter->second;

				PInfo("Mapped: %s => %z", stockId, ii);

				if (data->def().isStatic() || data->rows() == 1) {
					if ((rt.appOptions.onlyValid && data->isValid(0, ii, 0)) ||
						(rt.appOptions.onlyInvalid && !data->isValid(0, ii, 0)) ||
						(!rt.appOptions.onlyValid && !rt.appOptions.onlyInvalid)) {

						std::string value = data->getString(0, ii, 0);
						if (!rt.appOptions.ignoreDefault || !Util::isInVector(rt.appOptions.defaultValues, value)) {
							PInfo("%s = %s", stockId, value);
						}
					}
				}
				else {
					diEnd = std::min(diEnd, (IntDay)data->rows() - 1);

					for (IntDay di = diStart; di <= diEnd; di++) {
						IntDate date = rt.dates[di];
						size_t depth = data->depthAt(di, ii);

						for (size_t kk = 0; kk < depth; kk++) {
                            std::string value = data->getString(di, ii, kk);

                            if ((rt.appOptions.onlyValid && data->isValid(di, ii, kk)) ||
                                (rt.appOptions.onlyInvalid && !data->isValid(di, ii, kk)) ||
                                (!rt.appOptions.onlyValid && !rt.appOptions.onlyInvalid)) {
                                if (!rt.appOptions.ignoreDefault || !Util::isInVector(rt.appOptions.defaultValues, value)) {
                                    PInfo("[%u] %s = %s", date, stockId, value);
                                }
                            }
                        }
					}
				}
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////
	/// Data debugging andd diagnostics below this line
	//////////////////////////////////////////////////////////////////////////
	template <typename T>
	struct BadDataPoint {
		IntDate				date;
		T					prevValue;
		T					value;
	};

	template <typename T>
	struct DiagnosticsInfo {
		Security			sec;
		std::vector<BadDataPoint<T> > badPoints;
		bool				nanEncountered = false;
	};

	template <class T>
	void runDiagnosticsOnData(DataRegistry& dr, IUniverse* universe, const MatrixData<T>* data, IntDay diStart, IntDay diEnd, T tolerance,
		const RuntimeData& rt_, const DumpInfo& dumpInfo, T minValue, T maxValue) {
		Info("DR", "Running Diagnostics: %s", dumpInfo.dataId);

		RuntimeData rt = rt_;
		size_t numRows = data->rows();
		diStart = std::max(diStart, 1);
		diEnd	= std::min(diEnd, (IntDay)numRows - 1);

		IntDate endDate = rt.getEndDate();

		std::map<std::string, DiagnosticsInfo<T> > result;

		//(const_cast<MatrixData<T>*>(data))->applyDelay() = true;

		/// We don't wanna start from the first day since we'll have no previous
		/// day's data to gauge the current data against ...
		for (IntDay di = std::max(diStart, 1); di <= diEnd; di++) {
			rt.di = di + data->delay();
			dr.tick(rt);
			rt.di = di;

			size_t size = universe->size(di);
			IntDay diD = di - 1;

			for (size_t ii = 0; ii < size; ii++) {
				T diValue = (*data)(di, ii);
				T diDValue = (*data)(di - 1, ii);
				auto sec = universe->security(di, (Security::Index)ii);
				bool wasInUniverse = result.find(sec.uuid) != result.end();
				auto& dinfo = result[sec.uuid];

				if (dinfo.sec.index == Security::s_invalidIndex)
					dinfo.sec = sec;

				/// If any of the values are invalid
				//if (std::isnan(diDValue) && !std::isnan(diValue)) {
				//	/// This looks like an error where the previous value was a NaN
				//	/// while the current value isn't (only if this was already in the universe)
				//	if (wasInUniverse) {
				//		dinfo.badPoints.push_back({
				//			rt.dates[di], diDValue, diValue
				//		});
				//	}
				//}
				if (std::isnan(diDValue) && !std::isnan(diValue)) {
					/// If both values are NaN then that means that this security is probably
					/// not valid anymore (we wait when we encounter a good value in the if above)
					continue;
				}
				else {
					if (!std::isnan(minValue) && diValue < minValue || diDValue < minValue)
						continue;

					if (!std::isnan(maxValue) && diValue > maxValue || diDValue > maxValue)
						continue;

					T delta = std::abs(diDValue - diValue);
					if (delta / diDValue >= tolerance) {
						dinfo.badPoints.push_back({
							rt.dates[di], diDValue, diValue
						});
					}
				}
			}
		}

		for (auto iter = result.begin(); iter != result.end(); iter++) {
			auto& dinfo = iter->second;
			if (!dinfo.badPoints.size())
				continue;

			std::ostringstream ss;
			ss << "Security: " << dinfo.sec.uuid << " [" << dinfo.sec.ticker << ", " << dinfo.sec.exchange << "] - Error Count: " << dinfo.badPoints.size();
			for (auto& pt : dinfo.badPoints)
				ss << "\n\t" << pt.date << " - " << pt.prevValue << " => " << pt.value;
			Info("DR", "%s", ss.str());
		}
	}

	void DataRegistry::runDiagnostics(const RuntimeData& rt, const DumpInfo& dumpInfo) {
		std::string dumpId = dumpInfo.dataId;
		StringVec parts = Util::split(dumpId, "@");
		std::string minValue, maxValue;
		std::string tolerance;

		if (parts.size() > 1) {
			dumpId = parts[0];
			tolerance = parts[1];
		}

		parts = Util::split(dumpId, ">");

		if (parts.size() > 1) {
			dumpId = parts[0];
			maxValue = parts[1];
		}

		parts = Util::split(dumpId, "<");
		if (parts.size() > 1) {
			dumpId = parts[1];
			minValue = parts[0];
		}

		parts = Util::split(dumpId, ".");
		IUniverse* universe = getSecurityMaster();

		if (parts.size() >= 2) {
			std::string universeId = parts[0];
			universe = getUniverse(universeId);
			ASSERT(universe, "Unable to load universe: " << parts[0]);
			dumpId = Util::combine(parts, ".", 1, parts.size());
		}

		std::map<IntDate, size_t> seekMap;
		DataHeader hdr = getDataHdr(universe, dumpId, &seekMap);
		PInfo("Header: %s", hdr.debugString());

		if (rt.appOptions.headerOnly) {
			/// Dump the seekmap
			for (auto iter = seekMap.begin(); iter != seekMap.end(); iter++) {
				PInfo("%u - %z", iter->first, iter->second);
			}

			return;
		}

		IntDay diStart = 0;
		IntDay diEnd = rt.diEnd - 1;

		if (dumpInfo.startDate != 0)
			diStart = rt.getDateIndex(dumpInfo.startDate);
		if (dumpInfo.endDate != 0)
			diEnd = rt.getDateIndex(dumpInfo.endDate);

		DataPtr data = getDataPtr(universe, dumpId, nullptr);

		ASSERT(data, "Unable to load data: " << dumpId);

		const DataDefinition& def = data->def();

		switch (def.itemType) {
		case DataType::kFloat:
		{
			float fminValue = 1.0f, fmaxValue = std::nanf("");
			float ftolerance = 0.5f;

			if (!minValue.empty())
				fminValue = Util::amount(minValue);

			if (!maxValue.empty())
				fmaxValue = Util::amount(maxValue);

			if (!tolerance.empty())
				ftolerance = Util::cast<float>(tolerance);

			runDiagnosticsOnData(*this, universe, dynamic_cast<MatrixData<float>*>(data.get()), diStart, diEnd, ftolerance, rt, dumpInfo, fminValue, fmaxValue);
			break;
		}
		default:
			PInfo("Don't handle data type: %d", (int)def.itemType);
			break;
		}
	}

	void DataRegistry::rawDump(const RuntimeData& rt, const DumpInfo& dumpInfo) {
		Dataset ds;
		DataValue dv("test", DataType::kFloat, 4, 1, 0);
		DataHandle hdata(nullptr, *this, nullptr, ds, dv, "", "");

		hdata.rawDump(rt, dumpInfo);
	}

	void DataRegistry::dumpCsv(const RuntimeData& rt_, const DumpInfo& dumpInfo) {
		IntDay diStart = 0;
		IntDay diEnd = rt_.diEnd;

		if (dumpInfo.startDate != 0)
			diStart = rt_.getDateIndex(dumpInfo.startDate);
		if (dumpInfo.endDate != 0) {
			diEnd = rt_.getDateIndex(dumpInfo.endDate);
			if (diEnd == Constants::s_invalidDay && rt_.isTodayHoliday)
				diEnd = rt_.diEnd;
		}

		diStart = std::max(diStart, 1);

		ASSERT(diStart != Constants::s_invalidDay && diStart <= rt_.diEnd, "Start date outside of the bounds of simulation. Start date: " << dumpInfo.startDate << " [" << rt_.dates[0] << ", " << rt_.dates[rt_.dates.size() - 1] << "]");
		ASSERT(diEnd != Constants::s_invalidDay && diEnd <= rt_.diEnd, "Start date outside of the bounds of simulation. End date: " << dumpInfo.endDate << " [" << rt_.dates[0] << ", " << rt_.dates[rt_.dates.size() - 1] << "]");
		ASSERT(diEnd >= diStart, "Cannot have end date less than the start date. Start date: " << dumpInfo.startDate << " - End date: " << dumpInfo.endDate);

		RuntimeData rt = rt_;

		const std::string& dumpId = dumpInfo.dataId;
		StringVec parts = Util::split(dumpId, "@");

		ASSERT(parts.size() == 3, "Invalid dump csv command. Should be of the format <UNIVERSE>@data-1,data-2...@security-1,security-2...");

		std::string universeId	= parts[0].substr(1);
		StringVec dataIds		= Util::split(parts[1], ",");
		std::vector<const Data*> data(dataIds.size());
		StringVec stockIds		= Util::split(parts[2], ",");
		IUniverse* universe		= universeId.empty() ? m_secMaster.get() : getUniverse(universeId);
		std::string header		= "Date, di, " + parts[1];

		PInfo("\tNum Securities: %z [Universe: %s]", universe->size(0), universeId);

		for (size_t i = 0; i < dataIds.size(); i++) {
			data[i] = getData(universe, dataIds[i], true); 
			size_t numRows = data[i]->rows();
			diEnd = std::min(diEnd, (IntDay)numRows - 1);
		}

		SizeVec secIds;
		for (size_t sid = 0; sid < stockIds.size(); sid++) {
			auto stockId = stockIds[sid];

			const Security* sec = universe->mapUuid(stockId);

			ASSERT(sec && sec->index != Security::s_invalidIndex, "Invalid security: " << stockId);

			PInfo("Stock ID: %s", stockId);
			PInfo("Detailed Information: %s", sec->debugString());

			auto ii = universe->alphaIndex(0, sec->index);
			secIds.push_back(ii);
		}

		PInfo("============ BEGIN CSV DUMP ============");
		Poco::Logger::root().information(header);

		for (IntDay di = diStart; di <= diEnd; di++) {
			rt.di = di;

			/// Tick the data
			tick(rt);

			IntDate date = rt.dates[di];
			std::string row = Poco::format("%u, %d", date, di);

			for (size_t i = 0; i < data.size(); i++) {
				for (size_t sid = 0; sid < secIds.size(); sid++) {
					auto secId = secIds[sid];
					std::string value = data[i]->getString(di, secId, 0);
					row += ", " + value;
				}
			}

			Poco::Logger::root().information(row);
		}

		PInfo("============ END CSV DUMP ============");
	}

	void DataRegistry::debugDump(const RuntimeData& rt, const DumpInfo& dumpInfo) {
		if (dumpInfo.runDiagnostics) {
			runDiagnostics(rt, dumpInfo);
			return;
		}
		else if (dumpInfo.dataId[0] == '!') {
			rawDump(rt, dumpInfo);
			return;
		}
		else if (dumpInfo.dataId[0] == '^') {
			dumpCsv(rt, dumpInfo);
			return;
		}

		auto dumpParts = Util::split(dumpInfo.dataId, "@");
		auto dumpId = dumpParts[0];
		auto stockIdsList = dumpParts.size() > 1 ? dumpParts[1] : "";
//		const std::string& dumpId = dumpInfo.dataId;
		auto parts = Util::split(dumpId, ".");

		if (parts.size() == 1) {
			std::string universeId = parts[0];
			StringVec universeParts = Util::split(parts[0], "@");
			bool unique = false;

			if (universeParts.size() > 1)
				universeId = universeParts[0];
			else if (universeId[0] == '~') {
				universeId = universeId.substr(1);
				unique = true;
			}

			PInfo("Dumping universe: %s", universeId);
			IUniverse* universe = getUniverse(universeId);
			ASSERT(universe, "Unable to find universe: " << universeId);

			debugDump(rt, universe, dumpInfo, unique);
		}
		else if (parts.size() >= 2 && parts.size() <= 3) {
			std::string universeId = parts[0];
			IUniverse* universe = universeId.empty() ? m_secMaster.get() : getUniverse(universeId);
			//ASSERT(universe, "Unable to find universe: " << universeId);

			std::string dataId;
			if (universe || !universeId.empty())
				dataId = Util::combine(parts, ".", 1, parts.size());
			else {
				PInfo("No universe found: %s, taking it as a NULL universe data!", universeId);
				dataId = Util::combine(parts, ".");
			}

///			parts = Util::split(dataId, "@");
///			dataId = parts[0];
			StringVec stockIds = Util::split(stockIdsList, ",");

///			for (size_t i = 1; i < parts.size(); i++)
///				stockIds.push_back(parts[i]);

			//if (stockIds.size())
			//	universe = getSecurityMaster();

			const Data* data = getData(universe, dataId);
			ASSERT(data, "Unable to load data: " << dataId);

			debugDump(rt, universe, data, dumpInfo, stockIds);
		}
		else { 
			std::string lhs = dumpId;
			std::string rhs = stockIdsList;

			parts = Util::split(lhs, ".");

			if (parts.size() == 4 && !rhs.empty()) {
				std::string refId = Util::combine(parts, ".", 0, 2);
				std::string dataId = Util::combine(parts, ".", 2, parts.size());

				//parts = Util::split(dataId, "@");
				//dataId = parts[0];

				StringVec stockIds = Util::split(rhs, ",");

				//for (size_t i = 1; i < parts.size(); i++)
				//	stockIds.push_back(parts[i]);

				const StringData* refData = stringData(nullptr, refId);

				const Data* data = getData(nullptr, dataId);
				ASSERT(data, "Unable to load data: " << dataId);

				debugDump(rt, refId, refData, data, dumpInfo, stockIds);
			}
		}
	}


	#undef DR_READ_LOCK
	#undef DR_WRITE_LOCK
	#undef DR_READ_WRITE_LOCK
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IPipelineComponent* createDataRegistry(const pesa::ConfigSection& config) {
#ifndef __WINDOWS__
	pesa::Application* app = pesa::Application::instance();
	pesa::AppOptions options = app->appOptions();

	/// If there is an option specified to run the DataRegistry in server mode, then we first try that ...
	if (options.runServer) {
		return new pesa::net::DataRegistry_Server((const pesa::ConfigSection&)config);
	}

	try {
		/// Really short timeout
		int port = config.getInt("port", pesa::net::s_dataRegistryPort);
		const long connTimeout = 1;
		Poco::Net::SocketAddress sa("localhost", port);
		Poco::Net::StreamSocket ss;

		/// Connect to the socket ...
		ss.connect(sa, Poco::Timespan(connTimeout, 0));
		ss.close();

        pesa::Info("DR", "Trying to create DataRegistry Client!");

		/// If we manage to connect then we can use the DataRegistry_Client; otherwise we'll just use the normal DataRegistry
		/// Otherwise, we try to run it as a client! The client falls back to the normal DataRegistry if 
		/// it cannot connect to the server!
		return new pesa::net::DataRegistry_Client((const pesa::ConfigSection&)config);
	}
    catch (const Poco::IllegalStateException& e) {
        pesa::Error("DR", "Socket IllegalStateException: %s", e.displayText());
    }
    catch (const Poco::InvalidAccessException& e) {
        pesa::Error("DR", "Socket InvalidAccessException: %s", e.displayText());
    }
    catch (const std::exception& e) {
        pesa::Error("DR", "Socket StdException: %s", std::string(e.what()));
    }
    catch (...) {
		/// NO OP: just create the normal DataRegistry
	}

    pesa::Info("DR", "Creating default DataRegistry");
#endif

	return new pesa::DataRegistry(config);
}
