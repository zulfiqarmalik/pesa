/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// DataRegistry.h
///
/// Created on: 28 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/Framework.h"
#include "DataHeader.h"

namespace pesa {
	class DataHandle;
	typedef std::shared_ptr<DataHandle> DataHandlePtr;

	////////////////////////////////////////////////////////////////////////////
	/// Data registry implementation
	////////////////////////////////////////////////////////////////////////////
	class DataRegistry : public IDataRegistry {
	protected:
		static const int		s_numFramesBeforeFlush = 50;
		static const int		s_maxSimultaneousUpdates = 4000;
		//////////////////////////////////////////////////////////////////////////
		struct CacheItem {
			DataHeader			hdr;
			DataFooter			ftr;
			DataPtr				data = nullptr;
			IntDay				lastUse = Constants::s_invalidDay;
			bool				isTemporary = false;

								CacheItem() {}
								CacheItem(const DataHeader& hdr_, DataPtr data_, IntDay lastUse_) : hdr(hdr_), data(data_), lastUse(lastUse_) {}
								CacheItem(DataPtr data_) : data(data_), isTemporary(true) {}
								~CacheItem() {
								}
			void				erase() { data = nullptr; }
		};

		//////////////////////////////////////////////////////////////////////////
		struct Checkpoint {
			StringVec			dataIds;					/// Data loaded after the checkpoint
		};

		typedef std::shared_ptr<Checkpoint> CheckpointPtr;
		typedef std::vector<CheckpointPtr> CheckpointPtrVec;
		typedef std::list<CheckpointPtr> CheckpointPtrList;
		typedef std::map<std::string, std::string> StrStr_Map;

		//////////////////////////////////////////////////////////////////////////
		struct WindowData {
			DataHandlePtr		hData;						/// The data handle
			DataPtr				data;						/// The data that is loaded as a window
		};

		typedef std::vector<WindowData> WindowDataVec;

		//////////////////////////////////////////////////////////////////////////
		struct UpdateSet {
			IUniverse*			universe = nullptr;			/// The universe that we need to update ...
			IUniverse*			secSrcUniverse = nullptr;	/// The universe that is used for getting the source securities to query the data for
			DataHandle*			hData = nullptr;			/// The data handle
			DataHeader 			existingHdr; 				/// The existing header at the start of the process
			DataFooter			existingFtr;				/// The existing footer at the start of the process
			IntDay				orgDiStart = 0;				/// The original diStart. This can be different for datasets that update in one go
			IntDay				diStart = 0;				/// The starting index that we're going to update 
			IntDay				diEnd = 0;					/// The ending day of this dataset
			IntDay				di = 0;						/// The current day that we're updating. Note that the Update set is an iterative process
			IntDay				diLast = 0;					/// The last date that was successfully constructed (valid only for daily data)
			size_t 				historyOverlap = 0;			/// How much did we overlap with the history while updating 
			size_t 				numNewRows = 0; 			/// Number of new rows that were added to this data ...
			size_t 				numRows = 0; 				/// Total number of the rows in the now updated dataset
			size_t				minRowSize = 0;				/// The minimum row size
			size_t 				maxRowSize = 0; 			/// The max row size
			IntDay				delay = 1;					/// What is the delay of this dataset
			int					minDaysToUpdate = 0;		/// Minimum number of days to update ...

			IntDate				minDate = 0;				/// The minimum date when to start updating this data 
			IntDate				maxDate = 0;				/// The maximum date when to start updating this data

			bool				useLastDayData = false;		/// Whether to use last day's data in the event some data is not available for some day
			bool				useLastDayDataOnHoliday = false; /// Use last day's data in the event data is not available and its a holiday in the exchange of the source security
			bool				useLastDayDataFrameIfEmpty = false; /// Whether to use last day's data in the event some data is not available for some day
			bool				useNullDataFrameIfEmpty	= false; /// Use a NULL data frame equal in size to the last data frame if no data is given by the IDataLoader
			bool				loadLastDayData = false;	/// Whether to use last day's data in the event some data is not available for some day
			bool				isOptimising = false;		/// Are we optimising or not

			bool				hDataOpen = false;			/// Is hData valid or not
			IntDay				diLastDataFlush = -1;		/// The last time the data was flushed

			DataFrame			lastGoodFrame;				/// The last good frame that we received
			DataFrame			emptyFrame;					/// Empty frame, this is when data has a minDate or maxDate specified in the config

			int					numFramesBeforeFlush = 0;	/// How many frames to write before flusing the data 

			bool				collectAllFramesBeforeWriting = false; /// Should we collect all the frames before writing them to disk or just write them one at a time

			DataPtr				srcData = nullptr;			/// The source data that we need to use (This is normally used for optimising a dataset for a universe)
			size_t				srcDataIndex = 0;			/// The index into the source from where the update must start ...

			IPipelineComponentPtr downloader = nullptr;		/// The downloader that was used to start data downloads (if any)

								~UpdateSet();
		};

		typedef std::shared_ptr<UpdateSet> 	UpdateSetPtr;
		typedef std::vector<UpdateSetPtr> 	UpdateSetPtrVec;

		//////////////////////////////////////////////////////////////////////////
		struct OptimalDataset {
			IDataLoaderPtr		dl;
			Datasets			dss;
			StringVec			optimiseTargets;	/// Target universes for the optimisation
								OptimalDataset() {}
								OptimalDataset(IDataLoaderPtr dl_, const Datasets& dss_) : dl(dl_), dss(dss_) {}
		};
		
		typedef std::vector<OptimalDataset> LazyDatasetVec;
		typedef std::unordered_map<std::string, CacheItem> DataCacheMap;
		typedef std::unordered_map<std::string, DataDefinition> DataDefinitionMap;
		typedef std::map<std::string, IDataLoaderPtr> IDataLoaderPtrMap;
		typedef std::vector<IUniversePtr> IUniversePtrVec;
		typedef std::vector<IUniverse*> IUniversePVec;
		typedef std::vector<DataPtr> DataPtrVec;
		typedef std::unordered_map<std::string, bool> BoolMap;
		typedef std::future<int> Future;
		typedef std::list<Future> FutureList;

		static const size_t		s_maxThreads;
		static const char* 		s_secMasterName;
		static const char*		s_timestamp;
		static const char*		s_optTimestamp;
		static const char*		s_preTimestamp; 
		static const int		s_waitForStart;

		DataPtrVec				m_tickDataCache; 	/// Temporary data cache that is created during each tick interval and cleared in the next one!
		DataCacheMap			m_dataCache;		/// Data cache ... This will get more sophisticated as we go along
		DataDefinitionMap		m_dataDefMap;		/// Data definition map
		IDataLoaderPtrMap		m_dlMap;			/// Data loaders for each of the datasets

		IUniversePtr 			m_secMaster; 		/// Security master
		IUniversePtrVec 		m_universes; 		/// Universe wrappers that we've loaded!

		std::recursive_mutex 	m_dataReadMutex;	/// Data read mutex
		std::recursive_mutex 	m_dataWriteMutex;	/// Data read mutex
		LazyDatasetVec			m_dsOptimal;		/// Datasets to optimise for universes after all the caching has been done

		BoolMap					m_updatedThisRun;	/// Datasets that have been updated in this run

		FutureList				m_asyncFutures;		/// Async cache futures
		bool					m_readOnly = false;	/// Is the cache readonly or not ...
		WindowDataVec			m_windowData;		/// Windowed data load ...
		CheckpointPtrList		m_checkpoints;		/// The checkpoints that have been created

		StrStr_Map				m_aliases;			/// The aliases for individual data

		static void 			parseDataName(const std::string& fullName, std::string& dsName, std::string& defName);

		Poco::Path 				getDataPath(IUniverse* universe, const ConfigSection& config, const Dataset& ds, const DataDefinition& def) const;
		StringVec				regexDataLoaders(const std::string& search);

		static void				mergeFrames(IUniverse* secMaster, const RuntimeData& rt, IntDay di, DataFrame& currFrame, DataFrame& lastFrame, bool mergeOnHoliday);
		static void				getNextFrame(IUniverse* secMaster, const RuntimeData& rt, const DataCacheInfo& dci, UpdateSet& uset, DataFrame& frame);
		static bool				getNextFrameData(IUniverse* secMaster, const RuntimeData& rt, const DataCacheInfo& dci, const UpdateSet& uset, DataFrame& frame);
		static void				updateFrameDataState(IUniverse* secMaster, const RuntimeData& rt, const DataCacheInfo& dci, UpdateSet& uset, DataFrame& frame);

		void 					prepareUpdateSets(const RuntimeData& rt, UpdateSetPtrVec& usets);
		void 					invalidate(const RuntimeData& rt, UpdateSetPtrVec& usets);
		void 					invalidateRange(const RuntimeData& rt, UpdateSetPtrVec& usets, size_t start, size_t end);
		void 					finaliseUpdateSets(const RuntimeData& rt, UpdateSetPtrVec& usets);

		IUniversePtr			loadUniverse(const std::string& name, bool noOptimise);

		pesa::Dataset			prepareForLoad(const std::string& id);

		void					defineAlias(const ConfigSection* module);

		void 					cacheUniverse(const ConfigSection& module);
		void 					cacheUniverses();

		int						optimiseAsync(const RuntimeData& rt, UpdateSetPtr uset, DataPtr srcData);
		void					optimiseDatasets(const RuntimeData& rt);
		void					optimiseDatasets(IUniverse* universe, const RuntimeData& rt);
		void					optimiseDatasets(IUniversePVec& universes, const RuntimeData& rt);
		DataPtr					optimiseDataForCrossUniverse(DataPtr data, IntDay diStart, const std::string& id, IUniverse* srcUniverse, IUniverse* dstUniverse, bool cacheData = true);

		std::string				getBaseDataCachePath() const;

		void					waitForTimestamp(const char* name, int intervalMilliseconds = 100);
		bool					getTimestamp(const char* name, Poco::Timestamp& ts);
		bool					removeTimestamp(const char* name);
		bool					createTimestamp(const char* name);

		std::string				fixDataId(const std::string& id); 

		IPipelineComponentPtr	prepareDownload(const RuntimeData& rt, ConfigSection* downloadSec, IntDay dciStart, IntDay dciEnd);
		void					finaliseDownload(const RuntimeData& rt, IPipelineComponentPtr downloader, ConfigSection* downloadSec);

		DataPtr					getSecurityMasterDataPtr(IUniverse* universe, const std::string& dataId, DataInfo& dinfo,
								const DataHeader& hdr, IDataLoaderPtr dl, const std::string& cacheId, DataValue& dv);
		void					dummyDataUpdate(IDataLoaderPtr dl, IUniverse* universe, const RuntimeData& rt, const Dataset& ds, const DataValue& dv, const DataDefinition& def, DataHandle* hData);

		static DataPtr			getMatrixWindow(IUniverse* universe, IDataHandlePtr hData, const DataDefinition& def, size_t backdays, size_t maxRows);

		static inline std::string getCacheId(IUniverse* universe, const std::string& id) {
			if (universe) {
				std::string cacheId = id;
				std::string uname = universe->name();

				if (!uname.empty() && cacheId.find(uname) != 0)
					cacheId = uname + "." + cacheId;

				return cacheId;
			}

			return id;
		}

	public:
								DataRegistry(const ConfigSection& config, std::string logChannel = "");
		virtual 				~DataRegistry();

		static DataPtr			createDataBuffer(IUniverse* universe, const DataDefinition& def, DataPtr metadata);
		virtual DataPtr			createDataBuffer(IUniverse* universe, const DataDefinition& def);
		DataPtr					createCustomData(IUniverse* universe, const std::string& dsName, const DataDefinition& def, DataPtr metadata);
		virtual DataPtr			createDataBuffer(IUniverse* universe, IDataLoaderPtr dl, const Dataset& ds, const DataDefinition& def, DataPtr metadata);
		virtual DataPtr			createDataBuffer(IUniverse* universe, IDataLoaderPtr dl, const Dataset& ds, const DataDefinition& def);

		////////////////////////////////////////////////////////////////////////////
		/// IComponent overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			prepare(RuntimeData& rtd);
		virtual void 			tick(const RuntimeData& rtd);
		virtual void 			finalise(RuntimeData& rt);

		////////////////////////////////////////////////////////////////////////////
		/// IDataRegistry overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			runForData(const RuntimeData& rt, IDataLoaderPtr dl);
		virtual void 			cacheDataValues(const Security& security, const Dataset& ds, const DataDefinition& dv,
								IntDay di, IntDate date, void* data, size_t numItems) {}
		virtual DataPtr			getDataPtr(IUniverse* universe, const std::string& dataId, DataInfo* dinfo);
		virtual DataPtrVec		getAllDataPtr(IUniverse* universe, const std::string& dataId, DataInfo* dinfo);
		virtual std::vector<pesa::IUniverse*> getActiveUniverses();
		DataHeader				getDataHdr(IUniverse* universe, const std::string& name, std::map<IntDate, size_t>* seekMap, Metadata* metadata = nullptr);
		virtual DataPtr			setData(const std::string& dataId, Data* data); 
		DataPtr					cacheData(const std::string& dataId, CacheItem cacheItem);
		virtual IDataLoader* 	getDataLoader(const std::string& dataId);
		virtual pesa::Metadata 	getMetadata(IUniverse* universe, const std::string& dataId);
		virtual void 			write(const DataCacheInfo& dci, const Data* data);
		virtual void 			clearData(const std::string& dataId);
		virtual void*			createDataCheckpoint();
		virtual void			deleteDataCheckpoint(void* checkpoint);

		virtual IUniverse* 		getSecurityMaster();
		virtual IUniverse* 		getUniverse(const std::string& id, bool noOptimise = false);

		void 					debugDump(const RuntimeData& rt, const DumpInfo& dumpInfo);
		void					debugDump(const RuntimeData& rt, IUniverse* universe, const DumpInfo& dumpInfo, bool unique = false);
		void					debugDump(const RuntimeData& rt, IUniverse* universe, const Data* data, const DumpInfo& dumpInfo, const StringVec& stockIds);
		void					debugDump(const RuntimeData& rt, const std::string& refId, const StringData* refData, const Data* data, const DumpInfo& dumpInfo, const StringVec& stockIds);
		void					dumpCsv(const RuntimeData& rt, const DumpInfo& dumpInfo);

		void					runDiagnostics(const RuntimeData& rt, const DumpInfo& dumpInfo);
		void					rawDump(const RuntimeData& rt, const DumpInfo& dumpInfo);
		void					tickWindowData(const RuntimeData& rt);

		IntDay					getDelay(DataHandle& hData);

		//////////////////////////////////////////////////////////////////////////
		/// Static Functions
		//////////////////////////////////////////////////////////////////////////
		bool					doesNeedUpdate(const ConfigSection& config, IUniverse* universe, const RuntimeData& rt, const Dataset& ds, const DataValue& dv, 
								const DataHeader& hdr, IntDate startDate, IntDate endDate, IntDay delay, bool readOnly);

	};

} /// namespace pesa 

