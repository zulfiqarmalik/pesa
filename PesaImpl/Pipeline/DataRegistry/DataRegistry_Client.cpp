/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// DataRegistry_Client.h
///
/// Created on: 03 Oct 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "DataRegistry_Client.h"

namespace pesa {
namespace net {
	DataRegistry_Client::DataRegistry_Client(const ConfigSection& config) : DataRegistry_Net(config, "DR_CL") {
	}

	DataRegistry_Client::~DataRegistry_Client() {
	}

	DataPtr DataRegistry_Client::getDataPtr(IUniverse* universe, const std::string& id_, DataInfo* dinfo_) {
		std::string id			= fixDataId(id_);
		bool didError			= false;
		DataInfo dinfo			= dinfo_ ? *dinfo_ : DataInfo();
		std::string cacheId		= getCacheId(universe, id);
		auto onlyCheckCached	= dinfo.onlyCheckCached;
		auto noThrow			= dinfo.noAssertOnMissingDataLoader;

		{
			AUTO_REC_LOCK(m_dataReadMutex);

			DataCacheMap::iterator iter = m_dataCache.find(cacheId);

			// first we see whether the data has already been loaded
			if (iter != m_dataCache.end()) 
				return iter->second.data;

			/// If no cache update 
			if (onlyCheckCached)
				return nullptr;
		}

		auto iter = m_dlMap.find(id);
		if (noThrow && iter == m_dlMap.end())
			return nullptr;

		/// Ok ... here is where we will do our magic with the DR server ...
		try {
			if (m_didConnect || !m_didTryConnect) {
				/// Ok here, we get the data definition first of all ...
				Dataset ds = prepareForLoad(id);
				DataValue& dv = ds.values[0];
				DataDefinition def = dv.definition;

				/// We only do this if it's an integer or floating point data ...
				if (def.isInt() || def.isFloat()) {
					int port = config().getInt("port", s_dataRegistryPort);

					PTrace("Trying to remotely get data: %s @ localhost:%d", id, port);

					m_didTryConnect = true;

					/// Really short timeout
					const long connTimeout = 1; 
					Poco::Net::SocketAddress sa("localhost", port);
					Poco::Net::StreamSocket ss;

					/// Connect to the socket ...
					ss.connect(sa, Poco::Timespan(connTimeout, 0));

					m_didConnect = true;

					/// Now that we have a connection ... ask for the data to be loaded ...
					TMessage<Req_GetDataPtr> msg(MsgId::Req_GetDataPtr);
					msg.innerData().dinfo = dinfo;

                    if (universe)
                        strncpy(msg.innerData().universeName, universe->name().c_str(), sizeof(msg.innerData().universeName) - 1);
					strncpy(msg.innerData().dataId, id.c_str(), sizeof(msg.innerData().dataId) - 1);

					DataRegistry_Net::sendMsg(ss, msg);

					TMessage<Res_GetDataPtr> rmsg(MsgId::Res_GetDataPtr);
					DataRegistry_Net::recvMsg(ss, rmsg);

					/// Close the socket!
					ss.close();

					const Res_GetDataPtr res = rmsg.innerData();

					PTrace("Got address for %s: %p", id, (void*)res.address);

					if (res.address != 0) {
						dinfo.useShMem = true;
						dinfo.shmemAddress = true;
					}
					else {
						dinfo.useShMem = false;
						dinfo.shmemAddress = 0;
					}

					return DataRegistry_Net::getDataPtr(universe, id, &dinfo);
				}
			}
		}
		catch (const Poco::IllegalStateException& e) {
			PError("Socket IllegalStateException: %s", e.displayText());
			didError = true;
		}
		catch (const Poco::InvalidAccessException& e) {
			PError("Socket InvalidAccessException: %s", e.displayText());
			didError = true;
		}
		catch (const std::exception& e) {
			PError("Socket StdException: %s", std::string(e.what()));
			didError = true;
		}

		/// If all else fails, we just fallback to the normal DataRegistry behaiour
		return DataRegistry_Net::getDataPtr(universe, id, &dinfo);
	}
}
} /// namespace pesa 

