/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// DataRegistry_Client.h
///
/// Created on: 03 Oct 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "DataRegistry_Net.h"

namespace pesa {
namespace net {
	////////////////////////////////////////////////////////////////////////////
	/// Data registry client side implementation implementation
	////////////////////////////////////////////////////////////////////////////
	class DataRegistry_Client : public DataRegistry_Net {
	private:
		bool					m_didTryConnect = false;			/// Have we tried connecting to the DR server ...
		bool					m_didConnect = false;				/// Have we managed to connect to the DR server ...

	protected:
		virtual bool			isServer() const { return false; }

	public:
								DataRegistry_Client(const ConfigSection& config);
		virtual 				~DataRegistry_Client();

		////////////////////////////////////////////////////////////////////////////
		/// IDataRegistry overrides
		////////////////////////////////////////////////////////////////////////////
		virtual DataPtr			getDataPtr(IUniverse* universe, const std::string& dataId, DataInfo* dinfo);
	};
} /// namespace net
} /// namespace pesa 

