/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// DataRegistry_Net.cpp
///
/// Created on: 03 Oct 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "DataRegistry_Net.h"

namespace pesa {
namespace net {
    const int MsgHeader::s_version = 1;

	//////////////////////////////////////////////////////////////////////////
	/// DataRegistry_Net
	//////////////////////////////////////////////////////////////////////////
	DataRegistry_Net::DataRegistry_Net(const ConfigSection& config, std::string logChannel) : DataRegistry(config, logChannel) {
	}

	DataRegistry_Net::~DataRegistry_Net() {
	}

#define DR_NET_HANDLE_CREATE_MESSAGE(Id)	case MsgId::Id: return MessagePtr(new TMessage<Id>(hdr));

	MessagePtr DataRegistry_Net::createMessage(const MsgHeader& hdr) {
		switch (hdr.msgId) {
			DR_NET_HANDLE_CREATE_MESSAGE(Msg_None);
			DR_NET_HANDLE_CREATE_MESSAGE(Req_GetDataHeader);
			DR_NET_HANDLE_CREATE_MESSAGE(Res_GetDataHeader);
			DR_NET_HANDLE_CREATE_MESSAGE(Req_GetDataPtr);
			DR_NET_HANDLE_CREATE_MESSAGE(Res_GetDataPtr);
		}

		throw Poco::IllegalStateException(Poco::format("Unknown message id requested: %d", (int)hdr.msgId));
		ASSERT(false, "Unknown message type: " << hdr.msgId);

		return nullptr;
	}

#undef DR_NET_HANDLE_CREATE_MESSAGE

	void DataRegistry_Net::sendMsg(Poco::Net::StreamSocket& ss, const Message& msg) {
		ss.sendBytes((const void*)&msg.hdr(), (int)sizeof(MsgHeader));
		ss.sendBytes((const void*)msg.data(), (int)msg.size());
	}

	int DataRegistry_Net::recvBufferOfLength(Poco::Net::StreamSocket& ss, char* buffer, int length) {
		ASSERT(length > 0, "Invalid length requested to be retrieved from the socket. Requested Length: " << length);
		int numRecv = 0;

		while (numRecv < length) {
			int numLeft = length - numRecv;
			int numCopied = ss.receiveBytes((void*)(buffer + numRecv), numLeft);
			
			if (numCopied == 0)
				throw Poco::IllegalStateException("Socket in illegal state. Unable to read data properly!");

			numRecv += numCopied;
		}

		return numRecv;
	}

	MsgHeader DataRegistry_Net::recvHdr(Poco::Net::StreamSocket& ss) {
		MsgHeader hdr;

		recvBufferOfLength(ss, reinterpret_cast<char*>(&hdr), (int)sizeof(MsgHeader));
		if (hdr.version != MsgHeader::s_version)
			throw new Poco::InvalidAccessException(Poco::format("Message header version mismatch. Expecting: %d - Found: %d", MsgHeader::s_version, hdr.version));

		return hdr;
	}

	void DataRegistry_Net::recvMsg(Poco::Net::StreamSocket& ss, Message& msg, MsgHeader& hdr) {
		if ((int)msg.size() != hdr.size)
			throw new Poco::InvalidAccessException(Poco::format("Message data size does not match. Expecting: %d - Found: %d", (int)msg.size(), hdr.size));

		recvBufferOfLength(ss, msg.data(), (int)msg.size());
	}

	void DataRegistry_Net::recvMsg(Poco::Net::StreamSocket& ss, Message& msg) {
		MsgHeader hdr = recvHdr(ss);
		recvMsg(ss, msg, hdr);
	}

	MessagePtr DataRegistry_Net::recvMsg(Poco::Net::StreamSocket& ss) {
		MsgHeader hdr = recvHdr(ss);
		MessagePtr msg = createMessage(hdr);
		recvMsg(ss, *(msg.get()), hdr);
		return msg;
	}

	void DataRegistry_Net::handleMsg_None(Poco::Net::StreamSocket& ss, Msg_None& data, MsgHeader hdr) {
	}

	void DataRegistry_Net::handleReq_GetDataHeader(Poco::Net::StreamSocket& ss, Req_GetDataHeader& data, MsgHeader hdr) {
	}

	void DataRegistry_Net::handleRes_GetDataHeader(Poco::Net::StreamSocket& ss, Res_GetDataHeader& data, MsgHeader hdr) {
	}

	void DataRegistry_Net::handleReq_GetDataPtr(Poco::Net::StreamSocket& ss, Req_GetDataPtr& data, MsgHeader hdr) {
	}

	void DataRegistry_Net::handleRes_GetDataPtr(Poco::Net::StreamSocket& ss, Res_GetDataPtr& data, MsgHeader hdr) {
	}

#define DR_NET_HANDLE_MESSAGE(Id)	case MsgId::Id: {\
										TMessage<Id>* tmsg = dynamic_cast<TMessage<Id>* >(msg.get()); \
										Id& data = tmsg->innerData(); \
										handle##Id(ss, data, hdr); \
									}

	void DataRegistry_Net::handleMsg(Poco::Net::StreamSocket& ss, MessagePtr msg) {
		const auto& hdr = msg->hdr();

		switch (hdr.msgId) {
			DR_NET_HANDLE_MESSAGE(Msg_None);
			DR_NET_HANDLE_MESSAGE(Req_GetDataHeader);
			DR_NET_HANDLE_MESSAGE(Res_GetDataHeader);
			DR_NET_HANDLE_MESSAGE(Req_GetDataPtr);
			DR_NET_HANDLE_MESSAGE(Res_GetDataPtr);
		}
	}

#undef DR_NET_HANDLE_MESSAGE

}
} /// namespace pesa 

