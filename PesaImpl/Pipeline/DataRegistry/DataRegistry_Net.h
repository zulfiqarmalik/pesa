/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// DataRegistry_Net.h
///
/// Created on: 03 Oct 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Poco/Net/ServerSocket.h"
#include "Poco/Net/StreamSocket.h"
#include "Poco/Net/SocketStream.h"
#include "Poco/Net/SocketAddress.h"

#include "DataRegistry.h"

namespace pesa {
namespace net { 
	static const int			s_dataRegistryPort = 37705;
	static const int			s_maxName = 256;

	namespace MsgId {
		enum Type {
			Msg_None,
			Req_GetDataHeader,
			Res_GetDataHeader,

			Req_GetDataPtr,
			Res_GetDataPtr
		};
	}

	//////////////////////////////////////////////////////////////////////////
	/// MsgHeader: The message header that comes with each message
	//////////////////////////////////////////////////////////////////////////
	struct MsgHeader {
		static const int		s_version;
		int						version = s_version;				/// The version of the data
		int						size = 0;							/// The size of the data
		MsgId::Type				msgId;								/// The message id
		int						status = 0;							/// The status (only for responses)
	};

	struct Msg_None {
	};

	//////////////////////////////////////////////////////////////////////////
	/// GetDataHeader: Request/Response Get the data header for a certain data
	//////////////////////////////////////////////////////////////////////////
	struct Req_GetDataHeader {
		char					universeName[s_maxName] = { '\0' };	/// The name of the universe
		char					dataId[s_maxName] = { '\0' };		/// The id of the data to retrieve
	};

	//////////////////////////////////////////////////////////////////////////
	/// GetDataHeader: Request/Response Get the data header for a certain data
	//////////////////////////////////////////////////////////////////////////
	struct Res_GetDataHeader {
		DataHeader				dataHdr;							/// The data header for the data requested
		char					universeName[s_maxName] = { '\0' };	/// The name of the universe
		char					dataId[s_maxName] = { '\0' };		/// The id of the data to retrieve
	};

	//////////////////////////////////////////////////////////////////////////
	/// GetDataHeader: Request/Response Get the data header for a certain data
	//////////////////////////////////////////////////////////////////////////
	struct Req_GetDataPtr {
		char					universeName[s_maxName] = { '\0' };	/// The name of the universe
		char					dataId[s_maxName] = { '\0' };		/// The id of the data to retrieve
		DataInfo				dinfo;								/// The input data info
	};

	//////////////////////////////////////////////////////////////////////////
	/// GetDataHeader: Request/Response Get the data header for a certain data
	//////////////////////////////////////////////////////////////////////////
	struct Res_GetDataPtr {
		size_t					address = 0;						/// The shared memory address or 0 otherwise
	};

	//////////////////////////////////////////////////////////////////////////
	/// Message Data
	//////////////////////////////////////////////////////////////////////////
	class Message {
	protected: 
		MsgHeader				m_hdr;								/// The message header

	public:
								Message() {}
								Message(const MsgHeader& hdr) : m_hdr(hdr) {}
		virtual					~Message() {}

		virtual const char*		data() const = 0;
		virtual char*			data() = 0;
		virtual size_t			size() const = 0;

		inline MsgHeader&		hdr() { return m_hdr; }
		inline const MsgHeader& hdr() const { return m_hdr; }
	};

	typedef std::shared_ptr<Message> MessagePtr;

	//////////////////////////////////////////////////////////////////////////
	/// TMessage
	//////////////////////////////////////////////////////////////////////////
	template <class T>
	class TMessage : public Message {
	protected:
		T						m_data;

	public:
								TMessage(const MsgHeader& hdr) : Message(hdr) {}
								TMessage(MsgId::Type msgId) {
									m_hdr.size = sizeof(T);
									m_hdr.msgId = msgId;
								}

		virtual const char*		data() const { return reinterpret_cast<const char*>(&m_data); }
		virtual char*			data() { return reinterpret_cast<char*>(&m_data); }
		virtual size_t			size() const { return sizeof(m_data); }
		inline T&				innerData() { return m_data; }
	};

	//////////////////////////////////////////////////////////////////////////
	/// DataRegistry_Net: Base class for all network data registry ...
	//////////////////////////////////////////////////////////////////////////
	class DataRegistry_Net : public DataRegistry {
	protected:
		int						recvBufferOfLength(Poco::Net::StreamSocket& ss, char* buffer, int length);
		MsgHeader				recvHdr(Poco::Net::StreamSocket& ss);
		MessagePtr				recvMsg(Poco::Net::StreamSocket& ss);
		void					recvMsg(Poco::Net::StreamSocket& ss, Message& msg);
		void					recvMsg(Poco::Net::StreamSocket& ss, Message& msg, MsgHeader& hdr);
		void					sendMsg(Poco::Net::StreamSocket& ss, const Message& msg);

		void					handleMsg(Poco::Net::StreamSocket& ss, MessagePtr msg);
		virtual void			handleMsg_None(Poco::Net::StreamSocket& ss, Msg_None& data, MsgHeader hdr);
		virtual void			handleReq_GetDataHeader(Poco::Net::StreamSocket& ss, Req_GetDataHeader& data, MsgHeader hdr);
		virtual void			handleRes_GetDataHeader(Poco::Net::StreamSocket& ss, Res_GetDataHeader& data, MsgHeader hdr);
		virtual void			handleReq_GetDataPtr(Poco::Net::StreamSocket& ss, Req_GetDataPtr& data, MsgHeader hdr);
		virtual void			handleRes_GetDataPtr(Poco::Net::StreamSocket& ss, Res_GetDataPtr& data, MsgHeader hdr);

		static MessagePtr		createMessage(const MsgHeader& hdr);

		virtual bool			isServer() const = 0;

		template <class T>
		void					sendData(Poco::Net::StreamSocket& ss, const T& data) {
			MsgHeader			hdr;
			hdr.size			= sizeof(data);
			hdr.status			= 0;

			TMessage<T>			msg;
			msg.innerData()		= data;

			sendMsg(ss, &msg);
		}

	public:
								DataRegistry_Net(const ConfigSection& config, std::string logChannel);
		virtual 				~DataRegistry_Net();
	};
}
} /// namespace pesa 

