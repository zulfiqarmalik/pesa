/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// DataRegistry_Server.cpp
///
/// Created on: 03 Oct 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "DataRegistry_Server.h"

namespace pesa {
namespace net {
	DataRegistry_Server::DataRegistry_Server(const ConfigSection& config) : DataRegistry_Net(config, "DR_SV") {
	}

	DataRegistry_Server::~DataRegistry_Server() {
		stopTcpServer();
	}

	void DataRegistry_Server::stopTcpServer() {
		if (m_server) {
			PInfo("Stopping TCP Server ...");

			m_server->close();
			delete m_server;
			m_server = nullptr;

			PInfo("TCP Server stopped!");
		}
	}

	void DataRegistry_Server::prepare(RuntimeData& rt) { 
		PInfo("Initialised DataRegistry in Server mode!");
		DataRegistry_Net::prepare(rt);
	}

	void DataRegistry_Server::handleReq_GetDataPtr(Poco::Net::StreamSocket& ss, Req_GetDataPtr& msg, MsgHeader hdr) {
        std::string universeName(msg.universeName);
        std::string dataId(msg.dataId);

		IUniverse* universe = !universeName.empty() ? DataRegistry_Net::getUniverse(std::string(msg.universeName)) : nullptr;

        PInfo("GetDataPtr request: %s.%s", universeName, dataId);

		TMessage<Res_GetDataPtr> rmsg(MsgId::Res_GetDataPtr);

		/// If we're unable to load the required universe then we just ignore the request ...
		//if (!universe) {
		//	rmsg.innerData().address = 0;
		//	DataRegistry_Net::sendMsg(ss, rmsg);
		//	return;
		//}

		DataInfo dinfo = msg.dinfo;
		dinfo.useShMem = true;
		dinfo.shmemAddress = 0;

		DataPtr data = DataRegistry_Net::getDataPtr(universe, dataId, &dinfo);
		if (data && data->shmemPtr()) {
			rmsg.innerData().address = reinterpret_cast<size_t>(data->data());
        }
		else 
			rmsg.innerData().address = 0;

		DataRegistry_Net::sendMsg(ss, rmsg);
	}

	void DataRegistry_Server::tick(const RuntimeData& rt) {
		stopTcpServer();

		PInfo("Setting up TCP Server for the DataRegistry server ...");
		int port = config().getInt("port", s_dataRegistryPort);
		m_server = new Poco::Net::ServerSocket(port);

		while (true) {
			try {
				Poco::Net::StreamSocket ss = m_server->acceptConnection();
				MessagePtr msg = recvMsg(ss);

				/// We don't do anything if no message has been decoded!
				if (!msg)
					continue;

				/// Now that we have a message, we are going to process it ...
				handleMsg(ss, msg);
			}
			catch (const Poco::IllegalStateException& e) {
				PError("Socket IllegalStateException: %s", e.displayText());
			}
			catch (const Poco::InvalidAccessException& e) {
				PError("Socket InvalidAccessException: %s", e.displayText());
			}
			catch (const std::exception& e) {
				PError("Socket StdException: %s", std::string(e.what()));
			}
		}
	}

	void DataRegistry_Server::finalise(RuntimeData& rt) {
		stopTcpServer();
		DataRegistry_Net::finalise(rt);
	}
}
} /// namespace pesa 

