/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// DataRegistry_Server.h
///
/// Created on: 03 Oct 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "DataRegistry_Net.h"

namespace pesa {
namespace net {
	////////////////////////////////////////////////////////////////////////////
	/// Data registry server side implementation implementation
	////////////////////////////////////////////////////////////////////////////
	class DataRegistry_Server : public DataRegistry_Net {
	private:
		Poco::Net::ServerSocket* m_server = nullptr;		/// The TCP server socket ...

		void					stopTcpServer();

	protected:
		virtual bool			isServer() const { return true; }
		virtual void			handleReq_GetDataPtr(Poco::Net::StreamSocket& ss, Req_GetDataPtr& data, MsgHeader hdr);

	public:
								DataRegistry_Server(const ConfigSection& config);
		virtual 				~DataRegistry_Server();

		////////////////////////////////////////////////////////////////////////////
		/// IComponent overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			prepare(RuntimeData& rt);
		virtual void 			tick(const RuntimeData& rt);
		virtual void 			finalise(RuntimeData& rt);

		////////////////////////////////////////////////////////////////////////////
		/// IDataRegistry overrides
		////////////////////////////////////////////////////////////////////////////
	};
} /// namespace net
} /// namespace pesa 

