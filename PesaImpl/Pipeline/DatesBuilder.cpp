/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// DatesBuilder.cpp
///
/// Created on: 7 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Framework/Helper/FileDataLoader.h"
#include <unordered_map>

#include "Poco/File.h"
#include "Poco/FileStream.h"
#include "Poco/DateTime.h"
#include "Poco/Timestamp.h"
#include "PesaImpl/Components/SP/SpRest.h"

#include "Framework/Helper/FileDataLoader.h"

#include "Core/IO/BasicStream.h"

namespace pesa {

	struct DatesInfo {
		IntDate					startDate = 0;
		IntDate					endDate = 0;
		unsigned int			backdays = 0;
		IntDate					forcedEndDate = 0;
	};

	////////////////////////////////////////////////////////////////////////////
	/// Data registry implementation
	////////////////////////////////////////////////////////////////////////////
	class DatesBuilder : public IPipelineComponent {
	private:
		typedef std::unordered_map<IntDate, Poco::Timespan> DateTimespanMap;

		static const int 		s_maxDaysBehind = 10;

		std::string				m_datesDataId;
		std::string				m_timesDataId;
		std::string				m_tradingHoursConfig;
		std::string				m_holidaysConfig;
		std::string				m_holidayDescConfig;
		bool					m_datesModuleUpdated = false;
		bool					m_timesModuleUpdated = false;
		bool					m_readOnly = true;
		bool					m_perExchangeData = false;

		StringVec				m_countries;
		StringVec				m_exchanges;
		StringVec				m_exchangeIds;

		DatesInfo				m_datesInfo;

		void 					loadHolidaysCalendar(RuntimeData& rt);
		void 					loadTradingHours(RuntimeData& rt);

		void					updateDatesModule(RuntimeData& rt);
		void					updateTimesModule(RuntimeData& rt);

		void					saveDatesInfo(const RuntimeData& rt);
		void					loadDatesInfo(const RuntimeData& rt);
		void					loadForcedDates(const RuntimeData& rt);

		IntDate					getAlphasStartDate(const ConfigSection* alphasSec, IntDate minStartDate);
		IntDate					getAlphasStartDate(IntDate minStartDate);

	public:
								DatesBuilder(const ConfigSection& config);
		virtual 				~DatesBuilder();

		void 					buildDates(RuntimeData& rt);
		void 					buildTradingTimes(RuntimeData& rt);
		bool 					isHoliday(const RuntimeData& rt, const Poco::DateTime& date);

		////////////////////////////////////////////////////////////////////////////
		/// IPipelineComponent overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			prepare(RuntimeData& rt);
		virtual void 			tick(const RuntimeData& rt) {}
	};

	DatesBuilder::DatesBuilder(const ConfigSection& config) : IPipelineComponent(config, "DATES") {
		trace("Created DatesBuilder!");

		m_datesDataId			= config.getString("datesDataId", "");
		m_timesDataId			= config.getString("timesDataId", "");
		m_tradingHoursConfig	= config.getString("tradingHours", "");
		m_holidaysConfig		= config.getString("holidays", "");
		m_holidayDescConfig		= config.getString("holidayDesc", "");

		m_perExchangeData		= config.getBool("perExchangeData", false);
		m_readOnly				= config.getBool("readOnly", m_readOnly);

		auto exchanges			= config.getResolvedString("exchanges", "");
		auto exchangeIds		= config.getResolvedString("exchangeIds", "");

		if (!exchangeIds.empty()) {
			StringMap exchangeNames, exchangeCountries;

			FrameworkUtil::parseExchangeIds_Detailed(exchangeIds, m_exchangeIds, exchangeNames, exchangeCountries);

			m_exchanges.resize(m_exchangeIds.size());
			m_countries.resize(m_exchangeIds.size());

			for (size_t ei = 0; ei < m_exchangeIds.size(); ei++) {
				const auto& exchangeId = m_exchangeIds[ei];
				auto exchangeName = exchangeNames[exchangeId];
				auto exchangeCountry = exchangeCountries[exchangeId];

				ASSERT(!exchangeId.empty(), "Invalid exchange ID which is empty!");
				ASSERT(!exchangeName.empty(), "No name for exchange id: " << exchangeId << ". Most probably the mapping is incorrect!");
				ASSERT(!exchangeCountry.empty(), "No country for exchange id: " << exchangeId << ". Most probably the mapping is incorrect!");

				m_exchanges[ei] = exchangeName;
				m_countries[ei] = exchangeCountry;
			}
		}
		else if (!exchanges.empty()) 
			FrameworkUtil::parseExchanges(exchanges, m_exchanges, m_countries);
	}

	DatesBuilder::~DatesBuilder() {
		trace("Deleting DatesBuilder!");
	}

	bool DatesBuilder::isHoliday(const RuntimeData& rt, const Poco::DateTime& date) {
		int dow = date.dayOfWeek();

		if (dow == 0 || dow == 6) // 0 = Sunday, 6 = Saturday
			return true;

		IntDate idate = Util::dateToInt(date);
		if (rt.holidays.find(idate) != rt.holidays.end())
			return true;

		return false;
	}

	void DatesBuilder::updateDatesModule(RuntimeData& rt) {
		if (m_datesModuleUpdated)
			return;

		/// add a dummy date to the list of dates since the DataRegistry expects at least one date!
		IntDate endDate = parentConfig()->defs().endDate();

		if (!rt.appOptions.updateDataOnly && m_datesInfo.endDate != 0 && endDate > m_datesInfo.endDate)
			endDate = m_datesInfo.endDate;
		rt.dates.push_back(endDate);

		ConfigSection* module = parentConfig()->modules().findUnique("name", m_datesDataId);
		ASSERT(module, "While using DatesBuilder to load data from module. Unable to load module config: " << m_datesDataId);
		IDataLoaderPtr dl = helper::ShLib::global().createComponent<IDataLoader>(*module, rt.dataRegistry);
		rt.dataRegistry->runForData(rt, dl);

		m_datesModuleUpdated = true;
	}

	void DatesBuilder::updateTimesModule(RuntimeData& rt) {
		if (m_timesModuleUpdated)
			return;

		ConfigSection* module = parentConfig()->modules().findUnique("name", m_timesDataId);
		ASSERT(module, "While using DatesBuilder to load data from module. Unable to load module config: " << m_timesDataId);
		IDataLoaderPtr dl = helper::ShLib::global().createComponent<IDataLoader>(*module, rt.dataRegistry);
		rt.dataRegistry->runForData(rt, dl);

		m_timesModuleUpdated = true;
	}

	static int64_t smallest(std::vector<Int64MatrixData*>& times, IntDay di) {
		int64_t v = times[0]->getValue((IntDay)di, 0);

		for (size_t i = 1; i < times.size(); i++) {
			int64_t vi = times[i]->getValue((IntDay)di, 0); 
			v = std::min(v, vi);
		}

		return v;
	}

	static int64_t largest(std::vector<Int64MatrixData*>& times, IntDay di) {
		int64_t v = times[0]->getValue((IntDay)di, 0);

		for (size_t i = 1; i < times.size(); i++) {
			int64_t vi = times[i]->getValue((IntDay)di, 0);
			v = std::max(v, vi);
		}

		return v;
	}

	void DatesBuilder::loadTradingHours(RuntimeData& rt) {
		updateTimesModule(rt);

		DataInfo dinfo;
		dinfo.noCacheUpdate = true;

        std::vector<Int64MatrixData*> utcOpen, utcClose;

        std::string utcOpenId = config().getString("utcOpen", "");
        std::string utcCloseId = config().getString("utcClose", "");
        std::string isDstId = config().getString("isDst", "");

        if (!utcOpenId.empty() && !utcCloseId.empty()) {
            Int64MatrixData* utcOpenData = rt.dataRegistry->int64Data(nullptr, utcOpenId, (DataInfo*)&dinfo);
            Int64MatrixData* utcCloseData = rt.dataRegistry->int64Data(nullptr, utcCloseId, (DataInfo*)&dinfo);

            utcOpen.push_back(utcOpenData);
            utcClose.push_back(utcCloseData);
        }
        else {
		    utcOpen = rt.dataRegistry->int64DataAll(nullptr, "calendar.gmt_open_*", &dinfo);
		    utcClose = rt.dataRegistry->int64DataAll(nullptr, "calendar.gmt_close_*", &dinfo);
        }

		ASSERT(utcOpen.size(), "Unable to get any gmt_open_* times!");
		ASSERT(utcClose.size(), "Unable to get any gmt_close_* times!");

		size_t count = rt.dates.size();

		if (rt.isTodayHoliday && count)
			count--;

		/// If we're dumping the data then just use whatever the count we have in utcOpen array
		if (!rt.appOptions.dumpData.empty()) {
			count = utcOpen[0]->rows();
		}

		rt.utcTradingStartTime.resize(count);
		rt.utcTradingEndTime.resize(count);

		rt.utcTradingStartTime_DT.resize(count);
		rt.utcTradingEndTime_DT.resize(count);

		rt.isDST.resize(count);
		std::fill(rt.isDST.begin(), rt.isDST.end(), false);

        if (!isDstId.empty()) {
            const UShortMatrixData* isDst = nullptr;
            isDst = rt.dataRegistry->uint16Data(nullptr, isDstId, true);
            ASSERT(isDst, "Unable to load isDst data: " << isDstId);

            for (size_t di = 0; di < isDst->rows(); di++) {
                uint16_t value = (*isDst)(di, 0);
                if (value != 0)
                    rt.isDST[di] = true;
            }
        }
        else {
            PWarning("No isDst data source specified. DST mode will be disabled for plugins and data-loaders!");
        }

        for (IntDay di = 0; di < (IntDay)count; di++) {
			int64_t utcTradingStartTime = smallest(utcOpen, di);
			int64_t utcTradingEndTime = largest(utcClose, di);

			Poco::DateTime tmpStart = Util::py_timestampToDateTime(utcTradingStartTime);
			Poco::DateTime tmpEnd = Util::py_timestampToDateTime(utcTradingEndTime);

			//Poco::DateTime dtStart(tmpStart.year(), tmpStart.month(), tmpStart.day(), tmpStart.hour(), tmpStart.minute());
			//Poco::DateTime dtEnd(tmpEnd.year(), tmpEnd.month(), tmpEnd.day(), tmpEnd.hour(), tmpEnd.minute());

			rt.utcTradingStartTime_DT[di] = Poco::DateTime(tmpStart.year(), tmpStart.month(), tmpStart.day(), tmpStart.hour(), tmpStart.minute());
			rt.utcTradingEndTime_DT[di] = Poco::DateTime(tmpEnd.year(), tmpEnd.month(), tmpEnd.day(), tmpEnd.hour(), tmpEnd.minute());

			rt.utcTradingStartTime[di] = utcTradingStartTime;
			rt.utcTradingEndTime[di] = utcTradingEndTime;
		}
	}

	void DatesBuilder::loadHolidaysCalendar(RuntimeData& rt) {
		updateDatesModule(rt);

		const IntMatrixData* holidays = rt.dataRegistry->get<IntMatrixData>(nullptr, m_holidaysConfig, true);
		//const StringData* holidayDesc = rt.dataRegistry->get<StringData>(nullptr, m_holidayDescConfig, true);

		ASSERT(holidays, "Unable to load holiday data: " << m_holidaysConfig);
		//ASSERT(holidayDesc, "Unable to load holiday descriptive data: calendar.holidayDesc");

		size_t numHolidays = holidays->cols();

		for (size_t i = 0; i < numHolidays; i++) {
			//const std::string& holidayName = (*holidayDesc)[i];
			IntDate holidayDate = (IntDate)(*holidays)(0, i);
			rt.holidays[holidayDate] = true;

			//PTrace("Holiday: %u - %s", holidayDate, holidayName);
		}

		/// Now we load per exchange holiday calendar
		if (m_perExchangeData && m_countries.size() && m_exchanges.size()) {
			for (size_t i = 0; i < m_countries.size(); i++) {
				const auto& country = m_countries[i];
				const auto& exchange = m_exchanges[i];
				std::string exchangeName = country + exchange;
				std::string exchangeId = m_exchangeIds.size() ? m_exchangeIds[i] : "";

				const IntMatrixData* exchHolidays = rt.dataRegistry->get<IntMatrixData>(nullptr, "calendar.holidays_" + exchange, true);
				ASSERT(exchHolidays, "Unable to load data for exchange: " << exchange);

				size_t numExchHolidays = exchHolidays->cols();
				rt.exchangeHolidays[exchangeName] = IntDateBoolMap();

				for (size_t i = 0; i < numExchHolidays; i++) {
					IntDate holidayDate = (IntDate)(*exchHolidays)(0, i);
					rt.exchangeHolidays[exchangeName][holidayDate] = true;

					PTrace("Holiday: [%s:%s] %u", country, exchange, holidayDate);
				}

				rt.exchangeHolidays[exchange] = rt.exchangeHolidays[exchangeName];

				if (!exchangeId.empty())
					rt.exchangeHolidays[exchangeId] = rt.exchangeHolidays[exchangeName];
			}
		}
	}

	IntDate DatesBuilder::getAlphasStartDate(const ConfigSection* alphasConfig, IntDate minStartDate) {
		auto childAlphasConfigs = alphasConfig->getAllChildrenOfType("Alphas");
		if (childAlphasConfigs.size()) {
			for (auto i = 0; i < childAlphasConfigs.size(); i++)
				minStartDate = getAlphasStartDate(childAlphasConfigs[i], minStartDate);
		}

		auto alphas = alphasConfig->alphas();

		for (auto i = 0; i < alphas.size(); i++) {
			auto* alphaConfig = alphas[i];

			/// OK here we try to see if there is a start date in the alpha that 
			/// overrides the startDate of the entire config ...
			IntDate alphaStartDate = alphaConfig->get<int>("startDate", 0);

			/// If we find even a single alpha without a startDate then we just abort the process ...
			if (alphaStartDate == 0)
				return 0;

			if ((minStartDate != 0 && alphaStartDate < minStartDate) || !minStartDate)
				minStartDate = alphaStartDate;
		}

		return minStartDate;
	}

	IntDate DatesBuilder::getAlphasStartDate(IntDate minStartDate) {
		const ConfigSection* alphasConfig = const_cast<ConfigSection&>(parentConfig()->portfolio()).alphasSec();
		return getAlphasStartDate(alphasConfig, minStartDate);
	}

	void DatesBuilder::buildDates(RuntimeData& rt) {
		loadHolidaysCalendar(rt);

		const Config* config = parentConfig();
		const ConfigSection& defs = config->defs();

		rt.dates.clear();

		auto configEndDateStr = defs.getRequired("endDate");
		int endDateOffset = 0;

		bool exactStartDate = defs.getBool("exactStartDate", false);
		std::string endDatePercent = defs.getString("endDatePercent", "");
		auto configStartDate = defs.startDate();
		auto configEndDate = Util::parseDate(configEndDateStr, &endDateOffset);

		//if (rt.appOptions.startDate > 0) {
		//	PWarning("Start date override. Overriding from: %u => %u", configEndDate, rt.appOptions.startDate);
		//	configStartDate = rt.appOptions.startDate;
		//}

		//if (rt.appOptions.endDate > 0) {
		//	PWarning("End date override. Overriding from: %u => %u", configEndDate, rt.appOptions.endDate);
		//	configEndDate = rt.appOptions.endDate;
		//}

		//IntDate minAlphaStartDate = getAlphasStartDate(0);

		/// If all the alphas have a start date and its later than the config start date then we want to start from there!
		//if (!rt.appOptions.updateDataOnly) {
		//	if (minAlphaStartDate && minAlphaStartDate >= configStartDate && minAlphaStartDate < configEndDate)
		//		configStartDate = minAlphaStartDate;
		//}

		if (configStartDate < m_datesInfo.startDate)
			m_datesInfo.startDate = configStartDate;

		auto configBackdays = defs.backdays();

		if (configBackdays > m_datesInfo.backdays)
			m_datesInfo.backdays = configBackdays;

		Poco::DateTime startDate = Util::intToDate(m_datesInfo.startDate);

		/// Clamp the end date if the user has requested it ...
		if (!rt.appOptions.updateDataOnly && rt.appOptions.clampEndDate && m_datesInfo.endDate != 0 && configEndDate > m_datesInfo.endDate)
			configEndDate = m_datesInfo.endDate;

		if (m_datesInfo.forcedEndDate > 0) {
			PInfo("[DatesBuilder] FOCING END DATE TO: %u", m_datesInfo.forcedEndDate);
			configEndDate = m_datesInfo.forcedEndDate;
		}

		m_datesInfo.endDate = configEndDate;
		Poco::DateTime endDate = Util::intToDate(m_datesInfo.endDate);

		auto actualStartDate = defs.startDate();

		rt.backdays = m_datesInfo.backdays;

		ASSERT(startDate < endDate, "Start date must be less than the end date!");

		trace("Got initial date range: [%u, %u]", Util::dateToInt(startDate), Util::dateToInt(endDate));

		Poco::DateTime dateIter = startDate;

		while (dateIter <= endDate) {
			if (!isHoliday(rt, dateIter)) {
				IntDate date = Util::dateToInt(dateIter);
				rt.dates.push_back(date);
			}

			dateIter += Util::daysSpan(1);
		}

		/// Remove the last N days (as specified in the config)
		for (int i = 0; i > endDateOffset; i--) {
			rt.dates.pop_back();
		}

		/// If we didn't get anything
		if (rt.dates.size() == 0)
			PWarning("The given date range has no valid dates!");

		rt.configStartDate = Util::dateToInt(startDate);

		if (endDateOffset == 0)
			rt.configEndDate = Util::dateToInt(endDate);
		else
			rt.configEndDate = rt.dates[rt.dates.size() - 1];

		IntDate endTradingDate = rt.dates[rt.dates.size() - 1];
		ASSERT(rt.configEndDate >= endTradingDate, "End trading date: " << endTradingDate << " - Config end date: " << rt.configEndDate);

		if (rt.configEndDate > endTradingDate) {
			PInfo("Back test end date is a holiday");
			rt.isTodayHoliday = true;

			/// Always have the ending date as part of the dates
			rt.dates.push_back(rt.configEndDate);
		}

		if (!rt.appOptions.dumpData.empty())
			m_datesInfo.backdays = 0;

		if (m_datesInfo.backdays > (unsigned int)rt.dates.size()) {
			PWarning("Not enough simulation dates to cover backdays. Total date range: %z - Num backdays: %u. Resetting to 1!", rt.dates.size(), m_datesInfo.backdays);
			m_datesInfo.backdays = 1;
			//ASSERT(m_datesInfo.backdays < (unsigned int)rt.dates.size(), "Not enough simulation dates to cover backdays. Total date range: " << rt.dates.size() << " - Num backdays: " << m_datesInfo.backdays);
		}

		rt.diEnd = (IntDay)rt.dates.size() - 1;
		rt.endDate = rt.dates[rt.dates.size() - 1];

		if (!exactStartDate) {
			/// We always have backdays worth of data and that is what our starting date is
			/// so we start with that as the first day of our simulation ...
			rt.startDate = rt.dates[0];
			rt.diStart = (IntDay)m_datesInfo.backdays; 
		}
		else {
			rt.diStart = rt.getDateIndex(configStartDate);
			ASSERT(rt.diStart >= 0 && rt.diStart < rt.diEnd, "Invalid start date: " << rt.configStartDate);

			rt.startDate = rt.dates[rt.diStart];
			rt.backdays = rt.diStart - 1;
		
		}

		if (!endDatePercent.empty()) {
			float percent = 100.0f - Util::cast<float>(endDatePercent);
			//ASSERT(percent >= 0.0 && percent < 100, "Invalid endDatePercent: " << endDatePercent);
			float factor = percent * 0.01f;

			/// In case the uses does not want any outsample, we just don't want any floating point round-offs
			/// causing us to run a smaller backtest!
			if (percent >= 99.9f)
				factor = 1.0f;

			rt.diEnd = rt.diStart + (IntDay)((float)(rt.diEnd - rt.diStart) * factor);
			rt.endDate = rt.dates[rt.diEnd];
			rt.backdays = rt.diStart - 1;
		}

		rt.holidaysAhead.resize(rt.dates.size());

		for (IntDay di = 0; di < (IntDay)rt.dates.size(); di++) {
			IntDate date = rt.dates[di];

			if (di < (IntDay)rt.dates.size() - 1) {
				Poco::DateTime currDate = Util::intToDate(date);
				Poco::DateTime nextDate = Util::intToDate(rt.dates[di + 1]);
				Poco::Timespan diff = nextDate - currDate;
				rt.holidaysAhead[di] = diff.days() - 1;
			}

			rt.datesIndex[date] = di;
		}

		rt.holidaysAhead[rt.holidaysAhead.size() - 1] = 0;

		std::string todayDesc = rt.isTodayHoliday ? "holiday" : "trading-day";
		PInfo("Got simulation date range: [%u, %u] (backdays = %u, today = %s)", rt.startDate, rt.endDate, rt.backdays, todayDesc);
	}

	void DatesBuilder::buildTradingTimes(RuntimeData& rt) {
		loadTradingHours(rt);
	}

	void DatesBuilder::prepare(RuntimeData& rt) {
		PTrace("DatesBuilder::exec");
		loadDatesInfo(rt);
		loadForcedDates(rt);

		buildDates(rt);
		buildTradingTimes(rt);

		/// TODO: Get the current time and then compare it against the trading time ... 
		/// If we have gone past the market end then we need to add a day
		/// 
		//Poco::DateTime dtCurr;
		//Poco::DateTime dtTradeEnd = rt.utcTradingEndTime[rt.diEnd - 1];
		//if (dtCurr > dtTradeEnd) {
		//}

		rt.initPeriods();

		saveDatesInfo(rt);

		PInfo("Total number of SIM days: %d [diStart = %d - diEnd = %d]", rt.diEnd - rt.diStart + 1, rt.diStart, rt.diEnd);
	}

	void DatesBuilder::saveDatesInfo(const RuntimeData& rt) {
		if (m_readOnly && !rt.appOptions.updateDataOnly)
			return; 

		/// Now that this has been built write it to the stream
		const auto* config = parentConfig();
		auto dataCacheDir = config->defs().dataCachePath();
		dataCacheDir = config->defs().resolve(dataCacheDir, (IntDate)0);
		Poco::File dataCacheDirPath(dataCacheDir);

		if (!dataCacheDirPath.exists())
			dataCacheDirPath.createDirectories();

		auto filePath = Poco::Path(dataCacheDir).append("dates");
		auto stream = io::BasicOutputStream::createBinaryWriter(filePath.toString());
		stream->stream()->write((char*)&m_datesInfo, sizeof(DatesInfo));

		/// Write simulation dates in a text file
		if (rt.appOptions.updateDataOnly) {
			auto filePath = Poco::Path(dataCacheDir).append("sim_dates.txt");
			FILE* file = fopen(filePath.toString().c_str(), "w");

			for (IntDate date : rt.dates)
				fprintf(file, "%d\n", (int)date);

			fflush(file);
			fclose(file);
		}
	}

	void DatesBuilder::loadForcedDates(const RuntimeData& rt) {
		const auto* config = parentConfig();
		auto dataCacheDir = config->defs().dataCachePath();
		dataCacheDir = config->defs().resolve(dataCacheDir, (IntDate)0);
		Poco::File dataCacheDirPath(dataCacheDir);

		if (!dataCacheDirPath.exists())
			dataCacheDirPath.createDirectories();

		auto filePath = Poco::Path(dataCacheDir).append("forcedDates");
		Poco::File forcedDatesFile(filePath.toString());

		/// If the file does not exist ...
		if (!forcedDatesFile.exists()) 
			return;

		/// Otherwise we load the data from it
		helper::FileDataLoader fdl(filePath.toString(), ",", false, false);
		auto endDates = fdl.read();

		if (endDates.size()) {
			m_datesInfo.forcedEndDate = Util::cast<IntDate>(endDates[0]);
		}
	}

	void DatesBuilder::loadDatesInfo(const RuntimeData& rt) {
		/// Now that this has been built write it to the stream
		const auto* config = parentConfig();
		auto dataCacheDir = config->defs().dataCachePath();
		dataCacheDir = config->defs().resolve(dataCacheDir, (IntDate)0);
		Poco::File dataCacheDirPath(dataCacheDir);

		if (!dataCacheDirPath.exists())
			dataCacheDirPath.createDirectories();

		auto filePath = Poco::Path(dataCacheDir).append("dates");
		Poco::File datesFile(filePath.toString());

		/// If the file does not exist ...
		if (!datesFile.exists()) {
			m_datesInfo.startDate = parentConfig()->defs().startDate();
			m_datesInfo.backdays = parentConfig()->defs().backdays();
			return;
		}

		auto stream = io::BasicInputStream::createBinaryReader(filePath.toString());
		stream->stream()->read((char*)&m_datesInfo, sizeof(m_datesInfo));

		//int configBackdays = config->defs().backdays();
		//ASSERT(configBackdays <= m_datesInfo.backdays, "Backdays mismatch. Previously it was: " << m_datesInfo.backdays << " - now it is: " << configBackdays); 

		int configStartDate = parentConfig()->defs().startDate();
		ASSERT(configStartDate >= m_datesInfo.startDate, "Invalid config start date. The cache starts at: " << m_datesInfo.startDate << " - the config starts at: " << configStartDate);

		/// If we are not updating the data then we make sure that the backtest doesn't try to go past the end!
		//if (!rt.appOptions.updateDataOnly) {
		//	int configEndDate = parentConfig()->defs().endDate();
		//	ASSERT(configEndDate <= m_datesInfo.endDate, "Invalid config end date. The cache ends at: " << m_datesInfo.endDate << " - the config ends at: " << configEndDate);
		//}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IPipelineComponent* createDatesBuilder(const pesa::ConfigSection& config) {
	return new pesa::DatesBuilder((const pesa::ConfigSection&)config);
}
