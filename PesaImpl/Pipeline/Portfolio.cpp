/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Portfolio.cpp
///
/// Created on: 22 Jun 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Framework/Helper/FileDataLoader.h"
#include <unordered_map>
#include "Core/Math/Stats.h"

#include "Poco/File.h"
#include "Core/Lib/Profiler.h"
#include "Core/IO/BasicStream.h" 
#include "Helper/DB/MongoStream.h"
#include "Helper/DB/MongoFileStream.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Portfolio - Executes all the alphas
	////////////////////////////////////////////////////////////////////////////
	class Portfolio : public IPipelineComponent {
	private:
		typedef std::unordered_map<std::string, LifetimeStatisticsPtr> LTStats_DataMap;
		static const int		s_checkpointVersion;
		static const float		s_alphaBookSize;
		static const std::string s_checkpointCollName;

		std::string				m_priceCalcId = Constants::s_defClosePrice;
		std::string				m_currDayPriceCalcId = Constants::s_defCurrDayPrice;
		std::string				m_borrowCostId = Constants::s_defBorrowCost;
		std::string				m_locatesId = Constants::s_defLocates;
		std::string				m_slippageDataId = "cost.spreadSlippage";
		std::string				m_impactDataId = "";
		std::string				m_sectorDataId;
		std::string				m_dividendDataId = "";
		std::string				m_openPriceDataId = "baseData.adjOpenPrice";
		std::string				m_highPriceDataId = "baseData.adjHighPrice";
		std::string				m_lowPriceDataId = "baseData.adjLowPrice";
		StringVec				m_intradayPriceDataIds;
		float					m_dividendTax = 0.0f;
		float					m_defaultSlippage = 0.1f;
		const ConfigSection*	m_statsSec = nullptr;
		bool					m_doOptimise = false;
		bool					m_debug = false;
		bool					m_calcAlpha = true;
		bool					m_calcPortfolio = true;
		IOptimiserPtr			m_optimiser	= nullptr;
		bool					m_enableCheckpoint = false;
		IntDay					m_checkPointDay = 5;
		bool					m_checkpointToDB = false;
		bool					m_checkpointIgnoreFileTime = false;
		std::string				m_portUuid;
		std::string				m_portIID;
		bool					m_runningWindowCalculations = false;

		LTStats_DataMap			m_ltStatsMap;			/// Lifetime statistics map!

		io::InputStream&		readResult(const RuntimeData& rt, AlphaResult& r, io::InputStream& is, size_t& level);
		io::OutputStream&		writeResult(const RuntimeData& rt, AlphaResult& r, io::OutputStream& os, size_t& level);

		void					calculateAlpha(const RuntimeData& rt);
		void					calculateAlpha(const RuntimeData& rt, const AlphaResult* yesterday, const AlphaResult* today);
		void					calculateAlpha(const RuntimeData& rt, IAlphaPtr today);

		bool					optimise(const RuntimeData& rt, float bookSize, CalcData& data);
		bool					saveCheckpoint(const RuntimeData& rt);
		bool					loadCheckpoint(RuntimeData& rt);
		std::string				getCheckpointFilename() const;

		void					calculatePortfolio(const RuntimeData& rt);
		LifetimeStatisticsPtr	getLTStats(const RuntimeData& rt, const ConfigSection* config, const std::string& id, const std::string& universeId, std::string level = "");

		static void				clearDataForDay(IDataRegistry& dr, IntDay di);

		static inline std::string genId(IntDay di) { return IAlpha::genId(di, "Portfolio"); }
		static inline std::string genRawId(IntDay di) { return IAlpha::genId(di, "RawPortfolio"); }
		static inline std::string genNoOptId(IntDay di) { return IAlpha::genId(di, "NoOptPortfolio"); }

	public:
								Portfolio(const ConfigSection& config);
		virtual 				~Portfolio();

		////////////////////////////////////////////////////////////////////////////
		/// IPipelineComponent overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			prepare(RuntimeData& rt);
		virtual void 			tick(const RuntimeData& rt);
		virtual void 			finalise(RuntimeData& rt);
	};

	const int Portfolio::s_checkpointVersion = 10;
	const float Portfolio::s_alphaBookSize = 20e6;
	const std::string Portfolio::s_checkpointCollName = "Checkpoint";

	Portfolio::Portfolio(const ConfigSection& config) : IPipelineComponent(config, "PORTFOLIO") {
		PTrace("Created Portfolio!");
		const auto& portConfig	= parentConfig()->portfolio();

		m_priceCalcId			= portConfig.get<std::string>("calc", m_priceCalcId);
		m_currDayPriceCalcId	= portConfig.get<std::string>("currDayCalc", m_currDayPriceCalcId);
		m_borrowCostId			= portConfig.get<std::string>("borrowCost", m_borrowCostId);
		m_locatesId				= portConfig.get<std::string>("locates", m_locatesId);
		m_sectorDataId			= portConfig.getString("sectorData", m_sectorDataId);
		m_slippageDataId		= portConfig.getString("slippageDataId", m_slippageDataId);
		m_impactDataId			= portConfig.getString("impactDataId", m_impactDataId);
		m_debug					= portConfig.debug();
		m_openPriceDataId		= portConfig.getString("openPriceDataId", m_openPriceDataId);
		m_highPriceDataId		= portConfig.getString("highPriceDataId", m_highPriceDataId);
		m_lowPriceDataId		= portConfig.getString("lowPriceDataId", m_lowPriceDataId);

		m_statsSec				= portConfig.getChildSection("Stats");
		m_calcPortfolio			= portConfig.getBool("calcPortfolio", m_calcPortfolio);
		m_enableCheckpoint		= portConfig.getBool("checkpoint", m_enableCheckpoint);
		m_checkPointDay			= portConfig.get<int>("checkpointDay", m_checkPointDay);
		m_checkpointToDB		= portConfig.getBool("checkpointToDB", m_checkpointToDB);
		m_checkpointIgnoreFileTime = portConfig.getBool("checkpointIgnoreFileTime", m_checkpointIgnoreFileTime);
		m_runningWindowCalculations = portConfig.getBool("runningWindowCalculations", m_runningWindowCalculations);

		auto intradayPriceDataId= portConfig.getString("intradayPriceDataId", "");

		if (!intradayPriceDataId.empty()) {
			m_intradayPriceDataIds = Util::split(intradayPriceDataId, ",");
		}

		m_dividendDataId		= portConfig.getString("dividendDataId", "");
		auto dividendTax		= portConfig.getString("dividendTax", "");

		if (!dividendTax.empty()) 
			m_dividendTax		= Util::percent(dividendTax) * 0.01f;

		if (Application::instance()->appOptions().updateDataOnly) {
			m_enableCheckpoint	= false;
			m_checkpointToDB	= false;
		}

		if (m_checkpointToDB) {
			m_portIID			= portConfig.getRequired("iid");
			m_portUuid			= m_portIID; /// portConfig.getRequired("uuid");
		}

		auto defSlippage		= portConfig.getString("defaultSlippage", "");
		if (!defSlippage.empty())
			m_defaultSlippage	= Util::percent(defSlippage);

		const ConfigSection* optimiseSec = portConfig.getChildSection("Optimise");
		if (optimiseSec) {
			m_doOptimise = optimiseSec->getBool("optimise");
		}
	}

	Portfolio::~Portfolio() {
		PTrace("Deleting Portfolio!");
	}

	LifetimeStatisticsPtr Portfolio::getLTStats(const RuntimeData& rt, const ConfigSection* config, const std::string& id, const std::string& universeId, std::string level /* = "" */) {
		LTStats_DataMap::iterator iter = m_ltStatsMap.find(id);
		if (iter != m_ltStatsMap.end())
			return iter->second;

		IDataRegistry& dr = *rt.dataRegistry;

		auto ltStatsPtr = std::make_shared<LifetimeStatistics>();
		ltStatsPtr->runningWindowCalculations = m_runningWindowCalculations;
		m_ltStatsMap[id] = ltStatsPtr;

		ltStatsPtr->uuid = id;

		auto market = parentConfig()->defs().market();
		ASSERT(!market.empty(), "Invalid market in the defs!");

		ltStatsPtr->universeId = universeId;
		ltStatsPtr->market = market;

		auto configCreationDate = config->getString("creationDate", "");
		auto configPublishDate = config->getString("publishDate", "");

		if (!configCreationDate.empty()) 
			ltStatsPtr->creationDate = Util::cast<IntDate>(configCreationDate);
		else {
			/// Use the last date as the creation date ... there will not be any outsample
			ltStatsPtr->creationDate = rt.dates[rt.diEnd];
		}

		if (!configPublishDate.empty())
			ltStatsPtr->publishDate = Util::cast<IntDate>(configPublishDate);
		else {
			/// Use the last date as the publis date ... there will not be any outsample
			ltStatsPtr->publishDate = rt.dates[rt.diEnd];
		}

		ASSERT(ltStatsPtr->creationDate <= ltStatsPtr->publishDate, "Invalid creation/publish date. Creation/OS date should always be <= publish date. Creation Date: " << ltStatsPtr->creationDate << " - Publish Date: " << ltStatsPtr->publishDate);
		//else {
		//	Poco::File configFile(parentConfig()->filename());

		//	if (configFile.isFile() && configFile.exists()) {
		//		Poco::Timestamp configTS = configFile.getLastModified();
		//		Poco::DateTime dt(configTS);
		//		ltStatsPtr->creationDate = Util::dateToInt(dt);
		//	}
		//}

		if (!level.empty())
			ltStatsPtr->level = level;

		if (config) {
			auto type = config->getString("type", "");
			if (!type.empty())
				ltStatsPtr->type = type;

			auto level = config->getString("level", "");
			if (!level.empty())
				ltStatsPtr->level = level;

			auto dataUsed = config->getString("dataUsed", "");
			if (!dataUsed.empty())
				ltStatsPtr->tags["dataUsed"] = dataUsed;

			auto gbatchId = config->getString("gbatchId", "");
			if (!gbatchId.empty())
				ltStatsPtr->tags["gbatchId"] = gbatchId;

			auto maxTime = config->get<int>("maxTime", 0);
			ltStatsPtr->maxTime = maxTime;

			auto delay = config->getString("delay", "1");
			if (!delay.empty())
				ltStatsPtr->delay = Util::cast<int>(delay);
		}

		return ltStatsPtr;
	}

	void Portfolio::calculateAlpha(const RuntimeData& rt, IAlphaPtr today) {
		PROFILE_SCOPED();

		/// IMPORTANT! Make a copy of the data so that we don't destroy the original data
		/// in yesterday's alpha
		//float bookSize = parentConfig()->portfolio().bookSize();
		float bookSize = s_alphaBookSize; ///parentConfig()->portfolio().bookSize();

		/// finalise todays data for PNL calculation later on anyway ... this bit has
		/// no dependence on yesterdays data hence can be done ...
		CalcData& todayData = today->data();
		const CalcData* yesterdayData = today->yesterdayData();
		std::string todayUuid = today->uuid();

		todayData.finaliseForPNLCalc(rt, bookSize, m_priceCalcId, m_currDayPriceCalcId);

		if (yesterdayData) {
			todayData.yesterday = today->yesterdayDataPtr(); ///new CalcData(*yesterdayData); 
			todayData.yesterday->yesterday = nullptr; /// we don't want to have an infinite chain of copying!

			todayData.calculatePNL(rt, bookSize, m_priceCalcId, "", "", "", nullptr, m_currDayPriceCalcId);
		}

		if (m_statsSec) { /// && m_statsSec->getBool("alpha") == true) {
			todayData.calculateStats(rt, bookSize);

			std::string ltStatsId = todayUuid;
			todayData.cumStats = getLTStats(rt, todayData.config, ltStatsId, todayData.universe->name(), "alpha");
			const Statistics& todayStats = today->stats();

			todayData.cumStats->add(todayStats);
			if (rt.isLastDi())
				todayData.cumStats->finalise();
			//ltStats->dataPtr()->add(todayData.stats);

			PTrace("ALPHA %s PNL\n%s", today->uuid(), todayData.stats.toString(true, true, todayData.cumStats.get()));
		}
	}

	void Portfolio::calculateAlpha(const RuntimeData& rt, const AlphaResult* yesterday, const AlphaResult* today) {
		PROFILE_SCOPED();

		//if (!today->childAlphas.size())
		//	return;

		for (size_t i = 0; i < today->childAlphas.size(); i++) {
			/// Only if we have this alpha within our history for yesterday ...
			if (yesterday && yesterday->childAlphas.size() > i)
				calculateAlpha(rt, &yesterday->childAlphas[i], &today->childAlphas[i]);
			else
				calculateAlpha(rt, nullptr, &today->childAlphas[i]);
		}

		//if (!m_locatesId.empty()) {
		//	const IntMatrixData* locates = rt.dataRegistry->get<IntMatrixData>(today->alpha->universe(), m_locatesId);
		//	ASSERT(locates, "Unable to load locates: " << m_locatesId);
		//	auto& alpha = today->alpha->alpha();
		//	size_t numRows = alpha.rows();
		//	size_t numCols = alpha.cols();

		//	for (size_t di = 0; di < numRows; di++) {
		//		for (size_t ii = 0; ii < numCols; ii++) {
		//			int canLocate = (*locates)(rt.di, ii);
		//			if (!canLocate) {
		//				alpha.makeInvalid((IntDay)di, ii);
		//				float& value = alpha((IntDay)di, ii);
		//				ASSERT(std::isnan(value), "What's this!: " << value);
		//			}
		//		}
		//	}
		//}

		if (today->alpha->config().getBool("calculatePnl", true) == false)
			return;

		if (yesterday) { 
			if (yesterday->childAlphas.size() != today->childAlphas.size())
				PWarning("Yesterday's and today's data is not the same. Yesterday: %z - Today: %z", yesterday->childAlphas.size(), today->childAlphas.size());

			if (yesterday->alpha && today->alpha) {
				if (yesterday->alpha->uuid() != today->alpha->uuid())
					PWarning("ID mismatch from yesterday to today-> Yesterday ID: %s - Today ID: %s", yesterday->alpha->uuid(), today->alpha->uuid());

				calculateAlpha(rt, today->alpha);
			}
		}
		else if (today->alpha)
			calculateAlpha(rt, today->alpha);
	}

	void Portfolio::calculateAlpha(const RuntimeData& rt) {
		PROFILE_SCOPED();

		/// We get the alpha values from the previous day since we can only 
		/// calculate previous days' PNL on the current day. Todays PNL
		/// will be calculated tomorrow going by this logic!
		std::string dataId	= IAlpha::genId(rt.di - 1);
		auto* yesterdayData = rt.dataRegistry->get<AlphaResultData>(nullptr, IAlpha::genId(rt.di - 1));
		auto* todayData = rt.dataRegistry->get<AlphaResultData>(nullptr, IAlpha::genId(rt.di));

		/// We can encounter a situation where there is no alpha generated even if there are alphas block
		if (!todayData)
			return;

		//ASSERT(todayData, "No data generated for today!");

		calculateAlpha(rt, yesterdayData ? yesterdayData->dataPtr().get() : nullptr, todayData->dataPtr().get());
	}

	void Portfolio::calculatePortfolio(const RuntimeData& rt) {
		PROFILE_SCOPED();

		if (!m_calcPortfolio)
			return;

		bool calcStats		= m_statsSec && m_statsSec->getBool("portfolio") == true;
		auto todayPort		= AlphaResultPtr(new AlphaResult());
		auto todayPortNoOpt	= AlphaResultPtr(new AlphaResult());
		auto todayPortRaw	= AlphaResultPtr(new AlphaResult());

		auto* yesterdayPort	= rt.dataRegistry->get<AlphaResultData>(nullptr, Portfolio::genId(rt.di - 1));
		auto* yesterdayPortNoOpt = rt.dataRegistry->get<AlphaResultData>(nullptr, Portfolio::genNoOptId(rt.di - 1), true, nullptr, true);
		auto* todayAlphaResult = rt.dataRegistry->get<AlphaResultData>(nullptr, IAlpha::genId(rt.di));

		if (!todayAlphaResult || !todayAlphaResult->dataPtr() || !todayAlphaResult->dataPtr()->alpha) {
			rt.dataRegistry->setData(Portfolio::genId(rt.di), nullptr);
			rt.dataRegistry->setData(Portfolio::genRawId(rt.di), nullptr);
			return;
		}

		auto* todayAlpha	= todayAlphaResult->dataPtr()->alpha.get();

		/// This can happen if there is no portfolio section!
		if (!todayAlpha) {
			PWarning("No alpha for today. Is there a Portfolio section in the config with valid alphas?");
			return;
		}

		//ASSERT(todayAlpha, "Invalid state! No alpha for today!");

		auto* config		= parentConfig();
		std::string name	= config->portfolio().getString("uuid", "port");
		todayPort->alpha	= std::make_shared<ComboResult>(name, todayAlpha->config());
		todayPortNoOpt->alpha = std::make_shared<ComboResult>(name + "_pre_opt", todayAlpha->config());
		todayPortRaw->alpha	= std::make_shared<ComboResult>(name + "_raw", todayAlpha->config());

		auto today			= todayPort->alpha;
		auto yesterday		= yesterdayPort ? yesterdayPort->dataPtr()->alpha : nullptr;
		auto todayNoOpt		= todayPortNoOpt->alpha;
		auto yesterdayNoOpt = yesterdayPortNoOpt ? yesterdayPortNoOpt->dataPtr()->alpha : nullptr;
		float bookSize		= config->portfolio().bookSize();

		/// finalise todays data for PNL calculation later on anyway ... this bit has
		/// no dependence on yesterdays data hence can be done ...
		today->data()		= todayAlpha->data();
		CalcData& todayData	= today->data();

		todayData.finaliseForPNLCalc(rt, bookSize, m_priceCalcId, m_currDayPriceCalcId);

		/// Make a copy to the NO-OPT
		todayNoOpt->data().deepCopy(todayData);

		/// We set yesterday's data before rather than further down since 
		/// we will need it to calculate the turnover for the day 
		/// so that the optimiser can do turnover optimisation
		if (yesterday) {
			todayData.yesterday = yesterday->dataPtr(); /// new CalcData(yesterday->data());
			todayData.yesterday->yesterday = nullptr; /// we don't want to have an infinite chain of copying!
		}

		/// Once the PNL calc has been finalised ... optimise it (OPTIONAL)
		bool didOpitmise = optimise(rt, bookSize, todayData);

		/// Reverse calculate the value from the shares
		todayData.calculateValueFromShares(rt, m_priceCalcId, m_currDayPriceCalcId, true);

		if (didOpitmise)
			todayNoOpt->data().calculateValueFromShares(rt, m_priceCalcId, m_currDayPriceCalcId, true);

		if (yesterday) {
			/// Recalculate the PNL since the alpha book size might have been different!
			todayData.calculatePNL(rt, bookSize, m_priceCalcId, m_openPriceDataId, m_highPriceDataId, m_lowPriceDataId, &m_intradayPriceDataIds, m_currDayPriceCalcId);

			if (didOpitmise) {

				if (yesterdayNoOpt) {
					todayNoOpt->data().yesterday = yesterdayNoOpt->dataPtr(); /// new CalcData(yesterdayNoOpt->data());
					todayNoOpt->data().yesterday->yesterday = nullptr; /// we don't want to have an infinite chain of copying!
					//todayNoOpt->data().calculateValueFromShares(rt, m_priceCalcId, m_currDayPriceCalcId, true);
					todayNoOpt->data().calculatePNL(rt, bookSize, m_priceCalcId, "", "", "", nullptr, m_currDayPriceCalcId);
				}

				if (calcStats) {
					todayNoOpt->data().calculateStats(rt, bookSize);

					std::string ltStatsId = today->uuid() + "_pre_opt";
					todayNoOpt->data().cumStats = getLTStats(rt, todayData.config, ltStatsId, todayData.universe->name(), "pre_opt_portfolio");
					todayNoOpt->data().cumStats->add(todayNoOpt->data().stats);

					if (rt.isLastDi())
						todayNoOpt->data().cumStats->finalise();
				}
			}
		}

		/// Recalculate stats without the cost (this is for the raw)
		if (calcStats) {
			todayData.calculateStats(rt, bookSize);

			/// These will be the RAW portfolio stats
			std::string ltStatsId = today->uuid() + "_raw";

			todayData.cumStats = getLTStats(rt, todayData.config, ltStatsId, todayData.universe->name(), "raw_portfolio");
			todayData.cumStats->add(today->stats());

			if (rt.isLastDi())
				todayData.cumStats->finalise();
		}

		/// Copy them over to the RAW alpha. This is essentially the RAW
		/// portfolio WITHOUT the cost!
		todayPortRaw->alpha->data().deepCopy(todayData);
		todayPortRaw->alpha->data().yesterday = nullptr; /// RAW Portfolio does NOT need this (we'll get a sharing violation)

		/// If there is yesterday's data then factor in the costs ... 
		if (yesterday) {
			todayData.factorSlippage(rt, m_priceCalcId, m_slippageDataId, 0.1f);
			//todayData.factorImpact(rt, m_priceCalcId, m_impactDataId);
			if (!m_borrowCostId.empty())
				todayData.factorBorrowCost(rt, m_priceCalcId, m_borrowCostId);
			todayData.factorTradingCost(rt, m_priceCalcId);

			if (!m_dividendDataId.empty())
				todayData.factorDividendCost(rt, m_dividendDataId, m_dividendTax);
		}

		/// Recalculate the stats WITH the cost this time around ...
		if (calcStats) {
			todayData.calculateStats(rt, bookSize);

			std::string ltStatsId = today->uuid();

			todayData.cumStats = getLTStats(rt, todayData.config, ltStatsId, todayData.universe->name(), "portfolio");
			todayData.cumStats->add(today->stats());

			if (rt.isLastDi())
				todayData.cumStats->finalise();

			//Statistics tmp = todayData.stats;
			//tmp.pnl = todayData.cumStats->pnl;
			//PTrace("FINAL PNL\n%s", tmp.toString(true, true, todayData.stats.pnl));
		}

		todayPort->data = todayPort->alpha->dataPtr();
		todayPortRaw->data = todayPortRaw->alpha->dataPtr();

		rt.dataRegistry->setData(Portfolio::genId(rt.di), new AlphaResultData(nullptr, todayPort, 0, DataDefinition()));
		rt.dataRegistry->setData(Portfolio::genRawId(rt.di), new AlphaResultData(nullptr, todayPortRaw, 0, DataDefinition()));

		if (didOpitmise) {
			todayNoOpt->data().calculateStats(rt, bookSize);
			todayPortNoOpt->data = todayPortNoOpt->alpha->dataPtr();
			rt.dataRegistry->setData(Portfolio::genNoOptId(rt.di), new AlphaResultData(nullptr, todayPortNoOpt, 0, DataDefinition()));
		}
	}

	void Portfolio::tick(const RuntimeData& rt) {
		PROFILE_SCOPED();

		PTrace("Portfolio::tick");

		/// Clear Portfolio data from 2 days ago ... we don't need it anymore ...
		clearDataForDay(*rt.dataRegistry, rt.di - 2);

		calculateAlpha(rt);
		calculatePortfolio(rt);

		if (m_enableCheckpoint && m_checkPointDay && rt.di == (rt.diEnd - m_checkPointDay)) {
			saveCheckpoint(rt);
		}
	}

	void Portfolio::prepare(RuntimeData& rt) {
		/// create empty data for all the days of the simulation ...
		PTrace("Preparing empty portfolio buffers");

		for (IntDay di = 0; di <= rt.diEnd; di++)
			rt.dataRegistry->setData(Portfolio::genId(di), nullptr);

		loadCheckpoint(rt);
	}

	bool Portfolio::optimise(const RuntimeData& rt, float bookSize, CalcData& data) {
		if (!m_doOptimise)
			return false;

		PROFILE_SCOPED();

		Poco::Timestamp startTime;

		if (!m_optimiser) {
			const ConfigSection& portConfig = parentConfig()->portfolio();
			const ConfigSection* optSec = portConfig.getChildSection("Optimise");

			if (!optSec)
				return false;

			m_optimiser = helper::ShLib::global().createComponent<IOptimiser>(*optSec, rt.dataRegistry);

			if (!m_optimiser) {
				Warning("OPT", "Unable to create optimiser!");
				return false;
			}
		}

		bool didOptimise = data.optimise(m_optimiser.get(), rt, bookSize);

		float seconds = (float)startTime.elapsed() * 0.001f * 0.001f;

		if (seconds > 5.0f) {
			PDebug("[%s] Optimiser took: %0.2hf seconds", Application::instance()->appOptions().simInstanceName, seconds);
		}

		return didOptimise;
	}

	void Portfolio::clearDataForDay(IDataRegistry& dr, IntDay di) {
		dr.clearData(Portfolio::genId(di));
		dr.clearData(Portfolio::genRawId(di));
		dr.clearData(Portfolio::genNoOptId(di));
	}

	void Portfolio::finalise(RuntimeData& rt) {
		PTrace("Clearing all portfolio buffers");

		for (IntDay di = 0; di <= rt.diEnd; di++) {
			clearDataForDay(*rt.dataRegistry, di);
		}

		m_optimiser = nullptr;
	}

	//////////////////////////////////////////////////////////////////////////
	/// Checkpoint related
	//////////////////////////////////////////////////////////////////////////

	std::string Portfolio::getCheckpointFilename() const {
		Poco::Path configPath(parentConfig()->filename());
		
		std::string configFilename = parentConfig()->portfolio().getString("checkpointFilename", "");
		if (!configFilename.empty())
			configPath = Poco::Path(parentConfig()->defs().resolve(configFilename, nullptr));

		configPath.setExtension("CHECKPOINT");
		std::string filename = configPath.toString();
		return filename;
	}

	io::InputStream& Portfolio::readResult(const RuntimeData& rt, AlphaResult& r, io::InputStream& is, size_t& level) {
		ASSERT(r.alpha, "Invalid result alpha!");
		r.alpha->data().read(is);

		auto& cdata = r.alpha->data();
		m_ltStatsMap[r.alpha->uuid()] = cdata.cumStats;
		cdata.cumStats->uuid = r.alpha->uuid();
		cdata.di = rt.getDateIndex(cdata.stats.date);

		ASSERT(cdata.di > 0 && cdata.di < rt.diEnd, "CHECKPOINT - UUID: " << r.alpha->uuid() << " - Invalid date: " << cdata.stats.date);

		cdata.universe = rt.dataRegistry->getUniverse(cdata.cumStats->universeId);
		ASSERT(cdata.universe, "CHECKPOINT - Unable to load universe id: " << cdata.cumStats->universeId);

		size_t numChildren = 0;
		std::string id = "numChildren-" + Util::cast(level);
		is.read(id.c_str(), &numChildren);

		if (numChildren) {
			level++;
			r.childAlphas.resize(numChildren);
			for (auto i = 0; i < numChildren; i++) {
				std::string uuid;
				PESA_READ_VAR(is, uuid);
				r.childAlphas[i].alpha = std::make_shared<ComboResult>(uuid, r.alpha->config());
				readResult(rt, r.childAlphas[i], is, level);
			}
		}

		/// Get the size of the universe 
		size_t universeSize = cdata.universe->size(rt.diEnd);
		size_t existingSize = cdata.alphaNotionals.size();

		if (universeSize > existingSize) {
			PInfo("Universe size has changed from checkpoint state from %z => %z. Resizing arrays to match ...", existingSize, universeSize);

			float defValue = std::nanf("");

			for (size_t ii = existingSize; ii < universeSize; ii++) {
				cdata.rawAlpha.push_back(defValue);
				cdata.alphaNotionals.push_back(0.0f);
				cdata.alphaShares.push_back(defValue);
				cdata.alphaPnls.push_back(defValue);
				cdata.alphaCosts.push_back(defValue);
				cdata.alphaReturns.push_back(defValue);
				cdata.alphaPrice.push_back(defValue);
				cdata.alphaPrevPrice.push_back(defValue);
			}
		}

		return is;
	}

	io::OutputStream& Portfolio::writeResult(const RuntimeData& rt, AlphaResult& r, io::OutputStream& os, size_t& level) {
		std::string uuid = r.alpha->uuid();
		PESA_WRITE_VAR(os, uuid);
		r.alpha->data().write(os);

		size_t numChildren = r.childAlphas.size();
		std::string id = "numChildren-" + Util::cast(level);
		os.write(id.c_str(), numChildren);

		if (numChildren) {
			level++;
			for (auto i = 0; i < numChildren; i++) {
				writeResult(rt, r.childAlphas[i], os, level);
			}
		}

		return os;
	}

	bool Portfolio::saveCheckpoint(const RuntimeData& rt) {
		PDebug("Saving checkpoint on day: %d [Date = %u, diStart = %d, diEnd = %d, CP-Day = %d]", rt.di, rt.dates[rt.di], rt.diStart, rt.diEnd, m_checkPointDay);

		auto filename					= getCheckpointFilename();
		io::OutputStreamPtr stream		= nullptr;
		io::OutputStreamPtr s_alpha		= nullptr;
		io::OutputStreamPtr s_rawPort	= nullptr;
		io::OutputStreamPtr s_port		= nullptr;
		io::OutputStreamPtr s_preOptPort= nullptr;
		int64_t	timestamp				= 0;
		io::MongoClientPtr mongoCl		= nullptr;

		Poco::Timestamp configTS		= m_config.config()->getModifiedTimestamp();
		timestamp						= configTS.epochMicroseconds();

		if (!m_checkpointToDB) {
			stream						= io::BasicOutputStream::createBinaryWriter(filename);
			s_alpha						= s_rawPort = s_port = s_preOptPort = stream;
		}
		else {
			ASSERT(!m_portUuid.empty(), "To save checkpoint to a DB the portfolio must have a valid uuid value");

			//std::string dbName			= parentConfig()->dbName();
			//auto mstream				= std::make_shared<io::OutputMongoStream>(s_checkpointCollName, m_portUuid, nullptr, dbName.c_str());
			//auto alpha					= std::make_shared<io::OutputMongoStream>(s_checkpointCollName, m_portUuid + "_alpha", nullptr, dbName.c_str());
			//auto rawPort				= std::make_shared<io::OutputMongoStream>(s_checkpointCollName, m_portUuid + "_rawPort", nullptr, dbName.c_str());
			//auto port					= std::make_shared<io::OutputMongoStream>(s_checkpointCollName, m_portUuid + "_port", nullptr, dbName.c_str());
			//auto preOptPort				= std::make_shared<io::OutputMongoStream>(s_checkpointCollName, m_portUuid + "_preOptPort", nullptr, dbName.c_str());

			std::string dbName			= parentConfig()->dbName();
			auto mstream				= std::make_shared<io::MongoFileOutputStream>(m_portUuid, s_checkpointCollName.c_str(), false, dbName.c_str());
			auto alpha					= std::make_shared<io::MongoFileOutputStream>(m_portUuid + "_alpha", s_checkpointCollName.c_str(), false, dbName.c_str());
			auto rawPort				= std::make_shared<io::MongoFileOutputStream>(m_portUuid + "_rawPort", s_checkpointCollName.c_str(), false, dbName.c_str());
			auto port					= std::make_shared<io::MongoFileOutputStream>(m_portUuid + "_port", s_checkpointCollName.c_str(), false, dbName.c_str());
			auto preOptPort				= std::make_shared<io::MongoFileOutputStream>(m_portUuid + "_preOptPort", s_checkpointCollName.c_str(), false, dbName.c_str());

			//mstream->replace()			= true;
			//alpha->replace()			= true;
			//rawPort->replace()			= true;
			//port->replace()				= true;
			//preOptPort->replace()		= true;

			//mstream->isUpdate()			= false;
			//alpha->isUpdate()			= false;
			//rawPort->isUpdate()			= false;
			//port->isUpdate()			= false;
			//preOptPort->isUpdate()		= false;

			mongoCl 					= std::make_shared<io::MongoClient>(dbName.c_str());

			mstream->mongoCl()			= mongoCl.get();
			alpha->mongoCl()			= mongoCl.get();
			rawPort->mongoCl()			= mongoCl.get();
			port->mongoCl()				= mongoCl.get();
			preOptPort->mongoCl()		= mongoCl.get();

			//mstream->write("_id",		m_portUuid);
			//alpha->write("_id",			m_portUuid + "_alpha");
			//rawPort->write("_id",		m_portUuid + "_rawPort");
			//port->write("_id",			m_portUuid + "_port");
			//preOptPort->write("_id",	m_portUuid + "_preOptPort");

			stream						= mstream;
			s_alpha 					= alpha;
			s_rawPort 					= rawPort;
			s_port 						= port;
			s_preOptPort 				= preOptPort;
		}

		DataInfo dinfo;
		dinfo.noAssertOnMissingDataLoader = true;

		auto rawPortData				= rt.dataRegistry->get<AlphaResultData>(nullptr, Portfolio::genRawId(rt.di), &dinfo);
		auto noOptPortData				= rt.dataRegistry->get<AlphaResultData>(nullptr, Portfolio::genNoOptId(rt.di), &dinfo);
		auto portData					= rt.dataRegistry->get<AlphaResultData>(nullptr, Portfolio::genId(rt.di), &dinfo);
		auto alphaData					= rt.dataRegistry->get<AlphaResultData>(nullptr, IAlpha::genId(rt.di), &dinfo);

		int version = s_checkpointVersion;
		stream->write("version", version);
		stream->write("timestamp", timestamp);

		IntDay di = rt.di;
		stream->write("di", di);

		ConfigSectionPVec childStrats;
		parentConfig()->portfolio().getChildStrategies(childStrats, true);
		size_t numChildStrats = childStrats.size();

		stream->write("numChildStrats", numChildStrats);

		for (size_t i = 0; i < numChildStrats; i++) {
			const auto* stratConfig = childStrats[i];
			const auto& workingDir = stratConfig->getRequired("workingDir");
			const auto& filename = stratConfig->getRequired("filename");
			Poco::Path configPath(workingDir);
			configPath.append(filename);

			std::string configFilename = configPath.toString();

			Poco::File configFile(configFilename);
			Poco::Timestamp configTS = configFile.getLastModified();
			stream->write("stratFilename", configFilename);
			stream->write("stratTimestamp", (size_t)configTS.epochMicroseconds());
		}

		PDebug("Checkpoint writing alphaData");
		size_t level = 0;
		writeResult(rt, *alphaData->dataPtr(), *s_alpha.get(), level);

		PDebug("Checkpoint writing RAW portfolio data");
		level = 0;
		writeResult(rt, *rawPortData->dataPtr(), *s_rawPort.get(), level);

		PDebug("Checkpoint writing FINAL portfolio data");
		level = 0;
		writeResult(rt, *portData->dataPtr(), *s_port.get(), level);

		bool hasNoOptPort = true;
		if (!noOptPortData)
			hasNoOptPort = false;

		stream->write("hasNoOptPort", hasNoOptPort);
		if (hasNoOptPort) {
			PDebug("Checkpoint writing PRE-OPT portfolio data");

			level = 0;
			writeResult(rt, *noOptPortData->dataPtr(), *s_preOptPort.get(), level);

			/// Flush the pre-opt stream
			s_preOptPort->flush();
		}

		PDebug("Checkpoint FLUSHING!");

		/// Flush all the other streams ...
		stream->flush();
		s_alpha->flush();
		s_rawPort->flush();
		s_port->flush();

		PDebug("Checkpoint saved!");

		return true;
	}

	bool Portfolio::loadCheckpoint(RuntimeData& rt) {
		/// If we're running with the force option then don't load any checkpoint ... let it run normally
		if (rt.appOptions.force)
			return false;

		/// if the "checkpoint" flag is set to false then don't even attempt to load the checkpoint
		if (!m_enableCheckpoint)
			return false; 

		if (!m_checkpointToDB) {
			auto filename = getCheckpointFilename();
			Poco::File file(filename);

			if (!file.exists() || file.getSize() == 0) 
				return false;
		}

		io::InputStreamPtr stream		= nullptr;
		io::InputStreamPtr s_alpha		= nullptr;
		io::InputStreamPtr s_rawPort	= nullptr;
		io::InputStreamPtr s_port		= nullptr;
		io::InputStreamPtr s_preOptPort	= nullptr;
		io::MongoClientPtr mongoCl		= nullptr;
		bool loadedFromDB				= false;

		if (!m_checkpointToDB) {
			auto filename 				= getCheckpointFilename();
			stream 						= io::BasicInputStream::createBinaryReader(filename);
			s_alpha 					= s_rawPort = s_port = s_preOptPort = stream;

			PInfo("Loading checkpoint: %s ...", filename);
		}
		else {
			ASSERT(!m_portUuid.empty(), "To load checkpoint from a DB the portfolio must have a valid uuid value");

			std::string dbName			= parentConfig()->dbName();
			//auto mstream				= std::make_shared<io::InputMongoStream>(s_checkpointCollName, m_portUuid, dbName.c_str());
			//auto alpha					= std::make_shared<io::InputMongoStream>(s_checkpointCollName, m_portUuid + "_alpha", dbName.c_str());
			//auto rawPort				= std::make_shared<io::InputMongoStream>(s_checkpointCollName, m_portUuid + "_rawPort", dbName.c_str());
			//auto port					= std::make_shared<io::InputMongoStream>(s_checkpointCollName, m_portUuid + "_port", dbName.c_str());
			//auto preOptPort				= std::make_shared<io::InputMongoStream>(s_checkpointCollName, m_portUuid + "_preOptPort", dbName.c_str());
			auto mstream				= std::make_shared<io::MongoFileInputStream>(m_portUuid, s_checkpointCollName.c_str(), false, dbName.c_str());
			auto alpha					= std::make_shared<io::MongoFileInputStream>(m_portUuid + "_alpha", s_checkpointCollName.c_str(), false, dbName.c_str());
			auto rawPort				= std::make_shared<io::MongoFileInputStream>(m_portUuid + "_rawPort", s_checkpointCollName.c_str(), false, dbName.c_str());
			auto port					= std::make_shared<io::MongoFileInputStream>(m_portUuid + "_port", s_checkpointCollName.c_str(), false, dbName.c_str());
			auto preOptPort				= std::make_shared<io::MongoFileInputStream>(m_portUuid + "_preOptPort", s_checkpointCollName.c_str(), false, dbName.c_str());

			if (!mstream->isValid() || !alpha->isValid() || !rawPort->isValid() || !port->isValid()) {
				PInfo("Checkpoint load from DB failed. Running full strategy ...");
				return false;
			}
			else {
				stream					= mstream;
				s_alpha					= alpha;
				s_rawPort				= rawPort;
				s_port					= port;
				s_preOptPort			= preOptPort;
				loadedFromDB			= true;

				PInfo("Loading checkpoint: %s [%s] ...", m_portUuid, m_portIID);
			}
		}

		DataInfo dinfo;
		dinfo.noAssertOnMissingDataLoader = true;

		int version = 0;
		stream->read("version", &version);

		if (version != s_checkpointVersion) {
			PInfo("Checkpoint version: %d - Latest version: %d. Mismatched checkpoint will not be used!", version, s_checkpointVersion);
			return false;
		}

		int64_t timestamp = 0;
		stream->read("timestamp", &timestamp);

		Poco::Timestamp configTS = m_config.config()->getModifiedTimestamp();
		Poco::Timestamp checkpointTS(timestamp);
		Poco::DateTime cpdt(checkpointTS);
		Poco::DateTime cfgdt(configTS);

		if ((checkpointTS < configTS || checkpointTS == 0 || configTS == 0) && !m_checkpointIgnoreFileTime) {
			PWarning("Config file has been modified. Checkpoint timestamp: %s - File timestamp: %s. NOT USING!", Util::dateTimeToStr(Poco::DateTime(checkpointTS)), Util::dateTimeToStr(Poco::DateTime(configTS)));
			return false;
		}

		IntDay di = rt.diStart;
		stream->read("di", &di);

		ConfigSectionPVec childStrats;
		parentConfig()->portfolio().getChildStrategies(childStrats, true);
		size_t numChildStrats = childStrats.size();
		size_t numChildStratsCP = 0;

		stream->read("numChildStrats", &numChildStratsCP);

		if (numChildStrats != numChildStratsCP) {
			PInfo("Checkpoint child strat count: %z - Current config count: %z. Mismatched. Re-running!", numChildStratsCP, numChildStrats);
			return false;
		}

		for (size_t i = 0; i < numChildStrats; i++) {
			const auto* stratConfig = childStrats[i];
			const auto& workingDir = stratConfig->getRequired("workingDir");
			const auto& filename = stratConfig->getRequired("filename");
			Poco::Path configPath(workingDir);
			configPath.append(filename);

			std::string configFilename = configPath.toString();
			std::string configFilenameCP;
			stream->read("stratFilename", &configFilenameCP);

			if (configFilename != configFilenameCP) {
				PInfo("Child strategy #%z has been changed from: %s => %s. NOT USING!", i, configFilename, configFilenameCP);
				return false;
			}

			Poco::File configFile(configFilename);
			Poco::Timestamp configTS = configFile.getLastModified();

			int64_t timestamp;
			stream->read("stratTimestamp", &timestamp);

			Poco::Timestamp configTSCP(timestamp);
			if (configTSCP < configTS) {
				PWarning("Config file has been modified [%s]. Checkpoint timestamp: %s - File timestamp: %s. NOT USING!", configFilename, 
					Util::dateTimeToStr(Poco::DateTime(configTSCP)), Util::dateTimeToStr(Poco::DateTime(configTS)));
				return false;
			}
		}

		auto todayPort = AlphaResultPtr(new AlphaResult());
		auto todayPortRaw = AlphaResultPtr(new AlphaResult());
		auto todayAlpha = AlphaResultPtr(new AlphaResult());

		std::string uuid;

		PESA_READ_VAR((*s_alpha.get()), uuid);
		todayAlpha->alpha = std::make_shared<ComboResult>(uuid, parentConfig()->portfolio());
		size_t level = 0;
		readResult(rt, *todayAlpha.get(), *s_alpha.get(), level);

		PESA_READ_VAR((*s_rawPort.get()), uuid);
		todayPortRaw->alpha = std::make_shared<ComboResult>(uuid, parentConfig()->portfolio());
		level = 0;
		readResult(rt, *todayPortRaw.get(), *s_rawPort.get(), level);

		PESA_READ_VAR((*s_port.get()), uuid);
		todayPort->alpha = std::make_shared<ComboResult>(uuid, parentConfig()->portfolio());
		level = 0;
		readResult(rt, *todayPort.get(), *s_port.get(), level);

		bool hasNoOptPort = false;
		stream->read("hasNoOptPort", &hasNoOptPort);

		if (hasNoOptPort) {
			PESA_READ_VAR((*s_preOptPort.get()), uuid);
			auto todayPortNoOpt = AlphaResultPtr(new AlphaResult());
			todayPortNoOpt->alpha = std::make_shared<ComboResult>(uuid, parentConfig()->portfolio());
			level = 0;
			readResult(rt, *todayPortNoOpt.get(), *s_preOptPort.get(), level);

			rt.dataRegistry->setData(Portfolio::genNoOptId(di), new AlphaResultData(nullptr, todayPortNoOpt, 0, DataDefinition()));
		}

		//CalcData& rawPort = todayPortRaw->alpha->state();
		//CalcData& port = todayPort->alpha->state();
		//CalcData& alpha = todayAlpha->alpha->state();

		/// We resume simulation from the next day ...
		rt.diStart = di + 1;
		//ASSERT(rt.diStart > 0 && rt.diStart < rt.diEnd, "Invalid start date: " << rawPort.stats.date << " in the checkpoint: " << filename);

		/// And set the di data so that it can be found in "yesterday"
		rt.dataRegistry->setData(Portfolio::genId(di), new AlphaResultData(nullptr, todayPort, 0, DataDefinition()));
		rt.dataRegistry->setData(Portfolio::genRawId(di), new AlphaResultData(nullptr, todayPortRaw, 0, DataDefinition()));
		rt.dataRegistry->setData(IAlpha::genId(di), new AlphaResultData(nullptr, todayAlpha, 0, DataDefinition()));

		PInfo("Checkpoint loaded!");

		return true;
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IPipelineComponent* createPortfolio(const pesa::ConfigSection& config) {
	return new pesa::Portfolio((const pesa::ConfigSection&)config);
}
