/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// PosWriter.cpp
///
/// Created on: 09 Nov 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include <unordered_map>
#include "Core/Math/Stats.h"

#include "Poco/FileStream.h"
#include "Poco/File.h"
#include "Poco/String.h"
#include "Poco/Thread.h"

#include "Core/IO/BasicStream.h"

#include "Helper/DB/MongoStream.h"
#include "Helper/DB/MongoFileStream.h"

#include "Core/Lib/Profiler.h"

namespace pesa {
	typedef std::shared_ptr<Poco::FileOutputStream> FOSPtr;

	////////////////////////////////////////////////////////////////////////////
	/// PosWriter - Writes the positions on a daily basis to some file on 
	/// the disk	
	////////////////////////////////////////////////////////////////////////////
	class PosWriter : public IPipelineComponent {
	private:
		bool					m_doWrite = false;			/// Whether we're going to write the positions to the disk or not
		bool					m_writeDaily = false;		/// Write daily values or not 
		std::string				m_dir;						/// The base directory where to dump the positions
		std::string				m_figiDataId = "secData.figi"; /// The data which we use for FIGI
		std::string				m_tradeTickerDataId = "secData.ticker"; /// The data which we use for trade ticker

		bool					m_writeDB = false;			/// Write position data to the DB
		bool					m_writeBinary = false;		/// Write binary position data to the file
		bool					m_writeFile = true;			/// Write positions to the file
		bool					m_writePortfolio = false;	/// Write the portfolio positions
		bool					m_writeAlpha = false;		/// Write the alpha positions

		float					m_maxDelta = 0.5f;
		float					m_optMaxTurnover = 0.0f;

		float					m_maxStockTolerance = 0.012f;
		int						m_maxStocks = 0;
		float					m_dailyTvrTolerance = 0.15f;
		float					m_maxTvrToleranceUpper = 0.05f;
		float					m_maxTvrToleranceLower = 0.13f;

		Statistics				m_lastStats;
		io::MongoClientPtr		m_mongoCl = nullptr;

		void					writePosDB(const RuntimeData& rt);
		void					writePosDB(const RuntimeData& rt, const AlphaResult& ar, std::string iid);

		void					writePosData(const RuntimeData& rt, const std::string& dbName, io::ObjectId& id, IntDay di, const char* suffix, const FloatVec& data);

		void					writeFile(const RuntimeData& rt, IAlphaPtr alpha, IAlphaPtr rawAlpha, IntDay di);
		void					writeDB(const RuntimeData& rt, IAlphaPtr alpha, IAlphaPtr rawAlpha, IntDay di);

		io::MongoClient*		mongoCl(const std::string& dbName);

	public:
								PosWriter(const ConfigSection& config);
		virtual 				~PosWriter();

		////////////////////////////////////////////////////////////////////////////
		/// IPipelineComponent overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			tick(const RuntimeData& rt);
		virtual void 			finalise(RuntimeData& rt);
	};

	PosWriter::PosWriter(const ConfigSection& config) : IPipelineComponent(config, "PosWriter") {
		PTrace("Created PosWriter!");

		auto portConfig = parentConfig()->portfolio();
		const ConfigSection* posConfig = portConfig.getChildSection("Pos");

		if (!posConfig) {
			PWarning("Unable to find a <Pos/> section in the config. PosWriter will be DISABLED!");
			m_doWrite = false;
			return;
		}

		std::string maxDeltaStr = portConfig.getString("maxDelta", "");

		if (!maxDeltaStr.empty())
			m_maxDelta = Util::percent(maxDeltaStr);

		const ConfigSection* optConfig = portConfig.getChildSection("Optimise");
		if (optConfig && optConfig->getBool("optimise", false)) {
			m_optMaxTurnover = optConfig->get<float>("maxTurnover", 0.0f);
		}

		m_doWrite				= posConfig->getBool("write", m_doWrite);
		m_writeDaily			= posConfig->getBool("daily", m_writeDaily);
		m_writeDB				= posConfig->getBool("writeDB", m_writeDB);
		m_writeBinary			= posConfig->getBool("writeBinary", m_writeBinary);
		m_writeFile				= posConfig->getBool("writeFile", m_writeFile);
		m_figiDataId			= posConfig->getString("figiDataId", m_figiDataId);
		m_tradeTickerDataId 	= posConfig->getString("tradeTickerDataId", m_tradeTickerDataId);

		m_writePortfolio		= posConfig->getBool("portfolio", m_writePortfolio);
		m_writeAlpha			= posConfig->getBool("alpha", m_writeAlpha);

		m_maxStockTolerance 	= posConfig->get<float>("maxStockTolerance", m_maxStockTolerance);
		m_maxStocks 			= posConfig->get<int>("maxStocks", m_maxStocks);
		m_dailyTvrTolerance 	= posConfig->get<float>("dailyTvrTolerance", m_dailyTvrTolerance);
		m_maxTvrToleranceUpper 	= posConfig->get<float>("maxTvrToleranceUpper", m_maxTvrToleranceUpper);
		m_maxTvrToleranceLower	= posConfig->get<float>("maxTvrToleranceLower", m_maxTvrToleranceLower);

		if (m_doWrite) {
			m_dir = posConfig->getRequired("dir");
			m_dir = parentConfig()->defs().resolve(m_dir, nullptr);

			if (m_dir.find("/") == std::string::npos && m_dir.find("\\") == std::string::npos) {
				Poco::Path configPath(parentConfig()->filename());
				configPath.pushDirectory(m_dir);
				m_dir = configPath.makeParent().toString();
			}
		}

	}

	PosWriter::~PosWriter() {
		PTrace("Deleting PosWriter!");
	}

	io::MongoClient* PosWriter::mongoCl(const std::string& dbName) {
		if (!m_mongoCl)
			m_mongoCl = std::make_shared<io::MongoClient>(dbName.c_str());
		return m_mongoCl.get();
	}

	void PosWriter::writePosData(const RuntimeData& rt, const std::string& dbName, io::ObjectId& id, IntDay di, const char* suffix, const FloatVec& data) {

		//if (di == rt.diStart) { 
		//	io::OutputMongoStream os(std::string("POS_") + std::string(suffix), id, nullptr, dbName.c_str());
		//	/// Replace the document
		//	os.isUpdate() = false;
		//	os.replace() = true;
		//	os.write("_id", id);
		//	os.flush();
		//}

		const size_t numRetries = 3;
		bool didWrite = false;
		size_t tryId = 0;

		while (!didWrite && tryId++ <= numRetries) {
			try {
				std::string sid = id.to_string() + "_" + Util::cast<int>(rt.dates[rt.di]);

				//if (!m_writeBinary || (!m_writeFile && m_writeDB)) 
				{
					io::OutputMongoStream os(std::string("POS_") + std::string(suffix), sid, nullptr, dbName.c_str());

					os.isUpdate() = false;
					os.replace() = true;
					os.mongoCl() = mongoCl(dbName);

					os.write("iid", id);
					os.write("date", (int)rt.dates[rt.di]);
					os.write("data", (const void*)&data[0], data.size() * sizeof(float));

					os.flush();
				}
				//else {
				//	/// OK, we write this to the file now
				//	Poco::Path path(m_dir);
				//	path.append(id.to_string());
				//	path.append(std::string("POS_") + suffix);

				//	Poco::File dir(path);

				//	if (!dir.exists()) {
				//		PInfo("Creating POS Dir for dumping positions: %s", path.toString());
				//		dir.createDirectories();
				//	}

				//	path.append(sid);
				//	auto os = io::BasicOutputStream::createBinaryWriter(path.toString());
				//	//io::MongoFileOutputStream os(sid, "POS", dbName.c_str());
				//	os->write(sid.c_str(), (const char*)&data[0], data.size() * sizeof(float));
				//	os->flush();
				//}

				didWrite = true;
			}
			catch (const std::exception& e) {
				//Util::reportException(e);
				PError("Error while writing positions: %s to DB [Date: %u, Retry: %z] - %s", std::string(suffix), rt.dates[di], tryId, std::string(e.what()));
			}
			catch (...) {
				PError("Unknown error while writing positions: %s to DB [Date: %u, Retry: %z]", std::string(suffix), rt.dates[di], tryId);
			}

			if (!didWrite) {
				/// release the connection ... we'll get it again!
				m_mongoCl = nullptr; 
				Poco::Thread::sleep((long)2000);
			}
		}

		ASSERT(didWrite, "Unable to write positions to the DB: " << suffix << ". Fatal application error. Aborting ...");
	}

	void PosWriter::writePosDB(const RuntimeData& rt, const AlphaResult& ar, std::string iid) {
		for (const auto& car : ar.childAlphas)
			writePosDB(rt, car, car.alpha->config().getString("iid", ""));

		//std::string iid = parentConfig()->portfolio().getRequired("iid");
		//ASSERT(!iid.empty() && iid != "MyPort", "Invalid iid for writing positions to DB: " << iid);

		iid = iid.empty() ? ar.alpha->config().getString("iid", "") : iid;

		/// If there is no iid then we don't do anything
		if (iid.empty())
			return;

		io::ObjectId id(iid);
		std::string dbName = parentConfig()->dbName();
		const CalcData& data = *(ar.data.get());

		writePosData(rt, dbName, id, rt.di, "Notionals", data.alphaNotionals);
		writePosData(rt, dbName, id, rt.di, "Prices", data.alphaSharesCalcPrice);
		writePosData(rt, dbName, id, rt.di, "Shares", data.alphaShares);

		/// On the last frame we write the global information
		if (rt.isLastDi()) {
			io::OutputMongoStream os("POS_Info", id, nullptr, dbName.c_str());
			os.isUpdate() = false;
			os.replace() = true;

			/// rt.diStart can be different in case of checkpoints!
			IntDate diStart = rt.getDateIndex(rt.startDate);
			os.write("_id", id);
			os.writeArray<int>("dates", *(reinterpret_cast<const IntVec*>(&rt.dates)), (size_t)diStart, (size_t)(rt.diEnd + 1));

			size_t universeSize = data.universe->size(rt.di);
			StringVec tickers(universeSize), uuids(universeSize);

			for (size_t ii = 0; ii < universeSize; ii++) {
				Security sec = data.universe->security(rt.di, (Security::Index)ii);
				tickers[ii] = sec.tickerStr();
				uuids[ii] = sec.uuidStr();
			}

			os.writeArray("tickers", tickers);
			os.writeArray("cfigis", uuids);
			os.write("bookSize", data.stats.bookSize);

			Poco::Timestamp ts;
			os.write("genTimestamp", ts);
			os.write("version", Application::instance()->version());

			os.flush();
		}
	}

	void PosWriter::writePosDB(const RuntimeData& rt) {
		if (!m_writeDB)
			return;

		DataInfo dinfo;
		dinfo.noAssertOnMissingDataLoader = true;

		if (m_writePortfolio) {
			const AlphaResultData* result = rt.dataRegistry->get<AlphaResultData>(nullptr, IAlpha::genId(rt.di, "Portfolio"), &dinfo);
			writePosDB(rt, *(result->dataPtr()), parentConfig()->portfolio().getRequired("iid"));
		}
		if (m_writeAlpha) {
			const AlphaResultData* result = rt.dataRegistry->get<AlphaResultData>(nullptr, IAlpha::genId(rt.di), &dinfo);
			writePosDB(rt, *(result->dataPtr()), "");
		}
	}

	void PosWriter::writeDB(const RuntimeData& rt, IAlphaPtr alpha, IAlphaPtr rawAlpha, IntDay di) {
		std::string iid = parentConfig()->portfolio().getRequired("iid");
		ASSERT(!iid.empty() && iid != "MyPort", "Invalid iid for writing positions to DB: " << iid);

		IntDate date = rt.dates[di];
		io::ObjectId id(iid);
		std::string dbName = parentConfig()->dbName();

		io::OutputMongoStream os("Positions", id, nullptr, dbName.c_str());
		os.isUpdate() = true;
		os.replace() = false;

		FloatVec& alphaNotionals = alpha->data().alphaNotionals;
		os.write("_id", id);
		os.replaceArray<float>(Util::cast(date).c_str(), alphaNotionals);

		//os.write(Util::cast(date).c_str(), alphaValues);
		os.flush();
	}

	void PosWriter::writeFile(const RuntimeData& rt, IAlphaPtr alpha, IAlphaPtr rawAlpha, IntDay di) {
		PROFILE_SCOPED();
		const auto& data = alpha->data();
		const auto& alphaUuid = alpha->uuid();

		Poco::Path path(m_dir);
		Poco::File dir(path);

		if (!dir.exists()) {
			PInfo("Creating POS Dir for dumping positions: %s", path.toString());
			dir.createDirectories();
		}

		//IntDay diSim = di - rt.diStart;
		IntDate diDate = rt.dates[di];
		std::string fileName = alphaUuid + "_" + Util::cast(diDate);

		const StringData* figiData = rt.dataRegistry->stringData(nullptr, m_figiDataId);
		ASSERT(figiData, "Unable to load FIGI data: " << m_figiDataId);

		//const StringData* tradeTickerData = rt.dataRegistry->stringData(nullptr, m_tradeTickerDataId);
		//ASSERT(tradeTickerData, "Unable to load Trade ticker data: " << m_tradeTickerDataId);

		/// On the last day we change the name ...
		if (rt.isLastDi(di))
			fileName = alphaUuid;

		path.append(fileName);
		path.setExtension("pos");

		PTrace("Opening alpha PNL stream: %s", path.toString());

		FOSPtr stream = std::make_shared<Poco::FileOutputStream>(path.toString());

		std::string tolerancesStr;
		Poco::format(tolerancesStr, "maxStockTolerance = %0.2hf, maxStocks = %d, maxTvrToleranceUpper = %0.2hf, maxTvrToleranceLower = %0.2hf, dailyTvrTolerance = %0.2hf, maxTvr = %0.2hf",
			m_maxStockTolerance, (int)m_maxStocks, m_maxTvrToleranceUpper, m_maxTvrToleranceLower, m_dailyTvrTolerance, m_optMaxTurnover * 100.0f);

		(*stream) << "# Universe              : " << data.universe->name() << "\n";
		(*stream) << "# BookSize              : " << data.stats.bookSize << "\n";
		(*stream) << "# Date                  : " << Util::cast(diDate) << "\n";
		(*stream) << "# Gen. Timestamp        : " << Util::dateTimeToStr(Poco::DateTime()) << "\n";
		(*stream) << "# Version               : " << Application::instance()->version() << "\n";
		(*stream) << "# $ Long / $ Short      : " << alpha->stats().longNotional << " / " << alpha->stats().shortNotional << " [Delta = " <<
			std::abs(alpha->stats().longNotional) - std::abs(alpha->stats().shortNotional) << ", MaxDelta = " << m_maxDelta << "]\n";
		(*stream) << "# Lng / Shrt / Liqd     : " << alpha->stats().longCount << " / " << alpha->stats().shortCount << " / " << alpha->stats().liquidateCount << " [TVR = " << 
			alpha->stats().tvr << "]\n";

		/// Deliberately un-aligned!
		(*stream) << "# Y - $ Long / $ Short  : " << m_lastStats.longNotional << " / " << m_lastStats.shortNotional << " [Delta = " <<
			std::abs(m_lastStats.longNotional) - std::abs(m_lastStats.shortNotional) << ", MaxDelta = " << m_maxDelta << "]\n";
		(*stream) << "# Y - Lng / Shrt / Liqd : " << m_lastStats.longCount << " / " << m_lastStats.shortCount << " / " << m_lastStats.liquidateCount << " [TVR = " <<
			m_lastStats.tvr << "]\n";
		(*stream) << "# Tolerances            : " << tolerancesStr << "\n";

		std::string headerStr;
		Poco::format(headerStr, "#%11s | %12s | %8s | %12s | %12s | %12s | %12s | %12s | %12s", std::string("BB-Ticker"), std::string("CFIGI"), std::string("Shares"), std::string("$ Alloc"), 
			std::string("$ Price"), std::string("FIGI"), std::string("Trade-TKR"), std::string("POpt-$ Alloc"), std::string("POpt-$ Delta"));
		(*stream) << headerStr << "\n";

		(*stream) << "# Market                : " << parentConfig()->defs().market() << "\n";

		auto numStocks = data.alphaShares.size();
		auto* universe = alpha->universe();

		for (auto ii = 0; ii < numStocks; ii++) {
			auto alphaShare = data.alphaShares[ii];
			auto alphaNotional = data.alphaNotionals[ii];
			auto alphaPrice = data.alphaSharesCalcPrice[ii];
			auto sec = universe->security(rt.di, ii);

			if (universe->isValid(rt.di, ii) && !std::isnan(alphaShare)) {
				std::string str;
				const std::string& figi = figiData->getValue(0, (size_t)sec.index);
				const std::string& tradeTicker = sec.tickerStr(); /// tradeTickerData->getValue(0, (size_t)sec.index);
				float preOptAlphaNotional = std::nanf("");

				if (rawAlpha) {
					preOptAlphaNotional = rawAlpha->data().alphaNotionals[ii];
				}

				Poco::format(str, "%12s | %s | %8d | %12.4hf | %12.4hf | %s | %12s | %12.4hf | %12.4hf", sec.bbTickerStr(), sec.uuidStr(), (int)alphaShare, (float)alphaNotional, (float)alphaPrice,
					figi, tradeTicker, (float)preOptAlphaNotional, (float)(preOptAlphaNotional - alphaNotional));
				//(*stream) << sec.uuidStr() << " " << (int)alphaShare << "\n";
				(*stream) << str << "\n";
			}
		}

		stream = nullptr;
	}

	void PosWriter::tick(const RuntimeData& rt) {
		/// Do nothing if not enabled!
		if (!m_doWrite)
			return;

		auto result = rt.dataRegistry->get<AlphaResultData>(nullptr, IAlpha::genId(rt.di, "Portfolio"));
		if (!result) {
			PWarning("No positions generated for day: %u", rt.getDate(rt.di));
			return;
		}

		auto portAlpha = result->dataPtr()->alpha;

		/// If we are not writing daily stats then we just write on the last day
		if (!m_writeDaily && !rt.isLastDi(rt.di)) {
			if (portAlpha)
				m_lastStats = portAlpha->stats();
			return;
		}

		PROFILE_SCOPED();

        DataInfo dinfo;
        dinfo.noAssertOnMissingDataLoader = true;
		auto preOptResult = rt.dataRegistry->get<AlphaResultData>(nullptr, IAlpha::genId(rt.di, "NoOptPortfolio"), &dinfo); 
		auto preOptPortAlpha = preOptResult ? preOptResult->dataPtr()->alpha : nullptr;

		if (m_writeFile && !m_writeBinary)
			writeFile(rt, portAlpha, preOptPortAlpha, rt.di);
		
		if (m_writeDB)
			writePosDB(rt);

		m_lastStats = portAlpha->stats();
	}

	void PosWriter::finalise(RuntimeData& rt) {
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IPipelineComponent* createPosWriter(const pesa::ConfigSection& config) {
	return new pesa::PosWriter((const pesa::ConfigSection&)config);
}
