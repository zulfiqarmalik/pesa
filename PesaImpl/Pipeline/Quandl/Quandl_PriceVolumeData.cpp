/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Quandl_PriceVolumeData.cpp
///
/// Created on: 7 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "./Quandl.h"
#include "Framework/Framework.h"
#include "Framework/Helper/FileDataLoader.h"
#include "Poco/String.h"

namespace pesa {

	////////////////////////////////////////////////////////////////////////////
	/// Data registry implementation
	////////////////////////////////////////////////////////////////////////////
	class Quandl_PriceVolumeData : public IDataLoader {
	private:
		typedef std::unordered_map<std::string, FloatMatrixDataPtr> FloatMatrixDataMap;

		FloatMatrixDataMap 		m_dataMap;
		Datasets				m_datasets;
		bool 					m_dataCachePrepared = false;

	public:
								Quandl_PriceVolumeData(const ConfigSection& config);
		virtual 				~Quandl_PriceVolumeData();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame);
		virtual void 			preBuild(IDataRegistry& dr, const DataCacheInfo& dci);
		virtual void 			postBuild(IDataRegistry& dr, const DataCacheInfo& dci);

		////////////////////////////////////////////////////////////////////////////
		/// IUniverse overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			combine(FloatMatrixData& thisAlpha, IUniversePtr other, FloatMatrixData& otherAlpha) {}
	};

	Quandl_PriceVolumeData::Quandl_PriceVolumeData(const ConfigSection& config)
		: IDataLoader(config, "QNDL_PVOL") {
		CTrace("Created Quandl_PriceVolumeData: %s", config.toString(true));
	}

	Quandl_PriceVolumeData::~Quandl_PriceVolumeData() {
		CTrace("Deleting Quandl_PriceVolumeData!");
	}

	void Quandl_PriceVolumeData::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		datasets.add(Dataset("Price", {
			DataValue("open", DataType::kFloat, sizeof(float), nullptr, DataFrequency::kDaily, DataFlags::kNone),
			DataValue("close", DataType::kFloat, sizeof(float), nullptr, DataFrequency::kDaily, DataFlags::kNone),
			DataValue("high", DataType::kFloat, sizeof(float), nullptr, DataFrequency::kDaily, DataFlags::kNone),
			DataValue("low", DataType::kFloat, sizeof(float), nullptr, DataFrequency::kDaily, DataFlags::kNone),
			DataValue("volume", DataType::kFloat, sizeof(float), nullptr, DataFrequency::kDaily, DataFlags::kNone),
			DataValue("splitRatio", DataType::kFloat, sizeof(float), nullptr, DataFrequency::kDaily, DataFlags::kNone),
		}));

		datasets.add(Dataset("AdjPrice", {
			DataValue("adjOpen", DataType::kFloat, sizeof(float), nullptr, DataFrequency::kDaily, DataFlags::kNone),
			DataValue("adjClose", DataType::kFloat, sizeof(float), nullptr, DataFrequency::kDaily, DataFlags::kNone),
			DataValue("adjHigh", DataType::kFloat, sizeof(float), nullptr, DataFrequency::kDaily, DataFlags::kNone),
			DataValue("adjLow", DataType::kFloat, sizeof(float), nullptr, DataFrequency::kDaily, DataFlags::kNone),
			DataValue("adjVolume", DataType::kFloat, sizeof(float), nullptr, DataFrequency::kDaily, DataFlags::kNone),
		}));

		m_datasets = datasets;
	}

	void Quandl_PriceVolumeData::build(IDataRegistry& dr, const DataCacheInfo& dci, DataFrame& frame) {
		DataPtr& data = frame.data;
		size_t& dataOffset = frame.dataOffset;
		size_t& dataSize = frame.dataSize;
		
		/// The data must exist!
		std::string dataDefId = dci.def.toString(dci.ds);
		ASSERT(m_dataMap.find(dataDefId) != m_dataMap.end(), "Unknown data def sent for building: " << dataDefId);

		CTrace("Returning data for: %s - (%d)", dataDefId, (int)dci.di);

		auto diOffset = dci.di - dci.diStart;
		data = m_dataMap[dataDefId];
		dataOffset = diOffset * data->cols() * data->itemSize();
		dataSize = data->cols() * data->itemSize();
	}

	inline float getValue(const StringVec& data, size_t index) {
		if (index >= data.size())
			return std::nanf("0");
		return Util::cast<float>(data[index]);
	}

	void Quandl_PriceVolumeData::preBuild(IDataRegistry& dr, const DataCacheInfo& dci) {
		/// The data cache has already been prepared. Since preBuild is called for each of the datasets
		/// and we construct the cache in one go, we don't need to do this again!
		if (m_dataCachePrepared ||
			dci.di == Constants::s_invalidDay ||
			dci.diStart == Constants::s_invalidDay ||
			dci.diEnd == Constants::s_invalidDay)
			return;

		SecurityDataPtr securitiesPtr = dr.get<SecurityData>("Universe.allSecurities");
		ASSERT(securitiesPtr, "Unable to get all securities for the main universe!");
		SecurityVecPtr securities = securitiesPtr->vector();
		ASSERT(securities && securities->size(), "Unable to load Universe.allSecurities vector!");

		CTrace("Num Securities: %z", securities->size());

		Quandl q;
		IntDate startDate = dci.rt.dates[dci.diStart];
		IntDate endDate = dci.rt.dates[dci.diEnd];
		std::string strStartDate = Util::intDateToStr(startDate, "-");
		std::string strEndDate = Util::intDateToStr(endDate, "-");
		size_t numDays = (size_t)(dci.diEnd - dci.diStart) + 1;

		CTrace("Number of days to update: %z (%d - %d)", numDays, (int)startDate, (int)endDate);

		if (!numDays)
			return;

		if (!m_dataMap.size()) {
			for (auto& ds : m_datasets) {
				for (auto& dv : ds.values) {
					std::string dataDefId = dv.definition.toString(ds);
					CTrace("Initialising data def: %s", dataDefId);
					ASSERT(m_dataMap.find(dataDefId) == m_dataMap.end(), "Data already defined in the data definition map: " << dataDefId);
					m_dataMap[dataDefId] = FloatMatrixDataPtr(new FloatMatrixData(dv.definition, numDays, securities->size(), false));
				}
			}
		}

		/// get the data in the start to end date range
		for (size_t i = 0; i < securities->size(); i++) {
			const Security& security = (*securities)[i];

			ASSERT(security.index != Security::s_invalidIndex, "Invalid index for security: " << security.toString());
			ASSERT(security.index >= 0 && security.index < securities->size(), "Security index out of range: " << security.index << " - Security: " << security.toString());

			std::string result = q.get("WIKI/" + security.symbolStr(), "asc", strStartDate, strEndDate);
			CTrace("Result length [%s]: %z", security.symbolStr(), result.length());

			helper::FileDataLoader loader(",", false, true);
			loader.loadBuffer(result);

			std::string lineStr;
			StringVec data = loader.read(&lineStr);
			size_t lineNumber = 0;
			Security::Index ii = security.index;

			while (data.size() && ++lineNumber) {
				IntDate date 		= Util::parseDate(data[0]);
				IntDay di 			= dci.rt.getDateIndex(date);

				if (di < dci.diStart || di > dci.diEnd)
					continue;

				CTrace("Precaching-%u: %s - %d", ii, security.toString(), di);

				(*(m_dataMap["Price.open"]))(di, ii) 			= getValue(data, 1);
				(*(m_dataMap["Price.high"]))(di, ii) 			= getValue(data, 2);
				(*(m_dataMap["Price.low"]))(di, ii) 			= getValue(data, 3);
				(*(m_dataMap["Price.close"]))(di, ii) 			= getValue(data, 4);
				(*(m_dataMap["Price.volume"]))(di, ii) 			= getValue(data, 5);
				(*(m_dataMap["Price.splitRatio"]))(di, ii) 		= getValue(data, 7);
				(*(m_dataMap["AdjPrice.adjOpen"]))(di, ii) 		= getValue(data, 8);
				(*(m_dataMap["AdjPrice.adjHigh"]))(di, ii) 		= getValue(data, 9);
				(*(m_dataMap["AdjPrice.adjLow"]))(di, ii) 		= getValue(data, 10);
				(*(m_dataMap["AdjPrice.adjClose"]))(di, ii) 	= getValue(data, 11);
				(*(m_dataMap["AdjPrice.adjVolume"]))(di, ii) 	= getValue(data, 12);

				data = loader.read(&lineStr);
			}
		}

		m_dataCachePrepared = true;
	}

	void Quandl_PriceVolumeData::postBuild(IDataRegistry& dr, const DataCacheInfo& dci) {
	}

} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createQuandl_PriceVolumeData(const pesa::ConfigSection& config) {
	return new pesa::Quandl_PriceVolumeData((const pesa::ConfigSection&)config);
}
