/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Quandl_UniverseBuilder.cpp
///
/// Created on: 7 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Framework/Helper/FileDataLoader.h"
#include "Framework/Helper/BasicUniverseBuilder.h"

#include "Poco/String.h"

namespace pesa {

	static const size_t 		s_maxSector = 32;
	static const size_t 		s_maxIndustry = 32;

	////////////////////////////////////////////////////////////////////////////
	/// Data registry implementation
	////////////////////////////////////////////////////////////////////////////
	class Quandl_UniverseBuilder : public helper::BasicUniverseBuilder {
	private:
		size_t 					m_numExistingSecurities = 0;

	public:
								Quandl_UniverseBuilder(const ConfigSection& config);
		virtual 				~Quandl_UniverseBuilder();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual DataPtr 		create(IDataRegistry& dr, const Dataset& ds, const DataDefinition& def);
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci, DataPtr& data, size_t& dataOffset, size_t& dataSize);
		virtual void 			preBuild(IDataRegistry& dr, const DataCacheInfo& dci);
		virtual void 			buildSecurities(IDataRegistry& dr, const DataCacheInfo& dci, DataPtr& data, size_t& dataOffset, size_t& dataSize);
		virtual void 			buildDailySecurities(IDataRegistry& dr, const DataCacheInfo& dci, DataPtr& data, size_t& dataOffset, size_t& dataSize);

		////////////////////////////////////////////////////////////////////////////
		/// IUniverse overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			combine(FloatMatrixData& thisAlpha, IUniversePtr other, FloatMatrixData& otherAlpha) {}
	};

	Quandl_UniverseBuilder::Quandl_UniverseBuilder(const ConfigSection& config)
		: helper::BasicUniverseBuilder(config, "UNIV_BLDR") {
		CTrace("Created Quandl_UniverseBuilder: %s", config.toString(true));
	}

	Quandl_UniverseBuilder::~Quandl_UniverseBuilder() {
		CTrace("Deleting Quandl_UniverseBuilder!");
	}

	DataPtr Quandl_UniverseBuilder::create(IDataRegistry& dr, const Dataset& ds, const DataDefinition& def) {
		return helper::BasicUniverseBuilder::create(dr, ds, def);
	}

	void Quandl_UniverseBuilder::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		datasets.add(Dataset("Universe", {
			DataValue("allSecurities", DataType::kSecurityData, sizeof(Security),
					THIS_DATA_FUNC(Quandl_UniverseBuilder::buildSecurities),
					DataFrequency::kStatic, DataFlags::kAlwaysLoad | DataFlags::kUpdatesInOneGo),

			DataValue("dailySecurities", DataType::kInt, sizeof(Security::Index),
					THIS_DATA_FUNC(Quandl_UniverseBuilder::buildDailySecurities),
					DataFrequency::kDaily, DataFlags::kNone),
		}));
	}


	void Quandl_UniverseBuilder::build(IDataRegistry& dr, const DataCacheInfo& dci, DataPtr& data, size_t& dataOffset, size_t& dataSize) {
	}

	void Quandl_UniverseBuilder::buildDailySecurities(IDataRegistry& dr, const DataCacheInfo& dci, DataPtr& data, size_t& dataOffset, size_t& dataSize) {
		SecurityVecPtr securities = toArray();
		SecurityIndexVectorData* dailyData = new SecurityIndexVectorData(dci.def, securities->size());
		SecurityIndexVectorPtr dailyDataVector = dailyData->vector();

		ASSERT(dailyDataVector->size() == securities->size(), "Size of the daily data vector does not match. Expecting: "
				<< securities->size() << ", Found: " << dailyDataVector->size());

		size_t index = 0;
		for (const Security& security : *securities) {
			(*dailyDataVector)[index] = security.index;
			index++;
		}

		data = SecurityIndexVectorDataPtr(dailyData);
		dataOffset = 0;
		dataSize = data->dataSize();
	}

	void Quandl_UniverseBuilder::preBuild(IDataRegistry& dr, const DataCacheInfo& dci) {
		helper::BasicUniverseBuilder::preBuild(dr, dci);

		if (dci.def.toString(dci.ds) == "Universe.allSecurities")
			m_numExistingSecurities = m_securities.size();
	}

	void Quandl_UniverseBuilder::buildSecurities(IDataRegistry& dr, const DataCacheInfo& dci, DataPtr& data, size_t& dataOffset, size_t& dataSize) {
		const RuntimeData& rt = dci.rt;
		IntDate date = rt.dates[dci.di];
		const ConfigSection& config = IPipelineComponent::config<ConfigSection>();

		std::string dataPath = config.dataPath();
		dataPath = config.config()->defs().resolve(dataPath, date);

		ASSERT(!dataPath.empty(), "Invalid data path given!");

		size_t existingSecCount = m_securities.size();

		if (!helper::BasicUniverseBuilder::hasFileBeenUsed(dataPath)) {
			CTrace("Loading file: %s - %s", dci.def.toString(dci.ds), dataPath);
			helper::FileDataLoader loader(dataPath, ",", false, false);
//			StringVec header = loader.header();

			std::string lineStr;
			StringVec data = loader.read(&lineStr);
			size_t lineNumber = 0;

			while (data.size() && ++lineNumber) {
	//			ASSERT(data.size() == header.size(), "Error " << lineNumber << ": Expecting number of items on line: " << header.size() << ", found: " << data.size() << ". Full Line: " << lineStr);
				std::string symbol 	= data[0];
				std::string name 	= data[1];
				size_t pos = name.find("(");

				if (pos != std::string::npos && pos > 1)
					name = name.substr(0, pos - 1);

				pos = symbol.rfind("/");
				if (pos != std::string::npos && pos < symbol.length() - 1)
					symbol = symbol.substr(pos + 1, symbol.length());

				std::string uuid 	= makeUuid(symbol, name);

				// first check if the security exists
				Security::Index index = find(uuid);

				if (index == Security::s_invalidIndex)
					index = add(Security(uuid, name, symbol));

				CDebug("UUID: %s (%u)", uuid, m_securities[uuid].index);

				data = loader.read(&lineStr);
			}

			helper::BasicUniverseBuilder::addParsedFile(dataPath);
			CInfo("Number of new securities: %z", m_securities.size() - existingSecCount);
		}

		if (existingSecCount < m_securities.size()) {
			SecurityVecPtr securities = toArray();
			data = SecurityDataPtr(new SecurityData(securities, dci.def));
			dataOffset = m_numExistingSecurities * sizeof(SecurityData);
			dataSize = data->dataSize() - dataOffset;
		}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IPipelineComponent* createQuandl_UniverseBuilder(const pesa::ConfigSection& config) {
	return new pesa::Quandl_UniverseBuilder((const pesa::ConfigSection&)config);
}
