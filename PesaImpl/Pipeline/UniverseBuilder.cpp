/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// UniverseBuilder.cpp
///
/// Created on: 7 Apr 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include "Framework/Helper/FileDataLoader.h"
#include "Framework/Helper/BasicUniverseBuilder.h"

#include "Poco/String.h"

namespace pesa {

	static const size_t 		s_maxSector = 32;
	static const size_t 		s_maxIndustry = 32;

	////////////////////////////////////////////////////////////////////////////
	/// Data registry implementation
	////////////////////////////////////////////////////////////////////////////
	class UniverseBuilder : public helper::BasicUniverseBuilder {
	public:
								UniverseBuilder(const ConfigSection& config);
		virtual 				~UniverseBuilder();

		////////////////////////////////////////////////////////////////////////////
		/// IDataLoader overrides
		////////////////////////////////////////////////////////////////////////////
		virtual DataPtr 		create(IDataRegistry& dr, const Dataset& ds, const DataDefinition& def);
		virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);
		virtual void 			build(IDataRegistry& dr, const DataCacheInfo& dci);
		virtual void 			buildSecurities(IDataRegistry& dr, const DataCacheInfo& dci);
		virtual void 			buildDailySecurities(IDataRegistry& dr, const DataCacheInfo& dci);

		////////////////////////////////////////////////////////////////////////////
		/// IUniverse overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			combine(FloatMatrixData& thisAlpha, IUniversePtr other, FloatMatrixData& otherAlpha) {}
	};

	UniverseBuilder::UniverseBuilder(const ConfigSection& config)
		: helper::BasicUniverseBuilder(config, "UNIV_BLDR") {
		CTrace("Created UniverseBuilder: %s", config.toString(true));
	}

	UniverseBuilder::~UniverseBuilder() {
		CTrace("Deleting UniverseBuilder!");
	}

	DataPtr UniverseBuilder::create(IDataRegistry& dr, const Dataset& ds, const DataDefinition& def) {
		if (ds.name == "Universe") {
			if (std::string(def.name) == "allSecurities")
				return SecurityDataPtr(new SecurityData(nullptr, def));
		}

		return nullptr;
	}

	void UniverseBuilder::initDatasets(IDataRegistry& dr, Datasets& datasets) {
		datasets.add(Dataset("Universe", {
			DataValue("allSecurities", kCustom, sizeof(Security), THIS_DATA_FUNC(UniverseBuilder::buildSecurities), kUniversal, kDataFlags_NeedsHistoryDuringUpdate),
//			DataValue("marketCap", kInt, sizeof(int), THIS_DATA_FUNC(UniverseBuilder::buildDailySecurities), kDaily),
//			DataDefinition("ticker", kString, Security::s_maxTicker),
//			DataDefinition("name", kString, Security::s_maxName),
//			DataDefinition("industry", kString, s_maxIndustry),
//			DataDefinition("sector", kString, s_maxSector),
//			DataDefinition("marketCap", kFloat, sizeof(float)),
		}));
	}

	void UniverseBuilder::build(IDataRegistry& dr, const DataCacheInfo& dci) {
	}

	void UniverseBuilder::buildDailySecurities(IDataRegistry& dr, const DataCacheInfo& dci) {
	}

	void UniverseBuilder::buildSecurities(IDataRegistry& dr, const DataCacheInfo& dci) {
		CInfo("Building %s", dci.def.toString(dci.ds));
		const RuntimeData& rt = dci.rt;
		IntDate date = rt.dataDates[dci.di];
		const ConfigSection& config = IPipelineComponent::config<ConfigSection>();

		std::string dataPath = config.dataPath();
		dataPath = config.config()->defs().resolve(dataPath, date);

		ASSERT(!dataPath.empty(), "Invalid data path given!");

		CTrace("Loading file: %s", dataPath);
		helper::FileDataLoader loader(dataPath, "\"([^\"]*)\"", true, true);
		StringVec header = loader.header();

		std::string lineStr;
		StringVec data = loader.read(&lineStr);
		size_t lineNumber = 1;
		size_t existingSecCount = m_securities.size();

		while (data.size() && ++lineNumber) {
			ASSERT(data.size() == header.size(), "Error " << lineNumber << ": Expecting number of items on line: " << header.size() << ", found: " << data.size() << ". Full Line: " << lineStr);
			std::string symbol 	= data[0];
			std::string name 	= data[1];
			float lastSale 		= Util::cast<float>(data[2]);
			float marketCap 	= Util::amount(data[3]);
			IntDate ipoYear 	= Util::cast<IntDate>(data[4]);
			std::string sector 	= Poco::toLower(data[5]);
			std::string industry= Poco::toLower(data[6]);
			std::string squote 	= data[7];

			std::string uuid 	= makeUuid(symbol, name);

			// first check if the security exists
			Security::Index index = find(uuid);

			if (index == Security::s_invalidIndex)
				index = add(Security(uuid, name, symbol));

//			CDebug("UUID: %s", uuid);

			data = loader.read(&lineStr);
		}

		CInfo("Number of new securities: %z", m_securities.size() - existingSecCount);

		if (dci.finalise) {
			SecurityVecPtr securities = toArray();
			SecurityData data(securities, dci.def);
			dr.write(dci, &data);
		}
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IPipelineComponent* createUniverseBuilder(const pesa::ConfigSection& config) {
	return new pesa::UniverseBuilder((const pesa::ConfigSection&)config);
}
