/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// AlphaGen.cpp
///
/// Created on: 30 Aug 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "AlphaGen.h"
#include "Poco/String.h"
#include "Poco/Glob.h"

#include "Helper/DB/MongoStream.h"

#include "Poco/StringTokenizer.h"
#include "Poco/String.h"
#include "Poco/FileStream.h"

#include "Core/Lib/Profiler.h"
#include <functional>

using namespace pesa::io;
using namespace bsoncxx;
using namespace mongocxx;

namespace pesa {
	ConfigGenInfo::ConfigGenInfo(const Config* baseConfig_, const ConfigSectionPtrVec& expressions_)
		: baseConfig(baseConfig_)
		, expressions(expressions_) {
		market = baseConfig->defs().market();
		ASSERT(!market.empty(), "Unable to get market from the config!");
	}

	ConfigGenInfo::~ConfigGenInfo() {
	}

	//////////////////////////////////////////////////////////////////////////

	std::string AlphaGen::s_batchCollName				= "Batches";
	std::string AlphaGen::s_aconfigCollName				= "AlphaConfigs";
	const std::string AlphaGen::s_expressionsSecName 	= "Expressions";
	const std::string AlphaGen::s_operationsSecName 	= "Operations";
	const std::string AlphaGen::s_datasetsSecName 		= "Datasets";
	const std::string AlphaGen::s_lateImportsSecName	= "LateImports";
	const std::string AlphaGen::s_lateImportSecName		= "LateImport";
	const size_t AlphaGen::s_maxAlphasPerConfig			= 10;
	const size_t AlphaGen::s_minDays					= 10;

	AlphaGen::AlphaGen(const ConfigSection& config) : IPipelineComponent(config, "ALPHA_GEN") {
		PTrace("Created AlphaGen!");

		m_maxThreads = parentConfig()->defs().get<size_t>("maxThreads", m_maxThreads);
		m_maxAlphasPerConfig = parentConfig()->defs().get<size_t>("maxAlphasPerConfig", s_maxAlphasPerConfig);
	}

	AlphaGen::~AlphaGen() {
		PTrace("Deleting AlphaGen!");
	}

	std::string AlphaGen::genUuid(const ConfigSection& exprSec, const std::string& universeId, const std::string& market) {
		typedef std::pair<std::string, std::string> StrPair;
		const auto& map = exprSec.map();
		auto expr = map.find("expr");

		std::string uuid = market + "_" + universeId + "_" + expr->second;
		auto children = exprSec.getAllChildrenOfType("Operation");
		for (auto child : children) {
			auto id = child->id();
			uuid += "_" + id;
		}

		std::vector<StrPair> values;

		for (auto iter : map) {
			const std::string& name = iter.first;

			if (name != "expr" && name != "id" && name != "dataUsed") {
				const std::string& value = iter.second;
				values.push_back(StrPair(name, value));
			}
		}

		std::sort(values.begin(), values.end());

		for (auto iter : values) 
			uuid += "_" + iter.first + "=" + iter.second;

		return uuid;
	}

	void AlphaGen::buildArgs(ConfigArgValueVecVec& allArgs, const ConfigSection& exprSec) {
		const auto& children = exprSec.children();
		ConfigArgVec usedArgs; 

		for (auto* child : children) {
			if (child->name() == "Args") {
				const auto& map = child->map();
				for (auto iter = map.begin(); iter != map.end(); iter++) {
					const auto& name = iter->first;
					const auto& value = iter->second;
					ConfigArg arg;
					arg.name = name;
					arg.values = Util::split(value, "|");
					usedArgs.push_back(arg);
				}
			}
		}

		ConfigArgValueVec arg;
		buildArgs(allArgs, arg, usedArgs, 0, 0);
	}

	void AlphaGen::buildArgs(ConfigArgValueVecVec& allArgs, ConfigArgValueVec& orgArg, ConfigArgVec& usedArgs, size_t i, size_t j) {
		for (; i < usedArgs.size(); i++) {
			const auto& usedArg = usedArgs[i];

			for (; j < usedArg.values.size(); j++) {
				auto arg = orgArg;
				const auto& value = usedArg.values[j];

				arg.push_back(ConfigArgValue(usedArg.name, value));

				if (i < usedArgs.size() - 1)
					buildArgs(allArgs, arg, usedArgs, i + 1, 0);
				else
					allArgs.push_back(arg);
			}
		}
	}

	void AlphaGen::buildDatasets(StringVecVec& allDss, StringVec& orgDss, StringVecVec& usedDss, size_t i, size_t j) {
		for (; i < usedDss.size(); i++) {
			const auto& usedDs = usedDss[i];

			for (; j < usedDs.size(); j++) {
				StringVec dss = orgDss;
				const auto& value = usedDs[j];

				dss.push_back(value);

				if (i < usedDss.size() - 1) 
					buildDatasets(allDss, dss, usedDss, i + 1, 0);
				else
					allDss.push_back(dss);
			}
		}
	}

	void AlphaGen::generateAll(ConfigSectionPtrVec& genExprs, const ConfigSectionPVec& exprsSec, const ConfigSectionCPVec& opsSec, 
		const StringVecVec& allDss, const StringMap* extraProps) {
		for (auto* exprSec : exprsSec) {
			for (const auto& dss : allDss) {
				AlphaGen::genExprsForOps(genExprs, *exprSec, opsSec, dss, extraProps);
			}
		}
	}

	ConfigSectionPtr AlphaGen::genOneExpr(const std::string& orgExpr, const ConfigSection& opSec, const StringVec& dss) {
		ConfigSection* genExpr = new ConfigSection(nullptr);
		std::string expr = orgExpr;
		std::string dataUsed = "";

		for (size_t i = 0; i < dss.size(); i++) {
			const auto& ds = dss[i];
			std::string dataName = Util::split(ds, ".").at(0);
			if (i == dss.size() - 1)
				dataUsed += dataName;
			else
				dataUsed += dataName + "-";
			std::string varName = "$x" + Util::cast<size_t>(i);
			expr = Poco::replace(expr, varName, ds);
		}

		genExpr->set("id", "ExprVM");
		genExpr->set("expr", expr);
		//genExpr->set("dataUsed", dataUsed);

		/// Now get all the operations under the operation section
		const auto& ops = opSec.children();
		for (const auto* op : ops) {
			auto* genOp = new ConfigSection(nullptr, *op);
			genExpr->addChildSection(genOp);
		}

		Trace("ALPHA_GEN", "Resolved expression: %s => %s [Op: %s]", orgExpr, expr, opSec.name());

		return ConfigSectionPtr(genExpr);
	}

	void AlphaGen::genExprsForOps(ConfigSectionPtrVec& genExprs, const ConfigSection& exprSec, const ConfigSectionCPVec& opsSec, 
		const StringVec& dss, const StringMap* extraProps) {
		const auto& expr = exprSec.getRequired("expr");
		ASSERT(!expr.empty(), "Invalid or empty expression specified!");

		ConfigArgValueVecVec allArgs;
		AlphaGen::buildArgs(allArgs, exprSec);

		/// Have at least one dummy arg (if there are none) so that the loop below runs
		if (!allArgs.size())
			allArgs.resize(1);

		/// now we generate a new set of expressions with all the operations added underneath the expression section
		for (const auto* opSec : opsSec) {
			auto genExpr = AlphaGen::genOneExpr(expr, *opSec, dss);
			ASSERT(genExpr, "Unable to generate target expression!");

			for (auto& arg : allArgs) {
				auto exprCopy = std::make_shared<ConfigSection>(nullptr, *genExpr);

				for (auto& argValue : arg)
					exprCopy->set(argValue.first, argValue.second);

				if (extraProps) {
					for (auto& iter : *extraProps) 
						exprCopy->set(iter.first, iter.second);
				}

				genExprs.push_back(exprCopy);
			}
		}
	}

	void AlphaGen::resolveDataset(StringVec& dss, const std::string& dsName, const Config* config) {
		if (dsName.find("*") == std::string::npos &&
			dsName.find("?") == std::string::npos) {
			dss.push_back(dsName);
			return;
		}

		auto dsNameParts = Util::split(dsName, ".");
		const auto& dsDir = dsNameParts[0];
		const auto& defs = config->defs();
		auto rootPath = defs.get("dataCachePath");
		ASSERT(!rootPath.empty(), "Defs section must have a dataCachePath variable defined!");

		DataDefinition def;
		auto path = def.path("SecurityMaster", rootPath, dsDir);

		for (size_t i = 1; i < dsNameParts.size(); i++)
			path.append(dsNameParts[i]);

		std::string searchString = defs.resolve(path.toString(), 0U);
		Debug("ALPHA_GEN", "Searching: %s", searchString);

		/// Now we glob everything
		std::set<std::string> files;
		Poco::Glob::glob(searchString, files);

		//std::cout << "Num files: " << files.size() << std::endl;
		Debug("ALPHA_GEN", "Number of files for dataset %s: %z", dsName, files.size());

		for (const auto& file : files) {
			Poco::Path p(file);
			auto fname = p.getFileName();
			auto pos = fname.find(",");
			if (pos != std::string::npos) 
				fname = fname.substr(0, pos);
			auto ds = dsDir + "." + fname;
			dss.push_back(ds);
		}
	}

	void AlphaGen::getUsedDatasets(StringVecVec& usedDss, const ConfigSection& config, const ConfigSection& datasetsSec) {
		size_t index = 0;

		while (true) {
			std::string varName = "x" + Util::cast<size_t>(index++);
			const auto& varValue = config.getString(varName, "");
			if (varValue.empty())
				break;

			auto dssNames = Util::split(varValue, "|");
			StringVec dss;
			std::string typeFilter = datasetsSec.getString("typeFilter", ",*float,*");

			for (const auto& dssName : dssNames) {
				const auto* dssSec = datasetsSec.getChildSection(dssName);
				ASSERT(dssSec, "Invalid dataset named: " << dssName);

				const auto& dssSecValue = dssSec->getRequired("value");
				const auto& dsNames = Util::split(dssSecValue, "|");
				ASSERT(dsNames.size(), "Empty value for dataset: " << dssName);

				for (const auto& dsName : dsNames) {
					size_t start = dss.size();
					auto fullDsName = dsName;

					if (fullDsName.find("*") != std::string::npos)
						fullDsName += typeFilter;

					AlphaGen::resolveDataset(dss, fullDsName, config.config());

					if (start < dss.size()) {
						auto cstr = Util::combine(dss, ", ", start, dss.size());
						Trace("ALPHA_GEN", "%s => %s", dsName, cstr);
					}
				}

			}

			ASSERT(dss.size(), "Invalid dataset(s) which resolved to empty: " << varValue);
			usedDss.push_back(dss);
		}
	}

	void AlphaGen::parseExprVars(std::string& str, StringMap& args, size_t lineNum) {
		auto cleanVarsStr = str.substr(0, str.length() - 1);
		auto argDefs = Util::split(cleanVarsStr, ",");

		for (auto& argDef : argDefs) {
			auto parts = Util::split(argDef, "=");
			ASSERT(parts.size() == 2, "Malformed variable def: " << str << " @ LINE: " << lineNum);
			auto& argName = parts[0];
			auto& argValue = parts[1];

			args[argName] = argValue;
		}
	}

	void AlphaGen::parseExprString(std::string& str, std::string& expr, StringMap& args, size_t lineNum) {
		auto parts = Util::split(str, "[");
		ASSERT(parts.size() <= 2, "Invalid expression string: " << str << " @ LINE: " << lineNum);
		expr = parts[0];

		if (parts.size() == 2) {
			auto& argsStr = parts[1];
			if (argsStr.empty())
				return;

			AlphaGen::parseExprVars(argsStr, args, lineNum);
		}
	}

	void AlphaGen::loadExpressionsFromFile(Config* config, const ConfigSection* exprSec, const std::string& filename, ConfigSectionPVec& exprSecs) {
		try {
			std::string fullFilename = config->defs().resolve(filename, nullptr);
			Poco::FileInputStream stream(fullFilename);
			static const size_t maxLine = 8192;
			char lineBuffer[maxLine];
			std::string baseName = exprSec->name();
			ConfigSectionPVec currExprs;
			size_t lineNum = 0;

			while (!stream.eof()) {
				stream.getline(lineBuffer, maxLine);

				/// Change to a string object and trim it
				std::string line(lineBuffer);
				line = Util::trim(line);

				lineNum++;

				if (line.empty())
					continue;

				if (line[0] == '@') {
					/// Make a list of current expressions
					currExprs.clear();
					std::string namesStr = line.substr(1);
					StringVec names = Util::split(namesStr, "|");
					ASSERT(names.size(), "[" << lineNum << "] Invalid dataset names: " << namesStr << ". Names should be in the format: @name1 | name2 | name3 ...");

					for (auto& name : names) {
						ConfigSection* dynExpr = new ConfigSection(config);
						std::string exprName = baseName + "." + name;
						dynExpr->name() = exprName;
						currExprs.push_back(dynExpr);
						exprSecs.push_back(dynExpr);
					}

					continue;
				}

				/// Otherwise we just parse the expression ...
				if (line[0] == '/' && line[1] == '/') /// Comment ...
					continue;

				StringMap exprArgs;
				std::string expr;

				parseExprString(line, expr, exprArgs, lineNum);
				ASSERT(!expr.empty(), "Invalid expression at line: " << lineNum);

				ConfigSection newExprSec(config);
				newExprSec.name() = "Expr";
				newExprSec.set("expr", expr);

				if (exprArgs.size()) {
					ConfigSection* exprArgsSec = new ConfigSection(config);
					newExprSec.addChildSection(exprArgsSec);
					exprArgsSec->name() = "Args";

					for (auto iter = exprArgs.begin(); iter != exprArgs.end(); iter++) {
						auto& name = iter->first;
						auto& value = iter->second;

						exprArgsSec->set(name, value);
					}
				} 

				for (auto* dynExpr : currExprs) 
					dynExpr->addChildSection(new ConfigSection(config, newExprSec));
			}
		}
		catch (const Poco::PathNotFoundException& e) {
			Error("PathNotFoundException: %s", e.message());
		}
		catch (const Poco::FileNotFoundException& e) {
			Error("FileNotFoundException: %s", e.message());
		}
		catch (const std::exception& e) {
			Error("std::exception: %s", std::string(e.what()));
		}
	}

	void AlphaGen::resolveDynamicExpressions(Config* config, const ConfigSection* exprSec, ConfigSectionPVec& dynExprs) {
		auto filename = exprSec->getRequired("filename");
		PDebug("Resolving dynamic expressions for section:  %s [Filename: %s]", exprSec->name(), filename);
		loadExpressionsFromFile(config, exprSec, filename, dynExprs);
	}

	void AlphaGen::prepareSection(RuntimeData& rt, Config* pconfig, const ConfigSection* batchSec) {
		// PROFILE_SCOPED();

		auto& defs = pconfig->defs();

		auto* expressionsSec = m_expressionsSec.get(); /// defs.getChildSection(s_expressionsSecName);
		ASSERT(expressionsSec, "Defs does not have an Expressions section which is required by AlphaGen");
		auto& childExpressionSecs = expressionsSec->children();

		const auto* operationsSec = defs.getChildSection(s_operationsSecName);
		ASSERT(operationsSec, "Defs does not have an Operations section which is required by AlphaGen");

		const auto* datasetsSec = defs.getChildSection(s_datasetsSecName);
		ASSERT(datasetsSec, "Defs does not have a Datasets section which is required by AlphaGen");

		auto opsNames = Util::split(pconfig->defs().resolve(batchSec->getString("operations", ""), (Poco::DateTime*)nullptr), "|");
		PTrace("Number of operations: %z", opsNames.size());

		ConfigSectionCPVec opsSec;
		for (const auto& opName : opsNames) {
			auto* op = const_cast<ConfigSection*>(operationsSec->getChildSection(opName));
			ASSERT(op, "Invalid operation named: " << opName);
			op->resolveAllChildren(pconfig->defs(), 0);
			opsSec.push_back(op);
		}

		/// If there are no operations specified ... we create a null one so that the 
		/// loop below runs. The rest of the code can detect this and handle the cases
		/// of NULL operations section
		if (!opsSec.size())
			opsSec.push_back(nullptr);

		StringVecVec usedDss;
		AlphaGen::getUsedDatasets(usedDss, *batchSec, *datasetsSec);

		PDebug("Total used datasets: %z", usedDss.size());

		/// Now we have all the datasets
		StringVec dss;
		StringVecVec allDss;
		std::vector<StringMap> allDssExprProps;

		if (usedDss.size()) {
			AlphaGen::buildDatasets(allDss, dss, usedDss, 0, 0);
			PDebug("Total dataset combinations: %z", allDss.size());
		}

		auto timesStr = batchSec->getString("timesValues", "");
		if (!timesStr.empty()) {
			StringVec times = Util::split(timesStr, "|");
			IntVec itimes;

			for (const auto& time : times)
				itimes.push_back(Util::cast<int>(time));

			allDssExprProps.resize(allDss.size());

			for (size_t i = 0; i < allDss.size(); i++) {
				const auto& adss = allDss[i];
				int maxTime = 0;

				for (size_t j = 0; j < itimes.size(); j++) {
					for (const auto& adssValue : adss) {
						if (adssValue.find(times[j]) != std::string::npos) 
							maxTime = std::max(maxTime, itimes[j]);
					}
				}

				allDssExprProps[i]["maxTime"] = Util::cast(maxTime);
			}
		}

		const std::string& expressionsStr = batchSec->getRequired("expressions");
		StringVec inputExpressions = Util::split(expressionsStr, "|");
		ConfigSectionPVec exprsSec;

		for (const auto& inputExpression : inputExpressions) {
			const auto* exprSec = expressionsSec->getChildSection(inputExpression);
			ASSERT(exprSec, "Unable to find expressions: " << inputExpression);
			const auto& children = exprSec->children();

			for (auto* childExprSec : children) {
				exprsSec.push_back(childExprSec);
			}
		}

		/// The generated expressions
		//ConfigSectionPtrVec genExprs;
		//AlphaGen::generateAll(genExprs, exprsSec, opsSec, allDss);

		//for (auto* exprSec : exprsSec) {
		//	for (const auto& dss : allDss) {
		//		AlphaGen::genExprsForOps(genExprs, *exprSec, opsSec, dss);
		//	}
		//}

		PDebug("Total number of base expressions: %z", exprsSec.size() * allDss.size());

		auto universesStr = batchSec->getRequired("universes");
		auto delay = batchSec->getRequired("delay");
		auto startDate = batchSec->getString("startDate", "");
		auto universes = Util::split(universesStr, "|");

		ASSERT(universes.size(), "Invalid config with no valid universes specified!");

		std::string batchId = batchSec->id();
		createBatch(batchId);

		typedef std::future<int> Future;
		typedef std::list<Future> FutureList;

		FutureList asyncJobs;

		PDebug("Total number of universes: %z", universes.size());
		for (const auto& universeId : universes) {
			PDebug("##########################################################################################");
			PDebug("Universe: %s - %s", batchId, universeId);

			StringVec universeIds;
			universeIds.push_back(universeId);
			auto parenPos = universeId.find("(");

			if (parenPos != std::string::npos) {
				std::string allUniverseId = Poco::trim(universeId);
				std::string parentUniverse = allUniverseId.substr(0, parenPos);
				std::string childUniversesStr = allUniverseId.substr(parenPos + 1, allUniverseId.length() - parenPos - 2); /// Remove the leading and trailing paren as well
				StringVec childUniverses = Util::split(childUniversesStr, ",");
				universeIds.clear();
				universeIds.push_back(parentUniverse);
				universeIds.insert(universeIds.begin() + 1, childUniverses.begin(), childUniverses.end());
			}

			//std::list<std::future> asyncJobs;

			size_t dssIter = 0;
			size_t totalGen = 0;

			while (dssIter < allDss.size()) {
				ConfigSectionPtrVec genExprs;
				const auto& dss = allDss[dssIter];
				const StringMap* extraProps = allDssExprProps.size() ? &allDssExprProps[dssIter] : nullptr;

				for (size_t j = 0; j < exprsSec.size(); j++) {
					auto exprSec = exprsSec[j];
					AlphaGen::genExprsForOps(genExprs, *exprSec, opsSec, dss, extraProps);
				}

				dssIter++;

				//for (size_t i = 0; i < allDss.size(); i++) {

					/// We at least 100 configs worth of data in one go ...
				//	while (genExprs.size() < m_maxAlphasPerConfig * 100 && exprIter < exprsSec.size()) {
						//auto exprSec = exprsSec[exprIter];
						//PTrace("[%s] Expr iterator: %z", batchSec->id(), exprIter);

						//AlphaGen::genExprsForOps(genExprs, *exprSec, opsSec, dss, extraProps);

				//		exprIter++;
				//	}
				//}

				size_t exprIndex = 0;
				while (exprIndex < genExprs.size()) {
					ConfigGenInfo cgi(pconfig, genExprs);
					cgi.delay = delay;
					cgi.startDate = startDate;
					cgi.exprStart = exprIndex;
					cgi.exprEnd = cgi.exprStart + std::min(genExprs.size() - cgi.exprStart, m_maxAlphasPerConfig);
					cgi.universeIds = universeIds;

					PDebug("[Universe: %s - %s] - Generating %z - %z", universeId, batchSec->id(), totalGen + cgi.exprStart, totalGen + cgi.exprEnd);

					/// Increment the iterator
					exprIndex = cgi.exprEnd;

					if (m_maxThreads > 1) {
						/// Wait for one of the threads to finish
						while (asyncJobs.size() >= m_maxThreads) {
							auto& future = asyncJobs.front();
							future.wait();
							asyncJobs.pop_front();
						}

						/// We make a copy since we are running things in a separate thread ...
						ConfigGenInfo cgiCopy = cgi;
						auto future = std::async(std::launch::async, &AlphaGen::generateAndFinalise, this, rt, cgiCopy, batchId);
						asyncJobs.push_back(std::move(future));
					}
					else {
						this->generateAndFinalise(rt, cgi, batchId);
					}
				}

                /// clear all the async jobs so that the configs vector is still in scope ...
                asyncJobs.clear();

				totalGen += genExprs.size();
			}
		}

		/// Wait for all the jobs to finish ...
		asyncJobs.clear();
	}

	int AlphaGen::generateAndFinalise(const RuntimeData& rt, ConfigGenInfo cgi, const std::string& batchId) {
		//std::string dbName = parentConfig()->dbName();
		io::MongoClient mongoCl;

		auto newConfig = generateConfig(rt, mongoCl, cgi, m_batchId.to_string(), batchId);

		if (newConfig) {
			ASSERT(newConfig, "Unable to generate a new config!");
			finaliseConfig(parentConfig(), newConfig.get());

			AlphaGen::queueJob(mongoCl, newConfig, parentConfig());
		}
		else {
			Trace("ALPHA_GEN", "No config generated. Possibly because the config has no alphas at all. Ignoring ...");
		}

		return 0;
	}

	void AlphaGen::prepare(RuntimeData& rt) {
		Config* pconfig = const_cast<Config*>(parentConfig());
		auto& defs = pconfig->defs();

		auto* expressionsSec = defs.getChildSection(s_expressionsSecName);
		ASSERT(expressionsSec, "Defs does not have an Expressions section which is required by AlphaGen");

		m_expressionsSec = std::make_shared<ConfigSection>(*expressionsSec);
		defs.deleteChildSection(s_expressionsSecName);

		expressionsSec = nullptr;

		auto& childExpressionSecs = m_expressionsSec->children();

		/// Resolve the dynamic expressions ...
		ConfigSectionPVec dynamicExprs;

		for (auto i = 0; i < childExpressionSecs.size(); i++) {
			auto* exprSec = childExpressionSecs[i];
			auto filename = exprSec->getString("filename", "");

			if (!filename.empty()) 
				resolveDynamicExpressions(pconfig, exprSec, dynamicExprs);
		}

		if (dynamicExprs.size()) {
			/// Add it to the expressions section!
			for (auto* dynamicExpr : dynamicExprs) {
				m_expressionsSec->addChildSection(dynamicExpr);
			}
		}

		auto& batchSecs = config().children();
		for (auto* batchSec : batchSecs) {
			prepareSection(rt, pconfig, batchSec);
		}
	}

	void AlphaGen::queueJob(io::MongoClient& mongoCl, DBConfigPtr config, const Config* parentConfig) {
		// PROFILE_SCOPED();

		/// The reason for adding a random factor is to make sure that normal priority 
		/// jobs added from different regions get served almost immediately rather than 
		/// having to wait for the firt job to completely finish before launching the next one!
		srand((unsigned int)time(0));
		int rfactor = rand() % 20; 
		config->priority() = (Util::Priority)(Util::kNormal - 10 + rfactor);
		config->market() = parentConfig->defs().market();

		config->save(&mongoCl);

		OutputMongoStream os(AlphaGen::s_batchCollName);

		os.write("filename", parentConfig->filename());

		BsonDocumentBuilder find;

		find << "_id" << io::ObjectId(config->batchId());

		BsonDocumentBuilder update;
		update <<
			"$inc" << open_document <<
				"numJobs" << 1 <<
			close_document;

		options::find_one_and_update opt;
		opt.upsert(false);

		auto coll = mongoCl.coll(AlphaGen::s_batchCollName);
		auto result = coll.find_one_and_update(find.view(), update.view(), opt);

		ASSERT(result, "Unable to increment the numJobs in document: " << config->batchId());
	}

	DBConfigPtr AlphaGen::generateConfig(const RuntimeData& rt, io::MongoClient&, const ConfigGenInfo& cgi, const std::string& batchId, 
		const std::string& globalBatchId) {
		// PROFILE_SCOPED();

		ASSERT(cgi.baseConfig, "Invalid base config given!");
		ASSERT(cgi.exprEnd > cgi.exprStart, "Invalid start/end. Start: " << cgi.exprStart << " - End: " << cgi.exprEnd);

		DBConfigPtr newConfig = DBConfigPtr(new DBConfig(*cgi.baseConfig, batchId));

		/// We don't want the extra modules from this config to muddy the new config that we're generating!!!
		newConfig->pipeline().deleteAllChildren();
		newConfig->modules().deleteAllChildren();

		auto& portConfig = newConfig->portfolio();
		//ASSERT(!portConigf.id().empty(), "Invalid Portfolio entry with an empty ID");

		auto market = newConfig->defs().market();

		/// Now we add alphas to it (remove any existing ones before)
		auto& alphasSec = *(newConfig->portfolio().alphasSec());
		size_t numAdded = 0;
		alphasSec.deleteAllChildrenOfType("Alpha");

		std::string dbName = parentConfig()->dbName();
		io::MongoClient mongoCl(dbName.c_str());
		Poco::DateTime today;
		Poco::Timespan ts(450, 0, 0, 0, 0);
		Poco::Timestamp fixedCreationDate = Util::intToDate(20160101).timestamp(); ///(today - ts).timestamp();

		/// Now we create a an Alphas section
		//ASSERT(alphasSec, "Cannot have a portfolio without an Alphas section!");

		for (size_t i = cgi.exprStart; i < cgi.exprEnd; i++) {
			auto expression = cgi.expressions[i];

			/// Now we create a duplicate of this expressions
			std::string parentUuid = "";

			/// Now we need to generate an ID for this expression
			for (const auto& universeId : cgi.universeIds) {
				auto newExpression = new ConfigSection(newConfig.get(), *expression);
				auto uuid = AlphaGen::genUuid(*newExpression, universeId, market);
				auto exprUuid = uuid; /// We make a copy of the original generated UUID since the UUID used in the DB could change below ...

				if (uuid.length() > 1000) {
					std::hash<std::string> hashFunc;
					size_t uuidHash = hashFunc(uuid);
					std::string uuidPrefix = Util::cast(uuidHash);

					uuid = uuidPrefix + "_" + uuid.substr(0, std::min<int>((int)uuid.length() - 1, 1000));
				}

				/// Use the first one as the parent uuid
				if (parentUuid.empty())
					parentUuid = uuid;
				else 
					newExpression->set("parentUuid", parentUuid);

				newExpression->set("delay", cgi.delay);

				if (!cgi.startDate.empty())
					newExpression->set("startDate", cgi.startDate);
				else
					newExpression->set("startDate", "{$DB_ENDDATE}");

				{
					// PROFILE_SCOPED();
					Trace("ALPHA_GEN", "UUID: %s", uuid);

					io::ObjectId oid;

					/// We try to read the uuid from the database and then try to see whether this alpha has been 
					/// generated before. If it has been generated in the past and the stats saved then we just 
					/// run the alpha from the last run date (-5) and NOW
					{
						// PROFILE_SCOPED();

						io::InputMongoStream readStream(mongoCl, s_aconfigCollName, uuid);
						bool pruned = false;

						if (readStream.isValid()) {
							int endDate = 0;
							//readStream.read("endDate", &endDate);
							readStream.read("pruned", &pruned);
							readStream.read("iid", &oid);
							//ASSERT(endDate, "Invalid start date in the DB for alpha UUID: " << uuid);

							//Poco::DateTime tendDate = Util::intToDate(endDate);
							//Poco::Timespan maxDaysBehind = Poco::Timespan(s_minDays, 0, 0, 0, 0);
							//tendDate = tendDate - maxDaysBehind;

							//auto newStartDate = Util::dateToInt(tendDate);

							/// now we setup the alpha to start at this start date 
							//Trace("ALPHA_GEN", "Changed the start date to: %d", (int)newStartDate);
						}

						/// The alpha has been pruned ... we don't wanna use it
						if (pruned) {
							delete newExpression;
							continue;
						}
					}

					/// Here we try to read the original creationDate of the Alpha. This is useful for ensuring that 
					/// the OUTSAMPLE data for the alpha is collected correctly
					Poco::Timestamp creationDate = fixedCreationDate;
					{
						// PROFILE_SCOPED();

						io::InputMongoStream is(mongoCl, s_aconfigCollName, uuid);
						if (is.isValid()) {
							is.read("creationDate", &creationDate);
						}
					}

					/// OK here we try to add this to expressions database
					newExpression->set("type", "auto");
					newExpression->set("level", "alpha");
					newExpression->set("universeId", universeId);

					io::OutputMongoStream os(s_aconfigCollName, uuid);
					os.mongoCl() = &mongoCl;
					os.isUpdate() = false;
					os.replace() = true;

					Poco::Timestamp updateDate;

					{
						// PROFILE_SCOPED();

						newExpression->name() = "Alpha";
						newExpression->set("uuid", uuid);
						newExpression->set("iid", oid.to_string());
						newExpression->set("universeId", universeId);
						newExpression->set("market", cgi.market);
						//newExpression->set("dataUsed", expression->getString("dataUsed", ""));
						newExpression->set("creationDate", Util::timestampToIntDate(creationDate));
						newExpression->set("gbatchId", globalBatchId);
					}

					{
						// PROFILE_SCOPED();

						newExpression->write(os);

						os.write("iid", oid);
						os.write("lastUpdated", updateDate);
						os.write("universeId", universeId);
						os.write("market", cgi.market);

						/// Set the creation date that might have been read from the document earlier
						os.write("creationDate", creationDate);

						os.flush();
					}
				}

				//////////////////////////////////////////////////////////////////////////

				{
					// PROFILE_SCOPED();

					AUTO_LOCK(m_exprMutex);

					/// Now we set the uuid of this expression and add
					auto iter = m_exprIds.find(exprUuid);
					if (iter != m_exprIds.end()) {
						//ASSERT(iter == m_exprIds.end(), "Non-unique ID for the expression: " << uuid);
						Warning("ALPHA_GEN", "IGNORING non-unique ID for expression: %s", exprUuid);
						delete newExpression;
						continue;
					}

					//newExpression->set("uuid", uuid);
					newExpression->name() = "Alpha";
					alphasSec.addChildSection(newExpression);
					numAdded++;

					m_exprIds[uuid] = true;
				}
			}
		}

		/// If we haven't added anything then don't even bother with the config
		return numAdded ? newConfig : nullptr;
	}

	void AlphaGen::finaliseConfig(const Config* baseConfig, Config* config) {
		/// Ok ... during finalise we need to remove redundant elements from the config and expand the import section ...
		auto& defs = config->defs();
		defs.deleteChildSection(s_expressionsSecName);
		defs.deleteChildSection(s_operationsSecName);
		defs.deleteChildSection(s_datasetsSecName);
		defs.deleteChildSection("Pipeline"); /// We don't need this anymore. This will come from the import!

		defs.map().clear();

		Poco::DateTime dtToday;
		IntDate today = Util::dateToInt(dtToday);
		auto endDate = baseConfig->defs().getString("endDate", Util::cast(today));
		defs.set("endDate", endDate);

		IntDate startDate = baseConfig->defs().get<IntDate>("startDate", 0);
		ASSERT(startDate, "Invalid startDate!");
		defs.set("startDate", Util::cast(startDate));
		defs.set("exactStartDate", "true");

		std::string dbName = baseConfig->defs().getString("dbName", "pesa");
		defs.set("dbName", dbName);
		defs.set("ignoreAlphaDates", "false");
		defs.set("dataCachePath", baseConfig->defs().getRequired("dataCachePath"));

		const ConfigSection* pruneSec = baseConfig->defs().getChildSection("Prune");
		if (pruneSec)
			defs.addChildSection(new ConfigSection(const_cast<Config*>(defs.config()), *pruneSec));

		/// Now we are going to get the imports sorted ...
		/// First of all we are going to clear the existing imports ...
		config->imports().clear();

		/// Then we're going to add the late imports ...
		auto* importsSec = defs.getChildSection(s_lateImportsSecName);
		if (importsSec) {
			auto& importSecsList = importsSec->children();
			for (auto* importSec : importSecsList) {
				if (importSec->name() == s_lateImportSecName) {
					auto* newImportSec = new ConfigSection(config, *importSec);
					newImportSec->name() = "Import";
					config->imports().push_back(newImportSec);
				}
			}
		}

		/// OK, now we can delete the LateImports section from this config
		defs.deleteChildSection(s_lateImportsSecName);
	}

	void AlphaGen::finalise(RuntimeData& rt) {
	}

	void AlphaGen::createBatch(const std::string& batchId) {
		io::OutputMongoStream os(AlphaGen::s_batchCollName);
		io::MongoClient mongoCl;

		os.mongoCl() = &mongoCl;
		os.write("filename", parentConfig()->filename());
		os.write("batchId", batchId);
		os.write("isCancelled", false);

		Poco::Timestamp creationDate;
		os.write("creationDate", creationDate);
		os.write("numJobs", 0); 
		os.write("numFinished", 0);
		os.write("numSuccessful", 0);
		os.write("numFailed", 0);

		os.flush();

		m_batchId = os.insertedId();
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IPipelineComponent* createAlphaGen(const pesa::ConfigSection& config) {
	return new pesa::AlphaGen((const pesa::ConfigSection&)config);
}
