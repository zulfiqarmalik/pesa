/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// AlphaGen.h
///
/// Created on: 30 Aug 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/Framework.h"

#include "DBConfig.h"

namespace pesa {
	struct ConfigArg {
		std::string				name;
		StringVec				values;
	};

	typedef std::shared_ptr<ConfigSection>		ConfigSectionPtr;
	typedef std::vector<ConfigSectionPtr>		ConfigSectionPtrVec;
	typedef std::vector<ConfigArg>				ConfigArgVec;
	typedef std::vector<ConfigArgVec>			ConfigArgVecVec;
	typedef std::pair<std::string, std::string> ConfigArgValue;
	typedef std::vector<ConfigArgValue>			ConfigArgValueVec;
	typedef std::vector<ConfigArgValueVec>		ConfigArgValueVecVec;
	typedef std::vector<StringVec>				StringVecVec;

	struct ConfigGenInfo {
		const Config*			baseConfig = nullptr;		/// The base config which will be used a template for generating the final config(s)
		const
		ConfigSectionPtrVec&	expressions;				/// The list of expressions to be used in the new config
		StringVec 				universeIds;				/// The universe that this config is targetting 
		std::string				delay = "1";				/// What is the delay
		std::string				market;						/// The market that the config is targetting
		std::string				startDate;					/// The starting date of this batch
		size_t					exprStart = 0;				/// The starting expression index in expressions array above. 0 means the start
		size_t					exprEnd = 0;				/// The ending expression index in the expressions array above. 0 resolves to the end of the array

								ConfigGenInfo(const Config* baseConfig_, const ConfigSectionPtrVec& expressions_);
								~ConfigGenInfo();

		//////////////////////////////////////////////////////////////////////////
		/// Inline functions
		//////////////////////////////////////////////////////////////////////////
		inline size_t			numExpressions() const { return exprEnd - exprStart; }
	};

	////////////////////////////////////////////////////////////////////////////
	/// AlphaGen - Executes all the alphas
	////////////////////////////////////////////////////////////////////////////
	class AlphaGen : public IPipelineComponent {
	public:
		static std::string		s_batchCollName;
		static std::string		s_aconfigCollName;

	private:
		static const size_t		s_maxAlphasPerConfig;
		static const size_t		s_minDays;

		size_t					m_maxAlphasPerConfig;		/// What is the max number of alphas we want in each of the configs
		io::ObjectId			m_batchId;					/// The ID of the batch that we created
		std::mutex 				m_exprMutex;				/// Data mutex
		std::unordered_map<std::string, bool> m_exprIds;	/// The uuids generated in this run

		size_t					m_maxThreads = 8;

		ConfigSectionPtr		m_expressionsSec;			/// The expressions section

		void					createBatch(const std::string& batchId);
		static void				queueJob(io::MongoClient& mongoCl, DBConfigPtr config, const Config* parentConfig);
		void					loadExpressionsFromFile(Config* config, const ConfigSection* exprSec, const std::string& filename, ConfigSectionPVec& exprSecs);
		void					resolveDynamicExpressions(Config* config, const ConfigSection* exprSec, ConfigSectionPVec& dynExprs);

		int						generateAndFinalise(const RuntimeData& rt, ConfigGenInfo cgi, const std::string& batchId);

		static void				parseExprVars(std::string& str, StringMap& vars, size_t lineNum);
		static void				parseExprString(std::string& str, std::string& expr, StringMap& vars, size_t lineNum);

	public:
								AlphaGen(const ConfigSection& config);
		virtual 				~AlphaGen();

		//////////////////////////////////////////////////////////////////////////
		/// Some static defs
		//////////////////////////////////////////////////////////////////////////
		static const std::string s_expressionsSecName;
		static const std::string s_operationsSecName;
		static const std::string s_datasetsSecName;
		static const std::string s_lateImportsSecName;
		static const std::string s_lateImportSecName;

		////////////////////////////////////////////////////////////////////////////
		/// IPipelineComponent overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			prepareSection(RuntimeData& rt, Config* pconfig, const ConfigSection* genExprSec);
		virtual void 			prepare(RuntimeData& rt);
		virtual void 			tick(const RuntimeData& rt) {}
		virtual void 			finalise(RuntimeData& rt);

		//////////////////////////////////////////////////////////////////////////
		/// Static functions
		//////////////////////////////////////////////////////////////////////////
		static void				getUsedDatasets(StringVecVec& allDss, const ConfigSection& config, const ConfigSection& datasetsSec);
		static void				resolveDataset(StringVec& dss, const std::string& dsName, const Config* config);
		static void				buildDatasets(StringVecVec& allDss, StringVec& dss, StringVecVec& usedDss, size_t i, size_t j);
		static void				buildArgs(ConfigArgValueVecVec& allArgs, const ConfigSection& exprSec);
		static void				buildArgs(ConfigArgValueVecVec& allArgs, ConfigArgValueVec& orgArg, ConfigArgVec& usedArgs, size_t i, size_t j);
		static std::string		genUuid(const ConfigSection& exprSec, const std::string& universeId, const std::string& market);

		static ConfigSectionPtr	genOneExpr(const std::string& expr, const ConfigSection& opSec, const StringVec& dss);
		static void				genExprsForOps(ConfigSectionPtrVec& genExprs, const ConfigSection& exprSec, const ConfigSectionCPVec& opsSec, 
								const StringVec& dss, const StringMap* extraProps);
		static void				generateAll(ConfigSectionPtrVec& genExprs, const ConfigSectionPVec& exprsSec, const ConfigSectionCPVec& opsSec, 
								const StringVecVec& allDss, const StringMap* extraProps);

		DBConfigPtr				generateConfig(const RuntimeData& rt, io::MongoClient& mongoCl, const ConfigGenInfo& cgi, const std::string& batchId, const std::string& gbatchId);
		void					finaliseConfig(const Config* baseConfig, Config* config);
	};
} /// namespace pesa
