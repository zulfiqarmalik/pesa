/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// DBConfig.cpp
///
/// Created on: 08 Sep 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "DBConfig.h"
#include "Poco/String.h"
#include "Poco/Glob.h"
#include "AlphaGen.h"

using namespace pesa::io;
using namespace bsoncxx;
using namespace mongocxx;

namespace pesa {
	const std::string DBConfig::s_jobsCollName = "Jobs";
	const int DBConfig::s_maxTries = 3;

	DBConfig::DBConfig() {
	}
	
	DBConfig::DBConfig(const Config& rhs, const std::string& batchId)
		: Config(rhs)
		, m_batchId(batchId) {
	}

	DBConfig::~DBConfig() {
	}

	bool DBConfig::load() {
		std::string jobId = Application::instance()->appOptions().jobId;

		/// If we've been passed a jobId by someone then we just load that specific jobId
		if (!jobId.empty()) 
			return loadJob(jobId);

		/// Otherwise, we just load the next available job
		return loadAnyJob();
	}

	bool DBConfig::loadJob(const std::string& jobId) {
		StringVec parts = Util::split(jobId, "@");
		std::string dbName = Config::dbName();

		m_isJob = true;

		if (parts.size() == 1) {
			m_id = io::ObjectId(jobId);
		}
		else {
			StringVec nss = Util::split(parts[0], ".");
			ASSERT(parts.size() == 2 && nss.size() == 2, "Invalid jobId: " << jobId << ". Must be of format <db>.<coll>@<jobId>");

			dbName = nss[0];
			m_jobsCollName = nss[1];
			std::string jobName = parts[1];
			auto pos = jobName.rfind('_');

			if (pos != std::string::npos) {
				pos++;
				jobName = jobName.substr(pos, jobName.length() - pos);
			}
			m_id = io::ObjectId(jobName);
		}

		m_mongoCl = std::make_shared<io::MongoClient>(dbName.c_str());
		InputMongoStream is(*m_mongoCl.get(), m_jobsCollName, m_id);

		ASSERT(is.isValid(), "Unable to load document: " << m_id.to_string() << " [DB: " << dbName << ", Coll: " << m_jobsCollName << "]");

		is.read("_id", &m_id);
		read(is);

		/// Now that we've read the config ... we're going to create a iid, if one doesn't exist
		std::string portIID = m_portfolio.getString("iid", "");

		/// If no iid is found, then we will just assign the job id as the iid
		if (portIID.empty()) {
			m_portfolio.set("iid", m_id.to_string());
		}

		return true;
	}

	bool DBConfig::loadAnyJob() {
		BsonDocumentBuilder find;
		Poco::Timespan maxMinutesBehind = Poco::Timespan(0, 0, 60, 0, 0);
		Poco::Timestamp oldestServerTickTime = Poco::Timestamp() - maxMinutesBehind;
		m_isJob = true;

		auto oldestTime = std::chrono::milliseconds(oldestServerTickTime.epochMicroseconds() / 1000);
		auto dt = types::b_date{ oldestTime };

		//find <<
		//	"isFinished" 	<< false <<
		//	"serverTickTime"<< open_document <<
		//		"$lte"		<< dt <<
		//	close_document;

		find <<
			"$or" << open_array <<
				open_document <<
					"handlingServer" << open_document <<
						"$eq" << "" <<
				close_document <<
				"isFinished" << false <<
			close_document <<

			open_document <<
				"isFinished" << false <<
				"serverTickTime" << open_document <<
					"$lte" << dt <<
				close_document <<
				"numTries" << open_document << 
					"$lt" << s_maxTries <<
				close_document <<
			close_document <<
		close_array;

		//Info("DBConfig", bsoncxx::to_json(find));

		m_serverTickTime = Poco::Timestamp();
		//auto update = bsoncxx::from_json("{\"$set\": {\"handlingServer\": \"test\"}}");
		BsonDocumentBuilder update;
		update <<
			"$set" << open_document <<
				"handlingServer" << Application::instance()->appOptions().simInstanceName <<
				"serverTickTime" << types::b_date{ std::chrono::milliseconds(Poco::Timestamp().epochMicroseconds() / 1000) } <<
			close_document << 
			/// Increment the tries count ...
			"$inc" << open_document <<
				"numTries" << 1 <<
			close_document
			;

		BsonDocumentBuilder sort;
		sort << "priority" << -1 << finalize;

		options::find_one_and_update opt;
		opt.sort(sort.view());
		opt.return_document(options::return_document::k_before);
		opt.upsert(false);

		std::string dbName = Config::dbName();
		io::MongoClient& mongoCl = *m_mongoCl.get();
		auto coll = mongoCl.coll(m_jobsCollName);
		auto result = coll.find_one_and_update(find.view(), update.view(), opt);

		/// There are no more jobs ...
		if (!result) {
			Debug("JOB", "No more pending jobs within the system!");
			return false;
		}

		/// OK now that we have a job ... load it ...
		InputMongoStream is(result);
		/// read the id from the database
		is.read("_id", &m_id);
		read(is);

		return true;
	}

	void DBConfig::updateStatus(int exitStatus, const RuntimeData* rt /* = nullptr */) {
		m_exitStatus = exitStatus;
		m_endTime = Poco::Timestamp();

		std::string fieldToUpdate = "numSuccessful";

		if (exitStatus != 0) {
			m_isFailed = true;
			fieldToUpdate = "numFailed";
		}

		/// OK now over here we just update the database with the latest information 
		/// and the status of the simulation ... 
		io::OutputMongoStream os(m_jobsCollName, m_id, nullptr, dbName().c_str());
		os.isUpdate() = true;
		os.mongoCl() = m_mongoCl.get();

		m_isFinished = true;

		bool isRunning = false;
		PESA_WRITE_VAR(os, isRunning);

		/// Set the exit status of the job and also mark it as finished
		PESA_WRITE_CVAR(os, exitStatus);
		PESA_WRITE_CVAR(os, isFinished);
		PESA_WRITE_CVAR(os, isFailed);
		PESA_WRITE_CVAR(os, isCancelled);

		/// Write the vital statistics of the simulation 
		PESA_WRITE_CVAR(os, startTime);
		PESA_WRITE_CVAR(os, endTime);
		PESA_WRITE_CVAR(os, percent);

		/// Now we see if there are any log messages, then we update the log messages
		if (m_logMsgs.size() && m_isMinimal) {
			/// Disable updating ... we want to overwrite the array ...
			//bool isUpdate = os.isUpdate();
			//os.isUpdate() = false;
			os.write("log", m_logMsgs);
			//os.isUpdate() = isUpdate;

			m_logMsgs.clear();
		}

		float endDatePercent = m_defs.getFloat("endDatePercent");
		if (rt && endDatePercent > 0.0f && endDatePercent < 100.0f) {
			os.document() << "$unset" << open_document <<
				std::string("defs.map.endDatePercent") << "" <<
			close_document;

			os.document() << "$set" << open_document <<
				std::string("defs.map.endDate") << Util::cast(rt->endDate) <<
			close_document;
		}

		Info("DB", bsoncxx::to_json(os.document()));
		os.flush();

		/// Also update the batch
		if (!m_batchId.empty()) {
			ObjectId batchId(m_batchId);
			io::OutputMongoStream bos(AlphaGen::s_batchCollName, batchId);
			bos.isUpdate() = true;

			bos.document() << "$inc" << open_document <<
				fieldToUpdate << 1 <<
				"numFinished" << 1 <<
			close_document;

			bos.flush();
		}
	}

	void DBConfig::save(io::MongoClient* mongoCl, io::ObjectId* id /* = nullptr */) {
		OutputMongoStream os(m_jobsCollName);

		os.mongoCl() = mongoCl;
		/// Now write the job to the database ...
		if (id != nullptr) {
			m_id = *id; 
			os.write("_id", m_id);
		}

		write(os);
		os.flush();

		//auto insertedId = os.insertedIdStr();
		//Trace("JOB", "Inserted job: %s", insertedId);
	}

	void DBConfig::overwriteModified() {
		std::string dbName = this->dbName();
		io::MongoClient& stratMongoCl = *m_mongoCl.get();
		const std::string aconfigCollName = "AlphaConfigs";

		io::OutputMongoStream os(aconfigCollName, m_id);
		os.mongoCl() = &stratMongoCl;
		os.isUpdate() = false;
		os.replace() = true;

		Poco::Timestamp updateTime(0);
		os.write("updateTime", updateTime);

		this->write(os);
		//Config::write(os);

		os.flush();
	}

	void DBConfig::loadRefConfig(const std::string& refConfig, const StringVec& additionalImports) {
		for (const auto& additionalImport : additionalImports) {
			ConfigSection* importSec = new ConfigSection(this);
			importSec->set("path", additionalImport);
			m_imports.push_back(importSec);
		}

		Config::parseXml(refConfig, *this, false);
	}

	InputStream& DBConfig::read(InputStream& is) {
		PESA_READ_CVAR(is, isMinimal);
		PESA_READ_CVAR(is, refConfig);

		if (m_isMinimal && !m_refConfig.empty()) {
			m_refConfig = this->defs().resolve(m_refConfig, (unsigned int)0);

			std::string market;
			PESA_READ_VAR(is, market);

			ASSERT(!market.empty(), "Invalid market specified for a minimal config!");

			std::string lmarket(Poco::toLower(market));
			StringVec additionalConfigs = { Poco::format("{$APP}/Configs/%s/%s_config.xml", market, std::move(Poco::toLower(market))) };

			loadRefConfig(m_refConfig, additionalConfigs);

			Config::m_defs.set("dbName", "pesa_" + lmarket);
			PESA_READ_CVAR(is, modifiedDate);
		}
		else {
			is.read("priority", (int*)&m_priority);

			PESA_READ_CVAR(is, isFinished);
			PESA_READ_CVAR(is, isFailed);
			PESA_READ_CVAR(is, handlingServer);
			PESA_READ_CVAR(is, serverTickTime);
			PESA_READ_CVAR(is, creationDate);
			PESA_READ_CVAR(is, modifiedDate);
			PESA_READ_CVAR(is, publishedDate);
			PESA_READ_CVAR(is, updateTime);
			PESA_READ_CVAR(is, exitStatus);
			PESA_READ_CVAR(is, batchId);
			PESA_READ_CVAR(is, isCancelled);
			PESA_READ_CVAR(is, numTries);
			PESA_READ_CVAR(is, email);
			PESA_READ_CVAR(is, emailSuccess);
			PESA_READ_CVAR(is, emailFailure);
			PESA_READ_CVAR(is, pruned);
			PESA_READ_CVAR(is, universeId);
			PESA_READ_CVAR(is, market);
			PESA_READ_CVAR(is, type);
			PESA_READ_CVAR(is, level);
			PESA_READ_CVAR(is, name);

			std::string psimVersion = Application::instance()->version();
			PESA_READ_VAR(is, psimVersion);
		}

		return Config::read(is);
	}

	OutputStream& DBConfig::write(OutputStream& os) {
		os.write("priority", (int)m_priority);

		m_updateTime = Poco::Timestamp();

		PESA_WRITE_CVAR(os, isMinimal);
		PESA_WRITE_CVAR(os, refConfig);
		PESA_WRITE_CVAR(os, isFinished);
		PESA_WRITE_CVAR(os, isFailed);
		PESA_WRITE_CVAR(os, handlingServer);
		PESA_WRITE_CVAR(os, serverTickTime);
		PESA_WRITE_CVAR(os, creationDate);
		PESA_WRITE_CVAR(os, modifiedDate);
		PESA_WRITE_CVAR(os, publishedDate);
		PESA_WRITE_CVAR(os, updateTime);
		PESA_WRITE_CVAR(os, exitStatus);
		PESA_WRITE_CVAR(os, batchId);
		PESA_WRITE_CVAR(os, isCancelled);
		PESA_WRITE_CVAR(os, numTries);
		PESA_WRITE_CVAR(os, email);
		PESA_WRITE_CVAR(os, emailSuccess);
		PESA_WRITE_CVAR(os, emailFailure);
		PESA_WRITE_CVAR(os, pruned);
		PESA_WRITE_CVAR(os, universeId);
		PESA_WRITE_CVAR(os, market);
		PESA_WRITE_CVAR(os, type);
		PESA_WRITE_CVAR(os, level);
		PESA_WRITE_CVAR(os, name);

		std::string psimVersion = Application::instance()->version();
		PESA_WRITE_VAR(os, psimVersion);

		return Config::write(os);
	}

	void DBConfig::tick(const RuntimeData& rt, float percent) {
		m_percent = percent;

		/// Every few seconds we update the document to update the latest stats of the doc
		float seconds = (float)m_serverTickTime.elapsed() * 0.001f * 0.001f;
		float updateInterval = m_defs.getFloat("updateInterval", 5.0f);
		 
		/// We update once every second
		if (seconds < updateInterval)
			return; 

		std::string dbName = this->dbName();

		/// First of all we read the document and see 
		io::InputMongoStream is(*m_mongoCl.get(), m_jobsCollName, m_id);
		bool isCancelled = false;
		PESA_READ_VAR(is, isCancelled);

		if (isCancelled) {
			m_isCancelled = true;
			m_isFailed = false;
			m_isFinished = true;

			Warning("DBConfig", "Got cancel request for job: %s", m_id.to_string());
			end(rt, 666);
			std::exit(666);

			return;
		}


		/// Now we update the document
		io::OutputMongoStream os(m_jobsCollName, m_id, nullptr, dbName.c_str());
		os.isUpdate() = true;
		os.mongoCl() = m_mongoCl.get();

		m_serverTickTime = Poco::Timestamp();

		PESA_WRITE_CVAR(os, percent);
		PESA_WRITE_CVAR(os, serverTickTime);

		bool isRunning = true;
		PESA_WRITE_VAR(os, isRunning);

		//if (m_logMsgs.size() && m_isMinimal) {
		//	if (!m_firstLogWrite)
		//		os.write("log", m_logMsgs);
		//	else
		//		os.replaceArray("log", m_logMsgs);

		//	m_logMsgs.clear();
		//	m_firstLogWrite = false;
		//}

		os.flush();
	}

	void DBConfig::log(const Poco::Message& msg) {
		/// The logging is currently disabled on this ...
		if (!m_doLog)
			return;

		/// Over here we now log the messages
		if (Application::instance()->appOptions().quiet) {
			auto msgText = msg.getText();
			std::cout << Util::cast<int>((int)msg.getPriority()) + ">" + msgText << std::endl;
		}

		//m_logMsgs.push_back(Util::cast<int>((int)msg.getPriority()) + ">" + msgText);
	}

	void DBConfig::exit(int errCode) {
		updateStatus(errCode);
	}

	void DBConfig::abort() {
		updateStatus(-999);
		return;
	}

	void DBConfig::start() {
		m_startTime = Poco::Timestamp();

		std::string dbName = this->dbName();
		io::OutputMongoStream os(m_jobsCollName, m_id, nullptr, dbName.c_str());
		os.isUpdate() = true;
		os.mongoCl() = m_mongoCl.get();

		PESA_WRITE_CVAR(os, startTime);

		bool isRunning = true;
		PESA_WRITE_VAR(os, isRunning);

		os.flush();
	}

	void DBConfig::end(const RuntimeData& rt, int errCode) {
		updateStatus(errCode, &rt);
	}

	Poco::Timestamp DBConfig::getModifiedTimestamp() const {
		return m_modifiedDate;
	}
} /// namespace pesa

