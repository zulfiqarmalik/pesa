/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// DBConfig.h
///
/// Created on: 08 Sep 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/Framework.h"
#include "Framework/Config/Config.h"
#include "Poco/ConsoleChannel.h"

#include "Helper/DB/MongoStream.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// DBConfig - Runs the next job from the DB
	////////////////////////////////////////////////////////////////////////////
	class DBConfig : public Config, public Poco::Channel {
	public:
		static const std::string	s_jobsCollName;  
		static const int			s_maxTries;

	private:

		std::string					m_jobsCollName = "Jobs";

		bool						m_doLog = false;			/// Whether we should log or not
		Util::Priority 				m_priority = Util::kNormal;	/// What is the priority of this config
		std::string					m_market;					/// The market that this belongs to
		std::string					m_universeId;				/// The universeId
		bool						m_isMinimal = false;		/// Whether this is a minimal config
		std::string					m_refConfig;				/// What is the reference config
		bool						m_isFinished = false;		/// Whether the job has finished or not
		bool						m_isFailed = false;			/// Whether the job has failed or not
		std::string					m_handlingServer;			/// The server that is handling this config
		Poco::Timestamp				m_serverTickTime;			/// The last time this record was updated
		Poco::Timestamp				m_creationDate;				/// The time when this was created
		Poco::Timestamp				m_publishedDate;			/// The time when this was published
		Poco::Timestamp				m_modifiedDate;				/// The time when the last time this config was modified
		Poco::Timestamp				m_updateTime;				/// The time when it was last updated
		int							m_exitStatus = 1;			/// The exit status of the simulaton
		io::ObjectId				m_id;						/// The id of the document that we're using 
		std::string					m_batchId;					/// The id of the batch that this job belongs
		int							m_numTries = 0;				/// The number of tries that we've done on this config
		StringVec					m_logMsgs;					/// These are the log messages of the config
		bool						m_firstLogWrite = true;		/// First time writing log?
		bool						m_isJob = false;			/// Is this a job

		bool						m_isCancelled = false;		/// Whether the job was cancelled or not. This is normally propogated from the batch!
		std::string					m_email;					/// Whether to send an email if the job fails
		bool						m_emailFailure = false;		/// Email failure 
		bool						m_emailSuccess = false;		/// Email success or not
		bool						m_pruned = false;			/// Pruned or not

		io::MongoClientPtr			m_mongoCl;					/// The mongo client

		std::string					m_type;						/// Type
		std::string					m_level;
		std::string					m_name;

		//////////////////////////////////////////////////////////////////////////
		/// This data is collected from the the calls made by the pipeline handler 
		//////////////////////////////////////////////////////////////////////////
		float						m_percent = 0.0f;			/// What percent of the job is done
		Poco::Timestamp				m_startTime;				/// The time that the simulation started
		Poco::Timestamp				m_endTime;					/// The time that the simulation ended (could have been aborted)

	public:
									DBConfig(const Config& rhs, const std::string& batchId);
									DBConfig();
		virtual 					~DBConfig();

		////////////////////////////////////////////////////////////////////////////
		/// DB Handling
		////////////////////////////////////////////////////////////////////////////
		bool						loadAnyJob();
		bool						loadJob(const std::string& jobId);
		bool 						load();
		void						loadRefConfig(const std::string& refConfig, const StringVec& additionalImports);
		void 						save(io::MongoClient* mongoCl, io::ObjectId* id = nullptr);

		void						updateStatus(int exitStatus, const RuntimeData* rt = nullptr);

		//////////////////////////////////////////////////////////////////////////
		/// Config overrides
		//////////////////////////////////////////////////////////////////////////
		virtual void				tick(const RuntimeData& rt, float percent);
		virtual io::InputStream&	read(io::InputStream& is);
		virtual io::OutputStream&	write(io::OutputStream& os);
		virtual void				overwriteModified();

		virtual void				exit(int errCode);
		virtual void				abort();
		virtual void				start();
		virtual void				end(const RuntimeData& rt, int errCode);
		virtual Poco::Timestamp		getModifiedTimestamp() const;

		//////////////////////////////////////////////////////////////////////////
		/// Poco::Channel
		//////////////////////////////////////////////////////////////////////////
		virtual void				log(const Poco::Message& msg);

		//////////////////////////////////////////////////////////////////////////
		/// Inline functions
		//////////////////////////////////////////////////////////////////////////
		inline Util::Priority		priority() const { return m_priority; }
		inline Util::Priority&		priority() { return m_priority; }
		inline std::string&			market() { return m_market; }
		inline std::string			market() const { return m_market; }
		inline bool&				doLog() { return m_doLog; }
		inline const std::string&	batchId() { return m_batchId; }
		inline bool					isCancelled() const { return m_isCancelled; }
		inline int					numTries() const { return m_numTries; }
		inline std::string&			email() { return m_email; }
		inline std::string			email() const { return m_email; }
		inline Poco::Timestamp&		creationTime() { return m_creationDate; }
		inline Poco::Timestamp&		updateTime() { return m_updateTime; }
		inline bool&				emailSuccess() { return m_emailSuccess; }
		inline bool&				emailFailure() { return m_emailFailure; }
		inline std::string&			jobsCollName() { return m_jobsCollName; }
		inline io::ObjectId&		id() { return m_id; }
		inline std::string&			universeId() { return m_universeId; }
		inline std::string&			type() { return m_type; }
		inline std::string&			level() { return m_level; }
		inline std::string&			name() { return m_name; }
		inline bool					isMinimal() const { return m_isMinimal; }
		inline const std::string&	refConfig() const { return m_refConfig; }

		inline bool					pruned() const { return m_pruned; }
		inline bool&				pruned() { return m_pruned; }
	};

	typedef std::shared_ptr<DBConfig> DBConfigPtr;
} /// namespace pesa
