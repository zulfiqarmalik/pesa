/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// DBJobRun.cpp
///
/// Created on: 08 Sep 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "DBJobRun.h"
#include "Poco/String.h"
#include "Poco/Glob.h"
#include "Poco/SplitterChannel.h"

namespace pesa {
	DBJobRun::DBJobRun(const ConfigSection& config) : IPipelineComponent(config, "ALPHA_GEN") {
		PTrace("Created DBJobRun!");
	}

	DBJobRun::~DBJobRun() {
		PTrace("Deleting DBJobRun!");
	}

	void DBJobRun::prepare(RuntimeData& rt) {
		/// Basically load the next job from the database
		try {
			DBConfigPtr config = std::make_shared<DBConfig>();

			if (config->load()) {
				if (config->isCancelled() || config->numTries() >= DBConfig::s_maxTries)
					return; 

				/// We have read the config ... Finalise it now ... 
				if (!config->isMinimal()) {
					config->finalise();
					config->postLoad();
				}

				config->doLog() = true;
				auto existingChannel = dynamic_cast<Poco::SplitterChannel*>(Poco::Logger::root().getChannel());

				/// add the config as a logging channel to the splitter channel that we 
				/// have over here in the log ...
				if (existingChannel)
					existingChannel->addChannel(config.get());
				/// If we do not have a splitter channel then we just replace the current log channel
				/// with our own so that everything is logged to the database in any case!
				else
					Poco::Logger::root().setChannel(config.get());

				/// Then queue it ...
				pipelineHandler()->queueConfig(config);

				m_config = config;
			}
		}
		catch (const Poco::Exception& e) {
			Util::reportException(e);
		}
		catch (const std::exception& e) {
			Util::reportException(e);
		}
	}

	void DBJobRun::tick(const RuntimeData& rt) {
	}

	void DBJobRun::finalise(RuntimeData& rt) {
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IPipelineComponent* createDBJobRun(const pesa::ConfigSection& config) {
	return new pesa::DBJobRun((const pesa::ConfigSection&)config);
}
