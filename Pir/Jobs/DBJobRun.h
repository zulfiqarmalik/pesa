/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// DBJobRun.h
///
/// Created on: 08 Sep 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/Framework.h"

#include "DBConfig.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// DBJobRun - Runs the next job from the DB
	////////////////////////////////////////////////////////////////////////////
	class DBJobRun : public IPipelineComponent {
	private:
		DBConfigPtr				m_config;
	public:
								DBJobRun(const ConfigSection& config);
		virtual 				~DBJobRun();

		////////////////////////////////////////////////////////////////////////////
		/// IPipelineComponent overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			prepare(RuntimeData& rt);
		virtual void 			tick(const RuntimeData& rt);
		virtual void 			finalise(RuntimeData& rt);
	};
} /// namespace pesa
