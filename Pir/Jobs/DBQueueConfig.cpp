/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// DBQueueConfig.cpp
///
/// Created on: 22 Mar 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "DBQueueConfig.h"
#include "Poco/String.h"
#include "Poco/Glob.h"
#include "Poco/SplitterChannel.h"
#include "Poco/File.h"

using namespace pesa::io;
using namespace bsoncxx;
using namespace mongocxx;

namespace pesa {
	DBQueueConfig::DBQueueConfig(const ConfigSection& config) : IPipelineComponent(config, "QUEUE_CONFIG") {
		PTrace("Created DBQueueConfig!");
	}

	DBQueueConfig::~DBQueueConfig() {
		PTrace("Deleting DBQueueConfig!");
	}

	DBConfigPtr DBQueueConfig::loadExistingConfig(io::MongoClient& mongoCl, const std::string& filename) {
		BsonDocumentBuilder find;

		find << std::string("defs.map._filename") << filename;
		auto coll = mongoCl.coll("AlphaConfigs");
		auto result = coll.find_one(find.view());

		/// There are no more jobs ...
		if (!result) 
			return nullptr;

		DBConfigPtr dbConfig = std::make_shared<DBConfig>();

		/// OK now that we have a job ... load it ...
		InputMongoStream is(result);
		io::ObjectId id;
		/// read the id from the database
		is.read("_id", &id);

		dbConfig->id() = id;
		dbConfig->read(is);

		return dbConfig;
	}

	void DBQueueConfig::prepare(RuntimeData& rt) {
		/// Basically load the next job from the database
		try {
			std::string parentDBName	= parentConfig()->defs().getRequired("dbName");

			const auto& myConfig		= config();
			const auto childConfigs		= myConfig.getAllChildrenOfType("Config");
			const auto aconfigCollName	= "AlphaConfigs";

			IntDate startDate			= parentConfig()->defs().get<IntDate>("startDate", 0);
			ASSERT(startDate, "Invalid startDate!");

			std::string endDate			= parentConfig()->defs().getString("endDate", "");
			Util::Priority priority		= (Util::Priority)(parentConfig()->defs().get<int>("priority", (int)Util::kHigh));
			bool queueJobs				= parentConfig()->defs().getBool("queueJobs", false);
			std::string email			= parentConfig()->defs().getString("email", "");
			bool emailSuccess			= parentConfig()->defs().getBool("emailSuccess", false);
			bool emailFailure			= parentConfig()->defs().getBool("emailFailure", false);

			auto* pruneSec				= parentConfig()->defs().getChildSection("Prune");

			for (const ConfigSection* childConfig : childConfigs) {
				const std::string& baseFilename = childConfig->getRequired("path");

				PDebug("Loading multiple configs from path: %s", baseFilename);

				std::set<std::string> filenames;
				Poco::Glob::glob(baseFilename, filenames);

				PInfo("Total number of files: %z", filenames.size());

				for (const auto& filename : filenames) {
					if (filename.find("MASTER_") != std::string::npos) {
						PInfo("Ignoring config: %s [Looks like a MASTER config with child strategies]", filename);
						continue;
					}

					/// Load the config
					Config loadedChildConfig;
					bool didLoad = Config::parseXml(filename, loadedChildConfig);
					std::string targetDBName = parentDBName; /// loadedChildConfig.defs().getString("dbName", parentDBName);
					io::MongoClient stratMongoCl(targetDBName.c_str());

					PInfo("Loading config: %s", filename);

					ASSERT(didLoad, "Unable to load config: " << filename);

					DBConfigPtr existingConfig = loadExistingConfig(stratMongoCl, filename);

					if (existingConfig && existingConfig->pruned()) {
						PInfo("Config has been pruned: %s", filename);
						continue;
					}

					if (pruneSec) {
						/// Add the prune sec
						loadedChildConfig.defs().addChildSection(new ConfigSection(&loadedChildConfig, *pruneSec));
					}

					auto& portSec = loadedChildConfig.portfolio();
					std::string configIID = portSec.getString("iid", "");
					std::string configUuid = portSec.getString("uuid", "MyPort");
					std::string universeId = portSec.getString("universeId", "");
					std::string market = loadedChildConfig.defs().market();

					auto* optimiseSec = portSec.getChildSection("Optimise");
					if (optimiseSec && optimiseSec->getString("objectiveFunction", "") == "l1norm") {
						PInfo("Changing l1norm");
						optimiseSec->set("objectiveFunction", "maxret");
						optimiseSec->set("useSqConstraint", "true");

						if (market == "EU") {
							optimiseSec->set("sqConstraintParam", "0.7");
							optimiseSec->set("overrideMaxPosPctBook", "5");
						}
						else if (market == "US") {
							optimiseSec->set("sqConstraintParam", "0.55");
							optimiseSec->set("overrideMaxPosPctBook", "3.5");
						}
					}

					ConfigSection& defsSec = loadedChildConfig.defs();

					defsSec.set("startDate", Util::cast(startDate));
					defsSec.set("dbName", targetDBName);
					defsSec.set("exactStartDate", "true");
					defsSec.set("isMinimal", "false");

					if (!endDate.empty())
						defsSec.set("endDate", endDate);

					if (universeId.empty()) {
						const ConfigSection* alphasSec = portSec.getChildSection("Alphas");
						//ASSERT(alphasSec, "No Alphas section under Portfolio!");
						if (!alphasSec) {
							PInfo("No Alphas section. Ignoring: %s", filename);
							continue;
						}

						const ConfigSection* firstAlphaSec = alphasSec->getChildSection("Alpha");
						//ASSERT(firstAlphaSec, "No Alpha section under Alphas section!");
						if (!firstAlphaSec) {
							PInfo("No Alpha section under Alphas section. Ignoring: %s", filename);
							continue;
						}

						universeId = firstAlphaSec->getRequired("universeId");
					}

					/// We cannot really use MyPort as the strategy UUID (or one that doesn't start with the target market!)
					if (configIID.empty() || configIID == "MyPort" || configIID.find(market) != 0) {
						if (existingConfig)
							configIID = existingConfig->id().to_string();
						else {
							io::ObjectId oid;
							configIID = oid.to_string();
						}
					}

					if (!existingConfig)
						configUuid = market + "_" + universeId + "_" + configIID;
					else
						configUuid = existingConfig->portfolio().getRequired("uuid");

					portSec.set("checkpoint", "true");
					portSec.set("checkpointDay", "5");
					portSec.set("checkpointToDB", "false");
					portSec.set("checkpointFilename", "{$DATA_DRV}/prod/Strategies/Checkpoints/{$market}/{$PORT_UUID}");
					portSec.set("uuid", configUuid);
					portSec.set("iid", configIID);

					/// We don't want to write the portfolio section!
					auto* posSec = portSec.getChildSection("Pos");
					ASSERT(posSec, "Must have a <Pos> secton: " << filename);

					posSec->set("write", "true");
					posSec->set("daily", "true");
					posSec->set("writeDB", "true");

					/// Setup misc fields under the Stats section
					auto* statsSec = portSec.getChildSection("Stats");
					statsSec->set("alpha", "false");
					statsSec->set("portfolio", "true");
					statsSec->set("writeFile", "true");
					statsSec->set("writeDB", "true");
					statsSec->set("fileDir", "{$DATA_DRV}/prod/Strategies/pnls/{$market}");

					io::ObjectId oid;

					{
						// PROFILE_SCOPED();

						io::InputMongoStream readStream(stratMongoCl, aconfigCollName, configIID);
						bool pruned = false;

						if (readStream.isValid()) {
							int endDate = 0;
							//readStream.read("endDate", &endDate);
							readStream.read("pruned", &pruned);
							readStream.read("iid", &oid);
						}

						/// The alpha has been pruned ... we don't wanna use it
						if (pruned) {
							PDebug("Pruned: %s", filename);
							continue;
						}
					}

					/// Here we try to read the original creationDate of the Alpha. This is useful for ensuring that 
					/// the OUTSAMPLE data for the alpha is collected correctly
					Poco::Timestamp creationDate;

					/// If there is an existing file then we just get the various bits of details from there
					if (existingConfig)
						creationDate = existingConfig->creationTime();
					else {
						/// Otherwise we get the date time of the file
						Poco::File configFile(filename);
						creationDate = configFile.getLastModified();
					}

					try {
						portSec.set("creationDate", Util::cast(Util::dateToInt(Poco::DateTime(creationDate))));

						DBConfig dbConfig(loadedChildConfig, "");
						io::ObjectId configDBId(configIID);

						{
							dbConfig.dbName()		= targetDBName;
							dbConfig.jobsCollName() = aconfigCollName;
							dbConfig.universeId()	= universeId;
							dbConfig.market()		= market;
							dbConfig.type()			= "auto";
							dbConfig.level()		= "strategy";
							dbConfig.name()			= configUuid;

							dbConfig.creationTime() = creationDate;
							dbConfig.priority()		= Util::kHigh;

							dbConfig.email()		= email;
							dbConfig.emailSuccess() = emailSuccess;
							dbConfig.emailFailure() = emailFailure;

							dbConfig.defs().set("_filename", filename);
							dbConfig.defs().set("dbName", targetDBName);

							if (!existingConfig)
								dbConfig.save(&stratMongoCl, &configDBId);
							else
								dbConfig.overwriteModified();

							PInfo("Added config: %s [ID: %s]", std::string(aconfigCollName), configIID);
						}

						//{
						//	io::OutputMongoStream sos(astratConfigCollName, configUuid);
						//	//sos.mongoCl() = &stratMongoCl;
						//	sos.isUpdate() = false;
						//	sos.replace() = true;

						//	Poco::Timestamp updateTime(0);
						//	sos.write("updateTime", updateTime);
						//	sos.write("creationTime", creationDate);
						//	sos.write("priority", Util::kHigh);

						//	loadedChildConfig.write(sos);
						//	sos.flush();
						//}


						if (queueJobs) {
							dbConfig.jobsCollName() = DBConfig::s_jobsCollName;
							dbConfig.priority() = Util::kHigh;
							dbConfig.save(&m_mongoCl, &configDBId);

							PInfo("Queued: %s [ID: %s]", filename, configIID);
						}
					}
					catch (const Poco::Exception& e) {
						std::cerr << "Exception: " << e.displayText() << ". Ignoring: " << filename << std::endl;
						continue;
					}
					catch (const std::exception& e) {
						std::cerr << "Exception (std): " << e.what() << ". Ignoring: " << filename << std::endl;
						continue;
					}
				}
			}
		}
		catch (const Poco::Exception& e) {
			Util::reportException(e);
		}
		catch (const std::exception& e) {
			Util::reportException(e);
		}
	}

	void DBQueueConfig::tick(const RuntimeData& rt) {
	}

	void DBQueueConfig::finalise(RuntimeData& rt) {
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IPipelineComponent* createDBQueueConfig(const pesa::ConfigSection& config) {
	return new pesa::DBQueueConfig((const pesa::ConfigSection&)config);
}
