/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// DBQueueConfig.h
///
/// Created on: 22 Mar 2017
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/Framework.h"

#include "DBConfig.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// DBQueueConfig - Runs the next job from the DB
	////////////////////////////////////////////////////////////////////////////
	class DBQueueConfig : public IPipelineComponent {
	private:
		io::MongoClient			m_mongoCl;			/// The lone mongo client

		DBConfigPtr				loadExistingConfig(io::MongoClient& mongoCl, const std::string& filename);

	public:
								DBQueueConfig(const ConfigSection& config);
		virtual 				~DBQueueConfig();

		////////////////////////////////////////////////////////////////////////////
		/// IPipelineComponent overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			prepare(RuntimeData& rt);
		virtual void 			tick(const RuntimeData& rt);
		virtual void 			finalise(RuntimeData& rt);
	};
} /// namespace pesa
