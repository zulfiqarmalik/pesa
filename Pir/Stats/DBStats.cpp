/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// DBStats.cpp
///
/// Created on: 11 Sep 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "DBStats.h"
#include "Poco/String.h"
#include "Poco/Glob.h"

#include "Helper/DB/MongoStream.h"

#include <regex>

using namespace pesa::io;
using namespace bsoncxx;
using namespace mongocxx;

namespace pesa {
	const std::string DBStats::s_collName 		= "Stats";
	const std::string DBStats::s_searchCollName	= "StatsSearch";

	DBStats::DBStats(const Config* config, io::MongoClient& mongoCl, const std::string& uuid, LifetimeStatistics& stats, 
		bool quarterlyStats /* = false */, bool monthlyStats /* = false */, bool weeklyStats /* = false */)
		: m_config(config)
		, m_mongoCl(mongoCl)
		, m_uuid(uuid)
		, m_stats(stats)
		, m_quarterlyStats(quarterlyStats)
		, m_monthlyStats(monthlyStats)
		, m_weeklyStats(weeklyStats) {
		ASSERT(!m_uuid.empty(), "Invalid UUID set for the stats. It must be set prior to being written to a stream!");
	}
	
	DBStats::~DBStats() {
	}

	void DBStats::createIndexes() {
		return;
		auto coll = m_mongoCl.coll(s_collName);
		options::index indexOpt;
		indexOpt.background(true);

		coll.create_index((BsonDocumentBuilder{}  << 
			"universeId" 		<< 1 << 
			finalize).view(), indexOpt);

		coll.create_index((BsonDocumentBuilder{} <<
			"info.numInvalidPnl" << 1 <<
			finalize).view(), indexOpt);

		coll.create_index((BsonDocumentBuilder{}  <<
			"universeId" 		<< 1 << 
			"market" 			<< 1 << 
			finalize).view(), indexOpt);

		coll.create_index((BsonDocumentBuilder{} <<
			"tags" << 1 <<
			finalize).view(), indexOpt);
	}

	bool DBStats::load() {
		return true;
	}

	void DBStats::createSearchIndexes(const std::string& collName) {
		return;
		auto coll = m_mongoCl.coll(collName);
		options::index indexOpt;
		indexOpt.background(true);

		coll.create_index((BsonDocumentBuilder{} << 
			"universeId"	<< 1 << 
			"market"		<< 1 <<
			"stype"			<< 1 <<
			finalize).view(), indexOpt);

		coll.create_index((BsonDocumentBuilder{} << "tags" 			<< 1 << finalize).view(), indexOpt);
		coll.create_index((BsonDocumentBuilder{} << "uuid" 			<< 1 << finalize).view(), indexOpt);
		coll.create_index((BsonDocumentBuilder{} << "universeId" 	<< 1 << finalize).view(), indexOpt);
		coll.create_index((BsonDocumentBuilder{} << "market" 		<< 1 << finalize).view(), indexOpt);
		coll.create_index((BsonDocumentBuilder{} << "stype" 		<< 1 << finalize).view(), indexOpt);
		coll.create_index((BsonDocumentBuilder{} << "numInvalidPnl" << 1 << finalize).view(), indexOpt);
		coll.create_index((BsonDocumentBuilder{} << "type" 			<< 1 << finalize).view(), indexOpt);
		coll.create_index((BsonDocumentBuilder{} << "startDate" 	<< 1 << finalize).view(), indexOpt);
		coll.create_index((BsonDocumentBuilder{} << "endDate" 		<< 1 << finalize).view(), indexOpt);
		coll.create_index((BsonDocumentBuilder{} << "pnl" 			<< 1 << finalize).view(), indexOpt);
		coll.create_index((BsonDocumentBuilder{} << "tvr" 			<< 1 << finalize).view(), indexOpt);
		coll.create_index((BsonDocumentBuilder{} << "returns" 		<< 1 << finalize).view(), indexOpt);
		coll.create_index((BsonDocumentBuilder{} << "marginBps" 	<< 1 << finalize).view(), indexOpt);
		coll.create_index((BsonDocumentBuilder{} << "marginCps" 	<< 1 << finalize).view(), indexOpt);
		coll.create_index((BsonDocumentBuilder{} << "volatility" 	<< 1 << finalize).view(), indexOpt);
		coll.create_index((BsonDocumentBuilder{} << "ir" 			<< 1 << finalize).view(), indexOpt);
		coll.create_index((BsonDocumentBuilder{} << "hitRatio" 		<< 1 << finalize).view(), indexOpt);
		coll.create_index((BsonDocumentBuilder{} << "maxTvr" 		<< 1 << finalize).view(), indexOpt);
		coll.create_index((BsonDocumentBuilder{} << "maxDD" 		<< 1 << finalize).view(), indexOpt);
		coll.create_index((BsonDocumentBuilder{} << "maxDH" 		<< 1 << finalize).view(), indexOpt);
	}

	void DBStats::saveSearch(const std::string& name, LifetimeStatistics& ltStats, IndexedRangeStatisticsVec& rstats, size_t offset) {
		for (size_t i = 0; i < rstats.size(); i++) {
			size_t index = i + offset;
			std::string fullName = name + Util::castPadded(index, 2);
			saveSearch(fullName, ltStats, rstats[i], rstats[i].startDate, rstats[i].endDate);
		}
	}

	void DBStats::saveMinimal(LifetimeStatistics& ltStats) {
		saveSearch("LT", ltStats, ltStats, ltStats.startDate, ltStats.endDate);
		saveSearch("OS", ltStats, ltStats.outSample, ltStats.outSample.startDate, ltStats.outSample.endDate);
		saveSearch("PS", ltStats, ltStats.publishSample, ltStats.publishSample.startDate, ltStats.publishSample.endDate);
		saveSearch("IS", ltStats, ltStats.inSample, ltStats.inSample.startDate, ltStats.inSample.endDate);

		for (AnnualStatisticsMap::iterator iter = ltStats.annual.begin(); iter != ltStats.annual.end(); iter++) {
			AnnualStatistics& astats = iter->second; 
			std::string prefix = Util::cast(astats.year);

			if (m_annualStats)
				saveSearch(prefix, ltStats, astats, astats.startDate, astats.endDate);

			if (m_monthlyStats)
				saveSearch(prefix + "_MN", ltStats, astats.monthly, 1);
			//if (m_weeklyStats)
			//	saveSearch(prefix + "_WK", ltStats, astats.weekly, 1);
			if (m_quarterlyStats)
				saveSearch(prefix + "_QT", ltStats, astats.quarterly, 1);
			//saveSearch(prefix + "_SA", ltStats, astats.semiAnnual, 1);
		}

		saveSearch("D5", ltStats, ltStats.D5, ltStats.D5.startDate, ltStats.D5.endDate);
		saveSearch("D10", ltStats, ltStats.D10, ltStats.D10.startDate, ltStats.D10.endDate);
		saveSearch("D21", ltStats, ltStats.D21, ltStats.D21.startDate, ltStats.D21.endDate);
		saveSearch("D63", ltStats, ltStats.D63, ltStats.D63.startDate, ltStats.D63.endDate);
		saveSearch("D120", ltStats, ltStats.D120, ltStats.D120.startDate, ltStats.D120.endDate);
		saveSearch("D250", ltStats, ltStats.D250, ltStats.D250.startDate, ltStats.D250.endDate);
	}

	void DBStats::saveSearch(const std::string& stype, LifetimeStatistics& ltStats, Statistics& stats, IntDate startDate, IntDate endDate) {
		DBStats::createSearchIndexes(DBStats::s_searchCollName);

		ASSERT(!ltStats.uuid.empty(), "Empty UUID specified for stats!");

		/// OK, we don't wanna overwrite previously written valid data with invalid one
		if (!startDate || !endDate) 
			return;

		std::string sid = m_uuid; /// ltStats.uuid; /// + "_" + stype;
		io::OutputMongoStream os("STATS_" + stype, sid, nullptr);
		os.mongoCl() = &m_mongoCl;
		os.isUpdate() = false;
		os.replace() = true;

		Trace("STATS", "[%s] - Writing: %s [%s]", ltStats.uuid, sid, stype);

		/// Now we write some additional information about the alpha
		os.write("_id", sid);
		//os.write("uuid", ltStats.uuid);
		os.write("universeId", ltStats.universeId);
		os.write("market", ltStats.market);
		os.write("delay", ltStats.delay);
		os.write("tags", ltStats.tags);
		os.write("type", ltStats.type);
		os.write("startDate", startDate);
		os.write("endDate", endDate);
		os.write("stype", stype);
		os.write("numInvalidPnl", m_numInvalidPnl);
		os.write("maxTime", ltStats.maxTime);

		if (!m_writeDetailed)
			stats.writeMinimal(os);
		else
			stats.write(os);

		os.flush();
	}

	int DBStats::countInvalidPnl(LifetimeStatistics& stats) {
		/// Check whether we're going to prune this or not ...
		int numInvalidPnl = 0;
		for (const auto& ptStats : stats.vec) {
			bool hasPnl = std::abs(ptStats.pnl) > 0.0001f;
			if (!hasPnl)
				numInvalidPnl++;
		}

		return numInvalidPnl;
	}

	bool DBStats::checkPrunedGeneric(const ConfigSection* sec, const CumStatistics& stats) {
		float ir = sec->get<float>("ir", 0.0f);
		if (ir > 0.0f && std::abs(stats.ir) < ir)
			return true;

		float returns = sec->get<float>("returns", 0.0f);
		if (returns > 0.0f && std::abs(stats.returns) < returns)
			return true;

		// float volatility = sec->get<float>("volatility", 0.0f);
		// if (volatility > 0.0f && std::abs(stats.volatility) < volatility)
		// 	return true;

		// float tvr = sec->get<float>("tvr", 0.0f);
		// if (tvr > 0.0f && std::abs(stats.tvr) > tvr)
		// 	return true;

		// float maxDD = sec->get<float>("maxDD", 0.0f);
		// if (maxDD > 0.0f && std::abs(stats.maxDD) > maxDD)
		// 	return true;

		// float maxDH = sec->get<float>("maxDH", 0.0f);
		// if (maxDH > 0.0f && std::abs(stats.maxDH) > maxDH)
		// 	return true;

		return false;
	}

	bool DBStats::checkLTPruned() {
		const ConfigSection* pruneSec = m_config->defs().getChildSection("Prune");
		const ConfigSection* pruneLTSec = pruneSec->getChildSection("LT");

		if (!pruneLTSec)
			return false;

		return checkPrunedGeneric(pruneLTSec, m_stats);
	}

	bool DBStats::checkAnnualPruned() {
		const ConfigSection* pruneSec = m_config->defs().getChildSection("Prune");
		const ConfigSection* pruneAnnualSec = pruneSec->getChildSection("Annual");

		if (!pruneAnnualSec)
			return false;

		int currentYear = Poco::DateTime().year();

		for (auto iter = m_stats.annual.begin(); iter != m_stats.annual.end(); iter++) {
			const CumStatistics& astats = iter->second;

			/// We don't prune for the running year!
			int year = iter->first;
			if (year == currentYear)
				continue;

			/// if any one of the annual statistics fail the test then we just consider the thing pruned
			if (checkPrunedGeneric(pruneAnnualSec, astats))
				return true;
		}

		return false;
	}

	bool DBStats::checkPruned() {
		const ConfigSection* pruneSec = m_config->defs().getChildSection("Prune");
		if (!pruneSec)
			return false;

		float longShortRatioTolerance = pruneSec->get<float>("longShortRatioTolerance", 0.0f);
		float universeFillRatioTolerance = pruneSec->get<float>("universeFillRatioTolerance", 0.0f);

		/// See if there is a universe specific override ...
		universeFillRatioTolerance = pruneSec->get<float>("universeFillRatioTolerance_" + m_stats.universeId, universeFillRatioTolerance);
		float longShortSum = m_stats.longCount + m_stats.shortCount;
		float longShortRatio = 0.0f;

		if (longShortSum != 0.0f)
			longShortRatio = 1.0f - ((m_stats.longCount - m_stats.shortCount) / (longShortSum));

		/// TODO: Check later!
#if 0
		std::string suniverseSize = std::regex_replace(m_stats.universeId, std::regex(R"([\D])"), "");;
		float universeSize = Util::cast<float>(suniverseSize);
		float universeFillRatio = 1.0f;

		if (universeSize > 0.0f) {
			universeFillRatio = (m_stats.longCount + m_stats.shortCount) / universeSize;
		}

		int numInvalidPnlThreshold = pruneSec->get<int>("numInvalidPnlThreshold", 10);

		if (universeFillRatio < universeFillRatioTolerance ||
			longShortRatio < longShortRatioTolerance ||
			(m_numInvalidPnl > numInvalidPnlThreshold && numInvalidPnlThreshold > 0)) {

			//Debug("STATS", "[%s] - Puning. LSRatio: %0.2hf [%0.2hf] - Univ.Ratio: %0.2hf [%0.2hf]", m_uuid,
			//	longShortRatio, longShortRatioTolerance, universeFillRatio, universeFillRatioTolerance);

			return true;
		}
#endif /// TODO:

		if (checkLTPruned())
			return true;

		if (checkAnnualPruned())
			return true;

		return false;
	}

	void DBStats::save() {
		try {
			createIndexes();

			io::OutputMongoStream writeStream(s_collName, m_uuid);
			io::InputMongoStream readStream(m_mongoCl, s_collName, m_uuid);

			writeStream.mongoCl() = &m_mongoCl;

			m_numInvalidPnl = DBStats::countInvalidPnl(m_stats);

			/// Check whether we're going to prune this or not ...

			if (m_shouldPrune) {
				bool shouldPrune = checkPruned();

				if (shouldPrune) {
					Debug("STATS", "Puning: %s", m_uuid);

					std::shared_ptr<io::OutputMongoStream> alphaWriteStream = nullptr;

					if (m_iid.empty())
						alphaWriteStream = std::make_shared<io::OutputMongoStream>(m_configCollName, m_uuid);
					else {
						io::ObjectId oid(m_iid);
						alphaWriteStream = std::make_shared<io::OutputMongoStream>(m_configCollName, oid);
					}

					alphaWriteStream->mongoCl() = &m_mongoCl;
					bool prune = true;
					alphaWriteStream->write("pruned", prune);
					alphaWriteStream->flush();

					/// TODO: If this is pruned then we need to delete the per stock stats

					return;
				}
			}

			Trace("STATS", "[%s] - Writing statistics to the DB ...", m_uuid);

			if (!m_alwaysOverwrite && readStream.isValid()) {
				Trace("STATS", "Reading existing stats");

				std::string _id;
				PESA_READ_VAR(readStream, _id);
				ASSERT(_id == m_uuid, "IDs do not match with the one in the InputStream. Expecting: " << m_uuid << " - Found: " << _id);

				//readStream.read("info", this);

				LifetimeStatistics existingStats;
				existingStats.read(readStream);

				/// If the new stats cover the same sort of range that the 
				/// old stats did then we just replace the old document
				/// with the new one
				if (m_stats.startDate <= existingStats.startDate &&
					m_stats.endDate >= existingStats.endDate) {
					Trace("STATS", "Even with existing stats, replacing existing stats!");

					writeStream.isUpdate() = false;
					writeStream.replace() = true;

					m_stats.write(writeStream);
					saveMinimal(m_stats);
				}
				else {
					Trace("STATS", "Writing delta from: %d - %d", m_stats.startDate, m_stats.endDate);

					existingStats.uuid = m_uuid;
					writeStream.isUpdate() = true; 
					writeStream.replace() = false;

					/// Now we wanna modify according to the existing stats.
					/// So we are essentially going to write the existing stats 
					/// but the new stats are going to be used as a delta between
					/// the existing ones to the new ones ...
					existingStats.deltaStats = &m_stats;
					existingStats.write(writeStream);
					m_numInvalidPnl = DBStats::countInvalidPnl(m_stats);

					saveMinimal(existingStats);
				}
			}
			else {
				Trace("STATS", "Writing all the stats to the database ...");
				writeStream.isUpdate() = false;
				writeStream.replace() = m_alwaysOverwrite && readStream.isValid() ? true : false;

				//ASSERT(m_uuid == m_stats.uuid, "UUID mismatch. Expecting: " << m_uuid << " - Found: " << m_stats.uuid);
				writeStream.write("_id", m_uuid);
				m_stats.write(writeStream);

				saveMinimal(m_stats);
			}

			writeStream.write("info", *this);

			Debug("STATS", "[%s] - Finished writing stats. Flusing!", m_uuid);
			writeStream.flush();
		}
		catch (const std::exception& e) {
			Error("DBStats", "[UUID: %s] - Exception: %s", m_uuid, std::string(e.what()));
		}
	}

	io::InputStream& DBStats::read(io::InputStream& is) {
		PESA_READ_CVAR(is, numInvalidPnl);
		return is;
	}
	
	io::OutputStream& DBStats::write(io::OutputStream& os) {
		PESA_WRITE_CVAR(os, numInvalidPnl);
		return os;
	}
	
} /// namespace pesa

