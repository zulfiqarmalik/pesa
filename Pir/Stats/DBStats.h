/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// DBStats.h
///
/// Created on: 11 Sep 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/Framework.h"
#include "Framework/Data/Statistics.h"
#include "Helper/DB/MongoStream.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// DBStats - The Statistics
	////////////////////////////////////////////////////////////////////////////
	class DBStats : public io::Streamable {
	private:
		static const std::string	s_collName;
		static const std::string	s_searchCollName;

		const Config*				m_config = nullptr;			/// The parent config
		std::string					m_uuid;						/// The UUID of the stats
		std::string					m_iid;						/// Object id string
		LifetimeStatistics& 		m_stats; 					/// The actual stats within the system
		int							m_numInvalidPnl = 0;		/// The number of invalid PNLs
		io::MongoClient&			m_mongoCl;					/// The mongo client
		bool						m_alwaysOverwrite = false;	/// Always overwrite the stats or not
		bool						m_writeDetailed = false;	/// Write detaild data to the stats DB

		std::string					m_configCollName = "AlphaConfigs";

		bool						m_monthlyStats = false;		/// Write monthly stats
		bool						m_weeklyStats = false;		/// Write weekly stats
		bool						m_quarterlyStats = true;	/// Write quarterly stats
		bool						m_annualStats = true;		/// Write annual stats
		bool						m_shouldPrune = true;		/// Should we prune or not

		void						createIndexes();
		void						createSearchIndexes(const std::string& collName);

		void						saveSearch(const std::string& collName, LifetimeStatistics& ltStats, Statistics& stats, IntDate startDate = 0, IntDate endDate = 0);
		void						saveSearch(const std::string& collName, LifetimeStatistics& ltStats, IndexedRangeStatisticsVec& rstats, size_t offset);
		void						saveMinimal(LifetimeStatistics& stats);

		static int					countInvalidPnl(LifetimeStatistics& stats);
		bool						checkPruned();
		static bool					checkPrunedGeneric(const ConfigSection* sec, const CumStatistics& stats);
		bool						checkLTPruned();
		bool						checkAnnualPruned();

	public:
									DBStats(const Config* config, io::MongoClient& mongoCl, 
										const std::string& uuid, LifetimeStatistics& stats, 
										bool quarterlyStats = true, bool monthlyStats = false, bool weeklyStats = false);
		virtual 					~DBStats();

		////////////////////////////////////////////////////////////////////////////
		/// DB Handling
		////////////////////////////////////////////////////////////////////////////
		bool 						load();
		void 						save();

		//////////////////////////////////////////////////////////////////////////
		/// Streamable implementation
		//////////////////////////////////////////////////////////////////////////
		virtual io::InputStream&	read(io::InputStream& is);
		virtual io::OutputStream&	write(io::OutputStream& os);

		//////////////////////////////////////////////////////////////////////////
		/// Inline functions
		//////////////////////////////////////////////////////////////////////////
		inline LifetimeStatistics&	stats() { return m_stats; }
		inline bool&				monthlyStats() { return m_monthlyStats; } 
		inline bool&				weeklyStats() { return m_weeklyStats; }
		inline bool&				quarterlyStats() { return m_quarterlyStats; }
		inline bool&				annualStats() { return m_annualStats; }
		inline std::string&			configCollName() { return m_configCollName; }
		inline bool&				shouldPrune() { return m_shouldPrune; }
		inline std::string&			iid() { return m_iid; }
		inline bool&				alwaysOverwrite() { return m_alwaysOverwrite; }
		inline bool					alwaysOverwrite() const { return m_alwaysOverwrite; }
		inline bool&				writeDetailed() { return m_writeDetailed; }
		inline bool					writeDetailed() const { return m_writeDetailed; }
	};

	typedef std::shared_ptr<DBStats> DBStatsPtr;
} /// namespace pesa
