/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// StatsWriter.cpp
///
/// Created on: 14 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/Framework.h"
#include <unordered_map>
#include "Core/Math/Stats.h"
#include "Core/IO/BasicStream.h"

#include "Poco/FileStream.h"
#include "Poco/File.h"
#include "Poco/String.h"
#include "Poco/Thread.h"

#include "Helper/DB/MongoStream.h"
#include "Helper/DB/MongoFileStream.h"
#include "Poco/Process.h"
#include "Poco/TemporaryFile.h"

#include "Core/Lib/Profiler.h"
#include "DBStats.h"

namespace pesa {
	typedef std::shared_ptr<Poco::FileOutputStream> FOSPtr;
	typedef std::unordered_map<std::string, FOSPtr> StrFOSMap;

	struct TickerDumpInfo {
		Constants::Level		level = Constants::kLvlNone;
		StringVec				tickers;
		SecurityIndexVec		tickerIndexes;
	};

	////////////////////////////////////////////////////////////////////////////
	/// StatsWriter - Executes all the alphas
	////////////////////////////////////////////////////////////////////////////
	class StatsWriter : public IPipelineComponent {
	private:
		typedef std::unordered_map<std::string, LifetimeStatisticsPtr> LifetimeStatisticsPtrMap;

		StrFOSMap				m_alphaStreams;						/// Open alpha streams
		TickerDumpInfo			m_dumpInfo;
		bool					m_debug = false;
		bool					m_fileWriteStats = true;
		bool					m_writeBinary = false;				/// Write binary stats to the file
		bool					m_dbWriteStats = false;
		bool					m_writePortfolio = false;
		bool					m_writePerStockStats = false;		/// Write the detailed (per stock) pnls to the file/DB
		bool					m_perStockStatsAlpha = false;		/// Write the detailed (per stock) pnls to the file @ Alpha level
		bool					m_perStockStatsPortfolio = true;	/// Write the detailed (per stock) pnls to the file @ Portfolio level
		bool					m_writeStatsBlob = false;			/// Write stats blob or not ...
		bool					m_alwaysOverwriteDBStats = false;	/// Should we always overwrite DB stats or not?
		bool					m_writeDetailedDBStats = false;		/// Always write detailed stats to the database or not
		std::string				m_fileWriteDir;						/// Directory to write the file pnls into
		std::string				m_perStockWriteDir;					/// Directory to write per stock pnls into 
		bool					m_generateThumbnail = false;		/// Generate a thumbnail for this strategy or not
		std::string				m_generateThumbnailScript = "{$APP}/Tools/miniPlot.py";
		IntDay					m_diLast = 0;

		bool					m_writeMonthly = true;				/// Write monthly stats to the DB
		bool					m_writeQuarterly = true;			/// Write quarterly stats to the DB
		bool					m_writeWeekly = false;				/// Write weekly stats to the DB
		bool					m_writeAnnual = true;				/// Write annual stats to the DB
		bool					m_livePnlStream = false;			/// Write a live pnl stream
		bool					m_livePnlStreamPortfolio = false;	/// Write a live pnl stream of the portfolio
		bool					m_livePnlStreamAlpha = false;		/// Write a live pnl stream for each of the alphas

		const ConfigSection*	m_statsSec = nullptr;

		io::MongoClientPtr		m_mongoCl = nullptr;
		LifetimeStatisticsPtrMap m_stats;

		void					writeStatsToDB(const RuntimeData& rt);
		void					writeDB(const std::string& uuid, LifetimeStatistics& ltStats);

		void					writePssDB(const RuntimeData& rt);
		void					writePssDB(const RuntimeData& rt, const AlphaResult& ar, std::string iid);

		void					generateThumbnail(const RuntimeData& rt);
		void					generateThumbnail(const RuntimeData& rt, const AlphaResult& ar, const std::string& statsType, std::string iid);
		void					writeBlob(const RuntimeData& rt, const std::string& uuid, LifetimeStatisticsPtr ltStats);

		void					writeLivePnlStream(const RuntimeData& rt, const AlphaResult& ar, std::string iid);
		void					writeLivePnlStream(const RuntimeData& rt);

		void					writeAllStatsToFile(const RuntimeData& rt);
		void					writeAllStatsToFileDi(const RuntimeData& rt, IntDay di);
		void					write(const RuntimeData& rt, const AlphaResult& result, IntDay di, Constants::Level level, TickerDumpInfo& dumpInfo);
		void					writeTicker(const AlphaResult& result, const std::string& ticker, IntDay di, Security::Index ii);
		void					writePerStockStats(FOSPtr perStockStream, const AlphaResult& result, IntDay di);

		void					writePssData(const std::string& dbName, io::ObjectId& id, IntDate date, const char* suffix, const FloatVec& data);

		FOSPtr					getStream(const std::string& id, bool& newlyCreated);
		FOSPtr					getPerStockStream(const RuntimeData& rt, const std::string& id, IntDay di);
		SecurityIndexVec		resolveTickerIndexes(const IAlpha* alpha, IntDay di, StringVec& tickers);
		io::MongoClient*		mongoCl(const std::string& dbName);

	public:
								StatsWriter(const ConfigSection& config);
		virtual 				~StatsWriter();

		////////////////////////////////////////////////////////////////////////////
		/// IPipelineComponent overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			tick(const RuntimeData& rt);
		virtual void 			finalise(RuntimeData& rt);
	};

	StatsWriter::StatsWriter(const ConfigSection& config) 
		: IPipelineComponent(config, "StatsWriter") {
		PTrace("Created StatsWriter!");

		auto portConfig = parentConfig()->portfolio();
		const ConfigSection* dumpInfoSec = portConfig.getChildSection("Dump");
		m_debug = parentConfig()->portfolio().debug();
		bool doDump = m_debug && dumpInfoSec ? true : false;
		TickerDumpInfo& dumpInfo = m_dumpInfo;

		if (doDump) {
			std::string slevel;

			if (dumpInfoSec->get("level", slevel)) {
				slevel = Poco::toLower(slevel);
				if (slevel == "alpha")
					dumpInfo.level = Constants::kLvlAlpha;
				else if (slevel == "rawportfolio" || slevel == "raw_portfolio")
					dumpInfo.level = Constants::kLvlRawPortfolio;
				else if (slevel == "preoptportfolio" || slevel == "preopt_portfolio")
					dumpInfo.level = Constants::kLvlPreOptPortfolio;
				else
					dumpInfo.level = Constants::kLvlFinalPortfolio;
			}

			std::string tickers = dumpInfoSec->get("tickers");
			ASSERT(!tickers.empty(), "Cannot have a dump section under portfolio without specifying which tickers to dump");
			dumpInfo.tickers = Util::split(tickers, ",");
		}

		m_statsSec = parentConfig()->portfolio().getChildSection("Stats");
		const ConfigSection* statsSec = parentConfig()->portfolio().getChildSection("Stats");

		if (statsSec) {
			m_fileWriteStats = statsSec->getBool("writeFile", m_fileWriteStats);
			m_writeBinary = statsSec->getBool("writeBinary", m_writeBinary);
			m_writePerStockStats = statsSec->getBool("perStockStats", m_writePerStockStats);
			m_perStockStatsAlpha = statsSec->getBool("perStockStatsAlpha", m_perStockStatsAlpha);
			m_perStockStatsPortfolio = statsSec->getBool("perStockStatsPortfolio", m_perStockStatsPortfolio);

			m_fileWriteDir = statsSec->getString("fileDir", "pnl");
			m_fileWriteDir = parentConfig()->defs().resolve(m_fileWriteDir, 0U);

			if (m_fileWriteDir.find("/") == std::string::npos && m_fileWriteDir.find("\\") == std::string::npos) {
				Poco::Path configPath(parentConfig()->filename());
				configPath.pushDirectory(m_fileWriteDir);
				m_fileWriteDir = configPath.makeParent().toString();
			}

			if (m_writePerStockStats) {
				m_perStockWriteDir = statsSec->getString("perStockStatsDir", "pss");
				m_perStockWriteDir = parentConfig()->defs().resolve(m_perStockWriteDir, 0U);

				if (m_perStockWriteDir.find("/") == std::string::npos && m_perStockWriteDir.find("\\") == std::string::npos) {
					Poco::Path configPath(parentConfig()->filename());
					configPath.pushDirectory(m_perStockWriteDir);
					m_perStockWriteDir = configPath.makeParent().toString();
				}
			}

			m_dbWriteStats				= statsSec->getBool("writeDB", false);
			m_writeStatsBlob			= statsSec->getBool("writeStatsBlob", m_writeStatsBlob);
			m_alwaysOverwriteDBStats	= statsSec->getBool("alwaysOverwriteDBStats", m_alwaysOverwriteDBStats);
			m_writeDetailedDBStats		= statsSec->getBool("writeDetailedDBStats", m_writeDetailedDBStats);
			m_writePortfolio			= statsSec->getBool("portfolio", m_writePortfolio);
			m_generateThumbnail			= statsSec->getBool("generateThumbnail", m_generateThumbnail);

			m_writeWeekly				= statsSec->getBool("writeWeekly", m_writeWeekly);
			m_writeMonthly				= statsSec->getBool("writeMonthly", m_writeMonthly);
			m_writeQuarterly			= statsSec->getBool("writeQuarterly", m_writeQuarterly);
			m_writeAnnual				= statsSec->getBool("writeAnnual", m_writeAnnual);
			m_livePnlStream				= statsSec->getBool("livePnlStream", m_livePnlStream);
			m_livePnlStreamPortfolio	= statsSec->getBool("livePnlStreamPortfolio", m_livePnlStreamPortfolio);
			m_livePnlStreamAlpha		= statsSec->getBool("livePnlStreamAlpha", m_livePnlStreamAlpha);
			m_generateThumbnailScript	= statsSec->getResolvedString("generateThumbnailScript", m_generateThumbnailScript);
		}
	}

	StatsWriter::~StatsWriter() {
		PTrace("Deleting StatsWriter!");
	}

	io::MongoClient* StatsWriter::mongoCl(const std::string& dbName) {
		if (!m_mongoCl)
			m_mongoCl = std::make_shared<io::MongoClient>(dbName.c_str());
		return m_mongoCl.get();

		//auto alphaDBName = dbName;

		//if (m_statsSec && m_statsSec->getBool("portfolio", false) == true) {
		//	auto pos = dbName.find("_strats");
		//	if (pos == std::string::npos) {
		//		PDebug("Portfolio writing is enabled (this is a strats db), so we won't be writing alpha stats");
		//		return;
		//	}

		//	alphaDBName = dbName.substr(0, pos);
		//}

		//m_mongoCl = std::make_shared<io::MongoClient>(dbName.c_str());
	}

	FOSPtr StatsWriter::getStream(const std::string& id, bool& newlyCreated) {
		StrFOSMap::iterator iter = m_alphaStreams.find(id);

		if (iter != m_alphaStreams.end())
			return iter->second;

		Poco::Path path(m_fileWriteDir);
		Poco::File dir(path);

		if (!dir.exists()) {
			PInfo("Creating PNL Dir for dumping stats: %s", path.toString());
			dir.createDirectories();
		}

		path.append(id);
		path.setExtension("pnl");

		PTrace("Opening alpha PNL stream: %s", path.toString());
		FOSPtr stream = std::make_shared<Poco::FileOutputStream>(path.toString());
		m_alphaStreams[id] = stream;
		newlyCreated = true;

		return stream;
	}

	FOSPtr StatsWriter::getPerStockStream(const RuntimeData& rt, const std::string& id, IntDay di) {
		Poco::Path path(m_perStockWriteDir);
		path.append(id);

		Poco::File dir(path);

		if (!dir.exists()) {
			PInfo("Creating PNL Dir for dumping stats: %s", path.toString());
			dir.createDirectories();
		}

		IntDate date = rt.dates[di];

		if (!rt.isLastDi(di))
			path.append(id + "_" + Util::cast(date));
		else
			path.append(id);

		path.setExtension("pss");

		PTrace("Opening per-stock stream: %s", path.toString());
		return std::make_shared<Poco::FileOutputStream>(path.toString());
	}

	SecurityIndexVec StatsWriter::resolveTickerIndexes(const IAlpha* alpha, IntDay di, StringVec& tickers) {
		const IUniverse* universe = alpha->universe();
		size_t size = alpha->alpha().numItems();

		if (tickers.size() == 1 && tickers[0] == "*") {
			SecurityIndexVec tickerIndexes(size);
			tickers.resize(size);
			for (size_t ii = 0; ii < tickerIndexes.size(); ii++) {
				Security sec = universe->security(di, (Security::Index)ii);
				tickerIndexes[ii] = (Security::Index)ii;
				tickers[ii] = Util::split(sec.symbolStr(), " ")[0];
			}
			return tickerIndexes;
		}

		SecurityIndexVec tickerIndexes(tickers.size());
		for (size_t i = 0; i < tickerIndexes.size(); i++)
			tickerIndexes[i] = Security::s_invalidIndex;

		size_t numValid = 0;

		for (size_t ii = 0; ii < size && numValid < tickers.size(); ii++) {
			Security sec = universe->security(di, (Security::Index)ii);
			std::string secTicker = sec.tickerStr();
			std::string uuid = sec.uuidStr();

			for (size_t j = 0; j < tickerIndexes.size(); j++) {
				if (tickerIndexes[j] == Security::s_invalidIndex && 
					(secTicker.find(tickers[j]) != std::string::npos || uuid == tickers[j])) {
					numValid++;
					tickerIndexes[j] = (Security::Index)ii;
				}
			}
		}

		return tickerIndexes;
	}

	void StatsWriter::writePerStockStats(FOSPtr stream, const AlphaResult& results, IntDay di) {
		(*stream) << CalcData::perStockHeaderString(false, false) << std::endl;
		(*stream) << results.data->toPerStockString(false, false) << std::endl;
	}

	void StatsWriter::writeTicker(const AlphaResult& results, const std::string& ticker, IntDay di, Security::Index ii) {
		bool newlyCreated = false;
		std::string alphaUuid = results.alpha->uuid();
		FOSPtr stream = getStream(ticker + "_" + results.alpha->uuid(), newlyCreated);

		if (newlyCreated) {
			(*stream) << CalcData::tickerHeaderString(false, false) << std::endl;
		}

		(*stream) << results.data->toString(ii, false, false) << std::endl;
	}

	void StatsWriter::writeBlob(const RuntimeData& rt, const std::string& uuid, LifetimeStatisticsPtr ltStats) {
		PDebug("Writing blob stats for: %s", uuid);

		Poco::Path path(m_fileWriteDir);
		Poco::File dir(path);

		if (!dir.exists()) {
			PInfo("Creating PNL Dir for dumping stats: %s", path.toString());
			dir.createDirectories();
		}

		path.append(uuid);
		path.setExtension("bpnl");

		PTrace("Opening alpha binary pnl stream: %s", path.toString());
		auto stream = io::BasicOutputStream::createFileWriter(path.toString());
		ltStats->write(*stream.get());
	}

	void StatsWriter::write(const RuntimeData& rt, const AlphaResult& results, IntDay di, Constants::Level level, TickerDumpInfo& dumpInfo) {
		PROFILE_SCOPED();

		bool newlyCreated = false;

		LifetimeStatisticsPtr ltStats = results.data->cumStats;
		std::string alphaUuid = results.alpha->uuid();

		/// Write the children first ...
		for (auto& childAlpha : results.childAlphas) {
			/// calculate recursively ...
			write(rt, childAlpha, di, level, dumpInfo);
		}

		if (!ltStats)
			return;

		/// The reason why we quit the file write stats section over here is that we have to collect
		/// DB stats in our map if we're beeing asked to collect DB stats
		if (level == Constants::kLvlAlpha && ltStats && m_dbWriteStats && results.alpha->dbWriteStats())
			m_stats[alphaUuid] = ltStats;
		if ((level == Constants::kLvlRawPortfolio || 
			level == Constants::kLvlPreOptPortfolio || 
			level == Constants::kLvlFinalPortfolio) && ltStats && m_dbWriteStats && m_writePortfolio)
			m_stats[alphaUuid] = ltStats;

		if (!ltStats)
			return;

		if (!results.alpha->fileWriteStats())
			return;

		/// if we're not going to be writing alpha stats
		if ((level != Constants::kLvlRawPortfolio && level != Constants::kLvlPreOptPortfolio &&
			level != Constants::kLvlFinalPortfolio) && (m_statsSec && m_statsSec->getBool("alpha", false) == false)) {
			return;
		}

		if (!m_fileWriteStats || m_writeBinary)
			return;

		if (m_writePerStockStats) {
			if ((m_perStockStatsAlpha && level == Constants::kLvlAlpha) ||
				(m_perStockStatsPortfolio && level == Constants::kLvlFinalPortfolio)) {
				FOSPtr perStockStream = getPerStockStream(rt, alphaUuid, di);
				writePerStockStats(perStockStream, results, di);
			}
		}

		FOSPtr stream = getStream(alphaUuid, newlyCreated);
		Statistics stats(results.alpha->stats());

		if (level == dumpInfo.level) {
			if (dumpInfo.tickers.size() && !dumpInfo.tickerIndexes.size()) {
				dumpInfo.tickerIndexes = resolveTickerIndexes(results.alpha.get(), di, dumpInfo.tickers);
			}

			for (size_t i = 0; i < dumpInfo.tickerIndexes.size(); i++) {
				Security::Index ii = dumpInfo.tickerIndexes[i];

				if (ii != Security::s_invalidIndex)
					writeTicker(results, dumpInfo.tickers[i], di, ii);
			}
		}

		if (newlyCreated) {
			(*stream) << stats.headerString(false, false) << std::endl;
			auto numPoints = (int)ltStats->vec.size();

			if (numPoints > 1) {
				auto pnl = ltStats->pnl;
				ltStats->pnl = 0.0f;

				for (int i = 0; i < numPoints - 1; i++) {
					auto& ptStats = ltStats->vec[i];
					ltStats->pnl += ptStats.pnl;
					std::string str = ptStats.toString(false, false, ltStats.get());
					(*stream) << str << std::endl;
				}

				ltStats->pnl = pnl;
			}

			/// Dump all the initial pnl output
			//for (const std::string& pnlPt : ltStats->pnlOutput)
			//	(*stream) << pnlPt << std::endl; 
		}

		std::string str = stats.toString(false, false, ltStats.get());
		(*stream) << str << std::endl;
		//ltStats->pnlOutput.push_back(str);

		if (rt.isLastDi(di)) {
			(*stream) << "\n";
			(*stream) << "###########################################################################\n";
			(*stream) << "########################### DETAILED STATS ################################\n";
			(*stream) << "###########################################################################\n";
			(*stream) << "\n";

			(*stream) << CumStatistics::detailedHeaderString(false, false) << std::endl;

			(*stream) << "# D5\n";
			(*stream) << ltStats->D5.detailedString(false, false) << std::endl;
			(*stream) << "# D10\n";
			(*stream) << ltStats->D10.detailedString(false, false) << std::endl;
			(*stream) << "# D21\n";
			(*stream) << ltStats->D21.detailedString(false, false) << std::endl;
			(*stream) << "# D63\n";
			(*stream) << ltStats->D63.detailedString(false, false) << std::endl;
			(*stream) << "# D120\n";
			(*stream) << ltStats->D120.detailedString(false, false) << std::endl;
			(*stream) << "# D250\n";
			(*stream) << ltStats->D250.detailedString(false, false) << std::endl;
			(*stream) << "# InSample\n";
			(*stream) << ltStats->inSample.detailedString(false, false) << std::endl;
			(*stream) << "# OutSample-" << ltStats->outSample.startDate << "\n";
			(*stream) << ltStats->outSample.detailedString(false, false) << std::endl;
			(*stream) << "# PublishSample-" << ltStats->publishSample.startDate << "\n";
			(*stream) << ltStats->publishSample.detailedString(false, false) << std::endl;

			for (auto iter = ltStats->annual.begin(); iter != ltStats->annual.end(); iter++) {
				auto& astats = iter->second;
				(*stream) << "# Y" << astats.year << "\n";
				(*stream) << astats.detailedString(false, false) << std::endl;
			}

			(*stream) << "# LT\n";
			(*stream) << ltStats->detailedString(false, false) << std::endl;

			if (m_writeStatsBlob) {
				writeBlob(rt, alphaUuid, ltStats);
			}
		}
	}

	void StatsWriter::writeAllStatsToFile(const RuntimeData& rt) {
		PROFILE_SCOPED();

		///// If we're not writing file or DB stats then we don't need to continue
		//if (!m_fileWriteStats && !m_dbWriteStats)
		//	return;

		//if (!const_cast<Config*>(parentConfig())->portfolio().alphasSec())
		//	return;

		//ConfigSection& alphasConfig = *(const_cast<Config*>(parentConfig())->portfolio().alphasSec());
		//auto* childAlphasConfig = alphasConfig.alphasSec();
		//auto alphas = alphasConfig.alphas();

		///// There is no alphas section!
  //      //		if (!alphas.size())
		////	return;

		/// We get the alpha values from the previous day since we can only 
		/// calculate previous days' PNL on the current day. Todays PNL
		/// will be calculated tomorrow going by this logic!
		IntDay di = rt.di;
		writeAllStatsToFileDi(rt, di);
	}

	void StatsWriter::writeAllStatsToFileDi(const RuntimeData& rt, IntDay di) {
		PROFILE_SCOPED();

		/// We get the alpha values from the previous day since we can only 
		/// calculate previous days' PNL on the current day. Todays PNL
		/// will be calculated tomorrow going by this logic!
		const AlphaResultData* result = nullptr;

		DataInfo dinfo;
		dinfo.noAssertOnMissingDataLoader = true;

		if ((result = rt.dataRegistry->get<AlphaResultData>(nullptr, IAlpha::genId(di), &dinfo)) != nullptr)
			write(rt, *(result->dataPtr()), di, Constants::kLvlAlpha, m_dumpInfo);
		if ((result = rt.dataRegistry->get<AlphaResultData>(nullptr, IAlpha::genId(di, "Portfolio"), &dinfo)) != nullptr)
			write(rt, *(result->dataPtr()), di, Constants::kLvlFinalPortfolio, m_dumpInfo);
		if ((result = rt.dataRegistry->get<AlphaResultData>(nullptr, IAlpha::genId(di, "RawPortfolio"), &dinfo)) != nullptr)
			write(rt, *(result->dataPtr()), di, Constants::kLvlRawPortfolio, m_dumpInfo);
		if ((result = rt.dataRegistry->get<AlphaResultData>(nullptr, IAlpha::genId(di, "NoOptPortfolio"), true, nullptr, true)) != nullptr)
			write(rt, *(result->dataPtr()), di, Constants::kLvlPreOptPortfolio, m_dumpInfo);

		m_diLast = di;
	}

	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	void StatsWriter::writePssData(const std::string& dbName, io::ObjectId& id, IntDate date, const char* suffix, const FloatVec& data) {
		if (!data.size())
			return;

		const size_t numRetries = 5;
		bool didWrite = false;
		size_t tryId = 0;

		while (!didWrite && tryId++ <= numRetries) {
			try {
				std::string sid = id.to_string() + "_" + Util::cast<int>(date);

				//if (!m_writeBinary || (!m_fileWriteStats && m_dbWriteStats)) 
				{
					io::OutputMongoStream os(std::string("PSS_") + std::string(suffix), sid, nullptr, dbName.c_str());

					os.isUpdate() = false;
					os.replace() = true;
					os.mongoCl() = mongoCl(dbName);

					os.write("iid", id);
					os.write("date", (int)date);
					os.write("data", (const void*)&data[0], data.size() * sizeof(float));

					os.flush();
				}
				//else {
				//	/// OK, we write this to the file now
				//	Poco::Path path(m_fileWriteDir);
				//	path.append(id.to_string());
				//	path.append(std::string("PSS_") + suffix);

				//	Poco::File dir(path);

				//	if (!dir.exists()) {
				//		PInfo("Creating POS Dir for dumping positions: %s", path.toString());
				//		dir.createDirectories();
				//	}

				//	path.append(sid);
				//	auto os = io::BasicOutputStream::createBinaryWriter(path.toString());
				//	//io::MongoFileOutputStream os(sid, "PSS", dbName.c_str());
				//	os->write(sid.c_str(), (const char*)&data[0], data.size() * sizeof(float));
				//	os->flush();
				//}

				didWrite = true;
			}
			catch (const std::exception& e) {
				//Util::reportException(e);
				PError("Error while writing stats: %s to DB [Date: %u, Retry: %z]. Exception: %s", std::string(suffix), date, tryId, std::string(e.what()));
			}
			catch (...) {
				PError("Unknown error while writing stats: %s to DB [Date: %u, Retry: %z]", std::string(suffix), date, tryId);
			}

			if (!didWrite) {
				/// release the connection ... we'll get it again!
				m_mongoCl = nullptr;
				Poco::Thread::sleep((long)2000);
			}
		}

		ASSERT(didWrite, "Unable to write stats to the DB: " << suffix << ". Fatal application error. Aborting ...");
	}

	void StatsWriter::writePssDB(const RuntimeData& rt, const AlphaResult& ar, std::string iid) {
		for (const auto& car : ar.childAlphas)
			writePssDB(rt, car, car.alpha->config().getString("iid", ""));

		//std::string iid = parentConfig()->portfolio().getRequired("iid");
		//ASSERT(!iid.empty() && iid != "MyPort", "Invalid iid for writing positions to DB: " << iid);

		iid = iid.empty() ? ar.alpha->config().getString("iid", "") : iid;

		/// If there is no iid then we don't do anything
		if (iid.empty())
			return;

		IntDate di = rt.di;
		IntDate date = rt.dates[di];
		io::ObjectId id(iid);
		std::string dbName = parentConfig()->dbName();
		std::string strDate = Util::cast(date);

		FloatVec empty;
		const CalcData& data = *(ar.data.get());
		const FloatVec& prevAlphaNotional = data.yesterday ? data.yesterday->alphaNotionals : empty;
		const FloatVec& prevAlpha = data.yesterday ? data.yesterday->rawAlpha : empty;
		//const FloatVec& lowPnl = data.alphaLowPnls;
		//const FloatVec& highPnl = data.alphaHighPnls;
		const FloatVec& closePnl = data.alphaPnls;

		writePssData(dbName, id, date, "Notionals", prevAlphaNotional);
		writePssData(dbName, id, date, "Alphas", prevAlpha);
		writePssData(dbName, id, date, "ClosePnls", closePnl);
		writePssData(dbName, id, date, "Turnovers", data.alphaTurnover);
		writePssData(dbName, id, date, "Costs", data.alphaCosts);
	}

	void StatsWriter::writePssDB(const RuntimeData& rt) {
		if (!m_dbWriteStats)
			return;

		DataInfo dinfo;
		dinfo.noAssertOnMissingDataLoader = true;

		if (m_perStockStatsPortfolio) {
			const AlphaResultData* result = rt.dataRegistry->get<AlphaResultData>(nullptr, IAlpha::genId(rt.di, "Portfolio"), &dinfo);
			writePssDB(rt, *(result->dataPtr()), parentConfig()->portfolio().getRequired("iid"));
		}
		if (m_perStockStatsAlpha) {
			const AlphaResultData* result = rt.dataRegistry->get<AlphaResultData>(nullptr, IAlpha::genId(rt.di), &dinfo);
			writePssDB(rt, *(result->dataPtr()), "");
		}
	}

	void StatsWriter::writeLivePnlStream(const RuntimeData& rt) {
		if (!m_livePnlStream)
			return;

		DataInfo dinfo;
		dinfo.noAssertOnMissingDataLoader = true;

		if (m_livePnlStreamPortfolio) {
			const AlphaResultData* result = rt.dataRegistry->get<AlphaResultData>(nullptr, IAlpha::genId(rt.di, "Portfolio"), &dinfo);
			writeLivePnlStream(rt, *(result->dataPtr()), parentConfig()->portfolio().getRequired("iid"));
		}
		if (m_livePnlStreamAlpha) {
			const AlphaResultData* result = rt.dataRegistry->get<AlphaResultData>(nullptr, IAlpha::genId(rt.di), &dinfo);
			writeLivePnlStream(rt, *(result->dataPtr()), "");
		}
	}

	void StatsWriter::writeLivePnlStream(const RuntimeData& rt, const AlphaResult& ar, std::string iid) {
		for (const auto& car : ar.childAlphas)
			writeLivePnlStream(rt, car, car.alpha->config().getString("iid", ""));

		//std::string iid = parentConfig()->portfolio().getRequired("iid");
		//ASSERT(!iid.empty() && iid != "MyPort", "Invalid iid for writing positions to DB: " << iid);

		iid = iid.empty() ? ar.alpha->config().getString("iid", "") : iid;

		/// If there is no iid then we don't do anything
		if (iid.empty())
			return;

		std::string dbName = parentConfig()->dbName();
		io::ObjectId id(iid);
		const CalcData& data = *(ar.data.get());

		IntDate diDate = rt.dates[rt.di];
		float bookSize = m_config.config()->portfolio().bookSize();

		/// This is a full run. Just clean the existing pnl stream and start all over again ...
		if (diDate == rt.startDate) {
			/// Replace the array at the start of the simulation ...
			io::OutputMongoStream os(std::string("LivePnl"), id, nullptr, dbName.c_str());
			os.isUpdate() = true;
			os.replace() = false;
			os.mongoCl() = mongoCl(dbName);

			FloatVec pnlArray;
			IntVec dateArray(rt.diEnd - rt.diStart + 1);

			for (int i = rt.diStart; i <= rt.diEnd; i++)
				dateArray[i - rt.diStart] = rt.dates[i];

			PInfo("Resetting Live PNL to 0!");

			os.write("bookSize", bookSize);
			os.replaceArray("pnls", pnlArray);
			os.replaceArray("dates", dateArray);
			os.flush();
		}
		/// This is loaded from a checkpoint! We need to handle this slightly differently
		else if (rt.di == rt.diStart) { /// implied => && date != rt.startDate
			/// First of all we need to construct the exiting pnls from the cumStats
			FloatVec pnlArray(data.cumStats->vec.size() - 1);
			IntDay diStart = rt.getDateIndex(rt.startDate);
			IntVec dateArray(rt.diEnd - diStart + 1);

			for (size_t i = 0; i < data.cumStats->vec.size() - 1; i++) {
				pnlArray[i] = data.cumStats->vec[i].pnl;
				dateArray[i] = rt.dates[i + diStart];
			}

			for (int i = rt.di; i <= rt.diEnd; i++)
				dateArray[i - diStart] = rt.dates[i];

			PInfo("Live PNL checkpoint adjustment to: %d [%u]!", rt.diStart, rt.startDate);

			/// Now we update the document
			io::OutputMongoStream os(std::string("LivePnl"), id, nullptr, dbName.c_str());
			os.isUpdate() = true;
			os.replace() = false;
			os.mongoCl() = mongoCl(dbName);

			os.write("bookSize", bookSize);
			os.replaceArray("pnls", pnlArray);
			os.replaceArray("dates", dateArray);
			os.flush();
		}

		IntDate di = rt.di;
		IntDate date = rt.dates[di];
		float pnl = data.stats.pnl;

		io::OutputMongoStream os(std::string("LivePnl"), id, nullptr, dbName.c_str());
		os.isUpdate() = true;
		os.replace() = false;
		os.mongoCl() = mongoCl(dbName);

		os.write("_id", id);
		os.push<float>("pnls", pnl);
		//os.push<int>("dates", (int)date);
		//os.write(strDate.c_str(), pnl);
		os.flush();
	}

	void StatsWriter::writeDB(const std::string& uuid, LifetimeStatistics& ltStats) {
		PROFILE_SCOPED();

		//std::string iid = parentConfig()->portfolio().getRequired("iid");
		std::string dbName = parentConfig()->dbName();

		if (ltStats.level == "portfolio" || ltStats.level == "raw_portfolio" || ltStats.level == "pre_opt_portfolio") {
			//if (ltStats.level != "portfolio")
			//	return;

			//if (ltStats.level == "portfolio")
			//	dbName += "_strats_final";
			//else if (ltStats.level == "raw_portfolio")
			//	dbName += "_strats_raw";
			//else if (ltStats.level == "pre_opt_portfolio")
			//	dbName += "_strats_pre_opt";

			std::string iid = parentConfig()->portfolio().getRequired("iid");

			/// If its the raw or pre-opt portfolio then we suffix the relevant strings so that we can easily search
			/// these at a later stage
			if (ltStats.level == "raw_portfolio")
				iid = iid + "_RAW";
			else if (ltStats.level == "pre_opt_portfolio")
				iid = iid + "_PRE_OPT";

			DBStats dbStats(parentConfig(), *mongoCl(dbName), iid, ltStats);
			dbStats.iid() = iid;
			dbStats.monthlyStats() = m_writeMonthly;
			dbStats.annualStats() = m_writeAnnual;
			dbStats.weeklyStats() = m_writeWeekly;
			dbStats.quarterlyStats() = m_writeQuarterly;
			dbStats.shouldPrune() = !parentConfig()->defs().getBool("isUserConfig", false);
			dbStats.alwaysOverwrite() = true;
			dbStats.writeDetailed() = m_writeDetailedDBStats;

			dbStats.save();
		}
		else if (m_statsSec && m_statsSec->getBool("alpha", false) == true) {
			DBStats dbStats(parentConfig(), *mongoCl(dbName), uuid, ltStats);
			dbStats.alwaysOverwrite() = m_alwaysOverwriteDBStats;
			dbStats.weeklyStats() = m_writeWeekly;
			dbStats.quarterlyStats() = m_writeQuarterly;
			dbStats.monthlyStats() = m_writeMonthly;
			dbStats.annualStats() = m_writeAnnual;
			dbStats.alwaysOverwrite() = true;
			dbStats.shouldPrune() = !parentConfig()->defs().getBool("isUserConfig", false);
			dbStats.writeDetailed() = m_writeDetailedDBStats;

			dbStats.save();
		}
	}

	void StatsWriter::writeStatsToDB(const RuntimeData& rt) {
		PROFILE_SCOPED();

		if (!m_dbWriteStats || !m_stats.size())
			return;

		Poco::Timestamp startTime;

		for (LifetimeStatisticsPtrMap::iterator iter = m_stats.begin(); iter != m_stats.end(); iter++) {
			LifetimeStatisticsPtr stats = iter->second;
			writeDB(iter->first, *stats);
		}

		float seconds = (float)startTime.elapsed() * 0.001f * 0.001f;
		PInfo("Writing stats to the DB took: %0.2hf seconds", seconds);
	}

	void StatsWriter::tick(const RuntimeData& rt) {
		PROFILE_SCOPED();

		////io::InputMongoStream readStream("pesa", "Stats", "Alpha1");
		////if (readStream.isValid()) {
		////	LifetimeStatistics ltstats;
		////	ltstats.read(readStream);
		////	std::cout << ltstats.pnl << std::endl;
		////}


		///// We finally write the db stats
		//auto vvalue = cl["pesa"]["Stats"].find_one(bsoncxx::builder::stream::document{} << "_id" << "Alpha1" << bsoncxx::builder::stream::finalize);
		//bsoncxx::document::view view = vvalue.get();
		////mongocxx::cursor cursor = cl["pesa"]["Stats"].find(bsoncxx::builder::stream::document{} << "_id" << "Alpha1" << bsoncxx::builder::stream::finalize);
		////bsoncxx::document::view view = *cursor.begin();
		//bsoncxx::document::element elt = view["_id"];
		//std::string _id = elt.get_utf8().value.to_string();
		////io::InputMongoStream readStream(view);
		//io::InputMongoStream readStream(cl, "pesa", "Stats", "Alpha1");
		//LifetimeStatistics lts;
		//lts.uuid = "Alpha1";
		//lts.read(readStream);
		//std::string id = elt.get_utf8().value.to_string();
		////double pnl = view["annual"]["2016"]["weekly"].get_array().value[32]["pnl"].get_double().value;
		////std::string id = value.get()["_id"];

		////for (auto&& doc : cursor)
		////	std::cout << bsoncxx::to_json(doc) << std::endl;

		writeAllStatsToFile(rt);
		writePssDB(rt);
		writeLivePnlStream(rt);

		if (rt.isLastDi())
			generateThumbnail(rt);
	}

	void StatsWriter::generateThumbnail(const RuntimeData& rt, const AlphaResult& ar, const std::string& statsName, std::string iid) {
		/// Write the children first ...
		for (const auto& car : ar.childAlphas) {
			/// calculate recursively ...
			generateThumbnail(rt, car, statsName, car.alpha->config().getString("iid", ""));
		}

		iid = iid.empty() ? ar.alpha->config().getString("iid", "") : iid;

		const LifetimeStatistics* ltStats = ar.alpha->cumStats();
		
		if (!ltStats)
			return;

		const CumStatistics* cumStats = ltStats->childStats(statsName);

		ASSERT(cumStats, "Unable to find any child stats with name: " << statsName);

		/// If there is no iid then we don't do anything
		if (iid.empty() || !cumStats->size())
			return;

		try {
			PDebug("Generating thumbnail for: %s [%s]", iid, statsName);

			const CalcData& data = *(ar.data.get());

			/// First of all we're going to write a mini PnL file
			Poco::Path configPath(parentConfig()->filename());
			configPath.makeParent();

			Poco::TemporaryFile miniPnlFilePath;
			std::string miniPnlFilename = miniPnlFilePath.path(); /// (Poco::Path(configPath).setFileName(data.cumStats->uuid).setExtension("pnl")).toString();
			PTrace("Generating mini pnl filename: %s - Stats: %s", miniPnlFilename, statsName);

			FOSPtr s_miniPnl = std::make_shared<Poco::FileOutputStream>(miniPnlFilename);

			size_t count = cumStats->size();
			std::string baseCollName = "Thumbnails";
			std::string collName = baseCollName + statsName;

			for (size_t i = 0; i < count; i++) {
				const Statistics* pt = cumStats->at(i);
				ASSERT(pt, "While generating thumbnail: " << collName << ", goto NULL point at: " << i);
				(*s_miniPnl) << Poco::format("%d,%0.2hf\n", pt->date, pt->pnl);
			}

			s_miniPnl = nullptr;

			/// Now we generate the thumbnail 
			Poco::TemporaryFile thmFilePath;
			/// We add .png at the end because matplotlib (Python) adds that extension when saving as a png using savefig(...) function 
			std::string thmFilename = thmFilePath.path() + ".png"; /// (Poco::Path(configPath).setFileName(data.cumStats->uuid).setExtension("png")).toString();
			Poco::ProcessHandle pngProc(Poco::Process::launch("python", { m_generateThumbnailScript, miniPnlFilename, "-f" + thmFilename }));
			int errCode = pngProc.wait();

			if (errCode != 0) 
				PInfo("Failed to generate thumbnail. Process failed with error code: %d", errCode);

			size_t fileSize = 0;
			char* fileData = Util::readEntireFile(thmFilename, fileSize);

			if (fileData) {
				/// Now we write the data to the mongodb stream as BinData
				io::ObjectId id(iid);
				std::string dbName = io::MongoClient::defaultDBName();

				io::OutputMongoStream os(collName, id, nullptr, dbName.c_str());
				os.isUpdate() = false;
				os.replace() = true;
				os.mongoCl() = mongoCl(dbName);

				bsoncxx::types::b_binary binData;
				binData.size = (uint32_t)fileSize;
				binData.bytes = (uint8_t*)fileData;

				os.write("_id", id);

				os.document() << std::string("png") << binData;
				os.flush();

				delete[] fileData;
			}
			else {
				PError("Error generating thumbnail: %s [Couldn't load PNG: %s]", data.cumStats->uuid, thmFilename);
			}

			Util::deleteFile(thmFilename);
		}
		catch (const std::exception& e) {
			PError("Unable to generate thumbnail: %s. Reason: %s", iid, std::string(e.what()));
		}
		catch (...) {
			PError("Unable to generate thumbnail: %s. Reason: CATCH ALL", iid);
		}
	}

	void StatsWriter::generateThumbnail(const RuntimeData& rt) {
		/// If generate thumbnail is not specified
		if (!m_generateThumbnail)
			return; 

		DataInfo dinfo;
		dinfo.noAssertOnMissingDataLoader = true;

		const AlphaResultData* result = rt.dataRegistry->get<AlphaResultData>(nullptr, IAlpha::genId(rt.di), &dinfo);
		std::string iid = result->dataPtr()->alpha->config().getString("iid", "");
		const LifetimeStatistics* ltStats = result->dataPtr()->alpha->cumStats();

		generateThumbnail(rt, *result->dataPtr(), "LT", iid);
		generateThumbnail(rt, *result->dataPtr(), "IS", iid);
		generateThumbnail(rt, *result->dataPtr(), "OS", iid);
		generateThumbnail(rt, *result->dataPtr(), "PS", iid);
	}

	void StatsWriter::finalise(RuntimeData& rt) {
		/// Close all the streams ...
		for (auto& iter : m_alphaStreams)
			iter.second->close();

		/// shared_ptr will delete itself
		m_alphaStreams.clear();

		/// We finally write the db stats
		writeStatsToDB(rt);
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IPipelineComponent* createStatsWriter(const pesa::ConfigSection& config) {
	return new pesa::StatsWriter((const pesa::ConfigSection&)config);
}
