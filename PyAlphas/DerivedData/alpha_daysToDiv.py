### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# Hammad Khan
#######################################################################################################################################
############ CopyRight Hammad Khan ####################################################################################################
#### No Part of psim or associated code or tools can be reproduced or used without prior written permission of Hammad Khan ############
#### This Header has to be used in all permitted authorized usage of psim #############################################################
#######################################################################################################################################

import sys
import traceback
import numpy as np
import datetime
import pandas as pd
from pandas.tseries.offsets import BDay

try:
    import Framework
    import PyUtil
    from PyBase import PyAlpha
    
    class PyTradingDaysToDividend(PyAlpha):
        def __init__(self, baseObject):
            super().__init__(baseObject, "PyTradingDaysToDividend")

        def run(self, rt):
            try:
                di          = rt.di
                d1          = rt.di - 1 # We hardcode this since this itself is going to be a delay-0 data!
                result      = self.alpha()
                universe    = self.getUniverse()
                
                wshDivExDate= rt.dataRegistry.stringData(universe,"wsh.Last_X-Dividend_Date")

                numStocks   = result.cols()

                todayDate   = PyUtil.intToDate(rt.getDate(rt.di))

                for ii in range(numStocks):

                    result.makeInvalid(0, ii)

                    if universe.isValid(di, ii):
                        nextDivDate = None

                        if wshDivExDate.getValue(d1, ii):
                            nextDivDate = PyUtil.strToDate(wshDivExDate.getValue(d1,ii))
                        else:
                            # Nothing to do over here ...
                            continue

                        if todayDate > nextDivDate:
                            continue

                        dates = pd.date_range(todayDate, nextDivDate, freq = BDay())
                        daysToDiv = len(dates)
                        tradingDaysToDiv = float(daysToDiv)

                        # trivial rejection (we don't get all the days if its more than 10 business days left)
                        if daysToDiv > 10:
                            tradingDaysToDiv = 10.0
                        else:
                            tradingDaysToDiv = 0.0
                            for i in range(0, len(dates) - 1):
                                idate = PyUtil.dateToInt(dates[i])
                                if idate not in rt.holidays:
                                    tradingDaysToDiv += 1.0

                        result[ii] = tradingDaysToDiv

            except Exception as e:
                print ("I am at Exception")
                print(traceback.format_exc())

except Exception as e:
    print(traceback.format_exc())
