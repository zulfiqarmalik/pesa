### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# Hammad Khan
#######################################################################################################################################
############ CopyRight Hammad Khan ####################################################################################################
#### No Part of psim or associated code or tools can be reproduced or used without prior written permission of Hammad Khan ############
#### This Header has to be used in all permitted authorized usage of psim #############################################################
#######################################################################################################################################

import sys
import traceback
import numpy as np
import datetime
import pandas as pd
from pandas.tseries.offsets import BDay

try:
    import Framework
    import PyUtil
    from PyBase import PyAlpha
    
    class PyDivAmount(PyAlpha):
        def __init__(self, baseObject):
            super().__init__(baseObject, "PyDivAmount")

        def run(self, rt):
            try:
                di          = rt.di
                d1          = rt.di - 1 # We hardcode this since this itself is going to be a delay-0 data!
                result      = self.alpha()
                universe    = self.getUniverse()
                
                divExDates  = rt.dataRegistry.stringData(universe,"wsh.Last_X-Dividend_Date")
                divAmounts  = rt.dataRegistry.floatData(universe,"wsh.Last_X-Dividend_Amount")

                assert divExDates, "Unable to load wsh.Last_X-Dividend_Date"
                assert divAmounts, "Unable to load wsh.Last_X-Dividend_Amount"

                numStocks   = result.cols()

                todayDate   = PyUtil.intToDate(rt.getDate(rt.di))

                for ii in range(numStocks):
                    result[ii] = 0.0

                    if universe.isValid(di, ii):
                        nextDivDate = None

                        if divExDates.getValue(d1, ii):
                            nextDivDate = PyUtil.strToDate(divExDates.getValue(d1, ii))
                        else:
                            # Nothing to do over here ...
                            continue

                        # if we are not on the dividend date then don't do anything 
                        if todayDate > nextDivDate or todayDate < nextDivDate:
                            continue

                        divAmount = divAmounts.getValue(d1, ii)
                        result[ii] = divAmount

            except Exception as e:
                print ("I am at Exception")
                print(traceback.format_exc())

except Exception as e:
    print(traceback.format_exc())
