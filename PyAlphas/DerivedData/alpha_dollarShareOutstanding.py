### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# Hammad Khan
# standard dev treated with shrinkage
import sys
import traceback
import numpy as np
import pdb
import warnings

try:
    import Framework
    import PyUtil
    from PyBase import PyAlpha

    
    
    class PySharesOutstanding(PyAlpha):
        def __init__(self, baseObject):
            super().__init__(baseObject, "PySharesOutstanding")

        def run(self, rt):
            try:
                di          = rt.di
                d1          = rt.di - self.getDelay()

                result      = PyUtil.floatToNPMatrix(self.alpha())
                universe    = self.getUniverse()
                adjclosePrice = self.getFloatArg(0, "baseData.adjClosePrice")
                shares_outstanding_qtr = rt.dataRegistry.floatData(universe,"ford.Shares_outstanding_from_balance_sheet_quarterly")
                
                adjclose_mat  = PyUtil.floatToNPMatrix(adjclosePrice)
                
                shares_outstanding_qtr_mat = PyUtil.floatToNPMatrix(shares_outstanding_qtr)
                
                #pdb.set_trace()
                warnings.filterwarnings('ignore')

                #print('quarterly:',shares_outstanding_qtr_mat[d1,0])
                #print('annual:',shares_outstanding_ann_mat[d1,0])
                #result[0]     = volatility * reversion
                result[0,:]    = shares_outstanding_qtr_mat[d1,:] * adjclose_mat[d1,:]
               # find indices of when last high occurs
                

            except Exception as e:
                print ("I am at Exception")
                print(traceback.format_exc())

except Exception as e:
    print(traceback.format_exc())
