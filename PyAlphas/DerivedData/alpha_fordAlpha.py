### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# Hammad Khan
import sys
import traceback
import numpy as np
import pdb


try:
    import Framework
    import PyUtil
    from PyBase import PyAlpha


    class PyFordAlpha(PyAlpha):
        def __init__(self, baseObject):
            super().__init__(baseObject, "PyFordAlpha")

        def run(self, rt):
            try:
                di          = rt.di                                               # current day
                d1          = rt.di - self.getDelay()                             # delayed day
                result      = PyUtil.floatToNPMatrix(self.alpha())
                universe    = self.getUniverse()

                
                ford_Alpha     = rt.dataRegistry.floatData(universe,"ford.Alpha")
                np_ford_Alpha     = PyUtil.floatToNPMatrix(ford_Alpha)
                

                #pdb.set_trace()
                result[0]     = np_ford_Alpha[d1,:]

                    # if value is bad then use: result.makeInvalid(0, ii)

            except Exception as e:
                print ("I am at Exception")
                print(traceback.format_exc())


except Exception as e:
    print(traceback.format_exc())
