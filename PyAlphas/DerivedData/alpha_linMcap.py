### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# Hammad Khan
# Maps Market Cap to cross sectional linear function (smaller mcap higher, larger lower)
import sys
import traceback
import numpy as np
import pdb

try:
    import Framework
    import PyUtil
    from PyBase import PyAlpha

    
    
    class PyLinMcap(PyAlpha):
        def __init__(self, baseObject):
            super().__init__(baseObject, "PyLinMcap")

        def run(self, rt):
            try:
                di          = rt.di
                d1          = rt.di - self.getDelay()
                
                reversion   =  int(self.config().getString("reversion","1"))

                result      = PyUtil.floatToNPMatrix(self.alpha())
                universe    = self.getUniverse()
                adjMCap    = rt.dataRegistry.floatData(universe,"baseData.adjMCap")

                npAdjMCap  = PyUtil.floatToNPMatrix(adjMCap)
                
                npAdjMcapLast = np.squeeze(npAdjMCap[d1,:])
                mcap_sort = np.sort(npAdjMcapLast)
                mcap_med = mcap_sort[int(len(npAdjMcapLast)*0.5)]
                
                linMcap    = (mcap_med - npAdjMcapLast) / (npAdjMcapLast + mcap_med)

                result[0]     = reversion * linMcap
               # find indices of when last high occurs
                

            except Exception as e:
                print ("I am at Exception")
                print(traceback.format_exc())

except Exception as e:
    print(traceback.format_exc())
