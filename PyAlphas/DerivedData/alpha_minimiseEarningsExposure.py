### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# Hammad Khan
#######################################################################################################################################
############ CopyRight Hammad Khan ####################################################################################################
#### No Part of psim or associated code or tools can be reproduced or used without prior written permission of Hammad Khan ############
#### This Header has to be used in all permitted authorized usage of psim #############################################################
#######################################################################################################################################

import sys
import traceback
import numpy as np
import datetime

try:
    import Framework
    import PyUtil
    from PyBase import PyAlpha

    class PyMinimiseEarningsExposure(PyAlpha):
        def __init__(self, baseObject):
            super().__init__(baseObject, "PyMinimiseEarningsExposure")

        def run(self, rt):
            try:
                earningsDays= float(self.config().getString("earningsDays", "1")) 
                di          = rt.di
                result      = self.alpha()
                universe    = self.getUniverse()
                numStocks   = result.cols()
                daysToEarnings= rt.dataRegistry.floatData(universe, "derivedData.daysToEarnings")

                for ii in range(numStocks):
                    result.makeInvalid(0, ii)
                    
                    if universe.isValid(di, ii):
                        daysToEarn = daysToEarnings.getValue(rt.di, ii)
                        if daysToEarn <= earningsDays:
                            result[ii] = 1.0

            except Exception as e:
                print ("I am at Exception")
                print(traceback.format_exc())

except Exception as e:
    print(traceback.format_exc())
