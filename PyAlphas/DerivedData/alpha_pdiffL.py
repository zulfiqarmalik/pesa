### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# Hammad Khan
# price diff from last high
import sys
import traceback
import numpy as np
import pdb

try:
    import Framework
    import PyUtil
    from PyBase import PyAlpha

    
    
    class PyPL(PyAlpha):
        def __init__(self, baseObject):
            super().__init__(baseObject, "PyPL")

        def run(self, rt):
            try:
                di          = rt.di
                d1          = rt.di - self.getDelay()

                reversion   =  int(self.config().getString("reversion","1"))
                result      = PyUtil.floatToNPMatrix(self.alpha())
                universe    = self.getUniverse()
                adjClose    = rt.dataRegistry.floatData(universe,"baseData.adjClosePrice")

                npAdjClose  = PyUtil.floatToNPMatrix(adjClose)
                
                

                #pdb.set_trace()
                # lastLow    = np.nanmin(npAdjClose[[np.arange(d1)],:],axis=1) # last high upto d1
                lastLow      = np.nanmin(np.squeeze(npAdjClose[[np.arange(d1)],:]),axis=0) # last low

                pL            = (npAdjClose[d1] - lastLow)/lastLow
                result[0]     = reversion * pL


            except Exception as e:
                print ("I am at Exception")
                print(traceback.format_exc())

except Exception as e:
    print(traceback.format_exc())
