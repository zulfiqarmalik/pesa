### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# Hammad Khan
# standard dev of Price treated with shrinkage
import sys
import traceback
import numpy as np
import pdb
import warnings

try:
    import Framework
    import PyUtil
    from PyBase import PyAlpha

    
    
    class PyPStd(PyAlpha):
        def __init__(self, baseObject):
            super().__init__(baseObject, "PyPStd")

        def run(self, rt):
            try:
                
                nDays       = int(self.config().getString("nDays", "21"))
                nSd         = float(self.config().getString("nDays", "2.0"))

                di          = rt.di
                d1          = rt.di - self.getDelay()


                
                result      = PyUtil.floatToNPMatrix(self.alpha())
                universe    = self.getUniverse()
                adjClose    = rt.dataRegistry.floatData(universe,"baseData.adjClosePrice")

                npAdjClose  = PyUtil.floatToNPMatrix(adjClose)

                
                #pdb.set_trace()
                warnings.filterwarnings('ignore')

                volatility    = np.nanstd(np.squeeze(npAdjClose[[d1 - nDays  + 1 + np.arange(nDays)],:]),axis=0)   # last nDays

                # now treat the volatility such that cross sectional vol is shrunk towards a mean
                mV            = np.nanmean(volatility)
                sV            = np.nanstd(volatility)

                # shrink towards the mean
                temp = volatility
                temp[np.isnan(volatility)] = 0
                
                volatility[temp > (mV + nSd*sV)] = (mV + nSd*sV)
                volatility[temp < (mV - nSd*sV)] = (mV - nSd*sV)


                result[0]     = volatility

               # find indices of when last high occurs
                

            except Exception as e:
                print ("I am at Exception")
                print(traceback.format_exc())

except Exception as e:
    print(traceback.format_exc())
