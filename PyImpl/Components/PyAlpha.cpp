/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// PyAlpha.cpp
///
/// Created on: 13 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "PyImplDefs.h"
#include "PyBase.h"
#include "Data/Data.h"
#include "Components/IAlpha.h"
#include <Poco/Path.h>

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Python IUniverse implementation
	////////////////////////////////////////////////////////////////////////////
	class PyAlpha : public IAlpha, public PyBase {
	public:
								PyAlpha(const ConfigSection& config, const std::string& logChannel);
								~PyAlpha(); 

		PyWrapper1(run, const pesa::RuntimeData&, rt);
		//virtual void			run(const RuntimeData& rt) {
		//	PyObject_CallMethodObjArgs(m_pyInstance, 
		//		py_string("run"), 
		//		(PyObject*)py_cast(pesa::RuntimeData, &rt), NULL);
		//}

		////////////////////////////////////////////////////////////////////////////
		/// Inline functions
		////////////////////////////////////////////////////////////////////////////
	};

	////////////////////////////////////////////////////////////////////////////
	/// IMPLEMENTATION
	////////////////////////////////////////////////////////////////////////////
	PyAlpha::PyAlpha(const ConfigSection& config, const std::string& logChannel) 
		: IAlpha(config, !logChannel.empty() ? logChannel : "PyAlpha")
		, PyBase(config, "pesa::IAlpha", static_cast<IAlpha*>(this)) {
	}
	
	PyAlpha::~PyAlpha() {
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createPyAlpha(const pesa::ConfigSection& config) {
	return new pesa::PyAlpha(config, "");
}
