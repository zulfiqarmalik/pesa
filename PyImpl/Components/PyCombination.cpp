/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// PyCombination.cpp
///
/// Created on: 21 Sep 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "PyImplDefs.h"
#include "PyBase.h"
#include "Data/Data.h"
#include "Components/ICombination.h"
#include <Poco/Path.h>

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Python IUniverse implementation
	////////////////////////////////////////////////////////////////////////////
	class PyCombination : public ICombination, public PyBase {
	public:
								PyCombination(const ConfigSection& config, const std::string& logChannel);
								~PyCombination(); 

		PyWrapper1(run, const pesa::RuntimeData&, rt);
		////////////////////////////////////////////////////////////////////////////
		/// Inline functions
		////////////////////////////////////////////////////////////////////////////
	};

	////////////////////////////////////////////////////////////////////////////
	/// IMPLEMENTATION
	////////////////////////////////////////////////////////////////////////////
	PyCombination::PyCombination(const ConfigSection& config, const std::string& logChannel) 
		: ICombination(config, !logChannel.empty() ? logChannel : "PyCombination")
		, PyBase(config, "pesa::ICombination", static_cast<ICombination*>(this)) {
	}
	
	PyCombination::~PyCombination() {
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createPyCombination(const pesa::ConfigSection& config) {
	return new pesa::PyCombination(config, "");
}
