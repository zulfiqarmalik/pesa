/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// PyDataLoader.cpp
///
/// Created on: 13 June 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "PyImplDefs.h"
#include "PyBase.h"
#include "Data/Data.h"
#include "Components/IDataLoader.h"
#include <Poco/Path.h>

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Python IDataLoader implementation
	////////////////////////////////////////////////////////////////////////////
	class PyDataLoader : public IDataLoader, public PyBase {
	public:
								PyDataLoader(const ConfigSection& config, const char* logChannel = nullptr);
								~PyDataLoader(); 

		virtual pesa::DataPtr	create(IDataRegistry& dr, const Dataset& ds, const DataDefinition& def);

		/// initDatasets function is called for initialising the datasets that this DataLoader
		/// is supposed to provide
		PyWrapper2(initDatasets, pesa::IDataRegistry&, dr, pesa::Datasets&, datasets);
		
		PyWrapper2(preDataBuild, pesa::IDataRegistry&, dr, const pesa::DataCacheInfo&, dci);
		PyWrapper2(postDataBuild, pesa::IDataRegistry&, dr, const pesa::DataCacheInfo&, dci);

		PyWrapper2(preBuild, pesa::IDataRegistry&, dr, const pesa::DataCacheInfo&, dci);
		PyWrapper2(postBuild, pesa::IDataRegistry&, dr, const pesa::DataCacheInfo&, dci);

		PyWrapper3(build, pesa::IDataRegistry&, dr, const pesa::DataCacheInfo&, dci, pesa::DataFrame&, frame);
		
		// virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);


		virtual std::string 		id() const {
			std::string datasetName = config().get("datasetName");
			std::string repository = config().get("repository");

			if (datasetName.empty())
				datasetName = "UnknownDataset";
			if (repository.empty())
				repository = "UnknownRepository";

			return config().get("id") + "." + m_className + "." + datasetName + "." + repository;
		}
		////////////////////////////////////////////////////////////////////////////
		/// Inline functions
		////////////////////////////////////////////////////////////////////////////
	};

	PyDataLoader::PyDataLoader(const ConfigSection& config, const char* logChannel /* = nullptr */) 
		: IDataLoader(config, logChannel ? logChannel : "PY_DL")
		, PyBase(config, "pesa::IDataLoader", static_cast<IDataLoader*>(this)) {
	}
	
	PyDataLoader::~PyDataLoader() {
		CTrace("Deleting PyDataLoader: %s", id());
	}
	
	////////////////////////////////////////////////////////////////////////////
	/// IMPLEMENTATION
	////////////////////////////////////////////////////////////////////////////
	pesa::DataPtr PyDataLoader::create(IDataRegistry& dr, const Dataset& ds, const DataDefinition& def) {
		/// unsupported for the time being ...
		return nullptr;
	}

	// void PyDataLoader::postBuild(IDataRegistry& dr, const pesa::DataCacheInfo& dci) {
	// 	PyObject_CallMethodObjArgs(m_pyInstance, py_string("postBuild"), 
	// 		(PyObject*)py_cast(pesa::IDataRegistry, &dr), 
	// 		(PyObject*)py_cast(pesa::DataCacheInfo, &dci), NULL);
	// }

} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createPyDataLoader(const pesa::ConfigSection& config) {
	return new pesa::PyDataLoader((const pesa::ConfigSection&)config);
}
