/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// PyUniverse.cpp
///
/// Created on: 17 June 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "PyImplDefs.h"
#include "PyBase.h"
#include "Data/Data.h"
#include "Components/IUniverse.h"
#include <Poco/Path.h>

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Python IUniverse implementation
	////////////////////////////////////////////////////////////////////////////
	class PyUniverse : public IUniverse, public PyBase {
	public:
								PyUniverse(const ConfigSection& config, const char* logChannel = nullptr);
								~PyUniverse(); 

		virtual pesa::DataPtr	create(IDataRegistry& dr, const Dataset& ds, const DataDefinition& def);
		/// initDatasets function is called for initialising the datasets that this DataLoader
		/// is supposed to provide
		PyWrapper2(initDatasets, pesa::IDataRegistry&, dr, pesa::Datasets&, datasets);
		
		PyWrapper2(preDataBuild, pesa::IDataRegistry&, dr, const pesa::DataCacheInfo&, dci);
		PyWrapper2(postDataBuild, pesa::IDataRegistry&, dr, const pesa::DataCacheInfo&, dci);

		PyWrapper2(preBuild, pesa::IDataRegistry&, dr, const pesa::DataCacheInfo&, dci);
		PyWrapper2(postBuild, pesa::IDataRegistry&, dr, const pesa::DataCacheInfo&, dci);

		PyWrapper3(build, pesa::IDataRegistry&, dr, const pesa::DataCacheInfo&, dci, pesa::DataFrame&, frame);

		// virtual void 			initDatasets(IDataRegistry& dr, Datasets& datasets);

		virtual std::string 		id() const {
			return config().get("id") + "." + m_className;
		}

		/// TODO!
		virtual Security 		security(pesa::IntDay di, Security::Index ii) const { return Security(); }
		virtual size_t 			size(pesa::IntDay di) const { return 0; }
		virtual size_t			maxAlphaSize(IntDay di) { return 0; }

		////////////////////////////////////////////////////////////////////////////
		/// Inline functions
		////////////////////////////////////////////////////////////////////////////
	};

	////////////////////////////////////////////////////////////////////////////
	/// IMPLEMENTATION
	////////////////////////////////////////////////////////////////////////////
	PyUniverse::PyUniverse(const ConfigSection& config, const char* logChannel /* = nullptr */) 
		: IUniverse(config, logChannel ? logChannel : "PY_UNIVERSE")
		, PyBase(config, "pesa::IUniverse", static_cast<IUniverse*>(this)) {
	}
	
	PyUniverse::~PyUniverse() {
	}
	
	pesa::DataPtr PyUniverse::create(IDataRegistry& dr, const Dataset& ds, const DataDefinition& def) {
		/// unsupported for the time being ...
		return nullptr;
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IComponent* createPyUniverse(const pesa::ConfigSection& config) {
	return new pesa::PyUniverse((const pesa::ConfigSection&)config);
}
