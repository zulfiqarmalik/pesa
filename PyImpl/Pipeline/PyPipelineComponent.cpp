/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// PyPipelineComponent.cpp
///
/// Created on: 09 Dec 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "PyImplDefs.h"
#include "PyBase.h"
#include "Data/Data.h"
#include "Pipeline/IPipelineComponent.h"
#include <Poco/Path.h>

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// Python IUniverse implementation
	////////////////////////////////////////////////////////////////////////////
	class PyPipelineComponent : public IPipelineComponent, public PyBase {
	public:
								PyPipelineComponent(const ConfigSection& config, const std::string& logChannel);
								~PyPipelineComponent(); 

		PyWrapper1(prepare, pesa::RuntimeData&, rt);
		PyWrapper1(tick, const pesa::RuntimeData&, rt);
		PyWrapper1(finalise, pesa::RuntimeData&, rt);
		////////////////////////////////////////////////////////////////////////////
		/// Inline functions
		////////////////////////////////////////////////////////////////////////////
	};

	////////////////////////////////////////////////////////////////////////////
	/// IMPLEMENTATION
	////////////////////////////////////////////////////////////////////////////
	PyPipelineComponent::PyPipelineComponent(const ConfigSection& config, const std::string& logChannel) 
		: IPipelineComponent(config, !logChannel.empty() ? logChannel : "PyPipelineComponent")
		, PyBase(config, "pesa::IPipelineComponent", static_cast<IPipelineComponent*>(this)) {
	}
	
	PyPipelineComponent::~PyPipelineComponent() {
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IPipelineComponent* createPyPipelineComponent(const pesa::ConfigSection& config) {
	return new pesa::PyPipelineComponent(config, "");
}
