/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// PyBase.h
///
/// Created on: 13 Jun 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "PyBase.h"
#include "Lib/Util.h"
#include "Lib/Application.h"
#include "Poco/String.h"
#include <frameobject.h>

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// PyBase: To provide a common base imple
	////////////////////////////////////////////////////////////////////////////
	PyObject* PyBase::tryInitPyClass(const ConfigSection& config, std::string pyBasePath) {
		m_className = config.get("pyClassName");
		std::string moduleName(config.getResolvedString("pyPath", m_className + ".py"));
		pyBasePath = config.config()->defs().resolve(pyBasePath, 0U);

		if (pyBasePath.empty() && moduleName[0] == '.' && (moduleName[1] == '/' || moduleName[1] == '\\')) {
			const std::string& configFilename = config.config()->filename();
			Poco::Path fnpath(configFilename);
			std::string configDir = fnpath.makeParent().toString();
			moduleName = Poco::replace(moduleName, "./", configDir.c_str());
			moduleName = Poco::replace(moduleName, ".\\", configDir.c_str());
		}

		Poco::Path ppath(!pyBasePath.empty() ? pyBasePath : moduleName);
		ppath.makeDirectory();

		ASSERT(!m_className.empty(), "Invalid class name. Must define a 'pyClassName' property");

		Poco::Path pmoduleName(moduleName);

		if (!pyBasePath.empty() && moduleName != pyBasePath) {
			ppath.append(pmoduleName);
			//ppath.setFileName(moduleName);
			ppath.setExtension("py");
		}
		else {
			ppath.makeFile();
		}

		m_modulePath = ppath.toString();

		ASSERT(!m_modulePath.empty(), "Python modules must have a pyPath in the config section!");

		m_moduleName = ppath.getBaseName();
		m_moduleDir = ppath.makeParent().toString();

		m_pyModule = Py::getModule(m_moduleDir + m_moduleName);

		if (m_className.empty())
			m_className = m_moduleName;

        //		Trace("Py", "Got module directory: %s", m_moduleDir);
        //		Trace("Py", "Got module name: %s", m_moduleName);

		Py::initPyLib();

		if (!m_pyModule) {

			if (!m_modulePath.empty() && !m_moduleDir.empty()) {
				Trace("Py", "Adding module dir to path: %s", m_moduleDir);
				Py::addPath(m_moduleDir);
			}

			Trace("Py", "Importing: %s", m_moduleName);
			m_pyModule = PyImport_Import(py_string(m_moduleName.c_str()));
			if (!m_pyModule)
				return nullptr;
			
			Trace("Py", "Import successful: %s", m_moduleName);
			//ASSERT(m_pyModule, "Unable to import module: " << m_moduleName);
		}

		Trace("Py", "Loaded module from path: %s [Dir = %s]", pyBasePath, m_moduleDir);

		PyObject* moduleDict = PyModule_GetDict(m_pyModule);
		ASSERT(moduleDict, "Unable to get module dictionary: " << m_moduleName);

		std::string fullClassId = m_moduleName + "." + m_className;
		Trace("Py", "Creating instance for Python dataloader: %s", fullClassId);

		PyObject* pyClass = PyDict_GetItemString(moduleDict, m_className.c_str());

		ASSERT(pyClass, "Unable to get class: " << fullClassId);
		ASSERT(PyCallable_Check(pyClass), "Python class is not callable: " << fullClassId);

		Py::addModule(m_moduleDir + m_moduleName, m_pyModule);

		return pyClass;
	}

	PyObject* PyBase::initPyClass(const ConfigSection& config) {
		PyObject* pyClass = nullptr; 

		pyClass = tryInitPyClass(config, config.getResolvedRequiredString("pyPath"));
		if (pyClass)
			return pyClass;

		std::string moduleId = config.getRequired("moduleId");
		ConfigSection* moduleConfig = config.config()->modules().find(moduleId);
		ConfigSectionPVec pathSecs = moduleConfig->getAllChildrenOfType("Path");

		for (ConfigSection* pathSec : pathSecs) {
			std::string value = pathSec->getString("value", "");
			pyClass = tryInitPyClass(config, value);
			if (pyClass)
				return pyClass;
		}

		ASSERT(false, "Unable to load: " << config.getString("pyClassName", "<UNKNOWN>"));

		return nullptr;
	}

	PyBase::PyBase(const ConfigSection& config)
		: m_pyModule(nullptr)
		, m_pyInstance(nullptr) {
			
		PyObject* pyClass = initPyClass(config);
		
		swig::PyObjectVar args = Py_BuildValue("(O)", (PyObject*)py_cast(pesa::ConfigSection, &config));
		m_pyInstance = PyObject_CallObject(pyClass, (PyObject*)args);

		ASSERT(m_pyInstance, "Unable to create instance of Python class id: " << (m_moduleName + "." + m_className));
	}

	PyBase::PyBase(const ConfigSection& config, const char* type, IComponent* arg)
		: m_pyModule(nullptr)
		, m_pyInstance(nullptr) {
		PyObject* pyClass = initPyClass(config);

		auto pyArg = swig::PyObjectPtr(SWIG_NewPointerObj(SWIG_as_voidptr(arg), SWIG_TypeQuery((Py::cleanString(type) + " *").c_str()), 0));
		swig::PyObjectVar args = Py_BuildValue("(O)", (PyObject*)pyArg);
		m_pyInstance = PyObject_CallObject(pyClass, (PyObject*)args);

		if (!m_pyInstance) {
			auto err = PyErr_Occurred();
			const char* full_backtrace = NULL;

			if (err != NULL) {
				PyObject *ptype, *pvalue, *ptraceback;
				PyObject *pystr, *module_name, *pyth_module, *pyth_func;
				char *str;

				PyErr_Fetch(&ptype, &pvalue, &ptraceback);
				pystr = PyObject_Str(pvalue);
				str = PyString_AsString(pystr);
				auto error_description = strdup(str);

				/* See if we can get a full traceback */
				module_name = PyString_FromString("traceback");
				pyth_module = PyImport_Import(module_name);
				Py_DECREF(module_name);

				if (pyth_module == NULL) {
					full_backtrace = NULL;
					return;
				}

				pyth_func = PyObject_GetAttrString(pyth_module, "format_exception");
				if (pyth_func && PyCallable_Check(pyth_func)) {
					PyObject *pyth_val;

					pyth_val = PyObject_CallFunctionObjArgs(pyth_func, ptype, pvalue, ptraceback, NULL);

					pystr = PyObject_Str(pyth_val);
					str = PyString_AsString(pystr);
					full_backtrace = strdup(str);
					Py_DECREF(pyth_val);
				}
			}			//PyThreadState *tstate = PyThreadState_GET();
			//if (NULL != tstate && NULL != tstate->frame) {
			//	PyFrameObject *frame = tstate->frame;

			//	printf("Python stack trace:\n");
			//	while (NULL != frame) {
			//		// int line = frame->f_lineno;
			//		/*
			//		frame->f_lineno will not always return the correct line number
			//		you need to call PyCode_Addr2Line().
			//		*/
			//		int line = PyCode_Addr2Line(frame->f_code, frame->f_lasti);
			//		const char *filename = PyString_AsString(frame->f_code->co_filename);
			//		const char *funcname = PyString_AsString(frame->f_code->co_name);
			//		printf("    %s(%d): %s\n", filename, line, funcname);
			//		frame = frame->f_back;
			//	}
			//}
		}

		ASSERT(m_pyInstance, "Unable to create instance of Python class id: " << (m_moduleName + "." + m_className));
	}

	PyBase::PyBase(const ConfigSection& config, const char* type, IPipelineComponent* arg)
		: m_pyModule(nullptr)
		, m_pyInstance(nullptr) {
		PyObject* pyClass = initPyClass(config);

		auto pyArg = swig::PyObjectPtr(SWIG_NewPointerObj(SWIG_as_voidptr(arg), SWIG_TypeQuery((Py::cleanString(type) + " *").c_str()), 0));
		swig::PyObjectVar args = Py_BuildValue("(O)", (PyObject*)pyArg);
		m_pyInstance = PyObject_CallObject(pyClass, (PyObject*)args);

		ASSERT(m_pyInstance, "Unable to create instance of Python class id: " << (m_moduleName + "." + m_className));
	}

	PyBase::~PyBase() {
		Py::freePyLib();
	}
}
