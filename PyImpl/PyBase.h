/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// PyBase.h
///
/// Created on: 13 Jun 2016
///   Author: zulfiqar
//////////////////////////////////////////////////////

#ifndef PY_IMPL_PYBASE_H_
#define PY_IMPL_PYBASE_H_

#include "PyImplDefs.h"
#include "Components/IDataLoader.h"

namespace pesa {
	////////////////////////////////////////////////////////////////////////////
	/// PyBase: To provide a common base imple
	////////////////////////////////////////////////////////////////////////////
	class PyBase {
	protected:
		std::string 			m_modulePath;			/// The path to the python module
		std::string 			m_moduleDir;			/// The module directory. This is added to python path
		std::string 			m_moduleName; 			/// This is calculated from module path
		std::string 			m_className; 			/// This is specified in the classname
		
		PyObject*				m_pyModule;				/// The instance of the module where the pyInstance is created from
		PyObject*				m_pyInstance;			/// Instance of the python object

		PyObject*				tryInitPyClass(const ConfigSection& config, std::string pyBasePath);
		PyObject*				initPyClass(const ConfigSection& config);

	public:
								PyBase(const ConfigSection& config);
								PyBase(const ConfigSection& config, const char* type, IComponent* arg);
								PyBase(const ConfigSection& config, const char* type, IPipelineComponent* arg);
        virtual                 ~PyBase();
    };
}

#define PyWrapper0(N)			void N() { \
									PyObject_CallMethodObjArgs(m_pyInstance, py_string(#N), NULL); \
								}
								
#define PyWrapper1(N, T1, A1)	void N(T1 A1) { \
									PyObject_CallMethodObjArgs(m_pyInstance, py_string(#N), \
										(PyObject*)py_cast(T1, &A1), NULL); \
								}

#define PyWrapper2(N, T1, A1, T2, A2) \
								void N(T1 A1, T2 A2) { \
									PyObject_CallMethodObjArgs(m_pyInstance, py_string(#N), \
										(PyObject*)py_cast(T1, &A1), \
										(PyObject*)py_cast(T2, &A2), NULL); \
								}

#define PyWrapper3(N, T1, A1, T2, A2, T3, A3) \
								void N(T1 A1, T2 A2, T3 A3) { \
									PyObject_CallMethodObjArgs(m_pyInstance, py_string(#N), \
										(PyObject*)py_cast(T1, &A1), \
										(PyObject*)py_cast(T2, &A2), \
										(PyObject*)py_cast(T3, &A3), NULL); \
								}

#define PyWrapper4(N, T1, A1, T2, A2, T3, A3, T4, A4) \
								void N(T1 A1, T2 A2, T3 A3, T4 A4) { \
									PyObject_CallMethodObjArgs(m_pyInstance, py_string(#N), \
										(PyObject*)py_cast(T1, &A1), \
										(PyObject*)py_cast(T2, &A2), \
										(PyObject*)py_cast(T3, &A3), \
										(PyObject*)py_cast(T4, &A4), NULL); \
								}

#endif /// PY_IMPL_PYBASE_H_ 
