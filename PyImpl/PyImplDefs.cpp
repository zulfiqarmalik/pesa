/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// PyImplDefs.h
///
/// Created on: 13 Jun 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "PyImplDefs.h"
#include "Core/Lib/Util.h"
#include "Core/Lib/Application.h"
#include "Poco/String.h"

namespace pesa {
	int Py::s_pyRefCount = 0;
	PyObject* Py::s_main = nullptr;
	PyObject* Py::s_dict = nullptr;
	Py::StrPyObjectMap Py::s_loadedModules;

	int Py::initPyLib() {
		if (s_pyRefCount)
			return ++s_pyRefCount;

		Info("Py", "Initialising Python interpreter!");
		
		s_pyRefCount++;
		Py_Initialize();
		
		s_main 			= PyImport_AddModule("__main__");
		s_dict 			= PyModule_GetDict(s_main);
		PyObject* sys 	= PyImport_ImportModule("sys");
		PyObject* path 	= PyObject_GetAttrString(sys, "path");
		
		PyList_Append(path, py_string("."));
		PyList_Append(path, py_string(Application::instance()->cmdDir().toString().c_str()));

        std::string pythonScriptsPath = Application::instance()->cmdDir().append(Poco::Path("Python")).toString();
        Trace("Py", "Adding base path: %s", pythonScriptsPath);
		PyList_Append(path, py_string(pythonScriptsPath.c_str()));

		return s_pyRefCount;
	}
	
	int Py::freePyLib() {
		if (--s_pyRefCount == 0) {
			s_loadedModules.clear();

			Info("Py", "Finalising Python interpreter!");
			Py_Finalize();
		} 
			
		return s_pyRefCount;
	}
	
	void Py::addPath(const std::string& path) {
		PyObject* sys = PyImport_ImportModule("sys");
		PyObject* pyPath = PyObject_GetAttrString(sys, "path");
		PyList_Append(pyPath, py_string(path.c_str()));
		Trace("Py", "CWD: %s", Poco::Path::current());
	}

	std::string Py::cleanString(const char* str_) {
		std::string str = str_;

		str = Poco::replace(str, "const", "");
		str = Poco::replace(str, "static", "");
		str = Poco::replace(str, "&", "");
		str = Poco::replace(str, "*", "");

		return Poco::trim(str);
	}

	void Py::addModule(const std::string& key, PyObject* module) {
		s_loadedModules[key] = module;
	}

	PyObject* Py::getModule(const std::string& key) {
		auto iter = s_loadedModules.find(key);
		if (iter == s_loadedModules.end())
			return nullptr;
		return iter->second;
	}
}

namespace swig {
	PyObjectPtr::PyObjectPtr() : m_pyObject(0) {
	}

	PyObjectPtr::PyObjectPtr(const PyObjectPtr& item) : m_pyObject(item.m_pyObject) {
		SWIG_PYTHON_THREAD_BEGIN_BLOCK;
		Py_XINCREF(m_pyObject);
		SWIG_PYTHON_THREAD_END_BLOCK;
	}

	PyObjectPtr::PyObjectPtr(PyObject * obj, bool initialRef /* = true */) : m_pyObject(obj) {
		if (initialRef) {
			SWIG_PYTHON_THREAD_BEGIN_BLOCK;
			Py_XINCREF(m_pyObject);
			SWIG_PYTHON_THREAD_END_BLOCK;
		}
	}

	PyObjectPtr::~PyObjectPtr() {
		SWIG_PYTHON_THREAD_BEGIN_BLOCK;
		Py_XDECREF(m_pyObject);
		SWIG_PYTHON_THREAD_END_BLOCK;
	}

	PyObjectPtr& PyObjectPtr::operator=(const PyObjectPtr& item) {
		SWIG_PYTHON_THREAD_BEGIN_BLOCK;
		Py_XINCREF(item.m_pyObject);
		Py_XDECREF(m_pyObject);
		m_pyObject = item.m_pyObject;
		SWIG_PYTHON_THREAD_END_BLOCK;
		return *this;
	}

	PyObjectPtr::operator PyObject*() const { 
		return m_pyObject; 
	}

	PyObject* PyObjectPtr::operator->() const { 
		return m_pyObject; 
	}

	PyObjectVar::PyObjectVar(PyObject* obj /* = 0 */) : PyObjectPtr(obj, false) {}

	PyObjectVar& PyObjectVar::operator=(PyObject* obj) {
		Py_XDECREF(m_pyObject);
		m_pyObject = obj;
		return *this;
	}
}
