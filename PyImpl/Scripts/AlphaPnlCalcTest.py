### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import pandas
import datetime as DT

def getDateRange(startDate, endDate = None, periods = 100):
	if endDate is None:
		dates = pandas.date_range(start = (startDate - DT.timedelta(days = periods)), periods = periods).tolist()
	else:
		dates = pandas.date_range(start = startDate, end = endDate).tolist()

	retDates = []
	for date in dates:
		retDates.append(str(date.year).zfill(4) + "-" + str(date.month).zfill(2) + "-" + str(date.day).zfill(2))
	return retDates

def fixDate(date):
	return DT.datetime.strptime(date, "%Y-%m-%d")
	
def getData(stocks, startDate, endDate = None, periods = 100):
	from bamdata.api import API
	api = API("P:\\Work\\Pesa\\test\\data\\factset.yml")

	cusips = []
	cusipLookup = {}
	for i, stock in enumerate(stocks):
		print(stock)
		cusips.append(stock["cusip"])
		cusipLookup[stock["cusip"]] = i

	dates = tuple(getDateRange(startDate = startDate, endDate = endDate, periods = periods))
	result = api.get('FactsetDailyPricesAndVolume', Dates = dates, Cusips = tuple(cusips), Region = 'US')
	print(result)

	values = result.get_values()

	colMapping = {}
	colMappingList = result.columns

	for i in range(len(result.columns)):
		colMapping[result.columns[i]] = i

	data = {
		"startDate": dates[0],
		"endDate": dates[-1],

		"stocks": []
	}

	for stock in stocks:
		data["stocks"].append({
			"cusip": stock["cusip"],
			"name": stock["name"],
			"values": []
		})

	for value in values:
		valueCusip = value[colMapping["cusip"]]
		dataValue = {}

		for i in range(len(result.columns)):
			dataValue[result.columns[i]] = value[i]

		dataValue["date"] = fixDate(dataValue["date"])
		index = -1

		if dataValue["cusip"] in cusipLookup:
			index = cusipLookup[dataValue["cusip"]]

		if index >= 0:
			data["stocks"][index]["values"].append(dataValue)
			if len(data["stocks"][index]["values"]) > 1:
				dataValue["returns"] = (data["stocks"][index]["values"][-1]["priceClose"] / data["stocks"][index]["values"][-2]["priceClose"]) - 1.0
			else:
				dataValue["returns"] = 0.0

	for stock in data["stocks"]:
		stock["values"].sort(key = lambda r: r["date"])

	return data

class Pipeline:
	def __init__(self, data, days):
		self.data 		= data
		self.days 		= days
		self.booksize 	= 2e6
		self.alpha 		= [None] * days # actual calculate alpha
		self.nalpha 	= [None] * days # normalised with average
		self.palpha 	= [None] * days # percentage allocation (normalised with long/short sum)
		self.balpha 	= [None] * days # booksize allocated to each of the alphas
		self.salpha 	= [None] * days # booksize allocated to each of the alphas
		self.pnlAlpha 	= [None] * days # pnl of each of the alphas
		self.diPnl 		= [None] * days # pnl of each of the alphas

		self.longSum 	= [None] * days
		self.shortSum 	= [None] * days

		self.longCount 	= [None] * days
		self.shortCount = [None] * days

	def average(self, di):
		s = 0;
		for a in self.alpha[di]:
			s += a
		return s / float(len(self.alpha[di]))

	def normalise(self, di):
		# calculate the average of the alpha values for that day ...
		avg = self.average(di)

		# normalise each of the alpha values using the average
		nalpha = [None] * len(self.alpha[di])
		for ii, alpha in enumerate(self.alpha[di]):
			if avg != 0.0:
				nalpha[ii] = alpha - avg
			else:
				nalpha[ii] = 0.0
				
		self.nalpha[di] = nalpha

	def calcLongShortSum(self, di):
		longSum = 0.0
		shortSum = 0.0
		longCount = 0
		shortCount = 0

		for ii, alpha in enumerate(self.nalpha[di]):
			if alpha > 0.0:
				longSum += alpha
				longCount += 1
			elif alpha < 0.0:
				shortSum += alpha
				shortCount += 1

		self.longSum[di] = longSum
		self.shortSum[di] = shortSum
		self.longCount[di] = longCount
		self.shortCount[di] = shortCount

	def calcPercentAllocation(self, di):
		palpha = [0.0] * len(self.alpha[di])

		for ii, alpha in enumerate(self.nalpha[di]):
			if alpha > 0.0:
				palpha[ii] = alpha / self.longSum[di]
			elif alpha < 0.0:
				palpha[ii] = -(alpha / self.shortSum[di]) # deliberate negative sign to indicate short ...

		self.palpha[di] = palpha

	def calcCapitalAllocation(self, di):
		balpha = [0.0] * len(self.alpha[di])
		halfBooksize = self.booksize * 0.5

		for ii, alpha in enumerate(self.palpha[di]):
			balpha[ii] = alpha * halfBooksize

		self.balpha[di] = balpha

	def calcPnl(self, di):
		pnlAlpha = [0.0] * len(self.alpha[di])
		diPnl = 0.0

		for ii, balpha in enumerate(self.balpha[di]):
			pnlAlpha[ii] = self.data["stocks"][ii]["values"][di]["returns"] * balpha
			diPnl += pnlAlpha[ii]

		self.pnlAlpha[di] = pnlAlpha
		self.diPnl[di] = diPnl

	def calcStocks(self, di):
		salpha = [0.0] * len(self.alpha[di])

		for ii, alpha in enumerate(self.balpha[di]):
			salpha[ii] = alpha / self.data["stocks"][ii]["values"][di]["priceClose"]

		self.salpha[di] = salpha


def calcAlpha(data, di):
	alpha = [None] * len(data["stocks"])
	di -= 1

	if di >= 0:
		for i, stock in enumerate(data["stocks"]):
			alpha[i] = 1.0 / stock["values"][di]["priceClose"]
	else:
		for i, stock in enumerate(data["stocks"]):
			alpha[i] = 0.0

	return alpha

if __name__ == "__main__":
	# data = getData(stocks = [{
	# 		"cusip": "5124123", 
	# 		"name": "Nonsense"
	# 	}], 
	# 	startDate = DT.datetime(year = 2015, month = 1, day = 1), 
	# 	endDate = DT.datetime.today())

	data = getData(stocks = ({
			"cusip": "594918104", 
			"name": "MSFT"
		}, {
			"cusip": "02079K107",
			"name": "GOOG"
		}, {
			"cusip": "984332106",
			"name": "YHOO"
		}, {
			"cusip": "30303M102",
			"name": "FB"
		}), 
		startDate = DT.datetime(year = 2016, month = 1, day = 1), 
		endDate = DT.datetime(year = 2016, month = 1, day = 5))

	# for stock in data["stocks"]:
	# 	print("%s: %d" % (stock["name"], len(stock["values"])))
		# for value in stock["values"]:

	days = len(data["stocks"][0]["values"])
	print("Total count: %d" % (days))

	pipeline = Pipeline(data, days)

	# 1. Calculate alpha
	# 2. Normalise alpha using the average 
	# 3. Calculate the sum of +ve (long) and -ve (short) alpha values
	# 4. Renormalise long alphas with longSum and short alphas with shortSum. 
	# 	 This will give us how much percentage to allocate in an alpha
	# 5. Transform percentage calculate in Step 4. to to the booksize.
	# 	 This will give us how much capital to allocate to each of the alphas.
	# 6. Calculate alpha pnl
	# 7. Calculate number of stocks for each alpha

	for di in range(days):
		# calculate alpha ...
		diAlpha = calcAlpha(data, di)
		pipeline.alpha[di] = diAlpha

		pipeline.normalise(di)
		pipeline.calcLongShortSum(di)
		pipeline.calcPercentAllocation(di)
		pipeline.calcCapitalAllocation(di)
		pipeline.calcPnl(di)
		pipeline.calcStocks(di)

		# print("%d: %d, %d [%0.2f (%0.2f), %0.2f (%0.2f), %0.2f (%0.2f), %0.2f (%0.2f)] = %0.2f" % (di, pipeline.longCount[di], pipeline.shortCount[di], 
		# 	pipeline.pnlAlpha[di][0], pipeline.salpha[di][0], 
		# 	pipeline.pnlAlpha[di][1], pipeline.salpha[di][1], 
		# 	pipeline.pnlAlpha[di][2], pipeline.salpha[di][2], 
		# 	pipeline.pnlAlpha[di][3], pipeline.salpha[di][3], 
		# 	pipeline.diPnl[di]))


	# print(data["startDate"])
	# print(data["endDate"])
	# print(len(data["values"]))
