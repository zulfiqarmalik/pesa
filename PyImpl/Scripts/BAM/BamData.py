### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import sys
import traceback

try:
    import Framework
    import PyUtil

    class BamData(Framework.IDataLoader):
        def __init__(self, config, logChannel = None):
            try:
                self.config = config 
                self.logChannel = logChannel

                if self.logChannel is None:
                    self.logChannel = "BAM_DATA"

                BamUtil.debug("BAM_DATA", "BamData::__init__")

            except Exception as e:
                print(traceback.format_exc())

        def loadConfig(self, logChannel = None):
            self.logChannel = logChannel

            if self.logChannel is None:
                self.logChannel = "BAM_DATA"

            self.datasetName = self.config.get("datasetName")
            assert self.datasetName, "Must have a datasetName!"

            self.ymlPath = self.config.getResolved("ymlPath")
            assert self.ymlPath, "Must have a ymlPath to give path to the YAML file!"

            self.repository = self.config.getKeyword()
            assert self.repository, "XML section must have a repository to tell which repository to fetch!"

            self.datasets = self.config.get("datasets")
            assert self.datasets, "Must have a valid list of (comma separated) datasets which to load from the repository!"

            self.datasets = BamUtil.parseDatasets(self.datasets)
            BamUtil.debug("BAM_DATA", "Got datasets: %s" % (str(self.datasets)))

            self.dataOps = self.config.get("dataOps")

            if self.dataOps:
                self.dataOps = BamUtil.parseDataOps(self.dataOps)

            datasetMappings = self.config.get("mappings")
            self.forwardMappings = {}
            self.reverseMappings = {}

            self.singleDate = self.config.getBool("singleDate", False)
            self.intDate = self.config.getBool("intDate", False)

            if datasetMappings:
                self.forwardMappings, self.reverseMappings = BamUtil.parseDatasetMappings(datasetMappings)

            functions = self.config.get("functions")
            self.functions = {}

            if functions:
                self.functions = BamUtil.parseFunctions(functions)

        def parseResults(self, result):
            colMapping = {}
            colMappingList = result.columns

            for i in range(len(result.columns)):
                colMapping[result.columns[i]] = i

            values = result.get_values()
            data = []

            for value in values:
                dataValue = {}

                for i in range(len(result.columns)):
                    if self.dataOps and result.columns[i] in self.dataOps:
                        import parse
                        parsedResult = parse.parse(self.dataOps[result.columns[i]], value[i])

                        for key in parsedResult.named:
                            dataValue[key] = parsedResult[key]
                    else:
                        dataValue[result.columns[i]] = value[i]

                data.append(dataValue)

            return data

        def initDatasets(self, dr, datasets):
            try:
                self.loadConfig()

                BamUtil.debug("BAM_DATA", "BamData::initDatasets")

                ds = Framework.Dataset(self.datasetName)

                for dataset in self.datasets:
                    name = dataset["name"]

                    if name in self.forwardMappings:
                        name = self.forwardMappings[name]

                    dv = Framework.DataValue(
                        name, 
                        dataset["type"], 
                        dataset["typeSize"], 
                        dataset["frequency"], 0)

                    ds.add(dv)

                datasets.add(ds)

            except Exception as e:
                print(traceback.format_exc())

        def getData(self, stocks, dates):
            from bamdata.api import API

            cusips = []
            cusipLookup = {}

            for i, stock in enumerate(stocks):
                cusips.append(stock["cusip"])
                cusipLookup[stock["cusip"]] = i

            api = API(self.ymlPath)

            if self.singleDate is False:
                for i in range(len(dates)):
                    dates[i] = BamUtil.intDateToStrDate(dates[i])

                result = api.get(self.repository, Dates = tuple(dates), Cusips = tuple(cusips), Region = self.config.config().defs().market())
            else:
                assert len(dates) == 1, "Function does not accept multiple dates. Num dates provided: %d" % (len(dates))
                dateId = dates[0]
                result = api.get(self.repository, DateId = dateId, Cusips = tuple(cusips))

            # print(result)
            values = result.get_values()

            colMapping = {}
            colMappingList = result.columns

            for i in range(len(result.columns)):
                colMapping[result.columns[i]] = i

            data = {
                "startDate": dates[0],
                "endDate": dates[-1],

                "stocks": []
            }

            for stock in stocks:
                data["stocks"].append({
                    "cusip": stock["cusip"],
                    "name": stock["name"],
                    "values": []
                })

            for value in values:
                valueCusip = value[colMapping["cusip"]]
                dataValue = {}

                for i in range(len(result.columns)):
                    valueIndex = result.columns[i]

                    if valueIndex in self.forwardMappings:
                        valueIndex = self.forwardMappings[valueIndex]

                    dataValue[valueIndex] = value[i]

                for key, value in dataValue.items():
                    if key in self.functions:
                        dataValue[key] = eval(self.functions[key])

                if "dateid" in dataValue:
                    dataValue["date"] = dataValue["dateid"]
                elif "date" in dataValue:
                    dataValue["date"] = BamUtil.strDateToIntDate(dataValue["date"])
                else:
                    assert False, "No date value in dictionary: %s" % (str(dataValue))

                index = -1

                if dataValue["cusip"] in cusipLookup:
                    index = cusipLookup[dataValue["cusip"]]

                if index >= 0:
                    data["stocks"][index]["values"].append(dataValue)

                    # if len(data["stocks"][index]["values"]) > 1:
                    #   dataValue["returns"] = (data["stocks"][index]["values"][-1]["priceClose"] / data["stocks"][index]["values"][-2]["priceClose"]) - 1.0
                    # else:
                    #   dataValue["returns"] = 0.0

            # for stock in data["stocks"]:
            #   stock["values"].sort(key = lambda r: r["date"])

            return data

        def build(self, dr, dci, frame):
            try:
                import pprint

                # BamUtil.debug("BAM_DATA", "BamData::build")

                if self.diStart is None:
                    self.diStart = dci.di

                date = dci.rt.getDate(dci.di)
                numSecurities = dci.universe.size(dci.di)
                stocks = []

                for ii in range(numSecurities):
                    security = dci.universe.security(dci.di, ii)

                    if security.index != Framework.Security.s_invalidIndex:
                        assert security.cusip, "Invalid security: %d (@ii = %d)" % (security.index, ii)

                        stocks.append({
                            "cusip": security.cusip,
                            "name": security.ticker
                        })

                BamUtil.debug("BAM_DATA", "BamData::build - [%d - %s] - Number of stocks: %d" % (date, dci._def.name, len(stocks)))

                cacheIndex = dci.di - self.diStart
                data = None

                # see if its already in the cache ...
                if cacheIndex < len(self.dataCache):
                    data = self.dataCache[cacheIndex]
                    BamUtil.debug("BAM_DATA", "Found data in the cache!")
                else:
                    data = self.getData(stocks, [date])
                    self.dataCache.append(data)

                dataPtr = Framework.FloatMatrixData(dci._def, 1, len(stocks), False)

                pp = pprint.PrettyPrinter(indent = 4)

                for ii in range(len(data["stocks"])):
                    values = data["stocks"][ii]["values"]

                    if len(values) > 0:
                        assert len(values) == 1, "How come the result has more than one value? (%s)" % (str(values))
                        dataPtr.setValue(0, ii, values[0][dci._def.name])
                    else:
                        dataPtr.makeInvalid(0, ii)

                frame.setData(BamUtil.script_sptr(dataPtr))
                frame.dataOffset = 0
                frame.dataSize = dataPtr.dataSize()

            except Exception as e:
                print(traceback.format_exc())

        def preDataBuild(self, dr, dci):
            try:
                # from bamdata.api import API

                BamUtil.debug("BAM_DATA", "BamData::preDataBuild")
                self.dataCache = []
                self.diStart = None
                
                # self.dataCache = []
                # dates = BamUtil.getStrDates(dci.rt.dates)

                # securityMaster = dr.getSecurityMaster()
                # assert securityMaster, "Cannot build universe without a security master!"

                # securities = securityMaster.securities()
                # stocks = []

                # for ii in range(10):
                #   security = securities[ii]
                #   assert ii == security.index, "Securities not laid out sequentially. Expecting: %d - Found: %d" % (ii, security.idnex)

                #   stocks.append({
                #       "cusip": security.cusip,
                #       "name": security.ticker
                #   })

                # BamUtil.debug("BAM_DATA", "Number of dates: %d" % (len(dates)))
                # BamUtil.debug("BAM_DATA", "Number of stocks: %d" % (len(stocks)))

                # data = self.getData(stocks = stocks, dates = dates)
                # print(data);

            except Exception as e:
                print(traceback.format_exc())

        def postDataBuild(self, dr, dci):
            try:
                BamUtil.debug("BAM_DATA", "BamData::postDataBuild - Removing the cache!")
                self.dataCache = None
            except Exception as e:
                print(traceback.format_exc())

        def preBuild(self, dr, dci):
            try:
                # BamUtil.debug("BAM_DATA", "BamData::preBuild")
                pass
            except Exception as e:
                print(traceback.format_exc())

        def postBuild(self, dr, dci):
            try:
                # BamUtil.debug("BAM_DATA", "BamData::postBuild")
                pass
            except Exception as e:
                print(traceback.format_exc())

except Exception as e:
    print(traceback.format_exc())
