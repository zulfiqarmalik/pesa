### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import traceback
import datetime as DT

import PyUtil
import sys

try:
    import Framework
    from BamRestData import BamRestData

    class BamRestCalendar(BamRestData):
        def __init__(self, baseObject):
            try:
                super().__init__(baseObject, "EXCH_HOLIDAY_CALENDAR")
                self.debug("BamRestCalendar::__init__")

                self.loadConfig()

                self.useMICCode     = self.config().getBool("useMICCode", False)
                self.perExchangeData= self.config().getBool("perExchangeData", False)
                self.holidayLookup  = {}
                exchanges           = self.config().getRequired("exchanges")
                exchanges           = self.config().config().defs().resolve(exchanges, 0) 
                self.countries, self.exchanges = BamUtil.parseExchanges(exchanges)
                assert self.exchanges and len(self.exchanges), "Must specify which exchanges to load the calendar for!"

                self.timesKeyword   = self.config().get("timesKeyword")
                self.timesExchanges = self.config().get("timesExchanges")

                self.mapExchanges   = {}
                self.mapExchangesStr= self.config().getString("mapExchanges", "")
                if self.mapExchangesStr:
                    exchanges = self.mapExchangesStr.split("|")
                    for exchange in exchanges:
                        mapFrom, mapTo = exchange.split("=")
                        self.mapExchanges[mapFrom.strip()] = mapTo.strip()

                self.exchangeHolidays = {}

                self.allHolidays    = []
                self.newHolidays    = []
                self.holidaysBuilt  = False

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def initDatasets(self, dr, datasets):
            try:
                self.debug("BamRestCalendar::initDatasets")

                ds = Framework.Dataset("calendar")

                if self.perExchangeData is True:
                    for i in range(0, len(self.exchanges)):
                        country = self.countries[i]
                        exchange = self.exchanges[i]
                        exchangeId = self.exchangeId(country, exchange)

                        ds.add(Framework.DataValue("holidays_" + exchangeId, Framework.DataType.kInt, 4, Framework.DataFrequency.kStatic, 
                            Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite))
                        # ds.add(Framework.DataValue("holidayDesc_" + exchangeId, Framework.DataType.kInt, 4, Framework.DataFrequency.kStatic, 
                        #     Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite))

                ds.add(Framework.DataValue("holidays", Framework.DataType.kInt, 4, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo))
                ds.add(Framework.DataValue("holidayDesc", Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo))

                datasets.add(ds)

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def preDataBuild(self, dr, dci):
            try:
                data = dr.intData(dci.universe, "calendar.holidays", noCacheUpdate = True)

                if data:
                    cols = data.cols()

                    for ii in range(cols):
                        idate = data.getValue(0, ii)
                        self.holidayLookup[idate] = True

                    self.debug("Number of existing holidays found: %d" % (cols))
                else:
                    self.debug("Number of existing holidays found: 0")
            except Exception as e:
                print(traceback.format_exc())
                raise e

        def exchangeId(self, country, exchange):
            return country + "_" + exchange

        def getHolidaysForExchange(self, dr, dci, country, exchange, mappedExchange):
            exchangeId = self.exchangeId(country, exchange)

            if exchangeId in self.exchangeHolidays:
                return self.exchangeHolidays[exchangeId]

            self.debug("Getting data for exchange: '%s:%s [%s]'" % (country, exchange, mappedExchange))
            args = {}

            if self.useMICCode is False:
                args["FinancialCenterCode"] = "'" + mappedExchange + "'"
            else:
                args["CountryCode"] = "'" + country + "'"
                args["MICExchangeCode"] = "'" + mappedExchange + "'"
 
            data = self.getData(dci, args)

            if data is None or "data" not in data or data["data"] is None:
                self.warning("Unable to get data for exchange: %s [%s]" % (exchange, mappedExchange))
                return None

            # assert data, "Unable to get calendar data for exchange: %s" % (exchange)
            # assert "data" in data and data["data"], "Unable to get data for exchange: %s" % (exchange)

            refDates = data["data"]["CALENDARDATE"]
            refDesc = data["data"]["HOLIDAY"]
            financialCentre = data["data"]["FINANCIALCENTERCODE"]

            uniqueDates = {}
            holidayDates = []
            holidayDesc = []

            for refIndex, refDate in enumerate(refDates):
                irefDate = BamUtil.strDateToIntDate(refDate)
                if refDate not in uniqueDates:
                    # if refDesc[refIndex] == "Independence Day" and country == "GB" and exchange == "XLON" and irefDate >= 20170701:
                    if financialCentre[refIndex] == "LnY" and irefDate >= 20170701:
                        self.debug("Ignoring LnY holiday: %s" % (str(refDate)))
                        continue

#                    self.debug("Country: %s - Exchange: %s - %s [%s]" % (country, exchange, str(refDate), refDesc))
                    holidayDates.append(refDate)
                    holidayDesc.append(refDesc[refIndex])
                    uniqueDates[refDate] = True

            assert len(holidayDates) == len(holidayDesc), "Length of holiday dates does not match holiday desc. Holiday dates: %d - Holiday desc: %d" % (len(holidayDates), len(holidayDesc))
            self.debug("Number of holidays: %d" % (len(holidayDates)))

            exchangeData = {
                'holidayDates': holidayDates,
                'holidayDesc': holidayDesc
            }

            self.exchangeHolidays[exchangeId] = exchangeData

            return exchangeData

        def updateHolidays(self, dr, dci):
            if self.holidaysBuilt:
                return

            allHolidays = {}

            for exchangeIndex, exchange in enumerate(self.exchanges):
                country = self.countries[exchangeIndex]
                mappedExchange = exchange
                if exchange in self.mapExchanges:
                    mappedExchange = self.mapExchanges[exchange]
                exchangeData = self.getHolidaysForExchange(dr, dci, country, exchange, mappedExchange)

                if exchangeData is None:
                    continue
                # assert exchangeData is not None, "Unable to get data for exchange: %s" % (self.exchangeId(country, exchange))

                holidayDates = exchangeData["holidayDates"]
                holidayDesc = exchangeData["holidayDesc"]

                for i, date in enumerate(holidayDates):
                    idate = BamUtil.strDateToIntDate(date)
                    self.trace("Holiday on date: %d" % (idate))

                    if idate not in allHolidays:
                        allHolidays[idate] = {
                            "count": 0,
                            "date": idate,
                            "desc": holidayDesc[i]
                        }
                        
                    allHolidays[idate]["count"] += 1

            # Holiday is only counted if its a holiday in ALL the exchanges!
            self.allHolidays = []
            self.newHolidays = []
            numExchanges = len(self.exchanges)

            for idate, holiday in allHolidays.items():
                if holiday["count"] >= numExchanges:
                    self.debug("Found common holiday on: %d" % (idate))
                    self.allHolidays.append(holiday)

                    if idate not in self.holidayLookup:
                        self.newHolidays.append(holiday)
                        self.debug("New holiday: %d" % (idate))

            self.holidaysBuilt = True
            self.debug("Total number of common holidays: %d (New holidays: %d)" % (len(self.allHolidays), len(self.newHolidays)))

            return self.allHolidays, self.newHolidays

        def buildHolidays(self, dr, dci, frame):
            self.updateHolidays(dr, dci)
            newHolidays = self.newHolidays
            numNewHolidays = len(newHolidays)

            if numNewHolidays > 0:
                self.debug("Number of new holidays: %d" % (numNewHolidays))
                dataPtr = Framework.IntMatrixData(dci.universe, dci._def, 1, numNewHolidays, False)

                for i in range(numNewHolidays):
                    dataPtr.setValue(0, i, newHolidays[i]["date"])

                frame.setData(BamUtil.script_sptr(dataPtr))
                frame.dataOffset = 0
                frame.dataSize = dataPtr.dataSize()
            else:
                frame.noDataUpdate = True

        def buildHolidayDesc(self, dr, dci, frame):
            self.updateHolidays(dr, dci)
            newHolidays = self.newHolidays
            numNewHolidays = len(newHolidays)

            if numNewHolidays > 0:
                dataPtr = Framework.StringData(dci.universe, numNewHolidays, dci._def)

                for i in range(numNewHolidays):
                    dataPtr.setValue(0, i, newHolidays[i]["desc"])

                frame.setData(BamUtil.script_sptr(dataPtr))
                frame.dataOffset = 0
                frame.dataSize = dataPtr.dataSize()
            else:
                frame.noDataUpdate = True

        def buildExchangeHolidays(self, dr, dci, frame):
            self.updateHolidays(dr, dci)
            dataName = dci._def.name
            exchangeId = dataName[9:]
            self.debug("Updating holiday exchangeId: %s" % (exchangeId))

            if exchangeId not in self.exchangeHolidays:
                frame.noDataUpdate = True
                return

            hdata = self.exchangeHolidays[exchangeId]
            if hdata is None:
                frame.noDataUpdate = True
                
            holidayDates = hdata["holidayDates"]
            numHolidays = len(holidayDates)

            if numHolidays > 0:
                self.debug("Number of holidays: %d [%s]" % (numHolidays, exchangeId))
                dataPtr = Framework.IntMatrixData(dci.universe, dci._def, 1, numHolidays, False)

                for i in range(numHolidays):
                    sdate = holidayDates[i]
                    idate = BamUtil.strDateToIntDate(sdate)
                    dataPtr.setValue(0, i, idate)

                frame.setData(BamUtil.script_sptr(dataPtr))
                frame.dataOffset = 0
                frame.dataSize = dataPtr.dataSize()
            else:
                frame.noDataUpdate = True

        def buildExchangeHolidayDescs(self, dr, dci, frame):
            self.updateHolidays(dr, dci)
            dataName = dci._def.name
            exchangeId = dataName[12:]
            self.debug("Updating holidayDesc exchangeId: %s" % (exchangeId))

            if exchangeId not in self.exchangeHolidays:
                frame.noDataUpdate = True
                return

            hdata = self.exchangeHolidays[exchangeId]
            if hdata is None:
                frame.noDataUpdate = True

            holidayDesc = hdata["holidayDesc"]
            numHolidays = len(holidayDesc)

            if numHolidays > 0:
                self.debug("Number of holidays: %d [%s]" % (numHolidays, exchangeId))
                dataPtr = Framework.StringData(dci.universe, dci._def, 1, numHolidays, False)

                for i in range(numHolidays):
                    dataPtr.setValue(0, i, holidayDesc[i])

                frame.setData(BamUtil.script_sptr(dataPtr))
                frame.dataOffset = 0
                frame.dataSize = dataPtr.dataSize()
            else:
                frame.noDataUpdate = True


        def postBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())
                raise e

        def cacheTradingTimes(self, dr, dci):
            for date in dci.rt.dates:
                print(date)
            assert False, dci.rt.dates

        def buildTradingTimes(self, dr, dci, frame):
            self.cacheTradingTimes(dr, dci)

        def build(self, dr, dci, frame):
            try:
                dataName = dci._def.name
                self.debug("BamRestCalendar::build: %s" % (dataName))

                if dataName == "holidays":
                    return self.buildHolidays(dr, dci, frame)
                elif dataName == "holidayDesc":
                    return self.buildHolidayDesc(dr, dci, frame)
                elif dataName.startswith("holidays_"):
                    return self.buildExchangeHolidays(dr, dci, frame)
                elif dataName.startswith("holidaysDesc_"):
                    return self.buildExchangeHolidayDescs(dr, dci, frame)

            except Exception as e:
                print(traceback.format_exc())
                raise e

except Exception as e:
    print(traceback.format_exc())
    raise e
