### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import sys
import traceback

try:
    import Framework
    import PyUtil
    from PyBase import PyDataLoader

    class BamRestData(PyDataLoader):
        def __init__(self, baseObject, logChannel):
            super().__init__(baseObject, logChannel)

        def loadConfig(self, logChannel = None):
            config = self.config()

            self.serverUrl = config.getServerUrl()
            assert self.serverUrl, "Must have a serverUrl in the config section"

            self.keyword = config.getKeyword()
            assert self.keyword, "Must have a keyword in the config section"

            self.yml = config.getYml()
            assert self.yml, "No yml specified"

            functions = config.get("functions")
            self.functions = {}

            if functions:
                self.functions = BamUtil.parseFunctions(functions)

        def initDatasets(self, dr, datasets):
            try:
                self.loadConfig()
            except Exception as e:
                print(traceback.format_exc())

        def getData(self, dci, params = {}, serverUrl = None):
            if serverUrl is None:
                serverUrl = self.serverUrl

            if "Keyword" not in params:
                params['Keyword'] = self.keyword

            if "yml" not in params:
                params['yml'] = self.yml

            params['ultrajson'] = "'True'"

            # url = serverUrl
            # appendKey = "?"
            # for key, value in params.items():
            #   url += appendKey + key + "=" + value
            #   appendKey = "&"

            dataValues = []
            for key, value in params.items():
                dataValues.append((key, value))
            data = dict(dataValues)

            import requests
            from requests_ntlm import HttpNtlmAuth

            username = dci.rt.appOptions.username
            password = dci.rt.appOptions.password

            assert username, "Must specify a username -u parameter on the command line for NT authentication"
            assert password, "Must specify a password -p parameter on the command line for NT authentication"

            # self.debug(self.logChannel, "Requesting url: %s" % (serverUrl))

            responseData = requests.post(serverUrl, data = data, auth = HttpNtlmAuth(username, password))

            # Some error was returned
            if responseData.status_code < 200 or responseData.status_code >= 300:
                text = responseData.text.replace("{", "{{")
                text = text.replace("}", "}}")
                self.error("Server error. Response code: %d - Response Text: %s" % (responseData.status_code, text))
                return None
                
            import json
            jsonStr = responseData.text
            data = json.loads(jsonStr)
            return data

        def debug(self, *args):
            fstring = str.format(*args)
            Framework.debug("PYTHON - " + self._logChannel, fstring)

        def build(self, dr, dci, frame):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())

        def preDataBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())

        def postDataBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())

        def preBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())

        def postBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())

except Exception as e:
    print(traceback.format_exc())
