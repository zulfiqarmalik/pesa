### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import sys
import traceback
import datetime as DT

try:
    import Framework
    import PyUtil
    from BamRestData import BamRestData

    class BamRestGenericData(BamRestData):
        def __init__(self, config, logChannel = None):
            try:
                super().__init__(config, "BAM_SEC_MASTER")
                self.logChannel = "BAM_REST_GENERIC"
                BamUtil.debug(self.logChannel, "BamRestGenericData::__init__")

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def loadConfig(self, logChannel = None):
            super().loadConfig(logChannel)

            self.datasetName = self.config.get("datasetName")
            assert self.datasetName, "Must have a datasetName!"

            self.datasets = self.config.get("datasets")
            assert self.datasets, "Must have a valid list of (comma separated) datasets which to load from the repository!"

            self.datasets = BamUtil.parseDatasets(self.datasets)
            BamUtil.debug(self.logChannel, "Got datasets: %s" % (str(self.datasets)))

            datasetMappings = self.config.get("mappings")
            self.forwardMappings = {}
            self.reverseMappings = {}

            if datasetMappings:
                self.forwardMappings, self.reverseMappings = BamUtil.parseDatasetMappings(datasetMappings)

            functions = self.config.get("functions")
            self.functions = {}

            if functions:
                self.functions = BamUtil.parseFunctions(functions)


        def initDatasets(self, dr, datasets):
            try:
                self.loadConfig()
                BamUtil.debug(self.logChannel, "BamRestData::initDatasets")

                ds = Framework.Dataset(self.datasetName)

                for dataset in self.datasets:
                    name = dataset["name"]

                    if name in self.forwardMappings:
                        name = self.forwardMappings[name]

                    dv = Framework.DataValue(
                        name, 
                        dataset["type"], 
                        dataset["typeSize"], 
                        dataset["frequency"], 0)

                    ds.add(dv)

                datasets.add(ds)
            except Exception as e:
                print(traceback.format_exc())

        def getStockData(self, dci, stocks, date, startIndex, endIndex):
            startDate = BamUtil.intDateToStrDate(date)
            endDate = BamUtil.intToDate(date) + DT.timedelta(days = 1)
            endDate = BamUtil.dateToStr(endDate)
            idsStr = "" #"'" + stocks[startIndex]["figi"] + "'"

            for i in range(startIndex, endIndex):
                idsStr += "'" + stocks[i]["figi"] + "',"

            return super().getData(dci, params = {
                "StartDate": "'" + startDate + "'",
                "EndDate": "'" + endDate + "'",
                "Symbols": "dict(Symbology='CFIGI',IDs=(" + idsStr + "))"
            })

        def parseData(self, data, stocks, dataName, startIndex, endIndex):
            if data is None or "data" not in data:
                parsedData = []
                for ii in range(startIndex, endIndex):
                    parsedData.append({
                        "figi": stocks[ii]["figi"],
                        "values": []
                    })
                return parsedData, 0

            if "data" not in data:
                return None, 0

            figiData = data["data"]["CFIGI"]

            assert dataName in data["data"], "Unable to find data: %s in the returned data array" % (dataName)
            pdata = data["data"][dataName]

            figiDataLen = len(figiData)
            pdataLen = len(pdata)

            assert figiDataLen == pdataLen, "Data length does not match the FIGI data length. FIGI length: %d - Data length: %d" % (figiDataLen, pdataLen)
            parsedData = []

            index = 0
            numValidStocks = 0

            for ii in range(startIndex, endIndex):
                stock = stocks[ii]
                stockUuid = stock["figi"]
                dataObj = {
                    "figi": stockUuid,
                    "values": []
                }

                parsedData.append(dataObj)

                if index >= figiDataLen:
                    continue

                startIndex = index
                while index < figiDataLen and stockUuid == figiData[index]:
                    dataObj["values"].append(float(pdata[index]))
                    index += 1

                if startIndex != index:
                    numValidStocks += 1

            return parsedData, numValidStocks

        def build(self, dr, dci, frame):
            try:
                if self.diStart is None:
                    self.diStart = dci.di

                date = dci.rt.getDate(dci.di)
                numSecurities = dci.universe.size(dci.di)
                stocks = []

                for ii in range(numSecurities):
                    security = dci.universe.security(dci.di, ii)

                    if security.index != Framework.Security.s_invalidIndex:
                        assert security.uuid, "Invalid security: %d (@ii = %d)" % (security.index, ii)

                        stocks.append({
                            "figi": security.uuid,
                            "name": security.ticker
                        })

                dataName = dci._def.name

                if dataName in self.reverseMappings:
                    dataName = self.reverseMappings[dataName]

                BamUtil.debug(self.logChannel, "BamRestData::build - [%d - %s] - Number of stocks: %d" % (date, dataName, len(stocks)))

                cacheIndex = dci.di - self.diStart

                dataPtr = Framework.FloatMatrixData(dci._def, 1, len(stocks), False)
                numStocks = len(stocks)
                batchSize = numStocks #100
                numBatches = 1 #int(numStocks / batchSize) + 1

                for jj in range(0, numBatches):
                    startIndex = jj * batchSize

                    if startIndex >= numStocks:
                        break

                    endIndex = startIndex + batchSize

                    if endIndex > numStocks:
                        endIndex = numStocks

                    # see if its already in the cache ...
                    if cacheIndex < len(self.dataCache):
                        if jj in self.dataCache[cacheIndex]:
                            data = self.dataCache[cacheIndex][jj]
                            # BamUtil.debug(self.logChannel, "Found data in the cache!")
                        else:
                            data = self.getStockData(dci, stocks, date, startIndex, endIndex)
                            self.dataCache[cacheIndex][jj] = data
                    else:
                        data = self.getStockData(dci, stocks, date, startIndex, endIndex)
                        dataIndex = {}
                        dataIndex[jj] = data
                        self.dataCache.append(dataIndex)

                    parsedData, numValidStocks = self.parseData(data, stocks, dataName, startIndex, endIndex)
                    BamUtil.debug(self.logChannel, "Num valid stocks: %d" % (numValidStocks))
                    assert len(parsedData) == endIndex - startIndex, "Invalid size returned by parseData: %d - Expecting: %d" % (len(parsedData), endIndex - startIndex)

                    for kk, pdata in enumerate(parsedData):
                        ii = startIndex + kk
                        stock = stocks[ii]

                        assert "values" in pdata, "Invalid parsed data returned: %d - %s - %s" % (ii, stock["ticker"], stock["figi"])
                        assert pdata["figi"] == stock["figi"], "FIGI mismatch between source and parsed data. Expecting: %s - Found: %s" % (stock["figi"], pdata["figi"])

                        values = pdata["values"]

                        if len(values) > 0:
                            # TODO: Very important to enable this!
                            # assert len(values) == 1, "How come the result has more than one value? (%s) - FIGI: %s - Date: %d" % (str(values), stock["figi"], date)
                            if dci._def.name in self.functions:
                                dataValue = {}
                                dataValue[dci._def.name] = values[0]
                                values[0] = eval(self.functions[dci._def.name])

                            dataPtr.setValue(0, ii, values[0])
                        else:
                            dataPtr.makeInvalid(0, ii)

                    data = None

                frame.setData(BamUtil.script_sptr(dataPtr))
                frame.dataOffset = 0
                frame.dataSize = dataPtr.dataSize()

            except Exception as e:
                print(traceback.format_exc())

        def preDataBuild(self, dr, dci):
            try:
                self.dataCache = []
                self.diStart = None
            except Exception as e:
                print(traceback.format_exc())

        def postDataBuild(self, dr, dci):
            try:
                self.dataCache = None
            except Exception as e:
                print(traceback.format_exc())

        def preBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())

        def postBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())

except Exception as e:
    print(traceback.format_exc())
