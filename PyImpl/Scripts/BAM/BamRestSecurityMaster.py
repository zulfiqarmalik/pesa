### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import traceback
import datetime as DT

import PyUtil
import sys

try:
    import Framework
    from BamRestData import BamRestData

    class BamRestSecurityMaster(BamRestData):
        def __init__(self, config, logChannel = None):
            try:
                super().__init__(config, "BR_SEC_MSTR")
                self.logChannel = "BR_SEC_MSTR"
                BamUtil.debug(self.logChannel, "BamRestSecurityMaster::__init__")

                self.loadConfig(self.logChannel)

                self.existingSecurities = []
                self.securityLookup = {}
                self.numSecuritiesParsed = 0

                self.exchanges = config.get("exchanges")
                if self.exchanges:
                    exchanges = self.exchanges.split(" ")
                    self.exchanges = {}

                    for exchange in exchanges:
                        self.exchanges[exchange] = True
                else:
                    self.exchanges = {}

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def initDatasets(self, dr, datasets):
            try:
                BamUtil.debug(self.logChannel, "BamRestSecurityMaster::initDatasets")

                ds = Framework.Dataset("SecurityMaster")

                ds.add(Framework.DataValue(
                    "Universe", Framework.DataType.kSecurityData, 
                    Framework.Security.s_version, Framework.DataFrequency.kStatic, 
                    Framework.DataFlags.kAlwaysLoad | Framework.DataFlags.kUpdatesInOneGo))

                datasets.add(ds)

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def preDataBuild(self, dr, dci):
            try:
                BamUtil.debug(self.logChannel, "BamRestSecurityMaster::preDataBuild - START")

                # metadata = dr.getMetadata(self.datasetName + ".SecurityMaster")
                
                metadata = Framework.Metadata()
                data = dr.getData(dci.universe, "SecurityMaster.Universe", noCacheUpdate = True, metadata = metadata)
                self.numSecuritiesParsed = metadata.getInt("count", defValue = 0)

                # BamUtil.debug(self.logChannel, "Existing number of securities parsed: %d" % (self.numSecuritiesParsed))

                if data:
                    data.thisown = False
                    secData = Framework.SecurityData.script_cast(data)
                    secData.thisown = False

                    numSecurities = secData.numSecurities()

                    BamUtil.debug(self.logChannel, "Num existing securities: %d" % (numSecurities))

                    for ii in range(numSecurities):
                        security = secData.security(ii)
                        uuid = security.uuidStr()

                        if uuid not in self.securityLookup:
                            securityCopy = Framework.Security(security)
                            self.securityLookup[uuid] = securityCopy
                            self.existingSecurities.append(securityCopy)

                BamUtil.debug(self.logChannel, "BamRestSecurityMaster::preDataBuild - DONE!")

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def buildUniverse(self, dr, dci, frame):
            BamUtil.debug(self.logChannel, "BamRestSecurityMaster::buildSecurityMaster for market: %s" % (self.config.config().defs().market()))

            securities = Framework.SecurityVec()
            diDelta = 30

            for exchange in self.exchanges:
                diIter = dci.diStart

                while diIter < dci.diEnd:

                    istartDate = dci.rt.getDate(diIter)
                    startDate = BamUtil.intDateToStrDate(istartDate)

                    diIter += diDelta
                    if diIter > dci.diEnd:
                        diIter = dci.diEnd

                    iendDate = dci.rt.getDate(diIter)
                    endDate = BamUtil.intDateToStrDate(iendDate)

                    BamUtil.debug(self.logChannel, "Security Master Updating - [%d - %d]" % (istartDate, iendDate))

                    data = self.getData(dci, {
                        "EXCH": "'" + exchange + "'",
                        "StartDate": "'" + startDate + "'",
                        "EndDate": "'" + endDate + "'",
                    })

                    # If there is no data to update ...
                    if data is None:
                        continue

                    uuids               = data["data"]["CFIGI"]
                    tickers             = data["data"]["BTKR"]

                    uuidsLen            = len(uuids)
                    assert uuidsLen == len(tickers), "Invalid data returned. Length of tickers: %d - Length of FIGIs: %d" % (len(tickers), uuidsLen)

                    for i in range(uuidsLen):
                        uuid            = uuids[i]
                        ticker          = tickers[i]

                        if uuid in self.securityLookup:
                            continue

                        BamUtil.debug(self.logChannel, "New security: %s - %s" % (uuid, ticker))

                        security        = Framework.Security()

                        security.index  = len(self.existingSecurities)
                        security.uuid   = uuid
                        security.name   = uuid
                        security.exchange = exchange
                        security.ticker = ticker

                        security.isInactive= False

                        securityCopy    = Framework.Security(security)

                        securities.append(security)
                        self.existingSecurities.append(securityCopy)
                        self.securityLookup[uuid] = securityCopy

            BamUtil.debug(self.logChannel, "Number of new securities: %d" % (len(securities)))

            if len(securities):
                frame.setData(BamUtil.script_sptr(Framework.SecurityData(securities, dci._def)))
                frame.dataOffset = 0
                frame.dataSize = len(securities) * Framework.Security.s_version

            if frame.dataSize is 0:
                frame.noDataUpdate = True

            self.numSecuritiesParsed = len(self.existingSecurities)
            BamUtil.debug(self.logChannel, "Overall securities parsed: %d" % (self.numSecuritiesParsed))

        def postBuild(self, dr, dci):
            try:
                pass
                # dataName = dci._def.name
                # BamUtil.debug(self.logChannel, "BamRestSecurityMaster::postBuild: %s" % (dataName))

                # if dataName == "Universe":
                #   dci.metadata.clear()
                #   dci.metadata.set("count", str(len(self.existingSecurities)))

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def build(self, dr, dci, frame):
            try:
                dataName = dci._def.name
                BamUtil.debug(self.logChannel, "BamRestSecurityMaster::build: %s" % (dataName))

                if dataName == "Universe":
                    return self.buildUniverse(dr, dci, frame)

            except Exception as e:
                print(traceback.format_exc())
                raise e

except Exception as e:
    print(traceback.format_exc())
    raise e
