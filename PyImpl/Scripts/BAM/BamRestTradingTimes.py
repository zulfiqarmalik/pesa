### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import traceback
import datetime as DT

import PyUtil
import sys

try:
    import Framework
    from BamRestData import BamRestData

    class BamRestTradingTimes(BamRestData):
        def __init__(self, baseObject):
            try:
                super().__init__(baseObject, "EXCH_TRADING_TIMES")
                self.debug("BamRestTradingTimes::__init__")

                self.loadConfig()

                self.holidayLookup = {}

                self.useMICCode     = self.config().getBool("useMICCode", False)
                self.countries, self.exchanges = BamUtil.parseExchanges(self.config().get("exchanges"))
                assert self.exchanges and len(self.exchanges), "Must specify which exchanges to load the calendar for!"

                self.holidayDates   = None
                self.holidayDesc    = None
                self.allHolidays    = []
                self.holidaysBuilt  = False
                self.cache          = {}

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def initDatasets(self, dr, datasets):
            try:
                self.debug("BamRestTradingTimes::initDatasets")

                ds = Framework.Dataset("calendar")

                # !!! Trading times !!!
                ds.add(Framework.DataValue("tradingTimes", Framework.DataType.kFloat, 4, Framework.DataFrequency.kDaily, 0))
                # ds.add(Framework.DataValue("utc_tradingClose", Framework.DataType.kUInt, 0, Framework.DataFrequency.kStatic, 0))

                # ds.add(Framework.DataValue("utc_preMarketOpen", Framework.DataType.kUInt, 0, Framework.DataFrequency.kStatic, 0))
                # ds.add(Framework.DataValue("utc_preMarketClose", Framework.DataType.kUInt, 0, Framework.DataFrequency.kStatic, 0))

                # ds.add(Framework.DataValue("utc_openingCrossOpen", Framework.DataType.kUInt, 0, Framework.DataFrequency.kStatic, 0))
                # ds.add(Framework.DataValue("utc_openingCrossClose", Framework.DataType.kUInt, 0, Framework.DataFrequency.kStatic, 0))

                # ds.add(Framework.DataValue("utc_closingCrossOpen", Framework.DataType.kUInt, 0, Framework.DataFrequency.kStatic, 0))
                # ds.add(Framework.DataValue("utc_closingCrossClose", Framework.DataType.kUInt, 0, Framework.DataFrequency.kStatic, 0))

                # ds.add(Framework.DataValue("utc_afterHoursOpen", Framework.DataType.kUInt, 0, Framework.DataFrequency.kStatic, 0))
                # ds.add(Framework.DataValue("utc_afterHoursClose", Framework.DataType.kUInt, 0, Framework.DataFrequency.kStatic, 0))

                datasets.add(ds)

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def preDataBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())
                raise e

        def cacheTradingTimesOnDateForExchange(self, dr, dci, date, exchange, country):
            eid = exchange + str(date)

            # if we already have it in the cache ...
            if eid in self.cache:
                return self.cache[eid];

            args = {
                "Date": "'" + BamUtil.intDateToStrDate(date) + "'"
            }

            self.debug("Exchange: %s (%d)" % (exchange, int(self.useMICCode)))

            if self.useMICCode is False:
                args["Exchange"] = "'" + exchange + "'"
                args["Date"] = "'" + BamUtil.intDateToStrDate(date) + "'"
            else:
                args["CountryCode"] = "'" + country + "'"
                args["MICExchangeCode"] = "'" + exchange + "'"
                args["StartDate"] = "'" + BamUtil.intDateToStrDate(date) + "'"
                args["EndDate"] = "'" + BamUtil.intDateToStrDate(date) + "'"

            data = self.getData(dci, args)

            self.cache[eid] = data
            return data

        # def cacheTradingTimes(self, dr, dci):
        #     for date in dci.rt.dates:
        #         for exchange in self.exchanges:
        #             self.cacheTradingTimesOnDateForExchange(dr, dci, date, exchange)
        #             # dts = DT.datetime.fromtimestamp(data["data"]["GMT_OPEN"][0] / 1000000000)
        #             # print(dts)


        def buildTradingTimes(self, dr, dci, dateField, activity):
            date = dci.rt.getDate(dci.di)
            exchangeData = []

            for exchangeIndex, exchange in enumerate(self.exchanges):
                country = self.countries[exchangeIndex]
                data = self.cacheTradingTimesOnDateForExchange(dr, dci, date, exchange, country)
                exchangeData.append(data)

            if "data" not in data:
                return None

            index = -1
            for i, a in enumerate(data["data"]["ACTIVITY"]):
                if a == activity:
                    index = i
                    break

            # assert index != -1, "Unable to find activity: %s" % (activity)
            # print(DT.datetime.fromtimestamp(data["data"][dateField][index] / 1000000000))
            # dts = DT.datetime.fromtimestamp(data["data"][dateField][index] / 1000000000)

            # readDate = BamUtil.dateToInt(dts)
            # assert readDate == date, "Invalid data. Expecting date: %d - Found: %d" % (date, readDate)

            return float(data["data"][dateField][index])

            # return BamUtil.timeToInt(dts)


        def postBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())
                raise e

        def build(self, dr, dci, frame):
            try:
                dataName = dci._def.name

                gmt_tradeOpen           = self.buildTradingTimes(dr, dci, dateField = "GMT_OPEN", activity = "Regular Trading")
                gmt_tradeClose          = self.buildTradingTimes(dr, dci, dateField = "GMT_CLOSE", activity = "Regular Trading")
                gmt_preMarketOpen       = self.buildTradingTimes(dr, dci, dateField = "GMT_OPEN", activity = "Pre-Market")
                gmt_preMarketClose      = self.buildTradingTimes(dr, dci, dateField = "GMT_CLOSE", activity = "Pre-Market")
                gmt_afterHoursOpen      = self.buildTradingTimes(dr, dci, dateField = "GMT_OPEN", activity = "After Hours")
                gmt_afterHoursClose     = self.buildTradingTimes(dr, dci, dateField = "GMT_CLOSE", activity = "After Hours")
                gmt_openingCrossOpen    = self.buildTradingTimes(dr, dci, dateField = "GMT_OPEN", activity = "Opening Cross")
                gmt_openingCrossClose   = self.buildTradingTimes(dr, dci, dateField = "GMT_CLOSE", activity = "Opening Cross")
                gmt_closingCrossOpen    = self.buildTradingTimes(dr, dci, dateField = "GMT_OPEN", activity = "Closing Cross")
                gmt_closingCrossClose   = self.buildTradingTimes(dr, dci, dateField = "GMT_CLOSE", activity = "Closing Cross")

                local_tradeOpen         = self.buildTradingTimes(dr, dci, dateField = "LOCALTIME_OPEN", activity = "Regular Trading")
                local_tradeClose        = self.buildTradingTimes(dr, dci, dateField = "LOCALTIME_CLOSE", activity = "Regular Trading")
                local_preMarketOpen     = self.buildTradingTimes(dr, dci, dateField = "LOCALTIME_OPEN", activity = "Pre-Market")
                local_preMarketClose    = self.buildTradingTimes(dr, dci, dateField = "LOCALTIME_CLOSE", activity = "Pre-Market")
                local_afterHoursOpen    = self.buildTradingTimes(dr, dci, dateField = "LOCALTIME_OPEN", activity = "After Hours")
                local_afterHoursClose   = self.buildTradingTimes(dr, dci, dateField = "LOCALTIME_CLOSE", activity = "After Hours")
                local_openingCrossOpen  = self.buildTradingTimes(dr, dci, dateField = "LOCALTIME_OPEN", activity = "Opening Cross")
                local_openingCrossClose = self.buildTradingTimes(dr, dci, dateField = "LOCALTIME_CLOSE", activity = "Opening Cross")
                local_closingCrossOpen  = self.buildTradingTimes(dr, dci, dateField = "LOCALTIME_OPEN", activity = "Closing Cross")
                local_closingCrossClose = self.buildTradingTimes(dr, dci, dateField = "LOCALTIME_CLOSE", activity = "Closing Cross")

                times = [
                    # GMT times
                    gmt_tradeOpen, gmt_tradeClose, gmt_preMarketOpen, gmt_preMarketClose, gmt_afterHoursOpen, gmt_afterHoursClose, 
                    gmt_openingCrossOpen, gmt_openingCrossClose, gmt_closingCrossOpen, gmt_closingCrossClose,

                    # LOCAL times
                    local_tradeOpen, local_tradeClose, local_preMarketOpen, local_preMarketClose, local_afterHoursOpen, local_afterHoursClose, 
                    local_openingCrossOpen, local_openingCrossClose, local_closingCrossOpen, local_closingCrossClose
                ]

                if gmt_tradeOpen is None:
                    frame.noDataUpdate = True
                    frame.usePrevData = True
                    return

                numTimes = len(times)
                dataPtr = Framework.FloatMatrixData(dci.universe, dci._def, 1, numTimes, False)

                self.debug("Trading times updated: %d" % (dci.rt.getDate(dci.di)))

                for i in range(numTimes):
                    dataPtr.setValue(0, i, times[i])

                frame.setData(BamUtil.script_sptr(dataPtr))
                frame.dataOffset = 0
                frame.dataSize = dataPtr.dataSize()

            except Exception as e:
                print(traceback.format_exc())
                raise e

except Exception as e:
    print(traceback.format_exc())
    raise e
