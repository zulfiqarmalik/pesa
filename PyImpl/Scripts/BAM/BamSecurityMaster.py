### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import traceback
import datetime as DT

import PyUtil
import sys

try:
    import Framework
    import BamData

    class BamSecurityMaster(BamData.BamData):
        def __init__(self, config, logChannel = None):
            try:
                super().__init__(config, "BAM_SEC_MASTER")
                self.logChannel = "BAM_SEC_MASTER"
                BamUtil.debug(self.logChannel, "BamSecurityMaster::__init__")

                self.loadConfig(self.logChannel)

                self.existingSecurities = []
                self.securityLookup = {}
                self.numSecuritiesParsed = 0

                self.exchanges = config.get("exchanges")
                if self.exchanges:
                    exchanges = self.exchanges.split(" ")
                    self.exchanges = {}

                    for exchange in exchanges:
                        self.exchanges[exchange] = True
                else:
                    self.exchanges = {}

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def initDatasets(self, dr, datasets):
            try:
                # datasets.add(Dataset("Universe", {
                #   DataValue("allSecurities", DataType::kCustom, sizeof(Security),
                #           THIS_DATA_FUNC(Quandl_UniverseBuilder::buildSecurities),
                #           DataFrequency::kStatic, DataFlags::kAlwaysLoad | DataFlags::kUpdatesInOneGo),

                #   DataValue("dailySecurities", DataType::kInt, sizeof(Security::Index),
                #           THIS_DATA_FUNC(Quandl_UniverseBuilder::buildDailySecurities),
                #           DataFrequency::kDaily, DataFlags::kNone),
                # }));

                BamUtil.debug(self.logChannel, "BamSecurityMaster::initDatasets")

                ds = Framework.Dataset("SecurityMaster")

                ds.add(Framework.DataValue(
                    "Universe", Framework.DataType.kSecurityData, 
                    Framework.Security.s_version, Framework.DataFrequency.kStatic, 
                    Framework.DataFlags.kAlwaysLoad | Framework.DataFlags.kUpdatesInOneGo))

                # ds.add(Framework.DataValue(
                #   "securityNames", Framework.DataType.kString, 
                #   Framework.Security.s_maxName, Framework.DataFrequency.kStatic, 
                #   Framework.DataFlags.kUpdatesInOneGo))

                datasets.add(ds)

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def preDataBuild(self, dr, dci):
            try:
                BamUtil.debug(self.logChannel, "BamSecurityMaster::preDataBuild - START")

                # metadata = dr.getMetadata(self.datasetName + ".SecurityMaster")
                
                metadata = Framework.Metadata()
                data = dr.getData(dci.universe, "SecurityMaster.Master", noCacheUpdate = True, metadata = metadata)
                self.numSecuritiesParsed = metadata.getInt("count", defValue = 0)

                BamUtil.debug(self.logChannel, "Existing number of securities parsed: %d" % (self.numSecuritiesParsed))

                if data:
                    data.thisown = False
                    secData = Framework.SecurityData.script_cast(data)
                    secData.thisown = False
                    securities = secData.script_vector()

                    # BamUtil.debug(self.logChannel, "Num existing securities: %d" % (len(securities)))

                    for security in securities:
                        secSymbol = security.symbolStr()

                        if secSymbol not in self.securityLookup:
                            securityCopy = Framework.Security(security)
                            self.securityLookup[secSymbol] = securityCopy
                            self.existingSecurities.append(securityCopy)

                BamUtil.debug(self.logChannel, "BamSecurityMaster::preDataBuild - DONE!")

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def buildUniverse(self, dr, dci, frame):
            from bamdata.api import API

            BamUtil.debug(self.logChannel, "BamSecurityMaster::buildSecurityMaster for market: %s" % (self.config.config().defs().market()))

            api = API(self.ymlPath)
            result = api.get(self.repository, Dates = (BamUtil.dateToStr(DT.datetime.today())), Region = self.config.config().defs().market(), Count = 50000)

            rawSecurities = self.parseResults(result)

            BamUtil.debug(self.logChannel, "Num existing securities: %d - Total in DB: %d" % (self.numSecuritiesParsed, len(rawSecurities)))

            if len(rawSecurities) <= self.numSecuritiesParsed:
                BamUtil.debug(self.logChannel, "Already parsed %d securities. Not updating ..." % (self.numSecuritiesParsed))
                frame.noDataUpdate = True
                return

            simDates = []

            for date in dci.rt.dates:
                simDates.append(BamUtil.intDateToStrDate(dci.rt.getDate(0))

            simDates = tuple(simDates)

            BamUtil.debug(self.logChannel, "Number of securities in search results: %d" % (len(rawSecurities)))

            existingSecurityCount = len(self.existingSecurities)
            securities = Framework.SecurityVec()

            priceRepository = self.config.get("priceRepository")
            priceField = self.config.get("priceField")

            for ii in range(self.numSecuritiesParsed, len(rawSecurities)):
                rawSecurity = rawSecurities[ii]

                if self.exchanges and rawSecurity["exchange"] not in self.exchanges:
                    continue

                security            = Framework.Security()

                security.index      = existingSecurityCount + len(securities)
                security.uuid       = rawSecurity["uuid"]
                security.name       = rawSecurity["name"]
                security.exchange   = rawSecurity["exchange"]
                security.ticker     = rawSecurity["ticker"]

                assert security.exchange, "Unable to parse exchange!"
                assert security.ticker, "Unable to parse ticker!"

                security.isInactive = bool(rawSecurity["isInactive"])

                if "cusip" in rawSecurity:
                    security.cusip = rawSecurity["cusip"]
                elif "sedol" in rawSecurity:
                    security.sedol = rawSecurity["sedol"]
                else:
                    assert False, "Security must have at least a valid cusip or sedol: %s" % (str(rawSecurity))

                secSymbol           = security.symbolStr()

                if priceRepository and priceField:
                    priceResult = api.get(priceRepository, Dates = simDates, Cusips = tuple([rawSecurity["cusip"]]), Region = self.config.config().defs().market())

                    if priceResult.empty or len(priceResult.get_values()) == 0:
                        BamUtil.debug(self.logChannel, "DEFUNCT: %s => %s (%s-%s)" % (security.name, security.cusip, security.ticker, security.exchange))
                        continue

                # if "Microsoft" in security.name:
                #   print("%s -> %s (%s, %s-%s)" % (security.name, security.ticker, security.cusip, security.ticker, security.exchange))
                # elif "Microsoft" in security.name:
                #   print("%s -> %s (%s, %s-%s)" % (security.name, security.ticker, security.cusip, security.ticker, security.exchange))
                # elif "Alphabet" in security.name:
                #   print("%s -> %s (%s, %s-%s)" % (security.name, security.ticker, security.cusip, security.ticker, security.exchange))
                # elif "Google" in security.name:
                #   print("%s -> %s (%s, %s-%s)" % (security.name, security.ticker, security.cusip, security.ticker, security.exchange))
                # elif "Yahoo" in security.name:
                #   print("%s -> %s (%s, %s-%s)" % (security.name, security.ticker, security.cusip, security.ticker, security.exchange))
                # elif "Facebook" in security.name:
                #   print("%s -> %s (%s, %s-%s)" % (security.name, security.ticker, security.cusip, security.ticker, security.exchange))

                secSymbol = security.symbolStr()

                if secSymbol in self.securityLookup:
                    BamUtil.debug(self.logChannel, "Security alread in the cache: %s" % (secSymbol))
                    continue

                BamUtil.debug(self.logChannel, "Cached: %d - %s => %s (%s-%s)" % (security.index, security.name, security.cusip, security.ticker, security.exchange))
                securities.append(security)

            BamUtil.debug(self.logChannel, "Securities after filtering exchanges: %d" % (len(securities)))

            frame.data = BamUtil.script_sptr(Framework.SecurityData(securities, dci._def))
            frame.dataOffset = 0
            frame.dataSize = len(securities) * Framework.Security.s_version

            if frame.dataSize is 0:
                frame.noDataUpdate = True

            self.numSecuritiesParsed = len(rawSecurities)
            BamUtil.debug(self.logChannel, "Overall securities parsed: %d" % (self.numSecuritiesParsed))

        def postBuild(self, dr, dci):
            try:
                dataName = dci._def.name
                BamUtil.debug(self.logChannel, "BamSecurityMaster::postBuild: %s" % (dataName))

                if dataName == "Universe":
                    dci.metadata.clear()
                    dci.metadata.set("count", str(self.numSecuritiesParsed))

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def build(self, dr, dci, frame):
            try:
                dataName = dci._def.name
                BamUtil.debug(self.logChannel, "BamSecurityMaster::build: %s" % (dataName))

                if dataName == "Universe":
                    return self.buildUniverse(dr, dci, frame)
                elif dataName == "SecurityNames":
                    return self.buildSecurityNames(dr, dci, frame)

            except Exception as e:
                print(traceback.format_exc())
                raise e

except Exception as e:
    print(traceback.format_exc())
    raise e
