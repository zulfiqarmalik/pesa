### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import traceback
import datetime as DT

import PyUtil
import sys

try:
    import Framework
    import BamData
    from PyBase import PyUniverse

    class BamUniverse(PyUniverse):
        def __init__(self, baseObject):
            try:
                super().__init__(baseObject, "BAM_UNIVERSE")
                self.debug("BamUniverse::__init__")

                config = self.config()
                self.name = config.get("name")
                assert self.name, "Must have a name field in the config"

                self.cusips = config.get("cusips")
                assert self.cusips, "Must have a comma separated list of cusips that need to be loaded"

                cusips = self.cusips.split(",")
                self.cusips = []

                for cusip in cusips:
                    self.cusips.append(cusip.strip())

                self.dataPtr = None
                self.ids = []

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def initDatasets(self, dr, datasets):
            try:
                self.debug("BamUniverse::initDatasets")

                ds = Framework.Dataset(self.name)
                ds.add(Framework.DataValue("Universe", Framework.DataType.kUInt, 4, Framework.DataFrequency.kStatic, 0))
                datasets.add(ds)

            except Exception as e:
                print(traceback.format_exc())
                raise e


        def build(self, dr, dci, frame):
            try:
                self.dataPtr = Framework.UIntMatrixData(dci.universe, dci._def, 1, len(self.ids), False)

                for ii, _id in enumerate(self.ids):
                    self.dataPtr.setIntValue(0, ii, int(_id))

                frame.setData(BamUtil.script_sptr(self.dataPtr))
                frame.dataOffset = 0
                frame.dataSize = self.dataPtr.dataSize()

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def preDataBuild(self, dr, dci):
            try:
                self.debug("BamUniverse::preDataBuild")

                securityMaster = dr.getSecurityMaster()
                assert securityMaster, "Cannot build universe without a security master!"

                self.ids = []
                for cusip in self.cusips:
                    security = securityMaster.mapUuid(cusip)

                    if security:
                        self.debug("Got security: %d - %s => %s (%s-%s)" % (security.index, security.name, security.symbolStr(), security.ticker, security.exchange))
                        self.ids.append(security.index)
                    else:
                        self.debug("Unable to find security: %s" % (cusip))

                assert self.ids and len(self.ids) > 0, "Invalid list of ids retrieved from cusips: " + str(self.cusips)

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def postDataBuild(self, dr, dci):
            try:
                self.debug("BamUniverse::postDataBuild")
                self.ids = None
            except Exception as e:
                print(traceback.format_exc())
                raise e

        def preBuild(self, dr, dci):
            try:
                self.debug("BamUniverse::preBuild")
            except Exception as e:
                print(traceback.format_exc())
                raise e

        def postBuild(self, dr, dci):
            try:
                self.debug("BamUniverse::postBuild")
            except Exception as e:
                print(traceback.format_exc())
                raise e

except Exception as e:
    print(traceback.format_exc())
    raise e
