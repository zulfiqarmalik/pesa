### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import sys
import datetime as DT
import Framework as FW
import ctypes
import numpy as np

def debug(logChannel, *args):
    fstring = str.format(*args)
    FW.debug("PYTHON - " + logChannel, fstring)

def trace(logChannel, *args):
    fstring = str.format(*args)
    FW.trace("PYTHON - " + logChannel, fstring)

def info(logChannel, *args):
    fstring = str.format(*args)
    FW.info("PYTHON - " + logChannel, fstring)

def warning(logChannel, *args):
    fstring = str.format(*args)
    FW.warning("PYTHON - " + logChannel, fstring)

def error(logChannel, *args):
    fstring = str.format(*args)
    FW.error("PYTHON - " + logChannel, fstring)

def critical(logChannel, *args):
    fstring = str.format(*args)
    FW.critical("PYTHON - " + logChannel, fstring)

def script_sptr(dataPtr):
    dataPtr.thisown = False # C++ is now responsible for this pointer!
    return dataPtr.script_sptr()

def strToDate(date, sep = "-"):
    return DT.datetime.strptime(date, "%Y" + sep + "%m" + sep + "%d")
    
def intToDate(date):
    return DT.datetime.strptime(str(date), "%Y%m%d")

def intDateToStrDate(date, sep = "-"):
    return dateToStr(intToDate(date), sep)

def strDateToIntDate(date, sep = "-"):
    return dateToInt(strToDate(date, sep))

def dateToStr(date, sep = "-"):
    return str(date.year).zfill(4) + sep + str(date.month).zfill(2) + sep + str(date.day).zfill(2)
    
def dateToInt(date):
    return int(str(date.year).zfill(4) + str(date.month).zfill(2) + str(date.day).zfill(2))

def timeToInt(dt):
    return int(str(dt.hour).zfill(2) + str(dt.minute).zfill(2) + str(dt.second).zfill(2))

def intTimeToDatetime(date):
    return DT.datetime.fromtimestamp(int(date) / 1000000000)

def getStrDates(intDates):
    strDates = []

    for intDate in intDates:
        strDates.append(intDateToStrDate(intDate))

    return strDates
    
def resolveFrequency(f):
    import Framework 

    f = f.lower()

    if f == "daily":
        return Framework.DataFrequency.kDaily
    elif f == "universal":
        return Framework.DataFrequency.kStatic
    elif f == "weekly":
        return Framework.DataFrequency.kWeekly
    elif f == "monthly":
        return Framework.DataFrequency.kMonthly
    elif f == "quarterly":
        return Framework.DataFrequency.kQuarterly
    elif f == "semiAnnually":
        return Framework.DataFrequency.kSemiAnnually
    elif f == "annually":
        return Framework.DataFrequency.kAnnually

    assert False, "Invalid frequency: %s" % (f)

def resolveDataType(dataType):
    import Framework 

    dataType = dataType.lower()

    if dataType == "float":
        return Framework.DataType.kFloat
    if dataType == "float[]":
        return Framework.DataType.kFloatArray
    elif dataType == "int":
        return Framework.DataType.kInt
    elif dataType == "int[]":
        return Framework.DataType.kIntArray
    elif dataType == "string":
        return Framework.DataType.kString
    elif dataType == "blob":
        return Framework.DataType.kBlob
    elif dataType == "custom":
        return Framework.DataType.kCustom
    elif dataType == "unknown":
        return Framework.DataType.kUnknown

    assert False, "Unsupported type: %s" % (dataType)

def getDataSize(dataType):
    import Framework 
    
    if dataType == Framework.DataType.kFloat:
        return 4
    elif dataType == Framework.DataType.kInt:
        return 4

    # assert False, "Unsupported type for getDataSize: %s" % (dataType)

    return 0

def parseDataOps(dataOps):
    retDataOps = {}

    dataOps = dataOps.split(",")
    for dataOp in dataOps:
        dataOp.strip()
        fmt, value = dataOp.split("=")
        retDataOps[value] = fmt

    return retDataOps

def parseFunctions(functions):
    functions = functions.split(",")
    funcMap = {}

    for ii, function in enumerate(functions):
        function = function.strip()
        lhs, rhs = function.split("=")
        rhs = rhs.replace("{", "dataValue['")
        rhs = rhs.replace("}", "']")
        funcMap[lhs] = rhs

    return funcMap


def parseDatasetMappings(mappings):
    mappings = mappings.split(",")
    forwardMapping = {}
    reverseMapping = {}

    for ii, mapping in enumerate(mappings):
        mapping = mapping.strip()
        if mapping:
            original, mapping = mapping.split("=")
            forwardMapping[original] = mapping
            reverseMapping[mapping] = original

    return (forwardMapping, reverseMapping)

def rawPtrToNPGeneric(dataPtr, size, ctype, npType):
    if dataPtr is None:
        return None 
        
    arrayType = ctype * size
    address = ctypes.cast(dataPtr, ctypes.POINTER(arrayType))
    # npArray = np.frombuffer(arrayType.from_address(address))
    npArray = np.frombuffer(address.contents, npType)

    return npArray

def unpackPyCapsule(capsule):
    if capsule is None:
        return None
        
    import ctypes
    ctypes.pythonapi.PyCapsule_GetPointer.restype = ctypes.c_void_p
    ctypes.pythonapi.PyCapsule_GetPointer.argtypes = [ctypes.py_object, ctypes.c_void_p]
    return ctypes.pythonapi.PyCapsule_GetPointer(capsule, None)

def typeToNPGeneric(fdata, ctype, nptype, flattenDimension):
    rows = fdata.rows()
    cols = fdata.cols()
    matrixSize = rows * cols
    dataPtr = fdata.fptr()
    dataPtr = unpackPyCapsule(dataPtr)
    npArray = rawPtrToNPGeneric(dataPtr, matrixSize, ctype, nptype)
    
    if flattenDimension is False:
        return npArray.reshape((rows, cols))
        
    return npArray

def floatRowAsNPArray(fdata, rowIndex):
    cols = fdata.cols()
    matrixSize = cols
    dataPtr = fdata.rowPtr(rowIndex)
    dataPtr = unpackPyCapsule(dataPtr)
    ctype = ctypes.c_float
    nptype = np.float32
    npArray = rawPtrToNPGeneric(dataPtr, matrixSize, ctype, nptype)

    npArray.reshape(1, cols)
    return npArray

def floatToNPGeneric(fdata, flattenDimension = False):
    return typeToNPGeneric(fdata, ctypes.c_float, np.float32, flattenDimension)

def floatToNPMatrix(fdata):
    return floatToNPGeneric(fdata, flattenDimension = False)

def intToNPMatrix(fdata, flattenDimension = False):
    return typeToNPGeneric(fdata, ctypes.c_int, np.int32, flattenDimension)

def uintToNPMatrix(fdata, flattenDimension = False):
    return typeToNPGeneric(fdata, ctypes.c_uint, np.uint32, flattenDimension)

def int64ToNPMatrix(fdata, flattenDimension = False):
    return typeToNPGeneric(fdata, ctypes.c_longlong, np.int64, flattenDimension)

def ulongToNPMatrix(fdata, flattenDimension = False):
    return typeToNPGeneric(fdata, ctypes.c_ulong, np.uint64, flattenDimension)

def doubleToNPMatrix(fdata, flattenDimension = False):
    return typeToNPGeneric(fdata, ctypes.c_double, np.float64, flattenDimension)

def floatToNPArray(fdata):
    return floatToNPGeneric(fdata, flattenDimension = True)

def floatVecToNPArray(fvec, fvecPtr):
    size = fvec.size()
    fvecPtr = unpackPyCapsule(fvecPtr)
    return rawPtrToNPGeneric(fvecPtr, size, ctypes.c_float, np.float32)

def parseDatasets(strDatasets):
    import Framework 
    
    datasets = strDatasets.split(",")

    resolvedDatasets = []

    for i, dataset in enumerate(datasets):
        defs = dataset.strip().split(".")
        defsLength = len(defs)
        assert defsLength >= 2 and defsLength <= 3, "Invalid dataset definition: %s [%s]" % (defs, dataset)

        dataType = defs[0]
        dataName = defs[1]
        frequency = Framework.DataFrequency.kDaily

        if defsLength == 3:
            frequency = resolveFrequency(defs[2])

        dataType = resolveDataType(dataType)

        resolvedDatasets.append({
            "type": dataType,
            "typeSize": getDataSize(dataType),
            "name": dataName,
            "frequency": frequency
        })

    return resolvedDatasets

def parseExchanges(exchanges):
    retExchanges = []
    retCountries = []
    exchanges = exchanges.split("|")
    assert exchanges, "Must specify which exchanges to load the calendar for!"

    for exchange in exchanges:
        country, exList = exchange.split(":")
        exList = exList.split(",")
        for ex in exList:
            retExchanges.append(ex.strip())
            retCountries.append(country.strip())
            
    assert retExchanges, "Must specify which exchanges to load the calendar for!"
    return retCountries, retExchanges

# def runStandalone(psimPath):
#     import sys
    
#     psimPath = "P:\\Work\\Pesa\\build\\Debug\\windows\\psim.exe" 
#     os.chdir(os.path.dirname(psimPath))
#     cwd = os.getcwd()

#     sys.path.append(".")
#     sys.path.append(os.path.join(os.path.dirname(psimPath), "Python"))
    
#     import Framework as FW
#     args = FW.StringVec()
#     args.append("-c")
#     args.append("P:\\Work\\Pesa\\Configs\\test_config.xml")
#     app = FW.App()
#     app.pyInit(psimPath, args)
#     app.run()
