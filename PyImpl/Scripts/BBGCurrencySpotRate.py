### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import traceback
import datetime as DT

import PyUtil
import sys

import numpy as np
from pandas import DataFrame

try:
    import Framework as FW
    from PyBase import PyDataLoader

    class BBGCurrencySpotRate(PyDataLoader):
        def __init__(self, baseObject):
            super().__init__(baseObject, "BBGCurrencySpotRate")
            self.debug("BBGCurrencySpotRate::init")
            self.dataBuilt = False

        def initDatasets(self, dr, datasets):
            try:
                config                  = self.config()
                self.datasetName        = config.get("datasetName")

                self.bpipeHost          = config.getString('BPIPEHost', '69.184.252.131')
                self.bpipePort          = int(config.getString('BPIPEPort', '8194'))
                self.bpipeLogonType     = config.getString('BPIPELogon', 'user').lower()
                self.bpipeUserIpAddress = config.getString('BPIPEUserIPAddress', '10.113.113.155')
                self.bpipeUserAuthId    = config.getString('BPIPEUserAuthID', 'FOUNTAINHEAD\jdemiranda')

                self.bpipeFields        = config.getString('BPIPEFields', 'LAST_PRICE')
                self.currencies         = self.parentConfig().defs().resolve(config.getRequired("currency"), 0)
                self.bpipeFieldMap      = {}
                self.bpipeFieldTypes    = []

                self.currencies         = [x.strip() for x in self.currencies.split('|')]
                self.numCurrencies      = len(self.currencies)

                assert self.numCurrencies, "Invalid currency list!"

                ds = FW.Dataset(self.datasetName)

                for i in range(self.numCurrencies):
                    flags               = FW.DataFlags.kUpdatesInOneGo | FW.DataFlags.kAlwaysOverwrite
                    currency            = self.currencies[i]
                    ftype               = FW.DataType.kFloat

                    self.bpipeFieldMap[currency] = i
                    self.bpipeFieldTypes.append(ftype)

                    ds.add(FW.DataValue(currency, ftype, FW.DataType.size(ftype), FW.DataFrequency.kStatic, flags))

                datasets.add(ds)

            except Exception as e:
                print(traceback.format_exc())
                raise e


        def build(self, dr, dci, frame):
            try:
                self.buildData(dr, dci)

                dataPtr = FW.FloatMatrixData(None, dci._def, 1, 1, False)

                ccyIndex = self.bpipeFieldMap[dci._def.name]
                spotRate = self.currencySpotRates[ccyIndex]
                dataPtr.setValue(0, 0, spotRate)

                frame.setData(BamUtil.script_sptr(dataPtr))
                frame.dataOffset = 0
                frame.dataSize = dataPtr.dataSize()

            except Exception as e:
                print(traceback.format_exc())

        def preDataBuild(self, dr, dci):
            pass

        def buildData(self, dr, dci):
            try:
                from bbgApi import BPIPE

                # Data has already been built
                if self.dataBuilt:
                    return

                allBBTickers = []

                for ii in range(self.numCurrencies):
                    ccyTicker = self.currencies[ii] + " Curncy"
                    allBBTickers.append(ccyTicker)

                # it may be better to use a subscription rather than a reference data request
                t = None
                if self.bpipeLogonType == 'user':
                    t = BPIPE(host=self.bpipeHost,port=self.bpipePort,logonType=self.bpipeLogonType, userIpAddress=self.bpipeUserIpAddress, userAuthId=self.bpipeUserAuthId)
                    df = t.get_reference_data(allBBTickers, self.bpipeFields, ignore_security_error=True).as_frame()
                elif self.bpipeLogonType == 'app':
                    t = BPIPE(host=self.bpipeHost,port=self.bpipePort,logonType='app')
                    df = t.get_reference_data(allBBTickers, self.bpipeFields, ignore_security_error=True).as_frame()
                elif self.bpipeLogonType == 'dummy':
                    df = DataFrame(index=allBBTickers, columns=self.bpipeFields)
                    df[:] = 1

                # self.currencySpotRates = np.ones((self.bpipeNumFields, self.numCurrencies)) * np.nan
                self.currencySpotRates = []
                for ii in range(self.numCurrencies):
                    currency = self.currencies[ii]
                    s = df[self.bpipeFields]
                    val = s.get(currency + ' Curncy', np.nan)
                    self.currencySpotRates.append(val)

                self.dataBuilt = True
                

            except Exception as e:
                print(traceback.format_exc())

        def postDataBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())

        def preBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())

        def postBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())

except Exception as e:
    print(traceback.format_exc())
    raise e
