### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import traceback
import datetime as DT

import PyUtil
import sys

import numpy as np
from pandas import DataFrame

try:
    import Framework as FW
    from PyBase import PyDataLoader

    class BBGDataLoader(PyDataLoader):
        def __init__(self, baseObject):
            super().__init__(baseObject, "BBGDataLoader")
            self.debug("BBGDataLoader::init")
            self.dataBuilt = False

        def initDatasets(self, dr, datasets):
            try:
                config                  = self.config()
                self.datasetName        = config.get("datasetName")
                self.figis              = []

                self.bpipeHost          = config.getString('BPIPEHost', '69.184.252.131')
                self.bpipePort          = int(config.getString('BPIPEPort', '8194'))
                self.bpipeLogonType     = config.getString('BPIPELogon', 'user').lower()
                self.bpipeUserIpAddress = config.getString('BPIPEUserIPAddress', '10.113.113.155')
                self.bpipeUserAuthId    = config.getString('BPIPEUserAuthID', 'FOUNTAINHEAD\\jdemiranda')

                self.bpipeFields        = config.getString('BPIPEFields', 'LAST_PRICE')
                self.bpipeFieldMap      = config.getString('BPIPEFieldMap', '')
                self.bpipeFieldTypes    = []

                self.useFIGI            = config.getBool("useFigi", False)

                self.afterMarketOpen    = config.getBool("afterMarketOpen", True)

                if ',' in self.bpipeFields: # it is a list of fields:
                    self.bpipeFields    = [x.strip() for x in self.bpipeFields.split(',')]
                else: # single field, convert into a list
                    self.bpipeFields    = [self.bpipeFields]

                self.bpipeNumFields     = len(self.bpipeFields)

                if self.bpipeFieldMap == '':
                    self.bpipeFieldMap  = self.bpipeFields
                else:
                    if ',' in self.bpipeFieldMap:
                        self.bpipeFieldMap = [x.strip() for x in self.bpipeFieldMap.split(',')]
                    else:
                        self.bpipeFieldMap = [self.bpipeFieldMap]

                if self.bpipeNumFields != len(self.bpipeFieldMap):
                    raise Exception('Number of bloomberg field maps must match number of fields.')

                ds = FW.Dataset(self.datasetName)
                for i in range(self.bpipeNumFields):
                    name = self.bpipeFieldMap[i]
                    ftype = FW.DataType.kFloat

                    flags = FW.DataFlags.kUpdatesInOneGo | FW.DataFlags.kAlwaysOverwrite

                    if self.afterMarketOpen:
                        flags |= FW.DataFlags.kUpdateAfterMarketOpen

                    if "." in name:
                        typeStr, name = name.split(".")
                        ftype = FW.DataType.parse(typeStr)

                    if ftype is FW.DataType.kString:
                        flags = FW.DataFlags.kUpdatesInOneGo | FW.DataFlags.kAlwaysOverwrite

                    self.bpipeFieldMap[i] = name
                    self.bpipeFieldTypes.append(ftype)

                    ds.add(FW.DataValue(self.bpipeFieldMap[i], ftype, FW.DataType.size(ftype), FW.DataFrequency.kStatic, flags))

                datasets.add(ds)

            except Exception as e:
                print(traceback.format_exc())
                raise e


        def build(self, dr, dci, frame):
            try:
                self.buildData(dr, dci)

                if dci._def.itemType is FW.DataType.kFloat:
                    dataPtr = FW.FloatMatrixData(dci.universe, dci._def, 1, dci.universe.size(dci.di), False)
                elif dci._def.itemType is FW.DataType.kString:
                    dataPtr = FW.StringData(dci.universe, dci.universe.size(dci.di), dci._def)
                else:
                    assert False, "Unsupported data type: %d [%s]" % (dci._def.itemType, dci._def.name)

                field_j = self.bpipeFieldMap.index(dci._def.name)
                for ii in range(self.numSecurities):
                    val = self.storedVal[field_j][ii]
                    figi = self.figis[ii]
                    sec = dci.universe.mapUuid(figi)
                    assert sec, "Unable to mapUuid: %s" % (figi)
                    dataPtr.setValue(0, sec.index, val)

                frame.setData(BamUtil.script_sptr(dataPtr))
                frame.dataOffset = 0
                frame.dataSize = dataPtr.dataSize()
                

            except Exception as e:
                print(traceback.format_exc())

        def preDataBuild(self, dr, dci):
            pass

        def buildData(self, dr, dci):
            try:
                from bbgApi import BPIPE

                # Data has already been built
                if self.dataBuilt:
                    return

                # self.universe = dr.getUniverse(self.universeName) #dr.getSecurityMaster()
                # allBBTickers = []
                # self.numSecurities = self.universe.size(0) #ignores the argument
                # for ii in range(self.numSecurities):
                #     allBBTickers.append(self.universe.security(0,ii).bbTickerStr())

                allBBTickers = []
                self.numSecurities = dci.secSrcUniverse.size(dci.di)
                self.figis = []

                for ii in range(self.numSecurities):
                    sec = dci.secSrcUniverse.security(0, ii)

                    figi = sec.uuidStr()
                    self.figis.append(figi)

                    if self.useFIGI is False:
                        bbTicker = sec.bbTickerStr()
                        bbTicker = bbTicker.replace(" GR", " GY") # This is required for German stocks (XETRA / XFRA quirk!)
                        allBBTickers.append(bbTicker)
                    else:
                        allBBTickers.append(figi)


                if self.useFIGI is False:
                    allBBTickers = [x + " Equity" for x in allBBTickers]
                values = np.ones((self.bpipeNumFields,self.numSecurities))*np.nan

                # it may be better to use a subscription rather than a reference data request
                t = None
                if self.bpipeLogonType == 'user':
                    t = BPIPE(host=self.bpipeHost,port=self.bpipePort,logonType=self.bpipeLogonType, userIpAddress=self.bpipeUserIpAddress, userAuthId=self.bpipeUserAuthId)
                    df = t.get_reference_data(allBBTickers, self.bpipeFields, ignore_security_error=True).as_frame()
                elif self.bpipeLogonType == 'app':
                    t = BPIPE(host=self.bpipeHost,port=self.bpipePort,logonType='app')
                    df = t.get_reference_data(allBBTickers, self.bpipeFields, ignore_security_error=True).as_frame()
                elif self.bpipeLogonType == 'dummy':
                    df = DataFrame(index=allBBTickers, columns=self.bpipeFields)
                    df[:] = 1

                # self.storedVal = np.ones((self.bpipeNumFields, self.numSecurities)) * np.nan
                self.storedVal = []
                for jj in range(self.bpipeNumFields):
                    s = df[self.bpipeFields[jj]]
                    ftype = self.bpipeFieldTypes[jj]
                    currVal = []

                    if ftype is FW.DataType.kString:
                        for ii in range(self.numSecurities):
                            val = str(s.get(allBBTickers[ii], ""))
                            if not val:
                                currVal.append("@" + allBBTickers[ii])
                            else:
                                currVal.append(val)

                    else: # Float
                        for ii in range(self.numSecurities):
                            val = s.get(allBBTickers[ii], np.nan)
                            currVal.append(val)

                        # self.storedVal[jj,ii] = val

                    self.storedVal.append(currVal)

                self.dataBuilt = True
                

            except Exception as e:
                print(traceback.format_exc())

        def postDataBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())

        def preBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())

        def postBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())

except Exception as e:
    print(traceback.format_exc())
    raise e
