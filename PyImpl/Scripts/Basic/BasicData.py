### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import sys
import traceback

try:
    import Framework
    import PyUtil
    from PyBase import PyDataLoader

    class BasicData(PyDataLoader):
        def __init__(self, baseObject, logChannel):
            try:
                super().__init__(baseObject, logChannel)

            except Exception as e:
                print(traceback.format_exc())

        def loadConfig(self, logChannel = None):
            if logChannel is not None:
                self._logChannel = logChannel

            if self._logChannel is None:
                self._logChannel = "BASIC_DATA"

            # self.datasetName = self.config().get("datasetName")
            # assert self.datasetName, "Must have a datasetName!"

            # self.datasets = self.config().get("datasets")
            # assert self.datasets, "Must have a valid list of (comma separated) datasets which to load from the repository!"

            # self.datasets = PyUtil.parseDatasets(self.datasets)
            # PyUtil.debug("BASIC_DATA", "Got datasets: %s" % (str(self.datasets)))

            # datasetMappings = self.config().get("mappings")
            # self.forwardMappings = {}
            # self.reverseMappings = {}

            # self.singleDate = self.config().getBool("singleDate", False)
            # self.intDate = self.config().getBool("intDate", False)

            # if datasetMappings:
            #     self.forwardMappings, self.reverseMappings = PyUtil.parseDatasetMappings(datasetMappings)

        def initDatasets(self, dr, datasets):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())

        def build(self, dr, dci, frame):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())

        def postDataBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())

        def preBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())

        def postBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())

except Exception as e:
    print(traceback.format_exc())
