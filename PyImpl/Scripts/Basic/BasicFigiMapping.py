### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

######################################################
### ConfigSection.h
###
### Created on: 05 May 2018
###     Author: zulfiqar
######################################################

import traceback
import datetime as DT

import PyUtil
import sys
from collections import namedtuple, OrderedDict
import csv
import requests
import json
from enum import Enum

try:
    import Framework
    from BasicData import BasicData

    class BasicFigiMapping(BasicData):
        def __init__(self, baseObject):
            try:
                super().__init__(baseObject, "EXCH_HOLIDAY_CALENDAR")
                self.debug("BasicFigiMapping::__init__")

                self.loadConfig()

                self.figiInfo           = None
                self.datasetName        = self.config().getRequired("datasetName")
                self.datasetNames       = self.config().getRequired("datasetNames").split(',')
                self.datasetTypes       = self.config().getRequired("datasetTypes").split(',')
                self.figiServerUrl      = self.config().getRequired("figiServerUrl")
                self.figiServerApiKey   = self.config().getResolvedRequiredString("figiServerApiKey") 
                self.filename           = self.config().getResolvedRequiredString("filename")

                exchangeMappingsStr     = self.config().getResolvedRequiredString("exchangeMappings")
                self.exchangeMappings, self.exchangeRMappings = PyUtil.parseDatasetMappings(exchangeMappingsStr)

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def initDatasets(self, dr, datasets):
            try:
                self.debug("BasicFigiMapping::initDatasets")

                ds = Framework.Dataset(self.datasetName)

                self.moreInfoData = OrderedDict([
                    ("figi", "figi"),
                    ("name", "name"),
                    ("ticker", "ticker"),
                    ("exchCode", "exchCode"),
                    ("cfigi", "compositeFIGI"),
                    ("uniqueId", "uniqueID"),
                    ("securityType", "securityType"),
                    ("marketSector", "marketSector"),
                    ("shareClassFigi", "shareClassFIGI"),
                    ("desc", "securityDescription"),
                ])

                for name in self.moreInfoData:
                    ds.add(Framework.DataValue(name, Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite))

                # ds.add(Framework.DataValue("parentUuid", Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite))

                datasets.add(ds)

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def preDataBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())
                raise e

        def buildForDataset(self, dr, dci, datasetName, datasetType):
            data            = dr.stringData(None, datasetName, noCacheUpdate = True)
            numSecurities   = data.cols()

            if self.figiInfo is None:
                self.figiInfo = [None] * numSecurities
            else:
                assert numSecurities == len(self.figiInfo), "Number of securities are not matched. Expecting: %d - Found: %d" % (len(self.figiInfo), numSecurities)

            secMap = {}
            secList = []
            exchCodes = []

            for ii in range(numSecurities):
                secId = data.getValue(0, ii)

                if self.figiInfo[ii] is None and secId:

                    if datasetType == "instrument":
                        parts = secId.split(".")
                        assert parts[0] in self.exchangeMappings, "Unable to find mapping for exchange: %s [Instrument: %s]" % (parts[0], secId)
                        exchCode = self.exchangeMappings[parts[0]]
                        exchCodes.append(exchCode)
                        secId = parts[2]

                        if len(parts) > 3:
                            for pi in range(len(parts)):
                                secId += "." + parts[pi]

                    secList.append(secId)
                    secMap[secId] = ii

            self.debug("Trying to match: %d - %s [%s]" % (len(secList), datasetName, datasetType))
            securities, secMoreInfo = PyUtil.resolveSecurityForFigi(secList, self.figiServerUrl, self.figiServerApiKey, datasetType, exchCodes = exchCodes, filename = self.filename)

            for i in range(len(securities)):
                secId = securities[i]

                ii = secMap[secId]

                if self.figiInfo[ii] is None:
                    self.figiInfo[ii] = secMoreInfo[i]

        def buildSecurities(self, dr, dci):
            # We have already built this!
            if self.figiInfo is not None:
                return

            for dsi in range(len(self.datasetNames)):
                self.buildForDataset(dr, dci, self.datasetNames[dsi], self.datasetTypes[dsi])

        def buildGenericMoreInfo(self, dr, dci, frame, name):
            self.buildSecurities(dr, dci)

            numSecurities   = len(self.figiInfo)
            dataPtr         = Framework.StringData(dci.universe, numSecurities, dci._def)
            data            = dr.stringData(None, "pp0.ms_instrument", noCacheUpdate = True)

            for i in range(numSecurities):
                moreInfo    = self.figiInfo[i]
                secId       = data.getValue(0, i)

                if moreInfo is not None and name in moreInfo:
                    value = moreInfo[name]
                    if value == "None" or value == None:
                        value = ""
                    dataPtr.setValue(0, i, value)

            frame.setData(PyUtil.script_sptr(dataPtr))
            frame.dataOffset = 0
            frame.dataSize = dataPtr.dataSize()

        def build(self, dr, dci, frame):
            try:
                dataName = dci._def.name
                self.debug("BasicFigiMapping::build: %s.%s" % (self.datasetName, dataName))

                return self.buildGenericMoreInfo(dr, dci, frame, self.moreInfoData[dataName])

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def postBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())
                raise e

except Exception as e:
    print(traceback.format_exc())
    raise e
