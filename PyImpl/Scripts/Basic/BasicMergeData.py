### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

######################################################
### ConfigSection.h
###
### Created on: 05 May 2018
###     Author: zulfiqar
######################################################

import traceback
import datetime as DT

import PyUtil
import sys
from collections import namedtuple, OrderedDict
import csv
import requests
import json

try:
    import Framework
    from BasicData import BasicData

    class BasicMergeData(BasicData):
        def __init__(self, baseObject):
            try:
                super().__init__(baseObject, "BasicMergeData")
                self.debug("BasicMergeData::__init__")

                self.loadConfig()
    
                mappings        = [x.strip() for x in self.config().getRequired("mappings").split(',')]
                self.datasetName= self.config().getString("datasetName", "preprocess")
                self.datasets   = []
                self.datasetSources = []
                self.mappingsLUT= {}

                for i in range(len(mappings)):
                    mapping = mappings[i]
                    mappingInfo = mapping.split('=')

                    lhs = mappingInfo[0].strip()
                    rhs = mappingInfo[1].strip()
                    # self.debug("Merging %s = %s" % (lhs, rhs))
                    self.mappingsLUT[lhs] = i
                    self.datasets.append(lhs)
                    self.datasetSources.append([x.strip() for x in rhs.split('|')])

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def initDatasets(self, dr, datasets):
            try:
                self.debug("BasicMergeData::initDatasets")

                ds = Framework.Dataset(self.datasetName)

                for name in self.datasets:
                    ds.add(Framework.DataValue(name, Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite))

                datasets.add(ds)

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def preDataBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())
                raise e

        def build(self, dr, dci, frame):
            try:
                index = self.mappingsLUT[dci._def.name]
                dsSources = self.datasetSources[index]

                assert dsSources, 'Invalid sources for data: %s' % (dci._def.name)

                dataPtr = None

                for dsSource in dsSources:
                    data = dr.stringData(None, dsSource, noCacheUpdate = True)
                    numSecurities = data.cols()

                    if dataPtr is None:
                        dataPtr = Framework.StringData(None, numSecurities, dci._def)
                        for ii in range(numSecurities):
                            dataPtr.setValue(0, ii, data.getValue(0, ii))

                        continue

                    assert numSecurities == dataPtr.cols(), 'Column mismatch. Expecting: %d - Found: %d' % (dataPtr.cols(), numSecurities)

                    for ii in range(numSecurities):
                        if not dataPtr.getValue(0, ii):
                            srcValue = data.getValue(0, ii)
                            if srcValue:
                                dataPtr.setValue(0, ii, srcValue)

                self.debug("[%d] Merged %s => %s" % (numSecurities, dci._def.name, '|'.join(dsSources)))
                
                frame.setData(PyUtil.script_sptr(dataPtr))
                frame.dataOffset = 0
                frame.dataSize = dataPtr.dataSize()

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def postBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())
                raise e

except Exception as e:
    print(traceback.format_exc())
    raise e
