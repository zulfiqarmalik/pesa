### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import traceback
import datetime as DT

import PyUtil
import sys
from collections import namedtuple, OrderedDict
import csv
import requests
import json
from enum import Enum

try:
    import Framework
    from BasicData import BasicData

    class TryType(Enum):
        kCusip = 1
        kRelatedTicker = 2
        kTicker = 3
   
    SecurityInfo = namedtuple('SecurityInfo', 'ticker, name, cusip, famaIndustry, currency, sector, industry, lastUpdated, priorTickers, tickerChangeDate, relatedTickers, exchange, sIC, permaTicker, location, delistedFrom, isForeign, firstAdded')
    ExchangeSecInfo = namedtuple('ExchangeSecInfo', 'CsiNumber, Symbol, Name, Exchange, IsActive, StartDate, EndDate, ConversionFactor, SwitchCfDate, PreSwitchCf, SubExchange, ExchangeSymbol')

    class BasicPreprocessTickers(BasicData):
        def __init__(self, baseObject):
            try:
                super().__init__(baseObject, "EXCH_HOLIDAY_CALENDAR")
                self.debug("BasicPreprocessTickers::__init__")

                self.loadConfig()

                # Get the holidays file
                self.tickersFilename    = self.config().getResolvedRequiredString("tickersFilename")
                self.tickersDelimiter   = self.config().getRequired("tickersDelimiter")
                self.datasetName        = self.config().getRequired("datasetName")

                # Parse the exchanges ...
                exchanges               = self.config().getRequired("exchanges")
                exchanges               = self.config().config().defs().resolve(exchanges, 0) 
                self.countries, self.exchanges = PyUtil.parseExchanges(exchanges)
                assert self.exchanges and len(self.exchanges), "Must specify which exchanges to load the calendar for!"

                self.maxRequests        = self.config().getInt("maxRequests", 100)
                self.apiKey             = self.config().getRequired("figiServerApiKey")
                self.apiUrl             = self.config().getRequired("figiServerUrl")
                self.securitiesLookup   = {}
                self.securities         = []
                self.tickers            = []
                self.tickersExchInfo    = []
                self.secMoreInfo        = []
                self.secExchInfo        = []

                self.exchangeTickers    = {}

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def initDatasets(self, dr, datasets):
            try:
                self.debug("BasicPreprocessTickers::initDatasets")

                ds = Framework.Dataset(self.datasetName)

                self.basicData = OrderedDict([
                    ("quandl_ticker", "ticker"),
                    ("quandl_name", "name"),
                    ("quandl_cusip", "cusip"),
                    ("quandl_famaIndustry", "famaIndustry"),
                    ("quandl_currency", "currency"),
                    ("quandl_sector", "sector"),
                    ("quandl_industry", "industry"),
                    ("quandl_lastUpdated", "lastUpdated"),
                    ("quandl_priorTickers", "priorTickers"),
                    ("quandl_tickerChangeDate", "tickerChangeDate"),
                    ("quandl_relatedTickers", "relatedTickers"),
                    ("quandl_exchange", "exchange"),
                    ("quandl_sIC", "sIC"),
                    ("quandl_permaTicker", "permaTicker"),
                    ("quandl_location", "location"),
                    ("quandl_delistedFrom", "delistedFrom"),
                    ("quandl_isForeign", "isForeign"),
                    ("quandl_firstAdded", "firstAdded")
                ])

                self.moreInfoData = OrderedDict([
                    ("openfigi_figi", "figi"),
                    ("openfigi_name", "name"),
                    ("openfigi_ticker", "ticker"),
                    ("openfigi_exchCode", "exchCode"),
                    ("openfigi_cfigi", "compositeFIGI"),
                    ("openfigi_uniqueId", "uniqueID"),
                    ("openfigi_securityType", "securityType"),
                    ("openfigi_marketSector", "marketSector"),
                    ("openfigi_shareClassFigi", "shareClassFIGI"),
                    ("openfigi_desc", "securityDescription"),
                ])

                self.exchInfoData = OrderedDict([
                    ("csi_CsiNumber", "CsiNumber"),
                    ("csi_Symbol", "Symbol"),
                    ("csi_Name", "Name"),
                    ("csi_Exchange", "Exchange"),
                    ("csi_IsActive", "IsActive"),
                    ("csi_StartDate", "StartDate"),
                    ("csi_EndDate", "EndDate"),
                    ("csi_ConversionFactor", "ConversionFactor"),
                    ("csi_SwitchCfDate", "SwitchCfDate"),
                    ("csi_PreSwitchCf", "PreSwitchCf"),
                    ("csi_SubExchange", "SubExchange")
                ])

                for name in self.basicData:
                    ds.add(Framework.DataValue(name, Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite))

                for name in self.moreInfoData:
                    ds.add(Framework.DataValue(name, Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite))

                for name in self.exchInfoData:
                    ds.add(Framework.DataValue(name, Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite))

                ds.add(Framework.DataValue("parentUuid", Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite))

                datasets.add(ds)

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def preDataBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())
                raise e

        def loadTickersForExchange(self, dr, dci, exchange):
            exchangeTickerFilename  = self.config().getResolvedString(exchange + "Tickers", "")
            exchangeTickerDownloadUrl  = self.config().getResolvedString("download" + exchange, "")

            if not exchangeTickerFilename:
                self.info("No tickers filename given for exchange: %s" % (exchange))
                return

            if exchangeTickerDownloadUrl:
                self.debug("Downloading: %s => %s" % (exchangeTickerDownloadUrl, exchangeTickerFilename))
                if PyUtil.downloadToFile(exchangeTickerDownloadUrl, exchangeTickerFilename):
                    self.debug("Download FINISHED: %s => %s" % (exchangeTickerDownloadUrl, exchangeTickerFilename))

            self.debug("Loading exchange %s: %s" % (exchange, exchangeTickerFilename))
            exchangeTickerMap = map(ExchangeSecInfo._make, csv.reader(open(exchangeTickerFilename, "rt"), delimiter = ','))

            count                   = 0
            exchangeTickers         = {}

            for exchTickerInfo in exchangeTickerMap:
                count += 1

                # Ignore the header (count > 1) ... 
                if count > 1:
                    exchangeTickers[exchTickerInfo.Symbol] = exchTickerInfo

            self.exchangeTickers[exchange] = exchangeTickers

        def findExchangeTickersInfo(self, tickerInfo):
            if tickerInfo.exchange is not "DELISTED":
                if tickerInfo.exchange in self.exchangeTickers:
                    exchangeTickers = self.exchangeTickers[tickerInfo.exchange]

                    if tickerInfo.ticker in exchangeTickers:
                        return exchangeTickers[tickerInfo.ticker]
    
            # We have a delisted ticker. Find all occurences in all exchanges ...
            else:
                matches = []

                for exchangeTickers in self.exchangeTickers:
                    if tickerInfo.ticker in exchangeTickers:
                        matches.append(exhcangeTickers[tickerInfo.ticker])

                if len(matches) == 1:
                    return matches[0]

                elif len(matches) > 1:
                    bestMatchCountIndex = -1
                    bestMatchCount = 0

                    for i, match in enumerate(matches):
                        matchCount = set(match.Name.lower()).intersection(tickerInfo.name.lower())

                        if matchCount > bestMatchCount:
                            bestMatchCount = matchCount
                            bestMatchCountIndex = i

                    if bestMatchCountIndex >= 0 and bestMatchCountIndex < len(matches):
                        return matches[bestMatchCountIndex]

            return None

        def loadTickersFile(self, dr, dci):
            self.debug("Loading tickers filename: %s" % (self.tickersFilename))

            tickersDownloadUrl  = self.config().getResolvedString("downloadTickers", "")

            if tickersDownloadUrl:
                self.debug("Downloading: %s => %s" % (tickersDownloadUrl, self.tickersFilename))
                if PyUtil.downloadToFile(tickersDownloadUrl, self.tickersFilename):
                    self.debug("Download FINISHED: %s => %s" % (tickersDownloadUrl, self.tickersFilename))

            tickerMap = map(SecurityInfo._make, csv.reader(open(self.tickersFilename, "rt"), delimiter = '\t'))

            self.tickers            = []
            count                   = 0
            self.searchResults      = []
            self.parentIndexes      = []

            # Load individual exchange tickers info
            for exchange in self.exchanges:
                self.loadTickersForExchange(dr, dci, exchange)

            # Now load the ticker map as loaded from the combined tickers filename
            for tickerInfo in tickerMap:
                count += 1

                # Ignore the header (count > 1) ... 
                if count > 1 and tickerInfo.exchange and tickerInfo.exchange in self.exchanges or tickerInfo.exchange == "DELISTED":
                    self.tickers.append(tickerInfo)
                    self.parentIndexes.append(-1)
                    self.searchResults.append(False)

                    # OK find the exchange ticker info here ...
                    exchTickerInfo  = self.findExchangeTickersInfo(tickerInfo)

                    # It's fine if it's None as well
                    self.tickersExchInfo.append(exchTickerInfo)

            self.debug("Total tickers loaded: %d" % (len(self.tickers)))

        def resolveSecurityForFigi(self, dr, dci, numTry):
            self.debug("Resolving FIGI try: %s" % (str(numTry)))

            totalCount  = len(self.tickers)
            numLeft     = totalCount
            startIndex  = 0
            endIndex    = startIndex + min(self.maxRequests, numLeft)

            headers     = {
                'X-OPENFIGI-APIKEY': self.apiKey
            }

            while startIndex < endIndex:
                data = []
                srcDataMoreInfo = []
                secIndexes = []
                dataInfo = []
                exchCode = "US"

                for i in range(startIndex, endIndex):
                    if self.searchResults[i] is False:
                        tickerInfo = self.tickers[i]

                        if numTry == TryType.kCusip:
                            if tickerInfo.cusip:
                                data.append({ "idType": "ID_CUSIP", "idValue": tickerInfo.cusip, "exchCode": exchCode })
                                srcDataMoreInfo.append({ "orgTicker": "" })
                            else:
                                data.append({ "idType": "TICKER", "idValue": tickerInfo.ticker.replace(".", "/"), "exchCode": "US" })
                                srcDataMoreInfo.append({ "orgTicker": tickerInfo.ticker })

                            secIndexes.append(i)

                        elif numTry == TryType.kRelatedTicker:
                            relatedTickers = [] if tickerInfo.relatedTickers == "None" else tickerInfo.relatedTickers.split(',')
                            if len(relatedTickers):
                                self.parentIndexes[i] = i
                                priorTickers = [] if tickerInfo.priorTickers == "None" else tickerInfo.priorTickers.split(',')

                                for relatedTicker in relatedTickers:
                                    if relatedTicker not in priorTickers:
                                        data.append({ "idType": "TICKER", "idValue": relatedTicker.replace(".", "/"), "exchCode": "US" })
                                        srcDataMoreInfo.append({ "orgTicker": relatedTicker })
                                        secIndexes.append(i)
                        else:
                            data.append({ "idType": "TICKER", "idValue": tickerInfo.ticker.replace(".", "/"), "exchCode": "US" })
                            srcDataMoreInfo.append({ "orgTicker": tickerInfo.ticker })
                            secIndexes.append(i)

                self.debug("[%s] - Getting num FIGIs for range: %d - [%d, %d)" % (str(numTry), len(data), startIndex, endIndex))

                numLeft = totalCount - endIndex
                startIndex = endIndex
                endIndex = startIndex + min(self.maxRequests, numLeft)

                if len(data) == 0:
                    continue

                responseData = requests.post(self.apiUrl, json = data, headers = headers)

                # Some error was returned
                if responseData.status_code < 200 or responseData.status_code >= 300:
                    text = responseData.text.replace("{", "{{")
                    text = text.replace("}", "}}")
                    self.error("Server error. Response code: %d - Response Text: %s" % (responseData.status_code, text))
                    return None

                rdataList = json.loads(responseData.text)
                rdataCount = len(rdataList)

                for i in range(rdataCount):
                    rdata = rdataList[i]
                    secIndex = secIndexes[i]
                    tickerInfo = self.tickers[secIndex]
                    tickerExchInfo = self.tickersExchInfo[secIndex]

                    # Some error has occured over here ... ignore this figi?
                    if "error" in rdata:
                        if numTry == TryType.kTicker:
                            # if data[i]["idType"] == "ID_CUSIP":
                            #     self.warning("[%d] Cusip NOT FOUND: %s - Security Info: %s [%s, CUSIP = %s]" % (secIndex, rdata["error"], tickerInfo.ticker, tickerInfo.name, tickerInfo.cusip))
                            # else:
                            self.error("[%d] Unable to resolve security: %s - Security Info: %s [%s, CUSIP = %s]" % (secIndex, rdata["error"], tickerInfo.ticker, tickerInfo.name, tickerInfo.cusip))

                            # OK, since we had error on the last try over here, we're just going to ignore this security for the time being ...
                    else:
                        self.searchResults[secIndex] = True

                        # If more than one entry is returned then we give out a warning and just use the first entry as the 
                        if len(rdata["data"]) > 1:
                            self.info("[%d] More than one securities returned for entry: %s [%s, CUSIP = %s]" % (secIndex, tickerInfo.ticker, tickerInfo.name, tickerInfo.cusip))

                        for moreInfo in rdata["data"]:
                            orgTicker = srcDataMoreInfo[i]["orgTicker"]
    
                            # Don't add it if the ticker still exists
                            if orgTicker and orgTicker in self.securitiesLookup:
                                continue
                                
                            if tickerInfo.cusip in self.securitiesLookup:
                                continue

                            self.securities.append(tickerInfo)
                            self.secMoreInfo.append(moreInfo)
                            self.secExchInfo.append(tickerExchInfo)

                            # self.debug("Ticker: %s - %s" % (tickerInfo.ticker, moreInfo["compositeFIGI"]))

                            lutIndex = len(self.securities) - 1

                            moreInfo["parentIndex"] = self.parentIndexes[secIndex]

                            # if data[i]["idType"] == "TICKER":
                            #     ticker = data[i]["orgTicker"]

                            #     # If we already have this ticker in the securities lookup
                            #     if ticker in self.securitiesLookup:
                            #     # self.securitiesLookup[data[i]["idValue"]] = lutIndex

                            self.securitiesLookup[orgTicker] = lutIndex
                            self.securitiesLookup[tickerInfo.cusip] = lutIndex
                            self.securitiesLookup[moreInfo["ticker"]] = lutIndex

            self.debug("[%s] Total securities loaded: %d" % (str(numTry), len(self.securities)))

        def loadFigis(self, dr, dci):
            self.securitiesLookup   = {}
            self.securities         = []

            self.resolveSecurityForFigi(dr, dci, TryType.kCusip)
            self.resolveSecurityForFigi(dr, dci, TryType.kRelatedTicker)
            self.resolveSecurityForFigi(dr, dci, TryType.kTicker)

            self.debug("Total securities loaded: %d" % (len(self.securities)))

        def buildSecurities(self, dr, dci):
            # We have already built the data ... we just use it
            if len(self.securities) and len(self.securities) == len(self.secMoreInfo):
                return

            # Otherwise we build the complete dataset ...

            # First of all we load the tickers from the file ...
            self.loadTickersFile(dr, dci)

            # Then we load the figis from OpenFigi
            self.loadFigis(dr, dci)

        def buildGenericTickerDataList(self, dr, dci, frame, name):
            self.buildSecurities(dr, dci)

            numSecurities = len(self.securities)
            dataPtr = Framework.StringData(dci.universe, numSecurities, dci._def)

            for i in range(numSecurities):
                tickerInfo = self.securities[i]
                value = getattr(tickerInfo, name)
                if value == "None":
                    value = ""
                dataPtr.setValue(0, i, value)

            frame.setData(PyUtil.script_sptr(dataPtr))
            frame.dataOffset = 0
            frame.dataSize = dataPtr.dataSize()

        def buildGenericMoreInfo(self, dr, dci, frame, name):
            self.buildSecurities(dr, dci)

            numSecurities = len(self.securities)
            dataPtr = Framework.StringData(dci.universe, numSecurities, dci._def)

            for i in range(numSecurities):
                moreInfo = self.secMoreInfo[i]
                value = moreInfo[name]
                if value == "None" or value == None:
                    value = ""
                dataPtr.setValue(0, i, value)

            frame.setData(PyUtil.script_sptr(dataPtr))
            frame.dataOffset = 0
            frame.dataSize = dataPtr.dataSize()

        def buildGenericExchInfo(self, dr, dci, frame, name):
            self.buildSecurities(dr, dci)

            numSecurities = len(self.securities)
            dataPtr = Framework.StringData(dci.universe, numSecurities, dci._def)

            for i in range(numSecurities):
                exchInfo = self.secExchInfo[i]

                value = ""

                if exchInfo is not None:
                    value = getattr(exchInfo, name)
                    if value == "None" or value == None:
                        value = ""

                dataPtr.setValue(0, i, value)

            frame.setData(PyUtil.script_sptr(dataPtr))
            frame.dataOffset = 0
            frame.dataSize = dataPtr.dataSize()

        def buildParentUuid(self, dr, dci, frame):
            self.buildSecurities(dr, dci)

            numSecurities = len(self.securities)
            parentUuids = []
            dataPtr = Framework.StringData(dci.universe, numSecurities, dci._def)

            for i in range(numSecurities):
                parentIndex = self.secMoreInfo[i]["parentIndex"]
                parentUuid = ""

                if parentIndex >= 0:
                    tickerInfo = self.tickers[parentIndex]
                    mappedParentIndex = -1

                    if tickerInfo.cusip in self.securitiesLookup:
                        mappedParentIndex = self.securitiesLookup[tickerInfo.cusip]
                    elif tickerInfo.ticker in self.securitiesLookup:
                        mappedParentIndex = self.securitiesLookup[tickerInfo.ticker]

                    if mappedParentIndex >= 0:
                        parentUuid = self.secMoreInfo[mappedParentIndex]["compositeFIGI"]

                dataPtr.setValue(0, i, parentUuid)

            frame.setData(PyUtil.script_sptr(dataPtr))
            frame.dataOffset = 0
            frame.dataSize = dataPtr.dataSize()

        def build(self, dr, dci, frame):
            try:
                dataName = dci._def.name
                self.debug("BasicPreprocessTickers::build: %s.%s" % (self.datasetName, dataName))

                # 'ticker, name, cusip, famaIndustry, currency, sector, industry, lastUpdated, priorTickers, tickerChangeDate, relatedTickers, exchange, sIC, permaTicker, location, delistedFrom, isForeign, firstAdded'
                if dataName in self.basicData:
                    return self.buildGenericTickerDataList(dr, dci, frame, self.basicData[dataName])
                elif dataName in self.moreInfoData:
                    return self.buildGenericMoreInfo(dr, dci, frame, self.moreInfoData[dataName])
                elif dataName in self.exchInfoData:
                    return self.buildGenericExchInfo(dr, dci, frame, self.exchInfoData[dataName])
                elif dataName == "parentUuid":
                    return self.buildParentUuid(dr, dci, frame)
                else:
                    assert False, "Unknown/Unsupported dataset: %s" % (dataName)

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def postBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())
                raise e

except Exception as e:
    print(traceback.format_exc())
    raise e
