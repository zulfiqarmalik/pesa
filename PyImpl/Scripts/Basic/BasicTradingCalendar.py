### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import traceback
import datetime as DT
import os

import PyUtil
import sys

try:
    import Framework
    from BasicData import BasicData

    class BasicTradingCalendar(BasicData):
        def __init__(self, baseObject):
            try:
                super().__init__(baseObject, "EXCH_HOLIDAY_CALENDAR")
                self.debug("BasicTradingCalendar::__init__")

                self.loadConfig()

                # Get the holidays file
                self.holidaysFile   = self.config().getRequired("holidays")
                self.holidaysFile   = self.config().config().defs().resolve(self.holidaysFile, 0)

                # Misc. other settings ... mostly legacy
                self.useMICCode     = self.config().getBool("useMICCode", False)
                self.perExchangeData= self.config().getBool("perExchangeData", False)

                # Parse the exchanges ...
                exchanges           = self.config().getRequired("exchanges")
                exchanges           = self.config().config().defs().resolve(exchanges, 0) 
                self.countries, self.exchanges = PyUtil.parseExchanges(exchanges)
                assert self.exchanges and len(self.exchanges), "Must specify which exchanges to load the calendar for!"

                self.timesKeyword   = self.config().get("timesKeyword")
                self.timesExchanges = self.config().get("timesExchanges")

                # Constructing an exchange map ...
                self.mapExchanges   = {}
                self.mapExchangesStr= self.config().getString("mapExchanges", "")
                if self.mapExchangesStr:
                    exchanges = self.mapExchangesStr.split("|")
                    for exchange in exchanges:
                        mapFrom, mapTo = exchange.split("=")
                        self.mapExchanges[mapFrom.strip()] = mapTo.strip()

                self.exchangeHolidays = {}

                self.allHolidays    = []
                self.holidaysBuilt  = False

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def initDatasets(self, dr, datasets):
            try:
                self.debug("BasicTradingCalendar::initDatasets")

                ds = Framework.Dataset("calendar")

                if self.perExchangeData is True:
                    for i in range(0, len(self.exchanges)):
                        country = self.countries[i]
                        exchange = self.exchanges[i]

                        ds.add(Framework.DataValue("holidays_" + exchange, Framework.DataType.kInt, 4, Framework.DataFrequency.kStatic, 
                            Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite))
                        # ds.add(Framework.DataValue("holidayDesc_" + exchangeId, Framework.DataType.kInt, 4, Framework.DataFrequency.kStatic, 
                        #     Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite))
                else:
                    ds.add(Framework.DataValue("holidays", Framework.DataType.kInt, 4, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo))
                    # ds.add(Framework.DataValue("holidayDesc", Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo))

                datasets.add(ds)

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def preDataBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())
                raise e

        def updateHolidaysFromFile(self, dr, dci, holidaysFile):
            newHolidays = []
            holidayLookup = {}

            with open(holidaysFile, "r") as fileHandle:
                lines = fileHandle.readlines()

                for line in lines:
                    holidayDate = line.strip()

                    if holidayDate:
                        idate = int(holidayDate)

                        newHolidays.append(idate)
                        self.debug("New holiday: %d [%s]" % (idate, holidaysFile))
                        holidayLookup[idate] = True

            return newHolidays

        def updateHolidays(self, dr, dci, exchange):
            if self.holidaysBuilt:
                return

            holidaysFile = self.holidaysFile

            if exchange is not None:
                holidaysFile = os.path.join(self.holidaysFile, exchange + "_holidays.txt")
                self.debug("Updating holidays for exchange: %s" % (exchange))
            else:
                self.debug("Updating holidays globally!")

            return self.updateHolidaysFromFile(dr, dci, holidaysFile)

        def buildHolidays(self, dr, dci, exchange, frame):
            newHolidays = self.updateHolidays(dr, dci, exchange)
            numNewHolidays = len(newHolidays)

            if numNewHolidays > 0:
                self.debug("Number of new holidays: %d" % (numNewHolidays))
                dataPtr = Framework.IntMatrixData(dci.universe, dci._def, 1, numNewHolidays, False)

                for i in range(numNewHolidays):
                    dataPtr.setValue(0, i, newHolidays[i])

                frame.setData(PyUtil.script_sptr(dataPtr))
                frame.dataOffset = 0
                frame.dataSize = dataPtr.dataSize()
            else:
                frame.noDataUpdate = True

        def postBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())
                raise e

        def build(self, dr, dci, frame):
            try:
                dataName = dci._def.name
                self.debug("BasicTradingCalendar::build: %s" % (dataName))

                if dataName == "holidays":
                    return self.buildHolidays(dr, dci, None, frame)
                else:
                    exchange = dataName[len("holidays_"):].lower()
                    return self.buildHolidays(dr, dci, exchange, frame)
                # elif dataName == "holidayDesc":
                #     return self.buildHolidayDesc(dr, dci, frame)

            except Exception as e:
                print(traceback.format_exc())
                raise e

except Exception as e:
    print(traceback.format_exc())
    raise e
