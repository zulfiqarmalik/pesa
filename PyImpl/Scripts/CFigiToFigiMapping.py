### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import traceback
import datetime as DT

import PyUtil
import sys

import numpy as np
from pandas import DataFrame

import requests
import json

try:
    import Framework as FW
    from PyBase import PyDataLoader

    class CFigiToFigiMapping(PyDataLoader):
        def __init__(self, baseObject):
            super().__init__(baseObject, "CFigiToFigiMapping")

        def initDatasets(self, dr, datasets):
            try:
                self.datasetName = self.config().getRequired("datasetName")
                self.serverUrl = self.config().getRequired("serverUrl")

                ds = FW.Dataset(self.datasetName)
                ds.add(FW.DataValue("figis", FW.DataType.kString, 0, FW.DataFrequency.kStatic, FW.DataFlags.kUpdatesInOneGo | FW.DataFlags.kAlwaysOverwrite))
                ds.add(FW.DataValue("bamTickers", FW.DataType.kString, 0, FW.DataFrequency.kStatic, FW.DataFlags.kUpdatesInOneGo | FW.DataFlags.kAlwaysOverwrite))

                self.cfigis = None
                self.figis = None
                self.bamTickers = None

                datasets.add(ds)

            except Exception as e:
                print(traceback.format_exc())
                raise e


        def buildFigis(self, dr, dci, frame):
            secMaster = dr.getSecurityMaster()
            numSecurities = secMaster.size(dci.rt.diEnd)
            dataPtr = FW.StringData(dci.universe, numSecurities, dci._def)
            figis = self.figis

            for ii in range(numSecurities):
                if figis[ii]:
                    dataPtr.setValue(0, ii, figis[ii])
                else:
                    sec = secMaster.security(0, ii)
                    self.error("Unable to map cfigi: %s - %s - %s" % (self.cfigis[ii], sec.bbTickerStr(), sec.nameStr()))

            frame.setData(BamUtil.script_sptr(dataPtr))
            frame.dataOffset = 0
            frame.dataSize = dataPtr.dataSize()

        def buildBAMTickers(self, dr, dci, frame):
            secMaster = dr.getSecurityMaster()
            numSecurities = secMaster.size(dci.rt.diEnd)
            dataPtr = FW.StringData(dci.universe, numSecurities, dci._def)
            bamTickers = self.bamTickers

            for ii in range(numSecurities):
                if bamTickers[ii]:
                    dataPtr.setValue(0, ii, bamTickers[ii])

            frame.setData(BamUtil.script_sptr(dataPtr))
            frame.dataOffset = 0
            frame.dataSize = dataPtr.dataSize()

        def build(self, dr, dci, frame):
            dataName = dci._def.name

            self.buildData(dr, dci, frame)

            if dataName == "figis":
                return self.buildFigis(dr, dci, frame)
            elif dataName == "bamTickers":
                return self.buildBAMTickers(dr, dci, frame)

        def buildData(self, dr, dci, frame):
            try:
                if self.figis is not None:
                    return

                cfigis = []
                figis = []
                bamTickers = []
                cfigiIndexes = {}

                secMaster = dr.getSecurityMaster()
                numSecurities = secMaster.size(dci.rt.diEnd)
                maxBatch = numSecurities
                startIndex = 0
                endIndex = startIndex + maxBatch

                while startIndex < numSecurities:
                    batchFigis = []

                    self.debug("Batch: [%d, %d]" % (startIndex, endIndex))

                    for ii in range(startIndex, endIndex):
                        sec = secMaster.security(0, ii)
                        cfigi = sec.uuidStr()

                        cfigis.append(cfigi)
                        cfigiIndexes[cfigi] = len(cfigis) - 1
                        figis.append(None)
                        bamTickers.append(None)
                        batchFigis.append(cfigi)

                    self.debug("Batch CFIGI Count: %d" % (len(batchFigis)))

                    jsonStr = json.dumps(batchFigis)
                    headers = { "Content-Type": "application/json", "Content-Length": str(len(jsonStr)) }

                    responseData = requests.post(self.serverUrl, data = jsonStr, headers = headers)

                    if responseData.status_code < 200 or responseData.status_code >= 300:
                        text = responseData.text.replace("{", "{{")
                        text = text.replace("}", "}}")
                        self.error("Server error. Response code: %d - Response Text: %s" % (responseData.status_code, text))
                        self.error("Source JSON: %s" % (jsonStr))
                        return None

                    results = json.loads(responseData.text)

                    for result in results:
                        cfigi               = result["Cfigi"]
                        figi                = result["Figi"]
                        bamTicker           = result["Ticker"]

                        index               = cfigiIndexes[cfigi]
                        figis[index]        = figi
                        bamTickers[index]   = bamTicker

                    startIndex = endIndex
                    endIndex = min(startIndex + maxBatch, numSecurities)

                self.figis = figis
                self.cfigis = cfigis
                self.bamTickers = bamTickers


            except Exception as e:
                print(traceback.format_exc())

        def preDataBuild(self, dr, dci):
            pass

        def postDataBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())

        def preBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())

        def postBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())

except Exception as e:
    print(traceback.format_exc())
    raise e
