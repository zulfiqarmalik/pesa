### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import traceback
import datetime as DT

import PyUtil
import sys
from collections import namedtuple, OrderedDict
import csv
import requests
import json
from enum import Enum
import os
import math

sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'Basic'))

try:
    import Framework
    from BasicData import BasicData

    class MS_DailyListings(BasicData):
        def __init__(self, baseObject):
            try:
                super().__init__(baseObject, "MS_DailyListings")
                self.debug("MS_DailyListings::__init__")

                self.loadConfig()

                # Get the holidays file
                exchanges               = self.config().getResolvedRequiredString("exchanges")
                self.datasetName        = self.config().getRequired("datasetName")

                self.countries, self.exchanges, self.exchangeIds = PyUtil.parseExchangeIds(exchanges)
                assert self.exchanges and len(self.exchanges), "Must specify which exchanges to load the calendar for!"

                self.exchangeTickers    = {}

                self.listingType        = self.config().getString("listingType", "")

                self.downloadDir        = self.config().getResolvedRequiredString("downloadDir")
                self.data               = {}
                self.instruments        = []
                self.instrumentLookup   = {}

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def initDatasets(self, dr, datasets):
            try:
                self.debug("MS_DailyListings::initDatasets")

                ds = Framework.Dataset(self.datasetName)

                ds.flushDataset = True

                ds.add(Framework.DataValue("instrumentLUT", Framework.DataType.kInt, 0, Framework.DataFrequency.kDaily, 0))
                ds.add(Framework.DataValue("ms_exch_openPrice", Framework.DataType.kFloat, 4, Framework.DataFrequency.kDaily, 0))
                ds.add(Framework.DataValue("ms_exch_closePrice", Framework.DataType.kFloat, 4, Framework.DataFrequency.kDaily, 0))
                ds.add(Framework.DataValue("ms_exch_highPrice", Framework.DataType.kFloat, 4, Framework.DataFrequency.kDaily, 0))
                ds.add(Framework.DataValue("ms_exch_lowPrice", Framework.DataType.kFloat, 4, Framework.DataFrequency.kDaily, 0))
                ds.add(Framework.DataValue("ms_exch_volume", Framework.DataType.kFloat, 4, Framework.DataFrequency.kDaily, 0))
                ds.add(Framework.DataValue("ms_exch_vwap", Framework.DataType.kFloat, 4, Framework.DataFrequency.kDaily, 0))

                datasets.add(ds)

                ds2 = Framework.Dataset(self.datasetName)
                ds2.add(Framework.DataValue("ms_instrument", Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite)) 
                # ds2.add(Framework.DataValue("inst_ticker", Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite)) 
                datasets.add(ds2)

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def addInstrument(self, instrument):
            if instrument not in self.instrumentLookup:
                self.instrumentLookup[instrument] = len(self.instruments)
                self.instruments.append(instrument)

        def preDataBuild(self, dr, dci):
            try:
                if dci.rt.appOptions.updateDataOnly is False:
                    return 

                self.debug("MS_SecInfo::preDataBuild - START")

                instruments = dr.stringData(None, self.datasetName + ".ms_instrument", noCacheUpdate = True)

                if not instruments:
                    return
                    
                self.numSecuritiesParsed = instruments.cols()
                self.debug("Existing number of securities parsed: %d" % (self.numSecuritiesParsed))

                for ii in range(instruments.cols()):
                    instrument = instruments.getValue(0, ii)
                    self.addInstrument(instrument)

                self.debug("MS_SecInfo::preDataBuild - DONE!")

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def getFilename(self, exchangeIndex, date):
            country = self.countries[exchangeIndex]
            exchange = self.exchanges[exchangeIndex]
            exchangeId = self.exchangeIds[exchangeIndex]

            sdate = PyUtil.dateToStr(date)
            return os.path.join(self.downloadDir, country + "_" + exchange + "-" + exchangeId, sdate + '.json')

        def loadHistoricalFile(self, exchangeIndex, date):
            filename = self.getFilename(exchangeIndex, date)
            zipFilename = filename + '.zip'

            if os.path.isfile(zipFilename):
                self.debug("Loading: %s" % (zipFilename))
                import zipfile
                with zipfile.ZipFile(zipFilename, 'r') as zipHandle:
                    name = os.path.basename(filename)
                    fileContents = zipHandle.read(name,) 
                    data = json.loads(fileContents.decode("utf-8"))
                    return data

            self.debug("Historical file not found: %s" % (filename))
            return None

        def downloadFile(self, exchangeIndex, date):
            idate = PyUtil.dateToInt(date)
            exchangeId = self.exchangeIds[exchangeIndex]
            downloadUrl = self.config().getResolvedRequiredString("downloadUrl", idate) + "&exchange=" + str(exchangeId)
            filename = self.getFilename(exchangeIndex, date)

            self.debug("Downloading file: %s => %s" % (filename, downloadUrl))

            responseData = requests.get(downloadUrl)

            if responseData.status_code != 200:
                print("Server error. Response code: %d - Response Text: %s" % (responseData.status_code, text))
                sys.exit(1)

            data = responseData.text
            
            with open(filename, 'w') as file:
                file.write(responseData.text)

            zipFilename = filename + '.zip'
            self.debug("File downloaded: %s => %s" % (filename, zipFilename))
            PyUtil.zipFile(filename, zipFilename)
            os.remove(filename)

            return json.loads(responseData.text)

        def loadData(self, exchangeIndex, date, forceDownload = False):
            data = None

            if not forceDownload:
                data = self.loadHistoricalFile(exchangeIndex, date)

            if forceDownload or data is None:
                data = self.downloadFile(exchangeIndex, date)

            return data["ts"]["results"]

        def buildDataForExchange(self, dr, dci, exchangeIndex, date):
            # Force download of the last 2 days worth of data 
            forceDownload = dci.di >= dci.rt.diEnd and dci.rt.appOptions.noDownload is False
            securities = self.loadData(exchangeIndex, date, forceDownload)
            exchangeId = self.exchangeIds[exchangeIndex]

            data = []
            # assert len(securities), "[%d] Unable to get securities for exchange filename: %s" % (PyUtil.dateToInt(date), self.getFilename(exchangeIndex, date))
            for sec in securities:
                if len(sec["data"]) and int(sec["type"]) == 1:
                    instrument = str(exchangeId) + "." + str(sec["type"]) + "." + sec["symbol"]

                    secData = {}

                    secData["ms_exch_closePrice"] = float(sec["data"][0]["D2"])
                    secData["ms_exch_volume"] = float(sec["data"][0]["D16"])
                    secData["ms_exch_openPrice"] = float(sec["data"][0]["D17"])
                    secData["ms_exch_highPrice"] = float(sec["data"][0]["D18"])
                    secData["ms_exch_lowPrice"] = float(sec["data"][0]["D19"])
                    secData["ms_exch_vwap"] = float(sec["data"][0]["D243"]) if "D243" in sec["data"][0] else math.nan

                    # Don't want to include stocks that are too small (minimum 100k volume)
                    if sec["symbol"]:
                        self.addInstrument(instrument)
                        secData["instrumentLUT"] = self.instrumentLookup[instrument]
                        data.append(secData)

            return data

        def buildData(self, dr, dci):
            idate = dci.rt.getDate(dci.di)
            sdate = str(idate)
            date = PyUtil.intToDate(idate)

            # Last day's data is always empty
            if dci.di == dci.rt.diEnd:
                return []

            if sdate in self.data:
                return self.data[sdate]

            # Clear the cache ... we don't need the old data anymore!
            self.data = {}
            self.data[sdate] = []
            for exchangeIndex in range(len(self.exchanges)):
                exchangeData = self.buildDataForExchange(dr, dci, exchangeIndex, date)
                self.data[sdate] = self.data[sdate] + exchangeData

            return self.data[sdate]

        # def buildStringData(self, dr, dci, frame, securities, dataName):
        #     numSecurities = len(securities)

        #     if numSecurities == 0:
        #         frame.noDataUpdate = True
        #         return

        #     dataPtr = Framework.StringData(dci.universe, numSecurities, dci._def)

        #     for i in range(numSecurities):
        #         security = securities[i]
        #         dataPtr.setValue(0, i, security[dataName])

        #     frame.setData(PyUtil.script_sptr(dataPtr))
        #     frame.dataOffset = 0
        #     frame.dataSize = dataPtr.dataSize()

        #     self.debug("[%d]-[%s] Number of securities: %d" % (dci.rt.getDate(dci.di), dataName, numSecurities))

        def buildInstruments(self, dr, dci, frame, dataName):
            numInstruments = len(self.instruments)
            dataPtr = Framework.StringData(dci.universe, numInstruments, dci._def)

            for i in range(numInstruments):
                dataPtr.setValue(0, i, self.instruments[i])

            frame.setData(PyUtil.script_sptr(dataPtr))
            frame.dataOffset = 0
            frame.dataSize = dataPtr.dataSize()

            self.info("[%d]-[%s] Total Unique Instrument Count: %d" % (dci.rt.getDate(dci.di), dci._def.name, numInstruments))

        def buildTickers(self, dr, dci, frame, dataName):
            numTickers = len(self.instruments)
            dataPtr = Framework.StringData(dci.universe, numTickers, dci._def)

            for i in range(numTickers):
                instrument = self.instruments[i]
                parts = instrument.split('.')
                assert len(parts) == 3, "Unexpected instrument id: %s" % (instrument)
                ticker = parts[2]
                dataPtr.setValue(0, i, parts[2])

            frame.setData(PyUtil.script_sptr(dataPtr))
            frame.dataOffset = 0
            frame.dataSize = dataPtr.dataSize()

            self.info("[%d]-[%s] Total Unique Instrument Count: %d" % (dci.rt.getDate(dci.di), dci._def.name, numTickers))

        def buildMatrixData(self, dr, dci, frame, securities, dataName, MatrixType):
            numSecurities = len(securities)
            numInstruments = len(self.instruments)
            dataPtr = MatrixType(dci.universe, dci._def, 1, numInstruments, False)

            if numSecurities == 0:
                frame.noDataUpdate = True
                return

            dataPtr.invalidateMemory()
                
            for i in range(numSecurities):
                security = securities[i]
                ii = security["instrumentLUT"]
                dataPtr.setValue(0, ii, security[dataName])

            frame.setData(PyUtil.script_sptr(dataPtr))
            frame.dataOffset = 0
            frame.dataSize = dataPtr.dataSize()

            self.debug("[%d]-[%s] Number of securities: %d" % (dci.rt.getDate(dci.di), dataName, numSecurities))

        def build(self, dr, dci, frame):
            try:
                dataName = dci._def.name
                # self.debug("MS_DailyListings::build: %s.%s" % (self.datasetName, dataName))

                # 'ticker, name, cusip, famaIndustry, currency, sector, industry, lastUpdated, priorTickers, tickerChangeDate, relatedTickers, exchange, sIC, permaTicker, location, delistedFrom, isForeign, firstAdded'
                if dataName == "ms_instrument":
                    return self.buildInstruments(dr, dci, frame, dataName)
                elif dataName == "ms_ticker":
                    return self.buildTickers(dr, dci, frame, dataName)
                else:
                    securities = self.buildData(dr, dci)
                    if dataName == "instrumentLUT":
                        return self.buildMatrixData(dr, dci, frame, securities, dataName, Framework.IntMatrixData)
                    else:
                        return self.buildMatrixData(dr, dci, frame, securities, dataName, Framework.FloatMatrixData)

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def postBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())
                raise e

except Exception as e:
    print(traceback.format_exc())
    raise e
