### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import traceback
import datetime as DT

import PyUtil
import sys

import requests
import json

try:
    import Framework
    from BasicData import BasicData

    class MS_SecInfo(BasicData):
        def __init__(self, baseObject):
            try:
                super().__init__(baseObject, "MS_SecInfo")
                self.debug("MS_SecInfo::__init__")

                self.loadConfig()

                self.dataCache      = None
                self.totalCount     = 0
                self.datasetName    = self.config().getString("datasetName", "prepropcess")

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def initDatasets(self, dr, datasets):
            try:

                self.debug("MS_SecInfo::initDatasets")
                ds = Framework.Dataset(self.datasetName)

                # These are built from the initially cached preprocess.instrument data ...
                ds.add(Framework.DataValue("ms_ticker", Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite)) 
                ds.add(Framework.DataValue("ms_exchangeId", Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite)) 
                ds.add(Framework.DataValue("ms_type", Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite)) 

                # These are fetched directly from the MorningStar web service
                ds.add(Framework.DataValue("ms_name", Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite))
                ds.add(Framework.DataValue("ms_desc", Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite))
                ds.add(Framework.DataValue("ms_isin", Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite))
                ds.add(Framework.DataValue("ms_currency", Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite))
                ds.add(Framework.DataValue("ms_country", Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite))
                ds.add(Framework.DataValue("ms_parentISIN", Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite))
                ds.add(Framework.DataValue("ms_figiShareClass", Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite))
                ds.add(Framework.DataValue("ms_figi", Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite))
                ds.add(Framework.DataValue("ms_cfigi", Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite))
                ds.add(Framework.DataValue("ms_industry", Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite))
                ds.add(Framework.DataValue("ms_sector", Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite))
                ds.add(Framework.DataValue("ms_group", Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite))
                ds.add(Framework.DataValue("ms_marketInstrumentType", Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite))
                ds.add(Framework.DataValue("ms_marketSpecificFlag", Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite))
                ds.add(Framework.DataValue("ms_sedol", Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite))

                datasets.add(ds)

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def preDataBuild(self, dr, dci):
            try:
                self.debug("MS_SecInfo::preDataBuild: %s" % (dci._def.name))
            except Exception as e:
                print(traceback.format_exc())
                raise e

        def postBuild(self, dr, dci):
            try:
                self.debug("MS_SecInfo::postBuild: %s" % (dci._def.name))
            except Exception as e:
                print(traceback.format_exc())
                raise e

        def constructInstrumentStr(self, instruments, startIndex, endIndex):
            instrumentsStr = ""
            instrumentsMap = {}

            for ii in range(startIndex, endIndex):
                instrument = instruments.getValue(0, ii)
                instrumentsStr += instrument + ","
                instrumentsMap[instrument] = ii

            # remove the trailing comma (,) at the end of the string
            return instrumentsStr[:-1], instrumentsMap

        def getDataValue(self, rdata, dataId):
            return rdata[dataId] if rdata and dataId in rdata else ""

        def buildData(self, dr, dci):
            if self.dataCache:
                return self.dataCache

            instruments     = dr.stringData(None, "preprocess.instrument", noCacheUpdate = True)
            numSecurities   = instruments.cols()
            maxRequests     = self.config().getInt("maxRequests", 100)
            startIndex      = 0
            endIndex        = min(startIndex + maxRequests, numSecurities)
            data            = []
            downloadUrl     = self.config().getResolvedRequiredString("downloadUrl")
            numRetry        = 0
            maxRetry        = self.config().getInt("maxRetry", 3)

            self.debug("Base URL: %s" % (downloadUrl))

            while startIndex < endIndex:
                instrumentsStr, instrumentsMap = self.constructInstrumentStr(instruments, startIndex, endIndex)
                url         = downloadUrl + "&instrument=" + instrumentsStr

                try:
                    resp    = requests.get(url)
                except Exception as e:
                    numRetry+= 1
                    if numRetry >= maxRetry:
                        print("Request failed max retry exceeded: %d" % (numRetry))
                        sys.exit(1)
                    else:
                        import time
                        time.sleep(1)

                numRetry    = 0
                if resp.status_code != 200:
                    print("Server error. Response code: %d - Response Text: %s" % (resp.status_code, text))
                    sys.exit(1)

                rdata       = json.loads(resp.text)["quotes"]["results"]
                index       = 0

                self.debug("Downloaded data for range: [%d, %d). Num Results: %d" % (startIndex, endIndex, len(rdata)))

                results     = {}
                for rd in rdata:
                    instrumentId                    = str.format("%s.%s.%s" % (rd["H2"], rd["H3"], rd["H1"]))
                    results[instrumentId]           = rd 

                for ii in range(startIndex, endIndex):
                    index += 1
                    instrument                      = instruments.getValue(0, ii)
                    instrumentResult                = results[instrument] if instrument in results else None

                    data.append({
                        "instrumentId":             instrument,
                        "ms_name":                  self.getDataValue(instrumentResult, "S32"),
                        "ms_desc":                  self.getDataValue(instrumentResult, "S32"),
                        "ms_isin":                  self.getDataValue(instrumentResult, "S19"),
                        "ms_marketInstrumentType":  self.getDataValue(instrumentResult, "S20"),
                        "ms_marketSpecificFlag":    self.getDataValue(instrumentResult, "S35"),
                        "ms_currency":              self.getDataValue(instrumentResult, "D204"),
                        "ms_country":               self.getDataValue(instrumentResult, "S34"),
                        "ms_parentISIN":            self.getDataValue(instrumentResult, "D1582"),
                        "ms_figiShareClass":        self.getDataValue(instrumentResult, "S1407"),
                        "ms_figi":                  self.getDataValue(instrumentResult, "D2995"),
                        "ms_cfigi":                 self.getDataValue(instrumentResult, "S1405"),
                        "ms_industry":              self.getDataValue(instrumentResult, "S1041"),
                        "ms_group":                 self.getDataValue(instrumentResult, "S1042"),
                        "ms_sector":                self.getDataValue(instrumentResult, "S1043"),
                        "ms_sedol":                 self.getDataValue(instrumentResult, "S1013")
                    })

                startIndex = endIndex
                endIndex = min(startIndex + maxRequests, numSecurities)

            self.dataCache = data

            return self.dataCache

        def buildSubInstrument(self, dr, dci, frame, index):
            instruments = dr.stringData(None, "preprocess.instrument", noCacheUpdate = True)
            numSecurities = instruments.cols()
            dataPtr = Framework.StringData(dci.universe, numSecurities, dci._def)

            for ii in range(numSecurities):
                instrument = instruments.getValue(0, ii)
                value = instrument.split('.')[index] if index >= 0 else instrument
                dataPtr.setValue(0, ii, value)

            frame.setData(PyUtil.script_sptr(dataPtr))
            frame.dataOffset = 0
            frame.dataSize = dataPtr.dataSize()

            self.info("[%d]-[%s] Count: %d" % (dci.rt.getDate(dci.di), dci._def.name, numSecurities))

        def buildDataFrame(self, dr, dci, frame, data, dataName):
            instruments = dr.stringData(None, "preprocess.instrument", noCacheUpdate = True)
            numSecurities = instruments.cols()
            assert numSecurities == len(data), 'Mismatch in data count and securities. Expecting: %d - Found: %d' % (numSecurities, len(data))
            dataPtr = Framework.StringData(dci.universe, numSecurities, dci._def)

            for ii in range(numSecurities):
                dataPtr.setValue(0, ii, data[ii][dataName])

            frame.setData(PyUtil.script_sptr(dataPtr))
            frame.dataOffset = 0
            frame.dataSize = dataPtr.dataSize()

            self.info("[%d]-[%s] Count: %d" % (dci.rt.getDate(dci.di), dci._def.name, numSecurities))

        def build(self, dr, dci, frame):
            try:
                dataName = dci._def.name

                if dataName == "ms_ticker":
                    return self.buildSubInstrument(dr, dci, frame, 2)
                elif dataName == "ms_exchangeId":
                    return self.buildSubInstrument(dr, dci, frame, 0)
                elif dataName == "ms_type":
                    return self.buildSubInstrument(dr, dci, frame, 1)
                else:
                    data = self.buildData(dr, dci)
                    return self.buildDataFrame(dr, dci, frame, data, dci._def.name)

            except Exception as e:
                print(traceback.format_exc())
                raise e

except Exception as e:
    print(traceback.format_exc())
    raise e
