### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import traceback
import datetime as DT

import PyUtil
import sys

import requests
import json

from time import sleep

from collections import namedtuple, OrderedDict

try:
    import Framework
    from BasicData import BasicData

    class MS_SecInfoGeneric(BasicData):
        def __init__(self, baseObject):
            try:
                super().__init__(baseObject, "MS_SecInfoGeneric")
                # self.debug("MS_SecInfoGeneric::__init__")

                self.loadConfig()

                self.dataCache      = None
                self.totalCount     = 0
                self.datasetName    = self.config().getString("datasetName", "prepropcess")
                self.instrumentQuery= self.config().getString("instrumentQuery", "prepropcess.instrument")
                self.instrumentMatch= self.config().getString("instrumentMatch", self.instrumentQuery)
                self.instrumentQueryString = self.config().getString("instrumentQueryString", "instrument")
                self.fieldsString   = ""

                mappingsStr         = self.config().getString("mappings", "")
                
                if mappingsStr:
                    self.mappings, self.rmappings = PyUtil.parseDatasetMappings(mappingsStr)
                else:
                    self.mappings   = self.rmappings = {}

                numMappings         = len(self.mappings.keys())

                if numMappings:
                    keys            = list(self.mappings.keys())
                    self.fieldsString = keys[0]

                    for i in range(1, numMappings):
                        self.fieldsString += "," + keys[i]

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def initDatasets(self, dr, datasets):
            try:

                # self.debug("MS_SecInfoGeneric::initDatasets")
                ds = Framework.Dataset(self.datasetName)

                for mapping in self.rmappings:
                    ds.add(Framework.DataValue(mapping, Framework.DataType.kString, 0, Framework.DataFrequency.kStatic, Framework.DataFlags.kUpdatesInOneGo | Framework.DataFlags.kAlwaysOverwrite)) 

                datasets.add(ds)

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def preDataBuild(self, dr, dci):
            try:
                # self.debug("MS_SecInfoGeneric::preDataBuild: %s" % (dci._def.name))
                pass
            except Exception as e:
                print(traceback.format_exc())
                raise e

        def postBuild(self, dr, dci):
            try:
                # self.debug("MS_SecInfoGeneric::postBuild: %s" % (dci._def.name))
                pass
            except Exception as e:
                print(traceback.format_exc())
                raise e

        def constructInstrumentStr(self, instruments, startIndex, endIndex):
            instrumentsStr = ""
            instrumentsMap = {}

            for ii in range(startIndex, endIndex):
                instrument = instruments.getValue(0, ii)
                if instrument:
                    instrumentsStr += instrument + ","
                    instrumentsMap[instrument] = ii

            # remove the trailing comma (,) at the end of the string
            if instrumentsStr:
                return instrumentsStr[:-1], instrumentsMap

            return "", {}

        def getDataValue(self, rdata, dataId):
            return rdata[dataId] if rdata and dataId in rdata else ""

        def buildData(self, dr, dci):
            if self.dataCache:
                return self.dataCache

            instruments     = dr.stringData(None, self.instrumentQuery, noCacheUpdate = True)
            instrumentsMatch= dr.stringData(None, self.instrumentMatch, noCacheUpdate = True)
            numSecurities   = instruments.cols()
            maxRequests     = self.config().getInt("maxRequests", 25)
            requestDelay    = float(self.config().getInt("requestDelay", 0)) * 0.001
            startIndex      = 0
            endIndex        = min(startIndex + maxRequests, numSecurities)
            data            = []
            downloadUrl     = self.config().getResolvedRequiredString("downloadUrl")
            numRetry        = 0
            maxRetry        = self.config().getInt("maxRetry", 3)
            instrumentMatchFormat = self.config().getString("instrumentMatchFormat", "%s.%s.%s")
            instrumentMatchConstituents= self.config().getString("instrumentMatchConstituents", "H1,H2,H3").split(',')

            self.debug("Base URL: %s" % (downloadUrl))

            while startIndex < endIndex:
                instrumentsStr, instrumentsMap = self.constructInstrumentStr(instruments, startIndex, endIndex)

                if instrumentsStr:
                    url         = str.format("%s&%s=%s" % (downloadUrl, self.instrumentQueryString, instrumentsStr))

                    if self.fieldsString:
                        url     += "&fields=" + self.fieldsString

                    try:
                        resp    = requests.get(url)

                        if maxRequests and requestDelay:
                            sleep(requestDelay)
                    except Exception as e:
                        numRetry+= 1
                        if numRetry >= maxRetry:
                            print("Request failed max retry exceeded: %d" % (numRetry))
                            sys.exit(1)
                        else:
                            import time
                            time.sleep(1)
                            continue

                    numRetry    = 0
                    if resp.status_code != 200:
                        print("Server error. Response code: %d - Response Text: %s" % (resp.status_code, text))
                        sys.exit(1)

                    try:
                        # Replace all the back-slashes (MorningStar sometimes returns an invalid '\' character in the payload)
                        text    = resp.text #resp.text.replace("\\", "")
                        rdata   = []
                        jdata   = json.loads(text)

                        errors  = jdata["errors"] if "errors" in jdata else []

                        for error in errors:
                            if error != "No Data" and "Invalid enablement" not in error:
                                assert False, "Error: %s - Request %s" % (error, url)


                        if "quotes" in jdata:
                            jdata = jdata["quotes"]
                            if "results" in jdata:
                                rdata = jdata["results"]

                        #rdata   = rdata["quotes"]["results"]

                    except Exception as e:
                        self.error("Exception while downloading url: %s [%d, %d)" % (url, startIndex, endIndex))
                        self.error("JSON Payload: %s" % (resp.text))
                        self.error(traceback.format_exc())
                        sys.exit(1)

                    index       = 0

                    self.debug("Downloaded '%s' mapped data for range: [%d, %d). Num Results: %d" % (self.instrumentQuery, startIndex, endIndex, len(rdata)))

                    results     = {}
                    for rd in rdata:
                        instrumentIdList                = [rd[c] for c in instrumentMatchConstituents]
                        instrumentId                    = str.format(instrumentMatchFormat % tuple(instrumentIdList))
                        # instrumentId                    = str.format("%s.%s.%s" % (rd["H2"], rd["H3"], rd["H1"]))
                        results[instrumentId]           = rd 

                    for ii in range(startIndex, endIndex):
                        index += 1
                        instrument                      = instrumentsMatch.getValue(0, ii)
                        instrumentResult                = results[instrument] if instrument in results else None

                        dataItem                        = {}
                        dataItem["instrument"]          = instrument

                        for mapping in self.rmappings:
                            target                      = self.rmappings[mapping]
                            dataItem[mapping]           = self.getDataValue(instrumentResult, target)

                        data.append(dataItem)
                else:
                    for ii in range(startIndex, endIndex):
                        instrument                      = instruments.getValue(0, ii)
                        instrumentResult                = None

                        dataItem                        = {}
                        dataItem["instrument"]          = instrument

                        for mapping in self.rmappings:
                            target                      = self.rmappings[mapping]
                            dataItem[mapping]           = self.getDataValue(instrumentResult, target)

                        data.append(dataItem)

                startIndex = endIndex
                endIndex = min(startIndex + maxRequests, numSecurities)

            self.dataCache = data

            return self.dataCache

        def buildSubInstrument(self, dr, dci, frame, index):
            instruments = dr.stringData(None, self.instrumentQuery, noCacheUpdate = True)
            numSecurities = instruments.cols()
            dataPtr = Framework.StringData(dci.universe, numSecurities, dci._def)

            for ii in range(numSecurities):
                instrument = instruments.getValue(0, ii)
                value = instrument.split('.')[index] if index >= 0 else instrument
                dataPtr.setValue(0, ii, value)

            frame.setData(PyUtil.script_sptr(dataPtr))
            frame.dataOffset = 0
            frame.dataSize = dataPtr.dataSize()

            self.info("[%d]-[%d] %s" % (dci.rt.getDate(dci.di), numSecurities, dci._def.name))

        def buildDataFrame(self, dr, dci, frame, data, dataName):
            instruments = dr.stringData(None, self.instrumentQuery, noCacheUpdate = True)
            numSecurities = instruments.cols()
            assert numSecurities == len(data), 'Mismatch in data count and securities. Expecting: %d - Found: %d' % (numSecurities, len(data))
            dataPtr = Framework.StringData(dci.universe, numSecurities, dci._def)

            for ii in range(numSecurities):
                dataPtr.setValue(0, ii, data[ii][dataName])

            frame.setData(PyUtil.script_sptr(dataPtr))
            frame.dataOffset = 0
            frame.dataSize = dataPtr.dataSize()

            self.info("[%d]-[%d] %s" % (dci.rt.getDate(dci.di), numSecurities, dci._def.name))

        def build(self, dr, dci, frame):
            try:
                dataName = dci._def.name

                # if dataName == "ms_ticker":
                #     return self.buildSubInstrument(dr, dci, frame, 2)
                # elif dataName == "ms_exchangeId":
                #     return self.buildSubInstrument(dr, dci, frame, 0)
                # elif dataName == "ms_type":
                #     return self.buildSubInstrument(dr, dci, frame, 1)
                # else:
                data = self.buildData(dr, dci)
                return self.buildDataFrame(dr, dci, frame, data, dci._def.name)

            except Exception as e:
                print(traceback.format_exc())
                raise e

except Exception as e:
    print(traceback.format_exc())
    raise e
