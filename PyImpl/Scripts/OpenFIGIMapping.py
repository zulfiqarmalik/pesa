### This code is provided under the terms of the MIT license
### https://opensource.org/licenses/MIT

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import traceback
import datetime as DT

import PyUtil
import sys
from collections import namedtuple, OrderedDict
import csv
import requests
import json
from enum import Enum

def resolveSecurityForFigi(symbols, serverUrl, apiKey, symbolType, exchCode = "US"):
    print("PYTHON - FIGI", str.format("Resolving FIGI try: %s" % (str(symbolType))))

    maxRequests = 100
    totalCount  = len(symbols)
    numLeft     = totalCount
    startIndex  = 0
    endIndex    = startIndex + min(maxRequests, numLeft)
    apiUrl      = serverUrl
    securities  = []
    secMoreInfo = []

    headers     = {
        'X-OPENFIGI-APIKEY': apiKey
    }

    while startIndex < endIndex:
        data = []
        srcDataMoreInfo = []
        secIndexes = []
        dataInfo = []
        exchCode = "US"

        for i in range(startIndex, endIndex):
            symbol = symbols[i]

            if symbolType == "cusip":
                data.append({ "idType": "ID_CUSIP", "idValue": symbol, "exchCode": exchCode })
                srcDataMoreInfo.append({ "orgSymbol": "" })
            elif symbolType == "sedol":
                data.append({ "idType": "ID_SEDOL", "idValue": symbol, "exchCode": exchCode })
                srcDataMoreInfo.append({ "orgSymbol": "" })
            elif symbolType == "isin":
                data.append({ "idType": "ID_ISIN", "idValue": symbol, "exchCode": exchCode })
                srcDataMoreInfo.append({ "orgSymbol": "" })
            else:
                data.append({ "idType": "TICKER", "idValue": symbol, "exchCode": exchCode })
                srcDataMoreInfo.append({ "orgSymbol": symbol })

            secIndexes.append(i)


        print("PYTHON - FIGI", str.format("[%s] - Getting num FIGIs for range: %d - [%d, %d)" % (str(symbolType), len(data), startIndex, endIndex)))

        numLeft = totalCount - endIndex
        startIndex = endIndex
        endIndex = startIndex + min(maxRequests, numLeft)

        if len(data) == 0:
            continue

        import requests
        responseData = requests.post(apiUrl, json = data, headers = headers)

        # Some error was returned
        if responseData.status_code < 200 or responseData.status_code >= 300:
            text = responseData.text.replace("{", "{{")
            text = text.replace("}", "}}")
            print("PYTHON - FIGI", str.format("Server error. Response code: %d - Response Text: %s" % (responseData.status_code, text)))
            return None

        import json
        rdataList = json.loads(responseData.text)
        rdataCount = len(rdataList)

        for i in range(rdataCount):
            rdata = rdataList[i]
            secIndex = secIndexes[i]
            symbol = symbols[secIndex]

            # Some error has occured over here ... ignore this figi?
            if "error" in rdata:
                print("PYTHON - FIGI", str.format("[%d] Unable to resolve security: %s - Security Info: %s [%s]" % (secIndex, rdata["error"], symbol, symbolType)))

                    # OK, since we had error on the last try over here, we're just going to ignore this security for the time being ...
            else:
                # If more than one entry is returned then we give out a warning and just use the first entry as the 
                if len(rdata["data"]) > 1:
                    print("PYTHON - FIGI", str.format("[%d] More than one securities returned for entry: %s [%s]" % (secIndex, symbol, symbolType)))

                # for moreInfo in rdata["data"]:
                # we just take the first entry
                moreInfo = rdata["data"][0]
                orgSymbol = srcDataMoreInfo[i]["orgSymbol"]

                # Don't add it if the symbol still exists
                securities.append(symbol)
                secMoreInfo.append(moreInfo)

    print("PYTHON - FIGI", str.format("[%s] Total securities loaded: %d" % (str(symbolType), len(securities))))

    return securities, secMoreInfo

