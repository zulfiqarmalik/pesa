### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import sys 
import os

def runStandalone(psimPath, configPath, logPath = ""):
    assert psimPath, "Must specify the path to PSim so that the correct shared libraries can be loaded"
    assert configPath, "Must specify a config to run"
    # psimPath = "P:\\Work\\Pesa\\build\\Debug\\windows\\psim.exe" 
    os.chdir(os.path.dirname(psimPath))
    cwd = os.getcwd()

    sys.path.append(".")
    sys.path.append(os.path.join(os.path.dirname(psimPath), "Python"))
    
    import Framework as FW
    args = FW.StringVec()
    args.append("-c")
    args.append(configPath)
    
    if logPath:
        args.append("-l")
        args.append(logPath)
        
    app = FW.App()
    app.pyInit(psimPath, args)
    app.run()
    
