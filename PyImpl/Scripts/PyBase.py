### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import sys
import traceback
import ctypes
import numpy as np

try:
    import Framework
    import PyUtil

    class PyBase:
        def __init__(self, baseObject, logChannel):
            self.__class__ = type(baseObject.__class__.__name__,
                                  (self.__class__, baseObject.__class__),
                                  {})
            self.__dict__ = baseObject.__dict__

            if logChannel is not None:
                self.setLogChannel(logChannel)
            self._logChannel = self.getLogChannel()

        def debug(self, *args):
            fstring = str.format(*args)
            Framework.debug("PYTHON - " + self._logChannel, fstring)

        def trace(self, *args):
            fstring = str.format(*args)
            Framework.trace("PYTHON - " + self._logChannel, fstring)

        def info(self, *args):
            fstring = str.format(*args)
            Framework.info("PYTHON - " + self._logChannel, fstring)

        def warning(self, *args):
            fstring = str.format(*args)
            Framework.warning("PYTHON - " + self._logChannel, fstring)

        def error(self, *args):
            fstring = str.format(*args)
            Framework.error("PYTHON - " + self._logChannel, fstring)

        def critical(self, *args):
            fstring = str.format(*args)
            Framework.critical("PYTHON - " + self._logChannel, fstring)

        ###########################################################################################
        # BEGIN - Utility functions!                                                              #
        ###########################################################################################
        def IR(self, data, di, days = None):
            if days is None:
                days = self.config().getInt("days", 21)

            rows = data[di - days:di]
            mean = np.nanmean(rows, axis = 0)
            stddev = np.nanstd(rows, axis = 0)
            return mean / stddev
            
        def ZScore(self, data, di, stDays = None, ltDays = None):
            if stDays is None:
                stDays = self.config().getInt("shortTermDays", 5)

            if ltDays is None:
                ltDays = self.config().getInt("longTermDays", 21)

            stMean = np.nanmean(data[di - stDays:di], axis = 0)
            ltMean = np.nanmean(data[di - ltDays:di], axis = 0)
            ltStdDev = np.nanstd(data[di - ltDays:di], axis = 0)
            
            return (stMean - ltMean) / ltStdDev
            
        ###########################################################################################
        # END - Utility functions!                                                                #
        ###########################################################################################

    # Base component class
    class PyPipelineComponent(PyBase, Framework.IPipelineComponent):
        def __init__(self, baseObject, logChannel):
            PyBase.__init__(self, baseObject, logChannel)

    # Base component class
    class PyComponent(PyBase, Framework.IComponent):
        def __init__(self, baseObject, logChannel):
            PyBase.__init__(self, baseObject, logChannel)

        def alphaResult(self):
            result = self.alpha()
            return PyUtil.floatToNPArray(result)

        def floatData(self, universe, name):
            return PyUtil.typeToNPGeneric(self.dataRegistry().floatData(universe, name), ctypes.c_float, np.float32, flattenDimension = False)

        def intData(self, universe, name):
            return PyUtil.typeToNPGeneric(self.dataRegistry().intData(universe, name), ctypes.c_int, np.int32, flattenDimension = False)

        def uintData(self, universe, name):
            return PyUtil.typeToNPGeneric(self.dataRegistry().uintData(universe, name), ctypes.c_uint, np.uint32, flattenDimension = False)

        def ushortData(self, universe, name):
            return PyUtil.typeToNPGeneric(self.dataRegistry().ushortData(universe, name), ctypes.c_ushort, np.uint16, flattenDimension = False)

        def int64Data(self, universe, name):
            return PyUtil.typeToNPGeneric(self.dataRegistry().int64Data(universe, name), ctypes.c_longlong, np.int64, flattenDimension = False)

        def floatArg(self, index):
            farg = self.getFloatArg(index)
            assert farg, "Unable to float argument: %d" % (index)
            # OK, now that we have the data ... we just make a numpy array out of it ...
            return PyUtil.floatToNPMatrix(farg)

    # Base alpha class
    class PyAlpha(PyComponent):
        def __init__(self, baseObject, logChannel):
            super().__init__(baseObject, logChannel)

        def alphaAsNP(self):
            alpha = self.alpha()
            return PyUtil.floatToNP(alpha)

        def positions(self):
            return PyUtil.floatToNPMatrix(self.alpha())

    # Base alpha class
    class PyOperation(PyComponent):
        def __init__(self, baseObject, logChannel):
            super().__init__(baseObject, logChannel)

    # Base IDataLoader class
    class PyDataLoader(PyComponent):
        def __init__(self, baseObject, logChannel):
            super().__init__(baseObject, logChannel)

    # Base universe class
    class PyUniverse(PyDataLoader):
        def __init__(self, baseObject, logChannel):
            super().__init__(baseObject, logChannel)

    # Base combination class
    class PyCombination(PyComponent):
        def __init__(self, baseObject, logChannel):
            super().__init__(baseObject, logChannel)

    # IOptimiser implementation
    class PyOptimiser(PyComponent):
        def __init__(self, baseObject, logChannel):
            super().__init__(baseObject, logChannel)

    class PyCalcData: 
        def __init__(self, calcData):
            self.calcData           = calcData
            
            if calcData is None:
                return

            self.yesterday          = None
            self.remappedAlpha      = None
            self.stats              = calcData.stats
            self.alpha              = PyUtil.floatToNPArray(calcData.alphaPtr())

            self.rawAlpha           = PyUtil.floatVecToNPArray(calcData.rawAlpha, calcData.rawAlphaPtr())
            self.alphaNotionals     = PyUtil.floatVecToNPArray(calcData.alphaNotionals, calcData.alphaValuesPtr())
            self.alphaPnls          = PyUtil.floatVecToNPArray(calcData.alphaPnls, calcData.alphaPnlsPtr())
            self.cumAlphaPnls       = PyUtil.floatVecToNPArray(calcData.cumAlphaPnls, calcData.cumAlphaPnlsPtr())
            self.alphaCosts         = PyUtil.floatVecToNPArray(calcData.alphaCosts, calcData.alphaCostsPtr())
            self.alphaShares        = PyUtil.floatVecToNPArray(calcData.alphaShares, calcData.alphaSharesPtr())

            self.alphaReturns       = PyUtil.floatVecToNPArray(calcData.alphaReturns, calcData.alphaReturnsPtr())
            self.alphaPrice         = PyUtil.floatVecToNPArray(calcData.alphaPrice, calcData.alphaPricePtr())
            self.alphaPrevPrice     = PyUtil.floatVecToNPArray(calcData.alphaPrevPrice, calcData.alphaPrevPricePtr())
            self.remappedUniverse   = calcData.remappedUniverse

            # If we have some data from yesterday ...
            if calcData.yesterday:
                self.yesterday      = PyCalcData(calcData.yesterday)

            if calcData.remappedAlpha:
                self.remappedAlpha  = PyUtil.floatToNPArray(calcData.remappedAlphaPtr())

        def isValid(self):
            return True if self.calcData is not None else False
                

except Exception as e:
    print(traceback.format_exc())
