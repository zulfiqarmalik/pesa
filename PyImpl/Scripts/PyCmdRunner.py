### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import sys
import traceback
from datetime import datetime as DT
import numpy as np
from numpy import isnan
import os
import time

try:
    import Framework
    import PyUtil
    from PyBase import PyPipelineComponent

    class PyCmdRunner(PyPipelineComponent):
        def __init__(self, baseObject):
            super().__init__(baseObject, "PyCmdRunner")

        def runCommand(self, rt, commandSec):
            try:
                onlyRunWhenAsked    = commandSec.getBool("onlyRunWhenAsked", True)

                if onlyRunWhenAsked and rt.appOptions.runCmds is not True:
                    self.info("PyCmdRunner DISABLED! Run with -K option to enable!")
                    return

                cmd                 = commandSec.getRequired("cmd")
                args                = commandSec.getRequired("args")
                name                = commandSec.getString("name", "")
                workingDir          = commandSec.getString("dir", "")
                onlyOnTradingDays   = commandSec.getBool("onlyOnTradingDays", False)
                platform            = commandSec.getString("platform", "")
                isPyExec            = commandSec.getBool("pyExec", False)
                iff                 = commandSec.getString("iff", "")

                if iff:
                    iff             = self.config().resolve(iff, 0)
                    self.info("Evaluating iff: %s" % (iff))
                    result          = eval(iff)
                    if result is not True:
                        self.info("iff eval: %s = %s" % (iff, str(result)))
                        return

                if platform and platform != rt.platform():
                    self.info("Platform doesn't match. Current platform: %s - Requested: %s" % (platform, rt.platform()))
                    return

                if onlyOnTradingDays and rt.isTodayHoliday:
                    self.info("Not running command because of holiday: %s" % (cmd))
                    return

                # endDate = rt.getDate(rt.diEnd)

                cmd = self.config().resolve(cmd, 0)

                if workingDir:
                    workingDir = self.config().resolve(workingDir, 0)
                else:
                    workingDir = "./"

                if args:
                    args = self.config().resolve(args, 0)
                    args = eval(args)
                else:
                    args = []

                if name:
                    self.info("===== BEGIN - %s =====" % (name))

                if not isPyExec:
                    cmdName = cmd
                    cmd = [cmd] + args

                    self.info("Executing command: %s [dir = %s]" % (str(cmd), str(workingDir)))
                    import subprocess
                    devnull = open(os.devnull, 'w')

                    process = subprocess.Popen(cmd, cwd = workingDir, stdout = subprocess.PIPE, stderr = subprocess.PIPE)

                    while process.poll() is None:
                        time.sleep(5)

                        for line in process.stdout:
                            self.debug(line.decode('utf-8').rstrip())

                        for line in process.stderr:
                            self.error(line.decode('utf-8').rstrip())

                        continue
                        # line = str(process.stdout.readline())
                        # line = line.replace("\n", "")
                        # if line:
                        #     self.info("[%s] - %s" % (cmdName, line))

                        # line = str(process.stderr.readline())
                        # line = line.replace("\n", "")
                        # if line:
                        #     self.error("[%s - stderr] - %s" % (cmdName, line))

                    self.info("Process returned: %d" % (process.returncode))
                    assert process.returncode == 0, "Process returned: %d" % (process.returncode)
                else:
                    self.info("Python exec: %s" % (str(cmd)))
                    exec(cmd)

                if name:
                    self.info("===== END - %s =====" % (name))

            except Exception as e:
                self.error(traceback.format_exc())
                rt.exit(-199)

        def runSection(self, rt, secName):
            self.info("Loading section: %s" % (secName))
            sec = self.config().getChildSection(secName)
            if sec is None:
                return

            numChildren = sec.numChildren() 

            for ci in range(numChildren):
                commandSec = sec.getChildSectionAtIndex(ci)
                if commandSec.name() == "Cmd":
                    self.runCommand(rt, commandSec)
            # commandSecs = sec.getAllChildrenOfType("Cmd")
            # self.info("Number of %s/Cmd sections: %d" % (secName, len(commandSecs)))

            # for commandSec in commandSecs:
            #     self.runCommand(rt, commandSec)

        def tick(self, rt):
            pass

        def prepare(self, rt):
            try:
                self.info("Preparing!") 
                self.runSection(rt, "Pre")
            except Exception as e:
                self.error(traceback.format_exc())

        def finalise(self, rt):
            try:
                self.info("Finalising!")
                self.runSection(rt, "Post")
            except Exception as e:
                self.error(traceback.format_exc())
                rt.exit(-199)

except Exception as e:
    print(traceback.format_exc())

