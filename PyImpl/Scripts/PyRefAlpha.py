import numpy as np
import Framework as qv
import traceback
from PyBase import PyAlpha

class PyRefAlpha(PyAlpha):
    def __init__(self, baseObject):
        super().__init__(baseObject, "PyRefAlpha")

    def run(self, rt):
        try:
            # rt = QuanVolve Runtime and rt.di is the current day of the simulation
            # Delay here is the delay of the alpha. Most alphas use last day's EOD data so delay will be 1
            di          = rt.di - self.getDelay()

            # Get the universe. This is controlled by the settings. universe.name() will give the name to confirm this
            universe    = self.getUniverse()

            # Get the numpy array of positions
            positions   = self.positions()

            # Get the numpy matrix for the Adjusted Close price
            closePrice  = self.floatData(universe, "baseData.adjClosePrice")

            # Alpha is the reciprocal of the closing price
            positions[0]= 1.0 / closePrice[di]

        except Exception as e:
            print(traceback.format_exc())

