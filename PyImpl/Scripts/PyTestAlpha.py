import numpy as np
import Framework as qv
from PyBase import PyAlpha
import traceback

class IRAlpha(PyAlpha):
    def __init__(self, baseObject):
        super().__init__(baseObject, "IRAlpha")
        
    def ir(data, di, days = 21):
        rows = data[di - days:di]
        mean = np.nanmean(rows, axis=0)
        stddev = np.nanstd(rows, axis=0)
        return mean / stddev
        
    def run(self, rt):
        try:
            # rt = QuanVolve Runtime and rt.di is the current day of the simulation
            # Delay here is the delay of the alpha. Most alphas use last day's EOD data so delay will be 1
            di          = rt.di - self.getDelay()

            # Get the universe. This is controlled by the settings. universe.name() will give the name to confirm this
            universe    = self.getUniverse()

            # Get the numpy array of positions
            positions   = self.positions()
            
            dh          = self.floatData(universe, "derivedData.dH")
            dl          = self.floatData(universe, "derivedData.dL")
            openPrice   = self.floatData(universe, "baseData.adjOpenPrice")
            
            positions[0] = self.ir(openPrice, di) / (dh / dl)
            # IR(baseData.adjOpenPrice) / (derivedData.dH / derivedData.dL)            

            # Get the numpy matrix for the Adjusted Close price
            # closePrice  = self.floatData(universe, "baseData.adjClosePrice")

            # Alpha is the reciprocal of the closing price
            # positions[0]= 1.0 / closePrice[di]

        except Exception as e:
            print(traceback.format_exc())

