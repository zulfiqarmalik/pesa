### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import sys
import traceback
from datetime import datetime as DT
import numpy as np
from numpy import isnan
import os

try:
    import Framework
    import PyUtil
    from PyBase import PyAlpha
    from PyBase import PyCalcData

    class PyTestAlpha_Iter(PyAlpha):
        def __init__(self, baseObject):
            super().__init__(baseObject, "PyTestAlpha_Iter")

        def run(self, rt):
            try:
                di          = rt.di - self.getDelay()
                universe    = self.getUniverse()
                result      = self.alpha()
                npResult    = BamUtil.floatToNPArray(result)
                closePrice  = self.floatData(universe, "baseData.adjClosePrice")
                numStocks   = result.cols()

                for ii in range(numStocks):
                    cp      = closePrice[di, ii]
                    value   = (1.0 / cp)
                    npResult[ii] = value

            except Exception as e:
                print(traceback.format_exc())

except Exception as e:
    print(traceback.format_exc())

