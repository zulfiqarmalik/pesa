### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import sys
import traceback
import numpy as np
import os

try:
    import Framework
    import PyUtil
    from PyBase import PyAlpha

    class PyTestAlpha_Vector(PyAlpha):
        def __init__(self, baseObject):
            super().__init__(baseObject, "PyTestAlpha_Vector")

        def run(self, rt):
            try:
                di          = rt.di - self.getDelay()
                alpha       = self.alpha()
                result      = BamUtil.floatToNPMatrix(alpha)
                closePrice  = self.floatData(self.getUniverse(), "baseData.adjClosePrice")

                result[0]   = 1.0 / closePrice[di]

            except Exception as e:
                print(traceback.format_exc())

except Exception as e:
    print(traceback.format_exc())

