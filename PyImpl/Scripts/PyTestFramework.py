### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import sys
import traceback
from datetime import datetime as DT
import numpy as np
from numpy import isnan
import os

try:
    import Framework
    import PyUtil
    from PyBase import PyPipelineComponent
    import pytest

    class PyTestFramework(PyPipelineComponent):
        def __init__(self, baseObject):
            super().__init__(baseObject, "PyTestFramework")

        def prepare(self, rt):
            try:
                myPath = os.path.realpath(__file__)
                myDir = os.path.dirname(myPath)

                testsDir = os.path.join(myDir, "Tests")
                testsDir = self.config().getResolvedString("testsDir", testsDir)
                
                self.debug("Running tests in: %s" % (testsDir))

                if os.path.exists(testsDir):
                    pytest.main([testsDir])
                else:
                    self.error("Directory does not exist: %s" % (testsDir))

            except Exception as e:
                print(traceback.format_exc())

        def tick(self, rt):
            try:
                self.debug("tick!")
            except Exception as e:
                print(traceback.format_exc())

        def finalise(self, rt):
            try:
                self.debug("finalise!")
            except Exception as e:
                print(traceback.format_exc())


except Exception as e:
    print(traceback.format_exc())

