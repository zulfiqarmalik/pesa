### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import sys
import traceback
from datetime import datetime as DT
import numpy as np

try:
    import Framework
    import PyUtil
    from PyBase import PyOperation
    from BamUtil import PyCalc

    class PyTestOperation(PyOperation):
        def __init__(self, baseObject):
            super().__init__(baseObject, "PyTestOperation")

        def run(self, rt):
            try:
                alphaData   = self.alphaData()
                di          = rt.di - alphaData.delay
                alphaResult = self.alpha()
                numStocks   = alphaResult.cols()
                universe    = alphaData.universe
                stats       = self.stats()
                ltStats     = self.ltStats()

                ydata       = self.yesterdayData()
                alphaPnls   = ydata.alphaPnls

                mean        = np.nanmean(alphaPnls)

                # momentum    = rt.dataRegistry.floatData(universe, "axioma.f_US3_MH_Momentum")
                # cov         = rt.dataRegistry.floatData(None, "axioma.c_US3_MH_Cov")

                # size        = cov.cols()
                # w           = sqrt(size)
                # ithRow      = cov.rowArray(di)
                

                # for i in range(len(alphaPnls)):
                #     print(alphaPnls[i])


                # self.debug("PNL: %f" % (stats.pnl))
                self.debug("LT-PNL: %f" % (alphaPnls[0]))

                # for ii in range(numStocks):
                #     print("Alpha-%d: %f" % (ii, alphaResult.getValue(0, ii)))

            except Exception as e:
                print(traceback.format_exc())

except Exception as e:
    print(traceback.format_exc())
