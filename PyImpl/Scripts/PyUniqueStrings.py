### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import traceback
import datetime as DT

import PyUtil
import sys

import numpy as np
from pandas import DataFrame

try:
    import Framework as FW
    from PyBase import PyDataLoader

    class PyUniqueStrings(PyDataLoader):
        def __init__(self, baseObject):
            super().__init__(baseObject, "PyUniqueStrings")

        def initDatasets(self, dr, datasets):
            try:
                self.datasetName = self.config().getRequired("datasetName")
                self.srcName = self.config().getRequired("src")
                dstName = self.config().getRequired("dst")

                ds = FW.Dataset(self.datasetName)
                ds.add(FW.DataValue(dstName, FW.DataType.kString, 0, FW.DataFrequency.kStatic, FW.DataFlags.kUpdatesInOneGo | FW.DataFlags.kAlwaysOverwrite))

                datasets.add(ds)

            except Exception as e:
                print(traceback.format_exc())
                raise e


        def build(self, dr, dci, frame):
            try:
                srcData = dr.stringData(dci.universe, self.srcName)

                uniqueStringLookup = {}
                uniqueStrings = []

                numRows = srcData.rows()
                numCols = srcData.cols()

                for di in range(numRows):
                    for ii in range(numCols):
                        string = srcData.getValue(di, ii)

                        if string and string not in uniqueStringLookup:
                            uniqueStringLookup[string] = True
                            uniqueStrings.append(string)

                numUniqueStrings = len(uniqueStrings)
                dataPtr = FW.StringData(dci.universe, numUniqueStrings, dci._def)

                for i in range(numUniqueStrings):
                    dataPtr.setValue(0, i, uniqueStrings[i])

                frame.setData(BamUtil.script_sptr(dataPtr))
                frame.dataOffset = 0
                frame.dataSize = dataPtr.dataSize()
                

            except Exception as e:
                print(traceback.format_exc())

        def preDataBuild(self, dr, dci):
            pass

        def postDataBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())

        def preBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())

        def postBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())

except Exception as e:
    print(traceback.format_exc())
    raise e
