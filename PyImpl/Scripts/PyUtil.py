### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import sys
import datetime as DT
import ctypes
import numpy as np
import os

def debug(logChannel, *args):
    import Framework as FW
    fstring = str.format(*args)
    FW.debug("PYTHON - " + logChannel, fstring)

def trace(logChannel, *args):
    import Framework as FW
    fstring = str.format(*args)
    FW.trace("PYTHON - " + logChannel, fstring)

def info(logChannel, *args):
    import Framework as FW
    fstring = str.format(*args)
    FW.info("PYTHON - " + logChannel, fstring)

def warning(logChannel, *args):
    import Framework as FW
    fstring = str.format(*args)
    FW.warning("PYTHON - " + logChannel, fstring)

def error(logChannel, *args):
    import Framework as FW
    fstring = str.format(*args)
    FW.error("PYTHON - " + logChannel, fstring)

def critical(logChannel, *args):
    import Framework as FW
    fstring = str.format(*args)
    FW.critical("PYTHON - " + logChannel, fstring)

def script_sptr(dataPtr):
    dataPtr.thisown = False # C++ is now responsible for this pointer!
    return dataPtr.script_sptr()

def strToDate(date, sep = "-"):
    return DT.datetime.strptime(date, "%Y" + sep + "%m" + sep + "%d")
    
def intToDate(date):
    return DT.datetime.strptime(str(date), "%Y%m%d")

def dateTimeFormat(time, dateSep = "-", timeSep = ":"):
    dt = {
        "year": str(time.year).zfill(4), "month": str(time.month).zfill(2), "day": str(time.day).zfill(2),
        "hour": str(time.hour).zfill(2), "minute": str(time.minute).zfill(2), "second": str(time.second).zfill(2)
    }

    # formatString = str.format("{year}%s{month}%s{day} {hour}%s{minute}%s{second}" % (dateSep, dateSep, timeSep, timeSep))

    return "{year}-{month}-{day} {hour}:{minute}:{second}".format(**dt)

def sendHttpEmail(recipients, title, text, senderId = None, senderName = None, apiKey = None, msgUrl = None, senderUrl = None, isHtml = False):
    import requests

    if apiKey is None:
        apiKey = os.environ["MAILGUN_API_KEY"]

    assert apiKey, "Unable to get MailGun API key from the environment variable: MAILGUN_API_KEY. Environment: %s" % (str(os.environ))

    if msgUrl is None:
        msgUrl = os.environ["MAILGUN_MSG_URL"]

    assert msgUrl, "Unable to get MailGun message url from the environment variable: MAILGUN_MSG_URL. Environment: %s" % (str(os.environ))

    if senderUrl is None:
        senderUrl = os.environ["MAILGUN_SENDER_URL"]

    assert senderUrl, "Unable to get MailGun sender URL from the environment variable: MAILGUN_SENDER_URL. Environment: %s" % (str(os.environ))

    if not senderId:
        senderId = "quanvolve"

    if not senderName:
        senderName = senderId

    sender = str.format("%s <%s@%s>" % (senderName, senderId, senderUrl))

    data = {
        "from": sender,
        "to": recipients if isinstance(recipients, list) else [recipients],
        "subject": title,
    }

    if isHtml is False:
        data["text"] = text
    else:
        data["html"] = text

    return requests.post(msgUrl, auth = ("api", apiKey), data = data)

# def runSubProcess(cmd, workindDir, numRetries = 3, timeout = 0):
#     import subprocess
#     from subprocess import STDOUT, check_output
    
#     process = subprocess.Popen(cmd, cwd = workindDir, stdout = subprocess.PIPE, stderr = subprocess.STDOUT)

#     while process.poll() is None:
#         line = str(process.stdout.readline())
#         line = line.replace("\n", "")
#         print(line)

#     if process.returncode != 0:
#         assert False, "\n\nSWIG returned error: %d" % (process.returncode)

#     return

def intDateToStrDate(date, sep = "-"):
    return dateToStr(intToDate(date), sep)

def strDateToIntDate(date, sep = "-"):
    return dateToInt(strToDate(date, sep))

def dateToUKStr(date, sep = "-"):
    return str(date.day).zfill(2) + sep + str(date.month).zfill(2) + sep + str(date.year).zfill(4)
    
def dateToStr(date, sep = "-"):
    return str(date.year).zfill(4) + sep + str(date.month).zfill(2) + sep + str(date.day).zfill(2)
    
def dateToInt(date):
    return int(str(date.year).zfill(4) + str(date.month).zfill(2) + str(date.day).zfill(2))

def timeToInt(dt):
    return int(str(dt.hour).zfill(2) + str(dt.minute).zfill(2) + str(dt.second).zfill(2))

def intTimeToDatetime(date):
    return DT.datetime.fromtimestamp(int(date) / 1000000000)

def getStrDates(intDates):
    strDates = []

    for intDate in intDates:
        strDates.append(intDateToStrDate(intDate))

    return strDates
    
def resolveFrequency(f):
    import Framework 

    f = f.lower()

    if f == "daily":
        return Framework.DataFrequency.kDaily
    elif f == "universal":
        return Framework.DataFrequency.kStatic
    elif f == "weekly":
        return Framework.DataFrequency.kWeekly
    elif f == "monthly":
        return Framework.DataFrequency.kMonthly
    elif f == "quarterly":
        return Framework.DataFrequency.kQuarterly
    elif f == "semiAnnually":
        return Framework.DataFrequency.kSemiAnnually
    elif f == "annually":
        return Framework.DataFrequency.kAnnually

    assert False, "Invalid frequency: %s" % (f)

def resolveDataType(dataType):
    import Framework 

    dataType = dataType.lower()

    if dataType == "float":
        return Framework.DataType.kFloat
    if dataType == "float[]":
        return Framework.DataType.kFloatArray
    elif dataType == "int":
        return Framework.DataType.kInt
    elif dataType == "int[]":
        return Framework.DataType.kIntArray
    elif dataType == "string":
        return Framework.DataType.kString
    elif dataType == "blob":
        return Framework.DataType.kBlob
    elif dataType == "custom":
        return Framework.DataType.kCustom
    elif dataType == "unknown":
        return Framework.DataType.kUnknown

    assert False, "Unsupported type: %s" % (dataType)

def getDataSize(dataType):
    import Framework 
    
    if dataType == Framework.DataType.kFloat:
        return 4
    elif dataType == Framework.DataType.kInt:
        return 4

    # assert False, "Unsupported type for getDataSize: %s" % (dataType)

    return 0

def parseDataOps(dataOps):
    retDataOps = {}

    dataOps = dataOps.split(",")
    for dataOp in dataOps:
        dataOp.strip()
        fmt, value = dataOp.split("=")
        retDataOps[value] = fmt

    return retDataOps

def parseFunctions(functions):
    functions = functions.split(",")
    funcMap = {}

    for ii, function in enumerate(functions):
        function = function.strip()
        lhs, rhs = function.split("=")
        rhs = rhs.replace("{", "dataValue['")
        rhs = rhs.replace("}", "']")
        funcMap[lhs] = rhs

    return funcMap


def parseDatasetMappings(mappings):
    mappings = mappings.split(",")
    forwardMapping = {}
    reverseMapping = {}

    for ii, mapping in enumerate(mappings):
        mapping = mapping.strip()
        if mapping:
            original, mapping = mapping.split("=")
            forwardMapping[original.strip()] = mapping.strip()
            reverseMapping[mapping.strip()] = original.strip()

    return (forwardMapping, reverseMapping)

def rawPtrToNPGeneric(dataPtr, size, ctype, npType):
    if dataPtr is None:
        return None 
        
    arrayType = ctype * size
    address = ctypes.cast(dataPtr, ctypes.POINTER(arrayType))
    # npArray = np.frombuffer(arrayType.from_address(address))
    npArray = np.frombuffer(address.contents, npType)

    return npArray

def unpackPyCapsule(capsule):
    if capsule is None:
        return None
        
    import ctypes
    ctypes.pythonapi.PyCapsule_GetPointer.restype = ctypes.c_void_p
    ctypes.pythonapi.PyCapsule_GetPointer.argtypes = [ctypes.py_object, ctypes.c_void_p]
    return ctypes.pythonapi.PyCapsule_GetPointer(capsule, None)

def typeToNPGeneric(fdata, ctype, nptype, flattenDimension):
    assert fdata, "Invalid data pointer passed to be converted to numpy array!"
    
    rows = fdata.rows()
    cols = fdata.cols()
    matrixSize = rows * cols
    dataPtr = fdata.fptr()
    dataPtr = unpackPyCapsule(dataPtr)
    npArray = rawPtrToNPGeneric(dataPtr, matrixSize, ctype, nptype)
    
    if flattenDimension is False:
        return npArray.reshape((rows, cols))
        
    return npArray

def floatRowAsNPArray(fdata, rowIndex):
    cols = fdata.cols()
    matrixSize = cols
    dataPtr = fdata.rowPtr(rowIndex)
    dataPtr = unpackPyCapsule(dataPtr)
    ctype = ctypes.c_float
    nptype = np.float32
    npArray = rawPtrToNPGeneric(dataPtr, matrixSize, ctype, nptype)

    npArray.reshape(1, cols)
    return npArray

def floatToNPGeneric(fdata, flattenDimension = False):
    return typeToNPGeneric(fdata, ctypes.c_float, np.float32, flattenDimension)

def floatToNPMatrix(fdata):
    return floatToNPGeneric(fdata, flattenDimension = False)

def intToNPMatrix(fdata, flattenDimension = False):
    return typeToNPGeneric(fdata, ctypes.c_int, np.int32, flattenDimension)

def uintToNPMatrix(fdata, flattenDimension = False):
    return typeToNPGeneric(fdata, ctypes.c_uint, np.uint32, flattenDimension)

def ushortToNPMatrix(fdata, flattenDimension = False):
    return typeToNPGeneric(fdata, ctypes.c_ushort, np.uint16, flattenDimension)

def shortToNPMatrix(fdata, flattenDimension = False):
    return typeToNPGeneric(fdata, ctypes.c_short, np.int16, flattenDimension)

def int64ToNPMatrix(fdata, flattenDimension = False):
    return typeToNPGeneric(fdata, ctypes.c_longlong, np.int64, flattenDimension)

def ulongToNPMatrix(fdata, flattenDimension = False):
    return typeToNPGeneric(fdata, ctypes.c_ulong, np.uint64, flattenDimension)

def doubleToNPMatrix(fdata, flattenDimension = False):
    return typeToNPGeneric(fdata, ctypes.c_double, np.float64, flattenDimension)

def floatToNPArray(fdata):
    return floatToNPGeneric(fdata, flattenDimension = True)

def floatVecToNPArray(fvec, fvecPtr):
    size = fvec.size()
    fvecPtr = unpackPyCapsule(fvecPtr)
    return rawPtrToNPGeneric(fvecPtr, size, ctypes.c_float, np.float32)

def parseDatasets(strDatasets):
    import Framework 
    
    datasets = strDatasets.split(",")

    resolvedDatasets = []

    for i, dataset in enumerate(datasets):
        defs = dataset.strip().split(".")
        defsLength = len(defs)
        assert defsLength >= 2 and defsLength <= 3, "Invalid dataset definition: %s [%s]" % (defs, dataset)

        dataType = defs[0]
        dataName = defs[1]
        frequency = Framework.DataFrequency.kDaily

        if defsLength == 3:
            frequency = resolveFrequency(defs[2])

        dataType = resolveDataType(dataType)

        resolvedDatasets.append({
            "type": dataType,
            "typeSize": getDataSize(dataType),
            "name": dataName,
            "frequency": frequency
        })

    return resolvedDatasets

def parseExchangeIds(exchanges, sep = '|'):
    retExchanges = []
    retExchangeIds = []
    retCountries = []
    exchanges = exchanges.split(sep)
    assert exchanges, "Must specify which exchanges to load the calendar for!"

    for exchange in exchanges:
        exchange, exchangeId = exchange.split("=")
        country, exList = exchange.strip().split(":")
        exList = exList.split(",")

        for ex in exList:
            retExchanges.append(ex.strip())
            retCountries.append(country.strip())
            retExchangeIds.append(exchangeId.strip())
            
    assert retExchanges, "Must specify which exchanges to load the calendar for!"
    return retCountries, retExchanges, retExchangeIds

def parseExchanges(exchanges, sep = '|'):
    retExchanges = []
    retCountries = []
    exchanges = exchanges.split(sep)
    assert exchanges, "Must specify which exchanges to load the calendar for!"

    for exchange in exchanges:
        country, exList = exchange.split(":")
        exList = exList.split(",")
        for ex in exList:
            retExchanges.append(ex.strip())
            retCountries.append(country.strip())
            
    assert retExchanges, "Must specify which exchanges to load the calendar for!"
    return retCountries, retExchanges

def zipFile(srcFilename, zipFilename):
    import zipfile
    zipHandle = zipfile.ZipFile(zipFilename, 'w')
    zipHandle.write(srcFilename, os.path.basename(srcFilename), compress_type = zipfile.ZIP_DEFLATED)
    zipHandle.close()

def unzipFile(zipFilename):
    import zipfile
    dirname = os.path.dirname(zipFilename)

    with zipfile.ZipFile(zipFilename, "r") as zipHandle:
        zipHandle.extractall(dirname)
        dataFilenames = zipHandle.namelist()
        return dataFilenames

    return []

def getDataFromUrl(url, numRetries = 3, jsonLib = None, timeout = 30, headers = None):
    import requests

    numTries = 0
    responseData = None

    while numTries < numRetries:
        responseData = requests.get(url, timeout = timeout, headers = headers)

        if responseData.status_code != 200:
            print("[Try: %d / %d] Request error status: %d - Text: %s" % (numTry, numRetries, responseData.status_code, responseData.text))
            numTries += 1
            continue

        return responseData

    return responseData

def getJsonFromUrl(url, numRetries = 3, jsonLib = None, timeout = 30, headers = None):
    import requests

    numTries = 0
    while numTries < numRetries:
        responseData = requests.get(url, timeout = timeout, headers = headers)

        if responseData.status_code != 200:
            print("[Try: %d / %d] Request error status: %d - Text: %s" % (numTry, numRetries, responseData.status_code, responseData.text))
            numTries += 1
            continue

        import json
        jsonParser = json if jsonLib is None else jsonLib
        data = jsonParser.loads(responseData.text)

        return data

    return None


def downloadToFile(downloadUrl, filename):
    import requests

    responseData = requests.get(downloadUrl)

    if responseData.status_code != 200:
        return None

    with open(filename, 'w') as file:
        file.write(responseData.text.replace('\r\n', '\n'))

    return responseData.text

def _figiSymbolId(symbol, exchCode):
    return str.format("%s_%s" % (symbol, exchCode))

def resolveSecurityForFigi(symbols, serverUrl, apiKey, symbolType, exchCodes = None, filename = None):
    import Framework
    Framework.debug("PYTHON - FIGI", str.format("Resolving FIGI try: %s" % (str(symbolType))))

    maxRequests = 100
    totalCount  = len(symbols)
    numLeft     = totalCount
    startIndex  = 0
    endIndex    = startIndex + min(maxRequests, numLeft)
    apiUrl      = serverUrl
    securities  = []
    secMoreInfo = []
    maxRetry    = 3
    delimiter   = "|"

    loadedSymbols = {}
    fileHandle  = None
    numWritten  = 0
    date        = DT.datetime.now()
    idate       = dateToInt(date)
    zipFilename = filename + ".zip" if filename else None

    # first of all we try to load the file
    if zipFilename:

        if os.path.isfile(zipFilename):
            unzipFile(zipFilename)
                                                          
        if os.path.isfile(filename):
            Framework.debug("PYTHON - FIGI", str.format("Opening file for reading/writing: %s" % (filename)))

            # For some reason, when I open the file in append (a+) mode for reading and writing, the csv
            # reader is unable to read anything from it. So, I'm opening the file in read mode first to
            # read the existing contents, and then open it again in append mode to append the new entries
            fileHandle = open(filename, "r")

            import csv
            reader = csv.reader(fileHandle, delimiter = "|")
            numRows = 0
            for row in reader:
                numRows += 1

                if numRows == 1:
                    continue
                
                symbol = row[0].strip()
                exchCode = row[1].strip()

                info = {
                    "exchCode": row[1].strip(),
                    "figi": row[2].strip(),
                    "name": row[3].strip(),
                    "ticker": row[4].strip(),
                    "exchCode": row[5].strip(),
                    "compositeFIGI": row[6].strip(),
                    "uniqueID": row[7].strip(),
                    "securityType": row[8].strip(),
                    "marketSector": row[9].strip(),
                    "shareClassFIGI": row[10].strip(),
                    "securityDescription": row[11].strip(),
                    "date": int(row[12].strip()),
                }

                symbolId = _figiSymbolId(symbol, exchCode)
                loadedSymbols[symbolId] = info

            Framework.debug("PYTHON - FIGI", str.format("Number of existing FIGIs read from file: %d" % (len(loadedSymbols))))

            # Close the existing file handle and then open again in append mode ...
            fileHandle.close()
            fileHandle = open(filename, "a+")
            fileHandle.seek(0, os.SEEK_END)
        else:
            Framework.debug("PYTHON - FIGI", str.format("Opening NEW file for writing: %s" % (filename)))
            fileHandle = open(filename, "w")
            fileHandle.write("symbol | exchCode | figi | name | ticker | exchCode | compositeFIGI | uniqueID | securityType | marketSector | shareClassFIGI | securityDescription | date\n")
            fileHandle.flush()

    headers     = {
        'X-OPENFIGI-APIKEY': apiKey
    }

    while startIndex < totalCount:
        data = []
        srcDataMoreInfo = []
        secIndexes = []
        dataInfo = []
        exchCode = "US"

        orgStartIndex = startIndex

        while len(data) < maxRequests and startIndex < totalCount:
            i = startIndex
            startIndex += 1
            symbol = symbols[i]
            exchCode = exchCodes[i] if exchCodes and i < len(exchCodes) and exchCodes[i] else "US"
            symbolId = _figiSymbolId(symbol, exchCode)

            # If the symbol has already been loaded then we just take it from the LUT
            if symbolId in loadedSymbols:
                info = loadedSymbols[symbolId]
                securities.append(symbol)
                secMoreInfo.append(info)
            else:
                if symbolType == "cusip":
                    data.append({ "idType": "ID_CUSIP", "idValue": symbol, "exchCode": exchCode })
                    srcDataMoreInfo.append({ "orgSymbol": "" })
                elif symbolType == "sedol":
                    data.append({ "idType": "ID_SEDOL", "idValue": symbol, "exchCode": exchCode })
                    srcDataMoreInfo.append({ "orgSymbol": "" })
                elif symbolType == "isin":
                    data.append({ "idType": "ID_ISIN", "idValue": symbol, "exchCode": exchCode })
                    srcDataMoreInfo.append({ "orgSymbol": "" })
                else:
                    data.append({ "idType": "TICKER", "idValue": symbol, "exchCode": exchCode })
                    srcDataMoreInfo.append({ "orgSymbol": symbol })

                secIndexes.append(i)

        numRetry = 0

        if len(data) == 0:
            continue

        Framework.debug("PYTHON - FIGI", str.format("[%s] - Getting num FIGIs for range: %d - [%d, %d)" % (str(symbolType), len(data), orgStartIndex, startIndex)))

        try:
            import requests
            responseData = requests.post(apiUrl, json = data, headers = headers)
        except Exception as e:
            numRetry+= 1
            if numRetry >= maxRetry:
                print("Request failed max retry exceeded: %d" % (numRetry))
                sys.exit(1)
            else:
                import time
                time.sleep(1)
                continue

        # numLeft = totalCount - endIndex
        # startIndex = endIndex
        # endIndex = startIndex + min(maxRequests, numLeft)
        
        # Some error was returned
        if responseData.status_code < 200 or responseData.status_code >= 300:
            text = responseData.text.replace("{", "{{")
            text = text.replace("}", "}}")
            Framework.error("PYTHON - FIGI", str.format("Server error. Response code: %d - Response Text: %s" % (responseData.status_code, text)))
            return None

        import json
        rdataList = json.loads(responseData.text)
        rdataCount = len(rdataList)

        for i in range(rdataCount):
            rdata = rdataList[i]
            secIndex = secIndexes[i]
            symbol = symbols[secIndex]
            exchCode = exchCodes[secIndex] if exchCodes and secIndex < len(exchCodes) and exchCodes[secIndex] else "US"

            # Some error has occured over here ... ignore this figi?
            if "error" in rdata:
                Framework.error("PYTHON - FIGI", str.format("[%d] Unable to resolve security: %s - Security Info: %s %s [%s]" % (secIndex, rdata["error"], symbol, exchCode, symbolType)))

                if fileHandle:
                    fileHandle.write("%s | %s | | | | | | | | | | | %d\n" % (symbol, exchCode, idate))

                    # OK, since we had error on the last try over here, we're just going to ignore this security for the time being ...
            else:
                # If more than one entry is returned then we give out a warning and just use the first entry as the 
                if len(rdata["data"]) > 1:
                    Framework.debug("PYTHON - FIGI", str.format("[%d] More than one securities returned for entry: %s [%s]" % (secIndex, symbol, symbolType)))

                # for moreInfo in rdata["data"]:
                # we just take the first entry
                moreInfo = rdata["data"][0]
                orgSymbol = srcDataMoreInfo[i]["orgSymbol"]

                # Don't add it if the symbol still exists
                securities.append(symbol)
                secMoreInfo.append(moreInfo)

                if fileHandle:
                    numWritten += 1
                    fileHandle.write("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s | %s | %s | %d\n" % (
                        symbol, exchCode, 
                        moreInfo["figi"] if "figi" in moreInfo else "",
                        moreInfo["name"] if "name" in moreInfo else "",
                        moreInfo["ticker"] if "ticker" in moreInfo else "",
                        moreInfo["exchCode"] if "exchCode" in moreInfo else "",
                        moreInfo["compositeFIGI"] if "compositeFIGI" in moreInfo else "",
                        moreInfo["uniqueID"] if "uniqueID" in moreInfo else "",
                        moreInfo["securityType"] if "securityType" in moreInfo else "",
                        moreInfo["marketSector"] if "marketSector" in moreInfo else "",
                        moreInfo["shareClassFIGI"] if "shareClassFIGI" in moreInfo else "",
                        moreInfo["securityDescription"] if "securityDescription" in moreInfo else "",
                        idate
                    ))
                    
        if fileHandle:
            fileHandle.flush()

    Framework.debug("PYTHON - FIGI", str.format("[%s] Total securities loaded: %d" % (str(symbolType), len(securities))))

    if fileHandle:
        Framework.debug("PYTHON - FIGI", str.format("Total figi entries written: %d" % (numWritten)))
        fileHandle.close()

        # Now we zip up the file and remove the temporary file
        zipFile(filename, zipFilename)
        os.remove(filename)

    return securities, secMoreInfo

