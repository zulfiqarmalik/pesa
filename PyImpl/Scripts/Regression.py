### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import sys
import traceback
from datetime import datetime as DT
import numpy as np
from numpy import isnan
import os

from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.preprocessing import Imputer

try:
    import Framework
    import PyUtil
    from PyBase import PyAlpha
    from PyBase import PyCalcData

    class Regression(PyAlpha):
        def __init__(self, baseObject):
            super().__init__(baseObject, "Regression")

        def run(self, rt):
            try:
                di          = rt.di - self.getDelay()
                universe    = self.getUniverse()
                result      = self.alpha()
                npResult    = BamUtil.floatToNPArray(result)

                days        = self.config().getInt("days", 21)
                
                print("DI: %d" % (rt.di))

                # Transpose so that 
                numArgs     = self.getNumArgs()
                args        = []
                argsSample  = []
                predictArgs = []

                for ai in range(numArgs):
                    arg     = self.floatArg(ai)
#                    assert arg, "Unable to load argument at index: %d" % (ai)
                    argSample = np.transpose(np.nan_to_num(arg[-days - 1:-1]))

                    predictArg = np.transpose(np.nan_to_num(arg[-1:]))
                    predictArgs.append(predictArg)

                    # argSample.reshape(-1, 1)
                    argsSample.append(argSample)

                Y           = np.stack(predictArgs, axis=-1)
#                Y           = Y.reshape(-1, 1)
                X           = np.stack(argsSample, axis=-1)
                y           = self.floatData(universe, "baseData.returns")
                y           = np.transpose(np.nan_to_num(y[-days:]))

                import pdb; pdb.set_trace()

                # Calculate the slope using the data ...
                numStocks   = result.cols()

                # # npResult    = Regression(closePrice, price)

                for ii in range(numStocks):
                    regModel    = linear_model.LinearRegression()
                    regModel.fit(X[ii], y[ii].reshape(-1, 1))
                    pred        = regModel.predict(Y[ii])

                    npResult[ii] = pred[0][0] * 100.0 # To reduce rounding off errors
                
                import pdb; pdb.set_trace()

            except Exception as e:
                print(traceback.format_exc())

except Exception as e:
    print(traceback.format_exc())

