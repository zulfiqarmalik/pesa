### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import traceback
import datetime as DT

import PyUtil
import sys
from collections import namedtuple, OrderedDict
import csv
import requests
import json
from enum import Enum
import os

try:
    import Framework
    from BasicData import BasicData

    class Reuters_DailyListings(BasicData):
        def __init__(self, baseObject):
            try:
                super().__init__(baseObject, "Reuters_DailyListings")
                self.debug("Reuters_DailyListings::__init__")

                self.loadConfig()

                # Get the holidays file
                exchanges               = self.config().getResolvedRequiredString("exchanges")
                self.datasetName        = self.config().getRequired("datasetName")

                self.countries, self.exchanges, self.exchangeIds = PyUtil.parseExchangeIds(exchanges)
                assert self.exchanges and len(self.exchanges), "Must specify which exchanges to load the calendar for!"

                self.exchangeTickers    = {}

                self.listingType        = self.config().getString("listingType", "")

                self.baseDir            = self.config().getResolvedRequiredString("baseDir")
                self.currentFilename    = self.config().getRequired("currentFilename")
                self.zipFilename        = self.config().getRequired("zipFilename")
                self.data               = {}
                self.uuids              = []
                self.uuidLookup         = {}

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def initDatasets(self, dr, datasets):
            try:
                self.debug("Reuters_DailyListings::initDatasets")

                ds = Framework.Dataset(self.datasetName)

                ds.flushDataset = True

                ds.add(Framework.DataValue("uuid", Framework.DataType.kLongIndexedString, 0, Framework.DataFrequency.kDaily, 0))
                ds.add(Framework.DataValue("ticker", Framework.DataType.kLongIndexedString, 0, Framework.DataFrequency.kDaily, 0))
                ds.add(Framework.DataValue("tradingSymbol", Framework.DataType.kLongIndexedString, 0, Framework.DataFrequency.kDaily, 0))
                ds.add(Framework.DataValue("exchangeCode", Framework.DataType.kLongIndexedString, 0, Framework.DataFrequency.kDaily, 0))
                ds.add(Framework.DataValue("issuePermId", Framework.DataType.kLongIndexedString, 0, Framework.DataFrequency.kDaily, 0))
                ds.add(Framework.DataValue("issuerPermId", Framework.DataType.kLongIndexedString, 0, Framework.DataFrequency.kDaily, 0))
                ds.add(Framework.DataValue("quotePermId", Framework.DataType.kLongIndexedString, 0, Framework.DataFrequency.kDaily, 0))
                ds.add(Framework.DataValue("currency", Framework.DataType.kLongIndexedString, 0, Framework.DataFrequency.kDaily, 0))
                ds.add(Framework.DataValue("openPrice", Framework.DataType.kFloat, 0, Framework.DataFrequency.kDaily, 0))
                ds.add(Framework.DataValue("closePrice", Framework.DataType.kFloat, 0, Framework.DataFrequency.kDaily, 0))
                ds.add(Framework.DataValue("lowPrice", Framework.DataType.kFloat, 0, Framework.DataFrequency.kDaily, 0))
                ds.add(Framework.DataValue("highPrice", Framework.DataType.kFloat, 0, Framework.DataFrequency.kDaily, 0))
                ds.add(Framework.DataValue("volume", Framework.DataType.kFloat, 0, Framework.DataFrequency.kDaily, 0))
                ds.add(Framework.DataValue("vwap", Framework.DataType.kFloat, 0, Framework.DataFrequency.kDaily, 0))
                ds.add(Framework.DataValue("tradingStatus", Framework.DataType.kFloat, 0, Framework.DataFrequency.kDaily, 0))

                datasets.add(ds)

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def addUuid(self, uuid):
            if uuid not in self.uuidLookup:
                self.uuidLookup[uuid] = len(self.uuids)
                self.uuids.append(uuid)

            return self.uuidLookup[uuid]

        def preDataBuild(self, dr, dci):
            try:
                if dci.rt.appOptions.updateDataOnly is False:
                    return 

                self.debug("MS_SecInfo::preDataBuild - START")

                issuePermIds = dr.stringData(None, self.datasetName + ".issuePermId", noCacheUpdate = True)

                if not issuePermIds:
                    return
                    
                self.numSecuritiesParsed = issuePermIds.cols()
                self.debug("Existing number of securities parsed: %d" % (self.numSecuritiesParsed))

                for ii in range(issuePermIds.cols()):
                    issuePermId = issuePermIds.getValue(0, ii)
                    self.addUuid(issuePermId)

                self.debug("MS_SecInfo::preDataBuild - DONE!")

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def loadHistoricalFile(self, exchangeIndex, date):
            currentFilename = self.parentConfig().defs().resolve(self.currentFilename, date)
            zipFilename = os.path.join(self.baseDir, currentFilename)
            contentFilename = self.parentConfig().defs().resolve(self.zipFilename, date)

            if os.path.isfile(zipFilename):
                self.debug("Loading: %s" % (zipFilename))
                import zipfile
                with zipfile.ZipFile(zipFilename, 'r') as zipHandle:
                    name = os.path.basename(contentFilename)
                    fileContents = zipHandle.read(name,) 
                    return fileContents.decode("utf-8")

            self.debug("Historical file not found: %s" % (currentFilename))
            return None

        def buildDataForExchange(self, dr, dci, exchangeIndex, date):
            contents                            = self.loadHistoricalFile(exchangeIndex, date).split('\n')
            data                                = []
            rowCount                            = len(contents)

            for rowIndex in range(1, rowCount):
                secData                         = {}
                row                             = contents[rowIndex].strip().split(',')

                if len(row) >= 15:
                    tradeDate                   = row[0]
                    secData["ticker"]           = row[1]
                    secData["tradingSymbol"]    = row[2]
                    secData["exchangeCode"]     = row[3]
                    secData["issuePermId"]      = row[4]
                    secData["issuerPermId"]     = row[5]
                    secData["quotePermId"]      = row[6]
                    secData["currency"]         = row[7]
                    secData["openPrice"]        = row[8]
                    secData["closePrice"]       = row[9]
                    secData["lowPrice"]         = row[10]
                    secData["highPrice"]        = row[11]
                    secData["volume"]           = row[12]
                    secData["vwap"]             = row[13]
                    secData["tradingStatus"]    = row[14]
                    secData["uuid"]             = secData["issuePermId"]

                    uuid                        = secData["uuid"]

                    if uuid:
                        ii                      = self.addUuid(uuid)
                        secData["ii"]           = ii
                        data.append(secData)

            return data

        def buildData(self, dr, dci):
            date = dci.rt.getDate(dci.di)
            sdate = str(date)

            # Last day's data is always empty
            if dci.di == dci.rt.diEnd:
                return []

            if sdate in self.data:
                return self.data[sdate]

            # Clear the cache ... we don't need the old data anymore!
            self.data = {}
            self.data[sdate] = []

            for exchangeIndex in range(len(self.exchanges)):
                exchangeData = self.buildDataForExchange(dr, dci, exchangeIndex, date)
                self.data[sdate] = self.data[sdate] + exchangeData

            return self.data[sdate]

        # def buildUuid(self, dr, dci, frame, dataName):
        #     numUuids = len(self.uuids)        
        #     dataPtr = Framework.LondIndexedStringData(dci.universe, numUuids, dci._def)

        #     for i in range(numUuids):
        #         dataPtr.setValue(0, i, self.uuids[i])

        #     frame.setData(PyUtil.script_sptr(dataPtr))
        #     frame.dataOffset = 0
        #     frame.dataSize = dataPtr.dataSize()

        #     self.info("[%d]-[%s] Total Unique UUID Count: %d" % (dci.rt.getDate(dci.di), dci._def.name, numUuids))

        def buildStringData(self, dr, dci, frame, securities, dataName):
            numSecurities = len(securities)
            numUuids = len(self.uuids)        
            dataPtr = Framework.LongIndexedStringData(dci.universe, numUuids, dci._def)

            if numSecurities == 0:
                frame.noDataUpdate = True
                return

            for i in range(numSecurities):
                security = securities[i]
                ii = security["ii"]
                dataPtr.setValue(0, ii, security[dataName])

            frame.setData(PyUtil.script_sptr(dataPtr))
            frame.dataOffset = 0
            frame.dataSize = dataPtr.dataSize()

            self.debug("[%d]-[%s] Number of securities: %d" % (dci.rt.getDate(dci.di), dataName, numSecurities))

        def buildMatrixData(self, dr, dci, frame, securities, dataName, MatrixType):
            numSecurities = len(securities)
            numUuids = len(self.uuids)        
            dataPtr = MatrixType(dci.universe, dci._def, 1, numUuids, False)

            if numSecurities == 0:
                frame.noDataUpdate = True
                return

            dataPtr.invalidateMemory()
                
            for i in range(numSecurities):
                security = securities[i]
                ii = security["ii"]
                if security[dataName]:
                    dataPtr.setValue(0, ii, float(security[dataName]))

            frame.setData(PyUtil.script_sptr(dataPtr))
            frame.dataOffset = 0
            frame.dataSize = dataPtr.dataSize()

            self.debug("[%d]-[%s] Number of securities: %d" % (dci.rt.getDate(dci.di), dataName, numSecurities))

        def build(self, dr, dci, frame):
            try:
                dataName = dci._def.name

                securities = self.buildData(dr, dci)

                if dci._def.itemType == Framework.DataType.kLongIndexedString:
                    return self.buildStringData(dr, dci, frame, securities, dataName)
                else:
                    return self.buildMatrixData(dr, dci, frame, securities, dataName, Framework.FloatMatrixData)

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def postBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())
                raise e

except Exception as e:
    print(traceback.format_exc())
    raise e
