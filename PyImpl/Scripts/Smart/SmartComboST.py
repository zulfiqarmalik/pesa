### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import sys
import traceback
from datetime import datetime as DT
import numpy as np
from numpy import isnan
import os
import operator
from collections import deque

try:
    import Framework
    import PyUtil
    import SmartComboUtil
    from PyBase import PyCombination
    import trueskill as TS

    class SmartComboST(PyCombination):
        def __init__(self, baseObject):
            super().__init__(baseObject, "SmartComboST")
            self.rankHistory    = None

        def run(self, rt):
            try:
                self.trace("SmartComboST::run") 

                ar              = self.alphas()
                numChildren     = ar.numChildren()

                self.trace("Num child alphas: %d" % (numChildren))

                # Create a new list of ratings
                if self.rankHistory is None:
                    self.rankHistory = []
                    maxLen      = int(self.config().getString("maxLen", "5"))
                    self.rankHistory = deque(maxlen = maxLen)

                    # for i in range(numChildren):
                    #     self.ratings.append(AlphaStats(maxLen = maxLen))

                ar.ensureAlloc()
                result                  = BamUtil.floatToNPArray(ar.resultAlpha().remappedAlpha())
                result[0:]              = 0.0
                
                ranks, sortedPnls, ddFactors = SmartComboUtil.sortAndRankChildren(ar)

                # We only calculate if we have some rating
                if len(sortedPnls):
                    self.rankHistory.append({
                        "ranks": ranks,
                        "sortedPnls": sortedPnls,
                        "ddFactors": ddFactors
                    })

                    # Initialise a new ratings vector 
                    ratings = []
                    for i in range(numChildren):
                        ratings.append((TS.Rating(),))

                    # Then rate 
                    for j in range(len(self.rankHistory)):
                        try:
                            newRatings  = TS.TrueSkill().rate(ratings, ranks = self.rankHistory[j]["ranks"]) 
                        except Exception as e:
                            newRatings  = TS.TrueSkill(backend='mpmath').rate(ratings, ranks = self.rankHistory[j]["ranks"])

                        for ai in range(numChildren):
                            ratings[ai] = (newRatings[ai][0],)

                    numSecurities       = ar.resultAlpha().remappedAlpha().cols()
                    smartWeights        = np.zeros(numChildren)
                    minIndex            = 0
                    maxIndex            = 0

                    for ai in range(numChildren):
                        rating          = ratings[ai][0]

                        # calculate the smart weight
                        smartWeight     = rating.mu / rating.sigma
                        if smartWeight < 0.0:
                            smartWeight = 0.0

                        smartWeights[ai]= smartWeight #* self.ddFactors[ai]

                        if smartWeight < smartWeights[minIndex]:
                            minIndex    = ai
                        if smartWeight > smartWeights[maxIndex]:
                            maxIndex    = ai

                    smartWeightMean     = np.mean(smartWeights)
                    minWeight           = smartWeights[minIndex]

                    for ai in range(numChildren):
                        # self.ratings[ai]= newRatings[ai][0]
                        # rating          = self.ratings[ai]

                        # # calculate the smart weight
                        smartWeight     = smartWeights[ai] # - smartWeightMean # rating.mu / rating.sigma
                        import math
                        e               = (1.0 + (smartWeight - minWeight) / TS.BETA)
                        smartWeight     = math.exp(e) * 1.31
                        smartWeights[ai]= smartWeight
                        # smartWeights[ai]= smartWeight

                        # new weight is a combination of existing weight and the new weight
                        childAlpha      = ar.childAlpha(ai).resultAlpha()
                        weight          = childAlpha.getPInfo().weight
                        weight          = weight * smartWeight 
                        stats           = childAlpha.cumStats()

                        # pnls[ai]        = childAlpha.yesterdayData().stats.pnl
                        alpha           = BamUtil.floatToNPArray(childAlpha.remappedAlpha())
                        # result          = result + (np.nan_to_num(alpha) * weight)

                        for ii in range(numSecurities):
                            if not np.isnan(alpha[ii]):
                                result[ii] += alpha[ii] * weight


                    # minRating = self.ratings[minIndex][0]
                    # maxRating = self.ratings[maxIndex][0]

                    # self.info("Min - W: %0.2f [ddf = %0.2f], [mu: %0.2f, sg: %0.2f, bt: %0.2f] - UUID: %s" % (smartWeights[minIndex], self.ddFactors[minIndex], minRating.mu, minRating.sigma, TS.BETA, ar.childAlpha(minIndex).resultAlpha().uuid()))
                    # self.info("Max - W: %0.2f [ddf = %0.2f], [mu: %0.2f, sg: %0.2f, bt: %0.2f] - UUID: %s" % (smartWeights[maxIndex], self.ddFactors[maxIndex], maxRating.mu, maxRating.sigma, TS.BETA, ar.childAlpha(maxIndex).resultAlpha().uuid()))
                    # self.info("Average weight: %0.2f" % (smartWeightMean))

                    # if rt.isLastDi(rt.di):
                    #     self.info("\n === FINAL RANKINGS ===")
                    #     for ii in range(len(smartWeights)):
                    #         rating = self.ratings[ii][0]
                    #         self.info("%d - W: %0.2f, [mu: %0.2f, sg: %0.2f] - UUID: %s" % (ii, smartWeights[ii], rating.mu, rating.sigma, ar.childAlpha(ii).resultAlpha().uuid()))

            except Exception as e:
                self.error(traceback.format_exc())

except Exception as e:
    print(traceback.format_exc())

