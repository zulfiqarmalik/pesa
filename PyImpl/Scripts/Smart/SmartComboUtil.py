### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import sys
import traceback
from datetime import datetime as DT
import numpy as np
from numpy import isnan
import os
import operator
import Framework
import PyUtil

def sortAndRankChildren(ar):
    numChildren             = ar.numChildren()
    ddFactors               = []
    pnlRanks                = []
    sortedPnlRanks          = []
    ranks                   = []

    for i in range(numChildren):
        childAlpha          = ar.childAlpha(i).resultAlpha()
        ydata               = childAlpha.yesterdayData()

        if ydata:
            stats           = childAlpha.cumStats()
            rawAlphas       = BamUtil.floatToNPArray(ydata.alphaPtr())
            alphaPnls       = BamUtil.floatVecToNPArray(ydata.alphaPnls, ydata.alphaPnlsPtr())
            
            numSecurities   = len(rawAlphas)
            pnkRank         = 0.0
            ddFactor        = 1.0

            maxValue        = 0.3
            if stats.maxDD != 0.0:
                ddFactor    = min(max(maxValue - stats.D5.dd / stats.maxDD, 0.0), maxValue) / maxValue
                
                if ddFactor > 0.0:
                    ddFactors.append(ddFactor)
                else:
                    ddFactors.append(0.0)

            for ii in range(numSecurities):
                rawAlpha    = rawAlphas[ii]
                alphaPnl    = ydata.pnl(ii)
                percent     = abs(ydata.stats.pnl) / (abs(alphaPnl) if alphaPnl != 0.0 else 1.0)

                if alphaPnl > 0:
                    if rawAlpha > 0.0 or rawAlpha < 0.0:
                        pnkRank += abs(rawAlpha)
                elif alphaPnl < 0:
                    if rawAlpha > 0.0 or rawAlpha < 0.0:
                        pnkRank -= abs(rawAlpha)

            pnlRanks.append(pnkRank)
            sortedPnlRanks.append({
                "index": i, 
                "pnlRank": pnlRanks[-1]
            })

            ranks.append(0)

    # We only calculate if we have some rating
    if len(sortedPnlRanks):
        sortedPnlRanks      = sorted(sortedPnlRanks, key = operator.itemgetter("pnlRank"), reverse = True)

        prevRank            = 0
        prevPnl             = sortedPnlRanks[0]["pnlRank"]
        ichild              = sortedPnlRanks[0]["index"]
        ranks[ichild]       = prevRank

        for i in range(1, numChildren):
            currPnlRank     = sortedPnlRanks[i]["pnlRank"]
            ichild          = sortedPnlRanks[i]["index"]

            # increment the rank if the pnls don't match. But if they do then both have the same rank!
            if currPnlRank != prevPnl:
                prevRank += 1

            ranks[ichild]   = prevRank

    return ranks, sortedPnlRanks, ddFactors
