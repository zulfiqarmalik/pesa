### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import sys
import traceback
from datetime import datetime as DT
import numpy as np
from numpy import isnan
import os
import operator

try:
    import Framework
    import PyUtil
    from PyBase import PyOperation
    import trueskill as TS

    class SmartOp(PyOperation):
        def __init__(self, baseObject):
            super().__init__(baseObject, "SmartOp")
            self.ratings        = None

        def run(self, rt):
            try:
                self.trace("SmartOp::run") 
                ydata       = self.yesterdayData()

                import pdb; pdb.set_trace()

                # We cannot do anything without yesterday's data ...
                if ydata is None:
                    return

                alpha           = self.alpha()
                numStocks       = alpha.cols()
                alpha           = BamUtil.floatToNPArray(alpha)

                assert numStocks, "Invalid alpha with no stocks at all"

                self.trace("Num stocks: %d" % (numStocks))

                # Create a new list of ratings
                if self.ratings is None:
                    self.ratings= []

                    for i in range(numStocks):
                        self.ratings.append(TS.Rating())

                sortedPnls      = []
                ranks           = []
                oldRatings      = []
                pnls            = []

                for ii in range(numStocks):
                    pnl         = ydata.pnl(i)
                    pnls.append(pnl)
                    sortedPnls.append({
                        "ii":   ii, 
                        "pnl":  pnls[-1]
                    })

                    ranks.append(0)
                    oldRatings.append(None)

                # We only calculate if we have some rating
                sortedPnls      = sorted(sortedPnls, key = operator.itemgetter("pnl"), reverse = True)

                prevRank        = 0
                prevPnl         = sortedPnls[0]["pnl"]
                ii              = sortedPnls[0]["ii"]
                ranks[ii]       = prevRank
                oldRatings[ii]  = (self.ratings[ii],)

                for i in range(1, numStocks):
                    currPnl     = sortedPnls[i]["pnl"]
                    ii          = sortedPnls[i]["ii"]

                    # increment the rank if the pnls don't match. But if they do then both have the same rank!
                    if currPnl != prevPnl:
                        prevRank += 1

                    ranks[ii]   = prevRank
                    oldRatings[ii] = (self.ratings[ii],)


                newRatings      = TS.rate(oldRatings, ranks = ranks)
                # import pdb; pdb.set_trace()

                # print("Old rating: %0.2f - %0.2f" % (self.ratings[0].mu, self.ratings[0].sigma))

                smartWeights    = np.zeros(numStocks)

                for ii in range(numStocks):
                    self.ratings[ii]= newRatings[ii][0]
                    rating          = self.ratings[ii]

                    # calculate the smart weight
                    smartWeight     = rating.mu / rating.sigma
                    smartWeights[ii]= smartWeight

                # Then we rebalance using the smart weights
                import pdb; pdb.set_trace()
                alpha           = alpha * smartWeights

                # smartWeightMean     = np.mean(smartWeights)
                    

            except Exception as e:
                self.error(traceback.format_exc())

except Exception as e:
    print(traceback.format_exc())

