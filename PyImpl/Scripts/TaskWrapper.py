### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 20 22:37:59 2016

@author: jdemiranda
"""

import mosek
import numpy as np
from numpy import log, array, zeros, ones, inf, isnan
from numpy.linalg import cholesky
from pandas import DataFrame, Series
import pandas as pd


class TaskWrapper():
    def __init__(self):
        self.nextAvailableIndex = 0
        self.prob_blx = None
        self.prob_bux = None
        self.prob_bkx = None        
        self.nextAvailableConstraintIndex = 0
        self.prob_a = None
        self.prob_blc = None
        self.prob_buc = None
        self.prob_bkc = None
        self.prob_c = None
        self.cones = []
        
    def AddVariable(self, length, bound_type, lower_bound, upper_bound):
        indices = range(self.nextAvailableIndex, self.nextAvailableIndex + length)
        if self.prob_blx is None:
            self.prob_bkx = bound_type            
            self.prob_blx = lower_bound
            self.prob_bux = upper_bound
        else:
            self.prob_bkx.extend(bound_type)# np.hstack((self.prob_bkx, bound_type))
            self.prob_blx = np.hstack((self.prob_blx, lower_bound))
            self.prob_bux = np.hstack((self.prob_bux, upper_bound))
        self.nextAvailableIndex = self.nextAvailableIndex + length
        return indices
        
    def UpdateVariableLowerBound(self, idx, lower_bound):
        self.prob_blx[idx] = lower_bound
        
    def UpdateVariableUpperBound(self, idx, upper_bound):        
        self.prob_bux[idx] = upper_bound
        
    def AddLinearConstraint(self, constraint, constraint_type, lower_bound, upper_bound):
        length = constraint.shape[0]
        slices =  slice(self.nextAvailableConstraintIndex, self.nextAvailableConstraintIndex + length)
        if self.prob_a is None:
            self.prob_a = constraint.astype(np.float32)
            self.prob_bkc = constraint_type
            self.prob_blc = lower_bound
            self.prob_buc = upper_bound
        else:
            self.prob_a = np.vstack((self.prob_a, constraint.astype(np.float32)))
            self.prob_bkc.extend(constraint_type) #np.hstack((self.prob_bkc, constraint_type))
            self.prob_blc = np.hstack((self.prob_blc, lower_bound))
            self.prob_buc = np.hstack((self.prob_buc, upper_bound))
        self.nextAvailableConstraintIndex = self.nextAvailableConstraintIndex + length
        return slices
    
    def UpdateLinearConstraintSlice(self, constraint, idx, val):
        self.prob_a[constraint, idx] = val
        
    def UpdateLinearConstraintLowerBound(self, idx, lower_bound):
        self.prob_blc[idx] = lower_bound
        
    def UpdateLinearConstraintUpperBound(self, idx, upper_bound):
        self.prob_buc[idx] = upper_bound
    
    def SetObjectiveFunctionSlice(self, idx, val):
        if self.prob_c is None:
            self.prob_c = np.zeros(self.nextAvailableIndex)
        
        self.prob_c[idx] = val
    
    def UpdateObjectiveFunctionSlice(self, idx, val):
        self.prob_c[idx] = val # a little bit unnecessary!
        
    
    def AddCone(self, coneType, indices):
        self.cones.extend([(coneType, indices)])
    
    def InitialiseTask(self):
        
        (self.ind_prob_a_nonzero, self.ptrb, self.ptre, self.asub) = self.GetSparseMatrixAccessors2(self.prob_a)        
        
        self.ind_x = np.arange(self.nextAvailableIndex)
        self.ind_cons = np.arange(self.nextAvailableConstraintIndex)
        self.lenX = len(self.ind_x)        
        self.numConstraints = len(self.ind_cons)
        
        self.env = mosek.Env()
        self.task = self.env.Task(0,0)        
        
        # the following never change!
        self.task.appendcons(self.numConstraints)
        self.task.appendvars(self.lenX)
        self.task.putmaxnumvar(self.lenX)
        self.task.putmaxnumcon(self.numConstraints)
        self.task.putmaxnumcone(len(self.cones))
        self.task.putmaxnumanz(len(self.asub))
        for (coneType, indices) in self.cones:
            self.task.appendcone(coneType, 0.0, indices)
        
        self.task.putobjsense(mosek.objsense.minimize)
        self.task.putintparam(mosek.iparam.num_threads,2)
        
    def Optimize(self):
        self.task.putclist(self.ind_x, self.prob_c)
        self.task.putvarboundlist(self.ind_x, self.prob_bkx, self.prob_blx, self.prob_bux)
        self.task.putarowlist(self.ind_cons, self.ptrb, self.ptre, self.asub, self.prob_a[self.ind_prob_a_nonzero])
        self.task.putconboundlist(self.ind_cons, self.prob_bkc, self.prob_blc, self.prob_buc)

        self.task.optimize()
        
        #self.task.solutionsummary(mosek.streamtype.msg)
        solsta = self.task.getsolsta(mosek.soltype.itr)
        status = (solsta == mosek.solsta.optimal) or (solsta == mosek.solsta.near_optimal)
        self.xx = zeros(self.lenX, float)
        self.task.getxx(mosek.soltype.itr,self.xx)
        return (status, self.xx)
    
    @staticmethod
    def GetSparseMatrixAccessors2(A, rowBased = True):
        from scipy import sparse
        
        sA = sparse.csr_matrix(A)
        ptrb = sA.indptr[:-1]
        ptre = sA.indptr[1:]
        asub = sA.nonzero()[1]#.astype(int)
        aval = A != 0

        return (aval, ptrb, ptre, asub)

    @staticmethod
    def GetSparseMatrixAccessors(mat, rowBased = True):
        # slow and shitty for now.
        # implicitly assumes row based also
        nonZeros = mat != 0
        N = nonZeros.sum()
        ptrb = []
        ptre = []
        asub = np.zeros(N, 'int')
        nonzeroPos = 0
        for i in range(mat.shape[0]):
            foundFirstInRow = False
            for j in range(mat.shape[1]):
                if mat[i][j] != 0:
                    asub[nonzeroPos] = j
                    if not foundFirstInRow:
                        ptrb.append(nonzeroPos)
                        foundFirstInRow = True
                    nonzeroPos = nonzeroPos + 1
            ptre.append(nonzeroPos)
        
        ptrb = np.asarray(ptrb)
        ptre = np.asarray(ptre)
                
        return (nonZeros, ptrb, ptre, asub)
