### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import requests
from requests_ntlm import HttpNtlmAuth
import json
import pprint
from datetime import datetime as DT
import time
import pandas
import urllib

# URL = '''http://dalapi/v1/get?Keyword=BloombergExchangeListings&yml=\\\\iis\\KHAN_Exports\\share\\zulfi.yml'''
# URL = '''http://dalapi/v1/get?Keyword=FinancialCenterCodes&yml=\\\\iis\\KHAN_Exports\\share\\zulfi.yml'''
# URL = '''http://dalapi/v1/get?Keyword=BloombergExchangeListings&yml=\\\\iis\\KHAN_Exports\\share\\zulfi.yml&StartDate='2012-01-01'&EndDate='2012-06-30'&ExchangeCode=\'NDQ\''''

# URL = '''http://dalapi/v1/get?Keyword=TradingCalendarByFinancialCenter&yml=\\\\iis\\KHAN_Exports\\share\\zulfi.yml&FinancialCenterCode=\'NDQ\''''
# URL = '''http://dalapi/v1/get?Keyword=RestrictedSymbolList&yml=\\\\iis\\KHAN_Exports\\share\\zulfi.yml'''

# URL = "http://dalapi/v1/get?Keyword=TradingHoursByFinancialCenter&yml=\\\\iis\\KHAN_Exports\\share\\zulfi.yml&FinancialCenterCode='NDQA'&Date='2016-06-08'"

# URL = "http://dalapi/v1/get?Keyword=FactorLoadingByFIGI&yml=\\\\iis\\KHAN_Exports\\share\\zulfi_FactorModel.yml&FactorModelCode='AXUS3-MH'&StartDate='2016-09-02'&EndDate='2016-09-02'&Symbols=dict(Symbology='CFIGI',IDs=('BBG000B9XRY4',))"

# URL = '''http://dalapi/v1/get?Keyword=RestrictedSymbolList&ultrajson='True'&yml=\\\\iis\\KHAN_Exports\\share\\zulfi.yml'''


# URL = "http://dalapi/v1/get?Keyword=BloombergExchangeCodes&yml=\\\\fountainhead\\bam\\DataAPI_Config\\Shared\\Trading.yml"

# URL = "http://dalapi/v1/get?Keyword=SecurityRiskAttributeByFIGI&yml=\\\\iis\\KHAN_Exports\\share\\zulfi_FactorModel.yml&FactorModelCode='AXUS3-MH'&StartDate='2016-09-19'&EndDate='2016-09-19'&Symbols=dict(Symbology='CFIGI',IDs=('BBG000B9XRY4',))"

# URL = '''http://dalapi/v1/get?Keyword=BloombergExchangeCodes&yml=\\\\iis\\KHAN_Exports\\share\\zulfi.yml'''

# URL = "http://dalapi/v1/get?Keyword=IndicatorValues&yml=\\\\fountainhead\\bam\\DataAPI_Config\\Shared\\Trading.yml&data_environment=UAT&StartDate='2016-05-02'&EndDate='2016-05-02'&Symbols=dict(Symbology='CFIGI',IDs=None)"

# URL = '''http://dalapi/v1/get?Keyword=FactSetFundamentalBasicDerLTMByRegion&yml=\\\\Fountainhead\\BAM\\DataAPI_Config\\Shared\\FactSet.yml&StartDate='2016-06-29'&EndDate='2016-06-29'&Symbols=dict(Symbology='CUSIP',IDs=('459200101',))&Region=\'US\''''

# URL = "http://dal-app-dev:5005/v1/get?Keyword=FinancialCenterCodes&yml=\\\\fountainhead\\bam\\DataAPI_Config\\Shared\\Trading.yml"

# URL = "http://dal-app-dev:5005/v1/get?Keyword=TradingCalendarByFinancialCenter&yml=\\\\fountainhead\\bam\\DataAPI_Config\\Shared\\Trading.yml&FinancialCenterCode='LI'"

# URL = "http://dalapi/v1/get?Keyword=TradingHoursByBloombergExchange&yml=\\\\fountainhead\\bam\\DataAPI_Config\\Shared\\Trading.yml&Date='2016-07-08'&BloombergExchangeCode='LI'"

# URL = "http://dalapi/v1/get?Keyword=TradingHoursByExchange&yml=\\\\fountainhead\\bam\\DataAPI_Config\\Shared\\Trading.yml&Date='2016-07-08'&Exchange='London Stock Exchange'"

# URL = '''http://dalapi/v1/get?Keyword=BLP_US_EOD_CLSADJ&yml=\\\\iis\\KHAN_Exports\\share\\zulfi.yml&StartDate='2013-01-31'&EndDate='2013-01-31'&Symbols=dict(Symbology='CFIGI',IDs=('BBG000C6K6G9',))'''

# URL = "http://dalapi/v1/get?Keyword=BLP_US_EOD_CLSUNADJ&yml=\\\\iis\\KHAN_Exports\\share\\zulfi.yml&StartDate='2016-10-12'&EndDate='2016-10-12'&Symbols=dict(Symbology='CFIGI',IDs=('BBG000GZQ728',))"

# URL = '''http://dalapi/v1/get?Keyword=FactSetDailyMarketCapPriceAndVolumeNonUS&yml=\\\\Fountainhead\\BAM\\DataAPI_Config\\Shared\\FactSet.yml&StartDate='2016-10-04'&EndDate='2016-10-05'&Symbols=dict(Symbology='CFIGI',IDs=('BBG000C6K5W3',))'''

# URL = "http://dalapi/v1/get?Keyword=WSHCalendarEvent&yml=\\\\fountainhead\\bam\\DataAPI_Config\\Shared\\WallStreetHorizons.yml&StartDate='2012-01-01'&EndDate='2017-12-31'&Symbols=dict(Symbology='CFIGI',IDs=('BBG000BPH459',))"

# # BAIDU: BBG000QXWHD1
# # SOCL: BBG00283C2R3U
# # TCEHY: BBG000SGZ3Z0
# URL = '''http://dalapi/v1/get?Keyword=FactSetFundamentalBasicDerLTMByRegion&yml=\\\\Fountainhead\\BAM\\DataAPI_Config\\Shared\\FactSet.yml&StartDate='2016-10-12'&EndDate='2016-10-12'&Symbols=dict(Symbology='CFIGI',IDs=('BBG000B9XRY4',))&Region=\'US\''''

# URL = "http://dalapi/v1/get?Keyword=IndicatorValues&yml=\\\\fountainhead\\bam\\DataAPI_Config\\Shared\\SSAC.yml&data_environment=UAT&StartDate='2016-05-09'&EndDate='2016-05-09'&Symbols=dict(Symbology='CFIGI',IDs=None)"

# URL = "http://dalapi/v1/get?Keyword=EstimizeInstruments&yml=\\\\fountainhead\\bam\\DataAPI_Config\\Shared\\Estimize.yml&Symbols=dict(Symbology='CFIGI',IDs=('BBG000B9XRY4',))"

# URL = '''http://dalapi/v1/get?Keyword=OneTickSecurityMappings&Date='2016-09-15'&ultrajson='True'&yml=\\\\fountainhead\\bam\DataAPI_Config\\Shared\\Mappings.yml&Symbols=dict(Symbology='CFIGI',IDs=('BBG009S39JX6', 'BBG009S3NB30',))'''

# URL = "http://dalapi/v1/get?Keyword=FordEquityResearchUSStandard&yml=\\\\fountainhead\\bam\\DataAPI_Config\\Shared\\Ford.yml&StartDate='2016-10-11'&EndDate='2016-10-11'&Symbols=dict(Symbology='CFIGI',IDs=('BBG000DMBXR2',))"

# URL = '''http://dalapi/v1/get?Keyword=BloombergUnadjustedPricesNonUS&StartDate='2016-10-13'&EndDate='2016-10-20'&ultrajson='True'&yml=\\\\fountainhead\\BAM\\DataAPI_Config\\Shared\\Prices.yml&Symbols=dict(Symbology='CFIGI',IDs=('BBG002Z340D1',))'''

# URL = '''http://dalapi/v1/get?Keyword=OneTickSecurityMappingsNonUS&StartDate='2016-09-15'&EndDate='2016-09-20'&ultrajson='True'&yml=\\\\fountainhead\\bam\DataAPI_Config\\Shared\\Mappings.yml&Symbols=dict(Symbology='CFIGI',IDs=('BBG002Z340D1',))'''

# URL = "http://dalapi/v1/get?Keyword=EstimizeReleases&yml=\\\\fountainhead\\bam\\DataAPI_Config\\Shared\\Estimize.yml&fiscal_year=2016&fiscal_quarter=1&Symbols=dict(Symbology='CFIGI',IDs=None)"

# URL = '''http://dalapi/v1/get?KeywUord=CoreModelUSLargeCap&yml=\\\\Fountainhead\\BAM\\DataAPI_Config\\Shared\\Empirical.yml&StartDate='2016-10-01'&EndDate='2016-10-12'&Symbols=dict(Symbology='CFIGI',IDs=('BBG009S39JX6',))'''

# URL = "http://dalapi/v1/get?Keyword=IndicatorValues&yml=\\\\fountainhead\\bam\DataAPI_Config\\Shared\\SSAC.yml&data_environment=UAT&StartDate='2016-05-02'&EndDate='2016-05-02'&&Symbols=dict(Symbology='CFIGI',IDs=None)"

# URL = '''http://dalapi/v1/get?Keyword=OneTickSecurityMappings&StartDate='2016-09-15'&EndDate='2016-09-15'&ultrajson='True'&yml=\\\\fountainhead\\bam\DataAPI_Config\\Shared\\Mappings.yml&Symbols=dict(Symbology='CFIGI',IDs=('BBG000B9XRY4', 'BBG009S3NB30', 'BBG000BLNNH6', 'BBG000MM2P62', 'BBG000BPH459',))'''

# URL = '''http://dalapi/v1/get?Keyword=OneTickSecurityMappings&StartDate='2016-09-15'&EndDate='2016-09-15'&ultrajson='True'&yml=\\\\fountainhead\\bam\DataAPI_Config\\Shared\\Mappings.yml&Symbols=dict(Symbology='CFIGI',IDs=('BBG000B9XRY4', 'BBG009S3NB30', 'BBG000BLNNH6', 'BBG000MM2P62', 'BBG000BPH459','BBG009S39JX6',))'''

# URL = '''http://dalapi/v1/get?ultrajson='True'&yml=\\\\fountainhead\\bam\\DataAPI_Config\\Shared\\Trading.yml&StartDate='2015-07-31'&EndDate='2015-07-31'&EXCH='USXNMS'&Keyword=OneTickExchangeListings'''

# URL = "http://dalapi/v1/get?Keyword=BloombergSectors&yml=\\\\fountainhead\\bam\DataAPI_Config\\Shared\\Bloomberg.yml&StartDate='1960-01-01'&EndDate='2016-08-24'&Symbols=dict(Symbology='CFIGI',IDs=('BBG000B9XRY4',))"



# URL = '''http://dalapi/v1/get?Keyword=CoreModelUSLargeCap&yml=\\\\Fountainhead\\BAM\\DataAPI_Config\\Shared\\Empirical.yml&StartDate='2016-01-05'&EndDate='2016-01-31'&Symbols=dict(Symbology='CFIGI',IDs=('BBG000BLNNH6',))'''

# URL = '''http://dalapi/v1/get?Keyword=TradingCalendarByFinancialCenter&yml=\\\\iis\\KHAN_Exports\\share\\zulfi.yml&FinancialCenterCode=\'NDQ\''''

# URL = '''http://dalapi/v1/get?Keyword=OneTickSecurityMappings&StartDate='2011-06-01'&EndDate='2012-01-06'&ultrajson='True'&yml=\\\\Fountainhead\\BAM\\DataAPI_Config\\prod\\1.0\\Shared\\Mappings.yml&Symbols=dict(Symbology='CFIGI',IDs=('BBG000BBV9N3',))'''

# URL = '''http://dalapi/v1/get?Keyword=MarkitQsgFactorModels&yml=\\\\Fountainhead\\BAM\\DataAPI_Config\\Shared\\MarkitQsg.yml'''

# URL = '''http://dalapi/v1/get?Keyword=MarkitQsgFactorModelTimeSeries&yml=\\\\Fountainhead\\BAM\\DataAPI_Config\\Shared\\MarkitQsg.yml&StartDate='2016-11-02'&EndDate='2016-11-04'&FactorModel='HG_Model_US'&Symbols=dict(Symbology='CFIGI',IDs=None)'''

# URL = '''http://dalapi/v1/get?Keyword=CompanyIndustries&ultrajson='True'&yml=\\\\fountainhead\\bam\\DataAPI_Config\\Shared\\Revere.yml&Symbols=dict(Symbology='CFIGI',IDs=None)&Region=\'US\''''

# URL = '''http://dalapi/v1/get?Keyword=EntScrRelationshipsSource&yml=\\\\Fountainhead\\BAM\\DataAPI_Config\\prod\\1.1.6\\Shared\\SupplyChain.yml&Date='2016-11-16'&Symbols=dict(Symbology='CFIGI',IDs=None)'''

# URL = '''http://dalapi/v1/get?ultrajson='True'&StartDate='2012-03-01'&Symbols=dict(Symbology='CFIGI',IDs=None)&EndDate='2012-03-01'&yml=\\\\Fountainhead\\BAM\\DataAPI_Config\\prod\\1.1.6\\Shared\\Empirical.yml&Fields=['FreeCashFlowYield','MarketCapitalizationInBillion','Valuation','Return_YTD','ForwardPE','MarketReaction','Industry','Subsector','CoreModelRank','Price','CapitalDeployment','Sector','SectorAttractiveness','EarningsQualityAndTrend','Return_MT','SYMBOL']&Keyword=CoreModelUSLargeCap'''

# URL = '''http://dalapi/v1/get?Keyword=CoreModelUSLargeCap&yml=\\\\Fountainhead\\BAM\\DataAPI_Config\\prod\\1.1.6\\Shared\\Empirical.yml&StartDate='2016-11-03'&EndDate='2016-11-03'&Symbols=dict(Symbology='CFIGI',IDs=None)'''

# URL = '''http://dalapi/v1/get?Keyword=BloombergAdjustedPrices&yml=\\\\Fountainhead\\BAM\\DataAPI_Config\\prod\\1.1.6\\Shared\\Prices.yml&StartDate='2015-09-01'&EndDate='2015-09-10'&Symbols=dict(Symbology='CFIGI',IDs=('BBG000BPH459',))'''

# # URL = '''http://dalapi/v1/get?Keyword=BLP_US_EOD_CLSADJ&yml=\\\\iis\\KHAN_Exports\\share\\zulfi.yml&StartDate='2015-09-01'&EndDate='2015-09-10'&Symbols=dict(Symbology='CFIGI',IDs=('BBG000BPH459',))'''

# URL = '''http://dalapi/v1/get?Keyword=MarkitQsgFactorModelTimeSeries&yml=\\\\Fountainhead\\BAM\\DataAPI_Config\\Shared\\MarkitQsg.yml&StartDate='2015-08-12'&EndDate='2016-08-31'&FactorModel='US_SurpriseAnalyst_USTotalCap'&Symbols=dict(Symbology='CFIGI',IDs=('BBG000BPH459',))'''

# URL = '''http://dalapi/v1/get?ultrajson='True'&FactorModel='US_VMA_Model'&StartDate='2015-08-31'&Symbols=dict(Symbology='CFIGI',IDs=None)&EndDate='2016-05-02'&yml=\\\\Fountainhead\\BAM\\DataAPI_Config\\prod\\1.1.6\\Shared\\MarkitQsg.yml&Fields=['SYMBOL','Factor','Exposure']&Keyword=MarkitQsgFactorModelTimeSeries'''

# url1 = '''http://dalapi/v1/get?Keyword=HolidayCalendarByCountryCodeAndMICExchangeCode&yml=\\\\Fountainhead\\BAM\\DataAPI_Config\\prod\\1.1.6\\Shared\\Trading.yml&MICExchangeCode='XLON'&CountryCode='GB'&ultrajson=\'True\''''

# URL = '''http://dalapi/v1/get?Keyword=HolidayCalendarByCountryCodeAndMICExchangeCode&yml=\\\\Fountainhead\\BAM\\DataAPI_Config\\prod\\1.1.6\\Shared\\Trading.yml&MICExchangeCode='AMTS'&CountryCode='GB'&ultrajson=\'True\''''

# URL = '''http://dalapi/v1/get?Keyword=MarkitDataExplorersBorrowFees&yml=\\\\Fountainhead\\BAM\\DataAPI_Config\\prod\\1.1.26\\Shared\\Trading.yml&Date='2017-01-24'&Symbols=dict(Symbology='CFIGI',IDs=('BBG000BLNZL4',))'''

# URL = '''http://dalapi/v1/get?Keyword=FactorReturn&yml=\\\\Fountainhead\\BAM\\DataAPI_Config\\prod\\1.1.8\\Shared\\Axioma.yml&StartDate='2016-11-01'&EndDate='2016-11-06'&FactorModelCode='AXUS3-MH\''''

# URL = '''http://dalapi/v1/get?Keyword=TradingHoursByCountryCodeAndMICExchangeCode&yml=\\\\fountainhead\\bam\\DataAPI_Config\\Shared\\Trading.yml&CountryCode='GB'&MICExchangeCode='XLON'&StartDate='2016-12-13'&EndDate=\'2016-12-13\''''

URL = '''http://dalapi/v1/get?Keyword=Factor&yml=\\fountainhead\bam\DataAPI_Config\uat\OneTickEU\Axioma.yml&FactorModelCode=\'AXEU21-MH\''''

# URL = '''http://dalapi/v1/get?Keyword=Factor&FactorModelCode='AXEU21-MH'&yml=\\fountainhead\bam\DataAPI_Config\uat\OneTickEU\Axioma.yml'''
# URL = '''http://dalapi/v1/get?Keyword=Factor&yml=\\fountainhead\bam\DataAPI_Config\uat\OneTickEU\Axioma.yml&FactorModelCode=\'AXEU21-MH\''''

# http://dalapi/v1/get?Keyword=BloombergAdjustmentFactors&yml=\\Fountainhead\BAM\DataAPI_Config\prod\1.1.8\Shared\Bloomberg.yml&StartDate='2016-11-03'&EndDate='2016-11-03'&Symbols=dict(Symbology='CFIGI',IDs=('BBG000B9XRY4'),)
# "ultrajson='True'&FactorModel='US_SurpriseAnalyst_USTotalCap'&StartDate='2015-08-12'&Symbols=dict(Symbology='CFIGI',IDs=None)&EndDate='2015-08-12'&yml=\\\\Fountainhead\\BAM\\DataAPI_Config\\prod\\1.1.6\\Share...

def runRequest(localUrl):
    response_data = requests.get(localUrl, auth = HttpNtlmAuth("FOUNTAINHEAD\\zmalik", "BetterPa55"))
    jsonStr = response_data.text

    return json.loads(jsonStr)

data = runRequest(URL)

# numTimestamps = len(data["data"]["GMT_OPEN"])
#"BBG000CD9K76"
# NYSE = UA 0x00000000002b98fc "BBG009S3NB30"

# factorModels = data["data"]["FactorModelName"]
# f = open("factor_models.txt", "w")
# for i in range(len(factorModels)):
#     factorModel = factorModels[i];
#     fmUrl = QSG_URL.replace("$$$", factorModel)
    
#     s = str.format("### %d - %s ###\n" % (i, factorModel))
#     print(s)
#     f.write(s)
#     fmData = runRequest(fmUrl) 
    
#     if "data" not in fmData or "Factor" not in fmData["data"]:
#         f.write("########\n")
#         continue 
    
#     factors = fmData["data"]["Factor"]
    
#     for factor in factors:
#         f.write(factor + "\n")
        
# f.close()
         

for i in range(numTimestamps):
    dts = DT.fromtimestamp(data["data"]["GMT_OPEN"][i] / 1000000000)
    dts1 = DT.fromtimestamp(data["data"]["GMT_CLOSE"][i] / 1000000000)
    print("%s - %s" % (str(dts), str(dts1)))

for value in values:
    dataValue = {}

    for i in range(len(result.columns)):
        if self.dataOps and result.columns[i] in self.dataOps:
            import parse
            parsedResult = parse.parse(self.dataOps[result.columns[i]], value[i])

            for key in parsedResult.named:
                dataValue[key] = parsedResult[key]
        else:
            dataValue[result.columns[i]] = value[i]
if "TIMESTAMP" in data["data"]:
    for i, ts in enumerate(data["data"]["TIMESTAMP"]):
        ts = pandas.to_datetime(ts)
        print(ts) 
        # print("%s - %s - " % (ts, data['data']["CFIGI"][i]))
pp = pprint.PrettyPrinter(indent = 4)
pp.pprint(data)

# countries = {
#     "DE": 1,
#     "GB": 1,
#     "IT": 1,
#     "IS": 1,
#     "MC": 1,
#     "PL": 1,
#     "PT": 1,
#     "ES": 1,
#     "BE": 1,
#     "DK": 1,
#     "FI": 1,
#     "FR": 1,
#     "GR": 1,
#     "HU": 1,
#     "IE": 1,
# }

# exchanges = {}
# string = ""
# isoCountries = data["data"]["ISOCOUNTRY"]
# for index, isoCountry in enumerate(isoCountries):
#     if isoCountry in countries:
#         if isoCountry not in exchanges:
#             exchanges[isoCountry] = [data["data"]["CODE"][index]]
#         else:
#             exchanges[isoCountry].append(data["data"]["CODE"][index])
#         # string += ", " + isoCountry + " - " + data["data"]["CODE"][index]
        
# for key, value in exchanges.items():
#     string += key + " : "
#     for v in value:
#         string += v + ", "
# print(string)












# exchanges = '''DE:Aut,BbS,BnB,BnI,DdB,DsB,DsI,DsS,EEF,EUC,EUD,EUE,EUF,EUG,EUH,EUI,EUJ,EUK,EUL,EUM,EUN,EUO,EUQ,EUS,EUU,EUV,EUY,FrB,FrI,FrS,FrX,HbB,HbI,HbS,HnS,HSE,MuB,MuI,MuS,PoB,PoI,SOA,SOD,SOE,SOF,SOG,SOH,SOI,SOJ,SOK,SOL,SOM,SON,SOO,SOP,SOQ,SOR,SOT,SOZ,SuS,SxB,SxI,TgS,ZmB |
# 			IE:DuB,DuI,DuS,DuX |
# 			IT:ETL,GME,GMF,GMG,HMT,MiB,MiF,MiI,MiS,MiT,MiU,MiX,MiY,RmB,RmI,ToB,ToI |
# 			BE:BrB,BrI,BrS,BrX,Ecl,FDR |
# 			GR:AtB,AtI,AtS,AtX,HDT |
# 			MC:MzB |
# 			PT:LsB,LsI,LsS,LsX,OMP |
# 			ES:AzB,BlB,BlI,EbB,MdB,MdI,MdS,MdX,MEF,MEG,MEH,MEI,MqB,ObB,PxB,SvB,SvI,VlB,VlI |
# 			DK:CoB,CoS,CoX,NPA,NPW | 
# 			GB:AQK,AQL,AQM,AQN,AQO,AQP,AQQ,AQT,AQU,AQV,AQW,AQY,AQZ,BAG,BAH,BAI,BAJ,BAK,BAL,BAN,BAO,BAP,BAQ,BAR,BAU,BAV,BAW,BAY,BBP,BBQ,BBR,BBT,BBU,BBV,BBW,BOT,BTR,CMU,CMV,CMW,CMY,CMZ,CRE,CRG,CRP,CRU,EDE,EDV,EDW,EDY,EMA,EMC,EMD,EME,EMG,EMH,EMI,EMJ,EMK,EML,EMM,EMN,EMO,EMP,EMQ,EMR,EMS,EMU,EMV,EMW,EMY,EMZ,EQO,EQP,EQQ,EQR,EQS,EQT,EQU,EQV,EQW,EQY,EQZ,ICC,IcR,IcS,IcT,IPE,IPF,IPG,IPH,IPK,ISA,ISC,ISD,ISE,ISF,LBE,LBM,LBN,LBO,LBU,LCH,LIF,LIG,LIH,LIJ,LIK,LIL,LIM,LIN,LIO,LIP,LIQ,LIR,LIT,LIU,LIV,LIW,LME,LMP,LnB,LnO,LnS,LnX,LnY,LPM,LPN,LPO,MAT,MBP,MBQ,NIC,NYK,NYR,PuS,RFR,ScC,SML,SMM,SMN,SMO,SMP,SMQ,SMR,SMS,SMT,SMU,SMV,SMW,SMX,SMY,SMZ,SXC,SXD,SXE,SXF,SXG,SXH,SXJ,SXK,SXL,SXM,SXN,SXO,SXP,SXQ,SXR,TQE,TQG,TQH,TQI,TQJ,TQK,TQL,TQM,TQN,TQO,TQP,TQQ,TQR,TQS,TQT,TQU,TQV,TQW,TQY,TQZ,TrR,UKM,UKP,WMR |
# 			FR:FRP,NBM,PaB,PaI,PaS,PaX,PNH |
# 			PL:EMF,EMT,MPL,PPF,WaB,WaS,WaX | 
# 			IS:RkB,RkS,RkX'''
# exch = []
# exchanges           = exchanges.split("|")
# assert exchanges, "Must specify which exchanges to load the calendar for!"

# def test(*args):
#     fstring = str.format(*args)
#     print("PYTHON - " + fstring)
    
# test("This is a test: %s - %d" % ("123", 456))
    

# for exchange in exchanges:
#     country, exList = exchange.split(":")
#     exList = exList.split(",")
#     for ex in exList:
#         exch.append(ex.strip())
# exchanges = value.split("|")
