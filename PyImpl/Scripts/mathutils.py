### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import numpy as np
from pandas import DataFrame

def normalise(x):

    if np.sum(np.abs(x)) == 0:
        return x
    else:
        return x / np.sqrt(np.sum(x**2,axis=0))

# the following projects v onto u
def proj(u,v):
    if np.sum(np.abs(u)) == 0:
        return u
    else:
        return (np.dot(v,u)/np.dot(u,u))*u

    
def gram_schmidt(x):
    xPrime = np.copy(x)
    if sum(sum(np.isnan(xPrime))) > 0:
        # then we have a stock for which the exposure factors are undefined
        xPrimeDF = DataFrame(xPrime)
        xPrimeDF.fillna(xPrimeDF.mean(), inplace=True)
        xPrime = xPrimeDF.values
        
    xPrime[:,0] = normalise(xPrime[:,0])
    numColumns = x.shape[1]
    for i in range(1, numColumns):
        pastProjections = np.array([proj(xPrime[:,j], xPrime[:,i]) for j in range(i)]).T
        sumPastProjections = np.sum(pastProjections, axis=1).T
        xPrime[:,i] = xPrime[:,i] - sumPastProjections
        xPrime[:,i] = normalise(xPrime[:,i])
    return xPrime
