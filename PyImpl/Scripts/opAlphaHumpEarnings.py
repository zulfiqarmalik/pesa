### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# Hammad Khan
#######################################################################################################################################
############ CopyRight Hammad Khan ####################################################################################################
#### No Part of psim or associated code or tools can be reproduced or used without prior written permission of Hammad Khan ############
#### This Header has to be used in all permitted authorized usage of psim #############################################################
#######################################################################################################################################






import sys
import traceback
import csv
from datetime import datetime 
import numpy as np
import warnings

try:
    import Framework
    import PyUtil
    from PyBase import PyOperation

    class OpAlphaHumpEarnings(PyOperation):
        def __init__(self, baseObject):
            super().__init__(baseObject, "OpAlphaHumpEarnings")
            self.hist_alpha = []
            self.hist_pnl  = []
            self.ctr = 0
        
        def run(self, rt):
            try:
                
                nDaysS       = int(self.config().getString("daystoNextED", "10"))
                alphaHump    = float(self.config().getString("alphaHump", "0.1"))
                alphaData   = self.alphaData()
                alphaResult = self.alpha()
                # typecasting alpha to numpy array 
                # Note: self.alpha() is not an array initially. 
                # Need do np.asarray(.rowArray(0)) to make it become an array
                alpha       = self.alpha().rowArray(0)
            
                
                di          = rt.di
                d1          = rt.di - alphaData.delay
                
                stats       = self.stats()
                ydata       = self.yesterdayData()
                yesterdayDateTime = BamUtil.intToDate(rt.getDate(d1))
                yesterdayDate     = yesterdayDateTime.date()
                
                numStocks   = alphaResult.cols()
                universe    = alphaData.universe
                                
                alpha_array = np.asarray(alpha)

                daysToEarnings = rt.dataRegistry.floatData(universe,"derivedData.daysToEarnings")
                daysToEarnings_mat = BamUtil.floatToNPMatrix(daysToEarnings)

                warnings.filterwarnings('ignore')


                for ii in range(numStocks):
                    sec = universe.security(di, ii)
                    bbTicker = sec.bbTickerStr()

                    if universe.isValid(di,ii):
                        daysToNextED = daysToEarnings_mat[d1,ii]
                        if daysToNextED >= 0 and daysToNextED <= nDaysS:
                            newalpha = (alpha_array[ii] - np.nanmedian(alpha_array)) * (1-alphaHump) + np.nanmedian(alpha_array) 
                            alphaResult.setValue(0,ii,newalpha)

                #print(numStocks)
                #self.debug

            except Exception as e:
                print(traceback.format_exc())

except Exception as e:
    print(traceback.format_exc())




