### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# Hammad Khan
#######################################################################################################################################
############ CopyRight Hammad Khan ####################################################################################################
#### No Part of psim or associated code or tools can be reproduced or used without prior written permission of Hammad Khan ############
#### This Header has to be used in all permitted authorized usage of psim #############################################################
#######################################################################################################################################



import sys
import traceback
from datetime import datetime as DT
import numpy as np
import mathutils as mu
from collections import defaultdict
import abc

NODE_MAXDAYS = 255

class Ops(object):

    @staticmethod
    def inv_sector(rt, u, d1, name):
        d1_ind = d1 if 'revere' in name else 0
        size = u.size(d1_ind)
        sector = Framework.StringData.script_cast(rt.dataRegistry.getData(u, name))
        sec = { ii : sector.getValue(d1_ind, ii) for ii in range(size) }
        inv_sector = defaultdict(list)
        for k in sec:
            inv_sector[sec[k]].append(k)
        return inv_sector

    @staticmethod
    def nans(size):
        alpha = np.empty((size,))
        alpha[:] = np.nan
        return alpha

    @staticmethod
    def __shrink(d):
        if len(d) > 1:
            mult = 1.0 / (len(d) - 1.0)
            for key in d:
                d[key] *= mult 
                d[key] -= 0.5

    @staticmethod
    def __assign_range(d, s, lastidx, curridx):
        value = 0.5 * (lastidx + curridx - 1)
        for j in range(lastidx, curridx):
            d[s[j][1]] = value

    @staticmethod
    def __rank_dict(d):
        if not d:
            return
        s = sorted( [ ( d[ii], ii ) for ii in d ], key = lambda x : x[0] )
        lastval = s[0][0]
        lastidx = 0
        for i in range(1, len(s)):
            currval = s[i][0]
            if currval != lastval:
                Ops.__assign_range(d, s, lastidx, i)
                lastval = currval
                lastidx = i
        Ops.__assign_range(d, s, lastidx, len(s))
        Ops.__shrink(d)

    @staticmethod
    def __skew(x, b):
        return x * (b + 1) / (b * x + 1)

    @staticmethod
    def __getBfromA(a):
        if a == 0.0:
            return 0.0
        elif a > 10:
            return np.exp(10) - 1
        elif a < -10:
            return np.exp(-10) - 1
        else:
            return np.exp(a) - 1    

    @staticmethod
    def __skew_dict(d, b):
        for ii in d:
            d[ii] = -0.5 + Ops.__skew(d[ii] + 0.5, b)

    NAMES = { 'sector' : 'classification.bbSector', 
    'industry' : 'classification.bbIndustryGroup', 
    'country' : 'secData.countryCode3',
    'subindustry' : 'classification.bbIndustrySubgroup' }

    @staticmethod
    def group_rank(alpha, inv_sec, a = 0):
        for s in inv_sec:
            l = inv_sec[s]
            d = { ii : alpha[ii] for ii in l if np.isfinite(alpha[ii]) }
            Ops.__rank_dict(d)
            if a:
                Ops.__skew_dict(d, Ops.__getBfromA(a))
            for ii in d:
                alpha[ii] = d[ii]

    @staticmethod
    def __zscore_dict(d):
        if not len(d):
            return
        s = 0
        ss = 0
        for _, v in d.items():
            s += v
            ss += v * v
        s /= len(d)
        ss /= len(d)
        ss -= s * s
        if ss < 1e-5:
            for k in d:
                d[k] = 0
            return
        ss = np.sqrt(ss)
        for k in d:
            d[k] -= s
            d[k] /= ss

    @staticmethod
    def group_zscore(alpha, inv_sec):
        for s in inv_sec:
            l = inv_sec[s]
            d = { ii : alpha[ii] for ii in l if np.isfinite(alpha[ii]) }
            Ops.__zscore_dict(d)
            for ii in d:
                alpha[ii] = d[ii]

    @staticmethod
    def fill_valids_with_mean(alpha, size, d, u):
        m = 0
        n = 0
        indices = []
        for ii in range(size):
            if np.isfinite(alpha[ii]):
                m += alpha[ii] 
                n += 1
            elif u.isValid(d, ii):
                indices.append(ii)
        if not n:
            return
        m /= n
        if np.abs(m) < 1e-5:
            m = 0
        for ii in indices:
            alpha[ii] = m

class Node(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self, rt, obj):
        self.rt = rt
        self.u = obj.alphaData().universe
        self.children = []
        self.values = {}
        self.size = self.u.size(self.rt.di)
        
    def addChild(self, child):
        self.children.append(child)
        
    def init(self, d1, myLB = 0):
        self.myLB = myLB
        for child in self.children:
            child.init(d1, self.myLB + self.childLB)
        self.d1 = d1
        for d in range(d1 - self.myLB, d1 + 1):
            self.values[d] = self.calc(d)

    def value(self, d, ii):
        if d > self.d1:
            assert d == self.d1 + 1, 'd != self.d1 + 1'
            self.d1 = d
            self.values[d] = self.calc(d)
            if len(self.values) > self.myLB + 1:
                del self.values[min(self.values.keys())]
            assert len(self.values) <= self.myLB + 1, 'self.values.size() > self.myLB + 1'
        return self.values[d][ii]

    @abc.abstractmethod
    def calc(self, d):
        pass

    def valid(self, d, ii):
        return self.u.isValid(d, ii)

class NodeData(Node):

    def __init__(self, rt, obj, name):
        Node.__init__(self, rt, obj)
        self.childLB = 0
        self.childNum = 0

        self.data = rt.dataRegistry.floatData(self.u, name)

    def calc(self, d):
        size = self.size
        a = Ops.nans(self.size)
        for ii in range(size):
            if self.valid(d, ii):
                a[ii] = self.data[d, ii]
        Ops.fill_valids_with_mean(a, size, d, self.u)
        return a

class NodeCSOp(Node):

    def __init__(self, rt, obj, name, group, skew = 0):
        Node.__init__(self, rt, obj)
        self.childLB = 0
        self.childNum = 1

        self.name = name
        assert (group == 'none' or group in Ops.NAMES), 'Unknown group name'
        self.group = '' if group == 'none' else Ops.NAMES[group]
        self.skew = int(skew)


    def calc(self, d):
        size = self.size
        a = Ops.nans(size)
        child = self.children[0]
        for ii in range(size):
            if self.valid(d, ii):
                a[ii] = child.value(d, ii)
        if self.group == '':
            inv_sec = { '' : list(range(size)) }
        else:
            inv_sec = Ops.inv_sector(self.rt, self.u, d, self.group)
        if self.name == 'zscore':
            Ops.group_zscore(a, inv_sec)
        elif self.name == 'rank':
            Ops.group_rank(a, inv_sec, self.skew)
        else:
            raise Exception('Unknown CSOp')
        return a

class NodePair(Node):

    def __init__(self, rt, obj, name, op):
        Node.__init__(self, rt, obj)
        self.childLB = 0
        self.childNum = 2

        self.name = name
        self.op = op
        
    def calc(self, d):
        child0 = self.children[0]
        child1 = self.children[1]
        size = self.size
        vals0 = np.array([ child0.value(d, ii) for ii in range(size) ])
        vals1 = np.array([ child1.value(d, ii) for ii in range(size) ])
        dummy_inv_sec = { '' : list(range(size)) }
        if self.op == 'zscore':
            Ops.group_zscore(vals0, dummy_inv_sec)
            Ops.group_zscore(vals1, dummy_inv_sec)
        elif self.op == 'rank':
            Ops.group_rank(vals0, dummy_inv_sec)
            Ops.group_rank(vals1, dummy_inv_sec)
        elif self.op == 'simple':
            pass
        else:
            multskew = int(self.op)
            assert multskew in [-1, 1]
            Ops.group_rank(vals1, dummy_inv_sec)
            vals1 = 0.5 + multskew * vals1
        if self.name == 'plus':
            a = vals0 + vals1
        elif self.name == 'minus':
            a = vals0 - vals1
        elif self.name == 'mult':
            a = vals0 * vals1
        elif self.name == 'div':
            a = vals0 / vals1
        else:
            raise Exception('Unknown name: pairs')
        return a

class NodeConst(Node):

    def __init__(self, rt, obj, cmult):
        Node.__init__(self, rt, obj)
        self.childLB = 0
        self.childNum = 1

        self.cmult = float(cmult)

    def calc(self, d):
        child = self.children[0]
        size = self.size
        a = Ops.nans(size)
        for ii in range(size):
            a[ii] = child.value(d, ii)
        return self.cmult * a

class NodeDelay(Node):

    def __init__(self, rt, obj, days, trueDelay = False):
        Node.__init__(self, rt, obj)
        self.childLB = int(days)
        self.childNum = 1

        self.days = int(days)
        self.trueDelay = trueDelay

    def calc(self, d):
        child = self.children[0]
        size = self.size
        a = Ops.nans(size)
        ddelay = d - self.days
        if self.trueDelay:
            for ii in range(size):
                a[ii] = child.value(ddelay, ii)
        else:
            for ii in range(size):
                a[ii] = child.value(d, ii) - child.value(ddelay, ii)
        Ops.fill_valids_with_mean(a, size, d, self.u)
        return a

class NodeTS(Node):

    def __init__(self, rt, obj, days):
        Node.__init__(self, rt, obj)
        self.childLB = int(days)
        self.childNum = 1
        
        self.days = int(days)  
        self.valcache = {}
        self.last_updated = [-1] * self.size

    def updateCache(self, dd):
        child = self.children[0]
        self.valcache[dd] = [ child.value(dd, ii) for ii in range(self.size) ]

    def calc(self, d):
        if len(self.valcache) < self.days:
            assert not len(self.valcache)
            for dd in range(d - self.days, d + 1):
                self.updateCache(dd)
        else:
            assert len(self.valcache) == self.days
            self.updateCache(d)
        a = self.updateValues(d)
        del self.valcache[d - self.days]
        return a

    def fillNans(self, d, ii):
        for dd in reversed(range(d - self.days, d)):
            if not np.isfinite(self.valcache[dd][ii]):
                self.valcache[dd][ii] = self.valcache[dd + 1][ii]

    def updateValues(self, d):
        yd = d - 1
        size = self.size
        a = Ops.nans(size)
        for ii in range(size):
            if self.valid(d, ii): 
                if self.last_updated[ii] < yd:
                    self.fillNans(d, ii)
                    self._recalcII(d, ii)
                else:
                    self._calcII(d, ii)
                self.last_updated[ii] = d
                a[ii] = self._valueII(d, ii)
        Ops.fill_valids_with_mean(a, size, d, self.u)
        return a

class NodeTSstdev(NodeTS):

    def __init__(self, rt, obj, days):
        NodeTS.__init__(self, rt, obj, days)

        self.s = np.zeros(self.size)
        self.ss = np.zeros(self.size)

    def _calcII(self, d, ii):
        newx = self.valcache[d][ii]
        self.s[ii] += newx
        self.ss[ii] += newx * newx
        oldx = self.valcache[d - self.days][ii]
        self.s[ii] -= oldx
        self.ss[ii] -= oldx * oldx

    def _recalcII(self, d, ii):
        self.s[ii] = sum(self.valcache[j][ii] for j in range(d - self.days + 1, d + 1))
        self.ss[ii] = sum(self.valcache[j][ii] * self.valcache[j][ii] for j in range(d - self.days + 1, d + 1))

    def _valueII(self, d, ii):
        n = self.days
        x = self.s[ii] / n
        xx = self.ss[ii] / n
        xx -= x * x
        xx = np.sqrt(xx)
        res = (self.valcache[d][ii] - x) / xx
        return res

class NodeTSslope(NodeTS):

    def __init__(self, rt, obj, days):
        NodeTS.__init__(self, rt, obj, days)

        self.s = np.zeros(self.size)
        self.ss = np.zeros(self.size)
        self.st = np.zeros(self.size)

    def _calcII(self, d, ii):
        newx = self.valcache[d][ii]
        self.s[ii] += newx
        self.ss[ii] += newx * newx
        self.st[ii] += newx * (self.days + 1)
        oldx = self.valcache[d - self.days][ii]
        self.st[ii] -= self.s[ii]
        self.s[ii] -= oldx
        self.ss[ii] -= oldx * oldx

    def _recalcII(self, d, ii):
        self.s[ii] = sum(self.valcache[j][ii] for j in range(d - self.days + 1, d + 1))
        self.ss[ii] = sum(self.valcache[j][ii] * self.valcache[j][ii] for j in range(d - self.days + 1, d + 1))
        self.st[ii] = sum(self.valcache[j][ii] * (j + self.days - d) for j in range(d - self.days + 1, d + 1))

    def _valueII(self, d, ii):
        n = self.days
        if n < 2:
            return 0
        x = self.s[ii] / n
        xx = self.ss[ii] / n
        xx -= x * x
        xx = np.sqrt(xx)
        xy = self.st[ii] / n
        xy -= x * (n + 1) / 2
        yy = np.sqrt((n * n - 1) / 12)
        if xx < 1e-5 or yy < 1e-5:
            return 0
        res = xy / (xx * yy)
        if abs(res) > 1 + 1e-5 and ii == 0:
            print(res, self.st[ii], xy, xx, yy, x, self.d1, n, ii)
        return res    

class NodeTSdecay(NodeTS):

    def __init__(self, rt, obj, days):
        NodeTS.__init__(self, rt, obj, days)

        self.s = np.zeros(self.size)
        self.st = np.zeros(self.size)

    def _calcII(self, d, ii):
        newx = self.valcache[d][ii]
        self.s[ii] += newx
        self.st[ii] += newx * (self.days + 1)
        oldx = self.valcache[d - self.days][ii]
        self.st[ii] -= self.s[ii]
        self.s[ii] -= oldx

    def _recalcII(self, d, ii):
        self.s[ii] = sum(self.valcache[j][ii] for j in range(d - self.days + 1, d + 1))
        self.st[ii] = sum(self.valcache[j][ii] * (j + self.days - d) for j in range(d - self.days + 1, d + 1))

    def _valueII(self, d, ii):
        return self.st[ii]

class NodeAlpha(Node):

    def __init__(self, rt, obj):
        Node.__init__(self, rt, obj)
        self.childLB = 0
        self.childNum = 0

        self.obj = obj

    def calc(self, d):
        size = self.size
        a = Ops.nans(size)
        alpha = self.obj.alpha()
        for ii in range(size):
            a[ii] = alpha.getValue(0, ii)
        Ops.fill_valids_with_mean(a, size, d, self.u)
        return a

class NodeOp(Node):

    def __init__(self, rt, obj):
        Node.__init__(self, rt, obj)
        self.childLB = 0
        self.childNum = 1

        self.obj = obj

    def calc(self, d):
        size = self.size
        a = Ops.nans(size)
        child = self.children[0]
        for ii in range(size):
            a[ii] = -child.value(d, ii)
        return a


def nodeFactory(rt, obj, token):
    args = token.split(' ')
    args = [a.strip() for a in args]
    if args[0] == 'Data':
        n = NodeData(rt, obj, args[1])
    elif args[0] == 'Zscore':
        n = NodeCSOp(rt, obj, 'zscore', args[1])
    elif args[0] == 'Rank':
        n = NodeCSOp(rt, obj, 'rank', args[1], args[2])
    elif args[0] == 'Delay':
        n = NodeDelay(rt, obj, args[1])
    elif args[0] == 'Decay':
        n = NodeTSdecay(rt, obj, args[1])
    elif args[0] == 'Truedelay':
        n = NodeDelay(rt, obj, args[1], True)
    elif args[0] == 'Stdev':
        n = NodeTSstdev(rt, obj, args[1])
    elif args[0] == 'Slope':
        n = NodeTSslope(rt, obj, args[1])
    elif args[0] == 'Meandiff':
        n = NodeMeanDiff(rt, obj, args[1], args[2])
    elif args[0] == 'Pair':
        n = NodePair(rt, obj, args[1], args[2])
    elif args[0] == 'Const':
        n = NodeConst(rt, obj, args[1])
    elif args[0] == 'Alpha':
        n = NodeAlpha(rt, obj)
    elif args[0] == 'Op':
        n = NodeOp(rt, obj)
    else:
        raise Exception('Unknown expression')
    return n

def glueNodes(rt, obj, tokens):
    n = nodeFactory(rt, obj, tokens[0])
    tokens.pop(0)
    for i in range(n.childNum):
        n.addChild(glueNodes(rt, obj, tokens))
    return n

def evalString(rt, obj):
    tokens = obj.evalname.split(',')
    tokens = [ t.strip() for t in tokens ]
    return glueNodes(rt, obj, tokens)

try:
    import Framework
    import PyUtil
    from PyBase import PyOperation

    class OpEval(PyOperation):
        def __init__(self, baseObject):
            super().__init__(baseObject, 'OpEval')
            self.evalname = self.config().getString("evalname", "")
            if self.evalname == '':
                raise Exception('Empty name in OpEval')
        
        def run(self, rt):
            try:

                alphaData = self.alphaData()
                di = rt.di - alphaData.delay
                alphaResult = self.alpha()
                u = alphaData.universe
                numStocks = u.size(rt.di)

                if not hasattr(self, 'initNode'):
                    self.initNode = True
                    self.n = evalString(rt, self)
                    self.n.init(di)
                    
                for ii in range(numStocks):
                    if u.isValid(rt.di, ii):
                        #alphaResult.setValue(0, ii, ii)
                        alphaResult.setValue(0, ii, self.n.value(di, ii))

            except Exception as e:
                print(traceback.format_exc())

except Exception as e:
    print(traceback.format_exc())


