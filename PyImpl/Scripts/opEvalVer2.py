### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# Hammad Khan
#######################################################################################################################################
############ CopyRight Hammad Khan ####################################################################################################
#### No Part of psim or associated code or tools can be reproduced or used without prior written permission of Hammad Khan ############
#### This Header has to be used in all permitted authorized usage of psim #############################################################
#######################################################################################################################################



import sys
import traceback
from datetime import datetime as DT
import numpy as np
import mathutils as mu
from collections import defaultdict
import abc

NODE_MAXDAYS = 255

class Ops(object):

    @staticmethod
    def inv_sector(rt, u, d1, name):
        d1_ind = d1 if 'revere' in name else 0
        size = u.size(d1_ind)
        sector = Framework.StringData.script_cast(rt.dataRegistry.getData(u, name))
        sec = { ii : sector.getValue(d1_ind, ii) for ii in range(size) }
        inv_sector = defaultdict(list)
        for k in sec:
            inv_sector[sec[k]].append(k)
        return inv_sector

    @staticmethod
    def nans(size):
        alpha = np.empty((size,))
        alpha[:] = np.nan
        return alpha

    @staticmethod
    def __shrink(d):
        if len(d) > 1:
            mult = 1.0 / (len(d) - 1.0)
            for key in d:
                d[key] *= mult 
                d[key] -= 0.5

    @staticmethod
    def __assign_range(d, s, lastidx, curridx):
        value = 0.5 * (lastidx + curridx - 1)
        for j in range(lastidx, curridx):
            d[s[j][1]] = value

    @staticmethod
    def __rank_dict(d):
        if not d:
            return
        s = sorted( [ ( d[ii], ii ) for ii in d ], key = lambda x : x[0] )
        lastval = s[0][0]
        lastidx = 0
        for i in range(1, len(s)):
            currval = s[i][0]
            if currval != lastval:
                Ops.__assign_range(d, s, lastidx, i)
                lastval = currval
                lastidx = i
        Ops.__assign_range(d, s, lastidx, len(s))
        Ops.__shrink(d)

    @staticmethod
    def __skew(x, b):
        return x * (b + 1) / (b * x + 1)

    @staticmethod
    def __getBfromA(a):
        if a == 0.0:
            return 0.0
        elif a > 10:
            return np.exp(10) - 1
        elif a < -10:
            return np.exp(-10) - 1
        else:
            return np.exp(a) - 1    

    @staticmethod
    def __skew_dict(d, b):
        for ii in d:
            d[ii] = -0.5 + Ops.__skew(d[ii] + 0.5, b)

    NAMES = { 'sector' : 'classification.bbSector', 
    'industry' : 'classification.bbIndustryGroup', 
    'subindustry' : 'classification.bbIndustrySubgroup',
    'rsector' : 'revere.l1Name', 
    'rindustry' : 'revere.l2Name',
    'rsubindustry' : 'revere.l3Name',
    'country' : 'secData.countryCode3',
    }

    @staticmethod
    def group_rank(alpha, inv_sec, a = 0):
        for s in inv_sec:
            l = inv_sec[s]
            d = { ii : alpha[ii] for ii in l if np.isfinite(alpha[ii]) }
            Ops.__rank_dict(d)
            if a:
                Ops.__skew_dict(d, Ops.__getBfromA(a))
            for ii in d:
                alpha[ii] = d[ii]

    @staticmethod
    def __zscore_dict(d):
        if not len(d):
            return
        s = 0
        ss = 0
        for _, v in d.items():
            s += v
            ss += v * v
        s /= len(d)
        ss /= len(d)
        ss -= s * s
        if ss < 1e-5:
            for k in d:
                d[k] = 0
            return
        ss = np.sqrt(ss)
        for k in d:
            d[k] -= s
            d[k] /= ss

    @staticmethod
    def group_zscore(alpha, inv_sec):
        for s in inv_sec:
            l = inv_sec[s]
            d = { ii : alpha[ii] for ii in l if np.isfinite(alpha[ii]) }
            Ops.__zscore_dict(d)
            for ii in d:
                alpha[ii] = d[ii]

    @staticmethod
    def fill_valids_with_mean(alpha, size, d, u):
        m = 0
        n = 0
        indices = []
        for ii in range(size):
            if np.isfinite(alpha[ii]):
                m += alpha[ii] 
                n += 1
            elif u.isValid(d, ii):
                indices.append(ii)
        if not n:
            return
        m /= n
        if np.abs(m) < 1e-5:
            m = 0
        for ii in indices:
            alpha[ii] = m

class Node(object):
    __metaclass__ = abc.ABCMeta
    MAXLB = 0

    def __init__(self, rt, obj):
        self.rt = rt
        self.u = obj.alphaData().universe
        self.children = []
        self.values = {}
        self.size = self.u.size(self.rt.di)
        
    def addChild(self, child):
        self.children.append(child)
        
    def init(self, d1, myLB = 0):
        self.myLB = myLB
        if myLB > Node.MAXLB:
            Node.MAXLB = myLB
        for child in self.children:
            child.init(d1, self.myLB + self.childLB)
        self.d1 = d1
        for d in range(d1 - self.myLB, d1 + 1):
            self.values[d] = self.calc(d)

    def value(self, d, ii):
        if d > self.d1:
            assert d == self.d1 + 1, 'd != self.d1 + 1'
            self.d1 = d
            self.values[d] = self.calc(d)
            if len(self.values) > self.myLB + 1:
                del self.values[min(self.values.keys())]
            assert len(self.values) <= self.myLB + 1, 'self.values.size() > self.myLB + 1'
        return self.values[d][ii]

    @abc.abstractmethod
    def calc(self, d):
        pass

    def valid(self, d, ii):
        return self.u.isValid(d, ii)

class NodeData(Node):

    def __init__(self, rt, obj, name):
        Node.__init__(self, rt, obj)
        self.childLB = 0
        self.childNum = 0

        self.data = rt.dataRegistry.floatData(self.u, name)

    def calc(self, d):
        size = self.size
        a = Ops.nans(self.size)
        for ii in range(size):
            if self.valid(d, ii):
                a[ii] = self.data[d, ii]
        return a

class NodeFillMean(Node):

    def __init__(self, rt, obj):
        Node.__init__(self, rt, obj)
        self.childLB = 0
        self.childNum = 1

    def calc(self, d):
        size = self.size
        a = Ops.nans(self.size)
        child = self.children[0]
        for ii in range(size):
            if self.valid(d, ii):
                a[ii] = child.value(d, ii)
        Ops.fill_valids_with_mean(a, size, d, self.u)
        return a

class NodeCSOp(Node):

    def __init__(self, rt, obj, name, group, skew):
        Node.__init__(self, rt, obj)
        self.childLB = 0
        self.childNum = 1

        self.name = name
        assert (group == 'none' or group in Ops.NAMES), 'Unknown group name'
        self.group = '' if group == 'none' else Ops.NAMES[group]
        self.skew = int(skew)

    def calc(self, d):
        size = self.size
        a = Ops.nans(size)
        child = self.children[0]
        for ii in range(size):
            if self.valid(d, ii):
                a[ii] = child.value(d, ii)
        if self.group == '':
            inv_sec = { '' : list(range(size)) }
        else:
            inv_sec = Ops.inv_sector(self.rt, self.u, d, self.group)
        if self.name == 'zscore':
            Ops.group_zscore(a, inv_sec)
        elif self.name == 'rank':
            Ops.group_rank(a, inv_sec, self.skew)
        else:
            raise Exception('Unknown CSOp')
        return a

class NodePair(Node):
    PLUS = 0
    MINUS = 1
    MULT = 2
    DIV = 3
    FUNCS = { PLUS : (lambda x, y: x + y), MINUS : (lambda x, y: x - y), MULT : (lambda x, y: x * y), DIV : (lambda x, y: x / y if np.abs(y) > 1e-5 else np.nan) }

    def __init__(self, rt, obj, op):
        Node.__init__(self, rt, obj)
        self.childLB = 0
        self.childNum = 2

        self.op = op
        
    def calc(self, d):
        child0 = self.children[0]
        child1 = self.children[1]
        size = self.size
        func = NodePair.FUNCS[self.op]
        a = Ops.nans(size)
        for ii in range(size):
            a[ii] = func(child0.value(d, ii), child1.value(d, ii))
        return a

class NodeUnary(Node):

    def __init__(self, rt, obj, func):
        Node.__init__(self, rt, obj)
        self.childLB = 0
        self.childNum = 1

        self.func = func

    def calc(self, d):
        child = self.children[0]
        size = self.size
        a = Ops.nans(size)
        func = self.func
        for ii in range(size):
            a[ii] = func(child.value(d, ii))
        return a

def diffAbs(x, y):
    z = np.abs(x) + np.abs(y)
    return (x - y) / z if z > 1e-5 else 0

class NodeDelay(Node):
    DELAY = 0
    DIFF = 1
    DIFFABS = 2
    FUNCS = { 
        DELAY : (lambda x, y: y), 
        DIFF : (lambda x, y: x - y), 
        DIFFABS : diffAbs
    }

    def __init__(self, rt, obj, days, mode):
        Node.__init__(self, rt, obj)
        self.childLB = int(days)
        self.childNum = 1

        self.days = int(days)
        self.mode = mode

    def calc(self, d):
        child = self.children[0]
        size = self.size
        a = Ops.nans(size)
        ddelay = d - self.days
        func = NodeDelay.FUNCS[self.mode]
        for ii in range(size):
            a[ii] = func(child.value(d, ii), child.value(ddelay, ii))
        return a

class NodeTS(Node):
    SUM = 0
    MEAN = 1
    STD = 2
    TSTAT = 3
    DECAY = 4
    ZSCORE = 5
    SLOPE = 6

    def getFunc(self):
        FUNCS = { 
            NodeTS.SUM : self._TSsum_valueII, 
            NodeTS.MEAN : self._TSmean_valueII, 
            NodeTS.STD : self._TSstd_valueII, 
            NodeTS.TSTAT : self._TStstat_valueII,
            NodeTS.DECAY : self._TSdecay_valueII, 
            NodeTS.ZSCORE : self._TSzscore_valueII, 
            NodeTS.SLOPE : self._TSslope_valueII 
        }
        return FUNCS[self.mode]

    def __init__(self, rt, obj, days, mode):
        Node.__init__(self, rt, obj)
        self.childLB = int(days)
        self.childNum = 1

        self.mode = mode
        
        self.days = int(days)  
        self.valcache = {}
        self.last_updated = [-1] * self.size

        self.s = np.zeros(self.size)
        self.ss = np.zeros(self.size)
        self.st = np.zeros(self.size)

    def updateCache(self, dd):
        child = self.children[0]
        self.valcache[dd] = [ child.value(dd, ii) for ii in range(self.size) ]

    def calc(self, d):
        if len(self.valcache) < self.days:
            assert not len(self.valcache)
            for dd in range(d - self.days, d + 1):
                self.updateCache(dd)
        else:
            assert len(self.valcache) == self.days
            self.updateCache(d)
        a = self.updateValues(d)
        del self.valcache[d - self.days]
        return a

    def fillNans(self, d, ii):
        for dd in reversed(range(d - self.days, d)):
            if not np.isfinite(self.valcache[dd][ii]):
                self.valcache[dd][ii] = self.valcache[dd + 1][ii]

    def updateValues(self, d):
        yd = d - 1
        size = self.size
        a = Ops.nans(size)
        valueII = self.getFunc()
        for ii in range(size):
            if self.valid(d, ii): 
                if self.last_updated[ii] < yd:
                    self.fillNans(d, ii)
                    self.recalcII(d, ii)
                else:
                    self.calcII(d, ii)
                self.last_updated[ii] = d
                a[ii] = valueII(d, ii)
        return a

    def calcII(self, d, ii):
        newx = self.valcache[d][ii]
        self.s[ii] += newx
        self.ss[ii] += newx * newx
        self.st[ii] += newx * (self.days + 1)
        oldx = self.valcache[d - self.days][ii]
        self.st[ii] -= self.s[ii]
        self.s[ii] -= oldx
        self.ss[ii] -= oldx * oldx

    def recalcII(self, d, ii):
        self.s[ii] = sum(self.valcache[j][ii] for j in range(d - self.days + 1, d + 1))
        self.ss[ii] = sum(self.valcache[j][ii] * self.valcache[j][ii] for j in range(d - self.days + 1, d + 1))
        self.st[ii] = sum(self.valcache[j][ii] * (j + self.days - d) for j in range(d - self.days + 1, d + 1))

    def _TSsum_valueII(self, d, ii):
        return self.s[ii]

    def _TSmean_valueII(self, d, ii):
        return self.s[ii] / self.days

    def _TSstd_valueII(self, d, ii):
        n = self.days
        x = self.s[ii] / n
        xx = self.ss[ii] / n
        var = xx - x * x
        return np.sqrt(var) if var > 1e-9 else 0

    def _TStstat_valueII(self, d, ii):
        n = self.days
        x = self.s[ii] / n
        xx = self.ss[ii] / n
        var = xx - x * x
        return np.sqrt(n) * x / np.sqrt(var) if var > 1e-9 else 0

    def _TSdecay_valueII(self, d, ii):
        return self.st[ii]

    def _TSzscore_valueII(self, d, ii):
        n = self.days
        x = self.s[ii] / n
        xx = self.ss[ii] / n
        var = xx - x * x
        return (self.valcache[d][ii] - x) / np.sqrt(var) if var > 1e-9 else 0

    def _TSslope_valueII(self, d, ii):
        n = self.days
        if n < 2:
            return 0
        x = self.s[ii] / n
        xx = self.ss[ii] / n
        xx -= x * x
        if xx < 1e-9:
            return 0
        xx = np.sqrt(xx)
        xy = self.st[ii] / n
        xy -= x * (n + 1) / 2
        yy = np.sqrt((n * n - 1) / 12)
        res = xy / (xx * yy)
        return res  

class NodeAlpha(Node):

    def __init__(self, rt, obj):
        Node.__init__(self, rt, obj)
        self.childLB = 0
        self.childNum = 0

        self.obj = obj

    def calc(self, d):
        size = self.size
        a = Ops.nans(size)
        alpha = self.obj.alpha()
        for ii in range(size):
            a[ii] = alpha.getValue(0, ii)
        return a

def nodeFactory(rt, obj, token):
    args = token.split(' ')
    args = [a.strip() for a in args]

    if args[0] == 'Data':
        n = NodeData(rt, obj, args[1])
    
    elif args[0] == 'Zscore':
        group = 'none' if len(args) < 1 else args[1]
        n = NodeCSOp(rt, obj, 'zscore', group)
    elif args[0] == 'Rank':
        skew = "0" if len(args) < 2 else args[1]
        n = NodeCSOp(rt, obj, 'rank', 'none', skew)
    elif args[0] == 'GroupRank':
        group = args[1]
        skew = "0" if len(args) < 3 else args[2]
        n = NodeCSOp(rt, obj, 'rank', group, skew)

    elif args[0] == 'Delay':
        n = NodeDelay(rt, obj, args[1], NodeDelay.DELAY)
    elif args[0] == 'Diff':
        n = NodeDelay(rt, obj, args[1], NodeDelay.DIFF)
    elif args[0] == 'Diffabs':
        n = NodeDelay(rt, obj, args[1], NodeDelay.DIFFABS)

    elif args[0] == 'TsSum':
        n = NodeTS(rt, obj, args[1], NodeTS.SUM)
    elif args[0] == 'TsMean':
        n = NodeTS(rt, obj, args[1], NodeTS.MEAN)
    elif args[0] == 'TsStd':
        n = NodeTS(rt, obj, args[1], NodeTS.STD)
    elif args[0] == 'TsTstat':
        n = NodeTS(rt, obj, args[1], NodeTS.TSTAT)
    elif args[0] == 'TsDecay':
        n = NodeTS(rt, obj, args[1], NodeTS.DECAY)
    elif args[0] == 'TsZscore':
        n = NodeTS(rt, obj, args[1], NodeTS.ZSCORE)
    elif args[0] == 'TsSlope':
        n = NodeTS(rt, obj, args[1], NodeTS.SLOPE)

    elif args[0] == 'Plus':
        n = NodePair(rt, obj, NodePair.PLUS)
    elif args[0] == 'Minus':
        n = NodePair(rt, obj, NodePair.MINUS)
    elif args[0] == 'Mult':
        n = NodePair(rt, obj, NodePair.MULT)    
    elif args[0] == 'Div':
        n = NodePair(rt, obj, NodePair.DIV)

    elif args[0] == 'Opp':
        func = lambda x : -x
        n = NodeUnary(rt, obj, func)
    elif args[0] == 'Scale':
        a = float(args[1])
        b = float(args[2])
        func = lambda x : a * x + b
        n = NodeUnary(rt, obj, func)
    elif args[0] == 'Abs':
        func = lambda x : np.abs(x)
        n = NodeUnary(rt, obj, func)
    elif args[0] == 'Power':
        p = float(args[1])
        func = lambda x : np.sign(x) * (np.abs(x) ** p)
        n = NodeUnary(rt, obj, func)
    elif args[0] == 'NanOutNegative':
        func = lambda x : np.nan if x < 0 else x
        n = NodeUnary(rt, obj, func)
    elif args[0] == 'ZeroOutNegative':
        func = lambda x : 0 if x < 0 else x
        n = NodeUnary(rt, obj, func)

    elif args[0] == 'FillMean':
        n = NodeFillMean(rt, obj)

    elif args[0] == 'Alpha':
        n = NodeAlpha(rt, obj)

    else:
        raise Exception('Unknown expression')
    return n

def glueNodes(rt, obj, tokens):
    n = nodeFactory(rt, obj, tokens[0])
    tokens.pop(0)
    for i in range(n.childNum):
        n.addChild(glueNodes(rt, obj, tokens))
    return n

def evalString(rt, obj):
    tokens = obj.evalname.split(',')
    tokens = [ t.strip() for t in tokens ]
    return glueNodes(rt, obj, tokens)

try:
    import Framework
    import PyUtil
    from PyBase import PyOperation

    class OpEval(PyOperation):
        def __init__(self, baseObject):
            super().__init__(baseObject, 'OpEval')
            self.evalname = self.config().getString("evalname", "")
            if self.evalname == '':
                raise Exception('Empty name in OpEval')
        
        def run(self, rt):
            try:

                alphaData = self.alphaData()
                di = rt.di - alphaData.delay
                alphaResult = self.alpha()
                u = alphaData.universe
                numStocks = u.size(rt.di)

                if not hasattr(self, 'initNode'):
                    self.initNode = True
                    self.n = evalString(rt, self)
                    self.n.init(di)
                    if Node.MAXLB > 0 and 'Alpha' in self.evalname:
                        raise Exception('You can not use TS operations with Alpha - otherwise prod checkpoints will fail')
                    
                for ii in range(numStocks):
                    if u.isValid(rt.di, ii):
                        alphaResult.setValue(0, ii, self.n.value(di, ii))

            except Exception as e:
                print(traceback.format_exc())

except Exception as e:
    print(traceback.format_exc())




