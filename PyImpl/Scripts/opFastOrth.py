### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# Hammad Khan
#######################################################################################################################################
############ CopyRight Hammad Khan ####################################################################################################
#### No Part of psim or associated code or tools can be reproduced or used without prior written permission of Hammad Khan ############
#### This Header has to be used in all permitted authorized usage of psim #############################################################
#######################################################################################################################################

import sys
import traceback
from datetime import datetime as DT
import numpy as np
import mathutils as mu
from collections import defaultdict

try:
    import Framework
    import PyUtil
    from PyBase import PyOperation

    class OpFastOrth(PyOperation):
        SMALL_VALUE = 1e-5
        def __init__(self, baseObject):
            super().__init__(baseObject, 'OpFastOrth')

            self.p_factorNames = self.config().getString("factors", "")
            if self.p_factorNames:
                self.p_factorNames = [x.strip() for x in self.p_factorNames.split(',')]
            else:
            	self.p_factorNames = []
            self.p_factorNames = sorted(self.p_factorNames)

            self.p_groupNames = self.config().getString("groups", "")
            if self.p_groupNames:
                self.p_groupNames = [x.strip() for x in self.p_groupNames.split(',')]
            else:
            	self.p_groupNames = []
            self.p_groupNames = sorted(self.p_groupNames)

            if not self.p_factorNames and not self.p_groupNames:
            	raise Exception('Must provide at least one factor or group to orthonogalise with respect to.')

            self.p_picklePath = self.config().getString("picklePath", "")

        def __calc(self):
            self.recalculated = True
            self.__get_factors()
            self.__calc_risk_inverse()

        def __inv_sec(self, name):
            d1_ind = self.d1 if 'revere' in name else 0
            sector = Framework.StringData.script_cast(self.rt.dataRegistry.getData(self.u, name))
            sec = { ii : sector.getValue(d1_ind, ii) for ii in range(self.size) }
            inv_sector = defaultdict(list)
            for k in sec:
                inv_sector[sec[k]].append(k)
            inv_sector = [ (k, v) for k, v in inv_sector.items() ]
            inv_sector = sorted(inv_sector, key = lambda x : x[0])
            return inv_sector

        def __get_factors(self):
            inv_secs = [ (name, self.__inv_sec(name)) for name in self.p_groupNames ]
            self.n = len(self.p_factorNames) + sum( [ len(x[1]) for x in inv_secs ] )
            self.factors = np.zeros((self.n, self.size))
            for k in range(len(self.p_factorNames)):
                f = BamUtil.floatToNPMatrix(self.rt.dataRegistry.floatData(self.u, self.p_factorNames[k]))
                for ii in range(self.size):
                    if np.isfinite(f[self.d1, ii]) and self.u.isValid(self.rt.di, ii):
                        self.factors[k][ii] = f[self.d1, ii]
            k = len(self.p_factorNames)
            for name, inv_sector in inv_secs:
            	for key, v in inv_sector:
                    for ii in v:
                        if self.u.isValid(self.rt.di, ii):
                            self.factors[k][ii] = 1
                    k += 1
            self.__calc_matrices()

        def __calc_matrices(self):
            self.n = self.factors.shape[0]
            self.gram = np.dot(self.factors, self.factors.T)
            q, r = np.linalg.qr(self.gram)
            bad_indices = [ i for i in range(self.n) if np.abs(r[i][i]) < OpFastOrth.SMALL_VALUE ]
            self.factors = np.delete(self.factors, bad_indices, axis = 0)
            self.n = self.factors.shape[0]
            self.gram = np.dot(self.factors, self.factors.T)

        def __calc_risk_inverse(self):
            self.mat = np.linalg.inv(self.gram)
            if self.p_picklePath and self.recalculated:
                self.factors.dump(self.p_picklePath + "/factors-" + str(self.d1))
                self.mat.dump(self.p_picklePath + "/mat-" + str(self.d1))

        def run(self, rt):
            try:
                alphaData = self.alphaData()
                alphaResult = self.alpha()

                self.rt = rt
                self.d1 = rt.di - alphaData.delay
                self.u = alphaData.universe
                self.size = self.u.size(rt.di)
                self.inv_sectors = {}

                if self.p_picklePath:
                    try:
                        self.factors = np.load(self.p_picklePath + "/factors-" + str(self.d1))
                        self.mat = np.load(self.p_picklePath + "/mat-" + str(self.d1))
                        self.n = self.factors.shape[0]
                        assert self.n == np.shape(self.mat)[0]
                        assert self.n == np.shape(self.mat)[1]
                        self.recalculated = False
                    except:
                        self.__calc()
                else:
                    self.__calc()

                a = np.zeros((self.size,))
                valids = 0
                sumvalids = 0
                nanindices = []
                for ii in range(self.size):
                    if self.u.isValid(self.rt.di, ii):
                        val = alphaResult.getValue(0, ii)
                        if np.isfinite(val):
                            valids += 1
                            sumvalids += val
                            a[ii] = val
                        else:
                            nanindices.append(ii)

                if valids == 0:
                    return
                sumvalids /= valids
                for ii in nanindices:
                    a[ii] = sumvalids

                dotprod = np.dot(self.factors, a)
                p = np.dot(self.mat, dotprod)
                sub = np.dot(p, self.factors)
                a -= sub                

                for ii in range(self.size):
                    if self.u.isValid(self.rt.di, ii):
                        alphaResult.setValue(0, ii, a[ii])

            except Exception as e:
                print(traceback.format_exc())

except Exception as e:
    print(traceback.format_exc())