### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# Hammad Khan
#######################################################################################################################################
############ CopyRight Hammad Khan ####################################################################################################
#### No Part of psim or associated code or tools can be reproduced or used without prior written permission of Hammad Khan ############
#### This Header has to be used in all permitted authorized usage of psim #############################################################
#######################################################################################################################################



import sys
import traceback
from datetime import datetime as DT
import numpy as np
import mathutils as mu
from collections import defaultdict

try:
    import Framework
    import PyUtil
    from PyBase import PyOperation

    class OpPosHump(PyOperation):
        def __init__(self, baseObject):
            super().__init__(baseObject, 'OpPosHump')
            self.posHump = float(self.config().getString("posHump", "0.1"))
        
        def run(self, rt):
            try:

                alphaData = self.alphaData()
                di = rt.di - alphaData.delay
                alphaResult = self.alpha()
                universe = alphaData.universe
                numStocks = universe.size(rt.di)

                a = np.zeros(numStocks)
                if not hasattr(self, 'ya'):
                    self.ya = np.zeros(numStocks)
                    return

                mcap = rt.dataRegistry.floatData(universe, "baseData.mcap")

                for ii in range(numStocks):
                    if universe.isValid(rt.di, ii):
                        a[ii] = alphaResult.getValue(0, ii)

                a = np.nan_to_num(a)
                ma = np.mean(a)
                a -= ma
                na = sum(abs(a))
                if na < 1e-5:
                    for ii in range(numStocks):
                        if universe.isValid(rt.di, ii):
                            alphaResult.setValue(0, ii, self.ya[ii])
                    return

                a /= na

                diff = a - self.ya
                for ii in range(numStocks):
                    if abs(a[ii]) > 1e-5:
                        if abs(diff[ii] / a[ii]) < self.posHump:
                            a[ii] = self.ya[ii]

                ma = np.mean(a)
                a -= ma
                na = sum(abs(a))
                a /= na
                    
                self.ya = a

                for ii in range(numStocks):
                    if universe.isValid(rt.di, ii):
                        alphaResult.setValue(0, ii, a[ii])

            except Exception as e:
                print(traceback.format_exc())

except Exception as e:
    print(traceback.format_exc())




