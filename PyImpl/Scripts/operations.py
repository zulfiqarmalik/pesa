### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# Hammad Khan
#######################################################################################################################################
############ CopyRight Hammad Khan ####################################################################################################
#### No Part of psim or associated code or tools can be reproduced or used without prior written permission of Hammad Khan ############
#### This Header has to be used in all permitted authorized usage of psim #############################################################
#######################################################################################################################################



import sys
import traceback
from datetime import datetime as DT
import numpy as np
import mathutils as mu

try:
    import Framework
    import PyUtil
    from PyBase import PyOperation

    class OpOrthogonalise(PyOperation):

        def orthogonalise(self, factors, alpha):
            numStocks = len(alpha)
            A = np.hstack((factors, alpha.reshape(numStocks,1)))
            alphaNans = np.isnan(alpha) # which rows have a nan-alpha?
            APrime = np.ones((A.shape)) * np.nan
            APrime[~alphaNans,:] = mu.gram_schmidt(A[~alphaNans,:])
            return APrime[:,-1] # we are only interested in the orthogonalised alpha


        def __init__(self, baseObject):
            super().__init__(baseObject, 'OpOrthogonalise')
            self.factorNames = self.config().getString("factors", "")
            self.groupNames = self.config().getString("groups", "")

            self.numFactors = 0
            self.numGroups = 0

            if self.factorNames != '':
                self.factorNames = [x.strip() for x in self.factorNames.split(',')]
                self.numFactors = len(self.factorNames)

            if self.groupNames != '':
                self.groupNames = [x.strip() for x in self.groupNames.split(',')]
                self.numGroups = len(self.groupNames)

            if self.numFactors + self.numGroups == 0:
                raise Exception('Must provide at least one factor or group to orthonogalise with respect to.')


        
        def run(self, rt):
            try:

                alphaData = self.alphaData()
                di = rt.di - alphaData.delay
                alphaResult = self.alpha()
                universe = alphaData.universe
                numStocks = universe.size(rt.di)

                # load factors
                factors = np.zeros((numStocks,self.numFactors))
                for jj in range(self.numFactors):
                    factorName = self.factorNames[jj]
                    jjthFactor = rt.dataRegistry.floatData(universe, factorName)
                    for ii in range(numStocks):
                        factors[ii,jj] = jjthFactor.getValue(di, ii)

                if self.numGroups > 0:
                    # load groups
                    groupDict = {}
                    for jj in range(self.numGroups):
                        groupName = self.groupNames[jj]
                        groupData = rt.dataRegistry.stringData(universe, groupName)
                        for ii in range(numStocks):
                            classification = groupData.getValue(0, ii) # TODO! Check this!
                            if (groupName, classification) in groupDict:
                                groupDict[(groupName, classification)].append(ii)
                            else:
                                groupDict[(groupName, classification)] = [ii]

                    numGroupFactors = len(groupDict)
                    groupFactors = np.zeros((numStocks, numGroupFactors))
                    gn = 0

                    #import pdb; pdb.set_trace()
                    gk = list(groupDict.keys())
                    gk.sort()

                    for k in gk:
                        v = groupDict[k]
                        thisVector = np.zeros(numStocks)
                        thisVector[v] = 1
                        groupFactors[:, gn] = thisVector
                        gn = gn + 1

                    # add the group factors to the existing factors
                    factors = np.hstack([factors, groupFactors])

                # load existing alphas and isValid vector
                alpha = np.zeros(numStocks)
                isValid = np.zeros(numStocks)
                for ii in range(numStocks):
                    alpha[ii] = alphaResult.getValue(0,ii)
                    isValid[ii] = universe.isValid(rt.di,ii)

                alphaPrime = self.orthogonalise(factors, alpha)

                for ii in range(numStocks):
                    if isValid[ii]:
                        alphaResult.setValue(0,ii,alphaPrime[ii])


            except Exception as e:
                print(traceback.format_exc())




    class OpIdentity(PyOperation):
        def __init__(self, baseObject):
            super().__init__(baseObject, 'OpIdentity')
        
        def run(self, rt):
            try:

                alphaData = self.alphaData()
                di = rt.di - alphaData.delay
                alphaResult = self.alpha()
                universe = alphaData.universe
                numStocks = universe.size(rt.di)

                alpha = np.zeros(numStocks)
                isValid = np.zeros(numStocks)
                for ii in range(numStocks):
                    alpha[ii] = alphaResult.getValue(0,ii)
                    isValid[ii] = universe.isValid(rt.di,ii)

                std_alpha = np.nanstd(alpha)
                alpha = alpha / std_alpha # divide by the standard deviation
                import pdb; pdb.set_trace()
                for ii in range(numStocks):
                    if isValid[ii]:
                        alphaResult.setValue(0,ii,alpha[ii]*1)


            except Exception as e:
                print(traceback.format_exc())


except Exception as e:
    print(traceback.format_exc())




