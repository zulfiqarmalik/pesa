### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# Hammad Khan
#######################################################################################################################################
############ CopyRight Hammad Khan ####################################################################################################
#### No Part of psim or associated code or tools can be reproduced or used without prior written permission of Hammad Khan ############
#### This Header has to be used in all permitted authorized usage of psim #############################################################
#######################################################################################################################################


import sys
import traceback
from datetime import datetime as DT
import time
import numpy as np
import mosek
from TaskWrapper import TaskWrapper
from numpy import zeros, ones, inf, eye
from numpy.linalg import cholesky
from pandas import DataFrame
import pandas as pd
import json

class ObjFun:
    l1norm = 1
    maxret = 2
    maxretminuscosts = 3
    @staticmethod
    def getValue(name):
        return getattr(ObjFun(), name)

def printQuantiles(x, name):
    print('---', name, '---')
    y = sorted(x)
    l = len(y)
    print(l, 'values:', end = ' ')
    out = [ y[0], y[l//20], y[l//10], y[l//5], y[l//2], y[4*l//5], y[9*l//10], y[19*l//20], y[-1] ]
    for val in out:
        print('{:.4}'.format(val), end = ' ')
    print('')

try:
    import Framework
    import PyUtil
    from PyBase import PyOptimiser

    class PyMOSEK(PyOptimiser):

        ANNFACTOR = 252.0
        SQRTANNFACTOR = np.sqrt(252.0)

        def existsMarketCapLimit(self): 
            xx = [ self.smallMarketCapPctLimit, self.smallMarketCapNetPctLimit, self.largeMarketCapNetPctLimit, self.maxSmallMarketCapMaxPosPctBook, self.maxSmallMarketCapMaxPosDollar  ]
            return any( [ (x != -1) for x in xx ] )
            
        def __init__(self, baseObject):
            super().__init__(baseObject, "PyMOSEK")
            self.hist_alpha=[]
            self.hist_pnl  = []

            self.maxPosPctBook = float(self.config().getString("maxPosPctBook", "-1"))
            self.overrideMaxPosPctBook = float(self.config().getString("overrideMaxPosPctBook", "-1"))

            self.maxPosPctSharesOutLong = float(self.config().getString("maxPosPctSharesOutLong", "-1"))
            self.maxPosPctSharesOutShort = float(self.config().getString("maxPosPctSharesOutShort", "-1"))
            self.maxPosPctADV = float(self.config().getString("maxPosPctADV", "-1"))
            self.maxTradePctBook = float(self.config().getString("maxTradePctBook", "-1"))
            self.maxTradePctADV = float(self.config().getString("maxTradePctADV", "-1"))
            self.maxTurnover = float(self.config().getString("maxTurnover", "-1"))
            self.maxSmallMarketCapTurnover = float(self.config().getString("maxSmallMarketCapTurnover", "-1"))
            self.annualisedVol = float(self.config().getString("annualisedVol", "-1"))
            self.riskModel = self.config().getString("riskModel", "")
            self.riskModelFactors = self.config().getString("riskModelFactors","")
            self.preserveSign = self.config().getString("preserveAlphaSign","false").lower() == 'true'
            self.useShortHardLiquidate = self.config().getString("useShortHardLiquidate", "1").lower()
            self.spreadData = self.config().getString("spreadData", "")
            self.tCostMultiplier = float(self.config().getString("tCostMultiplier","1"))
            self.delay = int(self.config().getString("delay", "1"))

            self.printQuantiles = self.config().getString("printQuantiles","false").lower() == 'true'
            self.l1normMaxretParam = float(self.config().getString("l1normMaxretParam", "-1"))

            self.useSqConstraint = self.config().getString("useSqConstraint","false").lower() == 'true'
            self.sqConstraintParam = float(self.config().getString("sqConstraintParam", "0.75"))

            # some ugliness for backwards compatibility
            if self.useShortHardLiquidate == 'true':
                self.useShortHardLiquidate = '0' # force liquidation and do no contribute to turnover
            elif self.useShortHardLiquidate == 'false':
                self.useShortHardLiquidate = '1' # do nothing
            self.useShortHardLiquidate = float(self.useShortHardLiquidate)

            self.earningsExposure = float(self.config().getString("minimiseEarningsExposure", "1"))
            
            self.locatesSource = self.config().getString("locatesSource", "")
            self.locatesMultiplier = float(self.config().getString("locatesMultiplier", "1"))
                
            #self.useSmallMarketCapLimit = self.config().getString("useSmallMarketCapLimit", "false").lower() == 'true' # NOW DEPRECATED
            self.smallMarketCapSize = float(self.config().getString("smallMarketCapSize", "1000000000"))
            self.smallMarketCapPctLimit = float(self.config().getString("smallMarketCapPctLimit", "-1"))
            self.smallMarketCapNetPctLimit = float(self.config().getString("smallMarketCapNetPctLimit", "-1"))
            self.largeMarketCapNetPctLimit =  float(self.config().getString("largeMarketCapNetPctLimit", "-1"))

            self.maxSmallMarketCapMaxPosPctBook = float(self.config().getString("maxSmallMarketCapMaxPosPctBook", "-1"))
            self.maxSmallMarketCapMaxPosDollar = float(self.config().getString("maxSmallMarketCapMaxPosDollar", "-1"))
            self.maxSmallMarketCapMaxPosStartMult = float(self.config().getString("maxSmallMarketCapMaxPosStartMult", "-1"))

            self.verificationFile = self.config().getString("verificationFile", "")
            self.keepPrevPosOnFail = self.config().getString("keepPrevPosOnFail", "true").lower() == 'true'
            self.tradeableUniverse = self.config().getString("tradeableUniverse", "")
            if self.tradeableUniverse == "":
                self.tradeableUniverse = None

            self.useMaxPosPctBook = self.maxPosPctBook != -1
            self.useMaxPosPctSharesOutLong = self.maxPosPctSharesOutLong != -1
            self.useMaxPosPctSharesOutShort = self.maxPosPctSharesOutShort != -1
            self.useMaxPosPctADV = self.maxPosPctADV != -1
            self.useMaxTradePctBook = self.maxTradePctBook != -1
            self.useMaxTradePctADV = self.maxTradePctADV != -1
            self.useMaxTurnover = self.maxTurnover != -1
            self.useMaxSmallMarketCapTurnover = self.maxSmallMarketCapTurnover != -1
            self.useAnnualisedVol = self.annualisedVol != -1

            
            self.bookSize = self.parentConfig().portfolio().bookSize()

            # convert into percentages, with defaults nearing infinity
            self.maxPosPctBook = self.maxPosPctBook if self.useMaxPosPctBook else 10 # w and wAbs
            self.maxTurnover = self.maxTurnover if self.useMaxTurnover else 10 # sum of dwAbs (therfore dw also)
            self.maxSmallMarketCapTurnover = self.maxSmallMarketCapTurnover if self.useMaxSmallMarketCapTurnover else 10 # sum of dwAbs (therfore dw also)
            self.maxTradePctBook = self.maxTradePctBook if self.useMaxTradePctBook else 10 # w and wAbs
            self.dailyVol = self.annualisedVol * self.bookSize / np.sqrt(252) if self.useAnnualisedVol else self.bookSize
            self.maxPosPctADV = self.maxPosPctADV if self.useMaxPosPctADV else 10
            self.maxTradePctADV = self.maxTradePctADV if self.useMaxTradePctADV else 10

            # multiply up by the book size
            self.maxPosPctBook *= self.bookSize
            self.maxTurnover *= self.bookSize
            self.maxTradePctBook *= self.bookSize
            self.smallMarketCapPctLimit *= self.bookSize
            self.maxSmallMarketCapTurnover *= self.bookSize

            if self.maxSmallMarketCapMaxPosPctBook != -1:
                self.maxSmallMarketCapMaxPosPctBook *= self.bookSize
            self.maxSmallMarketCapMaxPos = min(self.maxSmallMarketCapMaxPosPctBook, self.maxSmallMarketCapMaxPosDollar) 
            if self.maxSmallMarketCapMaxPos < 0: #one or both are not defined
                self.maxSmallMarketCapMaxPos = max(self.maxSmallMarketCapMaxPosPctBook, self.maxSmallMarketCapMaxPosDollar) #use the positive one - or if both are negative, it will be negative as well
            
            # what is the objective function

            objFunString = self.config().getString("objectiveFunction", "l1norm") # default to l1norm(alpha,w)
            self.objFun = ObjFun.getValue(objFunString.lower())
            if self.objFun != ObjFun.l1norm:
                self.l1normMaxretParam = -1
            self.useSpreadData = (self.objFun == ObjFun.maxretminuscosts) # JDMTODO!!!
            self.hasInitialised = False

            # The following are all risk-related!


            self.customFactorLimitTuples = [] # each one is a pair of factorNames, index, pctOfBookSize
            factorLimits = self.config().getChildSection('CustomFactorExposureLimits')
            if factorLimits is not None:
                for f in factorLimits.dict():
                    limit = float(factorLimits.getString(f, '-1'))
                    if limit != -1:
                        self.customFactorLimitTuples.append((f, limit * self.bookSize))

            self.factorLimitTuples = [] # each one is a pair of factorNames, index, pctOfBookSize
            factorLimits = self.config().getChildSection('RiskModelFactorExposureLimits')
            if factorLimits is not None:
                for f in factorLimits.dict():
                    limit = float(factorLimits.getString(f, '-1'))
                    if limit != -1:
                        self.factorLimitTuples.append((f, limit * self.bookSize))

            self.groupLimitTuples = []
            groupLimits = self.config().getChildSection('GroupLimits')
            if groupLimits is not None:
                for g in groupLimits.dict():
                    limit = float(groupLimits.getString(g, '-1'))
                    if limit != -1:
                        self.groupLimitTuples.append((g, limit * self.bookSize))


            self.grossGroupLimitTuples = []
            groupLimits = self.config().getChildSection('GrossGroupLimits')
            if groupLimits is not None:
                for g in groupLimits.dict():
                    limit = float(groupLimits.getString(g, '-1'))
                    if limit != -1:
                        self.grossGroupLimitTuples.append((g, limit * self.bookSize))



            self.existsCustomFactorExposureLimits = len(self.customFactorLimitTuples) > 0
            self.existsFactorExposureLimits = len(self.factorLimitTuples) > 0
            self.existsGroupLimits = len(self.groupLimitTuples) > 0
            self.existsGrossGroupLimits = len(self.grossGroupLimitTuples) > 0
            
            self.nonStaticGroups = []
            self.nonStaticGrossGroups = []

            if self.useAnnualisedVol or self.existsFactorExposureLimits:
                if self.riskModel == '':
                    raise Exception('If you are using portfolio volatility limit or using the RiskModelEFactorExposureLimits tag, a risk model must be specified!')

            if self.existsFactorExposureLimits:
                # TODO: Can make the following more flexible later
               self.factorLimitTuples = [('axioma.' + x.strip(),y) for (x,y) in self.factorLimitTuples] # beef them up with the correct prefix.

            if self.existsCustomFactorExposureLimits:
                self.factorLimitTuples = self.factorLimitTuples + self.customFactorLimitTuples
                self.existsFactorExposureLimits = True

            constraintResolution = self.config().getChildSection('ConstraintResolution')
            if constraintResolution is None:
                raise Exception('At least one constraint resolution instruction must be provided!')

            numRelaxes = constraintResolution.numChildren()
            if numRelaxes == 0:
                raise Exception('At least one constraint resolution instruction must be provided!')

            self.constraintResolution = []
            for cr in range(numRelaxes):
                child_dict = constraintResolution.child(cr)
                temp = []
                for cons in child_dict.dict():
                    if cons == "maxPosPctADV":
                        self.warning('Please use maxTradePctADV instead of maxPosPctADV in RelaxConstraints')
                    temp.append((cons, float(child_dict.getString(cons, '-1'))))
                self.constraintResolution.append(temp)

            if self.useAnnualisedVol:
                # need the pieces of the covariance matrix
                self.dataNameCovMatrix = 'axioma.c_' + self.riskModel + '_Cov'
                self.dataNameSpecificRisk = 'axioma.r_' + self.riskModel + '_SpecificRisk'
                if self.riskModelFactors == '':
                    raise Exception('Factor names for the risk model must be provided')
                self.dataNamesFactorLoadings = ['axioma.' + x.strip() for x in self.riskModelFactors.split(',')]
                self.numFactors = len(self.dataNamesFactorLoadings)

            if self.doVerify():
                self.v_usdpos = None
                self.v_maxPosPctBook = None
                self.v_exposures = None
                self.v_groupExposures = None
                
        
        def AddLinearConstraints(self, N):
            # STATIC: MARKET NEUTRAL CONSTRAINT
            c = zeros((1, self.lenX))
            c[:, self.ind_w] = 1
            self.task.AddLinearConstraint(c, [mosek.boundkey.fx], np.array([0]), np.array([0]))
            
            # STATIC: BOOK SIZE CONSTRAINT
            c = zeros((1, self.lenX))
            c[:, self.ind_wAbs] = 1
            self.task.AddLinearConstraint(c, [mosek.boundkey.ra], np.array([0]), np.array([self.bookSize]))

            # STATIC: wAbs VDUMMY VARIABLE
            # define the relationship between w and wAbs
            # w - wAbs <= 0
            c = zeros((N, self.lenX))
            c[:, self.ind_w] = eye(N)
            c[:, self.ind_wAbs] = -eye(N)
            self.task.AddLinearConstraint(c, N*[mosek.boundkey.up], -inf * ones(N), zeros(N))
            # -w - wAbs <= 0
            c = zeros((N, self.lenX))
            c[:, self.ind_w] = -eye(N)
            c[:, self.ind_wAbs] = -eye(N)
            self.task.AddLinearConstraint(c, N*[mosek.boundkey.up], -inf * ones(N), zeros(N))

            if self.useMaxTradePctBook or self.useMaxTradePctADV or self.useMaxTurnover or self.useMaxSmallMarketCapTurnover or self.preserveSign:
                # w - dw = w_0
                c = zeros((N, self.lenX))
                c[:, self.ind_w] = eye(N)
                c[:, self.ind_dw] = -eye(N)
                # the following will be updated later! So we save a reference to it
                self.cons_w0 = self.task.AddLinearConstraint(c, N*[mosek.boundkey.fx], zeros(N), zeros(N))

                if self.preserveSign:
                    # 0 <= dw_i * alpha_i <= \inf; i.e. they must be the same sign
                    c = zeros((N, self.lenX))
                    c[:, self.ind_dw] = eye(N) # will be updated later!
                    self.cons_sign = self.task.AddLinearConstraint(c, N*[mosek.boundkey.ra], zeros(N), inf * ones(N))

                if self.useMaxTurnover or self.useMaxSmallMarketCapTurnover:
                    # dw - dwAbs <= 0
                    c = zeros((N, self.lenX))
                    c[:, self.ind_dw] = eye(N)
                    c[:, self.ind_dwAbs] = -eye(N)
                    self.task.AddLinearConstraint(c, N*[mosek.boundkey.up], -inf * ones(N), zeros(N))
                    # -dw - dwAbs <= 0
                    c = zeros((N, self.lenX))
                    c[:, self.ind_dw] = -eye(N)
                    c[:, self.ind_dwAbs] = -eye(N)
                    self.task.AddLinearConstraint(c, N*[mosek.boundkey.up], -inf * ones(N), zeros(N))

                    # sum of absolute changes in position
                    c = zeros((1, self.lenX))
                    c[:, self.ind_dwAbs] = 1 
                    # hardcoded for now
                    self.cons_turnover = self.task.AddLinearConstraint(c, [mosek.boundkey.ra], 0, self.maxTurnover)


            if self.existsFactorExposureLimits:
                self.cons_factorExposureLimits = [] # list of slices
                for (i,(factorName, limit)) in enumerate(self.factorLimitTuples):
                    c = zeros((1, self.lenX))
                    c[:, self.ind_w] = 1 # this will need to be updated
                    self.cons_factorExposureLimits.append(self.task.AddLinearConstraint(c, [mosek.boundkey.ra], -limit, limit))

            if self.smallMarketCapPctLimit != -1:
                c = zeros((1, self.lenX))
                c[:, self.ind_wAbs] = 1 # this will need to be updated in each run depending on whether something is classified small or not.
                self.cons_smallCap = self.task.AddLinearConstraint(c, [mosek.boundkey.ra], np.array([0]), np.array([self.smallMarketCapPctLimit]))

            if self.smallMarketCapNetPctLimit != -1:
                c = zeros((1, self.lenX))
                c[:, self.ind_w] = 1 # this will need to be updated in each run depending on whether something is classified small or not.
                self.cons_smallCapNet = self.task.AddLinearConstraint(c, [mosek.boundkey.ra], -np.array([self.smallMarketCapNetPctLimit]), np.array([self.smallMarketCapNetPctLimit]))

            if self.largeMarketCapNetPctLimit != -1:
                c = zeros((1, self.lenX))
                c[:, self.ind_w] = 1 # this will need to be updated in each run depending on whether something is classified small or not.
                self.cons_largeCapNet = self.task.AddLinearConstraint(c, [mosek.boundkey.ra], -np.array([self.largeMarketCapNetPctLimit]), np.array([self.largeMarketCapNetPctLimit]))


            if self.useMaxSmallMarketCapTurnover:
                c = zeros((1, self.lenX))
                c[:, self.ind_dwAbs] = 1
                self.cons_smallMarketCapTurnover = self.task.AddLinearConstraint(c, [mosek.boundkey.ra], 0, self.maxSmallMarketCapTurnover)

            if self.objFun == ObjFun.l1norm: # l1 norm, and associated variables
                # z = alpha - w => z + w = alpha
                c = zeros((N, self.lenX))
                c[:, self.ind_w] = eye(N)
                c[:, self.ind_z] = eye(N)
                self.cons_alpha = self.task.AddLinearConstraint(c, N*[mosek.boundkey.fx], zeros(N), zeros(N))

                # STATIC: wAbs VDUMMY VARIABLE
                # define the relationship between w and wAbs
                # z - zAbs <= 0
                c = zeros((N, self.lenX))
                c[:, self.ind_z] = eye(N)
                c[:, self.ind_zAbs] = -eye(N)
                self.task.AddLinearConstraint(c, N*[mosek.boundkey.up], -inf * ones(N), zeros(N))
                # -z - zAbs <= 0
                c = zeros((N, self.lenX))
                c[:, self.ind_z] = -eye(N)
                c[:, self.ind_zAbs] = -eye(N)
                self.task.AddLinearConstraint(c, N*[mosek.boundkey.up], -inf * ones(N), zeros(N))

        def AddSqOverallConstraint(self, N):
            c = np.zeros((N, self.lenX))
            c[:, self.ind_w] = eye(N)
            c[:, self.ind_SQ_t] = -eye(N)
            self.task.AddLinearConstraint(c, N*[mosek.boundkey.fx], zeros(N), zeros(N))
            self.task.AddCone(mosek.conetype.quad, self.ind_SQ_T + self.ind_SQ_t)

        def UpdateSqOverallConstraint(self, numValidStocks):
            #bound = self.bookSize / np.sqrt(numValidStocks / 1.5)
            b = self.bookSize / numValidStocks
            a = np.sqrt(b * self.maxPosPctBook)
            l = self.sqConstraintParam
            bound = (b ** l) * (a ** (1 - l))
            self.task.UpdateVariableLowerBound(self.ind_SQ_T, np.array([bound * np.sqrt(numValidStocks)]))
            self.task.UpdateVariableUpperBound(self.ind_SQ_T, np.array([bound * np.sqrt(numValidStocks)]))

        def AddRiskConstraints(self, N):
            sqrtOmega = ones((N,N)) # TODO! Actually, this is just triangular.
            # SQRTOmega*w - t = 0
            c = np.zeros((N, self.lenX))
            c[:, self.ind_w] = sqrtOmega
            c[:, self.ind_t] = -eye(N)
            self.cons_omega = self.task.AddLinearConstraint(c, N*[mosek.boundkey.fx], zeros(N), zeros(N))
            self.task.AddCone(mosek.conetype.quad, self.ind_T + self.ind_t)

        def DeclareObjectiveFunction(self, N, alpha):
            if self.objFun == ObjFun.l1norm: # minimize the l1 norm (w, alpha)
                self.task.SetObjectiveFunctionSlice(self.ind_zAbs, ones(N))
            else: 
                self.task.SetObjectiveFunctionSlice(self.ind_w, -alpha)

        def DeclareVariables(self, N): # where N is the number of stocks
            
            self.task = TaskWrapper()
            # always have w, wAbs
            self.ind_w = list(self.task.AddVariable(N, N*[mosek.boundkey.ra], -self.maxPosPctBook *ones(N), self.maxPosPctBook*ones(N)))
            self.ind_wAbs = list(self.task.AddVariable(N, N*[mosek.boundkey.ra], zeros(N), self.maxPosPctBook*ones(N)))
            # may have dw
            if self.useMaxTradePctBook or self.useMaxTradePctADV or self.useMaxTurnover:
                self.ind_dw = list(self.task.AddVariable(N, N*[mosek.boundkey.ra], -self.maxTradePctBook*ones(N), self.maxTradePctBook*ones(N)))
            # may have dwAbs
            if self.useMaxTurnover:
                self.ind_dwAbs = list(self.task.AddVariable(N, N*[mosek.boundkey.lo], zeros(N), self.maxTradePctBook*ones(N))) #TODO! This should be ra
            # may have t, T (in the event of conic formulation of risk)
            if self.useAnnualisedVol:
                self.ind_t = list(self.task.AddVariable(N, [mosek.boundkey.fr]*N, -inf * np.ones(N), inf * np.ones(N)))
                self.ind_T = list(self.task.AddVariable(1, [mosek.boundkey.fx], np.array([self.dailyVol]), np.array([self.dailyVol])))

            if self.useSqConstraint:
                self.ind_SQ_t = list(self.task.AddVariable(N, [mosek.boundkey.fr]*N, -inf * np.ones(N), inf * np.ones(N)))
                self.ind_SQ_T = list(self.task.AddVariable(1, [mosek.boundkey.fx], np.array([0]), np.array([0])))

            # what is our objective function?
            if self.objFun == ObjFun.l1norm: #
                self.ind_z = list(self.task.AddVariable(N, N*[mosek.boundkey.fr], -inf * np.ones(N), inf*ones(N)))
                self.ind_zAbs = list(self.task.AddVariable(N, N*[mosek.boundkey.lo], zeros(N), inf*ones(N)))

            # finished adding variables and their handles
            self.lenX = self.task.nextAvailableIndex # how many variables we have initialised


        def RelaxConstraints(self, numStocks, yvalues, forceLiquidation, dwLongRestrict, dwShortRestrict, ADV, pxs, isShortHardLiquidate, hasEarningsExposure, sharesOut, locatesAmount):

            self.info("Optimisation failed. Attempting constraint resolution.")

            numRelaxes = len(self.constraintResolution)
            multMaxTurnover = 1; multMaxTradePctBook = 1; multMaxTradePctADV=1; multMaxAnnualisedVol = 1

            status = None; xx = None; foundSolution = False
            for step in range(numRelaxes):
                if not foundSolution:
                    for (component,limit) in self.constraintResolution[step]:
                        if component == 'maxTurnover':
                            multMaxTurnover *= limit 
                        elif component == 'maxTradePctBook':
                            multMaxTradePctBook *= limit
                        elif component == 'maxTradePctADV':
                            multMaxTradePctADV *= limit
                        elif component == 'maxPosPctADV':
                            multMaxTradePctADV *= limit
                        elif component == 'annualisedVol':
                            multMaxAnnualisedVol *= limit
                        else:
                            raise Exception('Constraint type not recognised, please ensure your config file is correct!')

                    self.UpdateVariableBounds(numStocks, yvalues, forceLiquidation, dwLongRestrict, dwShortRestrict, ADV, pxs, isShortHardLiquidate, hasEarningsExposure, sharesOut, locatesAmount, multMaxTradePctBook, multMaxTradePctADV)
                    self.task.UpdateLinearConstraintUpperBound(self.cons_turnover, self.maxTurnover * multMaxTurnover)
                    if self.useAnnualisedVol:
                        self.task.UpdateVariableLowerBound(self.ind_T, np.array([self.dailyVol]) * multMaxAnnualisedVol)
                        self.task.UpdateVariableUpperBound(self.ind_T, np.array([self.dailyVol]) * multMaxAnnualisedVol)
                    
                    (status, xx) = self.task.Optimize()
                    if status != 1:
                        self.info("Relax Step %i failed" % step)
                    else:
                        self.info("Relax Step %i succeeded" % step)
                        foundSolution = True
            
            if not foundSolution:
                self.warning("Failed to find solution after all constraint resolution steps applied.")

            # Reset everything
            self.UpdateVariableBounds(numStocks, yvalues, forceLiquidation, dwLongRestrict, dwShortRestrict, ADV, pxs, isShortHardLiquidate, hasEarningsExposure, sharesOut, locatesAmount)
            self.task.UpdateLinearConstraintUpperBound(self.cons_turnover, self.maxTurnover)
            if self.useAnnualisedVol:
                self.task.UpdateVariableLowerBound(self.ind_T, np.array([self.dailyVol]))
                self.task.UpdateVariableUpperBound(self.ind_T, np.array([self.dailyVol]))

            return (status, xx)
                    

        def UpdateVariableBounds(self, numStocks, yvalues, forceLiquidation, dwLongRestrict, dwShortRestrict, ADV, pxs, isShortHardLiquidate, hasEarningsExposure, sharesOut, locatesAmount, multMaxTradePctBook=1., multMaxTradePctADV=1.):

            posLowerBounds = - ones(numStocks) * self.maxPosPctBook
            posUpperBounds =   ones(numStocks) * self.maxPosPctBook

            posLowerBounds[forceLiquidation] = 0
            posUpperBounds[forceLiquidation] = 0

            USDADV = None
            if ADV is not None:
                USDADV = ADV * pxs# max USD value we can have
                # TODO! The following is probably a bit over-conservative
                USDADV[np.isnan(USDADV)] = 0 # If the price or volume is zero, set to be 0.

            self.USDADV = USDADV

            if self.useMaxPosPctADV:
                # take the most binding of the restrictions
                posLowerBounds = np.vstack((posLowerBounds, -USDADV * self.maxPosPctADV)).max(axis=0)
                posUpperBounds = np.vstack((posUpperBounds, USDADV * self.maxPosPctADV)).min(axis=0)

            if self.maxSmallMarketCapMaxPos >= 0:
                smallCapUpper = self.maxSmallMarketCapMaxPosStartMult * self.smallMarketCapSize
                dummy = inf * ones(numStocks)
                for idx, val in enumerate(self.mcaps):
                    if val < smallCapUpper:
                        param = (val - self.smallMarketCapSize) / (smallCapUpper - self.smallMarketCapSize)
                        param = max(param, 0)
                        dummy[idx] = self.maxSmallMarketCapMaxPos + param * self.maxPosPctBook
                posLowerBounds = np.vstack((posLowerBounds, -dummy)).max(axis=0)
                posUpperBounds = np.vstack((posUpperBounds, dummy)).min(axis=0)

            if self.useMaxPosPctSharesOutLong:
                posUpperBounds = np.vstack((posUpperBounds, sharesOut * self.maxPosPctSharesOutLong)).min(axis=0)
            if self.useMaxPosPctSharesOutShort:
                posLowerBounds = np.vstack((posLowerBounds, -sharesOut * self.maxPosPctSharesOutShort)).max(axis=0)

            if self.useShortHardLiquidate < 1:
                posLowerBounds[isShortHardLiquidate] = np.vstack((posLowerBounds, -self.useShortHardLiquidate*self.maxPosPctBook*ones(numStocks))).max(axis=0)[isShortHardLiquidate]

            if self.earningsExposure < 1:
                posLowerBounds[hasEarningsExposure] = np.vstack((posLowerBounds, -self.earningsExposure*self.maxPosPctBook*ones(numStocks))).max(axis=0)[hasEarningsExposure]
                posUpperBounds[hasEarningsExposure] = np.vstack((posUpperBounds, self.earningsExposure*self.maxPosPctBook*ones(numStocks))).min(axis=0)[hasEarningsExposure]

            # TODO: for now we (WRONGLY) assume full liquidation of stocks if their isValid status
            # yesterday as 1 and today it is 0.
            # we furthermore assume that this DOES NOT COUNT TOWARDS TURNOVER
            # This behaviour is not correct, but we need to have an exact plan for dealing
            # with liquidations which I don't have at the moment.

            dPosLowerBounds = - ones(numStocks) * self.maxTradePctBook * multMaxTradePctBook
            dPosUpperBounds =   ones(numStocks) * self.maxTradePctBook * multMaxTradePctBook

            for idx, shortRestrict in enumerate(dwShortRestrict):
                if shortRestrict:
                    if yvalues[idx] < 0:
                        dPosLowerBounds[idx] = 0 # hold the short position and don't breathe
                    else:
                        dPosLowerBounds[idx] = max(dPosLowerBounds[idx], -yvalues[idx]) # still OK to unwind if long

            dPosLowerBounds[dwLongRestrict] = 0 # no new position taking in these
            dPosUpperBounds[dwLongRestrict] = 0 # no new position taking in these 

            dPosLowerBounds = np.vstack((dPosLowerBounds, -locatesAmount)).max(axis=0)
            
            if self.useMaxTradePctADV:
                dPosLowerBounds = np.vstack((dPosLowerBounds, -USDADV * self.maxTradePctADV * multMaxTradePctADV)).max(axis=0)
                dPosUpperBounds = np.vstack((dPosUpperBounds, USDADV * self.maxTradePctADV * multMaxTradePctADV)).min(axis=0)

            #if dw is zero, relax w
            for ii in range(numStocks):
                if dPosLowerBounds[ii] == 0.0:
                    posUpperBounds[ii] = self.maxPosPctBook
                if dPosUpperBounds[ii] == 0.0:
                    posLowerBounds[ii] = -self.maxPosPctBook

            self.task.UpdateVariableLowerBound(self.ind_w, posLowerBounds)
            self.task.UpdateVariableLowerBound(self.ind_wAbs, zeros(numStocks))
            self.task.UpdateVariableUpperBound(self.ind_w, posUpperBounds)
            self.task.UpdateVariableUpperBound(self.ind_wAbs, np.vstack((abs(posUpperBounds), abs(posLowerBounds))).max(axis = 0))

            if self.useMaxTradePctBook or self.useMaxTradePctADV or self.useMaxTurnover:
                self.task.UpdateVariableLowerBound(self.ind_dw, dPosLowerBounds)
                self.task.UpdateVariableLowerBound(self.ind_dwAbs, zeros(numStocks))
                self.task.UpdateVariableUpperBound(self.ind_dw, dPosUpperBounds)
                self.task.UpdateVariableUpperBound(self.ind_dwAbs, np.vstack((abs(posUpperBounds), abs(posLowerBounds))).max(axis = 0))


        def InitialiseOptimisation(self, N, universe, rt):
            # declare the variables we use
            t0 = time.time()
            self.DeclareVariables(N)

            t1 = time.time()
            self.debug("DeclareVariables: %0.2f seconds" % (t1 - t0))
            t0 = t1

            self.AddLinearConstraints(N)

            t1 = time.time()
            self.debug("AddLinearConstraints: %0.2f seconds" % (t1 - t0))
            t0 = t1

            if self.useAnnualisedVol:
                self.AddRiskConstraints(N)

            if self.useSqConstraint:
                self.AddSqOverallConstraint(N)
            
            t1 = time.time()
            self.debug("AddRiskConstraints: %0.2f seconds" % (t1 - t0))
            t0 = t1

            if self.existsGroupLimits:
                self.InitialiseGroupConstraints(N,universe, rt)
                
            t1 = time.time()
            self.debug("InitialiseGroupConstraints: %0.2f seconds" % (t1 - t0))
            t0 = t1

            if self.existsGrossGroupLimits:
                self.InitialiseGrossGroupConstraints(N, universe, rt)

            t1 = time.time()
            self.debug("InitialiseGrossGroupConstraints: %0.2f seconds" % (t1 - t0))
            t0 = t1

            self.DeclareObjectiveFunction(N, ones(N))

            t1 = time.time()
            self.debug("DeclareObjectiveFunction: %0.2f seconds" % (t1 - t0))
            t0 = t1

            self.task.InitialiseTask()
            
            t1 = time.time()
            self.debug("InitialiseTask: %0.2f seconds" % (t1 - t0))
            t0 = t1

        def UpdateNonStaticGroups(self, N, universe, rt, di):
            # Now pre-set, conservatively, the non-static groups
            self.nonStaticGroupDict = {}
            for (i, isThisUniversal) in enumerate(self.isUniversal):
                if (not isThisUniversal):
                    groups = self.groupsDataPointers[i]
                    groupName = self.groupLimitTuples[i][0]                    
                    limit = self.groupLimitTuples[i][1]
                    self.nonStaticGroupDict[groupName] = ({}, limit)
                    for ii in range(N):
                        classification = groups.getValue(di, ii)
                        if classification in self.nonStaticGroupDict[groupName][0]:
                            self.nonStaticGroupDict[groupName][0][classification].append(ii)
                        else:
                            self.nonStaticGroupDict[groupName][0][classification] = [ii]
            
            
            self.non_static_group_exposures = []
            gk = list(self.nonStaticGroupDict.keys())
            gk.sort()
            for (j,groupName) in enumerate(gk):
                limit = self.nonStaticGroupDict[groupName][1]
                classifications = self.nonStaticGroupDict[groupName][0]
                c_keys = list(classifications.keys())
                c_keys.sort()
                N = self.nonStaticGroupsCount[j]

                c = np.zeros((N, len(self.ind_w)))
                for (i,d) in enumerate(c_keys):
                    indices = classifications[d]
                    self.non_static_group_exposures.append((d, indices))
                    #subset = [self.ind_w[i] for i in indices]
                    c[i, indices] = 1

                self.task.UpdateLinearConstraintSlice(self.nonStaticGroups[j], self.ind_w, c)

        def InitialiseGroupConstraints(self, N, universe, rt):
            # first find out which ones are universal and which ones are not
            self.groupsDataPointers = []
            self.groupDict = {}
            self.isUniversal = []
            
            for (groupName, limit) in self.groupLimitTuples:
                groups = rt.dataRegistry.stringData(universe, groupName)
                self.groupsDataPointers.append(groups)
                #isThisUniversal = groups._def().isUniversal()
                isThisUniversal = groups.rows() == 1
                self.isUniversal.append(isThisUniversal)
                if isThisUniversal:
                    # DEAL WITH THE STATIC GROUPS FIRST
                    self.groupDict[groupName] = ({}, limit) # copy over the limit
                    for ii in range(N):
                        classification = groups.getValue(0,ii)
                        if classification in self.groupDict[groupName][0]:
                            self.groupDict[groupName][0][classification].append(ii)
                        else:
                            self.groupDict[groupName][0][classification] = [ii]
            
            # the above has accumulted data on the static groups
            self.static_group_exposures = []

            gk = list(self.groupDict.keys())
            gk.sort()
            for groupName in gk:
                limit = self.groupDict[groupName][1]
                classifications = self.groupDict[groupName][0]
                c_keys = list(classifications.keys())
                c_keys.sort()
                numClassifications = len(c_keys)
                c = zeros((numClassifications, self.lenX))

                for (jj,d) in enumerate(c_keys):
                    indices = classifications[d]
                    self.static_group_exposures.append((d, indices))
                    #c = zeros((1, self.lenX))
                    subset = [self.ind_w[i] for i in indices]
                    c[jj, subset] = 1

                self.task.AddLinearConstraint(c, [mosek.boundkey.ra]*numClassifications, -limit*np.ones(numClassifications), limit*np.ones(numClassifications))
            
            # Now pre-set, conservatively, the non-static groups
            self.nonStaticGroupsCount = []
            for (i, isThisUniversal) in enumerate(self.isUniversal):
                if (not isThisUniversal):
                    # we overestimate the number of groups to be on the safe side
                    limit = self.groupLimitTuples[i][1]
                    groupName = self.groupLimitTuples[i][0]
                    numUniqueEntries = rt.dataRegistry.stringData(universe, groupName + '_unique').cols()
                    self.nonStaticGroupsCount.append(numUniqueEntries)
                    c = np.zeros((numUniqueEntries, self.lenX))
                    c[:, self.ind_w] = 1
                    self.nonStaticGroups.append(self.task.AddLinearConstraint(c, numUniqueEntries*[mosek.boundkey.ra], -limit*np.ones(numUniqueEntries), limit*np.ones(numUniqueEntries)))

            self.debug("Group initialisation done!")

        def UpdateNonStaticGrossGroups(self, N, universe, rt, di):
            # Now pre-set, conservatively, the non-static groups
            self.nonStaticGrossGroupDict = {}
            for (i, isThisUniversal) in enumerate(self.isUniversal):
                if (not isThisUniversal):
                    groups = self.grossGroupsDataPointers[i]
                    groupName = self.grossGroupLimitTuples[i][0]                    
                    limit = self.grossGroupLimitTuples[i][1]
                    self.nonStaticGrossGroupDict[groupName] = ({}, limit)
                    for ii in range(N):
                        classification = groups.getValue(di, ii)
                        if classification in self.nonStaticGrossGroupDict[groupName][0]:
                            self.nonStaticGrossGroupDict[groupName][0][classification].append(ii)
                        else:
                            self.nonStaticGrossGroupDict[groupName][0][classification] = [ii]
            
            
            self.non_static_gross_group_exposures = []
            gk = list(self.nonStaticGrossGroupDict.keys())
            gk.sort()
            for (j,groupName) in enumerate(gk):
                limit = self.nonStaticGrossGroupDict[groupName][1]
                classifications = self.nonStaticGrossGroupDict[groupName][0]
                c_keys = list(classifications.keys())
                c_keys.sort()
                N = self.nonStaticGrossGroupsCount[j]

                c = np.zeros((N, len(self.ind_wAbs)))
                for (i,d) in enumerate(c_keys):
                    indices = classifications[d]
                    self.non_static_gross_group_exposures.append((d, indices))
                    #subset = [self.ind_wAbs[i] for i in indices]
                    c[i, indices] = 1

                self.task.UpdateLinearConstraintSlice(self.nonStaticGrossGroups[j], self.ind_wAbs, c)


        def InitialiseGrossGroupConstraints(self, N, universe, rt):
            # first find out which ones are universal and which ones are not
            self.grossGroupsDataPointers = []
            self.grossGroupDict = {}
            self.grossIsUniversal = []
            
            for (groupName, limit) in self.grossGroupLimitTuples:
                groups = rt.dataRegistry.stringData(universe, groupName)
                self.grossGroupsDataPointers.append(groups)
                #isThisUniversal = groups._def().isUniversal()
                isThisUniversal = groups.rows() == 1
                self.grossIsUniversal.append(isThisUniversal)
                if isThisUniversal:
                    # DEAL WITH THE STATIC GROUPS FIRST
                    self.grossGroupDict[groupName] = ({}, limit) # copy over the limit
                    for ii in range(N):
                        classification = groups.getValue(0,ii)
                        if classification in self.grossGroupDict[groupName][0]:
                            self.grossGroupDict[groupName][0][classification].append(ii)
                        else:
                            self.grossGroupDict[groupName][0][classification] = [ii]
            
            # the above has accumulted data on the static groups
            self.static_gross_group_exposures = []
            gk = list(self.grossGroupDict.keys())
            gk.sort()            
            for groupName in gk:
                limit = self.grossGroupDict[groupName][1]
                classifications = self.grossGroupDict[groupName][0]
                c_keys = list(classifications.keys())
                c_keys.sort()
                numClassifications = len(c_keys)
                c = zeros((numClassifications, self.lenX))

                for (jj,d) in enumerate(c_keys):
                    indices = classifications[d]
                    self.static_gross_group_exposures.append((d, indices))
                    # c = zeros((1, self.lenX))
                    subset = [self.ind_wAbs[i] for i in indices]
                    # c[:, subset] = 1
                    c[jj, subset] = 1
                    # self.task.AddLinearConstraint(c, [mosek.boundkey.ra], 0, limit)

                self.task.AddLinearConstraint(c, [mosek.boundkey.ra]*numClassifications, np.zeros(numClassifications), limit*np.ones(numClassifications))
            

            # Now pre-set, conservatively, the non-static groups
            self.nonStaticGrossGroupsCount = []
            for (i, isThisUniversal) in enumerate(self.isUniversal):
                if (not isThisUniversal):
                    # we overestimate the number of groups to be on the safe side
                    limit = self.grossGroupLimitTuples[i][1]
                    groupName = self.grossGroupLimitTuples[i][0]
                    numUniqueEntries = rt.dataRegistry.stringData(universe, groupName + '_unique').cols()
                    self.nonStaticGrossGroupsCount.append(numUniqueEntries)
                    c = np.zeros((numUniqueEntries, self.lenX))
                    c[:, self.ind_wAbs] = 1
                    self.nonStaticGrossGroups.append(self.task.AddLinearConstraint(c, numUniqueEntries*[mosek.boundkey.ra], np.zeros(numUniqueEntries), limit*np.ones(numUniqueEntries)))

        
        def Update_w0(self, yvalues, forceLiquidation):
            yvalues[forceLiquidation] = 0 # FORCE IMMEDIATE LIQUIDATION, i.s. something used to be true and now it is false
            self.task.UpdateLinearConstraintLowerBound(self.cons_w0, yvalues)
            self.task.UpdateLinearConstraintUpperBound(self.cons_w0, yvalues)


        def Update_alpha(self, alpha):
            self.task.UpdateLinearConstraintLowerBound(self.cons_alpha, alpha)
            self.task.UpdateLinearConstraintUpperBound(self.cons_alpha, alpha)
            if self.l1normMaxretParam > 0:
                mixed_idx = self.ind_zAbs + self.ind_w
                alpha_sq = 0 
                for ii in range(self.numStocks):
                    alpha_sq += alpha[ii] * alpha[ii]
                alpha_vals = zeros(self.numStocks)
                for ii in range(self.numStocks):
                    alpha_vals[ii] = -self.l1normMaxretParam * alpha[ii]
                    alpha_vals[ii] /= alpha_sq
                mixed_vals = list(ones(self.numStocks)) + list(alpha_vals)
                self.task.UpdateObjectiveFunctionSlice(mixed_idx, np.array(mixed_vals))

        def Update_sign(self, alpha):
            self.task.UpdateLinearConstraintSlice(self.cons_sign, self.ind_dw, np.diag(alpha))

        def Update_smallCap(self, isSmallCap):
            if self.smallMarketCapPctLimit != -1:
                self.task.UpdateLinearConstraintSlice(self.cons_smallCap, self.ind_wAbs, isSmallCap)
            if self.smallMarketCapNetPctLimit != -1:
                self.task.UpdateLinearConstraintSlice(self.cons_smallCapNet, self.ind_w, isSmallCap)
            if self.largeMarketCapNetPctLimit != -1:
                self.task.UpdateLinearConstraintSlice(self.cons_largeCapNet, self.ind_w,  1 -isSmallCap)
            if self.useMaxSmallMarketCapTurnover:
                self.task.UpdateLinearConstraintSlice(self.cons_smallMarketCapTurnover, self.ind_dwAbs, isSmallCap)

        def Update_factorExposures(self, universe, numStocks, rt, di):
            # There is some repetition here. We already got the factor loadings before.
            self.exposures = []
            for (i, (factorName, limit)) in enumerate(self.factorLimitTuples):
                exposures = zeros(numStocks)
                factorExposures = rt.dataRegistry.floatData(universe, factorName)
                for j in range(numStocks):
                    exposures[j] = factorExposures.getValue(di, j)
                
                exposures[np.isnan(exposures)] = 0
                # TODO! I don't think the below needs to be scaled, but double check.
                self.task.UpdateLinearConstraintSlice(self.cons_factorExposureLimits[i], self.ind_w, exposures)
                self.exposures.append(exposures)
            
        def UpdateRiskMatrix(self, covMatrix):
            # TODO! This can be improved used factor model structure!
            sqrtCovMatrix = cholesky(covMatrix).T
            self.task.UpdateLinearConstraintSlice(self.cons_omega, self.ind_w, sqrtCovMatrix)


        def GetCovarianceMatrix(self, universe, numStocks, rt, di):

            # 1. get the factor covariance matrix, \Sigma
            # 2. for each stock, get the factor loadings for each factor, B_{i,j} = the ith stock's exposure to the jth factor.
            #    Note that this will be len(universe)*80
            # 3. for each stock, get the specific risk, S
            # covMatrix = B \Sigma B' + Diag(S)

            # PART 1: the factor covariance matrix
            factorCovMatrix = np.array(rt.dataRegistry.floatData(None,self.dataNameCovMatrix).rowArray(di)).reshape(self.numFactors,self.numFactors)

            # PART 2: the factor loadings
            # populate B
            B = np.zeros((numStocks, self.numFactors))
            for j in range(self.numFactors):
                jthFactor = rt.dataRegistry.floatData(universe, self.dataNamesFactorLoadings[j])
                for i in range(numStocks):
                    B[i,j] = jthFactor.getValue(di, i)

            B[np.isnan(B)] = 0 # nans mean there is no exposure

            # for a stock that is invalid, there may be no exposure anywhere
            sNoExp =  np.where((B == 0).sum(axis =1) == self.numFactors)[0] # get the list of factors with no exposure to anything
#            print('Found %i with no exposures' % len(sNoExp))

            if len(sNoExp) > 0:
                B[sNoExp, :] = np.nan
                B[sNoExp, :] = np.nanmean(B, axis=0)

            
            # PART 3: get the specific variances
            specific_risk = rt.dataRegistry.floatData(universe, self.dataNameSpecificRisk)
            S = np.zeros(numStocks)
            for i in range(numStocks):
                S[i] = specific_risk.getValue(di, i)
            
            S *= 0.01 / self.SQRTANNFACTOR
            S[np.isnan(S)] = np.nanmean(S)

            covMatrix = np.dot(B, np.dot(factorCovMatrix, B.T))*0.0001/self.ANNFACTOR + np.diag(S**2)
            return covMatrix

    
        def verifyOptimisationResultsJDM(self, rt):

            # store the positions
            todaysPos = np.copy(self.optimisedPositions)
            todaysPos = todaysPos.reshape(1, self.numStocks)
            forcedLiquidation = np.copy(self.forceLiquidation)
            forcedLiquidation = forcedLiquidation.reshape(1, self.numStocks)
            dPos = todaysPos
            if self.v_usdpos is None:
                self.v_usdpos = todaysPos
                self.v_dusdpos = dPos # assumes previous pos was 0!
                self.v_forcedLiquidation = forcedLiquidation
                self.v_tradingDatesIndices = [rt.di]
            else:
                self.v_usdpos = np.vstack([self.v_usdpos, todaysPos])
                dPos = self.v_usdpos[-1,:] - self.v_usdpos[-2, :]
                self.v_dusdpos = np.vstack([self.v_dusdpos, dPos])
                self.v_forcedLiquidation = np.vstack([self.v_forcedLiquidation, forcedLiquidation])
                self.v_tradingDatesIndices.append(rt.di)

            # get the basics:
            maxPosPctBook = todaysPos / self.bookSize # (1,n)
            maxPosPctADV = todaysPos / self.USDADV # (1,n)
            maxPosPctADV[todaysPos == 0] = 0
            maxPosPctADV[np.isnan(todaysPos)] = 0
            maxPosPctADV[abs(todaysPos) < 1] = 0
            maxTradePctBook = dPos / self.bookSize # (1,n)
            maxTradePctADV = dPos / self.USDADV # (1,n)
            maxShortTradePctLocates = dPos / self.locatesAmount
            maxShortTradePctLocates[maxShortTradePctLocates > 0] = 0
            maxShortTradePctLocates = abs(maxShortTradePctLocates)
            
            maxTradePctADV[dPos == 0] = 0
            maxTradePctADV[np.isnan(dPos)] = 0
            maxTradePctADV[abs(dPos) < 1 ] = 0
            exAnteVol = np.nan
            if self.useAnnualisedVol:
                exAnteVol = np.sqrt(np.dot(todaysPos, np.dot(self.covMatrix, todaysPos.T)))*np.sqrt(252)/self.bookSize # (1)
            translationRatio = np.corrcoef(todaysPos, self.alpha)[0][1]
            
            maxPosPctSharesOutLong = np.zeros((1, self.numStocks))
            maxPosPctSharesOutShort = np.zeros((1, self.numStocks))
            
            if self.useMaxPosPctSharesOutLong:
                maxPosPctSharesOutLong = todaysPos / self.sharesOut
                maxPosPctSharesOutLong[maxPosPctSharesOutLong < 0] = 0
            if self.useMaxPosPctSharesOutShort:
                maxPosPctSharesOutShort = todaysPos / self.sharesOut
                maxPosPctSharesOutShort[maxPosPctSharesOutShort > 0] = 0
                maxPosPctSharesOutShort = -maxPosPctSharesOutShort
            
            
            if self.smallMarketCapPctLimit != -1:
                smallMarketCapHoldings = np.dot(self.isSmallCap, abs(todaysPos).T) / self.bookSize
            else:
                smallMarketCapHoldings = np.nan

            if self.smallMarketCapNetPctLimit != -1:
                smallMarketCapNetHoldings = np.dot(self.isSmallCap, todaysPos.T) / self.bookSize
            else:
                smallMarketCapNetHoldings = np.nan

            if self.largeMarketCapNetPctLimit != -1:
                largeMarketCapNetHoldings = np.dot(1 - self.isSmallCap, todaysPos.T) / self.bookSize
            else:
                largeMarketCapNetHoldings = np.nan

            if self.useMaxSmallMarketCapTurnover:
                smallMarketCapTurnover = np.dot(self.isSmallCap * forcedLiquidation[0], abs(dPos.T)) / self.bookSize
            else:
                smallMarketCapTurnover = np.nan

            if self.maxSmallMarketCapMaxPos:
                smallMarketCapMaxPos = np.max(abs(self.isSmallCap * todaysPos))/self.bookSize
            else:
                smallMarketCapMaxPos = np.nan


            if self.existsFactorExposureLimits:
                if self.v_exposures is None:
                    self.v_exposures = []
                    for e in self.exposures:
                        this_exposure = np.copy(e)
                        this_exposure = this_exposure.reshape(1, self.numStocks)
                        port_exposure = np.dot(this_exposure, todaysPos.T)
                        port_exposure = port_exposure.reshape((1,1))
                        self.v_exposures.append(port_exposure)
                else:
                    for (i,e) in enumerate(self.exposures):
                        this_exposure = np.copy(e)
                        this_exposure = this_exposure.reshape(1, self.numStocks)
                        port_exposure = np.dot(this_exposure, todaysPos.T)
                        port_exposure = port_exposure.reshape((1,1))
                        self.v_exposures[i] = np.vstack([self.v_exposures[i], port_exposure])

            if self.existsGroupLimits:
                if self.v_groupExposures is None:
                    self.v_groupExposures = []
                    self.v_groupExposuresNames = []
                    for (e_name, e_indices) in self.static_group_exposures:
                        c = np.zeros((1, self.numStocks))
                        c[:,e_indices] = 1
                        port_exposure = np.dot(c, todaysPos.T)
                        port_exposure = port_exposure.reshape((1,1))
                        self.v_groupExposures.append(port_exposure)
                        self.v_groupExposuresNames.append(e_name)
                else:
                    for i in range(len(self.static_group_exposures)):
                        (e_name, e_indices) = self.static_group_exposures[i]
                        c = np.zeros((1, self.numStocks))
                        c[:,e_indices] = 1
                        port_exposure = np.dot(c, todaysPos.T)
                        port_exposure = port_exposure.reshape((1,1))
                        self.v_groupExposures[i] = np.vstack([self.v_groupExposures[i], port_exposure])
                            

            if self.v_maxPosPctBook is None:
                self.v_maxPosPctBook = maxPosPctBook
                self.v_maxPosPctADV = maxPosPctADV
                self.v_maxTradePctBook = maxTradePctBook
                self.v_maxTradePctADV = maxTradePctADV
                self.v_exAnteVol = exAnteVol
                self.v_translationRatio = translationRatio
                self.v_smallMarketCapHoldings = smallMarketCapHoldings
                self.v_smallMarketCapNetHoldings = smallMarketCapNetHoldings
                self.v_largeMarketCapNetHoldings = largeMarketCapNetHoldings
                self.v_smallMarketCapTurnover = smallMarketCapTurnover
                
                self.v_maxPosPctSharesOutLong = maxPosPctSharesOutLong
                self.v_maxPosPctSharesOutShort = maxPosPctSharesOutShort
                
                self.v_maxShortTradePctLocates = maxShortTradePctLocates
                self.v_smallMarketCapMaxPos = smallMarketCapMaxPos
            else:
                self.v_maxPosPctBook = np.vstack([self.v_maxPosPctBook, maxPosPctBook])
                self.v_maxPosPctADV = np.vstack([self.v_maxPosPctADV, maxPosPctADV])
                self.v_maxTradePctBook = np.vstack([self.v_maxTradePctBook, maxTradePctBook])
                self.v_maxTradePctADV = np.vstack([self.v_maxTradePctADV, maxTradePctADV])
                self.v_exAnteVol = np.vstack([self.v_exAnteVol, exAnteVol])
                self.v_translationRatio = np.vstack([self.v_translationRatio, translationRatio])
                self.v_smallMarketCapHoldings = np.hstack([self.v_smallMarketCapHoldings, smallMarketCapHoldings])
                self.v_smallMarketCapNetHoldings = np.hstack([self.v_smallMarketCapNetHoldings, smallMarketCapNetHoldings])
                self.v_largeMarketCapNetHoldings = np.hstack([self.v_largeMarketCapNetHoldings, largeMarketCapNetHoldings])
                self.v_smallMarketCapTurnover = np.hstack([self.v_smallMarketCapTurnover, smallMarketCapTurnover])
                
                self.v_maxPosPctSharesOutLong = np.vstack([self.v_maxPosPctSharesOutLong, maxPosPctSharesOutLong])
                self.v_maxPosPctSharesOutShort = np.vstack([self.v_maxPosPctSharesOutShort, maxPosPctSharesOutShort])
                
                self.v_maxShortTradePctLocates = np.vstack([self.v_maxShortTradePctLocates, maxShortTradePctLocates])
                self.v_smallMarketCapMaxPos = np.vstack([self.v_smallMarketCapMaxPos, maxSmallMarketCapMaxPos])
            return True
            

        def generateVerificationReport(self, rt):
            p_maxPosPctBook = abs(DataFrame(self.v_maxPosPctBook))
            p_maxPosPctADV = abs(DataFrame(self.v_maxPosPctADV))
            p_maxTradePctBook = abs(DataFrame(self.v_maxTradePctBook))
            p_maxTradePctADV = abs(DataFrame(self.v_maxTradePctADV))
            p_forcedLiquidation = DataFrame(self.v_forcedLiquidation.astype('int'))
            p_maxPosPctSharesOutLong = DataFrame(self.v_maxPosPctSharesOutLong)
            p_maxPosPctSharesOutShort = DataFrame(self.v_maxPosPctSharesOutShort)
            p_maxShortTradePctLocates = DataFrame(self.v_maxShortTradePctLocates)
            p_smallMarketCapMaxPos = DataFrame(self.v_smallMarketCapMaxPos)

            portfolioStats = DataFrame(index = p_maxPosPctBook.index)
            portfolioStats['maxPosPctBook'] = p_maxPosPctBook.max(axis=1)
            portfolioStats['maxPosPctADV'] = p_maxPosPctADV.max(axis=1)
            portfolioStats['maxTradePctBook'] = p_maxTradePctBook.max(axis=1)
            portfolioStats['maxTradePctBook_ex'] = (p_maxTradePctBook * (1-p_forcedLiquidation)).max(axis=1)
            portfolioStats['maxTradePctADV'] = p_maxTradePctADV.max(axis=1)
            portfolioStats['maxTradePctADV_ex'] = (p_maxTradePctADV * (1- p_forcedLiquidation)).max(axis=1)
            portfolioStats['maxTurnover'] = p_maxTradePctBook.sum(axis=1)
            portfolioStats['maxTurnover_ex'] = (p_maxTradePctBook * (1-p_forcedLiquidation)).sum(axis=1)
            portfolioStats['exAnteVol'] = self.v_exAnteVol
            portfolioStats['translationRatio'] = self.v_translationRatio
            portfolioStats['smallMarketCapHoldings'] = self.v_smallMarketCapHoldings
            portfolioStats['smallMarketCapNetHoldings'] = self.v_smallMarketCapNetHoldings
            portfolioStats['largeMarketCapNetHoldings'] = self.v_largeMarketCapNetHoldings
            portfolioStats['smallMarketCapTurnover'] = self.v_smallMarketCapTurnover            
            portfolioStats['maxPosPctSharesOutLong'] = p_maxPosPctSharesOutLong.max(axis=1)
            portfolioStats['maxPosPctSharesOutShort'] = p_maxPosPctSharesOutShort.max(axis=1)
            portfolioStats['maxShortTradePctLocates'] = p_maxShortTradePctLocates.max(axis=1)
            portfolioStats['smallMarketCapMaxPos'] = p_smallMarketCapMaxPos

            if self.existsFactorExposureLimits:
                for i in range(len(self.v_exposures)):
                    factorName = self.factorLimitTuples[i][0]
                    portfolioStats['factor_' + factorName] = self.v_exposures[i] / self.bookSize

            if self.existsGroupLimits:
                for i in range(len(self.v_groupExposures)):
                    groupName = self.v_groupExposuresNames[i]
                    portfolioStats['group_' + groupName] = self.v_groupExposures[i] / self.bookSize


            if self.verificationFile != "":
                tradingDates = np.array(rt.dates)
                tradingDates = tradingDates[self.v_tradingDatesIndices]
                portfolioStats.index = pd.to_datetime(tradingDates,format='%Y%m%d')
                portfolioStats.to_csv(self.verificationFile)

        def run(self, rt):
            self.runInternal(rt)
            self.m_didOptimise = True
            if self.doVerify():
                self.verifyOptimisationResultsJDM(rt)

            if rt.isLastDi() and self.doVerify():
                self.generateVerificationReport(rt)            

        def runInternal(self, rt):
            
            try:
                calcData   = self.calcData()
                di          = rt.di - self.delay
                self.universe    = calcData.universe
                self.numStocks = calcData.universe.size(calcData.di)
                adv20     = rt.dataRegistry.floatData(self.universe, 'baseData.adv20')
                prices    = rt.dataRegistry.floatData(self.universe, 'baseData.adjClosePrice')
                mcap      = rt.dataRegistry.floatData(self.universe, 'baseData.mcap')
                unisub = None

                if self.tradeableUniverse is not None:
                    unisub = rt.dataRegistry.getUniverse(self.tradeableUniverse)

                # need this for turnover purposes among others

                spreadData = None
                spreadValues = np.zeros(self.numStocks)

                if self.useSpreadData:
                    spreadData = rt.dataRegistry.floatData(self.universe, self.spreadData)
                    spreadValues = np.asarray(spreadData.rowArray(di))
                    spreadValues[np.isnan(spreadValues)] = 10


                locatesData = None
                if self.locatesSource != "":
                    locatesData = rt.dataRegistry.floatData(self.universe, self.locatesSource)
                
                ydata       = calcData.yesterday
                if ydata is None:
                    yvalues = np.zeros(self.numStocks)
                else:
                    yvalues = np.array([v for v in calcData.yesterday.alphaNotionals])

                    # Load the last l1normMaxretParam from yesterday's data
                    serialDataStr = ydata.getCustomData("PyMOSEK_State")

                    if serialDataStr:
                        serialData = json.loads(serialDataStr)
                        if "l1normMaxretParam" in serialData:
                            self.l1normMaxretParam = serialData["l1normMaxretParam"]
                        if "lastOptimisedBooksize" in serialData:
                            self.lastOptimisedBooksize = serialData["lastOptimisedBooksize"]

                if not self.hasInitialised:
                    self.InitialiseOptimisation(self.numStocks, self.universe, rt)
                    self.hasInitialised = True # only need to do this once!
                
                self.isValidToday = zeros(self.numStocks, dtype=bool)
                self.isValidYesterday = zeros(self.numStocks, dtype=bool)
                self.isRestricted = zeros(self.numStocks, dtype=bool)
                self.isShortRestricted = zeros(self.numStocks, dtype=bool)
                self.isInSubUniverse = zeros(self.numStocks, dtype=bool)

                self.alpha = zeros(self.numStocks)

                self.ADV=None
                self.pxs=None
                # are we using anything that requires ADV?
                if self.useMaxPosPctADV or self.useMaxTradePctADV:
                    # self.ADV = BamUtil.floatRowAsNPArray(adv20, di)
                    # self.pxs = BamUtil.floatRowAsNPArray(prices, di)
                    self.ADV = zeros(self.numStocks)
                    self.pxs = zeros(self.numStocks)
                    for ii in range(self.numStocks):
                        self.ADV[ii] = adv20.getValue(di, ii)
                        self.pxs[ii] = prices.getValue(di, ii)
                        
                
                self.locatesAmount = np.inf * np.ones(self.numStocks)
                if locatesData is not None:
                    self.locatesAmount = np.asarray(locatesData.rowArray(rt.di))
                    

                self.sharesOut = None
                if self.useMaxPosPctSharesOutLong or self.useMaxPosPctSharesOutShort:
                    dollarSharesOut = rt.dataRegistry.floatData(self.universe, 'derivedData.dollarSharesOutstanding')
                    self.sharesOut = np.asarray(dollarSharesOut.rowArray(di)) * 1e6 # JDMREMOVE
                    self.sharesOut[np.isnan(self.sharesOut)] = np.inf

                # load up the data; this is horribly inefficient.
                for ii in range(self.numStocks):
                    self.isValidToday[ii] = self.universe.isValid(rt.di, ii) == 1
                    # restricted flags
                    self.isRestricted[ii] = self.universe.isRestricted(rt, rt.di, ii)
                    self.isShortRestricted[ii] = self.universe.isShortRestricted(rt, rt.di, ii)
                    if ydata is not None:
                        # check this logic
                        self.isValidYesterday[ii] = ydata.universe.isValid(rt.di-1, ii) == 1

                numValidStocks = np.sum(self.isValidToday)
                if self.overrideMaxPosPctBook > 0:
                    self.maxPosPctBook = self.bookSize * self.overrideMaxPosPctBook / numValidStocks

                if self.useSqConstraint:
                    self.UpdateSqOverallConstraint(numValidStocks)
                
                self.isSmallCap = zeros(self.numStocks)
                if self.existsMarketCapLimit():
                    self.mcaps = zeros(self.numStocks)
                    for ii in range(self.numStocks):
                        self.mcaps[ii] = mcap.getValue(di, ii)
                    self.mcaps *= 1e6
                    self.isSmallCap[self.mcaps < self.smallMarketCapSize] = 1 # set to true
                

                self.alpha = np.asarray(calcData.alphaNotionals)
                self.alpha[np.isnan(self.alpha)] = 0

                for ii in range(self.numStocks):
                    if abs(self.alpha[ii]) < 1e-5:
                        self.alpha[ii] = 0

                self.forceLiquidation = ~self.isValidToday

                isShortHardLiquidate = zeros(self.numStocks, dtype=bool)
                if self.useShortHardLiquidate < 1: # some action will be required
                    shortHardLiquidate = rt.dataRegistry.floatData(self.universe, 'derivedData.shortHardLiquidate')
                    for ii in range(self.numStocks):
                        isShortHardLiquidate[ii] = shortHardLiquidate.getValue(rt.di, ii) == 1

                    if self.useShortHardLiquidate == 0: # then we force to 0 and no contribution to turnover constraints
                        self.forceShortLiquidation = yvalues < 0 # what are we currently short?
                        self.forceShortLiquidation = self.forceShortLiquidation & isShortHardLiquidate # forceShortLiquidation will be true is BOTH the position is SHORT and we need to short liquidate
                        self.forceLiquidation = self.forceLiquidation | self.forceShortLiquidation

                hasEarningsExposure = zeros(self.numStocks, dtype=bool)
                if self.earningsExposure < 1:
                    earningsExposure = rt.dataRegistry.floatData(self.universe, 'derivedData.minimiseEarningsExposure')
                    for ii in range(self.numStocks):
                        hasEarningsExposure[ii] = earningsExposure.getValue(rt.di, ii) == 1

                dwLongRestrict = self.isRestricted | self.forceLiquidation
                dwShortRestrict = self.isRestricted | self.forceLiquidation | self.isShortRestricted

                if unisub is not None:
                    # first extract all the ids of elements in the sub universe
                    unisub_ids = []
                    unisub_numStocks = unisub.size(rt.di) # how many stocks in the smaller universe
                    for ii in range(unisub_numStocks):
                        if unisub.isValid(rt.di, ii):
                            sec = unisub.security(rt.di, ii).index
                            unisub_ids.append(sec)
                    # now go through the default universe and set self.isInSubUniverse to true only if that id is in the above
                    for ii in range(self.numStocks):
                        sec_id = self.universe.security(rt.di, ii).index
                        self.isInSubUniverse[ii] = sec_id in unisub_ids

                    dwLongRestrict = dwLongRestrict | (~self.isInSubUniverse)
                    dwShortRestrict = dwShortRestrict | (~self.isInSubUniverse)

                self.UpdateVariableBounds(self.numStocks, yvalues, self.forceLiquidation, dwLongRestrict, dwShortRestrict, self.ADV, self.pxs, isShortHardLiquidate, hasEarningsExposure, self.sharesOut, self.locatesAmount * self.locatesMultiplier)
                self.Update_w0(yvalues, self.forceLiquidation)

                if self.useAnnualisedVol:
                    self.covMatrix = self.GetCovarianceMatrix(self.universe, self.numStocks, rt, di)
                    self.UpdateRiskMatrix(self.covMatrix)

                if self.existsFactorExposureLimits:
                    self.Update_factorExposures(self.universe, self.numStocks, rt, di)

                if self.existsMarketCapLimit():
                    self.Update_smallCap(self.isSmallCap)

                if self.preserveSign:
                    self.Update_sign(self.alpha)

                if self.objFun == ObjFun.l1norm:
                    self.Update_alpha(self.alpha)
                elif self.objFun == ObjFun.maxret:
                    self.task.UpdateObjectiveFunctionSlice(self.ind_w, -self.alpha)
                elif self.objFun == ObjFun.maxretminuscosts:
                    self.task.UpdateObjectiveFunctionSlice(self.ind_dw, -numValidStocks * 1e4 * self.alpha / self.bookSize)
                    self.task.UpdateObjectiveFunctionSlice(self.ind_dwAbs, self.tCostMultiplier * spreadValues)
                    

                if self.existsGroupLimits:
                    self.UpdateNonStaticGroups(self.numStocks, self.universe, rt, di)
                
                if self.existsGrossGroupLimits:
                    self.UpdateNonStaticGrossGroups(self.numStocks, self.universe, rt, di)

                (status, xx) = self.task.Optimize()
                if status != 1: # THE PROBLEM HAS FAILED!
                    (status, xx) = self.RelaxConstraints(self.numStocks, yvalues, self.forceLiquidation, dwLongRestrict, dwShortRestrict, self.ADV, self.pxs, isShortHardLiquidate, hasEarningsExposure, self.sharesOut, self.locatesAmount * self.locatesMultiplier)
                
                optimizedBooksize = sum(abs(xx[self.ind_w]))

                if self.l1normMaxretParam > 0 and hasattr(self, 'lastOptimisedBooksize'):
                    booksizeThreshold = 0.9 * self.bookSize
                    if self.lastOptimisedBooksize > optimizedBooksize and optimizedBooksize < booksizeThreshold and optimizedBooksize > 1e-5:
                        self.l1normMaxretParam *= 2
                        self.info('Adjusted l1normMaxretParam = {}'.format(self.l1normMaxretParam))
                self.lastOptimisedBooksize = optimizedBooksize

                optimisedPositions = xx[self.ind_w] if status == 1 else yvalues
                self.optimisedPositions = optimisedPositions

                if (status != 1) and (not self.keepPrevPosOnFail):
                    self.error('Unable to optimise and NOT maintaining yesterdays positions. Throwing exception.')
                    self.error('Exiting.')
                    rt.exit(-666)

                for ii in range(self.numStocks):
                    if self.universe.isValid(rt.di,ii):
                        calcData.setAlphaValue(ii, optimisedPositions[ii])

                serialData = {
                    "lastOptimisedBooksize" : self.lastOptimisedBooksize,
                    "l1normMaxretParam": self.l1normMaxretParam
                }

                serialDataStr = json.dumps(serialData)
                calcData.setCustomData("PyMOSEK_State", serialDataStr)

            except Exception as e:
                self.error(traceback.format_exc())
                self.error('Fatal exception: exiting')
                rt.exit(-106)
                #print(traceback.format_exc())

except Exception as e:
    sys.exit(-106)
    #print(traceback.format_exc())
