### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import traceback
import datetime as DT

import PyUtil
import sys

import numpy as np
import pandas as pd

try:
    import Framework as FW
    from PyBase import PyDataLoader

    class wsh_EU_Next_ED(PyDataLoader):
        def __init__(self, baseObject):
            super().__init__(baseObject, "wsh_EU_Next_ED")
            self.dataBuilt = False
            self.divAmountMap = {}

        def initDatasets(self, dr, datasets):
            try:
                config                  = self.config()
                self.datasetName        = config.get("datasetName")

                ds = FW.Dataset("wsh")
                ftype = FW.DataType.kString;
                ds.add(FW.DataValue("Next_ED", ftype, FW.DataType.size(ftype), FW.DataFrequency.kDaily, 0))
                ds.add(FW.DataValue("Last_X-Dividend_Date", ftype, FW.DataType.size(ftype), FW.DataFrequency.kDaily, 0))

                ftype = FW.DataType.kFloat
                ds.add(FW.DataValue("Last_X-Dividend_Amount", ftype, FW.DataType.size(ftype), FW.DataFrequency.kDaily, 0))

                datasets.add(ds)

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def build(self, dr, dci, frame):
            try:
                dataName = dci._def.name
                self.debug("BamRestCalendar::build: %s" % (dataName))

                if dataName == "Next_ED":
                    return self.buildNextED(dr, dci, frame)
                elif dataName == "Last_X-Dividend_Date":
                    return self.buildDivExDate(dr, dci, frame)
                elif dataName == "Last_X-Dividend_Amount":
                    return self.buildDivAmount(dr, dci, frame)

            except Exception as e:
                print(traceback.format_exc())
                raise e

        def buildDivAmount(self, dr, dci, frame):
            try:
                secMaster       = dr.getSecurityMaster()
                numSecurities   = secMaster.size(dci.di)
                dataPtr         = FW.FloatMatrixData(secMaster, dci._def, 1, numSecurities, False)
                diSrc           = dci.di

                numDivAmounts   = 0

                for ii in range(numSecurities):
                    sec         = secMaster.security(dci.di, ii)
                    key         = sec.uuidStr() + str(dci.di)

                    damount     = np.nan

                    if key in self.divAmountMap:
                        numDivAmounts += 1
                        damount = self.divAmountMap[key]

                    dataPtr.setValue(0, ii, damount)

                frame.setData(BamUtil.script_sptr(dataPtr))
                frame.dataOffset = 0
                frame.dataSize = dataPtr.dataSize()

                self.debug("[%d - di = %d] - Div amount Count: %d" % (dci.rt.getDate(dci.di), dci.di, numDivAmounts))
                
            except Exception as e:
                print(traceback.format_exc())

        def buildDivExDate(self, dr, dci, frame):
            try:
                secMaster       = dr.getSecurityMaster()
                divExDate       = dr.int64VarData(secMaster, "wsh.Div_ex_div_date")
                divAmount       = dr.floatVarData(secMaster, "wsh.Div_amount")

                numSecurities   = secMaster.size(dci.di)
                dataPtr         = FW.StringData(dci.universe, numSecurities, dci._def)
                diSrc           = dci.di
                todayDate       = BamUtil.intToDate(dci.rt.getDate(dci.di))

                numDivDates     = 0

                for ii in range(numSecurities):
                    sec         = secMaster.security(dci.di, ii)
                    depth       = divExDate.getSize(diSrc, ii)

                    if depth == 0:
                        continue

                    assert divAmount.getSize(diSrc, ii) == depth, "Size mismatch between divAmount: %d - divExDate: %d" % (divAmount.getSize(diSrc, ii), depth)
                    bestDateIndex = -1
                    minDate = None

                    for kk in range(depth):
                        divDate = pd.Timestamp(int(divExDate.getString(dci.di, ii, kk)))

                        if divDate >= todayDate:
                            if ((minDate and divDate < minDate) or minDate is None):
                                bestDateIndex = kk

                    ddate = pd.Timestamp(0)
                    damount = np.nan

                    if bestDateIndex >= 0:
                        numDivDates += 1
                        ddate = pd.Timestamp(int(divExDate.getString(dci.di, ii, bestDateIndex)))
                        damount = divAmount.getValueKK(dci.di, ii, bestDateIndex)

                    dataPtr.setValue(0, ii, BamUtil.dateToStr(ddate))
                    self.divAmountMap[sec.uuidStr() + str(dci.di)] = damount

                frame.setData(BamUtil.script_sptr(dataPtr))
                frame.dataOffset = 0
                frame.dataSize = dataPtr.dataSize()

                self.debug("[%d - di = %d] - Div Dates Count: %d" % (dci.rt.getDate(dci.di), dci.di, numDivDates))

            except Exception as e:
                print(traceback.format_exc())

        def buildNextED(self, dr, dci, frame):
            try:
                secMaster       = dr.getSecurityMaster()
                eventTypes      = dr.stringData(secMaster, "wsh.EventType")
                financialDescs  = dr.stringData(secMaster, "wsh.FinancialDescription")
                timePeriods     = dr.stringData(secMaster, "wsh.TimePeriod")
                meetingDates    = dr.int64VarData(secMaster, "wsh.MeetingDate")
                numSecurities   = secMaster.size(dci.di)
                dataPtr         = FW.StringData(dci.universe, numSecurities, dci._def)
                diSrc           = max(dci.di - 1, 0)
                numEarnings     = 0

                for ii in range(numSecurities):
                    sec         = secMaster.security(dci.di, ii)
                    eventType   = eventTypes.getString(diSrc, ii, 0)
                    finDesc     = financialDescs.getString(diSrc, ii, 0)
                    timePeriod  = timePeriods.getString(diSrc, ii, 0)

                    if not eventType or not finDesc or not timePeriod:
                        continue

                    allEventTypes = [x.strip() for x in eventType.split("!&!")]
                    allFinDescs = [x.strip() for x in finDesc.split("!&!")]
                    allTimePeriods = [x.strip() for x in timePeriod.split("!&!")]
                    numMeetingDates = meetingDates.getSize(diSrc, ii)

                    if numMeetingDates == 0:
                        continue

                    assert len(allEventTypes) == len(allFinDescs), "[%s] - EventTypes: %s and FinancialDescs: %s do not match in length [%d / %d]" % (sec.uuidStr(), eventType, finDesc, len(allEventTypes), len(allFinDescs))
                    assert len(allEventTypes) == numMeetingDates, "[%s] - Number of event types [%s] do not match num meeting dates [%d / %d]" % (sec.uuidStr(), eventType, len(allEventTypes), numMeetingDates)

                    for kk in range(numMeetingDates):
                        eventType = allEventTypes[kk]
                        finDesc = allFinDescs[kk]
                        timePeriod = allTimePeriods[kk]
                        meetingDateStr = meetingDates.getString(diSrc, ii, kk)

                        if eventType == "Results & Trading Statements" and (finDesc.startswith("Q") or timePeriod.startswith("Q")):
                            numEarnings += 1
                            meetingDate = pd.Timestamp(int(meetingDateStr))
                            meetingDateStr = BamUtil.dateToStr(meetingDate)
                            # self.log("Event Type: %s - %s" % (eventType, meetingDateStr))
                            dataPtr.setValue(0, ii, meetingDateStr)
                            break
                        else:
                            dataPtr.setValue(0, ii, "")

                frame.setData(BamUtil.script_sptr(dataPtr))
                frame.dataOffset = 0
                frame.dataSize = dataPtr.dataSize()

                self.debug("[%d - di = %d] - Next_ED Count: %d" % (dci.rt.getDate(dci.di), dci.di, numEarnings))

            except Exception as e:
                print(traceback.format_exc())

        def preDataBuild(self, dr, dci):
            pass

        def postDataBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())

        def preBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())

        def postBuild(self, dr, dci):
            try:
                pass
            except Exception as e:
                print(traceback.format_exc())

except Exception as e:
    print(traceback.format_exc())
    raise e
