### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import traceback
import datetime as DT

import PyUtil
import sys

import numpy as np
import pandas as pd
from pandas.tseries.offsets import BDay

try:
    import Framework as FW
    from PyBase import PyDataLoader

    class wsh_daysToEarnings(PyDataLoader):
        def __init__(self, baseObject):
            super().__init__(baseObject, "wsh_daysToEarnings")
            self.dataBuilt = False
            self.lookUp = {}

        def initDatasets(self, dr, datasets):
            try:
                config                  = self.config()
                self.datasetName        = config.get("datasetName")

                ds = FW.Dataset(self.datasetName)
                ftype = FW.DataType.kFloat;
                ds.add(FW.DataValue("daysToEarnings", ftype, FW.DataType.size(ftype), FW.DataFrequency.kDaily, 0))

                datasets.add(ds)

            except Exception as e:
                print(traceback.format_exc())
                raise e


        def build(self, dr, dci, frame):
            try:
                secMaster               = dr.getSecurityMaster()
                nextEarningsDateDataId  = self.config().getString("earningsDateDataId", "wsh.Next_ED")
                nextEarningsDateData    = dr.stringData(secMaster, nextEarningsDateDataId)
                numSecurities           = secMaster.size(dci.di)
                dataPtr                 = FW.FloatMatrixData(None, dci._def, 1, numSecurities, False)
                diSrc                   = max(dci.di - 1, 0)
                numEarnings             = 0
                d1                      = max(dci.di - 1, 0)
                todayDate               = BamUtil.intToDate(dci.rt.getDate(dci.di))

                for ii in range(numSecurities):
                    nextEarningsDateStr = nextEarningsDateData.getString(d1, ii, 0)
                    sec                 = secMaster.security(dci.di, ii)
                    secId               = sec.uuidStr() + "-" + nextEarningsDateStr

                    dataPtr.makeInvalid(0, ii)

                    if nextEarningsDateStr and nextEarningsDateStr != "1970-01-01":
                        nextEarningsDate = BamUtil.strToDate(nextEarningsDateStr)
                        tradingDaysToEarnings = np.nan

                        if secId not in self.lookUp:
                            dates            = pd.date_range(todayDate, nextEarningsDate, freq = BDay())
                            daysToEarnings   = len(dates)
                            tradingDaysToEarnings = float(daysToEarnings)

                            if daysToEarnings >= 0:
                                tradingDaysToEarnings = 0.0
                                for i in range(0, len(dates) - 1):
                                    idate = BamUtil.dateToInt(dates[i])
                                    if idate not in dci.rt.holidays:
                                        tradingDaysToEarnings += 1.0

                            self.lookUp[secId] = {
                                "date": nextEarningsDateStr,
                                "tradingDaysToEarnings": tradingDaysToEarnings
                            }
                        else:
                            info = self.lookUp[secId]
                            tradingDaysToEarnings = info["tradingDaysToEarnings"]

                            if tradingDaysToEarnings > 0.0:
                                tradingDaysToEarnings -= 1.0
                                info["tradingDaysToEarnings"] = tradingDaysToEarnings
                            else:
                                tradingDaysToEarnings = np.nan

                        dataPtr.setValue(0, ii, tradingDaysToEarnings)

                frame.setData(BamUtil.script_sptr(dataPtr))
                frame.dataOffset = 0
                frame.dataSize = dataPtr.dataSize()

                self.debug("[%d - di = %d] - Built days to earnings" % (dci.rt.getDate(dci.di), dci.di))

            except Exception as e:
                print(traceback.format_exc())

        def preDataBuild(self, dr, dci):
            pass

        def postDataBuild(self, dr, dci):
            pass

        def preBuild(self, dr, dci):
            pass

        def postBuild(self, dr, dci):
            pass

except Exception as e:
    print(traceback.format_exc())
    raise e
