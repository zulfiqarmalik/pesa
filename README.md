Build Instructions:

[PESA] = Directory where you cloned

Requirements:
	- scons version > 3.0.0
	- cmake version > 3.1
	- [Windows] Install MSVC v14.0 or above. Must have VC 14.0 installed though because of Python 3.5.1
	- [Linux] Install gcc version >= 5.4.x (or get the admin to do it)
		- sudo add-apt-repository ppa:ubuntu-toolchain-r/test
		- sudo apt-get update
		- sudo apt-get install gcc-5 g++-5
		- sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-5 60 --slave /usr/bin/g++ g++ /usr/bin/g++-5

	- [Linux] Install autoconf tools
	- python 3.5.1 at least
		- numpy 
		- requests
	- If you are using Visual Studio 2017, then make sure to install the Visual Studio 2015 tool sets for Python building
		- Refer to: https://developercommunity.visualstudio.com/content/problem/48806/cant-find-v140-in-visual-studio-2017.html
	- Create a directory called "drv" anywhere on your local machine and ...
		- [Windows] map it as a Y: drive in explorer
		- [Linux/Mac] ln -s ~/drv <path_to_drv>
	- [Linux] export LD_LIBRARY_PATH=./:$LD_LIBRARY_PATH

Build:
	1. Download boost 1.61.* or above from - http://www.boost.org/users/history/version_1_61_0.html
	2. Extract boost to [PESA]/Thirdparty/boost
	3. Install python, preferably 3.5.1 or above
	4. Install conan: pip install conan. Add the following remote repositories:
		- conan remote add conan-community "https://api.bintray.com/conan/conan-community/conan"
		- [Linux] $HOME/.conan/profiles/default. Set value "compiler.libcxx=libstdc++11" under the [settings]. If this doesn't exist then run conan install once and it will be created
		- [Windows] $HOME/.conan/profiles/default. Set "compiler.version=14" under [settings]
	5. Install scons build tool vertion >= v3.0 from - http://scons.org/
	6. Running scons on windows will require running 'scons.py', on other platforms you can just type 'scons' (without the quotes). Lets call this just scons
	7. In [PESA]: scons -Q init=1 localInit=1 (add release=1 for release builds). This will initialise Thirdparty dependencies
	8. In [PESA]: scons -j4 (or however many threads you want) to do the build
	9. Add site-packages of your main Python installation as part of your PYTHONPATH. This will ensure that the embedded python picks up the packages that you install lcoally using either, pip or anaconda
	