#!python

### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os
import glob
import shutil
import sys

g_prevVersion   = "1.0.1"
g_version       = "1.0.2"

linux = False
windows = False
mac = False

def resolveLibs(libs):
    rlibs = []

    for lib in libs:
#        if lib == "python" and linux is True:
#            rlibs = rlibs + ["python35"]
#        else:
        if lib in conan:
            rlibs = rlibs + conan[lib]['LIBS']
        else:
            rlibs.append(lib)

        # print("\tLINKING => " + lib)

    return rlibs

def resolveInclude(include):
    return include.replace("{$PLATFORM}", platform)

def sourceDir(path):
    sourceFiles = []
    currDir = env["currDir"]
    fullPath = os.path.join(currDir, path, "*.cpp")

    print("Getting all source files under: %s" % (fullPath))

    for filename in glob.glob(fullPath):
        sourceFiles.append(filename)

    return sourceFiles


def copyFiles(paths, searchTerms = None, alwaysCopy = False, searchWord = "*.", targetDir = None, prefix = '', suffix = ''):
    if searchTerms is None:
        searchTerms = []
        for soName in env["soName"]:
            searchTerms.append(searchWord + soName + "*")

    elif type(searchTerms) is not list:
        searchTerms = [searchTerms]
    
    currDir = env["currDir"]

    # Target dir has to be relative to the output dir. You cannot use 
    # this function to copy to random directories!
    if targetDir is None:
        targetDir = outputDir
    else:
        targetDir = os.path.join(outputDir, targetDir)
        
        if os.path.exists(targetDir) is False:
            print("Creating dir: %s" % (targetDir))
            os.makedirs(targetDir)

    for searchTerm in searchTerms:
        for path in paths:
            # print("Copying file(s) from path: %s ..." % (path))
            
            filenames = glob.glob(os.path.join(currDir, path, prefix + searchTerm + suffix))

            for filename in filenames:
                fname = os.path.splitext(os.path.basename(filename))[0]
                targetFilenames = [os.path.join(targetDir, os.path.basename(filename))]

                for targetFilename in targetFilenames:
                    if (targetFilenames is not None and (os.path.exists(targetFilename) is False) or alwaysCopy is True):
                        shutil.copy(filename, targetFilename)

def includeDirs(dirs):
    for idir in dirs:
        idirResolved = resolveInclude(idir)
        # print("Including directory: %s" % (idirResolved))
        env.Append(CCFLAGS = "-I" + idirResolved)

def sourceDirs(paths, pattern = "*.cpp"):
    sources = []
    currDir = os.getcwd()

    for path in paths:
        print(os.path.join(currDir, path))
        for filename in glob.glob(os.path.join(currDir, path, pattern)):
            sources.append(filename)

    return sources

buildConfig     = "debug"
outputDir       = "./build/debug"
debug           = True
release         = ARGUMENTS.get("release", 0)
swigBuild       = ARGUMENTS.get("swig", 0)
dist            = ARGUMENTS.get("dist", 0)
sdk             = ARGUMENTS.get("sdk", None)
profile         = ARGUMENTS.get("profile", 0)
clean           = ARGUMENTS.get("clean", 0)
version         = ARGUMENTS.get("version", g_version)
prevVersion     = ARGUMENTS.get("prevVersion", g_prevVersion)
noVec           = ARGUMENTS.get("noVec", 0)
doInit          = ARGUMENTS.get("init", 0)
doLocalInit     = ARGUMENTS.get("localInit", 0)
compiler        = "clang++"
platform        = None

if sdk is not None:
    release = 1

if dist:
    release = 1
    assert version, "Must specify a version on the command line to make a distribution build!"

conanSConscript = os.path.join(os.getcwd(), "SConscript_conan_" + ("release" if release else "debug"))

if os.path.isfile(conanSConscript) is False:
    print("Conan SConscript file: %s NOT FOUND. Forcing an INIT" % (conanSConscript))
    doInit = True
    doLocalInit = True


customPackages = [{
    "name": "sparsepp",
    "path": "./ConanPkg/sparsepp/"
}, {
    "name": "Poco",
    "path": "./ConanPkg/Poco/"
}, {
    "name": "MongoCxx",
    "path": "./ConanPkg/mongo/"
}, {
    "name": "Python",
    "path": "./ConanPkg/python/",
    "buildType": "Release"
}, {
    "name": "cURL",
    "path": "./ConanPkg/libcurl/"
}]

if doInit:
    print("===== pSim INIT =====")
    import subprocess

    conanArgs = []

    boostPath = os.path.join(os.getcwd(), "Thirdparty", "boost")
    print("Setting BOOST_INCLUDEDIR: %s" % (boostPath))
    os.environ["BOOST_INCLUDEDIR"] = boostPath

    if doLocalInit:
        print("Building local packages!")

        # os.environ("BOOST_INCLUDEDIR", os.path.join(os.getcwd(), "Thirdparty", "boost"))
        
        for package in customPackages:

            if "buildType" not in package:
                conanArgs = ["-s", "build_type=Release"] if release else ["-s", "build_type=Debug"]
            else:
                print("Package: %s - Forcing build type: %s" % (package["name"], package["buildType"]))
                conanArgs = ["-s", "build_type=" + package["buildType"]]

            command = (["conan", "create", package["path"], "psim/stable", "--build", "missing"] + conanArgs)
            print("Installing %s package. Command: '%s' [CWD: %s]" % (package["name"], " ".join(command), os.getcwd()))

            conanPackageProc = subprocess.Popen(command, shell = False, cwd = os.getcwd())

            while conanPackageProc.poll() is None:
                pass

            print("%s package installed locally!" % (package["name"]))

    print("Doing conan install .")
    conanInstallProc = subprocess.Popen((["conan", "install", ".", "--build", "missing"] + conanArgs), shell = False, cwd = os.getcwd())

    while conanInstallProc.poll() is None:
        pass

    if os.path.isfile(conanSConscript):
        os.remove(conanSConscript)

    os.rename(os.path.join(os.getcwd(), "SConscript_conan"), conanSConscript)

    print("===== pSim INIT FINISHED =====")

ccFlags = []
# CPP Defines ...
cppDefines = []

if sys.platform == "darwin":
    ccFlags.append(["-std=c++14"])
    cppDefines.append("__MACOS__")
    platform = "MacOS"
    mac = True
elif sys.platform == "win32":
    cppDefines.append("__WINDOWS__")
    cppDefines.append("_WINDOWS")
    cppDefines.append("__WIN32__")
    platform = "Windows"
    windows = True
elif sys.platform == "win64":
    cppDefines.append("__WINDOWS__")
    cppDefines.append("_WINDOWS")
    cppDefines.append("__WIN64__")
    cppDefines.append("__WIN32__")
    platform = "Windows"
    windows = True
else:
    compiler = "g++"
    ccFlags.append(["-std=c++14", "-fpack-struct=8"])
    cppDefines.append("__LINUX__")
    cppDefines.append("_GLIBCXX_USE_CXX11_ABI=1")
    platform = "Linux"
    linux = True

if int(noVec):
    cppDefines.append("EIGEN_DONT_VECTORIZE")

if int(release):
    debug = False
    release = True
    buildConfig = "release"
    outputDir = "./build/release"
    print("Making RELEASE build ...")

    # ccFlags.append('-s')
    if platform is not "Windows":
        ccFlags.append('-O2')
        ccFlags.append("-march=native")
        # ccFlags.append("-fopenmp")
    else:
        ccFlags.append('/O2')
        ccFlags.append('/Oi')
        ccFlags.append('/Ot')
        ccFlags.append("/MD")
        ccFlags.append("-openmp")

    cppDefines.append("NDEBUG")
else:
    print("Making DEBUG build ...")
    if platform is not "Windows":
        ccFlags.append('-g')
        ccFlags.append('-O0')
    else:
        ccFlags.append("/MDd")
        ccFlags.append('/Zi')
        ccFlags.append('/W3')

    cppDefines.append("EIGEN_DONT_VECTORIZE")
    cppDefines.append("DEBUG")
    cppDefines.append("_DEBUG")
    # cppDefines.append("OMP_NUM_THREADS=16")

    if int(profile):
        cppDefines.append("__PROFILER_ENABLED__")
        print("##################################################################################################################################")
        print("######################################################## PROFILER ENABLED ########################################################")
        print("##################################################################################################################################")

outputDir = os.path.join(os.getcwd(), outputDir, platform.lower())

conan = SConscript(conanSConscript)
# env.MergeFlags(conan['conan'])

includePaths = conan['conan']['CPPPATH'] + ['./', "./Thirdparty/boost"] 
libPaths = conan['conan']['LIBPATH']

if int(clean):
    print("Cleaning output directories ...")

    try:
        # shutil.rmtree(outputDir, ignore_errors = True)
        for root, dirs, files in os.walk(outputDir):

            for f in files:
                path = os.path.join(root, f)
                print("Deleting file: %s" % (path))
                os.unlink(path)

            for d in dirs:
                path = os.path.join(root, d)
                print("Deleting dir : %s" % (path))
                shutil.rmtree(path, ignore_errors = True)
    except:
        # do nothing here ...
        print(".")

    Exit(0)

for key, value in ARGLIST:
    if key == 'define':
        cppDefines.append(value)


# cppDefines.append("_CRTDBG_MAP_ALLOC")

env = None

if windows:
    ccFlags.append("/EHsc")
    ccFlags.append("/bigobj")
    ccFlags.append("/Zp8") # Align structs to 8byte boundaries
    cppDefines.append("NOMINMAX")

    ccFlags.append("/arch:AVX2") # Enable AVX2 extensions
    # ccFlags.append("/fp:fast") # Enable FMA extensions

    env = Environment(
        PATH = os.environ['PATH'], 
        INCLUDE = [], 
        LIBS = [],
        CPPDEFINES = cppDefines,
        CCFLAGS = ccFlags,
        LIBPATH = libPaths + [
            outputDir,
            os.path.join(os.getcwd(), "Thirdparty", "ta-lib", "lib", platform),
        ]
        # , 
        #     os.path.join(os.getcwd(), "Thirdparty", "Poco", "lib64", "Windows"),
        #     os.path.join(os.getcwd(), "Thirdparty", "Python", "libs", "Windows"),
        #     os.path.join(os.getcwd(), "Thirdparty", "ta-lib", "lib", "Windows"),
        #     os.path.join(os.getcwd(), "Thirdparty", "CLP", "lib", "Windows"),
        #     os.path.join(os.getcwd(), "Thirdparty", "curl", "lib", "Windows", "lib"),
        #     # os.path.join(os.getcwd(), "Thirdparty", "OpenSSL", "Windows", "lib64"),
        #     os.path.join(os.getcwd(), "Thirdparty", "Mongo", "Windows", "mongo-c-driver", "lib", buildConfig),
        #     os.path.join(os.getcwd(), "Thirdparty", "Mongo", "Windows", "mongo-cxx-driver", "lib", buildConfig),
        # ]
    )
    
    env.AppendUnique(LINKFLAGS = '/SUBSYSTEM:CONSOLE')
    env.AppendUnique(LINKFLAGS = '/MACHINE:X64')
    
    if debug is True:
        env['CCPDBFLAGS'] = '/Zi /Fd${TARGET}.pdb'
        env.AppendUnique(LINKFLAGS = '/INCREMENTAL:No')
        env.AppendUnique(CXXFLAGS = ['/DEBUG'])
        env.AppendUnique(LINKFLAGS = ['/DEBUG'])
        env.AppendUnique(CCFLAGS = ['/Od'])
        
        env['CCPDBFLAGS'] = '/Zi /Fd${TARGET}.pdb'
        env['PDB']='${TARGET.base}.pdb'
        env.AppendUnique(LINKFLAGS=['/INCREMENTAL'])

    env.Append(LIBS = [])

else:
    if "CXX" in os.environ:
        compiler = os.environ["CXX"]

    envLibPaths = [
        outputDir,
        os.path.join(os.getcwd(), "Thirdparty", "ta-lib", "lib", platform),
    ]

    print("Lib Paths: %s" % (str(envLibPaths)))

    # for all other platforms we just use clang++
    env = Environment(
        CXX = compiler,
        CPPDEFINES = cppDefines,
        CCFLAGS = ccFlags,
        LIBS = [], 
        LIBPATH = envLibPaths
    )

# env['ENV']['TERM'] = os.environ['TERM']
env["windows"]      = windows
env["linux"]        = linux
env["mac"]          = mac
env["outputDir"]    = outputDir
env["rootDir"]      = os.getcwd()
env["swigBuild"]    = swigBuild
env["currDir"]      = os.getcwd()
env["copyFiles"]    = copyFiles
env["sourceDir"]    = sourceDir
env["resolveLibs"]  = resolveLibs
env["includeDirs"]  = includeDirs
env["sourceDirs"]   = sourceDirs
env["platform"]     = platform
env["debug"]        = debug
env["copyCheck"]    = ["Poco"]
env["soPrefix"]     = ""
env["resolveInclude"] = resolveInclude
env["version"]      = version
env["prevVersion"]  = prevVersion
env["installDir"]   = outputDir
env.Alias("install", outputDir)

deploymentDir = ""
if platform is "Windows":
    deploymentDir = "Y:\\"
else:
    deploymentDir = os.path.join(os.path.expanduser('~'), "drv")

print("Output Dir: %s" % (env["outputDir"]))
print("Current Dir: %s" % (env["currDir"]))
print("Deployment Dir: %s" % (deploymentDir))
print("Platform: %s" % (platform))
print("Version: %s [Previous: %s]" % (version, prevVersion))

if not os.path.exists(outputDir):
    os.makedirs(outputDir)

# write the version
f = open(os.path.join(outputDir, "VERSION"), "w")
f.write(version)
f.write("\n")
f.write(prevVersion)
f.close()


if mac:
    env["soName"] = ["dylib"]
    env["soPrefix"] = "lib"
elif windows:
    env["soName"] = ["dll", "pdb"]
else:
    env["soName"] = ["so", "a"]
    env["soPrefix"] = "lib"

print("Include Paths: %s" % (", ".join(includePaths)))
for includePath in includePaths:
    env.Append(CCFLAGS = "-I" + includePath)

# Copy the files from the bin paths
if windows:
    copyFiles(conan['conan']['BINPATH'])
else:
#    for key in conan:
#        if key is not 'conan':
#            for libPath in conan[key]['LIBPATH']:
#                print("LIBPATN - %s: %s" % (libPath, conan[key]['LIBS']))
#                copyFiles(libPath, searchTerms = conan[key]['LIBS'], prefix = 'lib')
    copyFiles(conan['conan']['LIBPATH'])

copyFiles([os.path.join("Thirdparty/ta-lib/lib/", platform)])

# if debug is True:
#     if platform is "Windows":
#         copyFiles([os.path.join("Thirdparty/Poco/bin64/", platform)], searchWord = "*64d.")
#         # copyFiles(["Thirdparty/OpenSSL/Windows/bin64/"], searchWord = "*MDd.")
#     else:
#         copyFiles([os.path.join("Thirdparty/Poco/bin64/", platform)], searchWord = "*.")
# else:
#     if platform is "Windows":
#         copyFiles([os.path.join("Thirdparty/Poco/bin64/", platform)], searchWord = "*64.")
#         # copyFiles(["Thirdparty/OpenSSL/Windows/bin64/"], searchWord = "*MD.")
#     else:
#         copyFiles([os.path.join("Thirdparty/Poco/bin64/", platform)], searchWord = "*.")

# copyFiles([os.path.join("Thirdparty/CLP/lib/", platform)])

# copyFiles([os.path.join("Thirdparty/Python/bin/", platform)])
# copyFiles([os.path.join("Thirdparty/curl/lib/", platform, "bin")])

# copyFiles([os.path.join("Thirdparty/Mongo/", platform, "mongo-c-driver/bin/", buildConfig)])
# copyFiles([os.path.join("Thirdparty/Mongo/", platform, "mongo-cxx-driver/bin/", buildConfig)])

env.Append(CPPDEFINES = "SO_EXT=" + env["soName"][0])

# thisFile = (lambda x:x).func_code.co_filename
# includeDirs([os.path.dirname(thisFile)])

print("")
env.SConscript("Core/SConscript",           variant_dir = os.path.join(outputDir, "obj", "Core"),           duplicate = 0, exports = { "env": env })
env.SConscript("Framework/SConscript",      variant_dir = os.path.join(outputDir, "obj", "Framework"),      duplicate = 0, exports = { "env": env })
env.SConscript("VirtualMachine/SConscript", variant_dir = os.path.join(outputDir, "obj", "VirtualMachine"), duplicate = 0, exports = { "env": env })
env.SConscript("Helper/SConscript",         variant_dir = os.path.join(outputDir, "obj", "Helper"),         duplicate = 0, exports = { "env": env })
env.SConscript("PesaImpl/SConscript",       variant_dir = os.path.join(outputDir, "obj", "PesaImpl"),       duplicate = 0, exports = { "env": env })
env.SConscript("Pir/SConscript",            variant_dir = os.path.join(outputDir, "obj", "Pir"),            duplicate = 0, exports = { "env": env })
env.SConscript("AlphaLib/SConscript",       variant_dir = os.path.join(outputDir, "obj", "AlphaLib"),       duplicate = 0, exports = { "env": env })
env.SConscript("PyImpl/SConscript",         variant_dir = os.path.join(outputDir, "obj", "PyImpl"),         duplicate = 0, exports = { "env": env })
env.SConscript("PSim/SConscript",           variant_dir = os.path.join(outputDir, "obj", "PSim"),           duplicate = 0, exports = { "env": env })
# env.SConscript("Tests/SConscript",          variant_dir = os.path.join(outputDir, "obj", "Tests"),          duplicate = 0, exports = { "env": env })
# env.SConscript("Tests/SConscript_EU",       variant_dir = os.path.join(outputDir, "obj", "Tests_EU"),       duplicate = 0, exports = { "env": env })
# env.SConscript("Tests/SConscript_US",       variant_dir = os.path.join(outputDir, "obj", "Tests_US"),       duplicate = 0, exports = { "env": env })

copyDirs = [
    "Tools",
    "Tools/bash",
    "Tools/BamTrade",
    "Tools/Tests",
    "Tools/Data",
    "Tools/Data/MorningStar",
    "Tools/Data/Reuters",
    "Tools/Data/Download",
    "Tools/Data/BB",
    "Tools/Mongo",
    "PDaemon",
    "PStrat"
]

# copy the configs directory to the target dir ...

for cdir in copyDirs:
    copyFiles([cdir], searchTerms = "*.*", targetDir = os.path.join(outputDir, cdir), alwaysCopy = True)

# copyFiles(["Tools"], searchTerms = "*.*", targetDir = os.path.join(outputDir, "Tools"), alwaysCopy = True)
# copyFiles(["Tools/BamTrade"], searchTerms = "*.*", targetDir = os.path.join(outputDir, "Tools/BamTrade"), alwaysCopy = True)
# copyFiles(["Tools/Tests"], searchTerms = "*.*", targetDir = os.path.join(outputDir, "Tools/Tests"), alwaysCopy = True)
# copyFiles(["Tools/Data"], searchTerms = "*.*", targetDir = os.path.join(outputDir, "Tools/Data"), alwaysCopy = True)
# copyFiles(["Tools/Data/MorningStar"], searchTerms = "*.*", targetDir = os.path.join(outputDir, "Tools/Data/MorningStar"), alwaysCopy = True)
# copyFiles(["Tools/Data/Reuters"], searchTerms = "*.*", targetDir = os.path.join(outputDir, "Tools/Data/Reuters"), alwaysCopy = True)
# copyFiles(["Tools/Data/Download"], searchTerms = "*.*", targetDir = os.path.join(outputDir, "Tools/Data/Download"), alwaysCopy = True)
# copyFiles(["Tools/Data/BB"], searchTerms = "*.*", targetDir = os.path.join(outputDir, "Tools/Data/BB"), alwaysCopy = True)
# copyFiles(["Tools/Mongo"], searchTerms = "*.*", targetDir = os.path.join(outputDir, "Tools/Mongo"), alwaysCopy = True)
# copyFiles(["PDaemon"], searchTerms = "*.*", targetDir = os.path.join(outputDir, "PDaemon"), alwaysCopy = True)
# copyFiles(["PStrat"], searchTerms = "*.*", targetDir = os.path.join(outputDir, "PStrat"), alwaysCopy = True)

print("Copying configs ...")

deploymentDir = os.path.join(outputDir, "Configs")

configsDirs = {
    "Shared": ["./"],
    "Common": ["./"],
    "MorningStar": ["./"],
    "Misc": ["./"],
    "US": ["./"],
    "EU": ["./"],
    "JP": ["./"],
    "Tests": ["./"],
    "USFut": ["./"]

    # "Misc": [
    #     "./"
    # ],

    # "Equities": [
    #     "Common", "Misc", "US", "EU", "JP", "US/Snapshot", "US/StratMaker", "EU/Snapshot", "EU/StratMaker", "JP/Snapshot", "JP/StratMaker",
    #     "Gen", "Gen/US", "Gen/EU", "Gen/JP", "Test"
    # ],

    # "Futures": [
    #     "Common", "US"
    # ]
}
    
# copyFiles(["Configs"], searchTerms = "*.xml", targetDir = deploymentDir, alwaysCopy = True)
for baseDir in configsDirs:
    xmlDirs = configsDirs[baseDir]

    for xmlDir in xmlDirs:
        copyFiles([str.format("Configs/%s/%s" % (baseDir, xmlDir))], searchTerms = "*.xml", targetDir = os.path.join(deploymentDir, baseDir, xmlDir), alwaysCopy = True)

print("Configs copied!")

# now we need to copy to the dist directory
if dist:
    if platform is "Windows":
        deploymentDir = "Y:\\"
    else:
        deploymentDir = os.path.join(os.path.expanduser('~'), "drv")

    dstDir = os.path.join(deploymentDir, "psim", "psim" + version, platform.lower())
    print("Removing existing build: %s ..." % (dstDir))

    if os.path.exists(dstDir):
        try:
            shutil.rmtree(dstDir)
        except Exception as e:
            print(e)

    print("Copying the new build %s ..." % (dstDir))
    copyFiles([outputDir], searchTerms = ["*.dll", "*.so", "*.so.*", "*.exe", "psim*", "psim.exe", "VERSION"], targetDir = dstDir, alwaysCopy = True)
    copyFiles([os.path.join(outputDir, "Python")], searchTerms = ["*.*"], targetDir = os.path.join(dstDir, "Python"), alwaysCopy = True)
    copyFiles([os.path.join(outputDir, "Python/Smart")], searchTerms = ["*.*"], targetDir = os.path.join(dstDir, "Python", "Smart"), alwaysCopy = True)
    copyFiles([os.path.join(outputDir, "Python/Basic")], searchTerms = ["*.*"], targetDir = os.path.join(dstDir, "Python", "Basic"), alwaysCopy = True)
    copyFiles([os.path.join(outputDir, "Python/MorningStar")], searchTerms = ["*.*"], targetDir = os.path.join(dstDir, "Python", "MorningStar"), alwaysCopy = True)
    copyFiles([os.path.join(outputDir, "Python/Reuters")], searchTerms = ["*.*"], targetDir = os.path.join(dstDir, "Python", "Reuters"), alwaysCopy = True)
    copyFiles([os.path.join(outputDir, "Python/DerivedData")], searchTerms = ["*.*"], targetDir = os.path.join(dstDir, "Python", "DerivedData"), alwaysCopy = True)

    for cdir in copyDirs:
        copyFiles([os.path.join(outputDir, cdir)], searchTerms = ["*.*"], targetDir = os.path.join(dstDir, cdir), alwaysCopy = True)

    # copyFiles([os.path.join(outputDir, "Tools")], searchTerms = ["*.*"], targetDir = os.path.join(dstDir, "Tools"), alwaysCopy = True)
    # copyFiles([os.path.join(outputDir, "Tools/BamTrade")], searchTerms = ["*.*"], targetDir = os.path.join(dstDir, "Tools/BamTrade"), alwaysCopy = True)
    # copyFiles([os.path.join(outputDir, "Tools/Tests")], searchTerms = ["*.*"], targetDir = os.path.join(dstDir, "Tools/Tests"), alwaysCopy = True)
    # copyFiles([os.path.join(outputDir, "Tools/Data")], searchTerms = ["*.*"], targetDir = os.path.join(dstDir, "Tools/Data"), alwaysCopy = True)
    # copyFiles([os.path.join(outputDir, "Tools/Data/MorningStar")], searchTerms = ["*.*"], targetDir = os.path.join(dstDir, "Tools/Data/MorningStar"), alwaysCopy = True)
    # copyFiles([os.path.join(outputDir, "Tools/Data/Reuters")], searchTerms = ["*.*"], targetDir = os.path.join(dstDir, "Tools/Data/Reuters"), alwaysCopy = True)
    # copyFiles([os.path.join(outputDir, "Tools/Data/Download")], searchTerms = ["*.*"], targetDir = os.path.join(dstDir, "Tools/Data/Download"), alwaysCopy = True)
    # copyFiles([os.path.join(outputDir, "Tools/Data/BB")], searchTerms = ["*.*"], targetDir = os.path.join(dstDir, "Tools/Data/BB"), alwaysCopy = True)
    # copyFiles([os.path.join(outputDir, "Tools/Mongo")], searchTerms = ["*.*"], targetDir = os.path.join(dstDir, "Tools/Mongo"), alwaysCopy = True)
    # copyFiles([os.path.join(outputDir, "PDaemon")], searchTerms = ["*.*"], targetDir = os.path.join(dstDir, "PDaemon"), alwaysCopy = True)
    # copyFiles([os.path.join(outputDir, "PStrat")], searchTerms = ["*.*"], targetDir = os.path.join(dstDir, "PStrat"), alwaysCopy = True)

    for baseDir in configsDirs:
        xmlDirs = configsDirs[baseDir]

        for xmlDir in xmlDirs:
            copyFiles([os.path.join(outputDir, "Configs", baseDir, xmlDir)], searchTerms = ["*.*"], targetDir = os.path.join(dstDir, "Configs", baseDir, xmlDir), alwaysCopy = True)

    # for xmlDir in xmlDirs:
    #     copyFiles([os.path.join(outputDir, "Configs", "Equities", xmlDir)], searchTerms = ["*.*"], targetDir = os.path.join(dstDir, "Configs", "Equities", xmlDir), alwaysCopy = True)
    #     copyFiles([os.path.join(outputDir, "Configs", "Futures", xmlDir)], searchTerms = ["*.*"], targetDir = os.path.join(dstDir, "Configs", "Futures", xmlDir), alwaysCopy = True)

    print("ALL DONE!")

# import shutil
# shutil.copyfile(os.path.join(outputDir, "Framework." + env["soName"][0]), os.path.join(outputDir, "_Framework.pyd"))

# shutil.copyfile(os.path.join(os.path.dirname(thisFile), "Framework", "SWIG", "Framework.py"), os.path.join(outputDir, "Framework.py"))

# app = env.Program(
#   target = os.path.join(outputDir, "psim"), 
#   source = sources)

# Default(app)
