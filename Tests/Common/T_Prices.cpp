/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// TestFramework.cpp
///
/// Created on: 22 Nov 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Tests/catch.hpp"
#include "Tests/TestFramework.h"
#include "PesaImpl/Pipeline/DataRegistry/DataRegistry.h"
#include "PesaImpl/Components/SP/SpRest.h"

using namespace pesa;
//////////////////////////////////////////////////////////////////////////
/// TESTS
//////////////////////////////////////////////////////////////////////////  
TEST_CASE("Prices", "[prices]") {
    SECTION("Check") {
        auto config = DR.config().getChildSection("prices");
        auto cp = DR.createDataCheckpoint();

        /// Get the data
        auto* secMaster = DR.getSecurityMaster();
        FloatMatrixData& adjClosePrice = DR.getDataPtr(nullptr, "baseData.adjClosePrice", nullptr)->asFloat();

        DR.deleteDataCheckpoint(cp);
    }

//	/// Try to match these with the data from the Data API!
//	//IDataLoader* dl = DR.getDataLoader("baseData.adjClosePrice");
//	SpRest restData(SERVER_URL, "BloombergAdjustedPrices", YML("Prices.yml"));
//	IntDay di = (RT.diEnd - RT.diStart) / 2;
//	IntDate date = RT.dates[di];
//
//	StringVec srcFigis = { "BBG009S39JX6", "BBG000B9XRY4" };
//	auto& result = restData.getData({ "float.PX_LAST" }, date, date, srcFigis);
//	StringData& uuids = result.data["SYMBOL"]->asString();
//	FloatMatrixData& prices = result.data["PX_LAST"]->asFloat();
//	auto numCols = prices.cols();
//
//	REQUIRE(numCols == uuids.cols());
//
//	for (size_t figiId = 0; figiId < numCols; figiId++) {
//		std::string figi = uuids.getValue(0, figiId);
//		const Security* sec = secMaster->mapUuid(figi);
//		Security::Index ii = sec->index;
//		float cacheValue = adjClosePrice(di, ii);
//		float apiValue = prices(0, figiId);
//		float diff = std::abs(cacheValue - apiValue);
//		REQUIRE(diff < 0.001f);
//	}
}
