/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// TestFramework.cpp
///
/// Created on: 22 Nov 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"

#include "TestFramework.h"
#include "PesaImpl/Pipeline/DataRegistry/DataRegistry.h"
#include "PesaImpl/Components/SP/SpRest.h"

namespace pesa {
	TestFramework* TestFramework::s_instance = nullptr;

	TestFramework& TestFramework::instance() {
		ASSERT(s_instance, "Invalid TestFramework instance!");
		return *s_instance;
	}

	const RuntimeData& TestFramework::rt() {
		return *(s_instance->m_rt);
	}

	DataRegistry& TestFramework::dr() {
		return *(s_instance->m_dr);
	}

	std::string TestFramework::yml(const std::string& name) {
		std::string ymlDir = s_instance->parentConfig()->defs().getRequired("ymlDir");
		return ymlDir + "\\" + name;
	}

	//////////////////////////////////////////////////////////////////////////
	TestFramework::TestFramework(const ConfigSection& config) : IPipelineComponent(config, "TestFramework") {
		PTrace("Created TestFramework!");

		TestFramework::s_instance = this;
	}

	TestFramework::~TestFramework() {
		PTrace("Deleting TestFramework!");
	}

	void TestFramework::prepare(RuntimeData& rt) {
		m_dr = dynamic_cast<DataRegistry*>(rt.dataRegistry);
		m_rt = &rt;

		/// Now we run the tests!
		auto appPath = Application::instance()->cmdPath().toString();
		const int argc = 1;
		char* argv[1] = { const_cast<char*>(appPath.c_str()) };

		int errCode = Catch::Session().run(argc, argv);

		/// Just exit
		Util::exit(errCode);
	}

	void TestFramework::tick(const RuntimeData& rt) {
	}

	void TestFramework::finalise(RuntimeData& rt) {
	}
} /// namespace pesa

////////////////////////////////////////////////////////////////////////////
/// To be called from the simulator
////////////////////////////////////////////////////////////////////////////
EXPORT_FUNCTION pesa::IPipelineComponent* createTestFramework(const pesa::ConfigSection& config) {
	return new pesa::TestFramework((const pesa::ConfigSection&)config);
}

