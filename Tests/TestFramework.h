/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// TestFramework.h
///
/// Created on: 22 Nov 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Framework/FrameworkDefs.h"
#include "Framework/Pipeline/IPipelineComponent.h"

namespace pesa {
	class DataRegistry; 

	////////////////////////////////////////////////////////////////////////////
	/// TestFramework - An implementation of the testing framework
	////////////////////////////////////////////////////////////////////////////
	class TestFramework : public IPipelineComponent {
	private:
		static TestFramework*		s_instance;
		DataRegistry*				m_dr; 
		RuntimeData*				m_rt;

	public:
									TestFramework(const ConfigSection& config);
		virtual 					~TestFramework();

		static TestFramework&		instance();
		static DataRegistry&		dr();
		static const RuntimeData&	rt();
		static std::string			yml(const std::string& name);

		////////////////////////////////////////////////////////////////////////////
		/// IPipelineComponent overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 				prepare(RuntimeData& rt);
		virtual void 				tick(const RuntimeData& rt);
		virtual void 				finalise(RuntimeData& rt);
	};

	inline TestFramework&			tf() { return TestFramework::instance(); }
	inline DataRegistry&			dr() { return TestFramework::dr(); }
} /// namespace pesa

#define TF							pesa::TestFramework::instance()
#define DR							pesa::TestFramework::dr()
#define RT							pesa::TestFramework::rt()
#define YML(n)						pesa::TestFramework::yml(n)
#define SERVER_URL					pesa::TestFramework::instance().parentConfig()->defs().getRequired("serverUrl")
