### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import csv
import requests
import json
import datetime

def post_order(query):


    
    uat_token = ''
    url1 = 'http://ems-gtwy-uat:8091/api/Order/PostOrders'
    uat_token = "Bearer " + uat_token
    

    headers = {'content-type' : 'application/json;charset=utf8', 'Authorization' : uat_token, 'Accept': 'application/json'}
    res =requests.post(url1,data = json.dumps(query),headers=headers,verify=False)
    print(res.status_code, res.text, res.headers)
    res.raise_for_status()     

class Order():

     def __init__(self,bamsy,pmcode,strategy,substrategy,batchID,aggr,cust,key,side,size,price,execInstructions):
        self._order=  {'Custodian': cust, 'BatchId': batchID, 
                       'Security':  {'Key': key, 'BamSymbol': bamsy, 'SecurityType': 'Equity'},
                       'Portfolio': {'PMCode': pmcode, 'SubStrategy': substrategy, 'AggregationUnit': aggr, 'Strategy': strategy} ,
                       'Size': size, 'Side': side, 'Price':price,'ExecutionInstructions': execInstructions}


					  
					  
class ExecInstruction():

     def __init__(self,code,value):
        self._instr=  {'Code': code, 'Value': value}

					   
reader = csv.DictReader(open('./orders.csv'))

query = list(reader)

orderList =[]

now = datetime.datetime.now()        
time = now.strftime("%Y%m%d-%H%M%S")
id = "GENERALIST"
batchID= "-".join(("HKHA",time,id))

for i in query:
    bamsy= i.get('Security')
    pmcode= i.get('PMCode')
    strategy= i.get('Strategy')
    substrategy= i.get('SubStrategy')
    
    aggr= 'AQTF'
    cust= i.get('Cust')
    key= bamsy
    side = i.get('Side')
    size = i.get('Amount')
    price = 0.0

    execInstructions =[]
    instr1 = ExecInstruction('Algo',i.get('Algo')) # 	
    execInstructions.append(instr1._instr)
    instr2 = ExecInstruction('StartTime',i.get('StartTime')) # 	
    execInstructions.append(instr2._instr)
    instr3 = ExecInstruction('EndTime',i.get('EndTime')) # 	
    execInstructions.append(instr3._instr)
    instr4 = ExecInstruction('Urgency',i.get('Urgency')) # 	
    execInstructions.append(instr4._instr)
    instr5 = ExecInstruction('MaxPartRate',i.get('MaxPartRate')) # 	
    execInstructions.append(instr5._instr)
    instr8 = ExecInstruction('Duration',i.get('Duration')) # 	
    execInstructions.append(instr8._instr)
    instr9 = ExecInstruction('AutoRoute',i.get('AutoRoute')) # 	TRUE to enable auto-routing
    execInstructions.append(instr9._instr)
    
	

    order = Order(bamsy,pmcode,strategy,substrategy,batchID,aggr,cust,key,side,size,price,execInstructions)
    orderList.append(order._order)

print(orderList)
post_order(orderList)
