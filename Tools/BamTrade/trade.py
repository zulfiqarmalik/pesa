### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import csv
import requests
from requests_ntlm import HttpNtlmAuth
import datetime as DT
import json
from datetime import datetime, timedelta
from trade_settings import Settings
import os
import traceback
import trade_util
import sys

def dateToStr(date = None, sep = ""):
    if date is None:
        date = DT.datetime.utcnow()
    return str(date.year).zfill(4) + sep + str(date.month).zfill(2) + sep + str(date.day).zfill(2)

def dateToStrMMDDYYYY(date = None, sep = ""):
    if date is None:
        date = DT.datetime.utcnow()
    return str(date.month).zfill(2) + sep + str(date.day).zfill(2) + sep + str(date.year).zfill(4) 

def datetimeToStr(date = None, sep = ""):
    if date is None:
        date = DT.datetime.utcnow()
    return str(date.year).zfill(4) + sep + str(date.month).zfill(2) + sep + str(date.day).zfill(2) + "_" + str(date.hour).zfill(2) + str(date.minute).zfill(2) + str(date.second).zfill(2)

def checkValuesWithinTolerance(valueName, prevValue, currValue, tolerance):
    if prevValue == 0.0:
        return

    diff = abs(currValue - prevValue) / prevValue
    assert diff <= tolerance, "%s violation: %0.2f [Curr: %0.2f, Prev: %0.2f, Tolerance: %0.2f]" % (valueName, diff, currValue, prevValue, tolerance)

def checkWithinTolerance(cp, pp, valueName, tolerance):
    return checkValuesWithinTolerance(valueName, pp[valueName], cp[valueName], tolerance)

class Trade:
    def __init__(self, settings = None):
        if settings is None:
            self.settings       = Settings(self)
            self.settings.parseArguments()
            self.settings.initLogging()
        else:
            self.settings       = settings

        opt                     = self.settings.options
        self.log                = self.settings.log

        self.ymlVer              = "1.1.26"

        self.posFilename        = opt.posFilename
        self.tradeServer        = opt.server
        self.custodian          = opt.custodian
        self.defaultAlgo        = "VWAP"
        self.algoOverrides      = {}
        self.algoFilename       = opt.algo
        self.manager            = opt.manager
        self.pmCode             = opt.pmCode
        self.strategyId         = opt.strategyId
        self.subStrategyId      = opt.subStrategy
        self.batchId            = self.pmCode + "-" + opt.batchId
        self.orderIds           = opt.orderId
        self.apiToken           = opt.apiToken
        self.username           = opt.username
        self.password           = opt.password
        self.swap               = opt.swap
        self.force              = opt.force
        self.dollarTurnOver     = 0.0
        self.autoRoute          = bool(opt.autoRoute)
        # self.dataEnv            = opt.dataEnv
        self.serverUrl          = opt.serverUrl
        self.positions          = None
        self.positionsIndexMap  = {}
        self.execute            = opt.execute
        self.cancel             = opt.cancel
        self.tickers            = opt.tickers
        self.intermediate       = opt.intermediate
        self.fullStrategyId     = self.pmCode + "-" + self.strategyId + "-" + self.subStrategyId
        self.ordersFilename     = opt.ordersFilename
        self.refDataMap         = {}
        self.invRefDataMap      = {}
        self.doMapPositions     = True
        self.cancelAll          = True if self.cancel is True and bool(opt.all) is True else False
        self.useLivePositions   = bool(opt.useLivePositions)
        self.recipients         = opt.email.split(";")
        self.restrictedLongFilename = os.path.expanduser(opt.restrictedLong)

        self.minNotional        = float(opt.minNotional)

        self.restrictedMLPDir   = opt.restrictedMLPDir
        self.restrictedMLPBroker= "CI"
        self.restrictedMLPFilename = None
        self.symbolRemap        = {}
        self.mlpSwapName        = ""

        if self.algoFilename:
            self.readAlgoOverrides(self.algoFilename)

        if "@" not in self.restrictedMLPDir:
            self.restrictedMLPDir = None

        if self.restrictedMLPDir:
            self.restrictedMLPDir, self.restrictedMLPBroker = self.restrictedMLPDir.split("@")
            self.restrictedMLPDir   = self.restrictedMLPDir.strip()
            self.restrictedMLPBroker= self.restrictedMLPBroker.strip()
            self.restrictedMLPFilename = os.path.join(os.path.expanduser(self.restrictedMLPDir), "RestrictedSymbolsMLP" + dateToStrMMDDYYYY() + ".csv")

            self.symbolRemap        = {}
            self.mlpSwapName        = "MLP " + self.restrictedMLPBroker + "-OTC"

        self.batchId            = self.batchId + "_" + datetimeToStr()  
        self.doGetTrades        = opt.getTrades
        self.dumpTradesPath     = opt.dumpTradesPath

        if "uat" in self.tradeServer:
            self.dataEnv        = "UAT"
        else:
            self.dataEnv        = "PROD"

        if self.orderIds:
            self.orderIds       = [x.strip() for x in self.orderIds.split(",")]
        else:
            self.orderIds       = []

        if self.tickers:
            self.tickers        = [x.strip() for x in self.tickers.split(",")]
        else:
            self.tickers        = []

        assert self.apiToken, "Invalid API Token!"
        self.readAPIToken()

        assert self.serverUrl, "Invalid server url for the BAM server!"
        assert self.dataEnv, "Invalid data environment!"

        assert self.tradeServer, "Invalid trade server specified!"
        assert self.strategyId, "Invalid strategyId!"
        assert self.subStrategyId, "Invalid subStrategy!"
        # assert opt.batchId, "Invalid batchId!"
        assert self.custodian, "Invalid custodian!"
        assert self.manager, "Invalid manager!"
        assert self.pmCode, "Invalid pmCode!"

        # assert self.batchId, "Invalid batchId!"
        # assert self.apiToken, "Invalid API Token!"

    def readAlgoOverrides(self, algoFilename):
        import csv
        self.log.info("Loading algo from file: %s" % (algoFilename))

        with open(algoFilename, 'r') as csvfile:
            reader = csv.reader(csvfile, delimiter = ',')

            for row in reader:
                assert len(row) == 2, "Invalid row in algo: %s" % (str(row))
                bamTicker = row[0]
                algo = row[1]

                if bamTicker == "default":
                    self.defaultAlgo = algo
                else:
                    self.algoOverrides[bamTicker] = algo

    def readAPIToken(self):
        # read the api token
        file = open(self.apiToken, "r")
        self.apiToken = file.readline()
        return self.apiToken

    def loadPosString(self, posString):
        positionsToLoad = posString.split(",")

        positions = []

        # We ignore the leading @
        for i in range(1, len(positionsToLoad)):
            positionToLoad = positionsToLoad[i].strip()
            ticker, position = positionToLoad.split("=")

            figi = None

            if ticker in self.invRefDataMap:
                figi = self.invRefDataMap[ticker][0]
                self.log.info("Found FIGI Mapping for ticker:  %s => %s" % (ticker, figi))
            else:
                self.log.info("NO FIGI found for ticker: %s. !!!IGNORING POSITION!!!" % (ticker))
                continue

            positions.append({
                "bbTicker": ticker,
                "figi": figi,
                "cfigi": figi,
                "tradeTicker": ticker,
                "currentPosition": 0,
                "targetPosition": float(position),
                "deltaPosition": 0,
                "notional": 0,
                "isRestricted": False,
                "isRestrictedShort": False,
                "isRestrictedLong": False,
                "price": 0.0
            }) 

            self.positionsIndexMap[ticker] = len(positions) - 1

        return positions

    def loadPositions(self):
        if self.posFilename:
            if self.posFilename[0] == '@':
                return self.loadPosString(self.posFilename)

            return self.loadPositionsFromPosFile(self.posFilename)
        elif self.intermediate:
            return self.loadPositionsFromIntermediateFile(self.intermediate)

        assert False, "Invalid input for loading positions. No pos file or intermediate file given for position loading!"

    def loadPositionsFromIntermediateFile(self, filename):
        import csv
        self.log.info("Loading positions from intermediate file: %s" % (filename))
        header = None
        positions = []

        with open(filename, 'r') as csvfile:
            reader = csv.reader(csvfile, delimiter = ',')

            for row in reader:
                if header is None:
                    header = row
                else:
                    figi = row[0].strip()

                    if not figi or figi[0] == '#':
                        continue

                    ticker = row[1].strip()
                    cfigi = row[13].strip() if len(row) > 13 else figi

                    positions.append({
                        "bbTicker": row[2].strip(),
                        "figi": figi,
                        "tradeTicker": ticker,
                        "currentPosition": float(row[5].strip()),
                        "targetPosition": float(row[4].strip()),
                        "deltaPosition": float(row[3].strip()),
                        "notional": float(row[9].strip()),
                        "isRestricted": True if row[6].strip() is "YES" else False,
                        "isRestrictedShort": True if row[7].strip() is "YES" else False,
                        "isRestrictedLong": True if len(row) > 8 and row[8].strip() is "YES" else False,
                        "price": float(row[12].strip()) if len(row) > 12 else 0.0,
                        "cfigi": cfigi,
                    }) 

                    self.positionsIndexMap[figi] = len(positions) - 1

                    if cfigi and cfigi != figi:
                        self.positionsIndexMap[cfigi] = len(positions) - 1

                    self.addToRefData(figi, ticker)
                    self.addToRefData(cfigi, ticker)

        self.doMapPositions = False 

        # unset this because we don't want to write to this file if we're loading from it ...
        self.intermediate = None

        return positions

    def loadDeltaPositions(self, newPosFilename, oldPosFilename):
        newPositions = self.loadPositionsFromPosFile(newPosFilename)
        oldPositions = self.loadPositionsFromPosFile(oldPosFilename)

        oldPosMap = {}

        for oldPos in oldPositions:
            oldPosMap[oldPos["figi"]] = oldPos

        self.positionsIndexMap = {}

        for i in range(len(newPositions)):
            newPos = newPositions[i]
            self.positionsIndexMap[newPos["figi"]] = i

            if newPos["figi"] in oldPosMap:
                oldPos = oldPosMap[newPos["figi"]]
                newPos["currentPosition"] = oldPos["targetPosition"]
                #newPos["targetPosition"] -= oldPos["targetPosition"]

        self.useLivePositions = False

        return newPositions

    def parseComment(self, comment, cp, pp):
        infoStr = comment[1:] # remove the leading # that indicates comment
        parts = infoStr.split(":") 

        # If it has more than 2 parts then we don't even care ... 
        if len(parts) != 2:
            return

        lhs = parts[0].strip()
        rhs = parts[1].strip()

        import parse
        if lhs == "BookSize":
            cp["bookSize"] = float(rhs)
            pp["bookSize"] = cp["bookSize"]

        elif lhs == "$ Long / $ Short":
            cp["dollarLong"], cp["dollarShort"], cp["dollarDelta"], cp["maxDelta"] = parse.parse("{:g} / {:g} [Delta = {:g}, MaxDelta = {:g}]", rhs)
        elif lhs == "Lng / Shrt / Liqd":
            cp["numLong"], cp["numShort"], cp["numLiquidate"], cp["tvr"] = parse.parse("{:d} / {:d} / {:d} [TVR = {:g}]", rhs)

        elif lhs == "Y - $ Long / $ Short":
            pp["dollarLong"], pp["dollarShort"], pp["dollarDelta"], pp["maxDelta"] = parse.parse("{:g} / {:g} [Delta = {:g}, MaxDelta = {:g}]", rhs)
        elif lhs == "Y - Lng / Shrt / Liqd":
            pp["numLong"], pp["numShort"], pp["numLiquidate"], pp["tvr"] = parse.parse("{:d} / {:d} / {:d} [TVR = {:g}]", rhs)
        elif lhs == "Tolerances":
            cp["maxStockTolerance"], cp["maxStocks"], cp["maxTvrToleranceUpper"], cp["maxTvrToleranceLower"], cp["dailyTvrTolerance"], cp["maxTvr"] = parse.parse("maxStockTolerance = {:g}, maxStocks = {:g}, maxTvrToleranceUpper = {:g}, maxTvrToleranceLower = {:g}, dailyTvrTolerance = {:g}, maxTvr = {:g}", rhs)
        elif lhs == "Universe":
            cp["universe"] = rhs
            pp["universe"] = cp["universe"]

    def checkIntegrityOfPositions(self, cp, pp):

        # cp = currPosInfo, pp = prevPosInfo

        # - Number of Longs and Number of shorts TODAY shouldn't differ from TODAY-1 by more than 5%
        # - Total number of positions TODAY shouldn't differ from TODAY-1 by more than 5%
        # - Number of liquidations should not differ TODAY shouldn't differ from TODAY-1 by more than 5%
        # - Turnover TODAY shouldn't differ from Turnover TODAY-1 by more than 2%
        # - Dollar Delta TODAY should not be more than specified in Master file (default 50bps)
        # - Total gross book value calculated from prices on TODAY should not differ from the booksize inside the Master config by more than 50bps.

        # We check each condition one by one
        checkValuesWithinTolerance("NumLong", cp["maxStocks"], cp["numLong"], cp["maxStockTolerance"])
        checkValuesWithinTolerance("NumShort", cp["maxStocks"], cp["numShort"], cp["maxStockTolerance"])
        checkValuesWithinTolerance("Delta-TVR", pp["tvr"], cp["tvr"], cp["dailyTvrTolerance"])

        maxTvr = cp["maxTvr"]
        if maxTvr > 0.0:
            tvrDeltaFromMax = ((cp["tvr"] - cp["maxTvr"]) / cp["maxTvr"]) 

            if tvrDeltaFromMax > 0.0:
                assert tvrDeltaFromMax <= cp["maxTvrToleranceUpper"], "Turnover upper bounds violation: %0.2f [Curr: %0.2f, Max: %0.2f, Tolerance: %0.2f]" % (tvrDeltaFromMax, cp["tvr"], cp["maxTvr"], cp["maxTvrToleranceUpper"])
            elif tvrDeltaFromMax < 0.0:
                assert abs(tvrDeltaFromMax) <= cp["maxTvrToleranceLower"], "Turnover lower bounds violation: %0.2f [Curr: %0.2f, Max: %0.2f, Tolerance: %0.2f]" % (tvrDeltaFromMax, cp["tvr"], cp["maxTvr"], cp["maxTvrToleranceLower"])

        dollarDeltaPercent = (cp["dollarDelta"] / cp["bookSize"]) * 100.0
        assert abs(dollarDeltaPercent) <= cp["maxDelta"], "Dollar delta violation: %0.2f [Max: %0.2f]" % (dollarDeltaPercent, cp["maxDelta"])

    def loadPositionsFromPosFile(self, posFilename):
        multiPos = posFilename.split("@")
        if len(multiPos) > 1:
            assert len(multiPos) == 2, "Invalid pos filename: %s" % (posFilename)
            return self.loadDeltaPositions(multiPos[0], multiPos[1])

        self.log.info("Loading positions: %s" % (posFilename))
        file = open(posFilename, "r")
        lines = file.readlines()
        numLines = len(lines)
        positions = []
        currPosInfo = {
            "grossBookSize": 0.0,
            "maxStockTolerance": 0.012,
            "maxStocks": 595,
            "dailyTvrTolerance": 0.15,
            "maxTvrToleranceUpper": 0.05,
            "maxTvrToleranceLower": 0.13,
            "maxTvr": 15.0,
        }
        prevPosInfo = {}
        
        for i in range(numLines):
            line = lines[i].strip()

            # if the line is not a comment
            if line and line[0] != '#':
                posRow = line.split("|")
                assert len(posRow) >= 4, "Invalid positions line:%d %s" % (i, line)
                bbTicker    = posRow[0].strip()
                figi        = posRow[1].strip()
                shares      = int(posRow[2])
                notional    = float(posRow[3])
                price       = float(posRow[4])
                localFigi   = posRow[5].strip()
                tradeTicker = posRow[6].strip()

                # For historical reasons ... perhaps we can get rid of this later!
                if self.subStrategyId == "US":
                    localFigi = figi

                positions.append({
                    "bbTicker": bbTicker,
                    "figi": localFigi,
                    "cfigi": figi,
                    "tradeTicker": tradeTicker,
                    "currentPosition": 0,
                    "targetPosition": shares,
                    "deltaPosition": 0,
                    "notional": notional,
                    "isRestricted": False,
                    "isRestrictedShort": False,
                    "isRestrictedLong": False,
                    "price": price
                })

                currPosInfo["grossBookSize"] += abs(price * shares)

                # Add both to the positionsIndexMap
                self.positionsIndexMap[localFigi] = len(positions) - 1

                if figi and figi != localFigi:
                    self.positionsIndexMap[figi] = len(positions) - 1

                self.addToRefData(figi, tradeTicker)
                self.addToRefData(localFigi, tradeTicker)

            elif line[0] == '#':
                self.parseComment(line, currPosInfo, prevPosInfo)

        self.checkIntegrityOfPositions(currPosInfo, prevPosInfo)

        if self.force is False:
            grossBookDiff = (abs(currPosInfo["bookSize"] - currPosInfo["grossBookSize"]) / currPosInfo["bookSize"]) * 100.0
            assert grossBookDiff <= 5.0, "Gross book difference voilation: %0.2f [Max: 5.0]" % (grossBookDiff)

        self.dollarTurnOver = currPosInfo["tvr"] * currPosInfo["bookSize"] * 0.01
            
        return positions

    def handleRestrictedLongList(self):
        import csv
        self.log.info("Loading restricted long filename: %s" % (self.restrictedLongFilename))
        header = None
        positions = []

        with open(self.restrictedLongFilename, 'r') as csvfile:
            reader = csv.reader(csvfile, delimiter = ',')

            for row in reader:
                if header is None:
                    header = row
                else:
                    ticker = row[0].strip()
                    figi = row[1].strip()

                    if figi:
                        self.log.info("LONG Restricted: %s - %s" % (figi, ticker))

                        if figi in self.refDataMap:
                            self.refDataMap[figi]["isRestrictedLong"] = True
                        else:
                            self.log.error("MISSING RESTRICTED LONG SYMBOL %s (%s)" % (figi, ticker))

                            self.addToRefData(figi, ticker)
                            self.refDataMap[figi]["isRestrictedLong"] = True

    def handleRestrictedList(self, restrictedList, key):
        utcNow = DT.datetime.utcnow()
        figis = restrictedList["data"]["SYMBOL"]
        tickers = restrictedList["data"]["Ticker"]
        unrestrictedDates = restrictedList["data"]["UNRESTRICTEDDATE"]
        numFigis = len(figis)

        for i in range(numFigis):
            figi = figis[i]
            
            if figi:
                ticker = tickers[i]
                unrestrictedDate = DT.datetime.fromtimestamp(unrestrictedDates[i] / 1000000000) if unrestrictedDates[i] else None

                if unrestrictedDate is None or unrestrictedDate >= utcNow:
                    if figi in self.refDataMap:
                        self.refDataMap[figi][key] = True
                    else:
                        self.log.error("MISSING RESTRICTED SYMBOL %s (%s)" % (figi, key))

                        self.addToRefData(figi, ticker)
                        self.refDataMap[key] = True

    def getRestrictedList(self):
        self.log.info("Getting restricted list ...")
        restrictedList = trade_util.bamRestrictedList(self.serverUrl, ymlVer = self.ymlVer, username = self.username, password = self.password)
        assert restrictedList, "Unable to get restricted list"
        self.handleRestrictedList(restrictedList, "isRestricted")
        self.log.info("... Got restricted list")

        self.log.info("Getting AQTF restricted SHORT list ...")
        restrictedAQTFShortList = trade_util.bamRestrictedShortListAQTF(self.serverUrl, ymlVer = self.ymlVer, username = self.username, password = self.password)
        assert restrictedAQTFShortList, "Unable to get AQTF restricted short list"
        self.handleRestrictedList(restrictedAQTFShortList, "isRestrictedShort")
        self.log.info("... Got AQTF restricted SHORT list")

        self.log.info("Getting restricted SHORT list ...")
        restrictedShortList = trade_util.bamRestrictedShortList(self.serverUrl, ymlVer = self.ymlVer, username = self.username, password = self.password)
        assert restrictedShortList, "Unable to get restricted short list"
        self.handleRestrictedList(restrictedShortList, "isRestrictedShort")
        self.log.info("... Got restricted SHORT list")

        self.log.info("Getting restricted LONG list ...")
        self.handleRestrictedLongList()
        self.log.info("... Got restricted LONG list")

    def addToRefData(self, figi, ticker):
        if figi and ticker and figi not in self.refDataMap and figi != "NOT_FOUND":
            self.refDataMap[figi] = {
                "ticker": ticker,
                "isRestricted": False,
                "isRestrictedShort": False,
                "isRestrictedLong": False,
            }

            if ticker not in self.invRefDataMap:
                self.invRefDataMap[ticker] = [figi]
            else:
                self.invRefDataMap[ticker].append(figi)

    def getReferenceData(self):
        self.log.info("Getting BAM reference data ...")

        self.refData = trade_util.bamReferenceData(self.serverUrl, ymlVer = self.ymlVer, username = self.username, password = self.password, dataEnv = self.dataEnv)
        figis = self.refData["data"]["SYMBOL"]
        tickers = self.refData["data"]["DisplayCode"]

        numSecurities = len(tickers)
        self.log.info("... Got BAM reference data: %d" % (numSecurities))

        assert len(figis) == numSecurities, "Invalid length of tickers array and BB global unique ids"

        for i in range(numSecurities):
            figi = figis[i]
            ticker = tickers[i]
            if "DO NOT TRADE" in ticker:
                continue
            ticker = ticker.replace("RSTD", '').strip()

            if self.mlpSwapName:
                ticker = ticker.replace(self.mlpSwapName, '').strip()

            if self.swap:
                ticker = ticker.replace(self.swap, '').strip()

            # if not ticker or not figi:
            #     self.log.info("Invalid FIGI: %s - TKR: %s" % (figi, ticker))

            self.addToRefData(figi, ticker)

    def mapPositions(self, positions, refDataMap):
        self.log.info("Mapping positions to BAM symbology ...")

        for position in positions:
            figi = position["figi"]
            bbTicker = position["bbTicker"]

            if figi not in refDataMap:
                # position["tradeTicker"] = None
                # If there is no ticker then we restrict that stock
                # position["isRestricted"] = True
                self.log.error("MISSING SYMBOL %s - %s" % (figi, bbTicker))
                continue
            # assert figi in refDataMap, "Unable to map figi: %s - %s" % (figi, bbTicker)

            tradeTicker = refDataMap[figi]["ticker"]
            if tradeTicker:
                position["tradeTicker"] = tradeTicker
                position["isRestricted"] = refDataMap[figi]["isRestricted"]
                position["isRestrictedShort"] = refDataMap[figi]["isRestrictedShort"]
                position["isRestrictedLong"] = refDataMap[figi]["isRestrictedLong"]
            else:
                print("Invalid trade ticker for: %s - %s" % (figi, bbTicker))
            # assert tradeTicker, "Invalid trade ticker for: %s - %s" % (figi, bbTicker)

        self.log.info("... Mapping done")

    def printPositions(self):
        positionsFile = open("./currentPositions", "w")
        positionsFile.write("%20s, %20s\n" % ("BAM-Ticker", "Position"))

        existingPositions = self.getPositions()

        for existingPosition in existingPositions:
            epPort = existingPosition["Portfolio"]
            if epPort["Strategy"] == self.strategyId and epPort["SubStrategy"] == self.subStrategyId:
                # This is our strategy
                security = existingPosition["Security"]
                bamSymbol = security["BamSymbol"]
                positionsFile.write("%20s, %20.2f\n" % (bamSymbol, existingPosition["ActualQuantity"]))

        positionsFile.close()

    def loadExistingPositions(self):
        if self.useLivePositions is False:
            return

        self.log.info("Loading current live positions!")

        existingPositions = self.getPositions()
        ourExistingPositions = []

        for existingPosition in existingPositions:
            epPort = existingPosition["Portfolio"]
            if epPort["Strategy"] == self.strategyId and epPort["SubStrategy"] == self.subStrategyId:
                ourExistingPositions.append(epPort)
                # This is our strategy
                security = existingPosition["Security"]
                bamSymbol = security["BamSymbol"]

                # Change in DO NOT TRADE BEHAVIOUR. We do not trade these any more. So if we have a DO NOT TRADE position
                # we just ignore it ...
                if "DO NOT TRADE" in bamSymbol:
                    self.log.info("Ignoring DNT: %s - [%d]" % (bamSymbol, int(existingPosition["ActualQuantity"])))
                    continue

                # bamSymbol = security["BamSymbol"].replace("DO NOT TRADE", '').strip()
                bamSymbol = bamSymbol.replace("RSTD", '').strip()

                if self.mlpSwapName:
                    bamSymbol = bamSymbol.replace(self.mlpSwapName, '').strip()

                if self.swap:
                    bamSymbol = bamSymbol.replace(self.swap, '').strip()

                # Is this a hedge
                if bamSymbol.strip().endswith("_IDX"):
                    self.log.info("Ignoring hedge: %s" % (bamSymbol)) 
                    continue

                if bamSymbol not in self.invRefDataMap:
                    self.log.info("Stock not in REF DATA MAP: %s. Liquidating ..." % (bamSymbol)) 

                    self.positions.append({
                        "bbTicker": bamSymbol,
                        "figi": "NOT_FOUND",
                        "cfigi": "NOT_FOUND",
                        "tradeTicker": bamSymbol,
                        "currentPosition": existingPosition["ActualQuantity"],
                        "targetPosition": 0,
                        "deltaPosition": 0,
                        "notional": 0,
                        "isRestricted": False,
                        "isRestrictedShort": False,
                        "isRestrictedLong": False,
                        "price": 0.0
                    }) 

                    continue

#                    self.log.info("Unable to find symbol: %s" % (bamSymbol))
                assert bamSymbol in self.invRefDataMap, "Unable to find symbol: %s" % (bamSymbol)

                figis = self.invRefDataMap[bamSymbol]
                posIndex = -1

                for figi in figis:
                    if figi in self.positionsIndexMap:
                        posIndex = self.positionsIndexMap[figi]
                        break
                    elif bamSymbol in self.positionsIndexMap:
                        posIndex = self.positionsIndexMap[bamSymbol]
                        break

                # We managed to find an existing position for this new position ... we need to net it
                assert posIndex < len(self.positions), "Invalid posIndex: %d" % (posIndex)
                if posIndex >= 0:
                    # We need to do an add in case our orders were split into multiple positions for some reason
                    self.positions[posIndex]["currentPosition"] += existingPosition["ActualQuantity"]
                else:
                    figi = figis[0]

                    self.log.info("Stock not found in current positions: %s - %s. Liquidating ..." % (figi, bamSymbol)) 
                    self.positions.append({
                        "bbTicker": bamSymbol,
                        "figi": figi,
                        "cfigi": figi,
                        "tradeTicker": bamSymbol,
                        "currentPosition": existingPosition["ActualQuantity"],
                        "targetPosition": 0,
                        "deltaPosition": 0,
                        "notional": 0,
                        "isRestricted": self.refDataMap[figi]["isRestricted"],
                        "isRestrictedShort": self.refDataMap[figi]["isRestrictedShort"],
                        "isRestrictedLong": self.refDataMap[figi]["isRestrictedLong"],
                        "price": 0.0
                    }) 

        self.log.info("Num existing positions: %d" % (len(ourExistingPositions)))
        return ourExistingPositions

    def loadOrdersForTickers(self):
        liveOrders = self.getStrategyLiveOrders()
        self.log.info("Loaded number of live orders: %d" % (len(liveOrders)))

        mappedLiveOrders = {}

        for liveOrder in liveOrders:
            sec = liveOrder["Security"]
            mappedLiveOrders[sec["BamSymbol"]] = liveOrder

        for ticker in self.tickers:
            if ticker not in mappedLiveOrders:
                self.log.info("Unable to find any orders for ticker: %s" % (ticker))
            else:
                liveOrder = mappedLiveOrders[ticker]
                orderId = liveOrder["Key"]
                self.log.info("Ticker: %s - OrderId: %s" % (ticker, orderId))
                self.orderIds.append(orderId)

    def cancelCurrentOrders(self):
        if self.cancel is False:
            return

        if self.cancelAll:
            trade_util.sendEmail("Cancelling Strategy: " + self.fullStrategyId, body = "Cancelling ...", recipients = self.recipients, attachments = [self.settings.logFilename], dataEnv = self.dataEnv)

            self.log.info("Cancelling existing strategy: %s" % (self.fullStrategyId))
            res = self.cancelStrategy()
            self.log.info("Cancel strategy: %s" % (str(res)))

            self.log.info("Checking whether strategy is complete ...")
            res = False

            while res is False:
                res = self.isStrategyComplete()
                self.log.info("IsStrategyComplete returned: %s" % (str(res)))
                res = bool(res)

            self.log.info("Strategy complete. Orders cancelled!")
            return

        # self.log.info("Only cancelling overlapping positions ...")
        liveOrders = self.getStrategyLiveOrders()
        self.log.info("Loaded number of live orders: %d" % (len(liveOrders)))

        mappedLiveOrders = {}

        for liveOrder in liveOrders:
            sec = liveOrder["Security"]
            mappedLiveOrders[sec["BamSymbol"]] = liveOrder

        # Now get the overlapping positions ...

    def getRestrictedMLPList(self):
        if self.restrictedMLPFilename is None:
            return 
            
        self.log.info("Loading Restricted MLP filename: %s" % (self.restrictedMLPFilename))
        if not os.path.exists(self.restrictedMLPFilename):
            self.log.warning("MLP RESTRICTED FILE NOT FOUND: %s" % (self.restrictedMLPFilename))
            return

        file = open(self.restrictedMLPFilename, "r")
        lines = file.readlines()

        self.log.info("Restricted MLP Count: %d" % (len(lines) - 1))

        for i in range(1, len(lines)):
            line = lines[i].strip()
            symbol = line
            newSymbol = symbol + " " + self.mlpSwapName
            self.symbolRemap[symbol] = newSymbol

    def dumpTrades(self):
        existingTrades = self.getTrades()
        ourTrades = []
        execInfo = {}

        for existingTrade in existingTrades:
            port = existingTrade["Portfolio"]

            if port["Strategy"] == self.strategyId and port["SubStrategy"] == self.subStrategyId:
                ourTrades.append(existingTrade)
                security = existingTrade["Security"]
                bamSymbol = security["BamSymbol"]
                bamSymbol = bamSymbol.replace("RSTD", '').strip()

                if self.mlpSwapName:
                    bamSymbol = bamSymbol.replace(self.mlpSwapName, '').strip()

                if self.swap:
                    bamSymbol = bamSymbol.replace(self.swap, '').strip()

                fillQuantity = int(existingTrade["FillQuantity"])
                price = existingTrade["AveragePrice"]
                side = existingTrade["Side"].lower()
                
                if side == "sell" or side == "shortsell" or "sell" in side:
                    fillQuantity *= -1

                if bamSymbol not in execInfo:
                    execInfo[bamSymbol] = {
                        "fillQuantity": fillQuantity,
                        "price": price,
                        "totalPrice": price * abs(fillQuantity),
                        "priceCount": abs(fillQuantity),
                        "position": 0
                    }
                else:
                    execInfo[bamSymbol]["fillQuantity"] += fillQuantity
                    execInfo[bamSymbol]["totalPrice"] += price * abs(fillQuantity)
                    execInfo[bamSymbol]["priceCount"] += abs(fillQuantity)
                    # execInfo[bamSymbol]["price"] = execInfo[bamSymbol]["totalPrice"] / execInfo[bamSymbol]["priceCount"]

        existingPositions = self.getPositions()
        ourExistingPositions = []

        for existingPosition in existingPositions:
            epPort = existingPosition["Portfolio"]
            if epPort["Strategy"] == self.strategyId and epPort["SubStrategy"] == self.subStrategyId:
                # This is our strategy
                security = existingPosition["Security"]
                bamSymbol = security["BamSymbol"]

                if "DO NOT TRADE" in bamSymbol:
                    continue
                    
                bamSymbol = bamSymbol.replace("RSTD", '').strip()

                if self.mlpSwapName:
                    bamSymbol = bamSymbol.replace(self.mlpSwapName, '').strip()

                if self.swap:
                    bamSymbol = bamSymbol.replace(self.swap, '').strip()

                if bamSymbol not in execInfo:
                    execInfo[bamSymbol] = {
                        "fillQuantity": 0,
                        "price": 0,
                        "totalPrice": 0,
                        "priceCount": 0,
                        "position": existingPosition["ActualQuantity"]
                    }
                else:
                    execInfo[bamSymbol]["position"] = existingPosition["ActualQuantity"]

        # self.log.info("Number of existing trades: %d / %d" % (len(ourTrades), len(existingTrades)))
        filename = os.path.join(os.path.expanduser(self.dumpTradesPath), self.dataEnv + "_MyPort_" + dateToStr() + ".fills")
        self.log.info("Dumping fills to file: %s" % (filename))

        file = open(filename, "w")
        file.write("%16s, %16s, %16s, %16s\n" % ("BAM-Ticker", "Quantity", "Price", "Position"))
        for ticker, einfo in execInfo.items(): 
            price = 0.0
            if einfo["priceCount"] != 0.0:
                price = einfo["totalPrice"] / einfo["priceCount"]
            einfo["price"] = price
            file.write("%16s, %16d, %16.4f, %16.4f\n" % (ticker, einfo["fillQuantity"], price, einfo["position"]))

        file.close()

        return

    def submit(self):
        try:
            if self.doGetTrades:
                self.dumpTrades()
                return 

            if not self.posFilename and not self.intermediate:
                if self.useLivePositions:
                    self.printPositions()
                    return
                elif self.cancel:
                    print("Cancelling current orders!")
                    self.cancelCurrentOrders()
                    return

            assert self.username, "Invalid username!"
            assert self.password, "Invalid password!"

            self.getReferenceData()
            self.getRestrictedList()
            self.getRestrictedMLPList()

            ordersCSV = self.ordersFilename

            if self.tickers and len(self.tickers):
                self.loadOrdersForTickers()

            if self.orderIds and len(self.orderIds) and self.cancel:
                self.log.info("Cancelling orders: %s" % (str(self.orderIds)))
                res = self.cancelOrders(self.orderIds)
                self.log.info("Cancel orders response: %s" % (res))
                return

            self.positions = self.loadPositions()
            assert self.positions, "Unable to load positions: %s" % (self.posFilename)

            self.mapPositions(self.positions, self.refDataMap)

            file = None
            intermediateFile = None
            orders = []

            self.cancelCurrentOrders()

            self.loadExistingPositions()

            if ordersCSV:
                file = open(ordersCSV, "w")
                file.write("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n" % ("Security", "Side", "Amount", "TAlpha2", "Manager", "Cust", "StartTime", "EndTime", "Algo", "Urgency", "MaxPartRate", "Duration", "AutoRoute", "Benchmark"))

            if self.intermediate:
                intermediateFile = open(self.intermediate, "w")
                intermediateFile.write("%20s, %20s, %20s, %20s, %20s, %20s, %20s, %20s, %20s, %20s, %20s, %20s, %20s, %20s\n" % 
                    ("FIGI", "BAM-Ticker", "BB-Ticker", "Trade", "New Target", "Curr. Position", "Is Restricted", 
                    "Is Short-Restricted", "Is Long-Restricted", "Target Notional", "CurrPos. Notional", "Trade Notional", "Price", "CFIGI"))

            posLogs = []

            netTargetNotional = 0
            netCurrentNotional = 0
            netTradeNotional = 0

            netAbsTargetNotional = 0
            netAbsCurrentNotional = 0
            netAbsTradeNotional = 0

            for position in self.positions:
                if position["tradeTicker"] is None:
                    continue

                orgTicker = position["tradeTicker"]

                if position["tradeTicker"] in self.symbolRemap:
                    oldSymbol = position["tradeTicker"]
                    newSymbol = self.symbolRemap[position["tradeTicker"]]

                    self.log.info("Remapping: %s => %s", oldSymbol, newSymbol)
                    position["tradeTicker"] = newSymbol

                if self.swap and self.swap not in position["tradeTicker"]:
                    position["tradeTicker"] = position["tradeTicker"] + " " + self.swap

                # self.log.info("%s - %s: %0.2f = %0.2f - %0.2f" % (position["figi"], position["tradeTicker"], position["deltaPosition"], position["targetPosition"], position["currentPosition"]))
                isRestricted = position["isRestricted"]
                isRestrictedShort = position["isRestrictedShort"]
                isRestrictedLong = position["isRestrictedLong"]
                ignorePosition = False
                isRestrictedStr = "no"
                isRestrictedShortStr = "no"
                isRestrictedLongStr = "no"

                if isRestricted:
                    self.log.info("Restricted: %s - %s" % (position["figi"], position["tradeTicker"]))
                    isRestrictedShortStr = "YES"
                    ignorePosition = True

                if isRestrictedShort and position["targetPosition"] < 0:
                    self.log.info("Restricted SHORT: %s - %s. Desired target: %0.2f" % (position["figi"], position["tradeTicker"], position["targetPosition"]))
                    isRestrictedShortStr = "YES"

                    # If we are currently LONG on this security then we can safely get it down to 0
                    if position["currentPosition"] > 0:
                        position["targetPosition"] = 0 # ensure that the delta = 0
                        self.log.info("Restricted SHORT: %s - %s. Liquidating from current position: %0.2f" % (position["figi"], position["tradeTicker"], position["currentPosition"]))
                        position["deltaPosition"] = 0
                        ignorePosition = False # We cannot ignore this position
                    else:
                        ignorePosition = True # Ok, either we're already short or we have nothing. In either case, we cannot short any more of this

                if isRestrictedLong and position["targetPosition"] > 0:
                    self.log.info("Restricted LONG: %s - %s. Desired target: %0.2f" % (position["figi"], position["tradeTicker"], position["targetPosition"]))
                    isRestrictedLongStr = "YES"

                    # If we are currently LONG on this security then we can safely get it down to 0
                    if position["currentPosition"] < 0:
                        position["targetPosition"] = 0 # ensure that the delta = 0
                        self.log.info("Restricted LONG: %s - %s. Liquidating from current position: %0.2f" % (position["figi"], position["tradeTicker"], position["currentPosition"]))
                        position["deltaPosition"] = 0
                        ignorePosition = False # We cannot ignore this position
                    else:
                        ignorePosition = True # Ok, either we're already long or we have nothing. In either case, we cannot long any more of this

                position["deltaPosition"] = position["targetPosition"] - position["currentPosition"]

                shares = position["deltaPosition"]

                if ignorePosition:
                    continue

                sharePrice = 0
                if "price" in position and position["price"] > 0:
                    sharePrice = position["price"]
                elif position["targetPosition"] != 0:
                    sharePrice = position["notional"] / position["targetPosition"]
                
                targetNotional = position["notional"]
                currentNotional = position["currentPosition"] * sharePrice
                tradeNotional = position["deltaPosition"] * sharePrice

                netTargetNotional += (targetNotional)
                netCurrentNotional += (currentNotional)
                netTradeNotional += (tradeNotional)

                netAbsTargetNotional += abs(targetNotional)
                netAbsCurrentNotional += abs(currentNotional)
                netAbsTradeNotional += abs(tradeNotional)

                # We don't send positions that do not meet a minimum notional requirements (currently $1000.0 as default, use -N to override)
                # but we also check whether we're trying to liquidate something. If we are, then we just let it go and get to 0
                if abs(tradeNotional) < self.minNotional and abs(targetNotional) > 0.1:
                    self.log.info("Minimum notional requirement not met: %s - %s [Trade Notional = %0.2f, Min. Notional: %0.2f]" % (position["cfigi"], position["tradeTicker"], tradeNotional, self.minNotional))
                    # readjust the net trade and net abs trade notionals
                    netTradeNotional -= (tradeNotional)
                    netAbsTradeNotional -= abs(tradeNotional)
                    continue

                ss = str.format("%s - %s: %0.2f = %0.2f - %0.2f" % (position["figi"], position["tradeTicker"], position["deltaPosition"], position["targetPosition"], position["currentPosition"]))
                posLogs.append(ss)

                if intermediateFile:
                    pfigi = position["figi"]
                    cfigi = position["cfigi"] if "cfigi" in position else pfigi
                    intermediateFile.write("%20s, %20s, %20s, %20.2f, %20.2f, %20.2f, %20s, %20s, %20s, %20.5f, %20.5f, %20.5f, %20.5f, %20s\n" % (pfigi, position["tradeTicker"], position["bbTicker"], position["deltaPosition"], 
                        position["targetPosition"], position["currentPosition"], isRestrictedStr, isRestrictedShortStr, isRestrictedLongStr, targetNotional, currentNotional, tradeNotional,
                        sharePrice, cfigi))

                ticker = position["tradeTicker"]
                if ticker and shares:
                    absShares = abs(shares)
                    side = "Buy" if shares > 0 else "Sell"

                    algo = self.defaultAlgo

                    if orgTicker in self.algoOverrides:
                        algo = self.algoOverrides[orgTicker]

                    order = {
                        'Custodian': self.custodian, 
                        'BatchId': self.batchId, # 
                        'Security':  {
                            'Key': ticker, 
                            'BamSymbol': ticker, 
                            'SecurityType': 'Equity'
                        },
                        'Portfolio': {
                            'PMCode': self.pmCode, 
                            'SubStrategy': self.subStrategyId,  # US/EU/ASIA
                            'AggregationUnit': "AQTF", # AQTF
                            'Strategy': "GENERALIST"
                        } ,
                        'Size': absShares,   # number of shares
                        'Side': side,   # Buy/Sell
                        'Price': 0, # 0 [limit price]
                        'ExecutionInstructions': [{ "Code": "Algo", "Value": algo }] # Code from Francois and Zohar 
                    }

                    if self.autoRoute:
                        order['ExecutionInstructions'].append({ "Code": "AutoRoute", "Value": "true" })

                    orders.append(order)
                    # orders = [order]

                    if file:
                        file.write("%s,%s,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n" % 
                            (ticker, side, absShares, self.pmCode + "-" + self.strategyId + "-" + self.subStrategyId, self.manager, self.custodian, "10:00", "14:00", algo, "", "10", "", "", ""))

            if file:
                file.close()

            if intermediateFile:
                intermediateFile.write("# Net target  : %20.5f [Abs: %20.5f]\n" % (netTargetNotional, netAbsTargetNotional))
                intermediateFile.write("# Net current : %20.5f [Abs: %20.5f]\n" % (netCurrentNotional, netAbsCurrentNotional))
                intermediateFile.write("# Net trade   : %20.5f [Abs: %20.5f]\n" % (netTradeNotional, netAbsTradeNotional))

                intermediateFile.close()

            if self.force is False:
                checkValuesWithinTolerance("Target Notional", netAbsCurrentNotional, netAbsTargetNotional, 0.01)

                if netAbsTradeNotional > self.dollarTurnOver:
                    checkValuesWithinTolerance("Dollar Turnover", self.dollarTurnOver, netAbsTradeNotional, 0.15)

            # self.log.info("######## BEGIN POSITIONS ########")
            # self.log.info("\n".join(posLogs))
            # self.log.info("######## END POSITIONS   ########")

            if self.execute is True:
                self.log.info("Submitting orders. Count: %d" % (len(orders)))
                # Now that we've written the order, we need to submit
                res = self.makeRequest("/api/Order/PostOrders", data = json.dumps(orders), rtype = "post")
                self.log.info("PostOrders Response: %s" % (str(res)))
                trade_util.sendEmail(subject = "Submitted", body = str(res), recipients = self.recipients, attachments = [self.settings.logFilename], dataEnv = self.dataEnv)

        except Exception as e:
            exStr = traceback.format_exc()
            self.log.error(exStr)

            trade_util.sendEmail(subject = "Fatal Exception!", body = exStr, recipients = self.recipients, attachments = [self.settings.logFilename], dataEnv = self.dataEnv)
            sys.exit(-1)

    def makeURL(self, endPoint):
        from urllib.parse import urljoin
        return urljoin(self.tradeServer, endPoint)

    def makeRequest(self, endPoint, params = None, data = None, headers = None, rtype = "get"):
        authToken = "Bearer " + self.apiToken

        if headers is None:
            headers = {
                'content-type': 'application/json;charset=utf8', 
                'Accept': 'application/json',
                'Authorization': authToken, 
            }
        elif 'Authorization' not in headers:
            headers['Authorization'] = authToken

        url = self.makeURL(endPoint)
        response = None
        if rtype == "get" and not data:
            response = requests.get(url, params = params, headers = headers)
        elif rtype == "delete" and not data:
            response = requests.delete(url, params = params, headers = headers)
        elif rtype == "post" or data is not None:
            response = requests.post(url, data = data, headers = headers, verify = False)

        response.raise_for_status()

        if response.status_code != 200:
            text = responseData.text.replace("{", "{{")
            text = text.replace("}", "}}")
            errorStr = str.format("Server error. Response code: %d - Response Text: %s" % (responseData.status_code, text))
            self.log.error(errorStr)
            raise errorStr

        return json.loads(response.text)


    def getPositions(self, headers = None):
        return self.makeRequest("/api/position/get")

    def getTrades(self, time = datetime.utcnow(), headers = None):
        # return self.makeRequest("/api/order/get", params = {
        #     "cutoffTimeUtc": time
        # })
        return self.makeRequest("/api/order/get")

    def cancelStrategy(self, headers = None):
        assert self.strategyId, "Empty strategyId"
        return self.makeRequest("/api/Order/CancelOrdersInDetailStrategy", params = {
            "detailStrategy": self.fullStrategyId
        })

    def cancelBatch(self, headers = None):
        assert self.strategyId, "Empty strategyId"
        assert self.batchId, "Empty batchId"

        return self.makeRequest("/api/Order/CancelOrdersInDetailStrategyAndBatch", params = {
            "detailStrategy": self.fullStrategyId,
            "batchId": self.batchId,
        })

    def cancelOrders(self, headers = None):
        assert self.orderIds and len(self.orderIds), "Empty orderIds array"
        return self.makeRequest("/api/Order/DeleteOrders", params = {
            "ordersIds": self.orderIds
        }, rtype = "delete")

    def getStrategyLiveOrders(self, headers = None):
        assert self.strategyId, "Empty strategyId"
        return self.makeRequest("/api/Order/GetLiveOrdersInDetailStrategy", params = {
            "detailStrategy": self.fullStrategyId,
        })

    def getBatchLiveOrders(self, headers = None):
        assert self.strategyId, "Empty strategyId"
        assert self.batchId, "Empty batchId"
        
        return self.makeRequest("/api/Order/GetLilveOrdersInDetailStrategyAndBatch", params = {
            "detailStrategy": self.fullStrategyId,
            "batchId": self.batchId,
        })

    def isStrategyComplete(self, headers = None):
        assert self.strategyId, "Empty strategyId"
        return self.makeRequest("/api/Order/IsStrategyComplete", params = {
            "detailStrategy": self.fullStrategyId,
        })

    def isBatchComplete(self, headers = None):
        assert self.batchId, "Empty batchId"
        return self.makeRequest("/api/Order/IsBatchComplete", params = {
            "detailStrategy": self.fullStrategyId,
        })

    def isStrategyAndBatchComplete(self, headers = None):
        assert self.strategyId, "Empty strategyId"
        assert self.batchId, "Empty batchId"
        
        return self.makeRequest("/api/Order/IsStrategyAndBatchComplete", params = {
            "detailStrategy": self.fullStrategyId,
            "batchId": self.batchId,
        })

trade = Trade()
trade.submit()
