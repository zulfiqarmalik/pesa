### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#!python

import os
import sys
import traceback
import time
import glob

if os.path.exists("inter.csv"):
    os.remove("inter.csv")

for filename in glob.glob("HKHA_EQUITIES_US_*.csv"):
    path = os.path.dirname(filename)
    fname = os.path.basename(filename)

    srcFilename = filename
    dstFilename = os.path.join(path, "TradeHistory", fname)
    print("Moving: %s => %s" % (srcFilename, dstFilename))

    os.rename(srcFilename, dstFilename)

for filename in glob.glob("*.pos"):
    print("Removing: %s" % (filename))
    os.remove(filename)

