### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys
import logging
from optparse import OptionParser
# from log_util import RainbowLoggingHandler

class Settings:
    def __init__(self, app):
        self.app        = app
        self.basePath   = os.path.realpath(__file__)
        self.options    = None
        self.args       = None
        self.argv       = sys.argv
        self.logDir     = "./"
        self.baseDir    = "./"
        
        if sys.platform == "win32" or sys.platform == "win64":
            self.platform= "windows"
            self.baseDir = "Y:\\"
        
    def initLogging(self):
        # setup `logging` module
        logger = logging.getLogger("trade")

        # console logging
        formatter = logging.Formatter("[%(asctime)s:%(name)s]: %(message)s ")  # same as default

        if not self.options.logFilename:
            self.logDir = self.baseDir
            self.logFilename = os.path.join(self.logDir, "trade.log")
        else:
            self.logFilename = self.options.logFilename
            self.logDir = os.path.dirname(self.logFilename)

        # file logging 
        fileHandler = logging.FileHandler(self.logFilename, "w")
        fileHandler.setFormatter(formatter) 

        consoleHandler = logging.StreamHandler()
        consoleHandler.setFormatter(formatter)

        logger.addHandler(consoleHandler)
        logger.addHandler(fileHandler) 

        logger.setLevel(logging.INFO)

        self.log = logger

        if self.options.logLevel is "debug":
            self.log.setLevel(logging.DEBUG)
        elif self.options.logLevel is "info":
            self.log.setLevel(logging.INFO)
        elif self.options.logLevel is "warn":
            self.log.setLevel(logging.WARNING)
        else:
            raise RuntimeError("Invalid logging option: " + logger.logLevel)

    def parseArguments(self):
        parser = OptionParser()

        parser.add_option("-a", "--all", dest = "all", action="store_false", default = True, help = "Whether to cancel ALL i.e. strategy or just the overlapping orders")
        parser.add_option("-A", "--algo", dest = "algo", default = "/home/zmalik/tdrv/Thirdparty/BAM/Algo.csv", help = "The algo filename")
        parser.add_option("-b", "--substrategy", dest = "subStrategy", default = "US", help = "The sub-strategy e.g. US/EU etc.")
        parser.add_option("-B", "--batchId", dest = "batchId", default = "EQUITIES", help = "A batch id that can be used to get some information about an update during the day")
        parser.add_option("-C", "--custodian", dest = "custodian", default = "CGMI", help = "The custodian")
        parser.add_option("-e", "--email", dest = "email", default = "zmalik@bamfunds.com;hkhan@bamfunds.com;ahellings@bamfunds.com;agoodridge@bamfunds.com;rpandey@bamfunds.com", help = "List of people who will be emailed a log file in the event the Trade module goes wrong")
        # parser.add_option("-e", "--email", dest = "email", default = "zmalik@bamfunds.com", help = "List of people who will be emailed a log file in the event the Trade module goes wrong")
        # parser.add_option("-E", "--dataEnv", dest = "dataEnv", default = "UAT", help = "The data environment")
        parser.add_option("-F", "--force", dest = "force", action="store_true", default = False, help = "Force sending of positions (disables some of the kill switches)")

        parser.add_option("-g", "--dumpTradesPath", dest = "dumpTradesPath", default = "~/drv/devwork/prod/Strategies/US/MASTER_D1/recon/fills", help = "Whether to cancel ALL i.e. strategy or just the overlapping orders")
        parser.add_option("-G", "--getTrades", dest = "getTrades", action="store_true", default = False, help = "Get and dump trades. Please override the default dumpTradesPath argument as well (if you need to)")

        parser.add_option("-I", "--intermediate", dest = "intermediate", default = None, help = "The path to the intermediate file to use")
        parser.add_option("--logLevel", dest = "logLevel", default = "debug", help = "What log level to set. Choose from [debug, info, warn]")

        parser.add_option("-K", "--autoRoute", dest = "autoRoute", default = False, action = "store_true", help = "What log level to set. Choose from [debug, info, warn]")

        parser.add_option("-l", "--logFile", dest = "logFilename", default = "", help = "The path to the log file")
        parser.add_option("-L", "--live", dest = "useLivePositions", default = False, action = "store_true", help = "What log level to set. Choose from [debug, info, warn]")

        parser.add_option("-M", "--manager", dest = "manager", default = "HKH", help = "The name of the manager")
        parser.add_option("-m", "--pmCode", dest = "pmCode", default = "HKHA", help = "The PM Code")

        parser.add_option("-N", "--minNotional", dest = "minNotional", default = 1000.0, help = "The minimal notional (in dollars) to send")

        parser.add_option("-o", "--ordersFilename", dest = "ordersFilename", default = "./orders.csv", help = "The name of the orders file")
        parser.add_option("-O", "--orderId", dest = "orderId", default = "", help = "The order id(s). Comma separated")
        parser.add_option("-p", "--password", dest = "password", default = "", help = "The password to use for the trading and Data API")
        parser.add_option("-P", "--pos", dest = "posFilename", default = "", help = "The input file to pick up the positions from")

        parser.add_option("-R", "--restrictedMLP", dest = "restrictedMLPDir", default = "~/tdrv/Thirdparty/RestrictedMLP/@CI", help = "The directory containing the RestrictedMLP files. The format of this argument is <PATH>@<Broker ID> where <Broker ID> will be the ID of the broker for the SWAP")
        parser.add_option("-r", "--restrictedLong", dest = "restrictedLong", default = "~/tdrv/Thirdparty/BAM/LongBuyRestricted.csv", help = "The restricted long stocks")

        parser.add_option("-s", "--server", dest = "server", default = "http://ems-gtwy-uat:8091", help = "The trading server URL")
        parser.add_option("-S", "--strategyId", dest = "strategyId", default = "GENERALIST", help = "The name of the strategy")

        parser.add_option("-t", "--tickers", dest = "tickers", default = None, help = "Perform action with a specific ticker(s). Comma separated")
        parser.add_option("-T", "--apiToken", dest = "apiToken", default = "", help = "The API token")
        parser.add_option("-u", "--username", dest = "username", default = "", help = "The username to use for the trading and Data API")
        parser.add_option("-U", "--URL", dest = "serverUrl", default = "http://dalapi/v1/get", help = "The BAM Data API base URL")

        parser.add_option("-W", "--swap", dest = "swap", default = "", help = "The SWAP that we're going to use")
        parser.add_option("-X", "--cancel", dest = "cancel", action="store_true", default = False, help = "Whether to cancel or not. Default is set to FALSE")
        parser.add_option("-Z", "--execute", dest = "execute", action="store_true", default = False, help = "Whether to send the trades or not")

        (self.options, self.args) = parser.parse_args()
        

