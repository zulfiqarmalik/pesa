### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import sys
import datetime as DT
import ctypes
import numpy as np
from requests_ntlm import HttpNtlmAuth

import os
import traceback
import time

import datetime as DT
import multiprocessing

def dateToStr(date, sep = "-"):
    return str(date.year).zfill(4) + sep + str(date.month).zfill(2) + sep + str(date.day).zfill(2)

def bamPOST(serverUrl, username, password, data):
    import requests
    from requests_ntlm import HttpNtlmAuth
    
    assert username, "Must specify a username -u parameter on the command line for NT authentication"
    assert password, "Must specify a password -p parameter on the command line for NT authentication"

    responseData = requests.post(serverUrl, data = data, auth = HttpNtlmAuth(username, password))

    # Some error was returned
    if responseData.status_code < 200 or responseData.status_code >= 300:
        text = responseData.text.replace("{", "{{")
        text = text.replace("}", "}}")
        print("Server error. Response code: %d - Response Text: %s" % (responseData.status_code, text))
        return None
        
    import json
    jsonStr = responseData.text
    data = json.loads(jsonStr)
    return data

def bamReferenceData(serverUrl, ymlVer, username, password, dataEnv = "UAT"):
    data = {
        "Keyword": "BAMReferenceData",
        "yml": "\\\\Fountainhead\\BAM\\DataAPI_Config\\prod\\" + ymlVer + "\\Shared\\Mappings.yml",
        "Symbols": "dict(Symbology='FIGI',IDs=None)",
        "SecurityTypeCode": "'Equity'",
        "AssetTypeCode": "'Equity'",
        "Fields": "['SYMBOL', 'DisplayCode']"
    }

    if dataEnv is not None:
        data["data_environment"] = "PROD"

    return bamPOST(serverUrl, username, password, data = data)

def bamRestrictedList(serverUrl, ymlVer, username, password):
    today = DT.datetime.utcnow()
    todayStr = dateToStr(today)
    return bamPOST(serverUrl, username, password, data = {
        "Keyword": "RestrictedSymbolList",
        "yml": "\\\\Fountainhead\\BAM\\DataAPI_Config\\prod\\" + ymlVer + "\\Shared\\Trading.yml",
        "Date": "'" + todayStr + "'",
        "Symbology": "'CFIGI'",
        "Fields": "['SYMBOL', 'RESTRICTEDDATE', 'UNRESTRICTEDDATE', 'Ticker']"
    })

def bamRestrictedShortListAQTF(serverUrl, ymlVer, username, password):
    today = DT.datetime.utcnow()
    todayStr = dateToStr(today)
    return bamPOST(serverUrl, username, password, data = {
        "Keyword": "RestrictedShortsForAQTF",
        "yml": "\\\\Fountainhead\\BAM\\DataAPI_Config\\prod\\" + ymlVer + "\\Shared\\TradingAQTF.yml",
        "Date": "'" + todayStr + "'",
        "Symbology": "'CFIGI'",
        "Fields": "['SYMBOL', 'RESTRICTEDDATE', 'UNRESTRICTEDDATE', 'Ticker']"
    })

def bamRestrictedShortList(serverUrl, ymlVer, username, password):
    today = DT.datetime.utcnow()
    todayStr = dateToStr(today)
    return bamPOST(serverUrl, username, password, data = {
        "Keyword": "RestrictedShortsForBAM",
        "yml": "\\\\Fountainhead\\BAM\\DataAPI\\bamdata_configs\\prod\\1.1.112\\config_shared\\Trading.yml",
        "Date": "'" + todayStr + "'",
        "Symbology": "'CFIGI'",
        "Fields": "['SYMBOL', 'RESTRICTEDDATE', 'UNRESTRICTEDDATE', 'Ticker']"
    })

def sendEmail(subject, body, recipients, sender = "hkha-trade@bamfunds.com", attachments = None, dataEnv = "UAT"):
    if dataEnv == "UAT":
        return
        
    import smtplib
    from os.path import basename
    from email.mime.application import MIMEApplication
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    from email.utils import COMMASPACE, formatdate

    msg             = MIMEMultipart()
    msg['From']     = sender
    msg['To']       = ", ".join(recipients)
    msg['Date']     = formatdate(localtime = True)
    msg['Subject']  = "HKHA - " + subject

    msg.attach(MIMEText(body))

    if attachments:
        for attachment in attachments:
            with open(attachment, "r") as fil:
                part = MIMEApplication(
                    fil.read(),
                    Name=basename(attachment)
                )
                part['Content-Disposition'] = 'attachment; filename="%s"' % basename(attachment)
                msg.attach(part)

    smtp = smtplib.SMTP("bamsmtp")
    smtp.sendmail(sender, recipients, msg.as_string())

