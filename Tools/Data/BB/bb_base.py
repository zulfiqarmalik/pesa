### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys
from optparse import OptionParser
import datetime as DT
import pandas as pd
from pandas.tseries.offsets import BDay, BMonthBegin

sys.path.append("../../../PyImpl/Scripts")
sys.path.append("../../../Python")
import PyUtil

import json
from shutil import copyfile
import zipfile
import csv
import math
import traceback

class BBBase:
    def __init__(self):
        self.opt                = None
        self.args               = None

    # def addTicker(self, ticker, figi, cfigi):
    #     assert ticker and figi and cfigi, 'Invalid ticker being added. Ticker: %s, FIGI: %s, C-FIGI: %s' % (ticker, figi, cfigi)

    #     if figi not in self.figisLUT:
    #         tindex = len(self.figis)
    #         self.figis.append({
    #             "ticker": ticker,
    #             "figi": figi,
    #             "cfigi": cfigi
    #         })
    #         self.figisLUT[figi] = tindex

    def loadIdMap(self, idmapFilename):
        assert os.path.isfile(idmapFilename), "Unable to find idmap filename: %s" % (idmapFilename)

        print("Loading idmap filename: %s" % (idmapFilename))

        with open(idmapFilename, 'r') as f:
            reader = csv.reader(f, delimiter = '|')
            rowCount = 0

            for row in reader:
                rowCount += 1

                if rowCount > 1 and len(row):
                    ticker = row[0].strip()
                    figi = row[5].strip()
                    cfigi = row[6].strip()
                    isin = row[8].strip()

                    self.addTicker(ticker, figi, cfigi, isin)

    def loadSecMaster(self, filename):
        secMasterFilename = filename + ".zip"
        assert os.path.isfile(secMasterFilename), "Invalid sec master filename that doesn't exist: %s" % (secMasterFilename)

        print("Loading sec master: %s" % (secMasterFilename))

        with zipfile.ZipFile(secMasterFilename, 'r') as zipHandle:
            name = os.path.basename(filename)
            fileContents = zipHandle.read(name,) 
            dataStr = fileContents.decode('utf-8')
            reader = csv.reader(dataStr.split('\n'), delimiter = '|')
            rowCount = 0

            for row in reader:
                rowCount += 1

                if rowCount == 1 or len(row) is 0:
                    continue

                ticker = row[2].strip()
                figi = row[9].strip()
                cfigi = row[10].strip()
                isin = row[11].strip()

                if ticker and figi and cfigi and isin:
                    self.addTicker(ticker, figi, cfigi, isin)
