### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys
from optparse import OptionParser
import datetime as DT
import pandas as pd
from pandas.tseries.offsets import BDay, BMonthBegin

sys.path.append("../../../PyImpl/Scripts")
sys.path.append("../../../Python")
import PyUtil

import json
from shutil import copyfile
import zipfile
import csv
import math
import traceback

from bb_base import BBBase

import threading
from multiprocessing import Process

class BBCombine(BBBase):
    def __init__(self):
        super().__init__()

        self.opt                = None
        self.args               = None
        self.figis              = []
        self.figisLUT           = {}
        self.unmappedTickers    = []
        self.numThreads         = 4

    def parseArguments(self):
        parser = OptionParser()

        parser.add_option("-i", "--input", dest = "inputDir", help = "The input directory")
        parser.add_option("-f", "--fields", default="PX_OPEN, PX_LAST, PX_LOW, PX_HIGH, PX_VOLUME, EQY_WEIGHTED_AVG_PX, EQY_SH_OUT, CUR_MKT_CAP", dest = "fields", help = "Comma separated fields to combine")
        parser.add_option("-o", "--output", dest = "outputDir", help = "The directory to output the data")
        parser.add_option("-n", "--name", dest = "name", help = "The name of the destination data")
        parser.add_option("-S", "--start", dest = "startDate", help = "The starting date of the download")
        parser.add_option("-E", "--end", dest = "endDate", help = "The end date of the download")
            
        return parser.parse_args()

    def run(self):
        self.opt, self.args = self.parseArguments()

        try:

            assert self.opt.inputDir, "Must specify a valid input directory!"
            assert self.opt.outputDir, "Must specify a valid output!"
            assert self.opt.fields, "Must specify the fields to combine!"
            assert self.opt.name, "Must specify a valid output name!"

            startDate = DT.datetime.strptime(self.opt.startDate, "%Y%m%d")
            endDate = DT.datetime.strptime(self.opt.endDate, "%Y%m%d") if self.opt.endDate else DT.datetime.now()

            dates = pd.date_range(startDate, endDate, freq = BDay())

            fields = self.opt.fields.split(',')

            for date in dates:
                idate = PyUtil.dateToInt(date)
                sdate = PyUtil.dateToStr(date)

                figiData = {}

                outputDir = os.path.join(self.opt.outputDir, self.opt.name, str(date.year), str(date.month).zfill(2))

                os.makedirs(outputDir, exist_ok = True)

                outputFilename = os.path.join(outputDir, str.format("%s_%d.csv" % (self.opt.name, idate)))
                outputZipFilename = outputFilename + '.zip'

                if os.path.exists(outputZipFilename):
                    print("File already exists: %s" % (outputZipFilename))
                    continue

                for field in fields:
                    field = field.strip()
                    inputDir = os.path.join(self.opt.inputDir, field)
                    inputFilename = os.path.join(inputDir, str(idate) + '.csv')
                    inputZipFilename = inputFilename + ".zip"

                    assert os.path.exists(inputZipFilename), "Unable to find inputFilename: %s" % (inputZipFilename)

                    # unzip the file first ... and then read it
                    PyUtil.unzipFile(inputZipFilename)

                    with open(inputFilename, 'r') as csvfile:
                        reader = csv.reader(csvfile, delimiter = '|')
                        numRows = 0

                        for row in reader:
                            numRows += 1

                            if numRows > 1 and len(row):
                                bbTicker    = row[0].strip()
                                figi        = row[1].strip()
                                cfigi       = row[2].strip()
                                isin        = row[3].strip()
                                value       = row[4].strip()

                                if figi and cfigi and value:
                                    if cfigi not in figiData:
                                        figiData[cfigi] = {
                                            "bbTicker": bbTicker,
                                            "figi": figi,
                                            "cfigi": cfigi,
                                            "isin": isin,
                                            "values": {}
                                        }

                                    figiData[cfigi]["values"][field] = value

                    # We've used up the file, now deleted the temporary zip that we created ...
                    os.remove(inputFilename)

                # Now we will write the data out
                with open(outputFilename, 'w') as f:
                    print("Writing: %s" % (outputFilename))

                    f.write("Ticker | FIGI | C-FIGI | ISIN | %s\n" % (" | ".join(fields)))

                    index = -1
                    for cfigi in figiData:
                        index += 1
                        data = figiData[cfigi]

                        f.write("%s | %s | %s | %s" % (data["bbTicker"], data["figi"], data["cfigi"], data["isin"]))

                        values = data["values"]

                        for field in fields:
                            field = field.strip()

                            if field in values:
                                f.write(" | %s" % (values[field]))
                            else:
                                f.write(" | ")

                        f.write("\n")

                PyUtil.zipFile(outputFilename, outputZipFilename)
                os.remove(outputFilename)

         
        except Exception as e:
            print(traceback.format_exc())

BBCombine().run()