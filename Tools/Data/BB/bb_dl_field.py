### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys
from optparse import OptionParser
import datetime as DT
import pandas as pd
from pandas.tseries.offsets import BDay, BMonthBegin

sys.path.append("../../../PyImpl/Scripts")
sys.path.append("../../../Python")
import PyUtil

import json
from shutil import copyfile
import zipfile
import csv
import math
import traceback

from bb_base import BBBase

import threading
from multiprocessing import Process

class BBDownloadField(BBBase):
    def __init__(self):
        super().__init__()

        self.opt                = None
        self.args               = None
        self.figis              = []
        self.figisLUT           = {}
        self.unmappedTickers    = []
        self.numThreads         = 4

    def parseArguments(self):
        parser = OptionParser()

        parser.add_option("-i", "--idmap", dest = "idmap", help = "Path to the id map downloaded from bb_dl_figis.py file")
        parser.add_option("-s", "--secMaster", dest = "secMaster", help = "The path to the sec master data")
        parser.add_option("-f", "--fields", dest = "fields", help = "Comma separated fields to download")
        parser.add_option("-F", "--factor", dest = "factor", default = "1.0", help = "The factor to multiply with")
        parser.add_option("-n", "--name", dest = "name", help = "The name of the ")
        parser.add_option("-o", "--output", dest = "outputDir", help = "The directory to output the data")
        parser.add_option("-O", "--onlyDownload", dest = "onlyDownload", action="store_true", default = False, help = "Only download and not write to individual files")
        parser.add_option("-S", "--start", dest = "startDate", help = "The starting date of the download")
        parser.add_option("-j", "--threads", dest = "numThreads", default = "4", help = "How many threads to use to write the data back")
        parser.add_option("-E", "--end", dest = "endDate", help = "The end date of the download")
            
        return parser.parse_args()

    def addTicker(self, ticker, figi, cfigi, isin):
        assert ticker and figi and cfigi, 'Invalid ticker being added. Ticker: %s, FIGI: %s, C-FIGI: %s' % (ticker, figi, cfigi)

        if figi not in self.figisLUT:
            tindex = len(self.figis)
            self.figis.append({
                "ticker": ticker,
                "figi": figi,
                "cfigi": cfigi,
                "isin": isin
            })
            self.figisLUT[figi] = tindex

    def run(self):
        self.opt, self.args = self.parseArguments()
        self.numThreads = int(self.opt.numThreads) if self.opt.numThreads else self.numThreads

        try:

            assert self.opt.idmap, "Must specify a valid idmap filename!"
            assert self.opt.outputDir, "Must specify a valid output!"
            assert self.opt.fields, "Must specify the fields to download!"
            # assert self.opt.name, "Must specify a valid output name!"

            startDate = DT.datetime.strptime(self.opt.startDate, "%Y%m%d")
            endDate = DT.datetime.strptime(self.opt.endDate, "%Y%m%d") if self.opt.endDate else DT.datetime.now()
            sstartDate = PyUtil.dateToStr(startDate)
            sendDate = PyUtil.dateToStr(endDate)

            dates = pd.date_range(startDate, endDate, freq = BDay())

            print("Date range: [%d, %d]" % (PyUtil.dateToInt(dates[0]), PyUtil.dateToInt(dates[-1])))

            if self.opt.secMaster:
                self.loadSecMaster(self.opt.secMaster)

            self.loadIdMap(self.opt.idmap)

            print("Total securities: %d" % (len(self.figis)))

            outputDir = self.opt.outputDir # os.path.join(self.opt.outputDir, name)

            # self.figis = [{
            #     "ticker": "GOOGL",
            #     "figi": "BBG009S4MT03",
            #     "cfigi": "BBG009S39JX6"
            # }]

            figisLUT = {}
            figis = []
            figiCount = len(self.figis)

            for i in range(figiCount):
                sec = self.figis[i]
                bbfigi = sec['figi'] + ' Equity'

                if bbfigi not in figisLUT:
                    figisLUT[bbfigi] = i
                    figis.append(bbfigi)

            if not os.path.exists(outputDir):
                os.makedirs(outputDir, exist_ok = True)

            self.factor = float(self.opt.factor)
            binFilename = os.path.join(outputDir, self.opt.fields + '.zip')

            if not os.path.exists(binFilename):
                print("Getting historical data for securities from BB terminal ...")

                import bbgApi
                t = bbgApi.LocalTerminal()
                data = t.get_historical(figis, flds=self.opt.fields, start=sstartDate, end=sendDate, 
                    ignore_security_error = 1, ignore_field_error = 1).as_frame()

                print("Got historical data, now writing!")

                # import pdb; pdb.set_trace()

                data.to_pickle(binFilename)
            else:
                print("Loading historical data from filename: %s" % (binFilename))
                data = pd.read_pickle(binFilename)

            if self.opt.onlyDownload is True:
                print("Only downloading and not writing. Exiting!")
                return

            maxCount = int(len(dates) / self.numThreads)
            dateIndex = 0
            self.threads = []

            if self.numThreads > 1:
                for i in range(self.numThreads - 1):
                    # thread = WriteThread(self.opt, self.figis, self.figisLUT, data, dates, dateIndex, dateIndex + maxCount)
                    thread = Process(target = self.saveData, args = (data, dates, dateIndex, dateIndex + maxCount,))
                    thread.start()
                    self.threads.append(thread)
                    dateIndex += maxCount

                # thread = WriteThread(self.opt, self.figis, self.figisLUT, data, dates, dateIndex, len(dates))
                thread = Process(target = self.saveData, args = (data, dates, dateIndex, len(dates),))
                thread.start()
                self.threads.append(thread)
            else:
                self.saveData(data, dates, 0, len(dates))

            print("Waiting for threads ...")

            for thread in self.threads:
                thread.join()

            print("All threads finished. Exiting!")
         
        except Exception as e:
            print(traceback.format_exc())

    def saveData(self, data, dates, startIndex, endIndex):
        outputDir = self.opt.outputDir # os.path.join(self.opt.outputDir, name)
        figiCount = len(self.figis)

        for i in range(startIndex, endIndex):
            date = dates[i]
            idate = PyUtil.dateToInt(date)
            sdate = PyUtil.dateToStr(date)

            print("Writing date: %s" % (sdate))

            dirname = os.path.join(outputDir, self.opt.fields)

            # if not os.path.exists(dirname):
            os.makedirs(dirname, exist_ok = True)

            filename = os.path.join(dirname, str(idate) + '.csv')
            zipFilename = filename + '.zip'

            if os.path.exists(zipFilename):
                print("Data already exists: %s" % (zipFilename))
                continue

            try:
                with open(filename, 'w') as f:
                    f.write('Ticker | FIGI | C-FIGI | ISIN | %s\n' % (self.opt.fields))

                    for i in range(figiCount):
                        sec = self.figis[i]
                        bbfigi = sec['figi'] + ' Equity'

                        if bbfigi in data:
                            figiData = data[bbfigi]

                            if self.opt.fields in figiData:
                                figiFieldData = figiData[self.opt.fields]

                                svalue = ''

                                if sdate in figiFieldData and not math.isnan(figiFieldData[sdate]):
                                    value = float(figiFieldData[sdate]) * self.factor
                                    f.write("%s | %s | %s | %s | %s\n" % (sec['ticker'], sec['figi'], sec['cfigi'], sec['isin'], str(value)))

                PyUtil.zipFile(filename, zipFilename)
                os.remove(filename)

            except Exception as e:
                print(traceback.format_exc())

# class WriteThread(threading.Thread):
#     def __init__(self, opt, figis, figisLUT, data, dates, startIndex, endIndex):
#         super().__init__()

#         self.opt            = opt
#         self.factor         = float(self.opt.factor)
#         self.data           = data
#         self.dates          = dates
#         self.startIndex     = startIndex
#         self.endIndex       = endIndex
#         self.figis          = figis
#         self.figisLUT       = figisLUT

#     def saveData(self, data, dates, startIndex, endIndex):
#         outputDir = self.opt.outputDir # os.path.join(self.opt.outputDir, name)
#         figiCount = len(self.figis)

#         for i in range(startIndex, endIndex):
#             date = dates[i]
#             idate = PyUtil.dateToInt(date)
#             sdate = PyUtil.dateToStr(date)

#             print("Writing date: %s" % (sdate))

#             dirname = os.path.join(outputDir, self.opt.fields)

#             if not os.path.exists(dirname):
#                 os.mkdir(dirname)

#             filename = os.path.join(dirname, str(idate) + '.csv')
#             zipFilename = filename + '.zip'

#             if os.path.exists(zipFilename):
#                 print("Data already exists: %s" % (zipFilename))
#                 continue

#             try:
#                 with open(filename, 'w') as f:
#                     f.write('Ticker | FIGI | C-FIGI | %s\n' % (self.opt.fields))

#                     for i in range(figiCount):
#                         sec = self.figis[i]
#                         bbfigi = sec['figi'] + ' Equity'

#                         if bbfigi in data:
#                             figiData = data[bbfigi]

#                             if self.opt.fields in figiData:
#                                 figiFieldData = figiData[self.opt.fields]

#                                 svalue = ''

#                                 if sdate in figiFieldData and not math.isnan(figiFieldData[sdate]):
#                                     value = float(figiFieldData[sdate]) * self.factor
#                                     f.write("%s | %s | %s | %s\n" % (sec['ticker'], sec['figi'], sec['cfigi'], str(value)))

#                 PyUtil.zipFile(filename, zipFilename)
#                 os.remove(filename)

#             except Exception as e:
#                 print(traceback.format_exc())

#     def run(self):
#         self.saveData(self.data, self.dates, self.startIndex, self.endIndex)
if __name__ == '__main__':
    BBDownloadField().run()
