### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys
from optparse import OptionParser
import datetime as DT
import pandas as pd
from pandas.tseries.offsets import BDay, BMonthBegin
import traceback

sys.path.append("../../../PyImpl/Scripts")
sys.path.append("../../../Python")
import PyUtil
import bbgApi

import json
from shutil import copyfile
import zipfile
import csv

def dateToInt(date):
    return int(str(date.year).zfill(4) + str(date.month).zfill(2) + str(date.day).zfill(2))

class BBDownloadFigis:
    def __init__(self):
        self.opt                = None
        self.args               = None
        self.tickers            = []
        self.tickersLUT         = {}
        self.unmappedTickers    = []
        self.secMasterData      = []
        self.secMasterLUT       = {}
        self.indices            = []

    def parseArguments(self):
        parser = OptionParser()

        parser.add_option("-i", "--index", dest = "index", help = "Path to the index directory")
        parser.add_option("-s", "--secMaster", dest = "secMaster", help = "The path to the sec master data")
        parser.add_option("-o", "--output", dest = "outputDir", help = "The directory to output the data")
        parser.add_option("-n", "--name", dest = "name", default="US_bb_id_map", help = "The name of the resultant CSV file")
        parser.add_option("-S", "--start", dest = "startDate", help = "The starting date of the download")
        parser.add_option("-m", "--market", dest = "market", default="US", help = "The market for the bb ticker")
        parser.add_option("-E", "--end", dest = "endDate", help = "The end date of the download")
            
        return parser.parse_args()

    def addTicker(self, ticker):
        if ticker not in self.tickersLUT:
            tindex = len(self.tickers)
            self.tickers.append(ticker)
            self.tickersLUT[ticker] = tindex

    def loadIndex(self, indexFilename):
        if not os.path.isfile(indexFilename):
            return

        print("Loading index: %s" % (indexFilename))

        indexName = os.path.basename(os.path.dirname(indexFilename))
        with open(indexFilename, 'r') as f:
            data = json.load(f)
            idata = data["INDX_MWEIGHT_HIST"][indexName + " Index"]["Index Member"]
            idataLen = len(idata)

            for i in range(idataLen):
                ticker = idata[str(i)]
                self.addTicker(ticker)


    def loadSecMaster(self, filename):
        secMasterFilename = filename + ".zip"
        assert os.path.isfile(secMasterFilename), "Invalid sec master filename that doesn't exist: %s" % (secMasterFilename)

        print("Loading sec master: %s" % (secMasterFilename))

        with zipfile.ZipFile(secMasterFilename, 'r') as zipHandle:
            name = os.path.basename(filename)
            fileContents = zipHandle.read(name,) 
            dataStr = fileContents.decode('utf-8')
            reader = csv.reader(dataStr.split('\n'), delimiter = '|')
            rowCount = 0

            for row in reader:
                rowCount += 1

                if rowCount == 1 or len(row) is 0:
                    continue

                sec = {
                    "ticker"    : row[2].strip(),
                    "figi"      : row[9].strip(),
                    "cfigi"     : row[10].strip()
                }

                index           = len(self.secMasterData)
                self.secMasterData.append(sec)

                self.secMasterLUT[sec["ticker"]] = index

                # self.addTicker(sec['ticker'])

                if sec["figi"]:
                    self.secMasterLUT[sec["figi"]]  = index
                    # self.addTicker(sec["figi"])

                if sec["cfigi"]:
                    self.secMasterLUT[sec["cfigi"]] = index

    def loadIndices(self, dates):      
        assert self.opt.index, "Must specify a valid index!"

        indexParts = self.opt.index.split("|")
        assert len(indexParts) == 2, "Invalid index name: %s" % (self.opt.index)

        assert self.opt.market, "Invalid market specified!"

        basePath = indexParts[0].strip()
        indexNames = indexParts[1].split(',')

        for indexName in indexNames:
            indexFilename = os.path.join(basePath, indexName.strip())
            self.indices.append(indexFilename)

        for index in self.indices:
            for date in dates:
                idate = dateToInt(date)

                indexFilename = os.path.join(index, str(idate) + '.json')
                self.loadIndex(indexFilename)


    def run(self):
        self.opt, self.args = self.parseArguments()

        assert self.opt.outputDir, "Must specify a valid output!"

        startDate = DT.datetime.strptime(self.opt.startDate, "%Y%m%d")
        endDate = DT.datetime.strptime(self.opt.endDate, "%Y%m%d") if self.opt.endDate else DT.datetime.now()

        dates = pd.date_range(startDate, endDate, freq = BDay())

        print("Date range: [%d, %d]" % (dateToInt(dates[0]), dateToInt(dates[-1])))

        # self.loadSecMaster(self.opt.secMaster)
        self.loadIndices(dates)

        print("Total securities: %d" % (len(self.secMasterData)))
        print("Total BB tickers: %d" % (len(self.tickers)))

        filename = os.path.join(self.opt.outputDir, self.opt.name + '.csv')

        tickerData = []
        t = bbgApi.LocalTerminal()

        with open(filename, 'w') as f:
            f.write('Ticker | BB-Ticker | Name | Market Status | Country | FIGI | C-FIGI | Parent FIGI | ISIN | Sector | Industry | Sub-Industry | Group\n')

            numTickers = len(self.tickers)
            tickerIndex = 0

            for ticker in self.tickers:
                tickerIndex += 1

                if self.opt.market == 'US':
                    ticker = ticker.split(' ')[0] + ' US'
                else:
                    if ' ' not in ticker:
                        continue

                orgTicker = ticker
                # if ' ' not in ticker and len(ticker) < 8:
                #     ticker = ticker + ' US'

                ticker = ticker + " Equity"
                print('[%d / %d] Ticker: %s' % (tickerIndex, numTickers, ticker))

                try:
                    # refData = t.get_reference_data(ticker, ['ID_ISIN']).as_frame()
                    #     # 'GICS_SECTOR_NAME', 'ISSUER_INDUSTRY', 'INDUSTRY_SUBGROUP', 'INDUSTRY_GROUP']).as_frame()
                    # td = {
                    #     "ticker": ticker,
                    #     "orgTicker": orgTicker,
                    #     "isin": refData['ID_ISIN'][ticker],
                    # }

                    # f.write("%s | %s | %s\n" % (td['orgTicker'], td['ticker'], td['isin']))

                    refData = t.get_reference_data(ticker, ['NAME', 'MARKET_STATUS', 'COUNTRY', 'ID_BB_GLOBAL', 
                        'COMPOSITE_ID_BB_GLOBAL', 'ID_BB_GLOBAL_COMPANY', 'ID_ISIN'], 
                        ignore_security_error = 1, ignore_field_error = 1).as_frame()

                    td = {
                        "ticker": ticker,
                        "orgTicker": orgTicker,
                        "name": refData['NAME'][ticker],
                        "marketStatus": refData['MARKET_STATUS'][ticker],
                        "country": refData['COUNTRY'][ticker],
                        "figi": refData['ID_BB_GLOBAL'][ticker],
                        "cfigi": refData['COMPOSITE_ID_BB_GLOBAL'][ticker],
                        "isin": refData['ID_ISIN'][ticker],
                        "pfigi": refData['ID_BB_GLOBAL_COMPANY'][ticker],
                    }

                    f.write("%s | %s | %s | %s | %s | %s | %s | %s | %s | " % (td['orgTicker'], td['ticker'], td['name'], td['marketStatus'], td['country'], td['figi'], td['cfigi'], td['pfigi'], td['isin']))

                    try:
                        secData = t.get_reference_data(ticker, ['INDUSTRY_SECTOR', 'ISSUER_INDUSTRY', 'INDUSTRY_SUBGROUP', 'INDUSTRY_GROUP', 
                            'DT_OF_LAST_TRADING_SUSPENSION', 'EQY_INIT_PO_DT', 'TICKER', 'GICS_SECTOR', 'GICS_INDUSTRY_GROUP', 
                            'GICS_INDUSTRY', 'GICS_SUB_INDUSTRY'], 
                            ignore_security_error = 1, ignore_field_error = 1).as_frame()

                        sd = {
                            'sector': secData['INDUSTRY_SECTOR'][ticker],
                            'industry': secData['ISSUER_INDUSTRY'][ticker],
                            'subIndustry': secData['INDUSTRY_SUBGROUP'][ticker],
                            'group': secData['INDUSTRY_GROUP'][ticker],
                        }

                        f.write("%s | %s | %s | %s\n" % (sd['sector'], sd['industry'], sd['subIndustry'], sd['group']))
                    except Exception as e:
                        f.write(" |  |  | \n")
                        pass

                    print('    %s => %s' % (ticker, td['isin']))

                except Exception as e:
                    print(traceback.format_exc())

                    # pass
            # tickersToGet.append(ticker)

            # for td in tickerData:
            #     f.write("%s | %s | %s | %s\n" % (td['orgTicker'], td['ticker'], td['figi'], td['cfigi'], td['pfigi']))

BBDownloadFigis().run()
