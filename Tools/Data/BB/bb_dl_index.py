### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys
from optparse import OptionParser
import datetime as DT
import pandas as pd
from pandas.tseries.offsets import BDay, BMonthBegin

sys.path.append("../../../PyImpl/Scripts")
sys.path.append("../../../Python")
import bbgApi

def parseArguments():
    parser = OptionParser()

    parser.add_option("-i", "--index", dest = "index", help = "The name of the index to download")
    parser.add_option("-o", "--output", dest = "outputDir", help = "The directory to output the data")
    parser.add_option("-S", "--start", dest = "startDate", help = "The starting date of the download")
    parser.add_option("-E", "--end", dest = "endDate", help = "The end date of the download")
    parser.add_option("-m", "--monthly", dest = "monthly", action="store_true", default = False, help = "Download data only monthly")
    parser.add_option("-w", "--weekly", dest = "weekly", action="store_true", default = False, help = "Download data only weekly")
    # parser.add_option("-w", "--weekly", dest = "weekly", action="store_true", default = False, help = "Download data only weekly")
        
    return parser.parse_args()

def dateToInt(date):
    return int(str(date.year).zfill(4) + str(date.month).zfill(2) + str(date.day).zfill(2))

def runForIndex(t, index, odir, dates):
    print("Index: %s" % (index))
    outputDir = os.path.join(odir, index)
    try:
        os.mkdir(outputDir)
    except:
        # directory already exists ... which is fine
        pass

    # if len(dates) == 1:
    for date in dates:
        try:
            idate = dateToInt(date)
            print("[%s] Date: %d" % (index, idate))
            fname = os.path.join(outputDir, str(idate) + ".json")

            if os.path.isfile(fname):
                print("[%s] File exists: %s" % (index, fname))
                continue

            data = t.get_reference_data(index + ' Index', 'INDX_MWEIGHT_HIST', END_DT=str(idate)).as_frame()
            data.to_json(fname) 
        except Exception as e:
            print(e)
    # else:
    #     startDate = dateToInt(dates[0])
    #     endDate = dateToInt(dates[-1])
    #     print("[%s] Getting in date range: %d - %d" % (index, startDate, endDate))
    #     data = t.get_historical(index + ' Index', 'INDX_MWEIGHT_HIST', start=str(startDate), end=str(endDate)).as_frame()
    #     print(data)
    #     # fname = os.path.join(outputDir, index + "_get_historical.json")
    #     # data.to_json(fname) 

def run():
    opt, args = parseArguments()

    assert opt.index, "Must specify a valid index!"
    assert opt.outputDir, "Must specify a valid output!"

    startDate = DT.datetime.strptime(opt.startDate, "%Y%m%d")
    endDate = DT.datetime.strptime(opt.endDate, "%Y%m%d") if opt.endDate else DT.datetime.now()

    freq = BDay()
    if opt.monthly:
        freq = BMonthBegin()

    dates = pd.date_range(startDate, endDate, freq = freq)

    indices = opt.index.split(',')

    print("Indices    : %s" % (indices))
    print("Output Dir : %s" % (opt.outputDir))
    print("Start Date : %s" % (startDate))
    print("End Date   : %s" % (endDate))
    print("BDay Count : %d" % (len(dates)))

    print("Creating LocalTerminal ...")
    t = bbgApi.LocalTerminal()

    for index in indices:
        runForIndex(t, index, opt.outputDir, dates)

run()
