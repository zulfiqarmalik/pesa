### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys
from optparse import OptionParser
import datetime as DT
import pandas as pd
from pandas.tseries.offsets import BDay, BMonthBegin

sys.path.append("../../../PyImpl/Scripts")
sys.path.append("../../../Python")
import PyUtil

import json
from shutil import copyfile

from bb_base import BBBase

def dateToInt(date):
    return int(str(date.year).zfill(4) + str(date.month).zfill(2) + str(date.day).zfill(2))

class BBFixIndices(BBBase):
    def __init__(self):
        super().__init__()

        self.lastSrcFilename    = None
        self.lastDstFilename    = None
        self.opt                = None
        self.args               = None

        self.tickers            = []
        self.tickersLUT         = {}

    def parseArguments(self):
        parser = OptionParser()

        parser.add_option("-i", "--idmap", dest = "idmap", help = "Path to the id map downloaded from [MARKET]_bb_dl_figis.py file")
        parser.add_option("-I", "--index", dest = "index", help = "Comma separated names to the indices that needs to be fixed")
        parser.add_option("-p", "--path", dest = "path", help = "Path to the base directory that has the indices")
        parser.add_option("-o", "--output", dest = "outputDir", help = "The directory to output the data")
        parser.add_option("-S", "--start", dest = "startDate", help = "The starting date of the download")
        parser.add_option("-E", "--end", dest = "endDate", help = "The end date of the download")
            
        return parser.parse_args()

    def addTicker(self, ticker, figi, cfigi, isin):
        assert ticker and figi and cfigi, 'Invalid ticker being added. Ticker: %s, FIGI: %s, C-FIGI: %s' % (ticker, figi, cfigi)

        if ticker not in self.tickersLUT:
            tindex = len(self.tickers)
            self.tickers.append({
                "ticker": ticker,
                "figi": figi,
                "cfigi": cfigi,
                "isin": isin
            })
            self.tickersLUT[ticker] = tindex

    def translate(self, idate, srcFilename, dstFilename):
        if not os.path.isfile(srcFilename):
            if self.lastDstFilename:
                print("Copying %s => %s" % (self.lastDstFilename, dstFilename))
                # copyfile(self.lastDstFilename + ".zip", dstFilename + ".zip")
                PyUtil.unzipFile(self.lastDstFilename + '.zip')
                os.rename(self.lastDstFilename, dstFilename)
                PyUtil.zipFile(dstFilename, dstFilename + '.zip')
                os.remove(dstFilename)
            return

        if os.path.isfile(dstFilename + ".zip"):
            self.lastSrcFilename = srcFilename
            self.lastDstFilename = dstFilename
            return

        directory = os.path.dirname(dstFilename)
        if not os.path.exists(directory):
            os.makedirs(directory)

        indexName = os.path.basename(os.path.dirname(srcFilename))

        with open(srcFilename, 'r') as f:
            data = json.load(f)
            newData = {
                "results": []
            }
            idata = data["INDX_MWEIGHT_HIST"][indexName + " Index"]["Index Member"]
            idataLen = len(idata)

            for i in range(idataLen):
                ticker = idata[str(i)] #.split(' ')[0]

                if ticker in self.tickersLUT:
                    index = self.tickersLUT[ticker]

                    cfigi = self.tickers[index]["cfigi"]
                    assert cfigi, "Invalid cfigi for ticker: %s" % (ticker)

                    newData["results"].append({
                        "ticker": self.tickers[index]["ticker"].split(' ')[0],
                        "figi": self.tickers[index]["figi"],
                        "cfigi": self.tickers[index]["cfigi"],
                        "isin": self.tickers[index]["isin"]
                    })
                # newData["results"].append(str.format("%s.%s" % (self.opt.id, ticker.split(' ')[0])))

            print("Translating %s => %s" % (srcFilename, dstFilename))

            with open(dstFilename, 'w') as o:
                json.dump(newData, o)

            # now we zip up the file
            PyUtil.zipFile(dstFilename, dstFilename + ".zip")
            os.remove(dstFilename)

        self.lastSrcFilename = srcFilename
        self.lastDstFilename = dstFilename

    def run(self):
        self.opt, self.args = self.parseArguments()

        assert self.opt.index, "Must specify a valid index!"
        assert self.opt.path, "Must specify a valid base path to where the indices are!"
        assert self.opt.outputDir, "Must specify a valid output!"

        startDate = DT.datetime.strptime(self.opt.startDate, "%Y%m%d")
        endDate = DT.datetime.strptime(self.opt.endDate, "%Y%m%d") if self.opt.endDate else DT.datetime.now()

        dates = pd.date_range(startDate, endDate, freq = BDay())

        self.loadIdMap(self.opt.idmap)

        print("Date range: [%d, %d]" % (dateToInt(dates[0]), dateToInt(dates[-1])))

        indices = self.opt.index.split(',')

        for index in indices:
            index = index.strip()

            indexBasePath = os.path.join(self.opt.path, index)
            print("Handling index: %s [Base path: %s]" % (index, indexBasePath))

            indexOutputDir = os.path.join(self.opt.outputDir, index)

            for date in dates:
                idate = dateToInt(date)
                srcFilename = os.path.join(indexBasePath, str(idate) + '.json')
                dstFilename = os.path.join(indexOutputDir, str(idate) + '.json')

                self.translate(idate, srcFilename, dstFilename)

BBFixIndices().run()
