### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys
from optparse import OptionParser
import datetime as DT
import pandas as pd
from pandas.tseries.offsets import BDay, BMonthBegin

sys.path.append("../../../PyImpl/Scripts")
sys.path.append("../../../Python")
import PyUtil

import json
from shutil import copyfile
import zipfile
import csv
import math
import traceback

from bb_base import BBBase

import threading
from multiprocessing import Process

class BBFixTicker(BBBase):
    def __init__(self):
        super().__init__()

        self.opt                = None
        self.args               = None

    def parseArguments(self):
        parser = OptionParser()

        parser.add_option("-i", "--input", dest = "input", help = "The input to the filename")
        parser.add_option("-o", "--output", dest = "output", help = "The output filename")
            
        return parser.parse_args()

    def run(self):
        self.opt, self.args = self.parseArguments()

        assert self.opt.input, "Invalid input filename"
        assert self.opt.output, "Invalid output filename"

        with open(self.opt.output, 'w') as out:
            with open(self.opt.input, 'r') as f:
                reader = csv.reader(f, delimiter = '|')
                rowCount = 0

                for row in reader:
                    rowCount += 1

                    if rowCount > 1 and len(row):
                        row[0] = row[0].split(' ')[0] + ' US '
                        row[1] = ' ' + row[0] + 'Equity '

                    out.write('%s\n' % ('|'.join(row)))



if __name__ == '__main__':
    BBFixTicker().run()
