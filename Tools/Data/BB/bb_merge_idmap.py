### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys
from optparse import OptionParser
import datetime as DT
import pandas as pd
from pandas.tseries.offsets import BDay, BMonthBegin

sys.path.append("../../../PyImpl/Scripts")
sys.path.append("../../../Python")
import PyUtil
import bbgApi

import json
from shutil import copyfile
import zipfile
import csv
import math
import traceback
import csv

figiFilename = 'X:/devwork/data/Vendors/Bloomberg/_US_bb_id_map.csv'
isinFilename = 'X:/devwork/data/Vendors/Bloomberg/isin_map.csv'
outputFilename = 'X:/devwork/data/Vendors/Bloomberg/US_bb_id_map.csv'

figis = []
figiMap = {}

with open(figiFilename, 'r') as f:
    reader = csv.reader(f, delimiter = '|')
    numLines = 0

    for row in reader:
        numLines += 1

        if len(row) > 0 and numLines > 1:
            figiData = {
                "orgTicker": row[0].strip(),
                "ticker": row[1].strip(),
                "name": row[2].strip(),
                "marketStatus": row[3].strip(),
                "country": row[4].strip(),
                "figi": row[5].strip(),
                "cfigi": row[6].strip(),
                "pfigi": row[7].strip(),
                "sector": row[8].strip(),
                "industry": row[9].strip(),
                "subIndustry": row[10].strip(),
                "group": row[11].strip(),
                "isin": ""
            }

            figis.append(figiData)
            index = len(figis) - 1

            figiMap[figiData["orgTicker"]] = index
            figiMap[figiData["ticker"]] = index

with open(isinFilename, 'r') as f:
    reader = csv.reader(f, delimiter = '|')
    numLines = 0

    for row in reader:
        numLines += 1

        if len(row) > 0 and numLines > 1:
            orgTicker = row[0].strip()
            ticker = row[1].strip()
            index = -1

            if orgTicker in figiMap:
                index = figiMap[orgTicker]

            if ticker in figiMap and index < 0:
                index = figiMap[ticker]

            if index >= 0:
                figis[index]['isin'] = row[2].strip()

with open(outputFilename, 'w') as f:
    f.write('Ticker | BB-Ticker | Name | Market Status | Country | FIGI | C-FIGI | Parent FIGI | ISIN | Sector | Industry | Sub-Industry | Group\n')

    for td in figis:
        f.write("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n" % (td['orgTicker'], td['ticker'], td['name'], td['marketStatus'], td['country'], td['figi'], td['cfigi'], 
            td['pfigi'], td['isin'], td['sector'], td['industry'], td['subIndustry'], td['group']))
