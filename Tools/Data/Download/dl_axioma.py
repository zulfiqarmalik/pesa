### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys
from optparse import OptionParser
import datetime as DT
import pandas as pd

from dl_common import Downloader, Args

class AxiomaArgs(Args):
    def __init__(self, opt, args):
        super().__init__(opt, args)

class Axioma(Downloader):
    def __init__(self):
        super().__init__(ArgsClass = AxiomaArgs)

    def download(self):
        print("[Axioma] Downloading date range: [%d, %d]" % (self.args.istartDate, self.args.iendDate))

        self.connectFTP()
        self.downloadAllFTP()
        self.disconnectFTP()

    def initArgsParser(self):
        parser = super().initArgsParser()

        # parser.add_option("-m", "--model", dest = "model", default = "AXNA21-MH", help = "The directory to output the data")

        return parser

Axioma().download()
