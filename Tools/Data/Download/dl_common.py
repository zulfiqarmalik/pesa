### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys, traceback
from optparse import OptionParser
import datetime as DT
import pandas as pd
from pandas.tseries.offsets import BDay
import pathlib

sys.path.append("../../../PyImpl/Scripts")
sys.path.append("../../../Python")
import PyUtil

import signal

def signal_handler(sig, frame):
    print('You pressed Ctrl+C!')
    sys.exit(0)
        
signal.signal(signal.SIGINT, signal_handler)

def dateToInt(date):
    return int(str(date.year).zfill(4) + str(date.month).zfill(2) + str(date.day).zfill(2))

class Args:
    def __init__(self, opt, args):
        assert opt.output, "Must specify a valid output directory!"

        self.args               = args
        self.output             = opt.output
        self.username           = opt.username
        self.password           = opt.password
        self.server             = opt.server
        self.dateList           = opt.dateList

        self.startDate          = DT.datetime.strptime(opt.startDate, "%Y%m%d") if opt.startDate else DT.datetime.now()
        self.endDate            = DT.datetime.strptime(opt.endDate, "%Y%m%d") if opt.endDate else DT.datetime.now()
        self.istartDate         = PyUtil.dateToInt(self.startDate) 
        self.iendDate           = PyUtil.dateToInt(self.endDate)
        self.dates              = []
        self.idates             = []
        self.format             = opt.format
        self.workingDir         = opt.workingDir.replace("\\", "/") if opt.workingDir is not None else None
        self.split              = opt.split
        self.error              = opt.error

        self.numThreads         = int(opt.numThreads) if opt.numThreads else 1

        self.market             = opt.market

        self.buildDates()

    def buildDefaultDates(self):
        self.dates              = pd.date_range(self.startDate, self.endDate, freq = BDay()) 

        for date in self.dates:
            self.idates.append(PyUtil.dateToInt(date))

    def buildDateList(self):
        print("Loading dates from file: %s" % (self.dateList))
        with open(self.dateList) as f:
            lines = f.readlines()

            for line in lines:
                line = line.strip()
                if line:
                    idate = int(line)

                    if idate >= self.istartDate and idate <= self.iendDate:
                        self.idates.append(idate)
                        date = DT.datetime.strptime(str(idate), "%Y%m%d")
                        self.dates.append(date)


    def buildDates(self):
        if self.dateList:
            return self.buildDateList();

        return self.buildDefaultDates();

class Downloader:
    def __init__(self, ArgsClass = Args):
        opt, args               = self.parseArguments()
        self.args               = ArgsClass(opt, args)

        # Different kinds of server implementations
        self.ftp                = None
        self.ftpFileList        = None
        self.ftpFileLookup      = {}
        self.minDaysToUpdate    = 5


    def initArgsParser(self):
        parser                  = OptionParser()

        parser.add_option("-d", "--dateList", dest = "dateList", help = "A text file containing the list of simulation dates")
        parser.add_option("-o", "--output", dest = "output", help = "The directory to output the data")
        parser.add_option("-S", "--start", dest = "startDate", default = None, help = "The starting date of the download")
        parser.add_option("-e", "--error", dest = "error", action="store_true", default = False, help = "Error and exit if some of the critical bits fail")
        parser.add_option("-E", "--end", dest = "endDate", default = None, help = "The end date of the download")
        parser.add_option("-f", "--format", dest = "format", help = "The file format on the server")
        parser.add_option("-p", "--password", dest = "password", help = "The password")
        parser.add_option("-P", "--split", dest = "split", action="store_true", default = False, help = "Split the input zip file into many parts")
        parser.add_option("-s", "--server", dest = "server", help = "Server path/url")
        parser.add_option("-t", "--threads", dest = "numThreads", default = "1", help = "How many threads to use for downloading")
        parser.add_option("-u", "--username", dest = "username", help = "Username")
        parser.add_option("-w", "--workingDir", dest = "workingDir", default = None, help = "Working directory in the server")
        parser.add_option("-m", "--market", dest = "market", default = None, help = "The target market for the run")

        return parser

    def formatFilename(self, formatStr, date):
        return formatStr.replace("$YYYY", str(date.year).zfill(4)).replace("$MM", str(date.month).zfill(2)).replace("$DD", str(date.day).zfill(2))

    def parseArguments(self):
        parser                  = self.initArgsParser()
        return parser.parse_args()

    def connectFTP(self):
        try:
            import ftplib
            assert self.args.server, "Cannot do an FTP login without a server. Specify it using the -s option"
            assert self.args.username, "Cannot do an FTP login without a username. Specify it using the -u option"
            assert self.args.password, "Cannot do an FTP login without a password. Specify it using the -p option"

            print("[FTP] Logging into FTP Server: %s [%s/%s]" % (self.args.server, self.args.username, self.args.password))

            self.ftp                = ftplib.FTP(self.args.server)
            self.ftp.login(self.args.username, self.args.password)

            print("[FTP] Logged in!")

            if self.args.workingDir:
                print("[FTP] Changing working dir to: %s" % (self.args.workingDir))
                self.ftp.cwd(self.args.workingDir)

        except Exception as e:
            self.disconnectFTP()
            print(traceback.format_exc())

    def disconnectFTP(self):
        print("[FTP] Disconnecting!")

        if self.ftp:
            self.ftp.quit()
            self.ftp = None

    def downloadAllFTP(self):
        try:
            for di in range(len(self.args.dates)):
                date = self.args.dates[di]
                self.downloadFTP(di, date)
        except Exception as e:
            self.disconnectFTP()
            print(traceback.format_exc())

    def splitZipfile(self, filename, delete = True):
        import zipfile
        outputDir = os.path.dirname(filename)

        with zipfile.ZipFile(filename, 'r') as zipHandle:
            names = zipHandle.namelist()
            print("[DL] Splitting: %s => %s" % (filename, str(names)))

            for name in names:
                print("[DL] Handling: %s@%s" % (filename, name))
                fileContents = zipHandle.read(name,) 

                dstFilename = os.path.join(outputDir, name)

                with open(dstFilename, 'w') as file:
                    file.write(fileContents.decode("utf-8"))

                zipFilename = dstFilename + '.zip'
                PyUtil.zipFile(dstFilename, zipFilename)
                os.remove(dstFilename)

                print("[DL] Extracted filename: %s@%s => %s" % (filename, name, zipFilename))

        if delete is True:
            print("[DL] Deleting filename: %s" % (filename))
            os.remove(filename)

    def checkExistsFTP(self, filename):
        if self.ftpFileList is None:
            self.ftpFileList = self.ftp.nlst()
            for f in self.ftpFileList:
                self.ftpFileLookup[f] = True

        return True if filename in self.ftpFileLookup else False

    def shouldForceDownload(self, di):
        return True if len(self.args.dates) - di <= self.minDaysToUpdate else False

    def downloadFTP(self, di, date):
        try:
            forceDownload = self.shouldForceDownload(di)
            inputFilename = self.formatFilename(self.args.format, date)
            outputDir = self.formatFilename(self.args.output, date)

            # print("[FTP] Ensuring output directory exists: %s" % (outputDir))

            pathlib.Path(outputDir).mkdir(parents = True, exist_ok = True)

            outputFilename = os.path.join(outputDir, inputFilename)
            (fname, ext) = os.path.splitext(inputFilename)

            if forceDownload is True or os.path.isfile(outputFilename) is False:

                # If we are going to force download something then we should not do a glob check
                if self.args.split is True and forceDownload is False:
                    import glob
                    searchStr = os.path.join(outputDir, fname + ".*" + ext)
                    res = glob.glob(searchStr)
                    if len(res):
                        print("[FTP] Already handled: %s" % (inputFilename))
                        return

                if self.checkExistsFTP(inputFilename) is False:
                    if forceDownload and self.args.error:
                        print("Fatal Error: Unable to find file on the server: %s" % (inputFilename))
                        import sys
                        sys.exit(1)

                    print("[FTP] No file on the server. Ignoring: %s" % (inputFilename))
                    return
                    
                print("[FTP] Downloading file: %s => %s" % (inputFilename, outputFilename))
                self.ftp.retrbinary("RETR " + inputFilename, open(outputFilename, 'wb').write)
            else:
                print("[FTP] File already exists: %s" % (outputFilename))

            if self.args.split is True and outputFilename.endswith(".zip"):
                self.splitZipfile(outputFilename, delete = True)
                
        except Exception as e:
            print(traceback.format_exc())
