### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys
from optparse import OptionParser
import datetime as DT
import pandas as pd
import pathlib

import subprocess
from subprocess import call

sys.path.append("../../../PyImpl/Scripts")
sys.path.append("../../../Python")
import PyUtil

from dl_common import Downloader, Args

class DukaArgs(Args):
    def __init__(self, opt, args):
        super().__init__(opt, args)

        self.currencies = opt.currencies.split(',')
        self.bulk       = opt.bulk

class Duka(Downloader):
    def __init__(self):
        super().__init__(ArgsClass = DukaArgs)

    def initArgsParser(self):
        parser = super().initArgsParser()

        parser.add_option("-B", "--bulk", dest = "bulk", action="store_true", default = False, help = "Do a bulk file download or not")
        parser.add_option("-C", "--currencies", dest = "currencies", help = "The currencies to download")

        return parser


    def download(self):
        print("[Duka] Downloading date range: [%d, %d]" % (self.args.istartDate, self.args.iendDate))

        self.downloadAll()

    def downloadAll(self):
        try:
            for currency in self.args.currencies:
                if self.args.bulk is False:
                    for di in range(len(self.args.idates)):
                        idate = self.args.idates[di]
                        self.downloadDate(currency.strip(), di, idate)
                else:
                    self.downloadDate(currency.strip(), 0, self.args.istartDate, self.args.bulk)

        except Exception as e:
            import traceback
            print(traceback.format_exc())
            sys.exit(1)

    def downloadDate(self, currency, di, idate, bulk = False):
        sdate = PyUtil.intDateToStrDate(idate)
        sdate2 = PyUtil.intDateToStrDate(idate, sep = '_')
        outputDir = os.path.join(self.args.output, currency)

        pathlib.Path(outputDir).mkdir(parents = True, exist_ok = True)
        filename = str.format("%s-%s-%s.csv" % (currency, sdate2, sdate2))
        filename = os.path.join(outputDir, filename)
        zipFilename = filename + ".zip"

        if os.path.isfile(zipFilename):
            print("[Duka] File already exists: %s" % (zipFilename))
            return

        cmd = ['duka', currency, '-s' if bulk is True else '-d', sdate, '-f', outputDir, '-c', 'M30', '--header']
        print("[Duka] Running command: %s" % (str(cmd)))

        process = subprocess.Popen(cmd, stdout = subprocess.PIPE, stderr = subprocess.PIPE)

        while process.poll() is None:
            for line in process.stdout:
                print(line.decode('utf-8').rstrip())

            for line in process.stderr:
                print(line.decode('utf-8').rstrip())

            continue

            print("Process returned: %d" % (process.returncode))
            assert process.returncode == 0, "[Duka - FATAL ERROR] Process returned: %d" % (process.returncode)

        PyUtil.zipFile(filename, zipFilename)
        os.remove(filename)

Duka().download()
