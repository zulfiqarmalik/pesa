### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys
from optparse import OptionParser
import datetime as DT
import pandas as pd
import pathlib

import subprocess
from subprocess import call, STDOUT, check_output

sys.path.append("../../../PyImpl/Scripts")
sys.path.append("../../../Python")
import PyUtil

import csv
from multiprocessing import Process

from dl_common import Downloader, Args

class DukaHistoryArgs(Args):
    def __init__(self, opt, args):
        super().__init__(opt, args)

        self.currencies = opt.currencies.split(',')
        self.bulk = opt.bulk
        self.intermediate = opt.intermediate

class DukaHistory(Downloader):
    def __init__(self):
        super().__init__(ArgsClass = DukaHistoryArgs)

    def initArgsParser(self):
        parser = super().initArgsParser()

        parser.add_option("-B", "--bulk", dest = "bulk", action="store_true", default = False, help = "Do a bulk file download or not")
        parser.add_option("-i", "--intermediate", dest = "intermediate", help = "The intermediate directory where the DukaScopy data will be downloaded")
        parser.add_option("-C", "--currencies", dest = "currencies", help = "The currencies to download")

        return parser


    def download(self):
        print("[DukaHistory] Downloading date range: [%d, %d]" % (self.args.istartDate, self.args.iendDate))

        self.downloadAll()

    def downloadAll(self):
        try:
            # for currency in self.args.currencies:
            threads = []

            for currency in self.args.currencies:
                if self.args.numThreads > 1:
                    thread = Process(target = self.downloadAllDates, args = (currency,))
                    thread.start()
                    threads.append(thread)
                else:
                    self.downloadAllDates(currency)

            for thread in threads:
                thread.join()

        except Exception as e:
            import traceback
            print(traceback.format_exc())
            sys.exit(1)

    def downloadAllDates(self, currency):
        for di in range(len(self.args.dates)):
            date = self.args.dates[di]
            self.downloadDate(currency.strip(), di, date)


    def splitFile(self, inputFilename, filename):
        with open(filename, 'w') as out:
            out.write("date,price\n")

            with open(inputFilename, "r") as f:
                reader = csv.reader(f, delimiter = ",")
                numRows = 0

                for row in reader:
                    numRows += 1

                    if len(row) and numRows > 1:
                        out.write("%s,%s\n" % (row[0], row[2]))

        # finally, we write to the zip file and delete the original input file
        # along with the intermediate file that we created!
        zipFilename = filename + ".zip"
        PyUtil.zipFile(filename, zipFilename)
        os.remove(filename)

    def downloadDate(self, currency, di, date, bulk = False):
        idate = PyUtil.dateToInt(date)
        sdate = PyUtil.intDateToStrDate(idate)
        sdate2 = PyUtil.intDateToStrDate(idate, sep = '_')

        # This is where the Duka data will be downloaded
        intermediateDir = os.path.join(self.args.intermediate, currency).replace("$CURRENCY", currency)

        pathlib.Path(intermediateDir).mkdir(parents = True, exist_ok = True)
        inputFilename = str.format("%s-%s-%s.csv" % (currency, sdate2, sdate2))
        inputFilename = os.path.join(intermediateDir, inputFilename)
        inputZipFilename = inputFilename + ".zip"

        outputDir = self.formatFilename(self.args.output, date).replace("$CURRENCY", currency)
        outputDir = os.path.dirname(outputDir)

        outputFilename = os.path.join(outputDir, str.format("%s-%s.csv" % (currency, sdate2)))
        zipFilename = outputFilename + ".zip"

        os.makedirs(outputDir, exist_ok = True)

        # If the output file already exists then we don't need to download it anymore ...
        if os.path.isfile(zipFilename):
            print("[DukaHistory] File already exists: %s" % (zipFilename))
            return

        # Download the data from Dukascopy
        if not os.path.isfile(inputZipFilename):
            cmd = ['duka', currency, '-d', sdate, '-f', intermediateDir, '-c', 'M5', '--header']
            print("[DukaHistory] [%d] Command: %s" % (idate, ' '.join(cmd)))

            process = subprocess.Popen(cmd, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
            output = check_output(cmd, stderr = STDOUT, timeout = 15)

            # while process.poll() is None:
            #     for line in process.stdout:
            #         print(line.decode('utf-8').rstrip())
     
            #     for line in process.stderr:
            #         print(line.decode('utf-8').rstrip())

            #     continue

            #     print("Process returned: %d" % (process.returncode))
            #     assert process.returncode == 0, "[DukaHistory - FATAL ERROR] Process returned: %d" % (process.returncode)

            # Now just zip up the file ...
            PyUtil.zipFile(inputFilename, inputZipFilename)
        else:
            print("[DukaHistory] [%d] Using existing downloaded file: %s" % (idate, inputZipFilename))
            PyUtil.unzipFile(inputZipFilename)

        self.splitFile(inputFilename, outputFilename)

        os.remove(inputFilename)
        print("[DukaHistory] [%d] Done: %s" % (idate, currency))

if __name__ == '__main__':
    DukaHistory().download()
