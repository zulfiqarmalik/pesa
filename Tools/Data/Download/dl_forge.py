### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys
from optparse import OptionParser
import datetime as DT
import pandas as pd
import pathlib

import subprocess
from subprocess import call

sys.path.append("../../../PyImpl/Scripts")
sys.path.append("../../../Python")
import PyUtil
import requests

from dl_common import Downloader, Args

def nearest(dt, delta = 5):
    minute = delta * int(dt.minute / delta)
    return dt.replace(minute = minute, second = 0)

class ForgeArgs(Args):
    def __init__(self, opt, args):
        super().__init__(opt, args)

        self.apiKey         = opt.apiKey
        self.currenciesStr  = opt.currencies
        self.currencies     = opt.currencies.split(',')
        self.currencyLUT    = {}
        self.overwrite      = opt.overwrite

class Forge(Downloader):
    def __init__(self):
        super().__init__(ArgsClass = ForgeArgs)

    def initArgsParser(self):
        parser = super().initArgsParser()

        parser.add_option("-K", "--apiKey", dest = "apiKey", default = None, help = "The default key")
        parser.add_option("-C", "--currencies", dest = "currencies", help = "The currencies to download")
        parser.add_option("-O", "--overwrite", dest = "overwrite", action="store_true", default = False, help = "Overwrite the destination file")

        return parser

    def download(self):
        urlInfo = {
            "apiKey": self.args.apiKey,
            "currencies": self.args.currenciesStr
        }

        url = self.args.server.format(**urlInfo)

        data = PyUtil.getJsonFromUrl(url, timeout = 1)

        if data is None:
            sys.exit(1)

        output = self.args.output

        for cdata in data:
            timestamp = int(cdata["timestamp"])
            time = DT.datetime.fromtimestamp(timestamp)

            # While overwriting we maintain the exact timestamp
            if self.args.overwrite is False:
                time = nearest(time)

            currency = cdata["symbol"]
            price = cdata["price"]

            outputDir = self.formatFilename(self.args.output, time)
            outputDir = outputDir.replace("$CURRENCY", currency)

            os.makedirs(os.path.dirname(outputDir), exist_ok = True)

            idate = PyUtil.dateToInt(time)

            # Write the data now ...
            filename = outputDir + '.csv'
            zipFilename = filename + '.zip'

            fileHandle = None

            # if the zipfilename exists, then extract it first ...
            if os.path.exists(zipFilename) and self.args.overwrite is False:
                PyUtil.unzipFile(zipFilename)
                fileHandle = open(filename, 'a+')
            else:
                fileHandle = open(filename, 'w')
                fileHandle.write("time,close\n")

            strTime = PyUtil.dateTimeFormat(time)

            print("%s => %s - %0.5f" % (currency, strTime, price))

            fileHandle.write("%s,%0.5f\n" % (strTime, price))
            fileHandle.close()

            if self.args.overwrite is False:
                PyUtil.zipFile(filename, zipFilename)
                os.remove(filename)

        # self.downloadAll()

Forge().download()
