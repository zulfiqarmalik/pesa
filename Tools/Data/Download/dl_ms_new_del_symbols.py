### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys
from optparse import OptionParser
import datetime as DT
import pandas as pd
import ujson
import traceback

from multiprocessing import Pool
from dl_common import Downloader, Args

import threading
from multiprocessing import Process
from threading import Thread

import requests

sys.path.append("../../../PyImpl/Scripts")
sys.path.append("../../../Python")
import PyUtil

class MS_NewDelSymbolsArgs(Args):
    def __init__(self, opt, args):
        super().__init__(opt, args)

        assert self.username, "Invalid username specified"
        assert self.password, "Invalid password specified"
        
        assert opt.baseUrl, "Must specify a base URL from which the data will be downloaded"
        assert opt.exchanges, "Must give exchanges to download in the format: <Country>:<Exchange_Name>=<Exchange_Code>"

        self.baseUrl        = opt.baseUrl 
        exchanges           = opt.exchanges.split("|")
        self.exchangesStr   = opt.exchanges

        self.exchanges      = []

        for exchange in exchanges:
            exchangeParts   = exchange.split(':')
            assert len(exchangeParts) == 2, "Invalid exchange specified: %s. Should be of format: <Country>:<Exchange_Name>=<Exchange_Code>" % (exchange)

            country         = exchangeParts[0].strip()
            exchangeInfo    = exchangeParts[1].split('=')
            assert len(exchangeInfo) == 2, "Invalid exchange specified: %s. Should be of format: <Country>:<Exchange_Name>=<Exchange_Code>" % (exchange)

            name            = exchangeInfo[0].strip()
            code            = exchangeInfo[1].strip()

            self.exchanges.append({
                "country"   : country,
                "name"      : name,
                "code"      : code
            })

class MS_NewDelSymbols(Downloader):
    def __init__(self):
        super().__init__(ArgsClass = MS_NewDelSymbolsArgs)
        self.processes = []

    def downloadExchangeSymbols(self, date, exchange, symType, forceDownload):
        sdate = PyUtil.dateToUKStr(date)

        exchangeName = str.format("%s_%s-%s" % (exchange["country"], exchange["name"], exchange["code"]))
        baseDir = self.formatFilename(self.args.output, date)
        outputDir = baseDir.replace("$EXCHANGE", exchangeName)

        os.makedirs(outputDir, exist_ok = True)

        filename = os.path.join(outputDir, str.format("%s_%d.json" % (symType, PyUtil.dateToInt(date))))

        # No need to downlod a file that already exists ...
        if forceDownload is False and os.path.exists(filename):
            print("[MS_NewDelSymbols] File already exists: %s" % (filename))
            return
            
        # 
        print("[MS_NewDelSymbols] [%s] Downloading %s - Exchange: %s:%s = %s" % (sdate, symType, exchange["country"], exchange["name"], exchange["code"]))

        url = str.format(self.args.baseUrl % (self.args.username, self.args.password, exchange["code"], sdate, sdate))
        url += "&" + symType
        responseData = requests.get(url)

        if responseData.status_code != 200:
            print("Unable to fetch data for date: %d" % (idate))
            sys.exit(1)

        with open(filename, "w") as f:
            f.write(responseData.text)

    def downloadExchange(self, date, exchange, forceDownload):
        # New symbols
        self.downloadExchangeSymbols(date, exchange, "newsymbols", forceDownload)
        self.downloadExchangeSymbols(date, exchange, "delsymbols", forceDownload)

    def downloadDate(self, date, forceDownload):
        for exchange in self.args.exchanges:
            self.downloadExchange(date, exchange, forceDownload)

    def download(self):
        numDates = len(self.args.dates)

        print("===== [MS_NewDelSymbols] Symbol download process STARTED on: %s [%s] =====" % (str(DT.datetime.now()), self.args.exchangesStr))

        # We just download for the last 5 working days (that's all that MorningStar keeps)
        startIndex = max(numDates - 5, 0)
        print("[MS_NewDelSymbols] Downloading date range: [%d, %d]" % (self.args.istartDate, self.args.iendDate))

        for di in range(startIndex, numDates): # date in self.args.dates:
            date = self.args.dates[di]
            # On the last run we force a download
            forceDownload = di == numDates - 5
            self.downloadDate(date, forceDownload)

        print("===== [MS_NewDelSymbols] Symbol download process FINISHED on: %s [%s] =====" % (str(DT.datetime.now()), self.args.exchangesStr))

    def initArgsParser(self):
        parser = super().initArgsParser()

        parser.add_option("-b", "--baseUrl", dest = "baseUrl", default = "http://msxml.tenfore.com/changes/?username=%s&password=%s&exchange=%s&security=1&jsonshort&sdate=%s&edate=%s", help = "The base URL that we're going to use for downloading the data")
        parser.add_option("-x", "--exchanges", dest = "exchanges", help = "The exchanges to download. Should be of the format: <Country>:<Exchange_Name>=<Exchange_Code>,...")

        return parser

if __name__ == '__main__':
    MS_NewDelSymbols().download()
