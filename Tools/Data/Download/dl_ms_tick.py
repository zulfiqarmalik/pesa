### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys
from optparse import OptionParser
import datetime as DT
import pandas as pd
import ujson
import traceback

from multiprocessing import Pool
from dl_common import Downloader, Args

import threading
from multiprocessing import Process
from threading import Thread

import requests

sys.path.append("../../../PyImpl/Scripts")
sys.path.append("../../../Python")
import PyUtil

class MS_TickArgs(Args):
    def __init__(self, opt, args):
        super().__init__(opt, args)

        assert self.username, "Invalid username specified"
        assert self.password, "Invalid password specified"
        
        assert opt.input, "Must specify a valid input directory with all the exchanges that need to be downloaded"
        assert opt.baseUrl, "Must specify a base URL from which the data will be downloaded"

        self.maxInstuments  = 50
        self.baseUrl        = opt.baseUrl 
        self.numThreads     = 8

        self.intermediateDir= opt.intermediate if opt.intermediate else self.output

        inputParts          = opt.input.split("@")
        assert len(inputParts) == 2, "Invalid input: %s. Should be of format: <DIR>@<Country>:<Exchange_Name>=<Exchange_Code> | ..." % (opt.input)

        self.inputDir       = inputParts[0].strip()
        exchanges           = inputParts[1].split('|')

        self.exchanges      = []

        for exchange in exchanges:
            exchangeParts   = exchange.split(':')
            assert len(exchangeParts) == 2, "Invalid exchange specified: %s. Should be of format: <Country>:<Exchange_Name>=<Exchange_Code>" % (exchange)

            country         = exchangeParts[0].strip()
            exchangeInfo    = exchangeParts[1].split('=')
            assert len(exchangeInfo) == 2, "Invalid exchange specified: %s. Should be of format: <Country>:<Exchange_Name>=<Exchange_Code>" % (exchange)

            name            = exchangeInfo[0].strip()
            code            = exchangeInfo[1].strip()

            self.exchanges.append({
                "country"   : country,
                "name"      : name,
                "code"      : code
            })

class MS_Tick(Downloader):
    def __init__(self):
        super().__init__(ArgsClass = MS_TickArgs)
        self.processes = []

    def getAllInstruments(self, name, zipFilename):
        assert os.path.exists(zipFilename), "Unable to find filename: %s" % (zipFilename)
        
        #if not os.path.exists(zipFilename):
        #    print("[MS_Tick] Unable to find filename: %s" % (zipFilename))
        #    return []

        print("Loading: %s" % (zipFilename))
        import zipfile

        data = None

        with zipfile.ZipFile(zipFilename, 'r') as zipHandle:
            fileContents = zipHandle.read(name,) 
            data = ujson.loads(fileContents.decode("utf-8"))

        assert data is not None, "Unable to load from filename: %s" % (zipFilename)

        securities = data["ts"]["results"]
        instruments = []

        # import pdb; pdb.set_trace()

        for security in securities:
            if security["type"] == '1':
                instrumentId = str.format("%s.%s.%s" % (security["exchangeid"], security["type"], security["symbol"]))
                instruments.append(instrumentId)

        # import pdb; pdb.set_trace()
        # print("[MS_Tick] %s = %d" % (zipFilename, len(instruments)))

        return instruments


    def getAllInstrumentsExchange(self, date, exchange):
        sdate = PyUtil.dateToStr(date)
        exchangeName = str.format("%s_%s-%s" % (exchange["country"], exchange["name"], exchange["code"]))
        name = sdate + '.json'
        zipFilename = os.path.join(self.args.inputDir, exchangeName, name + ".zip")

        return self.getAllInstruments(name, zipFilename)

    def downloadInstruments(self, outputDir, date, instruments, startIndex, endIndex, batchIndex):
        # if there are no instruments, then there is nothing to download
        if endIndex == startIndex:
            return

        idate = PyUtil.dateToInt(date)
        r_sdate = PyUtil.dateToUKStr(date)
        dateNext = date + DT.timedelta(days = 1)
        r_sdateNext = PyUtil.dateToUKStr(dateNext)

        si = startIndex
        ei = min(si + self.args.maxInstuments, endIndex)

        data = []

        while si < endIndex:
            try:
                instrumentsStr = instruments[si]

                for i in range(si + 1, ei):
                    instrumentsStr += "," + instruments[i]

                # form the URL and then send the download request
                url = str.format(self.args.baseUrl % (self.args.username, self.args.password, instrumentsStr, r_sdate, r_sdateNext))

                responseData = requests.get(url)

                if responseData.status_code != 200:
                    print("Unable to fetch data for date: %d" % (idate))
                    sys.exit(1)

                batchData = ujson.loads(responseData.text)

                data += batchData["ts"]["results"]

                si = ei
                ei = min(si + self.args.maxInstuments, endIndex)
            except:
                pass

        print("[MS_Tick - %d, %d] Downloaded: %d [%d, %d)" % (idate, batchIndex, len(data), startIndex, endIndex))

        return data

    def downloadDate(self, date, forceDownload):
        instruments = []
        idate = PyUtil.dateToInt(date)

        for exchange in self.args.exchanges:
            exchangeName = str.format("%s_%s-%s" % (exchange["country"], exchange["name"], exchange["code"]))

            intermediateDir = os.path.join(self.args.intermediateDir, exchangeName)
            os.makedirs(intermediateDir, exist_ok = True)

            baseDir = self.formatFilename(self.args.output, date)
            outputDir = baseDir.replace("$EXCHANGE", exchangeName)
            # outputDir = os.path.join(baseDir, exchangeName)

            filename = os.path.join(intermediateDir, str(idate) + '.json')
            zipFilename = os.path.join(outputDir, str(idate) + '.json.zip')

            # make sure that the destination directory exists
            os.makedirs(outputDir, exist_ok = True)

            print("[MS_Tick] Processing: %d" % (idate))

            if forceDownload is False and os.path.exists(zipFilename):
                print("[MS_Tick - %d] File already exists: %s" % (idate, zipFilename))
                return

            # for exchange in self.args.exchanges:
            instruments = self.getAllInstrumentsExchange(date, exchange)
                # instruments += exchangeInstruments

            numInstuments = len(instruments)

            print("[MS_Tick - %d] Num instruments: %d" % (idate, numInstuments))

            if numInstuments == 0:
                print("[MS_Tick - %d] No instruments for the day!" % (idate))
                return

            data = []
            if self.args.numThreads > 1:
                pool = Pool(processes = self.args.numThreads)

                maxCount = int(numInstuments / self.args.numThreads)
                # startIndex = 0
                # endIndex = min(startIndex + maxCount, numInstuments)
                # batchIndex = 0

                results = []

                for t in range(self.args.numThreads):
                    startIndex = t * maxCount
                    endIndex = startIndex + maxCount if t < self.args.numThreads - 1 else numInstuments
                    endIndex = min(endIndex, numInstuments)

                    result = pool.apply_async(self.downloadInstruments, (outputDir, date, instruments, startIndex, endIndex, t))
                    results.append({
                        "result": result,
                        "batchIndex": t,
                        "startIndex": startIndex,
                        "endIndex": endIndex
                    })

                    startIndex = endIndex
                    endIndex = min(startIndex + maxCount, numInstuments)

                # Close and join the pool
                pool.close()
                pool.join()

                # data = []
                allData = []

                for rinfo in results:
                    result = rinfo["result"]
                    idata = result.get()

                    if idata:
                        allData.append(idata)

                self.combineData(idate, allData, filename, zipFilename)

                # process = Thread(target = self.combineData, args = (idate, allData, filename, zipFilename,))
                # process.start()

                # self.processes.append(process)

                # # Every few processes we purge the process queue
                # if len(self.processes) > 10:
                #     for process in processes:
                #         process.join()
                #     self.processes = []

            else:
                data = self.downloadInstruments(outputDir, date, instruments, 0, numInstuments, 0)
                self.writeData(idate, data, filename, zipFilename)

    def writeData(self, idate, data, filename, zipFilename):
        print("Writing to file: %s" % (filename))

        with open(filename, 'w') as f:
            ujson.dump(data, f)

        print("[MS_Tick - %d] [Count: %d] Writing: %s" % (idate, len(data), zipFilename))

        PyUtil.zipFile(filename, zipFilename)
        os.remove(filename)

    def combineData(self, idate, idata, filename, zipFilename):
        print("[MS_Tick - %d] Combining num results: %d" % (idate, len(idata)))

        data = []

        for i in range(len(idata)):
            d = idata[i]

            sys.stdout.write('=')

            if d:
                data += idata

        sys.stdout.write('\n')

        self.writeData(idate, data, filename, zipFilename)

    def download(self):
        print("[MS_Tick] Downloading date range: [%d, %d]" % (self.args.istartDate, self.args.iendDate))

        for di in range(len(self.args.dates)): # date in self.args.dates:
            date = self.args.dates[di]
            forceDownload = self.shouldForceDownload(di)
            self.downloadDate(date, forceDownload)

    def initArgsParser(self):
        parser = super().initArgsParser()

        parser.add_option("-b", "--baseUrl", dest = "baseUrl", default = "http://msxml.tenfore.com/IndexTS/?username=%s&password=%s&jsonshort&vwap&unadjusted&Instrument=%s&sdate=%s&stime=00:00:00&edate=%s&etime=00:00:00&type=minbar", help = "The base URL that we're going to use for downloading the data")
        parser.add_option("-i", "--input", dest = "input", help = "The input directory with all the exchanges to be downloaded. The format is <DIR>@<Country>:<Exchange_Name>=<Exchange_Code> | ...")
        parser.add_option("-I", "--intermediate", dest = "intermediate", default = None, help = "The intermediate directory to use for writing")

        return parser

if __name__ == '__main__':
    MS_Tick().download()
