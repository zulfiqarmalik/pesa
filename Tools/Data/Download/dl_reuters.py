### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys
from optparse import OptionParser
import datetime as DT
import pandas as pd
import requests
import json

from dl_common import Downloader, Args

sys.path.append("../../../PyImpl/Scripts")
sys.path.append("../../../Python")
import PyUtil

def parseTime(time):
    return DT.datetime.strptime(time, '%Y-%m-%dT%H:%M:%S.%fZ')

class ReutersArgs(Args):
    def __init__(self, opt, args):
        super().__init__(opt, args)

        self.url                = opt.url
        self.scheduleId         = opt.scheduleId

        assert self.scheduleId, "Must specify a valid scheduleId to continue"

class Reuters(Downloader):
    def __init__(self):
        super().__init__(ArgsClass = ReutersArgs)

        self.accessToken        = None

    def connect(self):
        print("Connecting to url: %s" % (self.args.url))
        headers = {
            "Content-Type": "application/json; odata=minimalmetadata"
        }

        data = {
            "Credentials": {
                "Username": self.args.username,
                "Password": self.args.password
            }
        }

        url = self.args.url + "Authentication/RequestToken"

        response = requests.post(url, data = json.dumps(data), headers = headers)

        if response.status_code != 200:
            print("Server error. Response code: %d - Response Text: %s" % (response.status_code, response.text))
            sys.exit(1)

        loginInfo = json.loads(response.text)
        self.accessToken = loginInfo["value"]
        print("Access Token: %s" % (self.accessToken))

    def getRequest(self, path):
        url = str.format("%s%s" % (self.args.url, path))
        headers = {
            "Content-Type": "application/json",
            "Authorization": str.format("Token %s" % (self.accessToken))
        }

        response = requests.get(url, headers = headers)
        
        if response.status_code != 200:
            print("Server error. Response code: %d - Response Text: %s" % (response.status_code, response.text))
            sys.exit(1)

        return json.loads(response.text)

    def getScheduleDetails(self):
        scheduleInfo = self.getRequest(str.format("Extractions/Schedules('%s')" % (self.args.scheduleId)))
        extractions = self.getRequest("Extractions/ExtractedFiles")

        scheduleExtractions = []
        extractionsInfo = {}

        for extraction in extractions["value"]:
            if extraction["ScheduleId"] == self.args.scheduleId:
                stime = extraction["LastWriteTimeUtc"]
                time = parseTime(stime)
                extractedFilename = extraction["ExtractedFileName"]

                isNotes = extractedFilename.endswith(".notes.txt")
                isRelevant = extractedFilename.startswith(self.args.format)

                if isNotes is False and isRelevant is True:
                    sdate = extractedFilename[len(self.args.format):len(self.args.format) + 8]
                    idate = int(sdate)

                    if idate and idate >= self.args.istartDate and idate <= self.args.iendDate:
                        extraction["_time"] = time
                        extraction["_date"] = idate

                        if extractedFilename not in extractionsInfo:
                            scheduleExtractions.append(extraction)
                            extractionsInfo[extractedFilename] = {
                                "time": time,
                                "date": idate,
                                "index": len(scheduleExtractions) - 1
                            }
                        elif extractionsInfo[extractedFilename]["time"] < time:
                            scheduleExtractions[extractionsInfo[extractedFilename]["index"]] = extraction


        # for i in range(len(scheduleExtractions)):
        #     print("Schedule: %s => %s" % (scheduleExtractions[i]["ExtractedFileName"], scheduleExtractions[i]["ExtractedFileId"]))

        return scheduleExtractions

    def downloadFile(self, extraction):
        idate           = extraction["_date"]
        time            = extraction["_time"]
        efilename       = extraction["ExtractedFileName"]
        fileId          = extraction["ExtractedFileId"]
        date            = PyUtil.intToDate(idate)
        outputDir       = self.formatFilename(self.args.output, date)
        filename        = os.path.join(outputDir, efilename)

        if os.path.isfile(filename):
            print("File exists: %s" % (filename))

        import pathlib
        pathlib.Path(outputDir).mkdir(parents = True, exist_ok = True)

        url             = str.format("%sExtractions/ExtractedFiles('%s')/$value" % (self.args.url, extraction["ExtractedFileId"]))
        headers         = {
            "Content-Type": "application/json",
            "Authorization": str.format("Token %s" % (self.accessToken))
        }

        # response        = requests.post(url, data = json.dumps(data), headers = headers)
        print("Downloading: %s => %s" % (url, filename))

        r = requests.get(url, stream = True, headers = headers)
        with open(filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024): 
                if chunk: # filter out keep-alive new chunks
                    f.write(chunk)

        print("Downloaded: %s => %s" % (url, filename))

    def downloadAll(self):
        scheduleExtractions = self.getScheduleDetails()

        for i in range(len(scheduleExtractions)):
            self.downloadFile(scheduleExtractions[i])

    def download(self):
        print("[Reuters] Downloading date range: [%d, %d]" % (self.args.istartDate, self.args.iendDate))

        self.connect()
        self.downloadAll()

    def initArgsParser(self):
        parser = super().initArgsParser()

        parser.add_option("-I", "--scheduleId", dest = "scheduleId", help = "The ID of the schedule to download")
        parser.add_option("-U", "--url", dest = "url", default = "https://hosted.datascopeapi.reuters.com/RestApi/v1/", help = "The default base url of the Reuters DataScope platform")

        return parser

Reuters().download()
