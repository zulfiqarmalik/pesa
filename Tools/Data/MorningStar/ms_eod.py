### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys
from os import listdir
from os.path import isfile, join
import csv
import datetime as DT
import json
import pandas as pd
from pandas.tseries.offsets import BDay
import requests
from optparse import OptionParser
import threading

sys.path.append("../../../PyImpl/Scripts")
sys.path.append("../../../Python")
import PyUtil
import traceback

def parseArguments():
    parser = OptionParser()

    parser.add_option("-e", "--exchange", dest = "exchange", default = None, help = "A specific exchange to download")
    parser.add_option("-t", "--threads", dest = "threads", default = "4", help = "The number of threads to use")
    parser.add_option("-o", "--outputDir", dest = "outputDir", default = "X:/devwork/data/Vendors/MorningStar/EOD", help = "The output director where to download the data")
    parser.add_option("-S", "--start", dest = "startDate", help = "The starting date of the download")
    parser.add_option("-E", "--end", dest = "endDate", help = "The end date of the download")
    parser.add_option("-r", "--region", dest = "region", help = "The region that we wish to update")
    parser.add_option("-u", "--username", dest = "username", help = "The username for the remote server")
    parser.add_option("-p", "--password", dest = "password", help = "The password for the remote server")
        
    return parser.parse_args()

opt, args = parseArguments()

assert opt.username, "Must specify a username!"
assert opt.password, "Must specify a password!"

startDate = DT.datetime.strptime(opt.startDate, "%Y%m%d")
endDate = DT.datetime.strptime(opt.endDate, "%Y%m%d") if opt.endDate else DT.datetime.now()

dates = pd.date_range(startDate, endDate, freq = BDay())

baseUrl = 'http://msxml.tenfore.com/IndexTS/?Username=%s&password=%s&HisEODExchange&exchange=%d&JSONShort&unadjusted&vwap&security=1&sdate=%s'
baseDir =  opt.outputDir # "X:/devwork/data/Vendors/MorningStar/EOD"

numThreads = int(opt.threads)

allExchanges = {
# North-America
"North-America": [
    {
        "name": "XTSE", # Canada (Tonorto stock exchange)
        "id": 127,
        "country": "CA",
        "region": "North-America"
    },
    {
        "name": "COMP",
        "id": 126,
        "country": "US",
        "region": "North-America"
    }, 
    {
        "name": "XNYS",
        "id": 14,
        "country": "US",
        "region": "North-America"
    }, 
    {
        "name": "XPSE",
        "id": 16,
        "country": "US",
        "region": "North-America"
    }, 
    {
        "name": "XNAS",
        "id": 19,
        "country": "US",
        "region": "North-America"
    }, 
],

# Asia
"Asia-Pacific": [
    {
        "name": "XTKS", # Japan (Tokyo stock exchange)
        "id": 133,
        "country": "JP",
        "region": "Asia-Pacific"
    },
    {
        "name": "XKRX", # South Korea (Korean stock exchange)
        "id": 141,
        "country": "KR",
        "region": "Asia-Pacific"
    },
    {
        "name": "XHKG", # Hong Kong (Hong Kong stock exchange)
        "id": 134,
        "country": "HK",
        "region": "Asia-Pacific"
    },
    {
        "name": "XJKT", # Indonesia (Jakarta stock exchange)
        "id": 140,
        "country": "JD",
        "region": "Asia-Pacific"
    },
    {
        "name": "XKLS", # Malaysia (Kuala Lumpur stock exchange)
        "id": 135,
        "country": "MY",
        "region": "Asia-Pacific"
    },
    {
        "name": "XPHS", # Phillipines (Phillipine stock exchange)
        "id": 142,
        "country": "PH",
        "region": "Asia-Pacific"
    },
    {
        "name": "XSES", # Singapore (Singapore stock exchange)
        "id": 143,
        "country": "SG",
        "region": "Asia-Pacific"
    },
    {
        "name": "XTAI", # Taiwan (Taiwan stock exchange)
        "id": 144,
        "country": "TW",
        "region": "Asia-Pacific"
    },
    # {
    #     "name": "XHKG", # Hong Kong (Hong Kong stock exchange)
    #     "id": 134,
    #     "country": "HK",
    #     "region": "Asia-Pacific"
    # },
    {
        "name": "XASX", # Australia (Australian stock exchange)
        "id": 146,
        "country": "AU",
        "region": "Asia-Pacific"
    },
    {
        "name": "XBOM", # India (Mumbai stock exchange)
        "id": 139,
        "country": "IN",
        "region": "Asia-Pacific"
    },
    {
        "name": "XNSE", # India (National stock exchange)
        "id": 138,
        "country": "IN",
        "region": "Asia-Pacific"
    },
],

# Latin America
"LATAM": [
    {
        "name": "XBUE", # Argentina (Buenos Aris stock exchange)
        "id": 237,
        "country": "AR",
        "region": "Latin-America"
    },
    {
        "name": "XBSP", # Brazil (Sao Paulo stock exchange)
        "id": 90,
        "country": "BR",
        "region": "Latin-America"
    },
    {
        "name": "XSGO", # Chile (Santiago stock exchange)
        "id": 233,
        "country": "CL",
        "region": "Latin-America"
    },
    {
        "name": "XMEX", # Mexico (Mexican stock exchange)
        "id": 50,
        "country": "MX",
        "region": "Latin-America"
    },
],

# Europe

# {
#     "name": "BSEX", # Azerbijan (Baku stock exchange)
#     "id": 89,
#     "country": "AZ",
# },
# {
#     "name": "XBDA", # Bermuda (Bermuda stock exchange)
#     "id": 89,
#     "country": "AZ",
# },

"Europe": [
    {
        "name": "XLON",
        "id": 151,
        "country": "GB",
        "region": "Europe"
    }, {
        "name": "XFRA",
        "id": 200,
        "country": "DE",
        "region": "Europe"
    }, {
        "name": "XETR",
        "id": 213,
        "country": "DE",
        "region": "Europe"
    }, {
        "name": "XBER",
        "id": 215,
        "country": "DE",
        "region": "Europe"
    }, {
        "name": "XPAR",
        "id": 160,
        "country": "FR",
        "region": "Europe"
    }, {
        "name": "XBAR",
        "id": 199,
        "country": "ES",
        "region": "Europe"
    }, {
        "name": "XAMS",
        "id": 202,
        "country": "NL",
        "region": "Europe"
    }, {
        "name": "XBRU",
        "id": 207,
        "country": "BE",
        "region": "Europe"
    }, {
        "name": "XLIS",
        "id": 195,
        "country": "PT",
        "region": "Europe"
    }, {
        "name": "XDUB",
        "id": 190,
        "country": "IE",
        "region": "Europe"
    }, {
        "name": "XSWX",
        "id": 182,
        "country": "CH",
        "region": "Europe"
    }, {
        "name": "XVTX",
        "id": 185,
        "country": "CH",
        "region": "Europe"
    }, {
        "name": "XSTO",
        "id": 170,
        "country": "SE",
        "region": "Europe"
    }, {
        "name": "XCSE",
        "id": 172,
        "country": "DK",
        "region": "Europe"
    }, {
        "name": "XOSL",
        "id": 174,
        "country": "NO",
        "region": "Europe"
    }, {
        "name": "XWBO",
        "id": 194,
        "country": "AT",
        "region": "Europe"
    }, {
        "name": "XPRA",
        "id": 62,
        "country": "CZ",
        "region": "Europe"
    }, {
        "name": "XHEL",
        "id": 176,
        "country": "FI",
        "region": "Europe"
    }, {
        "name": "XCYS",
        "id": 239,
        "country": "CY",
        "region": "Europe" # Cyprus (Cyprus stock exchange)
    }, {
        "name": "XATH",
        "id": 212,
        "country": "GR",
        "region": "Europe" # Greece (Athens stock exchange)
    }, {
        "name": "XBUD", # Hugrary (Budapest stock exchange)
        "id": 63,
        "country": "HU",
        "region": "Europe"
    }, {
        "name": "XMIL", # Italy (Milan stock exchange)
        "id": 223,
        "country": "IT",
        "region": "Europe"
    }, {
        "name": "XLUX", # Luxomberg (Luxomberg stock exchange)
        "id": 232,
        "country": "LU",
        "region": "Europe"
    }, {
        "name": "XMAL", # Malta (Malta stock exchange)
        "id": 222,
        "country": "MA",
        "region": "Europe"
    }, {
        "name": "XWAR", # Poland (Warsaw stock exchange)
        "id": 42,
        "country": "PL",
        "region": "Europe"
    }, {
        "name": "XBSE", # Romania (Romania stock exchange)
        "id": 228,
        "country": "RO",
        "region": "Europe"
    }, {
        "name": "XKIS", # Ukraine (Kiev stock exchange)
        "id": 49,
        "country": "UA",
        "region": "Europe"
    }
]}

class DownloadThread(threading.Thread):
    def __init__(self, exchanges, startIndex, endIndex, dateStartIndex = None, dateEndIndex = None):
        super().__init__()

        self.startIndex = startIndex
        self.endIndex = endIndex
        self.exchanges = exchanges

        self.dateStartIndex = dateStartIndex if dateStartIndex is not None else 0
        self.dateEndIndex = dateEndIndex if dateEndIndex is not None else len(dates)

    def run(self):
        for eix in range(self.startIndex, self.endIndex):
            exchange = self.exchanges[eix]

            try:
                print("Handling exchange: %s-%s [%d, %d]" % (exchange["country"], exchange["name"], self.dateStartIndex, self.dateEndIndex))
                # dstDir = os.path.join(baseDir, exchange["region"], "MorningStar", str.format("%s-%d" % (exchange["name"], exchange["id"])))
                dstDir = os.path.join(baseDir, exchange["region"], str.format("%s_%s-%d" % (exchange["country"], exchange["name"], exchange["id"])))
                os.makedirs(dstDir, exist_ok = True)

                di = self.dateStartIndex

                numRetries = 0
                while di < self.dateEndIndex:
#                for di in range(self.dateStartIndex, self.dateEndIndex):
                    try:
                        date = dates[di]
                        sdate = PyUtil.dateToStr(date)
                        msdate = PyUtil.dateToUKStr(date)

                        url = str.format(baseUrl % (opt.username, opt.password, exchange["id"], msdate))
                        filename = os.path.join(dstDir, sdate + '.json')
                        zipFilename = filename + '.zip'

                        if os.path.isfile(zipFilename):
                            print("File already exists: %s" % (zipFilename))
                            di += 1
                            continue

                        # print("Handling date: %s - %s. URL: %s" % (exchange["name"], sdate, url))

                        print("Handling date: %s [URL: %s]" % (sdate, url))
                        responseData = requests.get(url)

                        if responseData.status_code != 200:
                            print("Server error. Response code: %d - Response Text: %s" % (responseData.status_code, text))
                            numRetries += 1
                            assert numRetries < 3, "Number of retries exceeded %d for URL: %s" % (numRetries, url)
                            continue
#                            sys.exit(1)

                        data = responseData.text
                        with open(filename, 'w') as file:
                            file.write(responseData.text)

                        print("File downloaded: %s => %s" % (filename, zipFilename))
                        PyUtil.zipFile(filename, zipFilename)
                        os.remove(filename)

                        di += 1
                        numRetries = 0
                    except Exception:
                        pass

            except Exception as e:
                print(traceback.format_exc())

exchanges = []
assert opt.region, "Must specify a region to download [North-America, Asia, Europe, LATAM]"

# If no specific exchange is specified, then we download the entire region
if opt.exchange is None:
    exchanges = allExchanges[opt.region]
else:
    for exchange in allExchanges[opt.region]:
        if exchange["name"] == opt.exchange:
            exchanges = [exchange]
            break

threads = []
totalCount = len(exchanges)

assert totalCount, "No exchanges selected for region: %s [Exchange: %s]" % (opt.region, opt.exchange)

totalDateCount = len(dates)
maxCount = 1 #max(int(totalCount / numThreads), 1)
maxDateCount = max(int(totalDateCount / numThreads), 1)
startIndex = 0
endIndex = 0

while startIndex < totalCount:
    endIndex = startIndex + maxCount

    thread = DownloadThread(exchanges, startIndex, endIndex)
    thread.start()
    threads.append(thread)

    startIndex = endIndex

# thread = DownloadThread(startIndex, len(exchanges))
# thread.start()

threads.append(thread)

print("Waiting for threads [Count: %d] ..." % (len(threads)))

for thread in threads:
    thread.join()

print("All threads finished. Exiting!")

