### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys
from os import listdir
from os.path import isfile, join
import csv
import datetime as DT
import json
import pandas as pd
from pandas.tseries.offsets import BDay
import requests
from optparse import OptionParser
import threading
import traceback
import json

sys.path.append("../../../PyImpl/Scripts")
import PyUtil

instrumentUrl = "http://msuxml.morningstar.com/indexTS/?username=%s&password=%s&instrument=%s&corpactions&jsonshort&sdate=%s&edate=%s"
maxInstruments = 20

def parseArguments():
    parser = OptionParser()

    parser.add_option("-i", "--instruments", dest = "instruments", help = "Comma separated list of securities to download")
    parser.add_option("-u", "--username", dest = "username", help = "The morningstar username to use")
    parser.add_option("-p", "--password", dest = "password", help = "The mornignstar password to use")
    parser.add_option("-S", "--startDate", dest = "startDate", help = "The starting date as YYYYMMDD")
    parser.add_option("-E", "--endDate", dest = "endDate", help = "The end date as YYYYMMDD")
    parser.add_option("-d", "--detailed", action = "store_true", default = False, help = "Whether to dump the detailed json or not")
        
    return parser.parse_args()

opt, args = parseArguments()

assert opt.username, "Must specify a username!"
assert opt.password, "Must specify a password!"
assert opt.instruments, "Must specify an instrument list!"

startDate = DT.datetime.strptime(opt.startDate if opt.startDate else "20070101", "%Y%m%d")
endDate = DT.datetime.strptime(opt.endDate, "%Y%m%d") if opt.endDate else DT.datetime.now()

startDateUK = PyUtil.dateToUKStr(startDate)
endDateUK = PyUtil.dateToUKStr(endDate)

# instruments = opt.instruments.split(',')
url = str.format(instrumentUrl % (opt.username, opt.password, opt.instruments, startDateUK, endDateUK))

print("Downloading: %s" % (url))

responseData = requests.get(url)
if responseData.status_code != 200:
    print("Server error. Response code: %d - Response Text: %s" % (responseData.status_code, text))

data = json.loads(responseData.text)
instruments = opt.instruments.split(',')

results = data["ts"]["results"]
instrumentResults = {}

for result in results:
    symbol = result["symbol"]
    print("Results: %s" % (symbol))

    for d in result["data"]:
        date = "____-__-__"
        annDate = "____-__-__"

        if "S750" in d:
            date = PyUtil.dateToStr(DT.datetime.strptime(d["S750"], "%d.%m.%Y"))

        if "S740" in d:
            annDate = PyUtil.dateToStr(DT.datetime.strptime(d["S740"], "%d.%m.%Y"))

        pstr = str.format("    [%s | %s] => %s" % (date, annDate, d["S730"]))

        if opt.detailed is True:
            pstr += str.format(" [%s]" % (json.dumps(d)))
                               
        print("%s" % (pstr))
