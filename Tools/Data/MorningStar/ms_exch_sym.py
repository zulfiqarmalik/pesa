### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys
from os import listdir
from os.path import isfile, join
import csv
import datetime as DT
import json
import pandas as pd
import requests
from optparse import OptionParser

sys.path.append("../../../PyImpl/Scripts")
import PyUtil

def strToDate(date, sep = "-"):
    return DT.datetime.strptime(date, "%d" + sep + "%m" + sep + "%Y")

def dateToStr(date, sep = "-"):
    return str(date.day).zfill(2) + sep + str(date.month).zfill(2) + sep + str(date.year).zfill(4)
    
def dateToUTCStr(date):
    return str(date.year).zfill(4) + str(date.month).zfill(2) + str(date.day).zfill(2) + "000000Z"

def parseArguments():
    parser = OptionParser()

    parser.add_option("-S", "--start", dest = "startDate", help = "The starting date of the download")
    parser.add_option("-E", "--end", dest = "endDate", help = "The end date of the download")
        
    return parser.parse_args()

opt, args = parseArguments()

startDate = DT.datetime.strptime(opt.startDate, "%Y%m%d")
endDate = DT.datetime.strptime(opt.endDate, "%Y%m%d") if opt.endDate else DT.datetime.now

baseUrl = 'http://mstxml.tenfore.com/IndexTS/?Username=OnOn17079&password=9ZCAZE&SymbolList&exchange=%d&jsonshort&sdate=%s'
baseDir = "X:/devwork/data/Vendors/MorningStar/ExchangeSymbols"

exchanges = [{
    "name": "GB_XLON",
    "id": 151,
    "market": "EU"
}, {
    "name": "DE_XFRA",
    "id": 213,
    "market": "EU"
}, {
    "name": "FR_XPAR",
    "id": 160,
    "market": "EU"
}, {
    "name": "ES_XMCE",
    "id": 174,
    "market": "EU"
}, {
    "name": "NL_XAMS",
    "id": 202,
    "market": "EU"
}, {
    "name": "BE_XBRU",
    "id": 207,
    "market": "EU"
}, {
    "name": "PT_XLIS",
    "id": 195,
    "market": "EU"
}, {
    "name": "IE_XDUB",
    "id": 190,
    "market": "EU"
}, {
    "name": "CH_XSWX",
    "id": 182,
    "market": "EU"
}, {
    "name": "SE_XSTO",
    "id": 170,
    "market": "EU"
}, {
    "name": "DK_XCSE",
    "id": 172,
    "market": "EU"
}, {
    "name": "NO_XOSL",
    "id": 174,
    "market": "EU"
}, {
    "name": "AT_WBAH",
    "id": 194,
    "market": "EU"
}, {
    "name": "CZ_XPRA",
    "id": 62,
    "market": "EU"
}, {
    "name": "US_COMP",
    "id": 126,
    "market": "US"
}]

for exchange in exchanges:
    print("Handling exchange: %s" % (exchange["name"]))
    dstDir = os.path.join(baseDir, str.format("%s-%d" % (exchange["name"], exchange["id"])))

    if not os.path.exists(dstDir):
        print("Making directory: %s" % (dstDir))
        os.mkdir(dstDir)

    for year in range(startDate.year, endDate.year + 1):
        sdate = "01-01-" + str(year)
        url = str.format(baseUrl % (exchange["id"], sdate))
        filename = os.path.join(dstDir, str(year) + '.json')

        if os.path.isfile(filename + ".zip"):
            print("File already exists: %s.zip" % (filename))
            continue

        # print("Handling date: %s - %s. URL: %s" % (exchange["name"], sdate, url))

        print("Downloading: %s" % (url))

        responseData = requests.get(url)

        if responseData.status_code != 200:
            print("Server error. Response code: %d - Response Text: %s" % (responseData.status_code, text))
            sys.exit(1)

        data = responseData.text
        with open(filename, 'w') as file:
            file.write(responseData.text)

        zipFilename = filename + '.zip'
        print("File downloaded: %s => %s" % (filename, zipFilename))
        PyUtil.zipFile(filename, zipFilename)
        os.remove(filename)

