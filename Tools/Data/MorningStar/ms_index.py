### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys
from os import listdir
from os.path import isfile, join
import csv
import datetime as DT
import json
import pandas as pd
from pandas.tseries.offsets import BDay
import requests
from optparse import OptionParser
import threading
import traceback

sys.path.append("../../../PyImpl/Scripts")
import PyUtil

indexUrl = "http://mstxml.tenfore.com/index.php?username=%s&password=%s&jsonshort&index=%s"
instrumentUrl = "http://msuxml.morningstar.com/Index.php?Username=%s&password=%s&JSONShort&fields=H1,H2,H3,S19,S1405,S1407&instrument=%s"
maxInstruments = 20

def downloadIndex(opt, index):
    url = str.format(indexUrl % (opt.username, opt.password, index))

    print("Downloading index from URL: %s" % (url))

    responseData = requests.get(url)
    assert responseData.status_code == 200, "Server error. Response code: %d - Response Text: %s" % (responseData.status_code, text)

    data = json.loads(responseData.text)

    return data["quotes"]["results"][0][index].split(",")
    

def resolveIndex(opt, instruments):
    # Now resolve the data for all the instruments ...
    instrumentIds = {}
    numInsturments = len(instruments)
    startIndex = 0
    endIndex = min(maxInstruments, numInsturments)

    resultsList = []

    while startIndex < numInsturments:
        print("Resolving instruments: [%d, %d)" % (startIndex, endIndex))
        instrumentList = instruments[startIndex]
        instrumentIds[instruments[startIndex]] = ""

        for i in range(startIndex + 1, endIndex):
            instrumentIds[instruments[i]] = ""
            instrumentList += "," + instruments[i]

        startIndex = endIndex
        endIndex = min(startIndex + maxInstruments, numInsturments)

        url = str.format(instrumentUrl % (opt.username, opt.password, instrumentList))
        responseData = requests.get(url)
        assert responseData.status_code == 200, "Server error. Response code: %d - Response Text: %s" % (responseData.status_code, text)

        results = json.loads(responseData.text)["quotes"]["results"]

        for result in results:
            instrumentId = str.format("%s.%s.%s" % (result["H2"], result["H3"], result["H1"]))
            assert instrumentId in instrumentIds, "Unable to find instrument in the instrument list: %s" % (instrumentId)
            assert result["S1405"], "Unable to find figi for instrument: %s" % (instrumentId)

            ticker = result["H1"]
            figi = result["S1407"]
            cfigi = result["S1405"]
            isin = result["S19"]

            instrumentIds[instrumentId] = figi

            resultsList.append({
                "ticker": ticker,
                "figi": figi,
                "cfigi": cfigi,
                "isin": isin
            })

    return instrumentIds, resultsList


def parseArguments():
    parser = OptionParser()

    parser.add_option("-i", "--indices", dest = "indices", help = "Comma separated list of indices to download. Indices should be of the format <MorningStar Index ID>|<BB Index Name> e.g. Stoxx 600 would be 221.10.SXXP|SXXP")
    parser.add_option("-o", "--output", dest = "outputDir", help = "The base directory where to output")
    parser.add_option("-u", "--username", dest = "username", help = "The username for the remote server")
    parser.add_option("-p", "--password", dest = "password", help = "The password for the remote server")
        
    return parser.parse_args()

opt, args = parseArguments()

assert opt.indices, "Must specify some indices to download"
assert opt.username, "Must specify a username!"
assert opt.password, "Must specify a password!"

indices = [x.strip() for x in opt.indices.split(',')]
date = DT.datetime.now()
idate = PyUtil.dateToInt(date)

# Now we download the index
for index in indices:
    try:
        indexInfo = index.split('|')
        assert len(indexInfo) == 2, "Invalid index: %s given as arguments. Index names must be of the format: <MorningStar Index ID>|<BB Index Name> e.g. Stoxx 600 would be 221.10.SXXP|SXXP" % (index)

        ms_index = indexInfo[0]
        bb_index = indexInfo[1]

        outputDir = os.path.join(opt.outputDir, bb_index)

        os.makedirs(outputDir, exist_ok = True)

        filename = os.path.join(outputDir, str(idate) + '.json')
        zipFilename = filename + '.zip'

        if os.path.exists(zipFilename):
            print("File already exists: %s" % (zipFilename))
            continue

        indexData = downloadIndex(opt, ms_index)
        instrumentIds, resultsList = resolveIndex(opt, indexData)

        print("Writing filename: %s" % (filename))

        with open(filename, 'w') as f:
            f.write('{"results":%s}' % (json.dumps(resultsList)))

        PyUtil.zipFile(filename, zipFilename)
        os.remove(filename)

    except Exception as e:
        print("Error downloading index: %s" % (index))
        print(traceback.format_exc())
