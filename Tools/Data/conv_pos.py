### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
from os import listdir
from os.path import isfile, join
import csv
import datetime as DT
import json
from optparse import OptionParser
import glob

def parseArguments():
    parser = OptionParser()

    parser.add_option("-S", "--start", dest = "startDate", default="20170301", help = "The starting filename")
    parser.add_option("-E", "--end", dest = "endDate", default="20170627", help = "The endng filename")
    parser.add_option("-s", "--search", dest = "search", default="W:\\incoming\\PnL\\DW_REPORT_PNLBYSTRAT_HKHA_DTD_*.CSV", help = "The search filename")
    parser.add_option("-o", "--output", dest = "outputDir", default="P:\\Work\\Research\\PosFiles\\US", help = "The directory to output the data")
    parser.add_option("-p", "--prefix", dest = "prefix", default="MyPort", help = "The prefix to use for output files. Defaults to MyPort")
    parser.add_option("-t", "--tag", dest = "tag", default="HKHA-GENERALIST-US", help = "The tag of the strategy to convert to pos file")
        
    return parser.parse_args()

opt, args = parseArguments()
print("Searching: %s" % (opt.search))
filenames = glob.glob(opt.search)

print("Num source files: %d" % (len(filenames)))

srcFilenames = []

startFilename = opt.search.replace('*', opt.startDate)
endFilename = opt.search.replace('*', opt.endDate)

for filename in filenames:
    if filename >= startFilename and filename <= endFilename:
        srcFilenames.append(filename)

print("Num selected files: %d" % (len(srcFilenames)))

for srcFilename in srcFilenames:
    print("Handling file: %s" % (srcFilename))

    with open(srcFilename, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter = ',')
        numRows = 0
        header = None

        srcDate = os.path.splitext(os.path.basename(srcFilename))[0].split("_")[-1]
        dstFilename = os.path.join(opt.outputDir, opt.prefix + "_" + srcDate + ".pos")

        with open(dstFilename, 'w') as dstFile:
            dstFile.write("# Universe: \n")
            dstFile.write("# BookSize: 0\n")
            dstFile.write("# Date: %s\n" % (srcDate))
            dstFile.write("# Gen. Timestamp: %s\n" % (srcDate))
            dstFile.write("# Version: \n")
            dstFile.write("# BB-Ticker | CFIGI | Shares | CCY Price\n")

            for row in reader:
                numRows += 1
                if header is None:
                    header = row
                else:
                    if len(row) >= 7:
                        busUnit = row[0].strip()
                        stratName = row[1].strip()

                        if busUnit == "HKHA" and opt.tag == stratName:
                            ticker = row[2].replace("MS SWAP", "").replace("MLP CI-OTC", "").strip()

                            if " CCY" in ticker or "DO NOT TRADE" in ticker or len(ticker) > 20:
                                continue 

                            numShares = row[4].strip().replace(')', '').replace('(', '-').replace(',', '')
                            ccy = row[5].strip()
                            price = row[6].strip().replace(')', '').replace(',', '')

                            if price and numShares:
                                numShares = int(numShares)
                                price = float(price)

                                if ccy == 'USD':
                                    ccy = ''

                                dstFile.write("%s | | %d | %s %0.2f\n" % (ticker, numShares, ccy, price))




