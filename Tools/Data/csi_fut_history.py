### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import zipfile
import pathlib

from shutil import copyfile

srcDir = "X:/devwork/data/Commodities/Commodities"
dstDir = "X:/devwork/data/Commodities/daily"
deleteSource = dstDir is None
secs = ["C2", "CB", "CC2", "CL1", "CL2", "CT2", "DA", "FF", "FV", "HO1", "HO2", "KC2", "LB", "NG1", "NG2", "OJ2", "QG", "QM", "RB1", "RB2", "SB1", "SB2", "T3", "TY", "UL2", "US", "US10", "ZC"]

os.chdir(srcDir)

for folder, subfolders, filenames in os.walk('./'):
    for filename in filenames:
        sedId = None

        for sec in secs:
            if filename.startswith(sec):
                secId = sec
                break

        if secId is None:
            continue

        name = os.path.basename(filename)
        name = name[:-4] # remove .csv at the end
        name = name[-5:] # get the year and the suffix for the expiry code

        fileDir = os.path.join(dstDir, folder, secId)
        pathlib.Path(fileDir).mkdir(parents = True, exist_ok = True)

        dstFilename = os.path.join(fileDir, secId + "_" + name + ".csv")

        print("%s => %s" % (filename, dstFilename))

        copyfile(filename, dstFilename)
         
