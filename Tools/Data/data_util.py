### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import sys
import datetime as DT
import ctypes
import numpy as np

def dateToStr(date, sep = "-"):
    return str(date.year).zfill(4) + sep + str(date.month).zfill(2) + sep + str(date.day).zfill(2)

def strToDate(date, sep = "-"):
    return DT.datetime.strptime(date, "%Y" + sep + "%m" + sep + "%d")
    
def bamPOST(serverUrl, username, password, data):
    import requests
    from requests_ntlm import HttpNtlmAuth
    
    assert username, "Must specify a username -u parameter on the command line for NT authentication"
    assert password, "Must specify a password -p parameter on the command line for NT authentication"

    responseData = requests.post(serverUrl, data = data, auth = HttpNtlmAuth(username, password))

    # Some error was returned
    if responseData.status_code < 200 or responseData.status_code >= 300:
        text = responseData.text.replace("{", "{{")
        text = text.replace("}", "}}")
        print("Server error. Response code: %d - Response Text: %s" % (responseData.status_code, text))
        return None
        
    import json
    jsonStr = responseData.text
    data = json.loads(jsonStr)
    return data

