### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
from os import listdir
from os.path import isfile, join
import csv
import datetime as DT
import json

def strToDate(date, sep = "-"):
    return DT.datetime.strptime(date, "%Y" + sep + "%m" + sep + "%d" + " 00:00:00")

def dateToStr(date, sep = "-"):
    return str(date.year).zfill(4) + sep + str(date.month).zfill(2) + sep + str(date.day).zfill(2)
    
def dateToUTCStr(date):
    return str(date.year).zfill(4) + str(date.month).zfill(2) + str(date.day).zfill(2) + "000000Z"

delimiter = ","
filename = "Y:/devwork/data/US/FoxBerry/mom_test.csv"
dstDir = "Y:/devwork/data/US/FoxBerry/daily"

print("Handling file: %s" % (filename))
header = None
data = {}

with open(filename, 'r') as csvfile:
    reader = csv.reader(csvfile, delimiter = delimiter)
    numRows = 0

    for row in reader:
        numRows += 1
        if header is None:
            header = row
        else:
            sdate = row[0]
            date = strToDate(sdate)
            sdate = dateToStr(date)
            dateData = None

            row[0] = sdate

            if row[2] == "Public" and row[5] == "United States" and row[7]:
                print("%s - %d - %s" % (filename, numRows, sdate))

                # fix the ticker ...
                parts = row[7].split(':')
                assert len(parts) == 2, 'Unable to format ticker string: %s' % (row[7])
                row[7] = parts[1]

                if sdate not in data:
                    dstFilename = os.path.join(dstDir, "mom_test_" + sdate + ".csv")
                    dateData = {
                        "filename": dstFilename,
                        "file": open(dstFilename, "w")
                    }

                    # Write the header first ...
                    dateData["file"].write(delimiter.join(header) + "\n")
                    data[sdate] = dateData
                else:
                    dateData = data[sdate]

                dateData["file"].write(delimiter.join(row) + "\n")

