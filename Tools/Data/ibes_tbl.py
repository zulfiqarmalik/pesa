### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import requests
from requests_ntlm import HttpNtlmAuth
import json
import pprint
from datetime import datetime as DT
from datetime import date, timedelta
import time
import pandas
import urllib
import os
import sys

serverUrl = "http://dalapi/v1/get"
username = "FOUNTAINHEAD\\zmalik"
password = "BetterPa55"

tables = [
    "IBES2ndEstL3", 
    "IBES2ndEstL2", 
    "IBESActL1", 
    "IBESActL2", 
    "IBESActL3", 
    "IBESAdj", 
    "IBESCur", 
    "IBESEPSL1", 
    "IBESEPSL2", 
    "IBESEPSL3", 
    "IBESEst2ML1", 
    "IBESEstL1", 
    "IBESEstL2", 
    "IBESEstL3", 
    "IBESFCo", 
    "IBESHist3", 
    "IBESInfo3", 
    "IBESIntDlyEst", 
    "IBESIntDlyPTg", 
    "IBESIntDlyRec", 
    "IBESPSum", 
    "IBESRsEPSL1", 
    "IBESRsEPSL2", 
    "IBESRsEPSL3", 
    "IBESSrmL1", 
    "IBESSrmL2", 
    "IBESSrmL3", 
    "IBESSurp", 
    "IBESUSDlyEst", 
    "IBESUSDlyPTg", 
    "IBESUSDlyRec", 
    "IBESActL1_changes", 
    "IBESActL2_changes", 
    "IBESActL3_changes", 
    "IBESAdj_changes", 
    "IBESCur_changes", 
    "IBESEPSL1_changes", 
    "IBESEPSL2_changes", 
    "IBESEPSL3_changes", 
    "IBESEst2ML1_changes", 
    "IBESEstL1_changes", 
    "IBESEstL2_changes", 
    "IBESEstL3_changes", 
    "IBESFCo_changes", 
    "IBESHist3_changes", 
    "IBESIntDlyEst_changes", 
    "IBESIntDlyPTg_changes", 
    "IBESIntDlyRec_changes", 
    "IBESPSum_changes", 
    "IBESRsEPSL1_changes", 
    "IBESRsEPSL2_changes", 
    "IBESRsEPSL3_changes", 
    "IBESSrmL1_changes", 
    "IBESSrmL2_changes", 
    "IBESSrmL3_changes", 
    "IBESSurp_changes", 
    "IBESUSDlyEst_changes", 
    "IBESUSDlyPTg_changes", 
    "IBESUSDlyRec_changes"
]

import data_util
for table in tables:
    try:
        td = data_util.bamPOST(serverUrl, username, password, data = {
            "Keyword": "IBESPerTypeByTable",
            "yml": "\\\\Fountainhead\\BAM\\DataAPI_Config\\prod\\1.1.17\\Shared\\IBES.yml",
            "TableName": "'" + table + "'"
        })

        if td and "data" in td and "PerType" in td["data"]:
            print("%s = %s" % (table, td["data"]["PerType"]))
    except:
        print("Error: %s" % (table))

