### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
from os import listdir
from os.path import isfile, join
import csv
import datetime as DT
import json
import zipfile
from pathlib import Path
import multiprocessing

def strToDate(date, sep = "/"):
    return DT.datetime.strptime(date, "%m" + sep + "%d" + sep + "%Y")

def dateToStr(date, sep = "-"):
    return str(date.year).zfill(4) + sep + str(date.month).zfill(2) + sep + str(date.day).zfill(2)
    
def dateToUTCStr(date):
    return str(date.year).zfill(4) + str(date.month).zfill(2) + str(date.day).zfill(2) + "000000Z"

# baseDir = "Y:\\devwork\\data\\US\\QSG"
baseDir = "Y:\\devwork\\data\\US\\QSG"
numThreads = 8

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        pass

def handleFile(filename):
    try:
        filename = str(filename)
        print("Handling file: %s" % (filename))

        dirname = os.path.dirname(filename)
        basename = os.path.basename(dirname)
        print("Extracting zip file %s => %s" % (filename, dirname))

        dataFilenames = []

        with zipfile.ZipFile(filename, "r") as zipHandle:
            zipHandle.extractall(dirname)
            dataFilenames = zipHandle.namelist()

        # dataFilename = os.path.splitext(filename)[0] + ".txt"
        print("Extraction done!")

        for dataFilename in dataFilenames:
            dataFilename = os.path.join(os.path.dirname(filename), dataFilename)

            print("Handling data file: %s", dataFilename)

            header = None
            data = {}

            with open(dataFilename, 'r') as csvfile:
                reader = csv.reader(csvfile, delimiter = ',')
                numRows = 0

                for row in reader:
                    numRows += 1
                    if header is None:
                        header = row
                    else:
                        sdate = row[0]
                        date = strToDate(sdate)
                        year = date.year
                        sdate = dateToStr(date, "")
                        dateData = None

                        # print("%s - %d - %s" % (filename, numRows, sdate))

                        if sdate not in data:
                            dstDir = os.path.join(dirname, basename, str(year))
                            dstFilename = os.path.join(dstDir, basename + "_" + sdate + ".txt")
                            mkdir_p(dstDir)

                            print("Opening filename: %s" % (dstFilename))

                            dateData = {
                                "filename": dstFilename,
                                "file": open(dstFilename, "w")
                            }

                            # Write the header first ...
                            dateData["file"].write(",".join(header) + "\n")
                            data[sdate] = dateData
                        else:
                            dateData = data[sdate]

                        dateData["file"].write(",".join(row) + "\n")

            for date, dateData in data.items():
                print("Closing filename: %s" % (dateData["filename"]))
                dateData["file"].close()

            print("Removing extracted filename: %s" % (dataFilename))
            os.remove(dataFilename)
            
    except Exception as e:
        print("Exception: %s" % (str(e)))

    return 0

def handleDir(dirname):
    print("Searching director for zip files: %s" % (dirname))
    filenames = Path(dirname).glob('**/*.zip')
    # print("Number of zip files: %d" % (len(filenames)))

    pool = multiprocessing.Pool(processes = numThreads)

    # for filename in filenames:
        # handleFile(str(filename))
        # break
        # pool.apply_async(handleFile, [str(filename)]) 
    pool.map(handleFile, filenames)


if __name__ == "__main__":
    handleDir(baseDir)
