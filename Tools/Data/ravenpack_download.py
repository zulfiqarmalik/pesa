### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import requests
import json
import pprint
from datetime import datetime as DT
from datetime import date, timedelta
import time
import pandas
import urllib
import os
import sys
from optparse import OptionParser

def dateToStr(date, sep = "-"):
    return str(date.year).zfill(4) + sep + str(date.month).zfill(2) + sep + str(date.day).zfill(2)

def strToDate(date, sep = "-"):
    return DT.strptime(date, "%Y" + sep + "%m" + sep + "%d")

def GET(url):
    response = requests.get(url, data = args, verify = False, auth = ("hkhan", "sw6jethu"))
    response.raise_for_status()
    return response.text

def download(url, path):
    print("Get: %s" % (url))
    data = GET(url)

    print("Write: %s" % (path))
    file = open(path, "w")
    file.write(data)
    file.close()

dstDir = ""

if sys.platform == "win32" or sys.platform == "win64" or sys.platform == "windows":
    dstDir = "X:\\Thirdparty\\Ravenpack"
else:
    dstDir = os.path.join(os.path.expanduser('~'), "tdrv", "Thirdparty/Ravenpack")

def parseArguments():
    parser = OptionParser()

    parser.add_option("-D", "--date", dest = "date", help = "The date on which to get the data. Otherwise its the current date", default = None)
    parser.add_option("-R", "--noRefData", dest = "noRefData", action="store_true", default = False, help = "Whether to download the reference data or not")
        
    return parser.parse_args()


opt, args = parseArguments()
date = opt.date if opt.date else dateToStr(DT.today())

if opt.noRefData is False:
    download(str.format("https://ravenpack.com/rpna/newsanalytics/4.0/entity/RP_company-mapping-4.0_%s.csv" % (date)), os.path.join(dstDir, "company_mappings.csv"))
else:
    download(str.format("https://ravenpack.com/rpna/newsanalytics/4.0/daily_ind_eqt/1530-NYC/RPNA_%s_4.0-indicators-equities-1530-NYC.csv" % (date)), os.path.join(dstDir, "daily", "indicators_" + date + ".csv"))
