### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import requests
from requests_ntlm import HttpNtlmAuth
import json
import pprint
from datetime import datetime as DT
from datetime import date, timedelta
import time
import pandas
import urllib
import os
import sys

serverUrl = "http://dalapi/v1/get"
username = "FOUNTAINHEAD\\zmalik"
password = ""

tables = [
    "SM2Code", 
    "SM2Code_changes", 
    "SM2DARMAAM",
    "SM2DARMAAM_changes",
    "SM2DARMAAP",
    "SM2DARMAAP_changes",
    "SM2DARMAEA",
    "SM2DARMAEA_changes",
    "SM2DARMAM",
    "SM2DARMAM_changes",
    "SM2DARMAP",
    "SM2DARMAP_changes",
    "SM2DARMEA", 
    "SM2DARMEA_changes", 
    "SM2DInfo", 
    "SM2DInfo_changes", 
    "SM2DIVAM", 
    "SM2DIVAM_changes", 
    "SM2DIVAP", 
    "SM2DIVAP_changes", 
    "SM2DIVEA", 
    "SM2DIVEA_changes", 
    "SM2DVMoAM",
    "SM2DVMoAM_changes",
    "SM2DVMoAP", 
    "SM2DVMoAP_changes", 
    "SM2DVMoEA", 
    "SM2DVMoEA_changes", 
    "SM2HARM", 
    "SM2HARM_changes", 
    "SM2HEQ", 
    "SM2HEQ_changes", 
    "SM2HIdentChg", 
    "SM2HIdentChg_changes", 
    "SM2HInfo", 
    "SM2HInfo_changes", 
    "SM2HIV", 
    "SM2HIV_changes", 
    "Sm2HSH", 
    "Sm2HSH_changes", 
    "SM2HVMo", 
    "SM2HVMo_changes", 
    "SM2Item", 
    "SM2ItemDesc", 
    "SM2Region"
]

import data_util
for table in tables:
    td = data_util.bamPOST(serverUrl, username, password, data = {
        "Keyword": "SM2ItemByTable",
        "yml": "\\\\Fountainhead\\BAM\\DataAPI_Config\\prod\\1.1.17\\Shared\\StarMine.yml",
        "TableName": "'" + table + "'"
    })

    if td and "data" in td and "Item" in td["data"]:
        print("%s = %s" % (table, td["data"]["Item"]))

