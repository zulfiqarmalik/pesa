### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import requests
from requests_ntlm import HttpNtlmAuth
import json
import pprint
from datetime import datetime as DT
from datetime import date, timedelta
import time
import pandas
import urllib
import os
import sys
from optparse import OptionParser
import traceback

def dateToStr(date, sep = "-"):
    return str(date.year).zfill(4) + sep + str(date.month).zfill(2) + sep + str(date.day).zfill(2)

def strToDate(date, sep = "-"):
    return DT.strptime(date, "%Y" + sep + "%m" + sep + "%d")

def GET(url, args):
    respData = requests.get(url, data = args, verify = False)
    jsonStr = respData.text
    return json.loads(jsonStr)
    
def POST(url, args, loadString = True):
    try:
        respData = requests.post(url, data = args, verify = False)
        jsonStr = respData.text
        # print(jsonStr)
        # if not jsonStr:
        #     return None
        if loadString is True:
            return json.loads(jsonStr)
    except Exception as e:
        exStr = traceback.format_exc()
        print(exStr)
        print("Json response: %s" % (jsonStr))
        return None
        
    return jsonStr

class Thinknum:
    def __init__(self, authToken, dataName, datasetUrl = None):
        self.authToken          = authToken
        self.dataName           = dataName
        self.datasetUrl         = datasetUrl if datasetUrl else "https://data.thinknum.com"
        self.clientId           = "dcf0a1842a4b2bcf44de"
        self.version            = "20151130"
        self.tickerList         = None 
        self.data               = None
        self.maxLimit           = 1000
        self.dstDir             = ""

        if sys.platform == "win32" or sys.platform == "win64" or sys.platform == "windows":
            self.dstDir         = "X:\\Thirdparty\\Thinknum\\daily"
        else:
            self.dstDir         = os.path.join(os.path.expanduser('~'), "tdrv", "Thirdparty/Thinknum/daily")

        self.dstDir             = os.path.join(self.dstDir, self.dataName)

    def makeURL(self, endPoint):
        from urllib.parse import urljoin
        return urljoin(self.datasetUrl, endPoint)

    def EP(self, path):
        return "/connections/dataset/" + self.dataName + "/" + path

    def getTickers(self):
        if self.tickerList:
            return

        tickerDataUrl           = self.makeURL(self.EP("tickers"))
        self.tickerList         = POST(tickerDataUrl, args={
            "version": self.version,
            "client_id": self.clientId,
            "auth_token": self.authToken,
        })

        if self.tickerList is None:
            self.tickerList     = {
                "count"         : 0
            }

        self.numTickers         = self.tickerList['count']
        self.allTickers         = []
        
        for i in range(self.numTickers):
            self.allTickers.append(self.tickerList['items'][i]['id'])

    def getData(self, date = None):
        if self.data:
            return self.data

        self.getTickers()

        requestData = {
            "tickers": self.allTickers
        }

        if date is not None:
            startDate = dateToStr(date)
            endDate = dateToStr(date + timedelta(1))
            requestData["filters"] = [
                { 'column': 'date_added', 'value': startDate, "type": ">=" },
                { 'column': 'date_added', 'value': endDate, "type": "<" }
                # { 'column': 'date_added', 'value': startDate, "type": ">=" },
                # { 'column': 'date_added', 'value': endDate, "type": "<=" }
            ]

        requestDataJson = json.dumps(requestData)
        dataUrl = self.makeURL(self.EP("query/new"))

        tickerIter = 0

        self.data = None

        print("Total ticker: %d" % (self.numTickers))

        while tickerIter < self.numTickers:
            limit = min(self.maxLimit, self.numTickers - tickerIter)

            print("Get range: [%d, %d)" % (tickerIter, tickerIter + limit)) 

            iterData = POST(dataUrl, args = {
                "version": self.version,
                "client_id": self.clientId,
                "auth_token": self.authToken,
                "request": requestDataJson,
                "start": tickerIter,
                "limit": limit,
            })

            if iterData is None:
                iterData = {
                    "count": 0
                }

            print("Count: %d" % (iterData["count"]))

            if iterData["count"] == 0:
                print("No more data")
                break

            if self.data is None:
                self.data = iterData
            else:
                self.data["items"]["rows"].extend(iterData["items"]["rows"])
                self.data["count"] += iterData["count"]
                self.data["total"] += iterData["total"]

            tickerIter += iterData["count"]

        return self.data

    def writeData(self, currDate = None):
        data = self.getData(date = currDate)

        if data is None:
            data = {}

        if currDate is None:
            currDate = date.today()

        filename = os.path.join(self.dstDir, dateToStr(currDate) + ".json")
        print("Writing filename: %s" % (filename))
        os.makedirs(os.path.dirname(filename), exist_ok = True)
        file = open(filename, "w")
        file.write(json.dumps(data))
        file.close()

authData = POST("https://data.thinknum.com/api/authorize", args = {
    "version": "20151130",
    "client_id": "dcf0a1842a4b2bcf44de",
    "client_secret": "454a0fa9bf70b752a8c18115558ea57620e52052",
    "response_type": "json"
})

authToken = authData["auth_token"]
assert authToken, "Unable to retrieve auth token"

def parseArguments():
    parser = OptionParser()

    parser.add_option("-d", "--data", dest = "dataName", help = "The name of the data", default = None)
    parser.add_option("-S", "--startDate", dest = "startDate", help = "The date on which to get the data. Otherwise its the current date", default = None)
    parser.add_option("-E", "--endDate", dest = "endDate", help = "The date on which to get the data. Otherwise its the current date", default = None)
    # parser.add_option("-e", "--email", dest = "email", default = None, help = "Email adderess to send to")
        
    return parser.parse_args()


opt, args = parseArguments()

assert opt.dataName, "Invalid data name given in input! Please give a valid name e.g. linkedin_company, social_twitter etc"
socialData = Thinknum(authToken, opt.dataName)

if opt.startDate is None:
    socialData.writeData()
else:
    if opt.endDate is None or opt.startDate == opt.endDate:
        socialData.writeData(currDate = strToDate(opt.startDate))
    else:
        import pandas as pd
        from pandas.tseries.offsets import BDay

        print("Fetching %s data in range: %s - %s" % (opt.dataName, opt.startDate, opt.endDate))
        startDate = DT.strptime(opt.startDate, "%Y%m%d")
        endDate = DT.strptime(opt.endDate, "%Y%m%d")
        dates = pd.date_range(startDate, endDate, freq = BDay())
        print(dates)

        for date in dates:
            print("Fetching date: %s" % (str(date)))
            socialData = Thinknum(authToken, opt.dataName)
            socialData.writeData(currDate = date)
