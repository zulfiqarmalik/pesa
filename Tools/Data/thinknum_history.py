### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
from os import listdir
from os.path import isfile, join
import csv
import datetime as DT
import json

srcDir = "X:\\Thirdparty\\Thinknum\\history\\government_contract"
dstDir = "X:\\Thirdparty\\Thinknum\\daily\\government_contract"
filenames = [f for f in listdir(srcDir) if isfile(join(srcDir, f))]

def strToDate(date, sep = "-"):
    return DT.datetime.strptime(date, "%Y" + sep + "%m" + sep + "%d")

def dateToStr(date, sep = "-"):
    return str(date.year).zfill(4) + sep + str(date.month).zfill(2) + sep + str(date.day).zfill(2)
    
def dateToUTCStr(date):
    return str(date.year).zfill(4) + str(date.month).zfill(2) + str(date.day).zfill(2) + "000000Z"
    
data = {}
fields = []
# fields = [{
#     "type": "string",
#     "id": "dataset__entity__entity_ticker__ticker__ticker"
# }, {
#     "type": "string",
#     "id": "dataset__entity__name"
# }, {
#     "type": "serial",
#     "id": "id"
# }, {
#     "type": "date",
#     "id": "as_of_date"
# }, {
#     "type": "string",
#     "id": "company_name"
# }, {
#     "type": "number",
#     "id": "followers_count"
# }, {
#     "type": "number",
#     "id": "employees_on_platform"
# }, {
#     "type": "string",
#     "id": "link"
# }, {
#     "type": "string",
#     "id": "industry"
# }, {
#     "type": "date",
#     "id": "date_added"
# }, {
#     "type": "string",
#     "id": "dataset__entity__entity_ticker__ticker__market_sector"
# }, {
#     "type": "string",
#     "id": "dataset__entity__entity_ticker__ticker__market_industry"
# }]

for fname in filenames:
    filename = os.path.join(srcDir, fname)
    print("Handling file: %s" % (filename))
    header = None

    with open(filename, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter = ',')

        for row in reader:
            if header is None:
                header = row
                for fieldName in header:
                    fields.append({
                        "id": fieldName
                    })
            else:
                sdate = row[3].split(' ')[0]
                date = strToDate(sdate)
                sdateUTC = dateToUTCStr(date)
                if sdate not in data:
                    data[sdate] = {
                        "header": header,
                        "count": 0,
                        "items": {
                            "fields": fields,
                            "rows": []
                        }
                    }

                dateData = data[sdate]
                dateData["count"] += 1
                row[3] = sdateUTC
                dateData["items"]["rows"].append(row)


for date in data.keys():
    dstFilename = os.path.join(dstDir, date + ".json")
    dateData = data[date]
    print("Writing date: %s [%d]" % (date, dateData["count"]))
    file = open(dstFilename, "w")
    file.write(json.dumps(dateData))
    file.close()
