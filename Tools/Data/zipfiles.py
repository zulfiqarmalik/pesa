### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import zipfile
import pathlib

srcDir = "D:/GoogleDrive/QuanVolve/Thirdparty/Axioma/Backfill/AxiomaRiskModels-FlatFiles/2018/05"
dstDir = None #"X:/devwork/data/Vendors/Markit/QSG"
deleteSource = dstDir is None

os.chdir(srcDir)

for folder, subfolders, filenames in os.walk('./'):
    for filename in filenames:
        if dstDir is not None:
            fileDir = os.path.join(dstDir, folder) 
        else:
            fileDir = os.path.join(srcDir, folder) 

        zipFilename = os.path.join(fileDir, filename + ".zip")

        if filename.endswith('.zip'):
            print("Ignoring zip file: %s" % (filename))
            continue

        if os.path.isfile(zipFilename):
            print("Exists: %s" % (zipFilename))
            continue

        print("%s => %s" % (filename, zipFilename))

        pathlib.Path(fileDir).mkdir(parents = True, exist_ok = True)

        zipFile = zipfile.ZipFile(zipFilename, 'w')
        zipFile.write(os.path.join(folder, filename), filename, compress_type = zipfile.ZIP_DEFLATED)
        zipFile.close()

        if deleteSource is True:
            os.remove(os.path.join(srcDir, folder, filename))
         
