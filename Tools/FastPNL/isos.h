/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
namespace isos {

class IsOs
{
	std::vector<int> m_isMask;
	std::vector<int> m_osMask;

	void check()
	{
		std::set<int> isSet;
		std::set<int> osSet;
		for (auto &x: m_isMask)
			isSet.insert(x);
		for (auto &x: m_osMask)
			osSet.insert(x);
		for (auto &x: m_isMask)
			if (osSet.find(x) != osSet.end())
				std::cerr << std::endl << "IS OS INTERSECT" << std::endl;
		for (auto &x: m_osMask)
			if (isSet.find(x) != isSet.end())
				std::cerr << std::endl << "IS OS INTERSECT" << std::endl;
	}

	static constexpr bool justEnd = 0;

public:

	const std::vector<int>& isMask() const
	{
		return m_isMask;
	}

	const std::vector<int>& osMask() const
	{
		return m_osMask;
	}

	void init(int size)
	{
		std::vector<int> v(size);
		for (int i = 0; i < size; i++) 
			v[i] = i;
		if (justEnd)
		{
			std::vector<int> endOsMask(v.end() - 100, v.end());
			v.resize(v.size() - 100);
			init(v);
			m_osMask = endOsMask;
		}
		else
			init(v);
	}
	void init(const std::vector<int> &idx)
	{
		m_isMask.clear();
		m_osMask.clear();
		int days = 42;
		int chunkSize = days + rand() % days;
		int chunkNum = idx.size() / chunkSize;
		int isChunks = int(0.75 * chunkNum);
		if ((!isChunks) || (isChunks == chunkNum))
			std::cerr << "EMPTY IS OS" << std::endl;
		auto isSet = math::randomSubset(isChunks, chunkNum);
		for (int i = 0; i < idx.size(); i++)
		{
			if (isSet.find(i / chunkSize) != isSet.end())
				m_isMask.push_back(idx[i]);
			else
				m_osMask.push_back(idx[i]);
		}
		check();
	}
};

}