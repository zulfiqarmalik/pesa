/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#include "pnl.h"
#include "isos.h"
#include "rankFuncs.h"
#include "rand.h"
#include "mathMatr.h"

std::vector<View> getViews(const pnl::Pnls& pnls, const std::vector<int>& mask)
{
	std::vector<View> res;
	res.reserve(pnls.size());
	for (int i = 0; i < pnls.size(); i++)
		res.emplace_back(pnls.pnl(i), mask);
	return res;
}

struct Stats
{
	float isIr;
	float osIr;
	Stats() : isIr(std::nan("")), osIr(std::nan("")) {}
	Stats(float x, float y) : isIr(x), osIr(y) {}
};

class PnlLearner
{

	void getFitness(
		const pnl::Pnls& pnls, 
		const std::vector<int>& isMask
		)
	{
		f.resize(ww.size());
		for (int i = 0; i < ww.size(); i++)
			f[i] = evaluate(pnls, isMask, ww[i], math::Calc::ir);
	}

	float evaluate(
		const pnl::Pnls& pnls, 
		const std::vector<int>& mask, 
		const std::vector<float> &weights,
		math::Calc mode = math::Calc::ir)
	{
		weightedPnl.assign(pnls.pnlSize(), 0);
		for (int j = 0; j < pnls.size(); j++)
			if (weights[j] != 0.0f)
				for (int i = 0; i < weightedPnl.size(); i++)
					weightedPnl[i] += weights[j] * pnls.pnl(j)[i];
		return math::calc(View(weightedPnl, mask), mode);
	}

	void runCenter(
		const pnl::Pnls& pnls, 
		const std::vector<int>& isMask, 
		std::vector<float> &weights)
	{


		std::vector<float> w(weights);
		std::vector<float> center(weights);
		int n = 0;
		float irBound = evaluate(pnls, isMask, weights) + 0.001f;
		float radius = 1.0f;
		for (int iter = 0; iter < 1e+3; iter++)
		{
			//radius *= 1.0 - 1.0 / (10 + 3 * iter);
			for (int i = 0; i < weights.size(); i++)
				w[i] = radius * rb.nextFloat() + center[i];
			float ir = evaluate(pnls, isMask, w);
			std::cout << ir << " " << irBound << std::endl;
			if (ir > irBound)
			{
				irBound = ir + 0.001f;
				weights = w;
				// int N = n;
				// for (int i = 0; i < weights.size(); i++)
				// 	weights[i] = (N * weights[i] + sign * w[i]) / (N + 1);
				n++;
			}
			// float driftParam = 0.1f;
			// float drift = (1.0 - exp(-5 * ir * sign)) * driftParam;
			// for (int i = 0; i < weights.size(); i++)
			// {
			// 	center[i] = (1.0f - drift) * center[i] + drift * sign * w[i];
			// }
			if (n == 10)
			{
				std::cout << iter << std::endl;
				break;
			}
		}
		//std::cout << n << std::endl;
		//weights = center;
		// if (!n)
		// {
		// 	std::cout << "n is zero" << std::endl;
		// 	weights.assign(weights.size(), -1);
		// }
	}

	static void filter(const std::vector<View>& isViews, std::vector<float> &weights)
	{
		std::vector<std::vector<float>> res(2);
		for (auto &c : res)
			c.resize(isViews.size());
		for (int i = 0; i < isViews.size(); i++)
		{
			auto& view = isViews[i];
			res[0][i] = fabs(math::calc(view, math::Calc::ir));
			res[1][i] = 1.0 / math::calc(view, math::Calc::std);
		}
		for (auto &c : res)
			rank::rank(c, 0);
		for (int i = 0; i < isViews.size(); i++)
			weights[i] = res[0][i] + 2.0f * res[1][i];
	    rank::rank(weights, 0);
	    float cut = 0.1f;
	    for (int i = 0; i < weights.size(); i++)
	    {
	        if (weights[i] > 1.0 - cut)
	            weights[i] = math::calc(isViews[i], math::Calc::sign);
	        else
	            weights[i] = 0;
	    }
	}

	static void adjust(const std::vector<View>& isViews, std::vector<float> &weights)
	{
		std::vector<int> idx;
		std::vector<float> signs;
		std::vector<View> views;
	    for (int i = 0; i < weights.size(); i++)
	    	if (weights[i] != 0.0f)
	    	{
	    		pb(idx, i);
	    		pb(views, isViews[i]);
	    		pb(signs, math::calc(util::last(views), math::Calc::sign));
	    	}
	    float correps = 0.01f;
	    auto matr = math::covMatrix(views, math::Calc::signcorr);
	    for (int i = 0; i < matr.size(); i++)
	    	for (int j = 0; j < matr[i].size(); j++)
	    			matr[i][j] *= correps;
	    auto matr2 = math::mult(matr, matr);
	    for (int i = 0; i < matr.size(); i++)
	    	for (int j = 0; j < matr[i].size(); j++)
	    		matr[i][j] = (i == j) - matr[i][j] + matr2[i][j];

	    std::vector<float> w(matr.size(), 1);
	    w = math::mult(matr, w);
	    for (int i = 0; i < matr.size(); i++)
	    	weights[idx[i]] = signs[i] * (w[i] > 0 ? w[i] : 0);
	}

	static constexpr bool SAVE = 1;

	// static constexpr int POPSIZE = 300;
	// static constexpr int ITER = 320;
	// static constexpr int NONZEROW = 100;

	int POPSIZE;
	int ITER;
	int NONZEROW;

	static constexpr float QUANTILE = 0.75f;
	static constexpr float MUTATION = 0.4f;

	randbytes::RandBytes rb;
	std::vector<std::vector<float>> ww;
	std::vector<float> f;
	std::vector<float> ff;
	std::vector<float> weightedPnl;
	std::vector<int> g;

public:

	void init(int gaPopsize, int gaIter, int gaNonzero)
	{
		POPSIZE = gaPopsize;
		ITER = gaIter;
		NONZEROW = gaNonzero;
		std::cout << "Init with nonzero " << NONZEROW << ", popsize " << POPSIZE << " and iters " << ITER << std::endl;
	}

	void saveOS(const std::vector<float> &osir, std::string fileName)
	{
		std::cout << std::endl << "Saving OS values to " << fileName << std::endl;
		std::ofstream ofs(fileName);
		for (auto &os : osir)
			ofs << "OS " << os << std::endl;
	}

	void finalize(std::vector<std::vector<Stats>> results)
	{
		std::vector<Stats> finalres;
		for (auto &x : results)
			finalres.insert(finalres.end(), x.begin(), x.end());
		std::vector<float> isir;
		std::vector<float> osir;
		for (auto &x : finalres)
		{
			pb(isir, x.isIr);
			pb(osir, x.osIr);
		}
		if (SAVE)
			saveOS(osir, "f1");
		std::cout << std::endl;
		float isres = math::hodgesLehmannSen(isir);
		std::cout << "IS fit: " << isres << std::endl;
		float res = math::hodgesLehmannSen(osir);
		std::sort(osir.begin(), osir.end());
		float q25 = osir[osir.size() / 4];
		float q75 = osir[3 * osir.size() / 4];
		std::cout << "OS result: " << res << " - " << (res - q25) << " + " << (q75 - res) << std::endl;
	}

	Stats learn(const pnl::Pnls& pnls, const isos::IsOs& isos, std::vector<float> &weights)
	{
		std::vector<float> wCenter(weights.size(), 0);
		std::vector<float> wBasic(weights.size(), 0);
		auto isViews = getViews(pnls, isos.isMask());
		filter(isViews, wBasic);
		adjust(isViews, wBasic);
		wCenter = wBasic;
		runCenter(pnls, isos.isMask(), wCenter);
		float lambda = 0.0f;
		for (int i = 0; i < weights.size(); i++)
			weights[i] = (1 - lambda) * wCenter[i] + lambda * wBasic[i];
		float isIr = evaluate(pnls, isos.isMask(), weights);
		float osIr = evaluate(pnls, isos.osMask(), weights);
		return Stats(isIr, osIr);
	}

};