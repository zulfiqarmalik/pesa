/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
namespace {

std::vector<View> getViews(const pnl::Pnls& pnls, const std::vector<int>& mask)
{
	std::vector<View> res;
	res.reserve(pnls.size());
	for (int i = 0; i < pnls.size(); i++)
		res.emplace_back(pnls.pnl(i), mask);
	return res;
}

}

struct Stats
{
	float isIr;
	float osIr;
	Stats() : isIr(std::nan("")), osIr(std::nan("")) {}
	Stats(float x, float y) : isIr(x), osIr(y) {}
};

template<class T>
void getCumPnl(int days, const T& t, std::vector<float>& res)
{
	res.clear();
	float c = 0;
	for (int i = 0; i < t.size(); i++)
	{
		c += t[i];
		if (i % days == 0)
			pb(res, c);
	}
}

class PnlLearner
{
	std::vector<std::vector<float>> x;
	std::vector<float> y;
	std::vector<float> coeff;

	static std::vector<std::vector<float>> getFactors(const std::vector<View>& isViews)
	{
		std::vector<std::vector<float>> res(FSIZE);
		for (auto &c : res)
			c.resize(isViews.size());

		std::vector<float> cumPnl;
		int days = 21;
		for (int i = 0; i < isViews.size(); i++)
		{
			auto& view = isViews[i];
			float ir = math::calc(view, math::Calc::ir);
			int sign = math::sign(ir);
			res[0][i] = fabs(ir);
			res[1][i] = 1.0 / math::calc(view, math::Calc::std);
			getCumPnl(days, view, cumPnl);
			res[2][i] = math::regrTstat(cumPnl) * sign;
		}
		for (auto &c : res)
			rank::rank(c, 0);
		return res;
	}

	static void filter(const std::vector<View>& isViews, std::vector<float> &weights)
	{
	    rank::rank(weights, 0);
	    float cut = 0.1f;
	    for (int i = 0; i < weights.size(); i++)
	    {
	        if (weights[i] > 1.0 - cut)
	            weights[i] = math::calc(isViews[i], math::Calc::sign);
	        else
	            weights[i] = 0;
	    }
	}

	static void adjust(const std::vector<View>& isViews, std::vector<float> &weights)
	{
		float correps = 0.0f;
		if (correps == 0.0f)
			return;
		std::vector<int> idx;
		std::vector<float> signs;
		std::vector<View> views;
	    for (int i = 0; i < weights.size(); i++)
	    	if (weights[i] != 0.0f)
	    	{
	    		pb(idx, i);
	    		pb(views, isViews[i]);
	    		pb(signs, math::calc(util::last(views), math::Calc::sign));
	    	}
	    auto matr = math::covMatrix(views, math::Calc::signcorr);
	    for (int i = 0; i < matr.size(); i++)
	    	for (int j = 0; j < matr[i].size(); j++)
	    			matr[i][j] *= correps;

	    for (int i = 0; i < matr.size(); i++)
	    	for (int j = 0; j < matr[i].size(); j++)
	    		matr[i][j] = (i == j) - matr[i][j];

	    std::vector<float> w(matr.size(), 1);
	    w = math::mult(matr, w);
	    for (int i = 0; i < matr.size(); i++)
	    	weights[idx[i]] = signs[i] * (w[i] > 0 ? w[i] : 0);
	}

	float evaluate(const pnl::Pnls& pnls, const std::vector<int>& mask, const std::vector<float> &weights) const
	{
		std::vector<float> weightedPnl(pnls.pnlSize(), 0);
		for (int i = 0; i < weightedPnl.size(); i++)
			for (int j = 0; j < pnls.size(); j++)
				weightedPnl[i] += weights[j] * pnls.pnl(j)[i];
		return math::calc(View(weightedPnl, mask), math::Calc::ir);
	}

	static constexpr bool SAVE = 1;
	static constexpr int FSIZE = 3;

public:

	PnlLearner() : x(FSIZE), coeff(FSIZE, 0) 
	{
		coeff = { 0, 2, 1 };
	}

	void saveOS(const std::vector<float> &osir, std::string fileName)
	{
		std::cout << std::endl << "Saving OS values to " << fileName << std::endl;
		std::ofstream ofs(fileName);
		for (auto &os : osir)
			ofs << "OS " << os << std::endl;
	}

	void finalize(std::vector<std::vector<Stats>> results)
	{
		std::vector<Stats> finalres;
		for (auto &x : results)
			finalres.insert(finalres.end(), x.begin(), x.end());
		std::vector<float> isir;
		std::vector<float> osir;
		for (auto &x : finalres)
		{
			pb(isir, x.isIr);
			pb(osir, x.osIr);
		}
		if (SAVE)
			saveOS(osir, "f1");
		std::cout << std::endl;
		float isres = math::hodgesLehmannSen(isir);
		std::cout << "IS fit: " << isres << std::endl;
		float res = math::hodgesLehmannSen(osir);
		std::sort(osir.begin(), osir.end());
		float q25 = osir[osir.size() / 4];
		float q75 = osir[3 * osir.size() / 4];
		std::cout << "OS result: " << res << " - " << (res - q25) << " + " << (q75 - res) << std::endl;
	}

	Stats learn(const pnl::Pnls& pnls, const isos::IsOs& isos, std::vector<float> &weights)
	{
		auto isViews = getViews(pnls, isos.isMask());
		auto osViews = getViews(pnls, isos.osMask());
		auto f = getFactors(isViews);
		weights = math::mult(coeff, f);
		filter(isViews, weights);
		adjust(isViews, weights);
		float isIr = evaluate(pnls, isos.isMask(), weights);
		float osIr = evaluate(pnls, isos.osMask(), weights);
		return Stats(isIr, osIr);
	}

};