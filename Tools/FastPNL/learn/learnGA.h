/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#include "pnl.h"
#include "isos.h"
#include "rankFuncs.h"
#include "rand.h"
#include "mathMatr.h"

std::vector<View> getViews(const pnl::Pnls& pnls, const std::vector<int>& mask)
{
	std::vector<View> res;
	res.reserve(pnls.size());
	for (int i = 0; i < pnls.size(); i++)
		res.emplace_back(pnls.pnl(i), mask);
	return res;
}

struct Stats
{
	float isIr;
	float osIr;
	Stats() : isIr(std::nan("")), osIr(std::nan("")) {}
	Stats(float x, float y) : isIr(x), osIr(y) {}
};

class PnlLearner
{

	void getFitness(
		const pnl::Pnls& pnls, 
		const std::vector<int>& isMask
		)
	{
		f.resize(ww.size());
		for (int i = 0; i < ww.size(); i++)
			f[i] = evaluate(pnls, isMask, ww[i], math::Calc::ir);
	}

	float evaluate(
		const pnl::Pnls& pnls, 
		const std::vector<int>& mask, 
		const std::vector<float> &weights,
		math::Calc mode = math::Calc::ir)
	{
		weightedPnl.assign(pnls.pnlSize(), 0);
		for (int j = 0; j < pnls.size(); j++)
			if (weights[j] != 0.0f)
				for (int i = 0; i < weightedPnl.size(); i++)
					weightedPnl[i] += weights[j] * pnls.pnl(j)[i];
		return math::calc(View(weightedPnl, mask), mode);
	}

	void cross(const std::vector<float> &w1, const std::vector<float> &w2, std::vector<float> &w)
	{
		std::vector<int> nonzeroW;
		for (int j = 0; j < w.size(); j++)
			if ((w1[j] != 0.0f) || (w2[j] != 0.0f)) 
				pb(nonzeroW, j);
		std::vector<int> count(nonzeroW.size());
		for (int i = 0; i < count.size(); i++)
			count[i] = i;
		std::set<int> toBeAssigned;
		int csize = count.size();
		for (int i = 0; i < NONZEROW; i++)
		{
			int idx = rb.next() % csize; 
			toBeAssigned.insert(count[idx]);
			std::swap(count[idx], count[--csize]);
		}
		float lambda = 0.5f;
		for (int j = 0; j < w.size(); j++)
			if (toBeAssigned.find(j) != toBeAssigned.end()) 
				do
					w[j] = lambda * w1[j] + (1.0f - lambda) * w2[j] + MUTATION * rb.nextFloat();
				while (w[j] == 0.0f);
			else
				w[j] = 0.0f;
	}

	void runGA(
		const pnl::Pnls& pnls, 
		const std::vector<int>& isMask, 
		std::vector<float> &weights)
	{
		ww.assign(POPSIZE, std::vector<float>(weights.size(), 0.0f));
		for (auto &w: ww)
		{
			std::vector<int> count(w.size());
			int csize = count.size();
			for (int i = 0; i < count.size(); i++)
				count[i] = i;
			for (int i = 0; i < NONZEROW; i++)
			{
				int idx = rb.next() % csize;
				do 
					w[count[idx]] = rb.nextFloat();
				while (w[count[idx]] == 0.0f);
				std::swap(count[idx], count[--csize]);
			}
		}
		getFitness(pnls, isMask);
		ff = f;
		for (int iter = 0; iter < ITER; iter++)
		{
			float cut = math::quantile(&ff, QUANTILE);
			g.clear();
			for (int i = 0; i < ww.size(); i++)
				if (f[i] >= cut)
					pb(g, i);
			for (int i = 0; i < ww.size(); i++)
				if (f[i] < cut)
				{
					int idx1 = rb.next() % g.size();
					int idx2 = rb.next() % g.size();
					cross(ww[g[idx1]], ww[g[idx2]], ww[i]);
				}
			getFitness(pnls, isMask);
			ff = f;
		}
		for (int j = 0; j < weights.size(); j++)
			weights[j] = math::calc(VView(ww, j), math::Calc::mean);
	}

	static void filter(const std::vector<View>& isViews, std::vector<float> &weights)
	{
		std::vector<std::vector<float>> res(2);
		for (auto &c : res)
			c.resize(isViews.size());
		for (int i = 0; i < isViews.size(); i++)
		{
			auto& view = isViews[i];
			res[0][i] = fabs(math::calc(view, math::Calc::ir));
			res[1][i] = 1.0 / math::calc(view, math::Calc::std);
		}
		for (auto &c : res)
			rank::rank(c, 0);
		for (int i = 0; i < isViews.size(); i++)
			weights[i] = res[0][i] + 2.0f * res[1][i];
	    rank::rank(weights, 0);
	    float cut = 0.2f;
	    for (int i = 0; i < weights.size(); i++)
	    {
	        if (weights[i] > 1.0 - cut)
	            weights[i] = math::calc(isViews[i], math::Calc::sign);
	        else
	            weights[i] = 0;
	    }
	}

	static void adjust(const std::vector<View>& isViews, std::vector<float> &weights)
	{
		std::vector<int> idx;
		std::vector<float> signs;
		std::vector<View> views;
	    for (int i = 0; i < weights.size(); i++)
	    	if (weights[i] != 0.0f)
	    	{
	    		pb(idx, i);
	    		pb(views, isViews[i]);
	    		pb(signs, math::calc(util::last(views), math::Calc::sign));
	    	}
	    float correps = 0.0f;
	    auto matr = math::covMatrix(views, math::Calc::signcorr);
	    for (int i = 0; i < matr.size(); i++)
	    	for (int j = 0; j < matr[i].size(); j++)
	    			matr[i][j] *= correps;
	    auto matr2 = math::mult(matr, matr);
	    for (int i = 0; i < matr.size(); i++)
	    	for (int j = 0; j < matr[i].size(); j++)
	    		matr[i][j] = (i == j) - matr[i][j] + matr2[i][j];

	    std::vector<float> w(matr.size(), 1);
	    w = math::mult(matr, w);
	    for (int i = 0; i < matr.size(); i++)
	    	weights[idx[i]] = signs[i] * (w[i] > 0 ? w[i] : 0);
	}

	static constexpr bool SAVE = 1;

	static constexpr int POPSIZE = 400;
	static constexpr int ITER = 80; //2560;
	static constexpr int NONZEROW = 250;

	// int POPSIZE;
	// int ITER;
	// int NONZEROW;

	static constexpr float QUANTILE = 0.75f;
	static constexpr float MUTATION = 0.4f;

	randbytes::RandBytes rb;
	std::vector<std::vector<float>> ww;
	std::vector<float> f;
	std::vector<float> ff;
	std::vector<float> weightedPnl;
	std::vector<int> g;

public:

	// void init(int gaPopsize, int gaIter, int gaNonzero)
	// {
	// 	POPSIZE = gaPopsize;
	// 	ITER = gaIter;
	// 	NONZEROW = gaNonzero;
	// 	std::cout << "Init with nonzero " << NONZEROW << ", popsize " << POPSIZE << " and iters " << ITER << std::endl;
	// }

	void saveOS(const std::vector<float> &osir, std::string fileName)
	{
		std::cout << std::endl << "Saving OS values to " << fileName << std::endl;
		std::ofstream ofs(fileName);
		for (auto &os : osir)
			ofs << "OS " << os << std::endl;
	}

	void finalize(std::vector<std::vector<Stats>> results)
	{
		std::vector<Stats> finalres;
		for (auto &x : results)
			finalres.insert(finalres.end(), x.begin(), x.end());
		std::vector<float> isir;
		std::vector<float> osir;
		for (auto &x : finalres)
		{
			pb(isir, x.isIr);
			pb(osir, x.osIr);
		}
		if (SAVE)
			saveOS(osir, "f1");
		std::cout << std::endl;
		float isres = math::hodgesLehmannSen(isir);
		std::cout << "IS fit: " << isres << std::endl;
		float res = math::hodgesLehmannSen(osir);
		std::sort(osir.begin(), osir.end());
		float q25 = osir[osir.size() / 4];
		float q75 = osir[3 * osir.size() / 4];
		std::cout << "OS result: " << res << " - " << (res - q25) << " + " << (q75 - res) << std::endl;
	}

	Stats learn(const pnl::Pnls& pnls, const isos::IsOs& isos, std::vector<float> &weights)
	{
		std::vector<float> wGA(weights.size(), 0);
		std::vector<float> wBasic(weights.size(), 0);
		//runGA(pnls, isos.isMask(), wGA);
		auto isViews = getViews(pnls, isos.isMask());
		filter(isViews, wBasic);
		//adjust(isViews, wBasic);
		float lambda = 0.1f;
		for (int i = 0; i < weights.size(); i++)
			weights[i] = (1 - lambda) * wGA[i] + lambda * wBasic[i];
		float isIr = evaluate(pnls, isos.isMask(), weights);
		float osIr = evaluate(pnls, isos.osMask(), weights);
		return Stats(isIr, osIr);
	}

};