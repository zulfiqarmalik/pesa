/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#include "pnl.h"
#include "isos.h"
#include "rankFuncs.h"
#include "rand.h"

struct Stats
{
	float isIr;
	float osIr;
	Stats() : isIr(std::nan("")), osIr(std::nan("")) {}
	Stats(float x, float y) : isIr(x), osIr(y) {}
};

class PnlLearner
{

	void getFitness(
		const pnl::Pnls& pnls, 
		const std::vector<int>& isMask
		)
	{
		f.resize(ww.size());
		for (int i = 0; i < ww.size(); i++)
			f[i] = evaluate(pnls, isMask, ww[i], math::Calc::ir);
	}

	float evaluate(
		const pnl::Pnls& pnls, 
		const std::vector<int>& mask, 
		const std::vector<float> &weights,
		math::Calc mode = math::Calc::ir)
	{
		weightedPnl.assign(pnls.pnlSize(), 0);
		for (int i = 0; i < weightedPnl.size(); i++)
			for (int j = 0; j < pnls.size(); j++)
				weightedPnl[i] += weights[j] * pnls.pnl(j)[i];
		return math::calc(View(weightedPnl, mask), mode);
	}

	void runGA(		
		const pnl::Pnls& pnls, 
		const std::vector<int>& isMask, 
		std::vector<float> &weights)
	{
		ww.assign(POPSIZE, std::vector<float>(weights.size()));
		for (auto &w: ww)
			for (auto &x : w)
				x = rb.nextFloat();
		getFitness(pnls, isMask);
		ff = f;
		for (int iter = 0; iter < ITER; iter++)
		{
			float cut = math::quantile(&ff, QUANTILE);
			g.clear();
			for (int i = 0; i < ww.size(); i++)
				if (f[i] >= cut)
					pb(g, i);
			for (int i = 0; i < ww.size(); i++)
				if (f[i] < cut)			
				{
					int idx1 = rb.next() % g.size();
					int idx2 = rb.next() % g.size();
					const std::vector<float> &w1 = ww[idx1];
					const std::vector<float> &w2 = ww[idx2];
					float lambda = 0.5f;
					for (int j = 0; j < ww[i].size(); j++)
						ww[i][j] = lambda * w1[j] + (1.0f - lambda) * w2[j] + MUTATION * rb.nextFloat();
				}
			getFitness(pnls, isMask);
			ff = f;
		}
		for (int j = 0; j < weights.size(); j++)
			weights[j] = math::calc(VView(ww, j), math::Calc::mean);

	}

	static constexpr bool SAVE = 1;
	
	//static constexpr int POPSIZE=160;
	//static constexpr int ITER=160;

	int POPSIZE;
	int ITER;

	static constexpr float QUANTILE = 0.75f;
	static constexpr float MUTATION = 0.5f;
	randbytes::RandBytes rb;
	std::vector<std::vector<float>> ww;
	std::vector<float> f;
	std::vector<float> ff;
	std::vector<float> weightedPnl;
	std::vector<int> g;

public:

	void init(int gaPopsize, int gaIter)
	{
		std::cout << "Init with popsize " << gaPopsize << " and iters " << gaIter << std::endl;
		POPSIZE = gaPopsize;
		ITER = gaIter;
	}

	void saveOS(const std::vector<float> &osir, std::string fileName)
	{
		std::cout << std::endl << "Saving OS values to " << fileName << std::endl;
		std::ofstream ofs(fileName);
		for (auto &os : osir)
			ofs << "OS " << os << std::endl;
	}

	void finalize(std::vector<std::vector<Stats>> results)
	{
		std::vector<Stats> finalres;
		for (auto &x : results)
			finalres.insert(finalres.end(), x.begin(), x.end());
		std::vector<float> isir;
		std::vector<float> osir;
		for (auto &x : finalres)
		{
			pb(isir, x.isIr);
			pb(osir, x.osIr);
		}
		if (SAVE)
			saveOS(osir, "f1");
		std::cout << std::endl;
		float isres = math::hodgesLehmannSen(isir);
		std::cout << "IS fit: " << isres << std::endl;
		float res = math::hodgesLehmannSen(osir);
		std::sort(osir.begin(), osir.end());
		float q25 = osir[osir.size() / 4];
		float q75 = osir[3 * osir.size() / 4];
		std::cout << "OS result: " << res << " - " << (res - q25) << " + " << (q75 - res) << std::endl;
	}

	Stats learn(const pnl::Pnls& pnls, const isos::IsOs& isos, std::vector<float> &weights)
	{
		runGA(pnls, isos.isMask(), weights);
		float isIr = evaluate(pnls, isos.isMask(), weights);
		float osIr = evaluate(pnls, isos.osMask(), weights);
		return Stats(isIr, osIr);
	}

};