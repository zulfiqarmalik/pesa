/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

namespace math 
{

template<class T>
std::vector<std::vector<float>> covMatrix(const T& a, math::Calc mode = math::Calc::cov)
{
	std::vector<std::vector<float>> res(a.size(), std::vector<float>(a.size()));
	for (int i = 0; i < a.size(); i++)
		for (int j = 0; j <= i; j++)
		{
			res[i][j] = math::calc(a[i], mode, &(a[j]));
			res[j][i] = res[i][j];
		}
	return res;
}

std::vector<float> mult(const std::vector<float> &v, const std::vector<std::vector<float>> &a)
{
	std::vector<float> res(a[0].size());
	for (int i = 0; i < res.size(); i++)
		res[i] = math::dot(VView(a, i), v);
	return res;
}

std::vector<float> mult(const std::vector<std::vector<float>> &a, const std::vector<float> &v)
{
	std::vector<float> res(a.size());
	for (int i = 0; i < res.size(); i++)
		res[i] = math::dot(a[i], v);
	return res;
}

std::vector<std::vector<float>> mult(const std::vector<std::vector<float>> &a, const std::vector<std::vector<float>> &b)
{
	std::vector<std::vector<float>> res(a.size(), std::vector<float>(b[0].size()));
	for (int i = 0; i < res.size(); i++)
		for (int j = 0; j < res[i].size(); j++)
			res[i][j] = math::dot(a[i], VView(b, j));
	return res;
}

template<class T>
std::vector<std::vector<float>> diag(const T& a,  std::vector<float> *values = nullptr)
{
	std::vector<std::vector<float>> res(a.size(), std::vector<float>(a.size()));
	if (values)
		values -> resize(a.size());
	for (int i = 0; i < res.size(); i++)
	{
		//init vector
		for (int j = 0; j < res[i].size(); j++)
			res[i][j] = rand();
		for (int k = 0; k < i; k++)
			math::makeOrth(res[i], res[k]);
		for (int iter = 0; iter < 1000; iter++)
		{
			float norm = math::norm(res[i]);
			for (int j = 0; j < res[i].size(); j++)
				res[i][j] /= norm;
			auto multResI = math::mult(a, res[i]);
			for (int k = 0; k < i; k++)
				math::makeOrth(multResI, res[k]);
			float dotProd = math::dot(multResI, res[i]);
			float cosVal = dotProd / math::norm(multResI);
			float sinSqVal = 1 - cosVal * cosVal;
			if ((fabs(dotProd) < 1e-4) || (sinSqVal < 1e-4))
			{
				if (values)
					(*values)[i] = dotProd;
				break;
			}
			res[i] = multResI;
		}
	}
	return res;
}

template<class T>
std::vector<std::vector<float>> invSymm(const T& a)
{
	std::vector<float> values;
	auto eigenVecs = diag(a, &values);
	for (int i = 0; i < values.size(); i++)
		if (fabs(values[i]) < 1e-5)
			std::cerr << "Values too small" << std::endl;
		else
			values[i] = 1.0 / values[i];
	std::vector<std::vector<float>> res(a.size(), std::vector<float>(a.size(), 0));
	for (int i = 0; i < res.size(); i++)
		for (int j = 0; j < res.size(); j++)
			for (int k = 0; k < res.size(); k++)
				res[i][j] += values[k] * eigenVecs[k][i] * eigenVecs[k][j];
	return res;
}

void minimizeSq(
	std::vector<float>& solution, 
	const std::vector<std::vector<float>>& sqMatrix, 
	const std::vector<float>& constraint,
	double tolerance
	)
{
	auto grad = mult(sqMatrix, solution);
	double val = dot(solution, grad);
	auto newgrad = grad;
	while (1)
	{
		makeOrth(newgrad, constraint);
		double denom = dot(newgrad, math::mult(sqMatrix, newgrad));
		double newMult = double(dot(newgrad, grad)) / denom;
		for (int i = 0; i < solution.size(); i++)
			solution[i] -= newMult * newgrad[i];
		grad = mult(sqMatrix, solution);
		double newval = dot(solution, grad);
		double diff = fabs(newval - val) / (fabs(newval) + fabs(val));
		val = newval;
		if (diff < tolerance)
			break;
		newgrad = grad;
	}
}



}