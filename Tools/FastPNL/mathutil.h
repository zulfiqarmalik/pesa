/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#include <iostream>
#include <cmath>
#include <cfloat>

namespace math {

template<class T>
int sign(T x)
{
	return x >= 0 ? 1 : -1;
}

template<class T>
float minmax(const T& t, int sign)
{
	if (!t.size())
		return std::nan("");
	float res = t[0];
	for (int i = 1; i < t.size(); i++)
		if (sign * t[i] > sign * res)
			res = t[i];
	return res;
}

template<class T>
float max(const T& t)
{
	return minmax(t, 1);
}

template<class T>
float min(const T& t)
{
	return minmax(t, -1);
}

enum class Calc { sign, sum, mean, std, var, ir, decay, cov, corr, regr, dot, signcorr, signcov };

template<class T>
float calc(const T &pnl, Calc mode, const T *other = nullptr )
{
	int n = 0;
	float sx = 0, sxx = 0, sy = 0, sxy = 0, syy = 0;
	float x = 0, y = 0, d = 0;
	for (int i = 0; i < pnl.size(); i++)
	{
		x = pnl[i];
		n++;
		d += i * x;
		sx += x;
		sxx += x * x;
		if (other)
		{
			y = (*other)[i];
			sy += y; 
			sxy += x * y;
			syy += y * y;
		}
	}
	if (!n)
		return 0;
	sx /= n;
	sxx /= n;
	sxx -= sx * sx;
	sxx = std::max(sxx, 0.0f);
	sxx = sqrt(sxx);
	switch(mode)
	{
		case Calc::sign : return sign(sx);
		case Calc::ir : return sx / sxx;
		case Calc::sum : return sx * n;
		case Calc::mean : return sx;
		case Calc::std : return sxx;
		case Calc::var : return sxx * sxx;
		case Calc::decay : return d;
		default : {}
	}
	if (!other)
		return 0;
	{
		sy /= n; 
		syy /= n;
		syy -= sy * sy;
		syy = std::max(syy, float(0.0));
		syy = sqrt(syy);
		sxy /= n;
		switch(mode) 
		{
			case Calc::dot : return sxy * n;
			case Calc::cov : return sxy - sx * sy;
			case Calc::corr : return (sxy - sx * sy) / (sxx * syy);
			case Calc::signcorr : return sign(sx) * sign(sy) * (sxy - sx * sy) / (sxx * syy);
			case Calc::signcov : return sign(sx) * sign(sy) * (sxy - sx * sy);
			case Calc::regr : return (sxy - sx * sy) * (sxx * sxx);
			default : return 0;
		}
	}
	return 0;
}

template<class T>
float regrTstat(const T& t)
{
	int n = t.size();
	float d = calc(t, Calc::decay) / n;
	float mean = calc(t, Calc::mean);
	float mid = 0.5f * (n - 1);
	float cov = d - mean * mid;
	float xvar = (n - 1) * (5 * n + 1) / 12.0f;
	float a = cov / xvar;
	float nom = 0;
	for (int i = 0; i < t.size(); i++)
		nom += (t[i] - a * i) * (t[i] - a * i);
	nom /= (n - 1);
	float denom = n * xvar;
	float s = std::sqrt(nom / denom);
	return a / s;
}

template<class T1, class T2>
float dot(const T1& t1, const T2& t2)
{
    float res = 0;
    for (int i = 0; i < t1.size(); i++)
            res += t1[i] * t2[i];
    return res;
}

template<class T>
float norm(const T& t)
{
	return std::sqrt(dot(t, t));
}

template<class T1, class T2>
float makeOrth(T1& t1, const T2& t2)
{
	float coeff = dot(t1, t2) / dot(t2, t2);
	for (int i = 0; i < t1.size(); i++)
		t1[i] -= t2[i] * coeff;
}

std::set<int> randomSubset(int m, int n)
{
	std::set<int> s;
	std::set<int> res;
	for (int i = 0; i < n; i++)
		s.insert(i);
	for (int i = 0; i < m; i++)
	{
		int idx = rand() % s.size();
		auto it = s.begin();
		std::advance(it, idx);
		res.insert(*it);
		s.erase(it);
	}
	return res;
}

template<class T>
void getCumSum(const T& t, std::vector<float> *res)
{
	res->resize(t.size());
	(*res)[0] = t[0];
	for (int i = 1; i < t.size(); i++)
		(*res)[i] = (*res)[i - 1] + t[i];
}

float quantile(std::vector<float>* ff, float q)
{
	std::sort(ff->begin(), ff->end());
	return (*ff)[int( q * ff->size())];
}

float median(std::vector<float>* ff)
{
	std::sort(ff->begin(), ff->end());
	return (*ff)[ff->size() / 2];
}

template<class T>
float median(const T& f, bool calcAbs = false)
{
	std::vector<float> ff(f.size());
	for (int i = 0; i < f.size(); i++)
	{
		ff[i] = f[i];
		if (calcAbs)
			ff[i] *= sign(f[i]);
	}
	return median(&ff);
}

template<class T>
float MAD(const T& f, float med = std::nanf("")) 
{
	float m = med;
	if (!std::isfinite(m))
		m = median(f);
	std::vector<float> ff(f.size());
	for (int i = 0; i < f.size(); i++)
		ff[i] = fabs(f[i] - m);
	return median(&ff);
}

float hodgesLehmannSen(const std::vector<float>& f)
{
	std::vector<float> ff;
	int iters = int(f.size() * log(2 + f.size()));
	for (int it = 0; it < iters; it++)
	{
		auto i = rand() % f.size();
		auto j = rand() % f.size();
		ff.push_back(0.5 * (f[i] + f[j]));
	}
	return median(&ff);
}

}