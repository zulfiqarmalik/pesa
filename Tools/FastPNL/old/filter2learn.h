/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#include "pnl.h"
#include "isos.h"
#include "rankFuncs.h"
#include "ols.h"

namespace {

std::vector<View> getViews(const pnl::Pnls& pnls, const std::vector<int>& mask)
{
	std::vector<View> res;
	res.reserve(pnls.size());
	for (int i = 0; i < pnls.size(); i++)
		res.emplace_back(pnls.pnl(i), mask);
	return res;
}

}

struct Stats
{
	float isIr;
	float osIr;
	Stats() : isIr(std::nan("")), osIr(std::nan("")) {}
	Stats(float x, float y) : isIr(x), osIr(y) {}
};

class PnlLearner
{
	std::vector<std::vector<float>> x;
	std::vector<float> y;
	std::vector<float> coeff;

	static std::vector<std::vector<float>> getFactors(const std::vector<View>& isViews)
	{
		std::vector<std::vector<float>> res(FSIZE);
		for (auto &c : res)
			c.resize(isViews.size());

		for (int i = 0; i < isViews.size(); i++)
		{
			auto& view = isViews[i];
			float m = math::calc(view, math::Calc::mean);
			float sign = math::sign(m);

			res[0][i] = fabs(math::calc(view, math::Calc::ir));
			res[1][i] = math::calc(view, math::Calc::std);
		}
		for (auto &c : res)
			rank::rank(c);
		return res;
	}

	void updateFactors(const std::vector<std::vector<float>> &f)
	{
		for (int i = 0; i < FSIZE; i++)
			x[i].insert(x[i].end(), f[i].begin(), f[i].end());
	}

	void updateTarget(const std::vector<View>& isViews, const std::vector<View>& osViews)
	{
		std::vector<float> yvals;
		for (int i = 0; i < isViews.size(); i++)
			yvals.push_back(math::calc(isViews[i], math::Calc::sign) * math::calc(osViews[i], math::Calc::ir));
		rank::rank(yvals);
		y.insert(y.end(), yvals.begin(), yvals.end());
	}

	void filter(const std::vector<std::vector<float>> &f, const std::vector<View>& isViews, std::vector<float> &weights) const
	{
	    weights = math::mult(coeff, f);
	    rank::rank(weights, 0);
	    float cut = 0.1;
	    std::vector<int> res;
	    for (int i = 0; i < weights.size(); i++)
	    {
	        if (weights[i] > 1.0 - cut)
	        {
	            weights[i] = math::calc(isViews[i], math::Calc::sign);
	            res.push_back(i);
	        }
	        else
	            weights[i] = 0;
	    }
	}

	float evaluate(const pnl::Pnls& pnls, const std::vector<int>& mask, const std::vector<float> &weights) const
	{
		std::vector<float> weightedPnl(pnls.pnlSize(), 0);
		for (int i = 0; i < weightedPnl.size(); i++)
			for (int j = 0; j < pnls.size(); j++)
				weightedPnl[i] += weights[j] * pnls.pnl(j)[i];
		return math::calc(View(weightedPnl, mask), math::Calc::ir);
	}

	static constexpr bool LEARN = 0;
	static constexpr int FSIZE = 2;

public:

	PnlLearner() : x(FSIZE), coeff(FSIZE, 0) 
	{
		coeff = { 0.06, -0.04 };
	}

	void finalize(std::vector<std::vector<Stats>> results)
	{
		if (LEARN)
		{
			std::cout << std::endl << "coeff = { ";
			auto cf = ols::learn(x, y);
			for (auto &c: cf)
				std::cout << c << ", ";
			std::cout << "};" << std::endl;
		}

		std::vector<Stats> finalres;
		for (auto &x : results)
			finalres.insert(finalres.end(), x.begin(), x.end());
		std::vector<float> isir;
		std::vector<float> osir;
		for (auto &x : finalres)
		{
			isir.push_back(x.isIr);
			osir.push_back(x.osIr);
		}
		std::cout << std::endl;
		float isres = math::hodgesLehmannSen(isir);
		std::cout << "IS fit: " << isres << std::endl;
		float res = math::hodgesLehmannSen(osir);
		std::sort(osir.begin(), osir.end());
		float q25 = osir[osir.size() / 4];
		float q75 = osir[3 * osir.size() / 4];
		std::cout << "OS result: " << res << " - " << (res - q25) << " + " << (q75 - res) << std::endl;
	}

	Stats learn(const pnl::Pnls& pnls, const isos::IsOs& isos, std::vector<float> &weights)
	{
		auto isViews = getViews(pnls, isos.isMask());
		auto osViews = getViews(pnls, isos.osMask());
		auto f = getFactors(isViews);
		filter(f, isViews, weights);
		float isIr = evaluate(pnls, isos.isMask(), weights);
		float osIr = evaluate(pnls, isos.osMask(), weights);
		if (LEARN)
		{
			updateFactors(f);
			updateTarget(isViews, osViews);
		}
		return Stats(isIr, osIr);
	}

};