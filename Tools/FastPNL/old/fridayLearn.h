/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#include "pnl.h"
#include "isos.h"
#include "rankFuncs.h"
#include "ols.h"

namespace {

std::vector<View> getViews(const pnl::Pnls& pnls, const std::vector<int>& mask)
{
	std::vector<View> res;
	res.reserve(pnls.size());
	for (int i = 0; i < pnls.size(); i++)
		res.emplace_back(pnls.pnl(i), mask);
	return res;
}

}

struct Stats
{
	float isIr;
	float osIr;
	Stats() : isIr(std::nan("")), osIr(std::nan("")) {}
	Stats(float x, float y) : isIr(x), osIr(y) {}
};

class PnlLearner
{
	std::vector<std::vector<float>> x;
	std::vector<std::vector<float>> currX;
	std::vector<float> y;
	std::vector<float> coeff;

	void updateCurrX(const std::vector<View>& isViews)
	{
		if (currX[0].empty())
			for (auto &c : currX)
				c.resize(isViews.size());

		for (int i = 0; i < isViews.size(); i++)
		{
			auto& view = isViews[i];
			float m = math::calc(view, math::Calc::mean);
			float sign = math::sign(m);

			currX[0][i] = fabs(math::calc(view, math::Calc::ir));
			currX[1][i] = math::calc(view, math::Calc::std);
			//currX[0][i] = fabs(rank::WXtest(view));
		}
		for (auto &c : currX)
			rank::rank(c);
	}

	void updateFactors()
	{
		for (int i = 0; i < FSIZE; i++)
			x[i].insert(x[i].end(), currX[i].begin(), currX[i].end());
	}

	void updateTarget(const std::vector<View>& isViews, const std::vector<View>& osViews)
	{
		std::vector<float> yvals;
		for (int i = 0; i < isViews.size(); i++)
			yvals.push_back(math::calc(isViews[i], math::Calc::sign) * math::calc(osViews[i], math::Calc::ir));
		rank::rank(yvals);
		y.insert(y.end(), yvals.begin(), yvals.end());
	}

	std::vector<int> filter(const std::vector<View>& isViews, std::vector<float> &weights) const
	{
	    weights = math::mult(coeff, currX);
	    rank::rank(weights, 0);
	    float cut = 0.1;
	    std::vector<int> res;
	    for (int i = 0; i < weights.size(); i++)
	    {
	        if (weights[i] > 1.0 - cut)
	        {
	            weights[i] = math::calc(isViews[i], math::Calc::sign);
	            res.push_back(i);
	        }
	        else
	            weights[i] = 0;
	    }
	    return res;
	}

	void adjust(const std::vector<int> &idx, const std::vector<View>& isViews, std::vector<float> &weights) const
	{
		int m = idx.size();
		int iterNum = int(m * sqrt(m));
		for (int iter = 0; iter < iterNum; iter++)
		{
			int i = idx[rand() % m];
			int j = idx[rand() % m];
			if (i == j)
				continue;
			float scorr = math::calc(isViews[i], math::Calc::signcorr, &(isViews[j]));
			if (scorr > 0.5)
			{
				weights[i] /= 2;
				weights[j] /= 2;
			}
		}
	}

	float evaluate(const pnl::Pnls& pnls, const std::vector<int>& mask, const std::vector<float> &weights) const
	{
		std::vector<float> weightedPnl(pnls.pnlSize(), 0);
		for (int i = 0; i < weightedPnl.size(); i++)
			for (int j = 0; j < pnls.size(); j++)
				weightedPnl[i] += weights[j] * pnls.pnl(j)[i];
		return math::calc(View(weightedPnl, mask), math::Calc::ir);
	}

	static constexpr bool LEARN = 0;
	static constexpr int FSIZE = 2;

public:

	PnlLearner() : x(FSIZE), currX(FSIZE), coeff(FSIZE, 0) 
	{
		//coeff = { 0.0253311, -0.0462925, 0.0530105, };
		coeff = { 0.0684699, -0.0456859, };
		//coeff = { 0.0751444, -0.0459537, };
	}

	void finalize(std::vector<std::vector<Stats>> results)
	{
		if (LEARN)
		{
			std::cout << std::endl << "coeff = { ";
			auto cf = ols::learn(x, y);
			for (auto &c: cf)
				std::cout << c << ", ";
			std::cout << "};" << std::endl;
		}

		std::vector<Stats> finalres;
		for (auto &x : results)
			finalres.insert(finalres.end(), x.begin(), x.end());
		std::vector<float> isir;
		std::vector<float> osir;
		for (auto &x : finalres)
		{
			isir.push_back(x.isIr);
			osir.push_back(x.osIr);
		}
		std::cout << std::endl;
		float isres = math::hodgesLehmannSen(isir);
		std::cout << "IS fit: " << isres << std::endl;
		float res = math::hodgesLehmannSen(osir);
		std::sort(osir.begin(), osir.end());
		float q25 = osir[osir.size() / 4];
		float q75 = osir[3 * osir.size() / 4];
		std::cout << "OS result: " << res << " - " << (res - q25) << " + " << (q75 - res) << std::endl;
	}

	Stats learn(const pnl::Pnls& pnls, const isos::IsOs& isos, std::vector<float> &weights)
	{
		auto isViews = getViews(pnls, isos.isMask());
		auto osViews = getViews(pnls, isos.osMask());
		updateCurrX(isViews);
		auto idx = filter(isViews, weights);
		adjust(idx, isViews, weights);
		float isIr = evaluate(pnls, isos.isMask(), weights);
		float osIr = evaluate(pnls, isos.osMask(), weights);
		if (LEARN)
		{
			updateFactors();
			updateTarget(isViews, osViews);
		}
		return Stats(isIr, osIr);
	}

};