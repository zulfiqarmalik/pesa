/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#include "learnGA.h"
#include <time.h>
#include <thread>

void run(const pnl::Pnls* pnls, PnlLearner* learner, std::vector<isos::IsOs>* isos, std::vector<Stats> *results, std::vector<float> *weights)
{
	for (int iter = 0; iter < results -> size(); iter++)
	{
		auto result = learner -> learn(*pnls, (*isos)[iter], *weights);
		(*results)[iter] = result;
		std::cout << ".";
		std::cout.flush();
	}
}

int main(int argc, char *argv[]) 
{
	int seed = 1; //(argc > 1) ? std::stoi(std::string(argv[1])) : 1;
	srand(unsigned(seed));

	int gaPopsize = std::stoi(std::string(argv[1]));
	int gaIters = std::stoi(std::string(argv[2]));
	int gaNonzero = std::stoi(std::string(argv[3]));

	int ITERS = 25;
	int THREADS = 4;
	std::cout << "Starting with " << THREADS << " thread(s) and " << ITERS << " iterations.." << std::endl;

	pnl::ParentPnls parentPnls;
	pnl::Pnls pnls(parentPnls, 1.0, true);
	
	std::vector<std::vector<Stats>> results(THREADS, std::vector<Stats>(ITERS));
	std::vector<std::vector<float>> weights(THREADS, std::vector<float>(pnls.size()));
	std::vector<std::vector<isos::IsOs>> isos(THREADS, std::vector<isos::IsOs>(ITERS));
	for (auto &x : isos)
		for (auto &y : x)
			y.init(pnls.pnlSize());
	std::vector<PnlLearner> learners(THREADS);

	for (auto &l : learners)
		l.init(gaPopsize, gaIters, gaNonzero);

	std::cout << "Running.." << std::endl;
	std::vector<std::thread> threads;
	for (int i = 0; i < THREADS; i++)
		threads.push_back(std::thread(run, &pnls, &(learners[i]), &(isos[i]), &(results[i]), &(weights[i])));
	for (int i = 0; i < THREADS; i++)
		threads[i].join();
	
	learners[0].finalize(results);
	// if (ITERS * THREADS == 1)
	// {
	// 	std::string name = "submit/alphas.txt";
	// 	std::ofstream ofs(name);
	// 	std::cout << "Saving alphas to " << name << std::endl;
	// 	for (int i = 0; i < pnls.size(); i++)
	// 		if (fabs(weights[0][i]) > 1e-9)
	// 			ofs << pnls.name(i) << " " << weights[0][i] << std::endl;
	// }
	return 0;
}