/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#include "pnl.h"
#include "isos.h"
#include "rankFuncs.h"
#include "ols.h"

namespace {

std::vector<View> getViews(const pnl::Pnls& pnls, const std::vector<int>& mask)
{
	std::vector<View> res;
	for (int i = 0; i < pnls.size(); i++)
		res.emplace_back(pnls.pnl(i), mask);
	return res;
}

}

struct Stats
{
	float isIr;
	float osIr;
	Stats() : isIr(std::nan("")), osIr(std::nan("")) {}
	Stats(float x, float y) : isIr(x), osIr(y) {}
};

class PnlLearner
{
	std::vector<std::vector<float>> x;
	std::vector<float> y;
	std::vector<float> coeff;

	void getFactors(std::vector<std::vector<float>> *fptr, const std::vector<View>& views) const
	{
		auto &f = *fptr;
		f.resize(FSIZE);
		for (auto &c : f)
			c.resize(views.size());
		for (int i = 0; i < views.size(); i++)
		{
			auto& view = views[i];
			f[0][i] = math::calc(view, math::Calc::ir);
			if (f[0][i] < 0)
				std::cerr << "NEG" << std::endl;
			f[1][i] = math::calc(view, math::Calc::std);
		}
		for (auto &c : f)
			rank::rank(c);
	}

	void updateAllHistoryFactors(const std::vector<std::vector<float>>& f)
	{
		for (int i = 0; i < FSIZE; i++)
			x[i].insert(x[i].end(), f[i].begin(), f[i].end());
	}

	void updateTarget(const std::vector<View>& isViews, const std::vector<View>& osViews)
	{
		std::vector<float> yvals;
		for (int i = 0; i < isViews.size(); i++)
		{
			float target = math::calc(osViews[i], math::Calc::ir);
			yvals.push_back(target);
		}
		rank::rank(yvals);
		y.insert(y.end(), yvals.begin(), yvals.end());
	}

	std::vector<int> filter(const std::vector<std::vector<float>> &f) const
	{
	    auto w = math::mult(coeff, f);
	    rank::rank(w, 0);
	    float cut = 0.1;
	    std::vector<int> res;
	    for (int i = 0; i < w.size(); i++)
	        if (w[i] > 1.0 - cut)
	            res.push_back(i);
	    return res;
	}

	// std::vector<int> filter2(const std::vector<std::vector<float>> &f) const
	// {
	// 	std::vector<std::vector<float>> v0;
	// 	std::vector<std::vector<float>> v1;
	// 	for (int i = 0; i < f.size(); i++)
	// 		if (i % 2)
	// 			v0.push_back(f[i]);
	// 		else
	// 			v1.push_back(f[i]);
	// 	auto r0 = filter(v0);
	// 	auto r1 = filter(v1);
	// 	std::set<int> rset;
	// 	for (auto &r: r0)
	// 		rset.insert(r);
	// 	for (auto &r: r1)
	// 		rset.insert(r);
	//     std::vector<int> res;
	// 	for (auto &r: rset)
	// 		res.push_back(r);
	// 	std::sort(res.begin(), res.end());
	//     return res;
	// }

	float evaluate(const pnl::Pnls& pnls, const std::vector<int>& mask, const std::vector<float> &weights) const
	{
		std::vector<float> weightedPnl(pnls.pnlSize(), 0);
		for (int i = 0; i < weightedPnl.size(); i++)
			for (int j = 0; j < pnls.size(); j++)
				weightedPnl[i] += weights[j] * pnls.pnl(j)[i];
		return math::calc(View(weightedPnl, mask), math::Calc::ir);
	}

	static constexpr bool LEARN = 1;
	static constexpr int FSIZE = 2;

public:

	PnlLearner() : x(FSIZE), coeff(FSIZE, 0)
	{
		//coeff = { 0.0684699, -0.0456859, };
		coeff = { 0.0411727, -0.0158344, };
	}

	void finalize(std::vector<std::vector<Stats>> results)
	{
		if (LEARN)
		{
			std::cout << std::endl << "coeff = { ";
			auto cf = ols::learn(x, y);
			for (auto &c: cf)
				std::cout << c << ", ";
			std::cout << "};" << std::endl;
		}
		std::vector<Stats> finalres;
		for (auto &x : results)
			finalres.insert(finalres.end(), x.begin(), x.end());
		std::vector<float> isir;
		std::vector<float> osir;
		for (auto &x : finalres)
		{
			isir.push_back(x.isIr);
			osir.push_back(x.osIr);
		}
		std::cout << std::endl;
		float isres = math::hodgesLehmannSen(isir);
		std::cout << "IS fit: " << isres << std::endl;
		float res = math::hodgesLehmannSen(osir);
		std::sort(osir.begin(), osir.end());
		float q25 = osir[osir.size() / 4];
		float q75 = osir[3 * osir.size() / 4];
		std::cout << "OS result: " << res << " - " << (res - q25) << " + " << (q75 - res) << std::endl;
	}

	Stats learn(const pnl::Pnls& pnls, const isos::IsOs& isos, std::vector<float> &weights)
	{
		auto isViews = getViews(pnls, isos.isMask());
		auto osViews = getViews(pnls, isos.osMask());
		for (int i = 0; i < pnls.size(); i++)
		{
			int sign = math::calc(isViews[i], math::Calc::sign);
			isViews[i].setSign(sign);
			osViews[i].setSign(sign);
		}
		std::vector<std::vector<float>> f;
		getFactors(&f, isViews);
		auto idx = filter(f);
		weights.assign(weights.size(), 0);
		for (auto &ind : idx)
			weights[ind] = 1;
		float isIr = evaluate(pnls, isos.isMask(), weights);
		float osIr = evaluate(pnls, isos.osMask(), weights);
		if (LEARN)
		{
			updateAllHistoryFactors(f);
			updateTarget(isViews, osViews);
		}
		return Stats(isIr, osIr);
	}

};