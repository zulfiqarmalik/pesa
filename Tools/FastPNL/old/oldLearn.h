/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#include "pnl.h"
#include "isos.h"
#include "rankFuncs.h"
#include "mathMatr.h"

namespace {

std::vector<int> filter(const pnl::Pnls& pnls, const std::vector<int>& isMask, std::vector<float> &weights)
{
    int m = weights.size();
    std::vector<int> signs(m);
    std::vector<float> irs(m);
    std::vector<float> stds(m);

    for (int i = 0; i < m; i++)
    {
        float ir = math::calc(View(pnls.pnl(i), isMask), math::Calc::ir);
        float std = math::calc(View(pnls.pnl(i), isMask), math::Calc::std);
        irs[i] = (fabs(ir));
        stds[i] = 1.0 / std;
        signs[i] = math::sign(ir);
    }
    rank::rank(irs, 0);
    rank::rank(stds, 0);
    for (int i = 0; i < m; i++)
            weights[i] = irs[i] + 0.2 * stds[i];
    rank::rank(weights, 0);
    float cut = 0.005;
    std::vector<int> res;
    for (int i = 0; i < m; i++)
    {
        if (weights[i] > 1.0 - cut)
        {
            weights[i] = signs[i]; // *= signs[i];
            res.push_back(i);
        }
        else
            weights[i] = 0;
    }
    return res;
}

void adjust(const std::vector<int>& goodIdx, const pnl::Pnls& pnls, const std::vector<int>& isMask, std::vector<float> &weights)
{
	int n = goodIdx.size();
	std::vector<float> w;
	for (int i = 0; i < n; i++)
	{
		int idx = goodIdx[i];
		w.push_back(weights[idx]);
	}
	rank::rankAbs(w);
	for (int i = 0; i < n; i++)
	{
		int idx = goodIdx[i];
		float denom = math::calc(View(pnls.pnl(idx), isMask), math::Calc::var);
		weights[idx] = w[i] / denom;
	}
	float wabs = 0;
	for (auto &w : weights)
		wabs += fabs(w);
	float scaleMult = 2 / wabs;
	for (auto &w : weights)
		w *= scaleMult;
}

}

struct Stats
{
	float isIr;
	float osIr;
};

class PnlLearner
{

	static constexpr bool printNames = true;

	static void assignWeights(const pnl::Pnls& pnls, const std::vector<int>& isMask, std::vector<float> &weights)
	{
		auto goodIdx = filter(pnls, isMask, weights);
		adjust(goodIdx, pnls, isMask, weights);
	}

	static float evaluate(const pnl::Pnls& pnls, const std::vector<int>& mask, const std::vector<float> &weights)
	{
		std::vector<float> weightedPnl(pnls.pnlSize(), 0);
		for (int i = 0; i < weightedPnl.size(); i++)
			for (int j = 0; j < pnls.size(); j++)
				weightedPnl[i] += weights[j] * pnls.pnl(j)[i];
		return math::calc(View(weightedPnl, mask), math::Calc::ir);
	}

public:

	static void finalize(const std::vector<Stats>& result)
	{
		std::vector<float> isir;
		std::vector<float> osir;
		for (auto &x : result)
		{
			isir.push_back(x.isIr);
			osir.push_back(x.osIr);
		}
		std::cout << std::endl;
		float isres = math::hodgesLehmannSen(isir);
		std::cout << "IS fit: " << isres << std::endl;
		float res = math::hodgesLehmannSen(osir);
		float mad = math::MAD(osir) / sqrt(result.size());
		std::cout << "OS result: " << res << " +- " << mad << std::endl;
		// std::sort(osir.begin(), osir.end());
		// float med = osir[osir.size() / 2];
		// float q25 = osir[osir.size() / 4];
		// float q75 = osir[3 * osir.size() / 4];
		// std::cout << "OS result: " << med << " - " << (med - q25) << " + " << (q75 - med) << std::endl;
	}

	static Stats learn(const pnl::Pnls& pnls, const isos::IsOs& isos, std::vector<float> &weights)
	{
		assignWeights(pnls, isos.isMask(), weights);
		Stats stats;
		stats.isIr = evaluate(pnls, isos.isMask(), weights);
		stats.osIr = evaluate(pnls, isos.osMask(), weights);
		return stats;
	}

};