/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#include "pnl.h"
#include "isos.h"
#include "rankFuncs.h"
#include "mathMatr.h"


class PnlLearner
{

	static void assignBasicWeights(const pnl::Pnls& pnls, const std::vector<int>& isMask, std::vector<float> &weights)
	{
		for (int i = 0; i < weights.size(); i++)
			weights[i] = math::calc(View(pnls.pnl(i), isMask), math::Calc::ir);
	}

	static void assignRankedStdWeights(const pnl::Pnls& pnls, const std::vector<int>& isMask, std::vector<float> &weights)
	{
		for (int i = 0; i < weights.size(); i++)
			weights[i] = math::calc(View(pnls.pnl(i), isMask), math::Calc::ir);
		std::multimap<float, int> v;
		for (int i = 0; i < weights.size(); i++)
		{
			float val = fabs(weights[i]);
			v.insert(std::pair<float, int>(val, i));
		}
		int idx = 0;
		int cut = v.size() - 100;
		for (auto &it : v)
		{
			if (idx < cut)
				weights[it.second] = 0;
			else
			{
				auto sign = math::sign(weights[it.second]);
				auto denom = math::calc(View(pnls.pnl(it.second), isMask), math::Calc::var);
				weights[it.second] = sign * (idx - cut) / (denom);
			}
			idx++;
		}
	}

	static void assignRecursiveWeights(const pnl::Pnls& pnls, const std::vector<int>& isMask, std::vector<float> &weights)
	{
		isos::IsOs isos;
		weights.assign(weights.size(), 0);
		for (int subsets = 0; subsets < 1000; subsets++)
		{
			pnl::Pnls pnlSubset(pnls, 0.1);
			std::vector<float> smallWeights(pnlSubset.size());
			std::vector<float> osirs;
			for (int iter = 0; iter < 30; iter++)
			{
				isos.init(isMask);
				assignBasicWeights(pnlSubset, isos.isMask, smallWeights);
				osirs.push_back(evaluate(pnlSubset, isos.osMask, smallWeights));
			}
			auto res = math::hodgesLehmannSen(osirs);
			if (res > 0.04)
			{
				assignBasicWeights(pnlSubset, isMask, smallWeights);
				for (int i = 0; i < pnlSubset.size(); i++)
					weights[pnlSubset.idxInParent()[i]] += smallWeights[i];
			}
		}
	}

	static float assignWeights(const pnl::Pnls& pnls, const isos::IsOs& isos, std::vector<float> &weights)
	{
		//filter pnls
		//assign weights
	}

	static float evaluate(const pnl::Pnls& pnls, const std::vector<int>& osMask, const std::vector<float> &weights)
	{
		std::vector<float> weightedPnl(pnls.pnlSize(), 0);
		for (int i = 0; i < weightedPnl.size(); i++)
			for (int j = 0; j < pnls.size(); j++)
				weightedPnl[i] += weights[j] * pnls.pnl(j)[i];
		return math::calc(View(weightedPnl, osMask), math::Calc::ir);
	}

public:

	static float learn(const pnl::Pnls& pnls, const isos::IsOs& isos, std::vector<float> &weights)
	{
		assignWeights(pnls, isos.isMask, weights);
		return evaluate(pnls, isos.osMask, weights);
	}

};