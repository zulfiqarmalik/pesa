/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#include "pnl.h"
#include "isos.h"
#include "rankFuncs.h"
#include "mathMatr.h"

namespace {

std::vector<int> filter(const pnl::Pnls& pnls, const std::vector<int>& isMask, std::vector<float> &weights)
{
	std::vector<int> signs(weights.size());
	for (int i = 0; i < weights.size(); i++)
	{
		float ir = math::calc(View(pnls.pnl(i), isMask), math::Calc::ir);
		weights[i] = fabs(ir);
		signs[i] = math::sign(ir);
	}
	rank::rank(weights, 0);
	float cut = 0.2;
	std::vector<int> res;
	for (int i = 0; i < weights.size(); i++)
	{
		if (weights[i] > 1.0 - cut)
		{
			weights[i] *= signs[i];
			res.push_back(i);
		}
		else
			weights[i] = 0;
	}
	return res;
}

float func1(float a, float b)
{
	float x0 = a;
	float x1 = b;
	float param = 0.2;
	return x0 + param * x1;
}

std::vector<int> filterIrAndStd(const pnl::Pnls& pnls, const std::vector<int>& isMask, std::vector<float> &weights)
{
	int m = weights.size();
	std::vector<int> signs(m);
	std::vector<float> irs(m);
	std::vector<float> stds(m);

	for (int i = 0; i < m; i++)
	{
		float ir = math::calc(View(pnls.pnl(i), isMask), math::Calc::ir);
		float std = math::calc(View(pnls.pnl(i), isMask), math::Calc::std);
		irs[i] = (fabs(ir));
		stds[i] = 1.0 / std;
		signs[i] = math::sign(ir);
	}
	rank::rank(irs, 0);
	rank::rank(stds, 0);
	for (int i = 0; i < m; i++)
		weights[i] = func1(irs[i], stds[i]);
	rank::rank(weights, 0);
	float cut = 0.1;
	std::vector<int> res;
	for (int i = 0; i < m; i++)
	{
		if (weights[i] > 1.0 - cut)
		{
			weights[i] *= signs[i];
			res.push_back(i);
		}
		else
			weights[i] = 0;
	}
	return res;
}

void adjust(const std::vector<int>& goodIdx, const pnl::Pnls& pnls, const std::vector<int>& isMask, std::vector<float> &weights)
{
	int n = goodIdx.size();
	std::vector<float> w;
	for (int i = 0; i < n; i++)
	{
		int idx = goodIdx[i];
		w.push_back(weights[idx]);
	}
	rank::rankAbs(w);
	for (int i = 0; i < n; i++)
	{
		int idx = goodIdx[i];
		float denom = math::calc(View(pnls.pnl(idx), isMask), math::Calc::var);
		weights[idx] = w[i] / denom;
	}
}

void adjustMedianDenom(const std::vector<int>& goodIdx, const pnl::Pnls& pnls, const std::vector<int>& isMask, std::vector<float> &weights)
{
	int n = goodIdx.size();
	for (int i = 0; i < n; i++)
	{
		int idx = goodIdx[i];
		View view(pnls.pnl(idx), isMask);
		std::vector<float> x;
		for (int i = 0; i < view.size(); i++)
			if (view[i] < 0)
				x.push_back(view[i]);
		std::sort(x.begin(), x.end());
		float denom = x[x.size() / 2];
		weights[idx] /= (denom * denom);
	}
}

void adjustMeanCorr(const std::vector<int>& goodIdx, const pnl::Pnls& pnls, const std::vector<int>& isMask, std::vector<float> &weights)
{
	int n = goodIdx.size();
	std::vector<View> views;
	for (int i = 0; i < n; i++)
		views.emplace_back(pnls.pnl(goodIdx[i]), isMask);
	auto covs = math::covMatrix(views, math::Calc::corr);
	std::vector<float> w(n);
	for (int i = 0; i < n; i++)
		w[i] = 1.0 - math::calc(covs[i], math::Calc::mean);
	rank::rank(w);
	float corrMult = 1;
	for (int i = 0; i < n; i++)
	{
		int idx = goodIdx[i];
		weights[idx] *= (1 + corrMult * w[i]);
		float denom = math::calc(View(pnls.pnl(idx), isMask), math::Calc::var);
		weights[idx] /= (denom * denom);
	}
}

void adjustInvCov(const std::vector<int>& goodIdx, const pnl::Pnls& pnls, const std::vector<int>& isMask, std::vector<float> &weights)
{
	int n = goodIdx.size();
	std::vector<View> views;
	for (int i = 0; i < n; i++)
		views.emplace_back(pnls.pnl(goodIdx[i]), isMask);
	auto covs = math::covMatrix(views);
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			if (i != j)
				covs[i][j] *= 0.1;
	auto invCov = math::invertSymm(covs);
	std::vector<float> rets(n);
	for (int i = 0; i < n; i++)
		rets[i] = weights[goodIdx[i]];
	auto w = math::mult(invCov, rets);
	for (int i = 0; i < n; i++)
	{
		int idx = goodIdx[i];
		weights[idx] = w[i];
	}
}

}

class PnlLearner
{

	static void assignWeights(const pnl::Pnls& pnls, const std::vector<int>& isMask, std::vector<float> &weights)
	{
		auto goodIdx = filterIrAndStd(pnls, isMask, weights);
		adjust(goodIdx, pnls, isMask, weights);
	}

	static float evaluate(const pnl::Pnls& pnls, const std::vector<int>& osMask, const std::vector<float> &weights)
	{
		std::vector<float> weightedPnl(pnls.pnlSize(), 0);
		for (int i = 0; i < weightedPnl.size(); i++)
			for (int j = 0; j < pnls.size(); j++)
				weightedPnl[i] += weights[j] * pnls.pnl(j)[i];
		return math::calc(View(weightedPnl, osMask), math::Calc::ir);
	}

public:

	static float learn(const pnl::Pnls& pnls, const isos::IsOs& isos, std::vector<float> &weights)
	{
		assignWeights(pnls, isos.isMask, weights);
		return evaluate(pnls, isos.osMask, weights);
	}

};