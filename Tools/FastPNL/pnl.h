/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

namespace pnl {

class ParentPnls
{
	friend class Pnls;

	std::vector<std::vector<float>> pnls;
	std::vector<std::string> names;

	bool check(const std::vector<float>& pnl)
	{
		int c = 0;
		for (auto &p : pnl)
		    if (fabs(p) < 1e-5)
				c++;
		if (c > 3)
			return 0;
		for (auto &p : pnls)
		{
			if (fabs(p[100] - pnl[100]) + fabs(p[144] - pnl[144]) + fabs(p[225] - pnl[225]) < 1e-5)
				return 0;
		}
		return 1;
	}

	bool loadPnl(std::string fileName, std::vector<float> *v)
	{
		float MAXDATE = 20171231;
		v -> clear();
		std::vector<std::string> lines;
		util::readLines(fileName, &lines);
		std::vector<float> nums;
		for (int i = 1; i < lines.size(); i++)
		{
			if (lines[i].empty())
				break;
			auto date = util::getCSVField(lines[i], 0);
			if (date > MAXDATE)
				break;
			auto numLongShort = util::getCSVField(lines[i], 16) + util::getCSVField(lines[i], 17);
			if (numLongShort)
				pb(nums, numLongShort);
			float val = util::getCSVField(lines[i], 3);
			val *= 1e-5;
			v -> push_back(val);
		}
		if (math::min(nums) < 100)
			return 0;
		return check(*v);
	}

public:

	ParentPnls()
	{
		auto allNames = names::pnlNames();
		for (auto &pnlName : allNames)
		{
			std::vector<float> pnl;
			//std::cout << pnlName << std::endl;
			if (loadPnl(pnlName, &pnl))
			{
				names.push_back(pnlName);
				pnls.push_back(pnl);
			}
		}
		std::cout << "Loaded " << pnls.size() << " pnls" << std::endl;
	}

};

class Pnls
{
	std::vector<int> parentIdx;
	const ParentPnls& parent;

public:
	const std::vector<float>& pnl(int i) const
	{
		return parent.pnls[i];
	}
	const std::vector<float>& operator[] (int i) const
	{
		return pnl(i);
	}
	const std::string& name(int i) const
	{
		return parent.names[i];
	}
	int size() const
	{
		return parentIdx.size();
	}
	int pnlSize() const
	{
		return pnl(0).size();
	}
	Pnls(const ParentPnls& parent_, float sampleRate = 1.0, bool randomize = true) : parent(parent_)
	{
		for (int i = 0; i < parent.pnls.size(); i++)
		{
			float p = float(i) / parent.pnls.size();
			if (randomize)
				p = float(rand()) / RAND_MAX;
			if (p < sampleRate)
			{
				parentIdx.push_back(i);
			}
		}
		std::cout << "Subset size: " << parentIdx.size() << " pnls" << std::endl;
	}
};

}

