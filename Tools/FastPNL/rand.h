/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
namespace randbytes
{

class RandBytes
{
	static constexpr int SIZE = 1000000;
	static constexpr int CUT = 1000;
	int count;
	std::vector<int> bytes;
public:
	RandBytes() : count(0)
	{
		for (int i = 0; i < SIZE; i++)
			pb(bytes, rand() % CUT);
	}
	unsigned next()
	{
		count++;
		if (count >= SIZE)
			count = 0;
		return bytes[count];
	}
	float nextFloat()
	{
		return (-0.5f + float(next()) / CUT);
	}
};

}