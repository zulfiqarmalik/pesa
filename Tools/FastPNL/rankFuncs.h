/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

namespace rank 
{

struct IdxVal
{
	float val;
	int idx;
	IdxVal(float val_, int idx_) : val(val_), idx(idx_) {}
};

bool operator< (const IdxVal& lhs, const IdxVal& rhs)
{
	return (lhs.val < rhs.val);
}


template<class T>
void rank(T& a, float shift = 0.5)
{
	std::vector<IdxVal> v;
	for (int ii = 0; ii < a.size(); ii++)
		if (std::isfinite(a[ii]))
			v.emplace_back(a[ii], ii);
	if (v.empty())
		return;
	std::sort(v.begin(), v.end());
	if (v.size() == 1)
	{
		a[v[0].idx] = 0;
		return;
	}
	float denom = 1.0 / (v.size() - 1.0);
	int ranksum = 0;
	int count = 1;
	float lastVal = v[0].val;
	for (int i = 1; i < v.size(); i++)
	{
		if (lastVal != v[i].val)
		{
			float value = float(ranksum) / count;
			value = denom * value - shift;
			for (int j = 0; j < count; j++)
				a[v[i - 1 - j].idx] = value;
			ranksum = count = 0;
			lastVal = v[i].val;
		}
		ranksum += i;
		count++;
	}
	float value = float(ranksum) / count;
	value = denom * value - shift;
	for (int j = 0; j < count; j++)
		a[v[v.size() - 1 - j].idx] = value;
}

template<class T>
void rankAbs(T& a)
{
	std::vector<float> b(a);
	for (auto &x : b)
		x = fabs(x);
	rank(b, 0);
	for (int i = 0; i < a.size(); i++)
		a[i] = math::sign(a[i]) * b[i];
}

template<class T>
void rankNonzeroAbs(T& a)
{
	std::vector<float> b;
	std::vector<int> idx;
	for (int i = 0; i < a.size(); i++)
		if (fabs(a[i]) != 0.0f)
		{
			idx.push_back(i);
			b.push_back(a[i]);
		}
	rankAbs(b);
	for (int i = 0; i < idx.size(); i++)
		a[idx[i]] = b[i];
}

template<class T>
float WXtest(const T& a)
{
	std::vector<float> aa(a.size());
	for (int i = 0; i < a.size(); i++)
		aa[i] = a[i];
	rankAbs(aa);
	return std::sqrt(a.size()) * math::calc(aa, math::Calc::mean) / math::calc(aa, math::Calc::std);
}

template<class T>
float WXtest(const T& a, const T& b)
{
	std::vector<float> x(a.size());
	for (int i = 0; i < a.size(); i++)
		x[i] = a[i] - b[i];
	return WXtest(x);
}



}