for nonzero in 50 100 150
do
 for popsize in 300 600 1000
 do
  for iters in 160 320 640 1280 2560
  do
   ./main $popsize $iters $nonzero
  done
 done
done
