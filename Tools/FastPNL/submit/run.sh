numlines=`wc -l $1 | cut -d" " -f1` 
for i in `seq 1 $numlines`
do 
 line=`head -n $i $1 | tail -n1`
 #echo $line
 weight=`echo $line | awk '{print $2;}'`
 #echo $weight
 num=`echo $line | cut -d" " -f1 | cut -d"." -f1 | cut -d"-" -f2`
 #echo $num
 fname=`ls $2/*$num*xml`
 #echo $fname
 echo "<Alpha"
 sed -n '/rev/,/<\/Alpha/p' $fname | sed s/"weight = \"1.0\""/"weight = \"$weight\""/g
done
