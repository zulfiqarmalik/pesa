/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <algorithm>
#include <math.h>
#include <map>
#include <set>
#include <time.h>
#include <thread>

template<typename T, typename V>
void pb(T& t, const V& v)
{
	t.push_back(v);
}

namespace util {

template<typename T>
void print(const T& t)
{
	for (auto &x : t)
		std::cout << x << " ";
	std::cout << std::endl;
}

template<typename T>
void pr(T t)
{
	std::cout << t << std::endl;
}

template<typename T>
typename T::value_type last(const T& t)
{
	return t[t.size() - 1];
}

void readLines(std::string fileName, std::vector<std::string> *res)
{
	std::ifstream ifs(fileName.c_str());
	res -> clear();
	std::string line;
	while (std::getline(ifs, line))
	{
		res -> push_back(line);
	}
}

void saveFloatVector(std::string fileName, const std::vector<float> &v)
{
	int sz = (int)v.size();
	FILE *f = fopen(fileName.c_str(), "wb");
	fwrite(&sz, 4, 1, f);
	const float *p = &(v[0]);
	fwrite(p, 4, sz, f);
	fclose(f);
}

void loadFloatVector(std::string fileName, std::vector<float> *v)
{
	v -> clear();
	FILE *f = fopen(fileName.c_str(), "rb");
	int sz;
	fread(&sz, 4, 1, f);
	v -> resize(sz);
	fread(&((*v)[0]), 4, sz, f);
	fclose(f);
}

float getCSVField(const std::string &st, int idx)
{
	int i = 0;
	int count = 0;
	std::string res;
	while ((count <= idx) && (i < (int)st.size()))
	{
		if (st[i] == ',')
			count++;
		else
		{
			if ((count == idx) && (st[i] != ' '))
				res.push_back(st[i]);
		}
		i++;
	}
	return std::stof(res);
}

}