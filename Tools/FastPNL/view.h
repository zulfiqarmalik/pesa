/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


class View
{
	const std::vector<float>& c;
	const std::vector<int>& mask;
	int sign;

public:
	View(const std::vector<float>& c_, const std::vector<int>& mask_) : c(c_), mask(mask_), sign(1) {}
	float operator[] (int i) const
	{
		return sign * c[mask[i]];
	}
	int size() const
	{
		return mask.size();
	}
	void setSign(int sign_)
	{
		sign = sign_;
	}
};

class VView
{
	const std::vector<std::vector<float>>& m;
	int idx;
public:
	VView(const std::vector<std::vector<float>>& m_, int idx_) : m(m_), idx(idx_) {}
	float operator[] (int i) const
	{
		return m[i][idx];
	}
	int size() const
	{
		return m.size();
	}
};