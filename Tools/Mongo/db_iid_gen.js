counter = 0;

db.AlphaConfigs.find({}).forEach(function(doc) {
    print("Updating: " + doc["_id"]);
    db.AlphaConfigs.update({
        _id: doc["_id"]
    }, {
        "$set": {
            "iid": ObjectId()
        }
    });
});
