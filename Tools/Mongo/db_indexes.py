### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#!python

import os
import sys
import traceback
import time

import datetime as DT
from optparse import OptionParser

# Add all the subdirectories to the search path ...
class DBIndexes:
    def __init__(self):
        opt, args                       = self.parseArguments()
        self.db                         = None
        self.mongoClient                = None
        self.dbPath                     = opt.dbPath
        self.dbName                     = opt.dbName
        self.background                 = True

        self.statsCollections           = [
            "STATS_2012", "STATS_2012_QT01", "STATS_2012_QT02", "STATS_2012_QT03", "STATS_2012_QT04", 
            "STATS_2013", "STATS_2013_QT01", "STATS_2013_QT02", "STATS_2013_QT03", "STATS_2013_QT04", 
            "STATS_2014", "STATS_2014_QT01", "STATS_2014_QT02", "STATS_2014_QT03", "STATS_2014_QT04", 
            "STATS_2015", "STATS_2015_QT01", "STATS_2015_QT02", "STATS_2015_QT03", "STATS_2015_QT04", 
            "STATS_2016", "STATS_2016_QT01", "STATS_2016_QT02", "STATS_2016_QT03", "STATS_2016_QT04", 
            "STATS_2017", "STATS_2017_QT01", "STATS_2017_QT02", "STATS_2017_QT03", "STATS_2017_QT04", 
            "STATS_2018", "STATS_2018_QT01", "STATS_2018_QT02", "STATS_2018_QT03", "STATS_2018_QT04", 
            "STATS_2019", "STATS_2019_QT01", "STATS_2019_QT02", "STATS_2019_QT03", "STATS_2019_QT04",
            "STATS_2020", "STATS_2020_QT01", "STATS_2020_QT02", "STATS_2020_QT03", "STATS_2020_QT04",
            "STATS_D10", "STATS_D21", "STATS_D250", "STATS_D5", "STATS_D55", "STATS_LT", "STATS_IS", "STATS_OS",
        ]

        self.weeklyStatsForYear         = [2017, 2018, 2019, 2020]

        self.connectDB()

    def parseArguments(self):
        parser = OptionParser()

        parser.add_option("-d", "--dbPath", dest = "dbPath", default = "mongodb://10.240.1.70:27018/?replicaSet=psim", help = "The path to the database server")
        parser.add_option("-D", "--dbName", dest = "dbName", default = "pesa", help = "The name of the database to connect to")
            
        return parser.parse_args()

    def createIndexForStatsColl(self, statsCollName):

        print("Creating: %s" % (statsCollName))
        import pymongo 

        statsColl = self.db[statsCollName]

        statsColl.create_index([("universeId", pymongo.ASCENDING), ("market", pymongo.ASCENDING), ("delay", pymongo.ASCENDING)], background = self.background)
        statsColl.create_index([("universeId", pymongo.ASCENDING), ("market", pymongo.ASCENDING)], background = self.background)
        statsColl.create_index([("numInvalidPnl", pymongo.ASCENDING)], background = self.background)
        statsColl.create_index([("startDate", pymongo.ASCENDING)], background = self.background)
        statsColl.create_index([("endDate", pymongo.ASCENDING)], background = self.background)
        statsColl.create_index([("pnl", pymongo.ASCENDING)], background = self.background)
        statsColl.create_index([("uuid", pymongo.ASCENDING)], background = self.background)
        statsColl.create_index([("ir", pymongo.ASCENDING)], background = self.background)
        statsColl.create_index([("tvr", pymongo.ASCENDING)], background = self.background)
        statsColl.create_index([("marginBps", pymongo.ASCENDING)], background = self.background)
        statsColl.create_index([("marginCps", pymongo.ASCENDING)], background = self.background)
        statsColl.create_index([("volatility", pymongo.ASCENDING)], background = self.background)
        statsColl.create_index([("returns", pymongo.ASCENDING)], background = self.background)
        statsColl.create_index([("hitRatio", pymongo.ASCENDING)], background = self.background)
        statsColl.create_index([("maxDD", pymongo.ASCENDING)], background = self.background)
        statsColl.create_index([("maxDH", pymongo.ASCENDING)], background = self.background)
        statsColl.create_index([("maxDU", pymongo.ASCENDING)], background = self.background)
        statsColl.create_index([("maxDUH", pymongo.ASCENDING)], background = self.background)
        statsColl.create_index([("tags.gbatchId", pymongo.ASCENDING)], background = self.background)
        statsColl.create_index([("tags.dataUsed", pymongo.ASCENDING)], background = self.background)
        statsColl.create_index([("maxTime", pymongo.ASCENDING)], background = self.background)
        statsColl.create_index([("type", pymongo.ASCENDING)], background = self.background)
        statsColl.create_index([("lsSum" , pymongo.ASCENDING), ("lsRatio", pymongo.ASCENDING)], background = self.background)

    def createStatsIndexes(self):
        for statsCollName in self.statsCollections:
            self.createIndexForStatsColl(statsCollName)

        for year in self.weeklyStatsForYear:
            for week in range(52):
                collName = "STATS_" + str(year) + "_WK" + str(week + 1).zfill(2)
                self.createIndexForStatsColl(collName)

        self.createIndexForStatsColl("STATS_YTD")
        self.createIndexForStatsColl("STATS_MTD")
        self.createIndexForStatsColl("STATS_WTD")

            # statsColl.create_index([("quantile_1.returns", pymongo.ASCENDING), ("quantile_2.returns", pymongo.ASCENDING), ("quantile_3.returns", pymongo.ASCENDING),
            #     ("quantile_4.returns", pymongo.ASCENDING), ("quantile_5.returns", pymongo.ASCENDING)], background = self.background)

    def createAlphaConfigsIndexes(self, collName):
        import pymongo

        alphaConfigsColl = self.db[collName]

        alphaConfigsColl.create_index([("defs.map._filename", pymongo.ASCENDING)], background = self.background)
        alphaConfigsColl.create_index([("map.gbatchId", pymongo.ASCENDING)], background = self.background)
        alphaConfigsColl.create_index([("name", pymongo.ASCENDING)], background = self.background)
        alphaConfigsColl.create_index([("type", pymongo.ASCENDING)], background = self.background)
        alphaConfigsColl.create_index([("portfolio.map.uuid", pymongo.ASCENDING)], background = self.background)
        alphaConfigsColl.create_index([("market", pymongo.ASCENDING)], background = self.background)
        alphaConfigsColl.create_index([("pruned", pymongo.ASCENDING)], background = self.background)
        alphaConfigsColl.create_index([("universeId", pymongo.ASCENDING), ("pruned", pymongo.ASCENDING)], background = self.background)

    def createIndexes(self):
        self.createStatsIndexes()
        self.createAlphaConfigsIndexes("AlphaConfigs")
        self.createAlphaConfigsIndexes("StratConfigs")

    def connectDB(self):
        from pymongo import MongoClient
        
        if self.db is not None:
            return
            
        self.mongoClient = MongoClient(self.dbPath)
        if self.mongoClient is None:
            sys.exit(1)
        
        self.db = self.mongoClient[self.dbName]
        if self.db is None:
            sys.exit(1)

DBIndexes().createIndexes()
