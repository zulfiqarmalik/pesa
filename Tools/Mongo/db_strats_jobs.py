### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#!python

import os
import sys
import traceback
import time

import datetime as DT
from optparse import OptionParser

# Add all the subdirectories to the search path ...
class StratsJobs:
    def __init__(self):
        opt, args           = self.parseArguments()
        self.db             = None
        self.jobsDB         = None
        self.jobsColl       = None
        self.mongoClient    = None
        self.dbPath         = opt.dbPath
        self.dbName         = opt.dbName
        self.collName       = opt.collName

        self.connectDB()

    def parseArguments(self):
        parser = OptionParser()

        parser.add_option("-d", "--dbPath", dest = "dbPath", default = "mongodb://10.240.1.70:27018/?replicaSet=psim", help = "The path to the database server")
        parser.add_option("-D", "--dbName", dest = "dbName", default = "pesa", help = "The name of the database to connect to")
        parser.add_option("-C", "--collName", dest = "collName", default = "AlphaConfigs", help = "The name of the collection")
            
        return parser.parse_args()

    def queue(self):
        import pymongo
        alphaConfigsColl    = self.db[self.collName]

        # We get all the non-pruned strategies
        strats              = alphaConfigsColl.find({ "pruned": False })
        numAdded            = 0

        for strat in strats:
            sid = strat["_id"]
            strat.pop("_id")
            jid = self.jobsColl.insert(strat, check_keys = False)
            print("Inserted: %s => %s" % (str(sid), str(jid)))
            numAdded += 1

        print("Num strats queued: %d" % (numAdded))


    def connectDB(self):
        from pymongo import MongoClient
        
        if self.db is not None:
            return
            
        self.mongoClient = MongoClient(self.dbPath)
        if self.mongoClient is None:
            sys.exit(1)
        
        self.db = self.mongoClient[self.dbName]
        if self.db is None:
            print("Unable to load DB: %s" % (self.dbName))
            sys.exit(1)

        self.jobsDB = self.mongoClient["pesa"]
        if self.jobsDB is None:
            print("Unable to load DB: pesa")
            sys.exit(1)

        self.jobsColl = self.jobsDB["Jobs"]

StratsJobs().queue()
