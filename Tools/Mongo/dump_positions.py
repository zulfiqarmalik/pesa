### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#!python

import os
import sys
import traceback
import time

import pymongo
from bson.objectid import ObjectId
from pymongo import MongoClient

import datetime as DT
from optparse import OptionParser
import pandas as pd
from pandas.tseries.offsets import BDay, BMonthBegin
import struct
import math

def dateToInt(date):
    return int(str(date.year).zfill(4) + str(date.month).zfill(2) + str(date.day).zfill(2))

# Add all the subdirectories to the search path ...
class DumpPositions:
    def __init__(self):
        opt, args           = self.parseArguments()

        startDate           = DT.datetime.strptime(opt.startDate, "%Y%m%d")
        endDate             = DT.datetime.strptime(opt.endDate, "%Y%m%d")

        self.db             = None
        self.mongoClient    = None
        self.dbPath         = opt.dbPath
        self.dbName         = opt.dbName
        self.uuid           = opt.uuid
        self.startDate      = startDate
        self.endDate        = endDate
        self.outputDir      = opt.outputDir

        # First thing: connect to the database
        self.connectDB()

    def parseArguments(self):
        parser = OptionParser()

        parser.add_option("-d", "--dbPath", dest = "dbPath", default = "mongodb://localhost:27017/", help = "The path to the database server")
        parser.add_option("-D", "--dbName", dest = "dbName", default = "pesa_user", help = "The name of the database to connect to")
        parser.add_option("-o", "--outputDir", dest = "outputDir", default = "./", help = "The output directory where positions will be dumped")
        parser.add_option("-S", "--startDate", dest = "startDate", default = None, help = "The starting date")
        parser.add_option("-E", "--endDate", dest = "endDate", default = None, help = "The ending date")

        parser.add_option("-U", "--uuid", dest = "uuid", default = None, help = "The UUID for which positions need to be dumped")

        return parser.parse_args()

    def run(self):
        posInfo             = self.db["POS_Info"].find_one({
            "_id"           : ObjectId(self.uuid)
        })

        assert posInfo, "Unable to find pos info for uuid: %s" % (self.uuid)

        tickers             = posInfo["tickers"]
        cfigis              = posInfo["cfigis"]
        dates               = pd.date_range(self.startDate, self.endDate, freq = BDay())

        for date in dates:
            idate           = dateToInt(date)
            posId           = str.format("%s_%d" % (self.uuid, idate))

            posShares       = self.db["POS_Shares"].find_one({
                "_id"       : posId
            })

            if posShares is not None: 
                data        = posShares["data"]

                assert len(data) % 4 == 0, "Data length not a multiple of 4"

                filename    = os.path.join(self.outputDir, str.format("positions_%s_%d.csv" % (self.uuid, idate)))
                print("Dumping: %s - %d => %s" % (self.uuid, idate, filename))

                with open(filename, "w") as file:
                    file.write("Ticker, FIGI, Num Shares\n")

                    for i in range(0, len(data), 4):
                        numShares = struct.unpack('<f', data[i:i + 4])[0]
                        if i < len(tickers) and not math.isnan(numShares):
                            file.write("%s, %s, %d\n" % (tickers[i], cfigis[i], int(numShares)))


    def connectDB(self):
        
        if self.db is not None:
            return
            
        self.mongoClient = MongoClient(self.dbPath)
        if self.mongoClient is None:
            sys.exit(1)
        
        self.db = self.mongoClient[self.dbName]
        if self.db is None:
            sys.exit(1)

DumpPositions().run()
