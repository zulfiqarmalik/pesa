### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#!python

import os
import sys
import traceback
import time

import datetime as DT
from optparse import OptionParser

def dateToInt(date):
    return int(str(date.year).zfill(4) + str(date.month).zfill(2) + str(date.day).zfill(2))

def formatUuid(uuid):
    import re
    return re.sub('[^a-zA-Z0-9\n_]', '-', uuid)

# Add all the subdirectories to the search path ...
class Prune:
    def __init__(self):
        opt, args                       = self.parseArguments()
        self.db                         = None
        self.mongoClient                = None
        self.dbPath                     = opt.dbPath
        self.dbName                     = opt.dbName
        self.longShortRatioTol          = opt.longShortRatioTol
        self.longShortSumTol            = opt.longShortSumTol

        self.skip                       = int(opt.skip)
        self.limit                      = int(opt.limit)

        self.statsCollections           = [
            "STATS_2012", "STATS_2012_QT01", "STATS_2012_QT02", "STATS_2012_QT03", "STATS_2012_QT04", "STATS_2013", "STATS_2013_QT01", "STATS_2013_QT02", "STATS_2013_QT03", 
            "STATS_2013_QT04", "STATS_2013_QT05", "STATS_2014", "STATS_2014_QT01", "STATS_2014_QT02", "STATS_2014_QT03", "STATS_2014_QT04", "STATS_2014_QT05", "STATS_2015", 
            "STATS_2015_QT01", "STATS_2015_QT02", "STATS_2015_QT03", "STATS_2015_QT04", "STATS_2015_QT05", "STATS_2016", "STATS_2016_QT01", "STATS_2016_QT02", "STATS_2016_QT03", 
            "STATS_2016_QT04", "STATS_2016_QT05", "STATS_2017", "STATS_2017_QT01", "STATS_2017_QT02", "STATS_2017_QT03", "STATS_2017_QT04", "STATS_D10", "STATS_D21", "STATS_D250", 
            "STATS_D5", "STATS_D55", "STATS_LT", "STATS_IS", "STATS_OS",
        ]

        self.connectDB()

    def parseArguments(self):
        parser = OptionParser()

        parser.add_option("-d", "--dbPath", dest = "dbPath", default = "mongodb://10.240.1.70:27018/?replicaSet=psim", help = "The path to the database server")
        parser.add_option("-D", "--dbName", dest = "dbName", default = "pesa", help = "The name of the database to connect to")

        parser.add_option("-l", "--limit", dest = "limit", default = 0, help = "Limit the number of records")
        parser.add_option("-s", "--skip", dest = "skip", default = 0, help = "The number of records to skip")

        parser.add_option("-t", "--longShortRatioTol", dest = "longShortRatioTol", default = 0.7, help = "The ratio of long to short tolerance (longCount / shortCount). This is ALWAYS done on the LIFETIME Stats object")
        parser.add_option("-T", "--longShortSumTol", dest = "longShortSumTol", default = 0.7, help = "The sum of longs and shorts should be within this much tolerance of the universe size. The universe size is deduced from the name of the universe. This is ALWAYS done on the LIFETIME Stats object")
            
        return parser.parse_args()

    def prune(self):
        alphasColl = self.db["AlphaConfigs"]
        didPruneAll = False
        totalCount = 0

        while didPruneAll is False and (totalCount < self.limit or not self.limit):
            try:
                # cursor = alphasColl.find({ "pruned": False })

                cursor = None
                if self.limit:
                    cursor = alphasColl.find({ pruned: False }).skip(self.skip + totalCount).limit(self.limit - totalCount)
                else:
                    cursor = alphasColl.find({ pruned: False }).skip(self.skip + totalCount)

                totalPruned = 0
                toPrune = []

                for doc in cursor:
                    if "pruned" in doc and doc["pruned"]:
                        continue

                    if "pruneCheck" in doc:
                        lastTime = doc["pruneCheck"]
                        daysSinceLastCheck = (DT.datetime.now() - lastTime).days
                        if daysSinceLastCheck < 60:
                            continue

                    totalCount += 1
                    universeId = doc["universeId"]
                    import re
                    universeCount = float(re.sub('[^0-9]','', universeId))
                    stats = self.db["Stats"].find_one({
                        "_id": doc["_id"]
                    })

                    if stats:
                        longCount = stats["longCount"]
                        shortCount = stats["shortCount"]
                        ratio = 0.0
                        longShortSum = longCount + shortCount
                        if longShortSum != 0.0:
                            ratio = 1.0 - ((longCount - shortCount) / (longCount + shortCount))
                        lsRatio = longShortSum / universeCount

                        if ratio < self.longShortRatioTol or lsRatio < self.longShortSumTol:
        #                    toPrune.append(doc["_id"])
                            totalPruned += 1
                            print("[[%d - %d] %d / %d] Pruning: %s - Ratio: %0.2f, LSRatio: %0.2f" % (self.skip, self.limit, totalPruned, totalCount, doc["_id"], ratio, lsRatio))
                            alphasColl.update({
                                "_id": doc["_id"]
                            }, {
                                "$set": {
                                    "pruned": True,
                                    "pruneCheck": DT.datetime.now()
                                }
                            })

                            self.db["Stats"].update({
                                "_id": doc["_id"]
                            }, {
                                "$unset": {
                                    "vec": [],
                                }
                            })

                            for statsCollName in self.statsCollections:
                                self.db[statsCollName].remove({
                                    "_id": doc["_id"]
                                })

                didPruneAll = True

            except Exception as e:
                print(traceback.format_exc())

            #     if len(toPrune) >= 1000:
            #         print("Total to prune: %d" % (len(toPrune)))
            #         totalPruned += len(toPrune)
            #         alphasColl.update({ 
            #             "_id": { 
            #                 "$in": toPrune
            #             }
            #         }, {
            #             "$set": {
            #                 "pruned": True,
            #                 "pruneCheck": DT.datetime.now()
            #             }
            #         }, multi = True)
            #         toPrune = []

            # if len(toPrune):
            #     print("Total to prune: %d" % (len(toPrune)))
            #     alphasColl.update({ 
            #         "_id": { 
            #             "$in": toPrune
            #         }
            #     }, {
            #         "$set": {
            #             "pruned": True,
            #             "pruneCheck": DT.datetime.now()
            #         }
            #     }, multi = True)


    def connectDB(self):
        from pymongo import MongoClient
        
        if self.db is not None:
            return
            
        self.mongoClient = MongoClient(self.dbPath)
        if self.mongoClient is None:
            sys.exit(1)
        
        self.db = self.mongoClient[self.dbName]
        if self.db is None:
            sys.exit(1)

Prune().prune()