### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#!python

import os
import sys
import traceback
import time

import pymongo

import datetime as DT
from optparse import OptionParser

def dateToInt(date):
    return int(str(date.year).zfill(4) + str(date.month).zfill(2) + str(date.day).zfill(2))

def formatUuid(uuid):
    import re
    return re.sub('[^a-zA-Z0-9\n_]', '-', uuid)

# Add all the subdirectories to the search path ...
class ReadStats:
    def __init__(self):
        opt, args           = self.parseArguments()
        self.db             = None
        self.mongoClient    = None
        self.dbPath         = opt.dbPath
        self.dbName         = opt.dbName
        self.universeId     = opt.universeId

        # First thing: connect to the database
        self.connectDB()

    def parseArguments(self):
        parser = OptionParser()

        parser.add_option("-d", "--dbPath", dest = "dbPath", default = "mongodb://10.240.1.70:27018/?replicaSet=psim", help = "The path to the database server")
        parser.add_option("-D", "--dbName", dest = "dbName", default = "pesa", help = "The name of the database to connect to")

        parser.add_option("-U", "--universeId", dest = "universeId", default = "Top1000", help = "The target universes id")

        return parser.parse_args()

    def readStats(self, alphaDoc):
        statsColl           = self.db["Stats"]
        return statsColl.find_one({
            "_id": alphaDoc["_id"]
        })

    def run(self):
        alphasColl          = self.db["AlphaConfigs"]
        cursor              = self.alphasColl.find({
            "$and": [{
                "universeId": self.universeId,
                "pruned": False,
            }]
        })

        for alphaDoc in cursor:
            # read the stats
            print("Loading stats for alpha: %s" % (alphaStats["_id"]))
            alphaStats      = self.readStats(alphaDoc)

            if alphaStats is None:
                print("Unable to load stats for alpha: %s" % (alphaStats["_id"]))
                continue

            print("Alpha PNL: %0.2f" % (alphaStats["pnl"]))


    def connectDB(self):
        from pymongo import MongoClient
        
        if self.db is not None:
            return
            
        self.mongoClient = MongoClient(self.dbPath)
        if self.mongoClient is None:
            sys.exit(1)
        
        self.db = self.mongoClient[self.dbName]
        if self.db is None:
            sys.exit(1)

ReadStats().run()
