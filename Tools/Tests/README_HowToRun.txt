How to run:

1. Copy reconcile.py, bbgApi.py, alpha_file_writer.py and ConfigSampleEU.xml to the directory accessible both from Windows and Linux.
2. Go to that directory and rename file "prices.csv" if it exists.
3. In Linux, run "psim -c ConfigSampleEU.xml". Psim version is at least 0.7.8.
4. In Windows in the same directory run command "python reconcile.py prices.csv".

Diffs between psim and Bloomberg data will be saved in csv files in the same directory.
