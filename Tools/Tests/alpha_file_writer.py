### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# Hammad Khan
import sys
import traceback
import numpy as np



try:
    import Framework
    import BamUtil
    from PyBase import PyAlpha


    
    class PyFileWriter(PyAlpha):
        def __init__(self, baseObject):
            super().__init__(baseObject, "PyFileWriter")

        def loadConfig(self, logChannel = None):
            self.serverUrl = 'http://dalapi/v1/get'
            self.keyword = 'BloombergAdjustmentFactorsNonUS'
            self.yml = '\\\\fountainhead\\bam\DataAPI_Config\\uat\\OneTickEU\\Bloomberg.yml'

        def getData(self):
            serverUrl = self.serverUrl

            params = {}
            params['StartDate'] = '2013-01-01'
            params['EndDate'] = '2017-03-08'
            params['Symbols'] = { 'Symbology' : 'CFIGI', 'IDs' : 'BBG000CSMRZ2' }

            if "Keyword" not in params:
                params['Keyword'] = self.keyword

            if "yml" not in params:
                params['yml'] = self.yml

            params['ultrajson'] = "'True'"

            # url = serverUrl
            # appendKey = "?"
            # for key, value in params.items():
            #   url += appendKey + key + "=" + value
            #   appendKey = "&"

            dataValues = []
            for key, value in params.items():
                dataValues.append((key, value))
            data = dict(dataValues)

            import requests
            # from requests_ntlm import HttpNtlmAuth

            # username = dci.rt.appOptions.username
            # password = dci.rt.appOptions.password

            # assert username, "Must specify a username -u parameter on the command line for NT authentication"
            # assert password, "Must specify a password -p parameter on the command line for NT authentication"

            # self.debug(self.logChannel, "Requesting url: %s" % (serverUrl))

            responseData = requests.post(serverUrl, data = data)

            # Some error was returned
            if responseData.status_code < 200 or responseData.status_code >= 300:
                text = responseData.text.replace("{", "{{")
                text = text.replace("}", "}}")
                self.error("Server error. Response code: %d - Response Text: %s" % (responseData.status_code, text))
                return None

            import json
            jsonStr = responseData.text
            data = json.loads(jsonStr)
            return data


        def run(self, rt):
            try:
                nDays       = int(self.config().getString("nDays", "21"))
                reversion   =  int(self.config().getString("reversion","-1"))
                di          = rt.di                                               # current day
                result      = self.alpha()
                universe    = self.getUniverse()
                adjClosePrice = rt.dataRegistry.floatData(universe, "baseData.local_adjClosePrice")
                usdClosePrice = rt.dataRegistry.floatData(universe, "baseData.adjClosePrice")
                adjOpenPrice = rt.dataRegistry.floatData(universe, "baseData.local_adjOpenPrice")
                adjHighPrice = rt.dataRegistry.floatData(universe, "baseData.local_adjHighPrice")
                adjLowPrice = rt.dataRegistry.floatData(universe, "baseData.local_adjLowPrice")
                volume      = rt.dataRegistry.floatData(universe,"baseData.adjVolume")
                returns     = rt.dataRegistry.floatData(universe,"baseData.local_returns")
                returns5    = rt.dataRegistry.floatData(universe,"baseData.local_returns5")
                adv5        = rt.dataRegistry.floatData(universe,"baseData.adv5")
                mcap        = rt.dataRegistry.floatData(universe,"baseData.local_adjMCap")
                adjVWAP     = rt.dataRegistry.floatData(universe,"baseData.local_adjVWAP")

                numStocks   = result.cols()

                sector      = Framework.StringData.script_cast(rt.dataRegistry.getData(universe, "classification.bbIndustryGroup"))

                f = open("prices.csv","a+")    
                date    = rt.getDate(rt.di)
                
                # self.loadConfig()
                # self.getData()
                
                #temp = np.asmatrix(returns)
                #print("############################")
                #print(np.size(temp))
                #print(di)
                #print(type(temp))
                #print(dir(temp))
                #print(np.asarray(adjClosePrice.rowArray(di)))
                #print(np.asarray(adjClosePrice.colArray(0)))
                #print(np.size(np.asarray(adjClosePrice.rowArray(di))))
                #print(np.size(np.asarray(adjClosePrice.colArray(1))))
                #print(numStocks)
                                   
                
                for ii in range(numStocks):
                    sec = universe.security(di, ii)
                    ret     = returns.getValue(di,ii)
                    ret5    = returns5[di,ii]
                    sumrets = np.sum(returns.getValue(di - i, ii) for i in range(5) )
                    adj_cp  = adjClosePrice.getValue(di,ii)
                    usd_adj_cp  = usdClosePrice.getValue(di,ii)
                    adj_op  = adjOpenPrice.getValue(di,ii)
                    adj_hp  = adjHighPrice.getValue(di,ii)
                    adj_lp  = adjLowPrice.getValue(di,ii)
                    adj_vwap = adjVWAP.getValue(di,ii)
                    vol     = volume.getValue(di,ii)
                    cap     = mcap[di,ii]
                    thisSector = sector.getValue(0,ii)
                    thisadv5 = adv5[di,ii]
                    meanvols = np.mean( [ volume.getValue(di - i,ii) for i in range(5) ] )

                    y_adj_cp = adjClosePrice.getValue(di - 1, ii)
                    
                    sec     = universe.security(di, ii)
                    #secInfo = universe.securityInfo(di,ii)
                    ## historical ticker map:
                    
                    ticker  = sec.tickerStr()
                    figi    = sec.uuidStr()
                    bbTicker= sec.bbTickerStr()
                    
                    if universe.isValid(di,ii):
                        valid="V"
                    else:
                        valid="I"

                    if abs(ret) > 1:
                        self.debug("DATE: %s - " % (date))
                        self.debug("Ticker: %s - " % (ticker))
                        self.debug("bbTicker: %s - " % (bbTicker))
                        self.debug("returns: %0.4f - " % (1e4*ret))


                    try:
                        if str(date) == str(20161003) and bbTicker == 'MOR GR':
                            print(date, bbTicker, sec.isHolidayInExchange(rt, di))
                        dayIsOk = not sec.isHolidayInExchange(rt, di)
                    except:
                        dayIsOk = False

                    if dayIsOk: 
                        l = [date,bbTicker,figi,adj_cp,usd_adj_cp,adj_op,adj_hp,adj_lp,y_adj_cp,sumrets,ret,ret5,cap,vol,meanvols,thisadv5,thisSector,valid]
                        str_to_write = ",".join( [ str(x) for x in l ] )
                        f.write(str_to_write)
                        f.write("\n")

                    if universe.isValid(di,ii):                                    # current universe as universe has to be valid on that date
                        if self.stddev(returns,di,ii,nDays) > 0:
                            value = self.mean(returns,di,ii,nDays)/self.stddev(returns,di,ii,nDays)  # data is delayed so using delayed di
                            value = value * reversion
                            
                        result.setValue(0,ii,float(value))
                
                    # if value is bad then use: result.makeInvalid(0, ii)
                
            except Exception as e:
                print ("I am at Exception")
                print(traceback.format_exc())
            
            #f.write("#####################################\n")
            f.close()
except Exception as e:
    print(traceback.format_exc())

