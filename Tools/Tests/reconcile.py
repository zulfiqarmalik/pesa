### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import pandas as pd
import numpy as np
from datetime import datetime

pd.options.mode.chained_assignment = None

def getCurrencyPair(ccy1, ccy2):
    ccy1 = ccy1.upper()
    ccy2 = ccy2.upper()
    strongest = ['EUR','GBP','AUD','NZD','USD']
    try:
        index1 = strongest.index(ccy1)
    except:
        index1 = 100 # a large number, doesn't matter what
    try:
        index2 = strongest.index(ccy2)
    except:
        index2 = 100
    return ccy1 + ccy2 if index1 < index2 else ccy2 + ccy1

def gather_stats(df, name):
    def count(df, name, f, values_group_name):
        df['COUNT'] = df[name].apply(lambda x : 1 if f(x) else 0)
        print(values_group_name, ':', df['COUNT'].sum())
    print('Statistics for the data field', name)
    count(df, name, np.isfinite, 'Finite')
    count(df, name, np.isnan, 'NAN')
    count(df, name, lambda x : not np.isfinite(x) and not np.isnan(x), 'Infinite' )
    count(df, name, lambda x : x < 0, 'Negative')
    count(df, name, lambda x : x == 0, 'Zero')
    df.drop('COUNT', axis = 1)

def abs_diff(a, b):
    val = np.abs(a-b)
    return val if np.isfinite(val) else 0

def rel_diff(a, b):
    val = np.abs(a-b) / max(a, b)
    return val if np.isfinite(val) else 0

def compare_columns(df, name1, name2, diff_func, verbose = False):
    def diff_pd(x):
        return diff_func(x[name1], x[name2])
    dfc = df[['date', 'bbTicker', 'figi', name1, name2]]
    dfc['diff'] = dfc.apply(diff_pd, axis = 1)
    dfc.dropna(inplace = True)
    try:
        dfc.sort_values(by = 'diff', inplace = True, ascending  = False)
    except:
        dfc.sort(columns = 'diff', inplace = True, ascending  = False)
    if verbose:
        gather_stats(df, name1)
        gather_stats(df, name2)
        print('Max differences between non-NAN values of', name1, 'and', name2, ':')
        print(dfc.head())
    return dfc.head()

def check_returns(df_our):
	df_our['price_move'] = df_our['adj_cp'] / df_our['y_adj_cp']
	df_our['ret_from_close'] = df_our['price_move'] - 1.0
	compare_columns(df_our, 'ret', 'ret_from_close', abs_diff, True)

def load_bbg_data(tickers, fields, startdate, enddate):
    try:
        from bbgApi import BPIPE
        b = BPIPE()
        CHUNK_SIZE = 300
        chunks = len(tickers) // CHUNK_SIZE
        dfs = []
        for i in range(chunks + 1):
            start = i * CHUNK_SIZE
            end = start + CHUNK_SIZE
            if end > len(tickers):
                end = len(tickers)
            print('Getting', start, '-', end)
            dfs.append(b.get_historical(tickers[start:end], fields, start = str(startdate), end = str(enddate)).as_frame())
            print('done')
        res = pd.concat(dfs, axis = 1)
        res.to_pickle('eu_bbg.pkl')
    except:
        print('Could not load BBG local prices, reading pickled file..')

def load_bbg_ref_and_spot(tickers, venues, startdate, enddate):
    try:
        from bbgApi import BPIPE
        t = BPIPE()
        ref = t.get_reference_data(tickers, 'CRNCY').as_frame()
        ref['CCYPAIR'] = ref['CRNCY'].apply(getCurrencyPair, args=('USD',))
        uniquePairs = ref['CCYPAIR'].unique()
        ccyTickers = [u + ' ' + y + ' Curncy' for u in uniquePairs for y in venues]
        spotRates = t.get_historical(ccyTickers, 'PX_LAST', start = str(startdate), end = str(enddate)).as_frame()
        ref.to_pickle('ref.pkl')
        spotRates.to_pickle('spotRates.pkl')
    except:
        print('Could not load BBG refdata and spot ccy prices, reading pickled files..')

def get_ref_data():
    df = pd.read_pickle('ref.pkl')
    return df

def get_spots():
    df = pd.read_pickle('spotRates.pkl')
    df = df.swaplevel(0, 1, axis = 1)
    return df['PX_LAST']

def get_usd_rate(px_last, ccy_venue):
    ref = get_ref_data()
    ccys = { row[0] : row[1] for row in ref[['CRNCY']].itertuples() }
    mults = { k : (1 if v == v.upper() else 100) for k, v in ccys.items() }
    ccypairs = { row[0] : row[1] for row in ref[['CCYPAIR']].itertuples() }
    spots = get_spots()
    l = []
    for bbg_symbol in px_last.columns:
        bbg_reduced = pd.DataFrame(px_last[bbg_symbol]).reset_index()
        ccy_pair = ccypairs[bbg_symbol]
        ccy_col_name = ' '.join( [ ccy_pair, ccy_venue, 'Curncy' ] )
        spots_reduced = pd.DataFrame(spots[ccy_col_name]).reset_index()
        m = pd.merge(bbg_reduced, spots_reduced, on = 'date', how = 'inner')
        mult = mults[bbg_symbol]
        m['EXCHRATE_unadj'] = m[ccy_col_name].apply(lambda x: 1.0 / x if ccy_col_name.startswith('USD') else x)
        m['EXCHRATE'] = m['EXCHRATE_unadj'] / mult
        to_drop = [ c for c in m.columns if c != 'EXCHRATE' and c != 'date' ]
        m = m.drop(to_drop, axis = 1)
        m = m.rename(columns = { 'EXCHRATE' : bbg_symbol })
        m = m.set_index('date')
        l.append(m)   
    ll = pd.concat(l, axis = 1)
    return ll

def parse_date(date):
	return datetime(year = date // 10000, month = date % 10000 // 100, day = date % 100).date()

def check_bbg_single(df_our, df_bbg, our_column_name, file_name = None):
    our_symbols = np.unique(df_our['bbTicker'])
    not_found_in_bbg = []
    diffs = []
    for symbol in our_symbols:
        bbg_symbol = symbol + ' Equity'
        if bbg_symbol not in df_bbg.columns:
            not_found_in_bbg.append(symbol)
        else:
            bbg_reduced = pd.DataFrame(df_bbg[bbg_symbol])
            bbg_reduced.rename(columns = { bbg_symbol : 'bbg_' + our_column_name }, inplace = True)
            bbg_reduced.reset_index(inplace = True)
            our_reduced = df_our[df_our['bbTicker'] == symbol]
            our_reduced = our_reduced[['date', 'bbTicker', 'figi', our_column_name]]
            our_reduced['date_d'] = our_reduced['date'].apply(parse_date)
            bbg_reduced['date_d'] = bbg_reduced['date'].apply(lambda x : x.date())
            bbg_reduced.drop('date', axis = 1, inplace = True)
            merged = pd.merge(our_reduced, bbg_reduced, how = 'inner', on = 'date_d')
            diffs.append(compare_columns(merged, our_column_name, 'bbg_' + our_column_name, rel_diff))
    print('Symbols not found in bbg:', len(not_found_in_bbg), 'in total')
    print('Symbols found in bbg', len(our_symbols) - len(not_found_in_bbg))
    diffs = pd.concat(diffs)
    try:
        diffs.sort_values(by = 'diff', inplace = True, ascending  = False)
    except:
        diffs.sort(columns = 'diff', inplace = True, ascending  = False)
    diffs.reset_index(inplace = True)
    print(diffs.head(n = 25))
    if not file_name:
        file_name = 'diffs_' + our_column_name
    diffs.to_csv(file_name + '.csv')

def check_bbg(df_our, df_bbg_multi, our_column_name, bbg_column_name):
    print('Checking', our_column_name, 'and', bbg_column_name)
    check_bbg_single(df_our, df_bbg_multi[bbg_column_name], our_column_name)

import sys
eu_our_name = sys.argv[1]
eu_our_cols = ['date', 'bbTicker', 'figi', 'adj_cp', 'usd_adj_cp', 'adj_op', 'adj_hp', 'adj_lp', 'y_adj_cp', 'sumrets', 'ret', 'ret5', 'cap', 'vol', 'meanvols', 'thisadv5', 'thisSector', 'valid']
print('Reading empty-header CSV from file', eu_our_name, 'with columns', eu_our_cols)
eu_our = pd.read_csv(eu_our_name, header = None, names = eu_our_cols)

#load bbg data
dates = sorted(np.unique(eu_our['date']))
print('Dates:', dates[0], '-', dates[-1])
tickers = np.unique(eu_our['bbTicker'])
print('Tickers', tickers)
tickers = [ t + ' Equity' for t in tickers ]
fields = ['PX_LAST', 'PX_CLOSE_1D', 'PX_HIGH', 'PX_LOW', 'PX_OPEN', 'CUR_MKT_CAP', 'PX_VOLUME']
load_bbg_data(tickers, fields, dates[0], dates[-1])
eu_bbg = pd.read_pickle('eu_bbg.pkl')
eu_bbg = eu_bbg.swaplevel(0, 1, axis = 1)

#validating our columns
check_returns(eu_our)
compare_columns(eu_our, 'sumrets', 'ret5', abs_diff, verbose = True)
compare_columns(eu_our, 'meanvols', 'thisadv5', rel_diff, verbose = True)

#compare with bbg
check_bbg(eu_our, eu_bbg, 'adj_cp', 'PX_LAST')
check_bbg(eu_our, eu_bbg, 'cap', 'CUR_MKT_CAP')
check_bbg(eu_our, eu_bbg, 'vol', 'PX_VOLUME')
check_bbg(eu_our, eu_bbg, 'adj_op', 'PX_OPEN')
check_bbg(eu_our, eu_bbg, 'adj_hp', 'PX_HIGH')
check_bbg(eu_our, eu_bbg, 'adj_lp', 'PX_LOW')

#checking usd prices
venues = [ 'BGN' ] #looks better than CMPL and CMPN
load_bbg_ref_and_spot(tickers, venues, dates[0], dates[-1])
for venue in venues:
    usd_rate = get_usd_rate(eu_bbg['PX_LAST'], venue)
    eu_our['usd_rate'] = eu_our['usd_adj_cp'] / eu_our['adj_cp']
    check_bbg_single(eu_our, usd_rate, 'usd_rate', 'diffs_usd_rate_' + venue)
