#!/bin/bash
source $HOME/.bashrc
cd $PSIM_DIR/Tools/Data/Download

python ./dl_ms_new_del_symbols.py -S20180101 -x"US:XNYS = 14 | US:XPSE = 16 | US:XNAS = 19 | US:COMP = 126 | CA:XSTE = 127" -o"$HOME/drv/devwork/data/Vendors/MorningStar/NewDelSymbols/North-America/\$EXCHANGE/\$YYYY/\$MM" -u"$MS_USER" -p"$MS_PASS"

python ./dl_ms_new_del_symbols.py -S20180101 -x"GB:XLON = 151 | DE:XFRA = 213 | FR:XPAR = 160 | ES:XBAR = 199 | NL:XAMS = 202 | BE:XBRU = 207 | PT:XLIS = 195 | IE:XDUB = 190 | CH:XSWX = 182 | SE:XSTO = 170 | DK:XCSE = 172 | FI:XHEL = 176 | NO:XOSL = 174 | AT:XWBO = 194" -o"$HOME/drv/devwork/data/Vendors/MorningStar/NewDelSymbols/Europe/\$EXCHANGE/\$YYYY/\$MM" -u"$MS_USER" -p"$MS_PASS"

python ./dl_ms_new_del_symbols.py -S20180101 -x"JP:XTKS = 133 | KR:XKRX = 141 | HK:XHKG = 134 | JD:XJKT = 140 | MY:XKLS = 135 | PH:XPHS = 142 | SG:XSES = 143 | TW:XTAI = 144 | HK:XHKG = 134 | AU:XASX = 146 | IN:XBOM = 139 | IN:XNSE = 138" -o"$HOME/drv/devwork/data/Vendors/MorningStar/NewDelSymbols/Asia-Pacific/\$EXCHANGE/\$YYYY/\$MM" -u"$MS_USER" -p"$MS_PASS"


