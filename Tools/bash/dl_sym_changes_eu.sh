#!/bin/bash
source $HOME/.bashrc
cd $PSIM_DIR/Tools/Data/Download

python ./dl_ms_new_del_symbols.py -S20180101 -x"GB:XLON = 151 | DE:XFRA = 213 | FR:XPAR = 160 | ES:XBAR = 199 | NL:XAMS = 202 | BE:XBRU = 207 | PT:XLIS = 195 | IE:XDUB = 190 | CH:XSWX = 182 | SE:XSTO = 170 | DK:XCSE = 172 | FI:XHEL = 176 | NO:XOSL = 174 | AT:XWBO = 194" -o"$HOME/drv/devwork/data/Vendors/MorningStar/NewDelSymbols/Europe/\$EXCHANGE/\$YYYY/\$MM" -u"$MS_USER" -p"$MS_PASS"


