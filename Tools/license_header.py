### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys
import logging
from optparse import OptionParser
import datetime as DT
import glob
import traceback

import matplotlib
matplotlib.use("Agg")

import matplotlib.ticker as mtick
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

def parseArguments():
    parser = OptionParser()

    parser.add_option("-d", "--dir", dest = "dir", default = "./", help = "The directory for which the license needs to be added.")
    parser.add_option("-f", "--fileTypes", dest = "fileTypes", default = "*.h,*.cpp", help = "Comma separated list of extensions where license header needs to be added.")
    parser.add_option("-l", "--license", dest = "license", default = "./license.txt", help = "The path to the file where the license is.")
    parser.add_option("-e", "--exclude", dest = "exclude", default = "Thirdparty,build", help = "Any child of Thirdparty.")
        
    return parser.parse_args()

def isExcluded(filename, excludeList):
    for e in excludeList:
        if e in filename: 
            return True

    return False

def updateLicense(filename, licenseStr):
    print(filename)
    fileContent = ""

    with open(filename) as contentFile:
        fileContent = contentFile.read()

    if filename.endswith(".xml"):
        fileContent = fileContent.replace('<?xml version="1.0" encoding="UTF-8"?>', '')
        fileContent = '<?xml version="1.0" encoding="UTF-8"?>\n\n' + fileContent

    fileContent = licenseStr + fileContent
    with open(filename, "w") as contentFile:
        contentFile.write(fileContent)

def run():
    options, args = parseArguments()

    fileTypes = options.fileTypes.split(',')
    excludeList = options.exclude.split(',')
    print("Exclude list: %s" % (str(excludeList)))

    licenseStr = ""
    with open(options.license) as licenseFile:
        licenseStr = licenseFile.read()

    assert licenseStr, "Empty license in file: %s" % (options.license)

    print(licenseStr)

    for fileType in fileTypes:
        searchString = options.dir + "/**/" + fileType
        print("Recursively searching: %s" % (searchString))

        for filename in glob.iglob(searchString, recursive=True):
            if isExcluded(filename, excludeList) is False:
                updateLicense(filename, licenseStr)
try:
    run()
except Exception as e:
    print(traceback.format_exc())
    sys.exit(1)
