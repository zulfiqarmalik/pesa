### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys
import logging
from optparse import OptionParser
import datetime as DT

import xml.etree.ElementTree as ET
from xml.etree.ElementTree import Comment
from xml.dom import minidom

def parseArguments():
    parser = OptionParser()

    parser.add_option("-o", "--outputFilename", dest = "outputFilename", default = "./merged.xml", help = "The name of the output file")
    return parser.parse_args()


def loadConfig(configFilename):
    refXml = ET.parse(configFilename).getroot()

    # get to the portfolio and alphas element
    portfolioElt = refXml.find("Portfolio")
    assert portfolioElt is not None, "Unable to load the portfolio element!"

    alphasElt = portfolioElt.find("Alphas")
    assert alphasElt is not None, "Unable to load the alphas element!"

    return refXml, portfolioElt, alphasElt

def run():
    options, args = parseArguments()
    filename = args[-1]
    filenames = args

    assert len(filenames), "Must specify at least one filename to plot!"

    sources = []
    baseXml = None
    baseAlphas = None

    for filename in filenames:
        xml, portfolioElt, alphasElt = loadConfig(filename)
        assert xml, "Unable to load file: %s" % (filename)

        sources.append({
            "filename": filename,
            "refXml": xml,
            "portfolioElt": portfolioElt,
            "alphasElt": alphasElt
        })

        if baseXml is None:
            baseXml = xml
            baseAlphas = alphasElt

        else:
            for alphaElt in list(alphasElt):
                if alphaElt.tag == "Alpha":
                    baseAlphas.append(alphaElt)

            
    # then push the output to it ...
    outputFilename = options.outputFilename
    print("Writing to file: %s" % (outputFilename))

    xmlString = ET.tostring(baseXml, 'utf-8')
    reparsed = minidom.parseString(xmlString)
    xmlPrettyString = reparsed.toprettyxml(indent="    ")
    file = open(outputFilename, "w")

    file.write(xmlPrettyString)
    file.close()

try:
    run()
except Exception as e:
    print(e)
    sys.exit(1)
