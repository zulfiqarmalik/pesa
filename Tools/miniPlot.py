### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys
import logging
from optparse import OptionParser
import datetime as DT
from glob import glob
import traceback

import matplotlib
matplotlib.use("Agg")

import matplotlib.ticker as mtick
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

def parseArguments():
    parser = OptionParser()

    parser.add_option("-p", "--pdf", dest = "pdf", action="store_true", default = False, help = "Whether to store and email PDF")
    parser.add_option("-s", "--subject", dest = "subject", default = None, help = "The subject of the email")
    parser.add_option("-f", "--file", dest = "filename", help = "Write to PNG", metavar = "FILE")
    parser.add_option("-e", "--email", dest = "email", default = None, help = "Email adderess to send to")
    parser.add_option("-S", "--startDate", dest = "startDate", default = None, help = "Start date")
    parser.add_option("-E", "--endDate", dest = "endDate", default = None, help = "End date")

    return parser.parse_args()

def getStratName(filename, suffix = ""):
    filename = os.path.abspath(filename)
    name = os.path.basename(filename)
    if name == "MyPort.pnl":
        return getStratName(os.path.dirname(filename), " - FINAL")
    elif name == "MyPort_raw.pnl":
        return getStratName(os.path.dirname(filename), " - RAW")
    elif name == "MyPort_pre_opt.pnl":
        return getStratName(os.path.dirname(filename), " - PRE-OPT")
    elif name == "pnl":
        return getStratName(os.path.dirname(filename), suffix)

    return os.path.splitext(name)[0] + suffix

def plot(plt, filename, startDate = 0, endDate = 0, noNewFig = False):
    file = open(filename, "r")
    lines = file.readlines()
    
    pnls = []
    dates = []
    cumPnl = 0.0
    addInfo = []
    addInfoIndex = -1
    numLines = len(lines)

    osStartDate = None
    osPnls = []
    osDates = []

    lcumPnl = 0.0

    annualPnls = []

    for i in range(1, numLines):
        line = lines[i].strip()
        if not line or line.startswith("#"):
            addInfoIndex = i
            break

        parts = line.split(",")
        date = parts[0].strip()
        idate = int(date)

        if startDate != 0 and idate < startDate:
            continue

        if endDate != 0 and idate > endDate:
            continue

        if idate == 0:
            continue

        date = DT.datetime.strptime(str(date), "%Y%m%d")
        year = date.year
        if 'nan' in parts[1] or 'ind' in parts[1]:
            pnl = 0.0
        else:
            pnl = float(parts[1].strip())

        lcumPnl += pnl
        dates.append(date)
        pnls.append(lcumPnl)

    # plot
    fname = getStratName(filename)
    line, = plt.plot(dates, pnls, label = fname, color = 'w', linewidth=2.0)

    # plt.title(fname)

    # plt.tick_params(axis = 'x', which = 'both', labelbottom = 'off', labeltop = 'on')
    return line, fname, addInfo
    

def run():
    options, args = parseArguments()
    # filename = args[-1]
    # filenames = args
    filenames = []
    isMultiSplit = False

    for i in range(len(args)):
        arg = args[i]
        if '@' not in arg:
            afilenames = glob(arg)
            filenames += afilenames
        else:
            isMultiSplit = True
            filenames.append(arg)

    assert len(filenames), "Must specify at least one filename to plot!"

    filenames.sort()
    
    lines = []  
    names = []

    startDate = 0 if options.startDate is None else int(options.startDate)
    endDate = 0 if options.endDate is None else int(options.endDate)

    addInfos = []

    pdfDocument = None
    pdfDocName = "psim_plot.pdf"
    if options.filename:
        pdfDocName = options.filename
    elif len(filenames) == 1:
        getStratName(filenames[0]) 

    mainFigures = []
    mainTableFigure = None

    addInfos = []
    for filename in filenames:
        line, name, addInfo = plot(plt, filename, startDate = startDate, endDate = endDate)
        lines.append(line)
        names.append(name)
        addInfos.append(addInfo)
    
    # beautify the x-labels
    plt.gcf().autofmt_xdate()
    # plt.xlabel("Dates")
    # plt.ylabel("PNL")
    
    # plt.text(3, 8, 'boxed italics text in data coords', style='italic', bbox={'facecolor':'red', 'alpha':0.5, 'pad':10})

    ax = plt.axes()
    # ax.xaxis.set_ticks_position('top')
    ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))
    
    # plt.legend(lines, names, loc = 2) # upper left 
    plt.axis("off")
    mng = plt.get_current_fig_manager()

    if pdfDocument and len(filenames) > 1:
        mainTableFigure = plt.figure()

    # mng.window.showMaximized()
    rowLabels = []
    tableVals = []

    defSize = plt.gcf().get_size_inches()
    # plt.gcf().set_size_inches((defSize[0] * 2.5, defSize[1] * 2.5))
    # print("Writing filename: %s" % (options.filename))
    plt.savefig(options.filename, transparent=True)

try:
    run()
except Exception as e:
    print(traceback.format_exc())
    sys.exit(1)
