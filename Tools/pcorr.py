### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys
import traceback
import logging
from optparse import OptionParser
import datetime as DT
from glob import glob
import numpy as np
import operator

def parseArguments():
    parser = OptionParser()

    parser.add_option("-e", "--email", dest = "email", default = None, help = "Email adderess to send to")
    parser.add_option("-s", "--subject", dest = "subject", default = "Correlation", help = "The subject")
    parser.add_option("-S", "--startDate", dest = "startDate", default = 0, help = "Start date for the correlation")
    parser.add_option("-E", "--endDate", dest = "endDate", default = 0, help = "End date for the correlation")
        
    return parser.parse_args()

def getStratName(filename, suffix = ""):
    filename = os.path.abspath(filename)
    name = os.path.basename(filename)

    if name == "MyPort.pnl":
        return getStratName(os.path.dirname(filename))
    elif name == "MyPort_raw.pnl":
        return getStratName(os.path.dirname(filename))
    elif name == "MyPort_pre_opt.pnl":
        return getStratName(os.path.dirname(filename))
    elif name == "pnl":
        return getStratName(os.path.dirname(filename), suffix)

    return os.path.splitext(name)[0] + suffix

def readPnlFile(filename):
    file = open(filename, "r")
    lines = file.readlines()
    
    pnls = []
    dates = []
    numLines = len(lines)

    for i in range(1, numLines):
        line = lines[i].strip()
        if not line or line.startswith("#"):
            break

        parts = line.split(",")
        date = parts[0].strip()
        date = int(date)
        pnl = float(parts[3].strip())
        dates.append(date)
        pnls.append(pnl)

    name = getStratName(filename)

    return {
        "name": name,
        "filename": filename,
        "pnls": pnls,
        "dates": dates,
        "corrs": []
    }

def readPnlFiles(pnlData, filenames):
    for filename in filenames:
        if filename not in pnlData:
            pnlData[filename] = readPnlFile(filename)

def findDateIndex(d, date):
    for i in range(0, len(d["dates"])):
        if d["dates"][i] == date:
            return i
        elif i > 0 and d["dates"][i - 1] < date and d["dates"][i] > date:
            return i

    return -1

def calculateCorrelation(d0, d1, startDate = 0, endDate = 0):
    # print(d0["name"], d1["name"])

    d0_istartDate       = 0
    d0_iendDate         = 0

    if startDate == 0:
        startDate       = max(d0["dates"][0], d1["dates"][0])

    if endDate == 0:
        endDate         = min(d0["dates"][-1], d1["dates"][-1])

    d0_istartDate       = findDateIndex(d0, startDate)
    d0_iendDate         = findDateIndex(d0, endDate)

    startDate           = d0["dates"][d0_istartDate]
    endDate             = d0["dates"][d0_iendDate]

    # if d0_iendDate != len(d0["dates"]): 
    #     d0_iendDate     += 1

    d1_istartDate       = findDateIndex(d1, startDate)
    d1_iendDate         = findDateIndex(d1, endDate)

    if d1_istartDate < 0 or d1_iendDate < 0:
        # print("Incompatible strategy: %s" % (d1["name"]))
        # d0["corrs"].append({
        #     "pnlData": d1, 
        #     "corr": np.nan
        # })
        # d1["corrs"].append({
        #     "pnlData": d1, 
        #     "corr": np.nan
        # })
        return False

    # if d1_iendDate != len(d1["dates"]): 
    #     d1_iendDate     += 1

    if d0_istartDate < 0 or d0_iendDate < 0 or d0_istartDate >= d0_iendDate:
        return False
        
    if d1_istartDate < 0 or d1_iendDate < 0 or d1_istartDate >= d1_iendDate:
        return False

    # assert d0_istartDate >= 0 and d0_iendDate >= 0 and d0_istartDate < d0_iendDate, "%s - Invalid d0 date range: [%d - %d, %d - %d]" % (d0["name"], startDate, d0_istartDate, endDate, d0_iendDate)
    # assert d1_istartDate >= 0 and d1_iendDate >= 0 and d1_istartDate < d1_iendDate, "%s - Invalid d1 date range: [%d - %d, %d - %d]" % (d0["name"], startDate, d1_istartDate, endDate, d1_iendDate)

    # print(d0["dates"][d0_istartDate], d1["dates"][d1_istartDate])
    # print(d0["dates"][d0_iendDate], d1["dates"][d1_iendDate])
    # print("%s x %s - %d / %d / %d - %d / %d / %d" % (d0["name"], d1["name"], startDate, d0_istartDate, d1_istartDate, endDate, d0_iendDate, d1_iendDate))
    # print("%d, %d" % (d0_iendDate - d0_istartDate, d1_iendDate - d1_istartDate))
    corr = np.corrcoef(d0["pnls"][d0_istartDate:d0_iendDate], d1["pnls"][d1_istartDate:d1_iendDate])[0][1]

    d0["corrs"].append({
        "pnlData": d1, 
        "corr": corr
    })

    d1["corrs"].append({
        "pnlData": d0, 
        "corr": corr
    })

    return True

    # print(corr)

def run():
    options, args = parseArguments()

    startDate = int(options.startDate) if options.startDate else 0
    endDate = int(options.endDate) if options.endDate else 0

    srcFilenames = glob(args[0])

    # if len(args) > 1:
    srcFilenames = []

    for i in range(0, len(args)):
        arg = args[i]
        srcFilenames.append(arg)
        # filenames = glob(arg)
        # dstFilenames += filenames
    # else: 
    #     # cross correlation
    #     dstFilenames = srcFilenames

    dstFilenames = srcFilenames
    pnlData = {}

    readPnlFiles(pnlData, srcFilenames)
    readPnlFiles(pnlData, dstFilenames)

    corrInfo = []
    done = {}
    numSrcFiles = len(srcFilenames)
    numDstFiles = len(dstFilenames)

    print(srcFilenames)
    print(dstFilenames)

    for si in range(numSrcFiles):
        srcFilename = srcFilenames[si]
        for di in range(numDstFiles):
            dstFilename = dstFilenames[di]
            d0 = pnlData[srcFilename]
            d1 = pnlData[dstFilename]
            key1 = srcFilename + "-" + dstFilename
            key2 = dstFilename + "-" + srcFilename

            if srcFilename != dstFilename and key1 not in done and key2 not in done:
                done[key1] = True
                done[key2] = True

                if calculateCorrelation(d0, d1, startDate, endDate):
                    if not np.isnan(d0["corrs"][-1]["corr"]):
                        corrInfo.append({
                            "d0": d0, 
                            "d1": d1,
                            "corr": d0["corrs"][-1]["corr"]
                        })

    corrInfo = sorted(corrInfo, key = operator.itemgetter("corr")) #, reverse = True)

    msgStr = ""

    for ci in corrInfo:
        msgStr += str.format("%0.6f - %12s - %s\n" % (ci["corr"], ci["d0"]["name"], ci["d1"]["name"]))


    if options.email:        
        import smtplib
        from os.path import basename
        from email.mime.application import MIMEApplication
        from email.mime.multipart import MIMEMultipart
        from email.mime.text import MIMEText
        from email.utils import COMMASPACE, formatdate

        msg = MIMEMultipart()
        msg['From'] = "PCORR@bamfunds.com"
        msg['To'] = options.email
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = options.subject

        msgStr = msgStr.replace("\n", "<br>")

        body = '<font face="Courier New, Courier, monospace">' + msgStr + '</font>'
        msg.attach(MIMEText(body, "html"))
        # msg.attach(MIMEText(msgStr))

        smtp = smtplib.SMTP("bamsmtp")
        smtp.sendmail("PCORR@bamfunds.com", options.email, msg.as_string())
        smtp.close()     

        print("Email sent: %s" % (options.email))
    else:
        print(msgStr)

try:
    run()
except Exception as e:
    print(traceback.format_exc())
    sys.exit(1)
