### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys
import logging
from optparse import OptionParser
import datetime as DT
from glob import glob
import traceback

import matplotlib
matplotlib.use("Agg")

import matplotlib.ticker as mtick
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

def parseArguments():
    parser = OptionParser()

    parser.add_option("-a", "--annual", dest = "annual", action="store_true", default = False, help = "Make annual plots as well")
    parser.add_option("-p", "--pdf", dest = "pdf", action="store_true", default = False, help = "Whether to store and email PDF")
    parser.add_option("-s", "--subject", dest = "subject", default = None, help = "The subject of the email")
    parser.add_option("-f", "--file", dest = "filename", help = "Write to PNG", metavar = "FILE")
    parser.add_option("-e", "--email", dest = "email", default = None, help = "Email adderess to send to")
    parser.add_option("-S", "--startDate", dest = "startDate", default = None, help = "Start date")
    parser.add_option("-E", "--endDate", dest = "endDate", default = None, help = "End date")

    # parser.add_option("-c", "--config", dest = "config", default = "./configs/c_default.py", help = "What config to use for extra settings", metavar = "FILE")

    # parser.add_option("-d", "--dbPath", dest = "dbPath", default = "mongodb://localhost:27017/", help = "The path to the database server")
    # parser.add_option("-D", "--dbName", dest = "dbName", default = "pesa", help = "The name of the database to connect to")

    # parser.add_option("-I", "--id", dest = "id", default = "MASTER", help = "What is the ID of the application. This is helpful in logging")
    # parser.add_option("-L", "--logLevel", dest = "logLevel", default = "debug", help = "What log level to set. Choose from [debug, info, warn]")
    # parser.add_option("-M", "--master", action = "store_true", dest = "master", default = False, help = "Run the application in master mode")

    # parser.add_option("-n", "--numSlaves", dest = "numSlaves", default = 0, help = "How many slave processes to launch on this machine. 0 = number of cores on the machine")
    # parser.add_option("-N", "--nodb", action = "store_true", dest = "nodb", default = False, help = "Do not connect to the database")

    # parser.add_option("-p", "--port", dest = "port", default = 8000, help = "What port to run on")
    # # parser.add_option("-P", "--masterPort", dest = "masterPort", default = 8000, help = "What is the port of the master that we're going to connect to")

    # parser.add_option("-Q", "--quiet", action = "store_false", dest = "verbose", default = True, help = "Don't print status messages to stdout")
    # parser.add_option("-S", "--slave", action = "store_true", dest = "slave", default = False, help = "Run the application in slave mode")

    # parser.add_option("-X", "--noSlaves", action = "store_true", dest = "noSlaves", default = False, help = "Instructs the master server to NOT spawn any slave processes")
        
    return parser.parse_args()

DW = 0.04
colLabels = ["BookSize", "Long", "Short", "PNL",  "IR / Sharpe", "Returns %", "TVR %", "MaxDD %", "MaxDH", "HitRatio", "Mrgn BPS", "Mrgn CPS"]
colWidths = [DW * 1.5,   DW,     DW,      DW * 2, DW * 2,        DW,           DW,     DW,        DW,      DW,         DW,         DW]
import math
sharpeConst = math.sqrt(252)
    
def getStratName(filename, suffix = ""):
    filename = os.path.abspath(filename)
    name = os.path.basename(filename)
    if name == "MyPort.pnl":
        return getStratName(os.path.dirname(filename), " - FINAL")
    elif name == "MyPort_raw.pnl":
        return getStratName(os.path.dirname(filename), " - RAW")
    elif name == "MyPort_pre_opt.pnl":
        return getStratName(os.path.dirname(filename), " - PRE-OPT")
    elif name == "pnl":
        return getStratName(os.path.dirname(filename), suffix)

    return os.path.splitext(name)[0] + suffix

def plotTable(name, addInfo):
    if len(addInfo) == 0:
        return
        
    rowLabels = []
    tableVals = []
    for stats in addInfo:
        statsName = stats[0]

        if statsName.startswith("Y") or statsName.startswith("LT") or statsName.startswith("OutSample"):
            rowLabels.append(name + "-" + statsName)
            row = []
            for i in range(1, len(stats)):
                row.append(stats[i])

            tableVals.append(row)

    table = plt.table(cellText = tableVals,
                      colWidths = colWidths,
                      rowLabels = rowLabels,
                      colLabels = colLabels,
                      loc = 'bottom')

    table.auto_set_font_size(False)
    table.set_fontsize(10)
    table.scale(1.5, 1.0)

def plot(plt, filename, startDate = 0, endDate = 0, pdfDocument = None, annualPlots = False, noNewFig = False):
    file = open(filename, "r")
    lines = file.readlines()
    
    pnls = []
    dates = []
    cumPnl = 0.0
    addInfo = []
    addInfoIndex = -1
    numLines = len(lines)

    osStartDate = None
    osPnls = []
    osDates = []

    lcumPnl = 0.0

    annualPnls = []

    for i in range(1, numLines):
        line = lines[i].strip()
        if not line or line.startswith("#"):
            addInfoIndex = i
            break

        parts = line.split(",")
        date = parts[0].strip()
        idate = int(date)

        if startDate != 0 and idate < startDate:
            continue

        if endDate != 0 and idate > endDate:
            continue

        if idate == 0:
            continue

        date = DT.datetime.strptime(str(date), "%Y%m%d")
        year = date.year
        pnl = float(parts[3].strip())

        lcumPnl += pnl
        dates.append(date)
        pnls.append(lcumPnl)

        if annualPlots:
            annualPnl = annualPnls[-1] if len(annualPnls) else None

            if annualPnl is None or annualPnl["year"] != year:
                annualPnl = {
                    "year": year,
                    "cumPnl": 0.0,
                    "pnls": [],
                    "dates": []
                }
                annualPnls.append(annualPnl)

            annualPnl["cumPnl"] += pnl
            annualPnl["dates"].append(date)
            annualPnl["pnls"].append(annualPnl["cumPnl"])

    if addInfoIndex >= 0:
        while addInfoIndex < numLines:
            line = lines[addInfoIndex].strip()

            # this is actual data here ...
            if line and line.startswith("# "):
                tag = line[2:]

                if tag.startswith("OutSample") and "-" in tag:
                    tag, osStartDate = tag.split("-")
                    tag = tag.strip()
                    if len(osStartDate) == 8:
                        osStartDate = DT.datetime.strptime(osStartDate.strip(), "%Y%m%d")

                addInfoIndex += 1
                line = lines[addInfoIndex].strip()
                parts = line.split(",")
                try:
                    ir = float(parts[5].strip())
                except:
                    ir = 0.0
                sharpe = ir * sharpeConst
                bookSize = float(parts[1].strip())
                fpnl = float(parts[4].strip())

                addInfo.append([
                    tag,                # Name
                    str.format("%0.2e" % (bookSize)),  # BookSize
                    parts[2].strip(),   # Long
                    parts[3].strip(),   # Short
                    str.format("%0.2e" % (fpnl)),   # PNL
                    str.format("%0.2f / %0.2f" % (ir, sharpe)),   # IR / Sharpe
                    parts[6].strip(),   # Returns
                    parts[7].strip(),   # MaxTVR
                    parts[8].strip(),   # MaxDD
                    parts[9].strip(),   # MaxDH
                    parts[10].strip(),  # HitRatio
                    parts[11].strip(),  # Margin BPS
                    parts[12].strip(),  # Margin CPS
                ])

            addInfoIndex += 1

    if osStartDate:
        for i in range(len(dates)):
            date = dates[i]
            if date >= osStartDate:
                osDates.append(date)
                osPnls.append(pnls[i])

    # plot
    fname = getStratName(filename)

    if pdfDocument:
        if noNewFig is False:
            plt.figure(figsize=(20, 12))

    line, = plt.plot(dates, pnls, label = fname)

    if len(osDates):
        line2, = plt.plot(osDates, osPnls, label = fname + "-OS")

    if annualPlots:
        for i in range(0, len(annualPnls)):
            annualPnl = annualPnls[i]
            aline, = plt.plot(annualPnl["dates"], annualPnl["pnls"], label = str(annualPnl["year"]))

    if pdfDocument:
        ax = plt.axes()
        ax.xaxis.set_ticks_position('top')
        plotTable(fname, addInfo)

        plt.title(fname, y = 1.08)
        pdfDocument.savefig()
        plt.close()
    else:
        plt.title(fname)

    # plt.tick_params(axis = 'x', which = 'both', labelbottom = 'off', labeltop = 'on')
    return line, fname, addInfo
    

def run():
    options, args = parseArguments()
    # filename = args[-1]
    # filenames = args
    annualPlots = bool(options.annual)
    filenames = []
    isMultiSplit = False

    for i in range(len(args)):
        arg = args[i]
        if '@' not in arg:
            afilenames = glob(arg)
            filenames += afilenames
        else:
            isMultiSplit = True
            filenames.append(arg)

    assert len(filenames), "Must specify at least one filename to plot!"

    filenames.sort()
    
    lines = []  
    names = []

    startDate = 0 if options.startDate is None else int(options.startDate)
    endDate = 0 if options.endDate is None else int(options.endDate)

    addInfos = []

    pdfDocument = None
    pdfDocName = "psim_plot.pdf"
    if options.filename:
        pdfDocName = options.filename
    elif len(filenames) == 1:
        getStratName(filenames[0]) 

    mainFigures = []
    mainTableFigure = None

    if options.pdf:
        pdfDocument = PdfPages(pdfDocName if pdfDocName.endswith('.pdf') else pdfDocName + '.pdf')

        srcFilenames = filenames
        filenames = []

        for filename in srcFilenames:
            if '@' not in filename:
                plot(plt, filename, startDate = startDate, endDate = endDate, pdfDocument = pdfDocument, annualPlots = annualPlots)
                filenames.append(filename)
            else:
                multiFilenames = filename.split('@')
                filenames.append(multiFilenames[0])

                plt.figure(figsize=(20, 12))
                lines = []
                names = []

                for multiFilename in multiFilenames:
                    line, name, addInfo = plot(plt, multiFilename, startDate = startDate, endDate = endDate, pdfDocument = None, annualPlots = annualPlots, noNewFig = True)
                    lines.append(line)
                    names.append(name)
                    # mainFigures.append(plt.figure())

                # plot(plt, multiFilenames[-1], startDate = startDate, endDate = endDate, pdfDocument = None, annualPlots = annualPlots, noNewFig = True)
                plt.legend(lines, names, loc = 2) # upper left 
                ax = plt.axes()
                ax.xaxis.set_ticks_position('top')
                # plotTable(fname, addInfo)

                plt.title(names[0], y = 1.08)
                pdfDocument.savefig()
                plt.close()

        mainFigures.append(plt.figure(figsize=(20, 12)))

    addInfos = []
    for filename in filenames:
        line, name, addInfo = plot(plt, filename, startDate = startDate, endDate = endDate, annualPlots = annualPlots)
        lines.append(line)
        names.append(name)
        addInfos.append(addInfo)
    
    # beautify the x-labels
    plt.gcf().autofmt_xdate()
    # plt.xlabel("Dates")
    plt.ylabel("PNL")
    
    plt.text(3, 8, 'boxed italics text in data coords', style='italic', bbox={'facecolor':'red', 'alpha':0.5, 'pad':10})

    ax = plt.axes()
    ax.xaxis.set_ticks_position('top')
    ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))
    
    plt.legend(lines, names, loc = 2) # upper left 
    mng = plt.get_current_fig_manager()

    if pdfDocument and len(filenames) > 1:
        mainTableFigure = plt.figure()

    # mng.window.showMaximized()
    rowLabels = []
    tableVals = []

    for i in range(0, len(addInfos)):
        addInfo     = addInfos[i]
        name        = names[i]
        print(name)

        for stats in addInfo:
            statsName = stats[0]

            if statsName.startswith("Y") or statsName.startswith("LT"):
                rowLabels.append(name + "-" + statsName)
                row = []
                for i in range(1, len(stats)):
                    row.append(stats[i])

                tableVals.append(row)

    if len(tableVals):
        loc = 'bottom'
        if pdfDocument:
            loc = 'center'
            ax = plt.gca()

            # Hide axes
            ax.xaxis.set_visible(False) 
            ax.yaxis.set_visible(False)

        table = plt.table(cellText = tableVals,
                          colWidths = colWidths,
                          rowLabels = rowLabels,
                          colLabels = colLabels,
                          loc = loc)

        table.auto_set_font_size(False)
        table.set_fontsize(10)
        table.scale(1, 0.5)

    # plt.text(12,3.4,'Stats', size = 20)

    if pdfDocument:
        if len(filenames) > 1 or isMultiSplit:
            # plt.savefig(pdfDocument)
            for mainFigure in mainFigures:
                pdfDocument.savefig(mainFigure)
            pdfDocument.savefig(mainTableFigure)

    if options.email is None:
        if pdfDocument is None:
            if options.filename is None:
                plt.show()
            else:
                defSize = plt.gcf().get_size_inches()
                plt.gcf().set_size_inches((defSize[0] * 2.5, defSize[1] * 2.5))
                plt.savefig(options.filename, format = 'pdf' if pdfDocument else 'png')
        else:
            pdfDocument.close()
    else:
        defSize = plt.gcf().get_size_inches()
        plt.gcf().set_size_inches((defSize[0] * 2.5, defSize[1] * 2.5))
        name = os.path.basename(filenames[0])

        if pdfDocument:
            d = pdfDocument.infodict()
            d['Title'] = 'PSIM Plot'
            d['Author'] = u'HKhan Team'
            d['Subject'] = pdfDocName
            d['CreationDate'] = DT.datetime.utcnow()
            d['ModDate'] = DT.datetime.utcnow()

            pngFilename = pdfDocName

            # plt.savefig(pngFilename, format = 'pdf')
            pdfDocument.close()
        else:
            pngFilename = name + ".png"
            plt.savefig(pngFilename)

        import smtplib
        from os.path import basename
        from email.mime.application import MIMEApplication
        from email.mime.multipart import MIMEMultipart
        from email.mime.text import MIMEText
        from email.utils import COMMASPACE, formatdate

        msg = MIMEMultipart()
        msg['From'] = "PSIM_PLOT@bamfunds.com"
        msg['To'] = options.email
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = options.subject if options.subject else pdfDocName

        msg.attach(MIMEText(pdfDocName))

        with open(pngFilename, "rb") as fil:
            part = MIMEApplication(
                fil.read(),
                Name=basename(pngFilename)
            )
            part['Content-Disposition'] = 'attachment; filename="%s"' % basename(pngFilename)
            msg.attach(part)

        smtp = smtplib.SMTP("bamsmtp")
        smtp.sendmail("psim_plot@bamfunds.com", options.email, msg.as_string())
        smtp.close()        

try:
    run()
except Exception as e:
    print(traceback.format_exc())
    sys.exit(1)
