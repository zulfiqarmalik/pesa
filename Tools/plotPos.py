### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 27 11:53:28 2017

@author: zfu
"""
import csv
from optparse import OptionParser
import numpy as np
import matplotlib.pyplot as plt

def parseArguments():
    parser = OptionParser()
    
    return parser.parse_args()


options, args = parseArguments()
filename = args[-1]

dollarAllocation = []
positiveAllocation = []
negativeAllocation = []

with open(filename, newline='') as f:
    reader = csv.reader(f, delimiter='|')
    line = 0
    for row in reader:
        # skip headers
        if line >= 11:
            dollarAllocation.append(float(row[3]))
        line += 1
      

for i in range(len(dollarAllocation)):
    if dollarAllocation[i] > 0:
        positiveAllocation.append(dollarAllocation[i])
    elif dollarAllocation[i] < 0:
        negativeAllocation.append(dollarAllocation[i])
sortedPosAllocation = sorted(positiveAllocation, reverse=True)
sortedNegAllocation = sorted(negativeAllocation, reverse=True)
sortedAllocation = np.absolute(np.asarray(sortedNegAllocation + sortedPosAllocation))

plt.plot(sortedAllocation)
plt.ylabel("Absolute value of dollar allocation")
plt.show()
