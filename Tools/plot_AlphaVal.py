### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys
import logging
from optparse import OptionParser
import datetime as DT

def parseArguments():
    parser = OptionParser()

    # parser.add_option("-f", "--file", dest = "filename", help = "write report to FILE", metavar = "FILE")
    # parser.add_option("-c", "--config", dest = "config", default = "./configs/c_default.py", help = "What config to use for extra settings", metavar = "FILE")

    # parser.add_option("-d", "--dbPath", dest = "dbPath", default = "mongodb://localhost:27017/", help = "The path to the database server")
    # parser.add_option("-D", "--dbName", dest = "dbName", default = "pesa", help = "The name of the database to connect to")

    # parser.add_option("-I", "--id", dest = "id", default = "MASTER", help = "What is the ID of the application. This is helpful in logging")
    # parser.add_option("-L", "--logLevel", dest = "logLevel", default = "debug", help = "What log level to set. Choose from [debug, info, warn]")
    # parser.add_option("-M", "--master", action = "store_true", dest = "master", default = False, help = "Run the application in master mode")

    # parser.add_option("-n", "--numSlaves", dest = "numSlaves", default = 0, help = "How many slave processes to launch on this machine. 0 = number of cores on the machine")
    # parser.add_option("-N", "--nodb", action = "store_true", dest = "nodb", default = False, help = "Do not connect to the database")

    # parser.add_option("-p", "--port", dest = "port", default = 8000, help = "What port to run on")
    # # parser.add_option("-P", "--masterPort", dest = "masterPort", default = 8000, help = "What is the port of the master that we're going to connect to")

    # parser.add_option("-Q", "--quiet", action = "store_false", dest = "verbose", default = True, help = "Don't print status messages to stdout")
    # parser.add_option("-S", "--slave", action = "store_true", dest = "slave", default = False, help = "Run the application in slave mode")

    # parser.add_option("-X", "--noSlaves", action = "store_true", dest = "noSlaves", default = False, help = "Instructs the master server to NOT spawn any slave processes")
        
    return parser.parse_args()

DW = 0.04
colLabels = ["BookSize", "Long", "Short", "PNL",  "IR / Sharpe", "Returns %", "TVR %", "MaxDD %", "MaxDH", "HitRatio", "Mrgn BPS", "Mrgn CPS"]
colWidths = [DW * 1.5,   DW,     DW,      DW * 2, DW * 2,        DW,           DW,     DW,        DW,      DW,         DW,         DW]
import math
sharpeConst = math.sqrt(252)
    
def plot(plt, filename):
    file = open(filename, "r")
    lines = file.readlines()
    
    pnls = []
    alphavals = []
    dates = []
    cumPnl = 0.0
    addInfo = []
    addInfoIndex = -1
    numLines = len(lines)

    for i in range(1, numLines):
        line = lines[i].strip()
        if not line or line.startswith("#"):
            addInfoIndex = i
            break

        parts = line.split(",")
        date = parts[0].strip()
        date = DT.datetime.strptime(str(date), "%Y%m%d")
        pnl = float(parts[2].strip())
        #==============================
        alphaval = float(parts[9].strip())
        alphavals.append(alphaval)
        dates.append(date)
        pnls.append(pnl)

    if addInfoIndex >= 0:
        while addInfoIndex < numLines:
            line = lines[addInfoIndex].strip()

            # this is actual data here ...
            if line and line.startswith("# "):
                tag = line[2:]
                addInfoIndex += 1
                line = lines[addInfoIndex].strip()
                parts = line.split(",")
                try:
                    ir = float(parts[5].strip())
                except:
                    ir = 0.0
                sharpe = ir * sharpeConst
                bookSize = float(parts[1].strip())
                fpnl = float(parts[4].strip())

                addInfo.append([
                    tag,                # Name
                    str.format("%0.2e" % (bookSize)),  # BookSize
                    parts[2].strip(),   # Long
                    parts[3].strip(),   # Short
                    str.format("%0.2e" % (fpnl)),   # PNL
                    str.format("%0.2f / %0.2f" % (ir, sharpe)),   # IR / Sharpe
                    parts[6].strip(),   # Returns
                    parts[7].strip(),   # MaxTVR
                    parts[8].strip(),   # MaxDD
                    parts[9].strip(),   # MaxDH
                    parts[10].strip(),  # HitRatio
                    parts[11].strip(),  # Margin BPS
                    parts[12].strip(),  # Margin CPS
                ])

            addInfoIndex += 1

    import matplotlib.pyplot as plt
    
    # plot
    fname = os.path.basename(filename);
    #line, = plt.plot(dates, pnls, label = fname)
    line, = plt.plot(dates, alphavals, label = fname)
    # plt.tick_params(axis = 'x', which = 'both', labelbottom = 'off', labeltop = 'on')
    return line, fname, addInfo
    

def run():
    options, args = parseArguments()
    filename = args[-1]
    filenames = args

    assert len(filenames), "Must specify at least one filename to plot!"
    
    import matplotlib
    import matplotlib.pyplot as plt
    import matplotlib.ticker as mtick
    import numpy as np

    lines = []  
    names = []

    addInfos = []

    for filename in filenames:
        line, name, addInfo = plot(plt, filename)
        lines.append(line)
        names.append(name)
        addInfos.append(addInfo)
    
    # beautify the x-labels
    plt.gcf().autofmt_xdate()
    # plt.xlabel("Dates")
    plt.ylabel("PNL")
    
    plt.text(3, 8, 'boxed italics text in data coords', style='italic',
        bbox={'facecolor':'red', 'alpha':0.5, 'pad':10})

    ax = plt.axes()
    ax.xaxis.set_ticks_position('top')
    ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))
    
    plt.legend(lines, names, loc = 2) # upper left 
    mng = plt.get_current_fig_manager()

    # mng.window.showMaximized()
    rowLabels = []
    tableVals = []

    for i in range(0, len(addInfos)):
        addInfo     = addInfos[i]
        name        = names[i]
        print(name)

        for stats in addInfo:
            statsName = stats[0]

            if statsName.startswith("Y") or statsName.startswith("LT"):
                rowLabels.append(name + "-" + statsName)
                row = []
                for i in range(1, len(stats)):
                    row.append(stats[i])

                tableVals.append(row)

    if len(tableVals):
        table = plt.table(cellText = tableVals,
                          colWidths = colWidths,
                          rowLabels = rowLabels,
                          colLabels = colLabels,
                          loc = 'bottom')

        table.auto_set_font_size(False)
        table.set_fontsize(10)
        table.scale(1, 0.5)

    # plt.text(12,3.4,'Stats', size = 20)

    plt.show()

run()
