### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#!python

import os
import sys
import traceback
import time

import datetime as DT
import multiprocessing

import subprocess

from optparse import OptionParser
import datetime as DT

sys.path.append("../PyImpl/Scripts")
sys.path.append("../Python")
import PyUtil

def parseArguments():
    parser = OptionParser()

    parser.add_option("-A", "--psimArgs", dest = "psimArgs", help = "PSIM arguments")
    parser.add_option("-m", "--machineName", dest = "machineName", default = "NO_MACHINE", help = "The name of the machine")
    parser.add_option("-b", "--body", dest = "body", default = "", help = "Body string about the process")
    parser.add_option("-e", "--senderId", dest = "senderId", default = "PSIM_RUN", help = "The source email address to use. This is just a prefix")
    parser.add_option("-r", "--recipients", dest = "recipients", default = "zmalik@quanvolve.com;hkhan@quanvolve.com", help = "The destination email addresses")
    parser.add_option("-s", "--subject", dest = "subject", default = "", help = "The subject of the email")
    parser.add_option("-E", "--exePath", dest = "exePath", default = "psim", help = "The name of the executable to use")

    return parser.parse_args()

def run():
    opt, args = parseArguments()

    assert opt.psimArgs, "Invalid arguments for PSIM"
    psimArgs = opt.psimArgs.split(' ')
 
    if not opt.body:
        opt.body = opt.subject

    exePath = opt.exePath
    cmd = [exePath]
    for i in range(len(psimArgs)):
        cmd.append(psimArgs[i].strip())

    print(cmd)

    recipients = [x.strip() for x in opt.recipients.split(";")] #["zmalik@bamfunds.com", "hkhan@bamfunds.com", "jdemiranda@bamfunds.com"]

    PyUtil.sendHttpEmail(recipients, title = "STARTING: " + opt.subject + " ...", text = "", senderId = opt.senderId)

    devnull = open(os.devnull, 'w')
    process = subprocess.Popen(cmd, stdout = devnull, stderr = devnull, stdin = devnull)
    process.wait()
    returnCode = process.returncode

    # fromAddr = "PSIM_DATA_SUCCESS@bamfunds.com"
    subject = "SUCCESS: " + opt.subject
    body = opt.machineName + "<br/>" + opt.body + "<br/>"
    body += "Command: " + exePath + " " + opt.psimArgs

    if returnCode:
        fromAddr = opt.senderId
        subject = "FAILURE: " + opt.subject
        body = opt.machineName + "\nThe process failed with error code: " + str(returnCode)

    body = '<font face="Courier New, Courier, monospace">' + body + '</font>'

    PyUtil.sendHttpEmail(recipients, title = subject, text = body, senderId = opt.senderId)
    msg.attach(MIMEText(body, "html"))

    smtp = smtplib.SMTP("bamsmtp")
    smtp.sendmail(fromAddr, recipients, msg.as_string())

try:
    run()
except Exception as e:
    print(traceback.format_exc())
