### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#!python

import os
import sys
import traceback
import time

import datetime as DT
import multiprocessing

import subprocess

from optparse import OptionParser
import datetime as DT
import time

from glob import glob
import subprocess

def sendEmail(subject, body, recipients, sender = "psim_strat_run@bamfunds.com"):
    import smtplib
    from os.path import basename
    from email.mime.application import MIMEApplication
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    from email.utils import COMMASPACE, formatdate

    msg             = MIMEMultipart()
    msg['From']     = sender
    msg['To']       = ", ".join(recipients)
    msg['Date']     = formatdate(localtime = True)
    msg['Subject']  = "STRAT_RUN - " + subject

    body            = '<font face="Courier New, Courier, monospace">' + body + '</font>'
    msg.attach(MIMEText(body, "html"))

    smtp = smtplib.SMTP("bamsmtp")
    smtp.sendmail(sender, recipients, msg.as_string())

def dateToInt(date = None):
    if date is None:
        date = DT.datetime.utcnow()
    return int(str(date.year).zfill(4) + str(date.month).zfill(2) + str(date.day).zfill(2))

class StratRunner:

    def parseArguments(self):
        parser = OptionParser()

        parser.add_option("-s", "--startDate", dest = "startDate", default = "20150101", help = "The start date argument for the psim")
        parser.add_option("-e", "--endDate", dest = "endDate", default = None, help = "The end date argument for the psim")
        parser.add_option("-E", "--email", dest = "email", default = "zmalik@bamfunds.com", help = "Email failed job information!")
        parser.add_option("-P", "--psimDir", dest = "psimDir", default = None, help = "The base psim directory")
        parser.add_option("-j", "--numProcesses", dest = "numProcesses", default = 16, help = "The number of threads to run the strategies in")

        return parser.parse_args()

    def __init__(self):
        opt, args       = self.parseArguments()

        self.opt        = opt
        self.args       = args

        if opt.endDate is None:
            opt.endDate = str(dateToInt())

        assert opt.psimDir, "Invalid psim directory"
        self.numProcesses = int(opt.numProcesses)
        if self.numProcesses <= 0:
            self.numProcesses  = 16

        self.processes = []
        for i in range(self.numProcesses):
            self.processes.append(None)

        # get all the configs under the subdirectory
        self.configs = []

        for arg in args:
            aconfigs = glob(arg)

            for aconfig in aconfigs:
                filename = os.path.abspath(aconfig)

                self.configs.append({
                    "path": aconfig
                })

        self.numConfigs = len(self.configs)
        print("Num configs: %d" % (self.numConfigs))
        self.configIter = 0
        self.numRunningProcesses = 0

    def runConfig(self, citer, index):
        config = self.configs[citer]

        exeName = "psim"
        if sys.platform == "win32" or sys.platform == "win64":
            exeName += ".exe"

        psimPath = os.path.join(self.opt.psimDir, exeName)
        configDir = os.path.dirname(config["path"])
        config["logFilename"] = os.path.join(configDir, "psrun_app.log")
        # psimLogFilename = os.path.join(self.settings.psimLogDir, psimLogFilename)

        cmd = [psimPath, "-c", os.path.basename(config["path"]), "-l", config["logFilename"], "-O", "-Q", "-j2", "-s", str(self.opt.startDate), "-e", str(self.opt.endDate)]
        
        print("[PROC-%d] - Running command: (%s) @ %s" % (citer, psimPath, str(cmd)))
        devnull = open(os.devnull, 'w')
        process = None
        
        try:
            process = subprocess.Popen(cmd, cwd = configDir, stdout = devnull, stderr = devnull, stdin = devnull)
        except Exception as e:
            print(traceback.format_exc())
            process = None
        
        if process is None:
            print("[PROC-%d] - Unable to start the process: %s" % (citer, cmd))
            return True
        
        self.processes[index] = { 
            "citer": citer,
            "process": process,
            "logFilename": config["logFilename"]
        }

        self.numRunningProcesses += 1

        return True

    def checkProcess(self, index):
        processInfo = self.processes[index]
        # If there is no process running then we just try to spawn a new process
        if processInfo is None:
            if self.configIter < self.numConfigs:
                if self.runConfig(self.configIter, index):
                    self.configIter += 1
            return None

        process = processInfo["process"]
        citer = processInfo["citer"]
        if process.poll() is None:
            return process

        self.numRunningProcesses -= 1
            
        returnCode = process.returncode
        print("[PROC-%d] - Return code: %d [config = %s]" % (index, returnCode, self.configs[citer]["path"]))

        logFilename = processInfo["logFilename"]
        logContents = ""

        # we only write the log file once we have a failure ...
        if process.returncode != 0:
            try:
                if os.path.exists(logFilename):
                    with open(logFilename, 'r') as fileHandle:
                        logContents = fileHandle.read()
            except Exception as e:
                print(e)
                logContents = "Could not read log file: " + logFilename + " - Process returned: " + str(process.returncode)

            if self.opt.email: 
                sendEmail(
                    recipients = self.opt.email, 
                    subject = os.path.basename(os.path.dirname(self.configs[citer]["path"])) + ": " + str(process.returncode), 
                    body = logContents)

    def run(self):
        while self.configIter < self.numConfigs:
            for i in range(self.numProcesses):
                self.checkProcess(i)

            time.sleep(5)

        # In the end we just want to make sure that all processes exit gracefully!
        while self.numRunningProcesses > 0:
            for i in range(self.numProcesses):
                self.checkProcess(i)

try:
    StratRunner().run()
except Exception as e:
    print(traceback.format_exc())
