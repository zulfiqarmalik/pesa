### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#!python

import os
import sys
import traceback
import time

import datetime as DT
import multiprocessing

import subprocess

from optparse import OptionParser
import datetime as DT

from pathlib import Path
import shutil

from os import listdir
from os.path import isfile, isdir, join

import traceback

reconDir = "recon"

def dateToStr(date = None, sep = ""):
    if date is None:
        date = DT.datetime.utcnow()
    return str(date.year).zfill(4) + sep + str(date.month).zfill(2) + sep + str(date.day).zfill(2)

def parseArguments():
    parser = OptionParser()

    parser.add_option("-p", "--path", dest = "path", default = "/home/zmalik/drv/devwork/prod/Strategies/US/", help = "The base directory on which to run the reconciliation")
    parser.add_option("-c", "--commit", dest = "commit", default = False, action = "store_true", help = "The base directory on which to run the reconciliation")

    return parser.parse_args()

def copyFile(srcFile, dstFile):
    print("Copying: %s => %s" % (srcFile, dstFile))
    try:
        shutil.copyfile(srcFile, dstFile)
        shutil.copystat(srcFile, dstFile)
    except:
        pass

def makeDirs(path):
    subDirs = ["pnl", "pos"]

    for subDir in subDirs:
        p = Path(os.path.join(path, reconDir, subDir))
        p.mkdir(parents = True, exist_ok = True)

def readPnlFile(pnlFilename):
    pnlData = {
        "lines": [],
        "dateIndex": {}, 
        "header": None,
        "lastDate": dateToStr()
    }

    try:
        if os.path.exists(pnlFilename) is False:
            return pnlData

        file = open(pnlFilename, "r")
        lines = file.readlines()

        if len(lines) == 0:
            return pnlData

        pnlData["header"] = lines[0]

        for i in range(1, len(lines)):
            line = lines[i]
            sline = line.strip()

            if not sline:
                continue

            if sline[0] == '#':
                break

            date = [x.strip() for x in line.split(",")][0]
            if date:
                pnlData["lastDate"] = date
                pnlData["lines"].append(line)
                pnlData["dateIndex"][date] = len(pnlData["lines"]) - 1

    except Exception as e:
        print(e)

    return pnlData

def writePnlData(filename, pnlData):
    assert pnlData["header"], "Invalid header!" 

    print("Writing pnl file: %s" % (filename))
    file = open(filename, "w") 

    file.write("%s\n" % (pnlData["header"]))

    for line in pnlData["lines"]:
        file.write("%s" % (line))

    file.close()

def runStrategy(name, path):
    print("Handling strategy: %s" % (name))
    makeDirs(path)
    
    reconPnlFilename = os.path.join(path, reconDir, "pnl", "recon.pnl")
    pnlData = readPnlFile(os.path.join(path, "pnl", "MyPort.pnl"))
    reconPnlData = readPnlFile(reconPnlFilename)
    lastDate = pnlData["lastDate"] #dateToStr()

    if lastDate not in pnlData["dateIndex"]:
        print("Unable to find date: %s [filename = %s]" % (lastDate, path))
        return

    copyFile(os.path.join(path, "pnl", "MyPort.pnl"), os.path.join(path, reconDir, "pnl", "Yesterday.pnl"))
    copyFile(os.path.join(path, "pnl", "MyPort.pnl"), os.path.join(path, reconDir, "pnl", "MyPort_" + lastDate + ".pnl"))
    copyFile(os.path.join(path, "pnl", "MyPort_raw.pnl"), os.path.join(path, reconDir, "pnl", "MyPort_raw" + lastDate + ".pnl"))
    copyFile(os.path.join(path, "pnl", "MyPort_pre_opt.pnl"), os.path.join(path, reconDir, "pnl", "MyPort_pre_opt" + lastDate + ".pnl"))
    copyFile(os.path.join(path, "pos", "MyPort.pos"), os.path.join(path, reconDir, "pos", "MyPort_" + lastDate + ".pos"))

    reconPnlData["header"] = pnlData["header"]
    srcIndex = pnlData["dateIndex"][lastDate]
    srcPoint = pnlData["lines"][srcIndex]
    dstIndex = -1

    if lastDate in reconPnlData["dateIndex"]:
        dstIndex = reconPnlData["dateIndex"][lastDate]

    if dstIndex < 0:
        reconPnlData["lines"].append(srcPoint)
        reconPnlData["dateIndex"][lastDate] = len(reconPnlData["lines"]) - 1
    else:
        reconPnlData["lines"][dstIndex] = srcPoint

    # now we write ...
    writePnlData(reconPnlFilename, reconPnlData)

def gitCommit(srcPath_):
    import subprocess

    srcPath = os.path.join(srcPath_, "..")
    print("gitCommit changing dir: %s" % (srcPath))
    
    process = subprocess.Popen(["git", "pull"], cwd = srcPath)
    process.wait()
    print("git pull returned: %d" % (process.returncode))

    process = subprocess.Popen(["git", "add", "US"], cwd = srcPath)
    process.wait()
    print("git add returned: %d" % (process.returncode))

    commitMsg = str.format("\"Reconciliation commit: %s\"" % (dateToStr()))
    process = subprocess.Popen(["git", "commit", "-am", commitMsg], cwd = srcPath)
    process.wait()
    print("git commit returned: %d" % (process.returncode))

    process = subprocess.Popen(["git", "push"], cwd = srcPath)
    process.wait()
    print("git push returned: %d" % (process.returncode))

    process = subprocess.Popen(["git", "tag", dateToStr()], cwd = srcPath)
    process.wait()
    print("git tag returned: %d" % (process.returncode))

    process = subprocess.Popen(["git", "push", "--tags"], cwd = srcPath)
    process.wait()
    print("git tag returned: %d" % (process.returncode))


def run():
    opt, args = parseArguments()
    srcPath = opt.path

    assert srcPath, "Invalid arguments for PSIM"

    childStrategies = [f for f in listdir(srcPath) if isdir(join(srcPath, f))]

    print("Strategy list: %s" % (str(childStrategies)))

    # Perform per-strategy operations first 

    for childStrategy in childStrategies:
        runStrategy(childStrategy, os.path.join(srcPath, childStrategy))

    if opt.commit is True:
        gitCommit(srcPath)
 
try:
    run()
except Exception as e:
    print(traceback.format_exc())
