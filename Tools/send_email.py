### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#!python

import os
import sys
import traceback
import time

import datetime as DT
import multiprocessing

import subprocess

from optparse import OptionParser
import datetime as DT
import glob

def parseArguments():
    parser = OptionParser()

    parser.add_option("-a", "--attachments", dest = "attachments", default = "", help = "Semi-colon separated list of attachments")
    parser.add_option("-B", "--body", dest = "body", default = "", help = "The body of the email")
    parser.add_option("-e", "--email", dest = "email", default = "khanteam@bamfunds.com", help = "List of people who will be emailed a log file in the event the Trade module goes wrong")
    parser.add_option("-s", "--sender", dest = "sender", default = "hkha-trade@bamfunds.com", help = "The sender")
    parser.add_option("-S", "--subject", dest = "subject", default = "", help = "The subject of the email")

    return parser.parse_args()

def sendEmail(subject, body, recipients, sender, attachments = None):
    import smtplib
    from os.path import basename
    from email.mime.application import MIMEApplication
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    from email.utils import COMMASPACE, formatdate

    recipients_    = recipients.split(";")

    msg             = MIMEMultipart()
    msg['From']     = sender
    msg['To']       = ", ".join(recipients_)
    msg['Date']     = formatdate(localtime = True)
    msg['Subject']  = "HKHA - " + subject

    msg.attach(MIMEText(body))

    if attachments:
        for attachment in attachments:
            for filename in glob.glob(attachment):
                print("Attaching: %s" % (filename))
                with open(filename, "r") as fil:
                    part = MIMEApplication(
                        fil.read(),
                        Name=basename(attachment)
                    )
                    part['Content-Disposition'] = 'attachment; filename="%s"' % basename(filename)
                    msg.attach(part)

    smtp = smtplib.SMTP("bamsmtp")
    smtp.sendmail(sender, recipients_, msg.as_string())

options, args = parseArguments()

attachments = options.attachments.split(";")
recipients = options.email.split(";")

sendEmail(options.subject, options.body, recipients = recipients, sender = options.sender, attachments = attachments)
