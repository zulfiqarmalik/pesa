### *** BEGIN LICENSE ***
### Copyright (C) QVTechnologies Inc. - All Rights Reserved
### Unauthorized copying, usage, dissemination of information or reproduction of this material, 
### via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
### Proprietary and confidential.
### *** END LICENSE ***

### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
### INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
### NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
### DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
### OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 26 19:11:45 2017

@author: zfu
"""
# Calculate long-term or short-term correlation matrix of daily pnls
# Example in linux command line
# python xcorr.py <directory> <output filename> (optional ndays)

import sys
import numpy as np
import datetime as DT
import os

def xcorr(directory, outfile, ndays = None):
    
    filelist = os.listdir(directory)
    newfilelist = []
     
    for i in range(0,len(filelist)):
        filename = filelist[i]
        if filename[-4:] == '.pnl':
            newfilelist.append(filelist[i])
    
    numfiles = len(newfilelist)
    print('Total number of pnls:', numfiles)
    print(newfilelist)
    corr_mat = np.zeros((numfiles, numfiles))
   

    for i in range(0, numfiles-1): # read pnls and dates from file1
        filename = newfilelist[i]
        file = open(directory + filename, "r")
        lines = file.readlines()
        dates1 = []
        pnls1 = []
        pnlCommonDates1 = []
        numLines = len(lines)
 
        print('i=',i)

        for k in range(1, numLines):
            line = lines[k].strip()
            if not line or line.startswith("#"):
                addInfoIndex = k
                break
    
            parts = line.split(",")
            
            date = parts[0].strip()
            date = DT.datetime.strptime(str(date), "%Y%m%d")
            dates1.append(date)
            
            pnl = float(parts[3].strip())
            pnls1.append(pnl)
            

            
        for j in range(i+1, numfiles): # read pnls and dates from file2
            filename = newfilelist[j]
            file = open(directory + filename, "r")
            lines = file.readlines()
            dates2 = []
            pnls2  = []
            pnlCommonDates1 = []
            pnlCommonDates2 = []
            numLines = len(lines)
            
            for k in range(1,numLines):
                line = lines[k].strip()
                if not line or line.startswith("#"):
                    addInfoIndex = k
                    break
                
                parts = line.split(",")
                date = parts[0].strip()
                date = DT.datetime.strptime(str(date), "%Y%m%d")
                dates2.append(date)
                
                pnl = float(parts[3].strip())
                pnls2.append(pnl)
                
            # Compare the two files and pick the common dates and the corresponding pnls
            for index, date in enumerate(dates1):
                if date in dates2:
                    index1 = index
                    index2 = dates2.index(date)
                    pnlCommonDates1.append(pnls1[index1])
                    pnlCommonDates2.append(pnls2[index2])
                    
            # Default: long-term cross-correlation    
                        
            if ndays != None: # short-term cross-correlation
                if len(pnlCommonDates1) >= ndays:
                    rij = np.corrcoef(pnlCommonDates1[-ndays:], pnlCommonDates2[-ndays:])
            
                else:
                    print('Time series not long enough!')
                    return
            else:
                rij = np.corrcoef(pnlCommonDates1, pnlCommonDates2)
              
            
            corr_mat[i,i] = rij[0,0]
            corr_mat[i,j] = rij[0,1]
            corr_mat[j,i] = rij[1,0]

    corr_mat[-1,-1] = 1.0



    if ndays != None:
        print(str(ndays) + '-day correlation matrix:')
    else:
        print('Long-Term correlation matrix:')
        
    print(corr_mat)
    
    # Write cross-correlation matrix
    f = open(outfile,'w')
    f.write(' ' + '\t')
    for index, filename in enumerate(newfilelist):
        f.write(filename + '\t')
    f.write('\n')
    for i in range(0, numfiles):
        f.write(newfilelist[i]+'\t')
        for j in range(0, numfiles):
            f.write(str(corr_mat[i,j])+'\t')
        f.write('\n')
    f.close()

    return 
    
if __name__ == "__main__":
    #pnls = []
    #if sys.argv[-1] == 'st' or sys.argv[-1] == 'ST':
    #    for arg in sys.argv[1:-1]:
    #        pnls.append(arg)
    #    xcorr(pnls,'st')
    #else:
    
    #    for arg in sys.argv[1:]:
    #        pnls.append(arg)
    #    xcorr(pnls)
#     pnls = ['gpachg3m_Top100_ind.pnl','gpachg3m_Top1000_ind_os.pnl','eps_Top500_os.pnl']
#     xcorr(pnls)
    
    if len(sys.argv) == 4:
        xcorr(sys.argv[1],sys.argv[2],int(sys.argv[3]))
    else:
        xcorr(sys.argv[1],sys.argv[2])
