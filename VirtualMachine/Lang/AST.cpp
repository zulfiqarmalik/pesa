/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Parser.cpp
///
/// Created on: 23 May 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "./AST.h"
#include "Core/Lib/Util.h"
#include "ICodeGenerator.h"

namespace pesa {
namespace ast {

	// void Expression::eval(ICodeGenerator* cg) {
	// 	cg->eval(static_cast<decltype(this)>(this));
	// }

	void Expression::eval(ICodeGenerator* cg) { cg->eval(this); }
	void Operator::eval(ICodeGenerator* cg) { cg->eval(this); }
	void UnaryOperator::eval(ICodeGenerator* cg) { cg->eval(this); }
	void BinaryOperator::eval(ICodeGenerator* cg) { cg->eval(this); }
	void Name::eval(ICodeGenerator* cg) { cg->eval(this); }
	void FloatConstant::eval(ICodeGenerator* cg) { cg->eval(this); }
	void StringConstant::eval(ICodeGenerator* cg) { cg->eval(this); }

	void ComplexExpression::eval(ICodeGenerator* cg) {
		for (ExpressionPtr expression : m_expressions)
			expression->eval(cg);
	}

	void MathExpression::eval(ICodeGenerator* cg) {
		cg->begin(this);
		ComplexExpression::eval(cg);
		cg->end(this);
	}

	void FunctionCall::eval(ICodeGenerator* cg) {
		ComplexExpression::eval(cg);
		cg->eval(this);
	}

	std::string Expression::toString() const {
		return "";
	}

	bool Expression::push(ExpressionPtr expr) {
		return false;
	}

	ExpressionPtr Expression::pop() {
		return nullptr;
	}

	std::string ComplexExpression::toString() const {
		return "";
	}

	std::string Name::toString() const {
		return Util::combine(m_expressions);
	}

	bool ComplexExpression::push(ExpressionPtr expr) {
		m_expressions.push_back(expr);
		return true;
	}

	ExpressionPtr ComplexExpression::pop() {
		if (!m_expressions.size())
			return nullptr;

		ExpressionPtr expr = m_expressions[m_expressions.size() - 1];
		m_expressions.pop_back();
		return expr;
	}

	ExpressionPtr ComplexExpression::lastExpr() {
		return m_expressions.size() ? m_expressions[m_expressions.size() - 1] : nullptr;
	}
}
} /// pesa namespace 
