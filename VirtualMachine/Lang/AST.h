/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// AST.h
///
/// Created on: 23 May 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#ifndef PESA_LANG_AST_H_
#define PESA_LANG_AST_H_

#include "VirtualMachine/VirtualMachineDefs.h"
#include <string>
#include <memory>
#include <vector>
#include "Core/Lib/Util.h"

namespace pesa {
	class ICodeGenerator;

namespace ast {
	class Expression;
	typedef std::shared_ptr<Expression> ExpressionPtr;

	enum Type {
		kNone,
		kGenreic,

		kOperator,
		kUnaryOperator,
		kBinaryOperator,

		kName,
		kFloatConstant,
		kStringConstant,
		kMathExpression,
		kFunctionCall,

		/// MUST BE THE LAST ONE!
		kTree = 1000
	};
	
	//////////////////////////////////////////////////////
	/// Expression: Base class for all AST nodes
	//////////////////////////////////////////////////////
	class VirtualMachine_API Expression {
	protected:
		Type 					m_type;

	public:
								Expression(Type type) : m_type(type) {}
		virtual 				~Expression() {}
		virtual std::string 	toString() const;
		virtual bool 			push(ExpressionPtr expr);
		virtual ExpressionPtr 	pop();

		virtual void 			eval(ICodeGenerator* cg);
		Type 					type() { return m_type; }
	};

	// typedef std::shared_ptr<Expression> ExpressionPtr;
	typedef std::vector<ExpressionPtr> 	ExpressionPtrVec;

	//////////////////////////////////////////////////////
	/// StringConstant
	//////////////////////////////////////////////////////
	class VirtualMachine_API Operator : public Expression {
	protected:
		std::string				m_op;
		int 					m_opType;

	public:
								Operator(Type type, const std::string& op, int opType) : Expression(type), m_op(op), m_opType(opType) {}
		virtual std::string 	toString() const { return m_op; }
		const std::string& 		op() const { return m_op; }
		int						opType() { return m_opType; }
		virtual void 			eval(ICodeGenerator* cg);
	};
	typedef std::shared_ptr<Operator> OperatorPtr;
	typedef std::vector<OperatorPtr> 	OperatorPtrVec;

	//////////////////////////////////////////////////////
	/// GenericOperator
	//////////////////////////////////////////////////////
	class VirtualMachine_API GenericOperator : public Operator {
	public:
								GenericOperator(const std::string& op, int opType) : Operator(kOperator, op, opType) {}
	};

	//////////////////////////////////////////////////////
	/// UnaryOperator
	//////////////////////////////////////////////////////
	class VirtualMachine_API UnaryOperator : public Operator {
	public:
								UnaryOperator(const std::string& op, int opType) : Operator(kUnaryOperator, op, opType) {}
		virtual void 			eval(ICodeGenerator* cg);
	};
	typedef std::shared_ptr<UnaryOperator> 	UnaryOperatorPtr;
	typedef std::vector<UnaryOperatorPtr> 	UnaryOperatorPtrVec;

	//////////////////////////////////////////////////////
	/// BinaryOperator
	//////////////////////////////////////////////////////
	class VirtualMachine_API BinaryOperator : public Operator {
	public:
								BinaryOperator(const std::string& op, int opType) : Operator(kBinaryOperator, op, opType) {}
		virtual void 			eval(ICodeGenerator* cg);
	};
	typedef std::shared_ptr<BinaryOperator> BinaryOperatorPtr;
	typedef std::vector<BinaryOperatorPtr> 	BinaryOperatorPtrVec;

	//////////////////////////////////////////////////////
	/// Named variable
	//////////////////////////////////////////////////////
	class VirtualMachine_API ComplexExpression : public Expression {
	protected:
		ExpressionPtrVec 		m_expressions;

	public:
								ComplexExpression(Type type) : Expression(type) {}
		ExpressionPtrVec&		expressions() { return m_expressions; }
		virtual std::string 	toString() const;
		virtual bool 			push(ExpressionPtr expr);
		virtual ExpressionPtr 	pop();
		virtual ExpressionPtr 	lastExpr();

		virtual void 			eval(ICodeGenerator* cg);
	};
	typedef std::shared_ptr<ComplexExpression> ComplexExpressionPtr;

	//////////////////////////////////////////////////////
	/// Named variable
	//////////////////////////////////////////////////////
	class VirtualMachine_API Name : public Expression {
	protected:
		StringVec 				m_expressions;

	public:
								Name(const StringVec& parts) : Expression(kName), m_expressions(parts) {}
		virtual std::string 	toString() const;
		virtual void 			eval(ICodeGenerator* cg);
		inline const StringVec& parts() const { return m_expressions; }
		inline StringVec&		parts() { return m_expressions; }
	};
	typedef std::shared_ptr<Name> NamePtr;

	//////////////////////////////////////////////////////
	/// FloatConstant
	//////////////////////////////////////////////////////
	class VirtualMachine_API FloatConstant : public Expression {
	protected:
		float 					m_value;
	public:
								FloatConstant(float value) : Expression(kFloatConstant), m_value(value) {}
		float 					value() { return m_value; }
		virtual void 			eval(ICodeGenerator* cg);
	};
	typedef std::shared_ptr<FloatConstant> FloatConstantPtr;

	//////////////////////////////////////////////////////
	/// StringConstant
	//////////////////////////////////////////////////////
	class VirtualMachine_API StringConstant : public Expression {
	protected:
		std::string				m_value;
	public:
								StringConstant(const std::string& value) : Expression(kStringConstant), m_value(value) {}
		std::string& 			value() { return m_value; }
		virtual void 			eval(ICodeGenerator* cg);
	};
	typedef std::shared_ptr<StringConstant> StringConstantPtr;

	//////////////////////////////////////////////////////
	/// Mathematical Expression
	//////////////////////////////////////////////////////
	class VirtualMachine_API MathExpression : public ComplexExpression {
		std::string				m_name;
	public:
								MathExpression() : ComplexExpression(kMathExpression) {}
		virtual void 			eval(ICodeGenerator* cg);
		inline std::string&		name() { return m_name; }
	};
	typedef std::shared_ptr<MathExpression> MathExpressionPtr;

	//////////////////////////////////////////////////////
	/// Function: Base class for all AST nodes
	//////////////////////////////////////////////////////
	class VirtualMachine_API FunctionCall : public ComplexExpression {
	protected:
		NamePtr 				m_name;

	public:
								FunctionCall(NamePtr name) : ComplexExpression(kFunctionCall), m_name(name) {}
		ExpressionPtrVec& 		arguments() { return m_expressions; }
		NamePtr 				name() { return m_name; }
		virtual std::string		toString() const { return m_name->toString(); }
		virtual void 			eval(ICodeGenerator* cg);
	};
	typedef std::shared_ptr<FunctionCall> FunctionCallPtr;

	//////////////////////////////////////////////////////
	/// Tree: The all encapsulating
	//////////////////////////////////////////////////////
	class Tree : public ComplexExpression {
	public:
								Tree() : ComplexExpression(kTree) {}
	};
	typedef std::shared_ptr<Tree> TreePtr;
} /// namespace ast
} /// namespace pesa

#endif /// PESA_LANG_AST_H_
