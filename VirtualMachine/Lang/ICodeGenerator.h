/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// AST.h
///
/// Created on: 23 May 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#ifndef PESA_LANG_ICODE_GENERATOR_H_
#define PESA_LANG_ICODE_GENERATOR_H_

#include "VirtualMachine/VirtualMachineDefs.h"
#include <string>
#include "AST.h"

namespace pesa {
	class ICodeGenerator {
	public:
		/// Implement the ones you need ...
		
		virtual void 			begin(ast::MathExpression* expr) 	{ eval((ast::Expression*)expr); }
		virtual void 			end(ast::MathExpression* expr) 		{ eval((ast::Expression*)expr); }

		virtual void 			eval(ast::Expression* expr) 		{}
		virtual void 			eval(ast::ComplexExpression* expr) 	{ eval((ast::Expression*)expr); }

		virtual void 			eval(ast::Operator* expr) 			{ eval((ast::Expression*)expr); }
		virtual void 			eval(ast::BinaryOperator* expr) 	{ eval((ast::Expression*)expr); }
		// virtual void 			eval(ast::UnaryOperator* expr) 		{ eval((ast::Expression*)expr); }

		virtual void 			eval(ast::FloatConstant* expr) 		{ eval((ast::Expression*)expr); }
		virtual void 			eval(ast::Name* expr) 				{ eval((ast::Expression*)expr); }
		virtual void 			eval(ast::FunctionCall* expr) 		{ eval((ast::Expression*)expr); }
	};
} /// namespace pesa

#endif /// PESA_LANG_ICODE_GENERATOR_H_
