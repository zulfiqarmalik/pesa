/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Lexer.cpp
///
/// Created on: 31 Mar 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////


#include "./Lexer.h"
#include "Core/Lib/Util.h"

#include "Poco/File.h"
#include "Poco/FileStream.h"

#include <cstring>

namespace pesa {

	#define PUNCTABLE

	//longer punctuations first
	Punctuation defPunc[] = {
		//binary operators
		{"<<=", P_LSHIFT_ASSIGN},
		{">>=", P_RSHIFT_ASSIGN},

		//
		{"...", P_PARMS},

		//define merge operator
		{"##", P_PRECOMPMERGE},				// pre-compiler

		//logic operators
		{"and", P_LOGIC_AND},				// pre-compiler
		{"&&", P_LOGIC_AND},				// pre-compiler
		{"or", P_LOGIC_OR},					// pre-compiler
		{"||", P_LOGIC_OR},					// pre-compiler
		{">=", P_LOGIC_GEQ},				// pre-compiler
		{"<=", P_LOGIC_LEQ},				// pre-compiler
		{"==", P_LOGIC_EQ},					// pre-compiler
		{"!=", P_LOGIC_UNEQ},				// pre-compiler

		//arithmatic operators
		{"*=", P_MUL_ASSIGN},
		{"/=", P_DIV_ASSIGN},
		{"%=", P_MOD_ASSIGN},
		{"+=", P_ADD_ASSIGN},
		{"-=", P_SUB_ASSIGN},
		{"++", P_INC},
		{"--", P_DEC},

		//binary operators
		{"&=", P_BIN_AND_ASSIGN},
		{"|=", P_BIN_OR_ASSIGN},
		{"^=", P_BIN_XOR_ASSIGN},
		{">>", P_RSHIFT},					// pre-compiler
		{"<<", P_LSHIFT},					// pre-compiler

		//reference operators
		{"->", P_POINTERREF},

		//C++
		{"::", P_CPP1},
		{".*", P_CPP2},

		//arithmatic operators
		{"*", P_MUL},						// pre-compiler
		{"/", P_DIV},						// pre-compiler
		{"%", P_MOD},						// pre-compiler
		{"+", P_ADD},						// pre-compiler
		{"-", P_SUB},						// pre-compiler
		{"=", P_ASSIGN},

		//binary operators
		{"&", P_BIN_AND},					// pre-compiler
		{"|", P_BIN_OR},					// pre-compiler
		{"^", P_BIN_XOR},					// pre-compiler
		{"~", P_BIN_NOT},					// pre-compiler

		//logic operators
		{"!", P_LOGIC_NOT},					// pre-compiler
		{">", P_LOGIC_GREATER},				// pre-compiler
		{"<", P_LOGIC_LESS},				// pre-compiler

		//reference operator
		{".", P_REF},

		//separators
		{",", P_COMMA},						// pre-compiler
		{";", P_SEMICOLON},

		//label indication
		{":", P_COLON},						// pre-compiler

		//if statement
		{"?", P_QUESTIONMARK},				// pre-compiler

		//embracements
		{"(", P_PARENTHESESOPEN},			// pre-compiler
		{")", P_PARENTHESESCLOSE},			// pre-compiler
		{"{", P_BRACEOPEN},					// pre-compiler
		{"}", P_BRACECLOSE},				// pre-compiler
		{"[", P_SQBRACKETOPEN},
		{"]", P_SQBRACKETCLOSE},

		//
		{"\\", P_BACKSLASH},

		//precompiler operator
		{"#", P_PRECOMP},					// pre-compiler
		{"$", P_DOLLAR},

		{NULL, P_NONE}
	};

	int g_defPuncTable[Lexer::s_maxPuncTable];
	int g_defNextPuncTable[sizeof(defPunc) / sizeof(Punctuation)];
	int g_defaultSetup;

	char Lexer::s_baseDir[256];

	const int Lexer::getFileOffset() {
		return (int)(m_script - m_buffer);
	}

	/*
	================
	Lexer::CreatePunctuationTable
	================
	*/
	void Lexer::createPunctuationTable(const Punctuation* punctuations) {
		int i, n, lastp;
		const Punctuation* p;
		const Punctuation* newp;

		//get memory for the table
		if (punctuations == defPunc) {
			m_punctuationTable = g_defPuncTable;
			m_nextPunctuation = g_defNextPuncTable;
			if (g_defaultSetup) {
				return;
			}
			g_defaultSetup = true;
			i = sizeof(defPunc) / sizeof(Punctuation);
		}
		else {
			if (!m_punctuationTable || m_punctuationTable == g_defPuncTable) {
				m_punctuationTable = new int [s_maxPuncTable];
			}
			if (m_nextPunctuation && m_nextPunctuation != g_defNextPuncTable) {
				delete[] m_nextPunctuation;
			}
			for (i = 0; punctuations[i].p; i++) {
			}
			m_nextPunctuation = new int [i];
		}

		memset(m_punctuationTable, 0xFF, s_maxPuncTable * sizeof(int));
		memset(m_nextPunctuation, 0xFF, i * sizeof(int));

		//add the punctuations in the list to the punctuation table
		for (i = 0; punctuations[i].p; i++) {
			newp = &punctuations[i];
			lastp = -1;
			//sort the punctuations in this table entry on length (longer punctuations first)
			for (n = m_punctuationTable[(unsigned int) newp->p[0]]; n >= 0; n = m_nextPunctuation[n]) {
				p = &punctuations[n];
				if (strlen(p->p) < strlen(newp->p)) {
					m_nextPunctuation[i] = n;
					if (lastp >= 0) {
						m_nextPunctuation[lastp] = i;
					}
					else {
						m_punctuationTable[(unsigned int) newp->p[0]] = i;
					}
					break;
				}
				lastp = n;
			}
			if (n < 0) {
				m_nextPunctuation[i] = -1;
				if (lastp >= 0) {
					m_nextPunctuation[lastp] = i;
				}
				else {
					m_punctuationTable[(unsigned int) newp->p[0]] = i;
				}
			}
		}
	}

	const char* Lexer::getPunctuationFromId(int id) {
		int i;

		for (i = 0; m_punctuations[i].p; i++) {
			if (m_punctuations[i].n == id) {
				return m_punctuations[i].p;
			}
		}
		return "unkown punctuation";
	}

	int Lexer::getPunctuationId(const char* p) {
		int i;

		for (i = 0; m_punctuations[i].p; i++) {
			if (!strcmp(m_punctuations[i].p, p)) {
				return m_punctuations[i].n;
			}
		}
		return 0;
	}

	void Lexer::setPunctuations(const Punctuation* p) {
	#ifdef PUNCTABLE
		if (p) {
			createPunctuationTable(p);
		}
		else {
			createPunctuationTable(defPunc);
		}
	#endif //PUNCTABLE
		if (p) {
			m_punctuations = p;
		}
		else {
			m_punctuations = defPunc;
		}
	}

	/*
	================
	Lexer::readWhiteSpace

	Reads spaces, tabs, C-like comments etc.
	When a newline character is found the scripts line counter is increased.
	================
	*/
	int Lexer::readWhiteSpace() {
		while (true) {
			// skip white space
			while (*m_script <= ' ') {
				if (!*m_script) {
					return 0;
				}
				if (*m_script == '\n') {
					m_line++;
				}
				m_script++;
			}
			// skip comments
			if (*m_script == '/') {
				// comments //
				if (*(m_script+1) == '/') {
					m_script++;
					do {
						m_script++;
						if (!*m_script) {
							return 0;
						}
					}
					while (*m_script != '\n');
					m_line++;
					m_script++;
					if (!*m_script) {
						return 0;
					}
					continue;
				}
				// comments /** /
				else if (*(m_script+1) == '*') {
					m_script++;
					while (1) {
						m_script++;
						if (!*m_script) {
							return 0;
						}
						if (*m_script == '\n') {
							m_line++;
						}
						else if (*m_script == '/') {
							if (*(m_script-1) == '*') {
								break;
							}
							if (*(m_script+1) == '*') {
								Warning("LEX", "Nested comment");
							}
						}
					}
					m_script++;
					if (!*m_script) {
						return 0;
					}
					m_script++;
					if (!*m_script) {
						return 0;
					}
					continue;
				}
			}
			break;
		}
		return 1;
	}

	/*
	========================
	Lexer::skipWhiteSpace

	Reads spaces, tabs, C-like comments etc. When a newline character is found, the scripts line 
	counter is increased. Returns false if there is no token left to be read.
	========================
	*/
	bool Lexer::skipWhiteSpace(bool currentLine) {
		while (true) {
			ASSERT(m_script <= m_end, "EOF reached while skipping whitespace!");

			if (m_script == m_end) {
				return false;
			}
			// skip white space
			while (*m_script <= ' ') {
				if (m_script == m_end) {
					return false;
				}
				if (!*m_script) {
					return false;
				}
				if (*m_script == '\n') {
					m_line++;
					if (currentLine) {
						m_script++;
						return true;
					}
				}
				m_script++;
			}
			// skip comments
			if (*m_script == '/') {
				// comments //
				if (*(m_script+1) == '/') {
					m_script++;
					do {
						m_script++;
						if (!*m_script) {
							return false;
						}
					}
					while (*m_script != '\n');
					m_line++;
					m_script++;
					if (currentLine) {
						return true;
					}
					if (!*m_script) {
						return false;
					}
					continue;
				}
				// comments /** /
				else if (*(m_script+1) == '*') {
					m_script++;
					while (1) {
						m_script++;
						if (!*m_script) {
							return false;
						}
						if (*m_script == '\n') {
							m_line++;
						}
						else if (*m_script == '/') {
							if (*(m_script-1) == '*') {
								break;
							}
							if (*(m_script+1) == '*') {
								Warning("LEX", "Nested comment");
							}
						}
					}
					m_script++;
					if (!*m_script) {
						return false;
					}
					continue;
				}
			}
			break;
		}
		return true;
	}

	int Lexer::readEscapeCharacter(char* ch) {
		int c, val, i;

		// step over the leading '\\'
		m_script++;
		// determine the escape character
		switch(*m_script) {
			case '\\': c = '\\'; break;
			case 'n': c = '\n'; break;
			case 'r': c = '\r'; break;
			case 't': c = '\t'; break;
			case 'v': c = '\v'; break;
			case 'b': c = '\b'; break;
			case 'f': c = '\f'; break;
			case 'a': c = '\a'; break;
			case '\'': c = '\''; break;
			case '\"': c = '\"'; break;
			case '\?': c = '\?'; break;
			case 'x':
			{
				m_script++;
				for (i = 0, val = 0; ; i++, m_script++) {
					c =* m_script;
					if (c >= '0' && c <= '9')
						c = c - '0';
					else if (c >= 'A' && c <= 'Z')
						c = c - 'A' + 10;
					else if (c >= 'a' && c <= 'z')
						c = c - 'a' + 10;
					else
						break;
					val = (val << 4) + c;
				}
				m_script--;
				if (val > 0xFF) {
					Warning("LEX", "Too large value in escape character");
					val = 0xFF;
				}
				c = val;
				break;
			}
			default: //NOTE: decimal ASCII code, NOT octal
			{
				if (*m_script < '0' ||* m_script > '9') {
					Error("LEX", "unknown escape char");
				}
				for (i = 0, val = 0; ; i++, m_script++) {
					c =* m_script;
					if (c >= '0' && c <= '9')
						c = c - '0';
					else
						break;
					val = val*  10 + c;
				}
				m_script--;
				if (val > 0xFF) {
					Warning("LEX", "too large value in escape character");
					val = 0xFF;
				}
				c = val;
				break;
			}
		}
		// step over the escape character or the last digit of the number
		m_script++;
		// store the escape character
		*ch = c;
		// succesfully read escape character
		return 1;
	}

	/*
	================
	Lexer::readString

	Escape characters are interpretted.
	Reads two strings with only a white space between them as one string.
	================
	*/
	int Lexer::readString(Token* token, int quote) {
		int tmpline;
		const char* tmpScript;
		char ch;

		if (quote == '\"') {
			token->type() = TT_STRING;
		} else {
			token->type() = TT_LITERAL;
		}

		// leading quote
		m_script++;

		while (1) {
			// if there is an escape character and escape characters are allowed
			if (*m_script == '\\' && !(m_flags & LEXFL_NOSTRINGESCAPECHARS)) {
				if (!readEscapeCharacter(&ch)) {
					return 0;
				}
				token->appendDirty(ch);
			}
			// if a trailing quote
			else if (*m_script == quote) {
				// step over the quote
				m_script++;
				// if consecutive strings should not be concatenated
				if ((m_flags & LEXFL_NOSTRINGCONCAT) &&
						(!(m_flags & LEXFL_ALLOWBACKSLASHSTRINGCONCAT) || (quote != '\"'))) {
					break;
				}

				tmpScript = m_script;
				tmpline = m_line;
				// read white space between possible two consecutive strings
				if (!readWhiteSpace()) {
					m_script = tmpScript;
					m_line = tmpline;
					break;
				}

				if (m_flags & LEXFL_NOSTRINGCONCAT) {
					if (*m_script != '\\') {
						m_script = tmpScript;
						m_line = tmpline;
						break;
					}
					// step over the '\\'
					m_script++;
					if (!readWhiteSpace() || (*m_script != quote)) {
						Error("LEX", "Expecting string after '\' terminated line");
						return 0;
					}
				}

				// if there's no leading qoute
				if (*m_script != quote) {
					m_script = tmpScript;
					m_line = tmpline;
					break;
				}
				// step over the new leading quote
				m_script++;
			}
			else {
				if (*m_script == '\0') {
					Error("LEX", "Missing trailing quote");
					return 0;
				}
				if (*m_script == '\n') {
					Error("LEX", "Newline inside string");
					return 0;
				}
				token->appendDirty(*m_script++);
			}
		}
		// token->data[token->len] = '\0';

		if (token->type() == TT_LITERAL) {
			if (!(m_flags & LEXFL_ALLOWMULTICHARLITERALS)) {
				if (token->length() != 1) {
					Warning("LEX", "Literal is not one character long");
				}
			}
			token->subType() = (*token)[0];
		}
		else {
			// the sub type is the length of the string
			token->subType() = (int)token->length();
		}
		return 1;
	}

	int Lexer::readName(Token* token) {
		char c;

		token->type() = TT_NAME;
		do {
			token->appendDirty(*m_script++);
			c =* m_script;
		} while ((c >= 'a' && c <= 'z') ||
				 (c >= 'A' && c <= 'Z') ||
				 (c >= '0' && c <= '9') ||
				 c == '_' ||
				 // if treating all tokens as strings, don't parse '-' as a seperate token
				 ((m_flags & LEXFL_ONLYSTRINGS) && (c == '-')) ||
				 // if special path name characters are allowed
				 ((m_flags & LEXFL_ALLOWPATHNAMES) && (c == '/' || c == '\\' || c == ':' || c == '.')));

		// token->data[token->len] = '\0';
		//the sub type is the length of the name

		token->subType() = (int)token->length();

		std::string tkStr = token->toString();
		if (tkStr == "true") {
			token->intValue() = 1;
			token->type() = TT_NUMBER;
			token->subType() = TT_BOOLEAN | TT_INTEGER | TT_VALUESVALID;
		}
		else if (tkStr == "false") {
			token->intValue() = 0;
			token->type() = TT_NUMBER;
			token->subType() = TT_BOOLEAN | TT_INTEGER | TT_VALUESVALID;
		}

		return 1;
	}

	int Lexer::checkString(const char* str) const {
		int i;

		for (i = 0; str[i]; i++) {
			if (m_script[i] != str[i]) {
				return false;
			}
		}
		return true;
	}

	int Lexer::readNumber(Token* token) {
		int i;
		int dot;
		char c, c2;

		token->type() = TT_NUMBER;
		token->subType() = 0;
		token->intValue() = 0;
		token->floatValue() = 0;

		c =* m_script;
		c2 =* (m_script + 1);

		if (c == '0' && c2 != '.') {
			// check for a hexadecimal number
			if (c2 == 'x' || c2 == 'X') {
				token->appendDirty(*m_script++);
				token->appendDirty(*m_script++);
				c =* m_script;
				while ((c >= '0' && c <= '9') ||
							(c >= 'a' && c <= 'f') ||
							(c >= 'A' && c <= 'F')) {
					token->appendDirty(c);
					c =* (++m_script);
				}
				token->subType() = TT_HEX | TT_INTEGER;
			}
			// check for a binary number
			else if (c2 == 'b' || c2 == 'B') {
				token->appendDirty(*m_script++);
				token->appendDirty(*m_script++);
				c =* m_script;
				while (c == '0' || c == '1') {
					token->appendDirty(c);
					c =* (++m_script);
				}
				token->subType() = TT_BINARY | TT_INTEGER;
			}
			// its an octal number
			else {
				token->appendDirty(*m_script++);
				c =* m_script;
				while (c >= '0' && c <= '7') {
					token->appendDirty(c);
					c =* (++m_script);
				}
				token->subType() = TT_OCTAL | TT_INTEGER;
			}
		}
		else {
			// decimal integer or floating point number or ip address
			dot = 0;
			while (1) {
				if (c >= '0' && c <= '9') {
				}
				else if (c == '.') {
					dot++;
				}
				else {
					break;
				}
				token->appendDirty(c);
				c =* (++m_script);
			}
			if (c == 'e' && dot == 0) {
				//We have scientific notation without a decimal point
				dot++;
			}
			// if a floating point number
			if (dot == 1) {
				token->subType() = TT_DECIMAL | TT_FLOAT;
				// check for floating point exponent
				if (c == 'e') {
					// Append the e so that getFloatValue code works
					token->appendDirty(c);

					c =* (++m_script);
					if (c == '-') {
						token->appendDirty(c);
						c =* (++m_script);
					}
					else if (c == '+') {
						token->appendDirty(c);
						c =* (++m_script);
					}
					while (c >= '0' && c <= '9') {
						token->appendDirty(c);
						c =* (++m_script);
					}
				}
				// check for floating point exception infinite 1.#INF or indefinite 1.#IND or NaN
				else if (c == '#') {
					c2 = 4;
					if (checkString("INF")) {
						token->subType() |= TT_INFINITE;
					}
					else if (checkString("IND")) {
						token->subType() |= TT_INDEFINITE;
					}
					else if (checkString("NAN")) {
						token->subType() |= TT_NAN;
					}
					else if (checkString("QNAN")) {
						token->subType() |= TT_NAN;
						c2++;
					}
					else if (checkString("SNAN")) {
						token->subType() |= TT_NAN;
						c2++;
					}
					for (i = 0; i < c2; i++) {
						token->appendDirty(c);
						c =* (++m_script);
					}
					while (c >= '0' && c <= '9') {
						token->appendDirty(c);
						c =* (++m_script);
					}
					if (!(m_flags & LEXFL_ALLOWFLOATEXCEPTIONS)) {
						token->appendDirty(0);	// zero terminate for c_str
						Error("LEX", "Parsed %s", token->c_str());
					}
				}
			}
			else if (dot > 1) {
				if (!(m_flags & LEXFL_ALLOWIPADDRESSES)) {
					Error("LEX", "More than one dot in number");
					return 0;
				}
				if (dot != 3) {
					Error("LEX", "IP address should have three dots");
					return 0;
				}
				token->subType() = TT_IPADDRESS;
			}
			else {
				token->subType() = TT_DECIMAL | TT_INTEGER;
			}
		}

		if (token->subType() & TT_FLOAT) {
			if (c > ' ') {
				// single-precision: float
				if (c == 'f' || c == 'F') {
					token->subType() |= TT_SINGLE_PRECISION;
					m_script++;
				}
				// extended-precision: long double
				else if (c == 'l' || c == 'L') {
					token->subType() |= TT_EXTENDED_PRECISION;
					m_script++;
				}
				// default is double-precision: double
				else {
					token->subType() |= TT_DOUBLE_PRECISION;
				}
			}
			else {
				token->subType() |= TT_DOUBLE_PRECISION;
			}
		}
		else if (token->subType() & TT_INTEGER) {
			if (c > ' ') {
				// default: signed long
				for (i = 0; i < 2; i++) {
					// long integer
					if (c == 'l' || c == 'L') {
						token->subType() |= TT_LONG;
					}
					// unsigned integer
					else if (c == 'u' || c == 'U') {
						token->subType() |= TT_UNSIGNED;
					}
					else {
						break;
					}
					c =* (++m_script);
				}
			}
		}
		else if (token->subType() & TT_IPADDRESS) {
			if (c == ':') {
				token->appendDirty(c);
				c =* (++m_script);
				while (c >= '0' && c <= '9') {
					token->appendDirty(c);
					c =* (++m_script);
				}
				token->subType() |= TT_IPPORT;
			}
		}

		// token->data[token->len] = '\0';
		return 1;
	}

	int Lexer::readPunctuation(Token* token) {
		int l, n, i;
		char const* p;
		const Punctuation* punc;

	#ifdef PUNCTABLE
		for (n = m_punctuationTable[(unsigned int)*(m_script)]; n >= 0; n = m_nextPunctuation[n])
		{
			punc = &(m_punctuations[n]);
	#else
		int i;

		for (i = 0; m_punctuations[i].p; i++) {
			punc = &m_punctuations[i];
	#endif
			p = punc->p;
			// check for this punctuation in the script
			for (l = 0; p[l] && m_script[l]; l++) {
				if (m_script[l] != p[l]) {
					break;
				}
			}
			if (!p[l]) {
				//
				token->ensureAllocated(l);

				for (i = 0; i < l; i++) {
					(*token)[i] = p[i];
				}

				/// set the final size ...
				token->resize(l);
				//
				m_script += l;
				token->type() = TT_PUNCTUATION;
				// sub type is the punctuation id
				token->subType() = punc->n;
				return 1;
			}
		}
		return 0;
	}

	void Lexer::consumeToken() {
		m_tokenAvailable = 0;
	}

	int Lexer::peekToken(Token* token) {
		if (!readToken(token))
			return 0;
		m_tokenAvailable = 1;
		return 1;
	}

	int Lexer::readToken(Token* token) {
		int c;

		if (!m_loaded) {
			Error("LEX", "Lexer::readToken: no file loaded");
			return 0;
		}

		if (m_script == NULL) {
			return 0;
		}

		// if there is a token available (from unreadToken)
		if (m_tokenAvailable) {
			m_tokenAvailable = 0;
			*token = m_token;
			return 1;
		} 

		m_prevToken = m_token;

		// save script pointer
		m_lastScript = m_script;
		// save line counter
		m_lastline = m_line;
		// clear the token stuff
		token->clear();
		// start of the white space
		m_whiteSpaceStart = m_script;
		token->setWhiteSpaceStart(m_script);

		// read white space before token
		if (!readWhiteSpace()) {
			return 0;
		}

		// end of the white space
		m_whiteSpaceEnd = m_script;
		token->setWhiteSpaceEnd(m_script);
		// line the token is on
		token->line() = m_line;
		// number of lines crossed before token
		token->linesCrossed() = m_line - m_lastline;
		// clear token flags
		token->flags() = 0;

		c =* m_script;

		// if we're keeping everything as whitespace deliminated strings
		if (m_flags & LEXFL_ONLYSTRINGS) {
			// if there is a leading quote
			if (c == '\"' || c == '\'') {
				if (!readString(token, c)) {
					return 0;
				}
			}
			else if (!readName(token)) {
				return 0;
			}
		}
		// if there is a number
		else if ((c >= '0' && c <= '9' && m_prevToken.subType() != P_REF) ||
				(c == '.' && (*(m_script + 1) >= '0' &&* (m_script + 1) <= '9') && m_prevToken.type() != TT_NAME)) {
			if (!readNumber(token)) {
				return 0;
			}
			// if names are allowed to start with a number
			if (m_flags & LEXFL_ALLOWNUMBERNAMES) {
				c =* m_script;
				if ((c >= 'a' && c <= 'z') ||	(c >= 'A' && c <= 'Z') || c == '_') {
					if (!readName(token)) {
						return 0;
					}
				}
			}
		}
		// if there is a leading quote
		else if (c == '\"' || c == '\'') {
			if (!readString(token, c)) {
				return 0;
			}
		}
		// if there is a name (we allow names to start with a number)
		else if ((c >= 'a' && c <= 'z') ||	(c >= 'A' && c <= 'Z') || c == '_' || (c >= '0' && c <= '9')) {
			if (!readName(token)) {
				return 0;
			}
		}
		// names may also start with a slash when pathnames are allowed
		else if ((m_flags & LEXFL_ALLOWPATHNAMES) && ((c == '/' || c == '\\') || c == '.')) {
			if (!readName(token)) {
				return 0;
			}
		}
		// check for punctuations
		else if (!readPunctuation(token)) {
			Error("LEX", "Unknown punctuation %c", c);
			return 0;
		}

		m_token = *token;
		
		// succesfully read a token
		return 1;
	}

	int Lexer::expectTokenString(const char* string) {
		Token token;

		if (!readToken(&token)) {
			Error("LEX", "Couldn't find expected '%s'", string);
			return 0;
		}
		if (token != string) {
			Error("LEX", "Expected '%s' but found '%s'", string, token.c_str());
			return 0;
		}

		return 1;
	}

	int Lexer::expectTokenType(int type, int subType, Token* token) {
		std::string str;

		if (!readToken(token)) {
			Error("LEX", "Couldn't read expected token");
			return 0;
		}

		if (token->type() != type) {
			switch(type) {
				case TT_STRING: str = "string"; break;
				case TT_LITERAL: str = "literal"; break;
				case TT_NUMBER: str = "number"; break;
				case TT_NAME: str = "name"; break;
				case TT_PUNCTUATION: str = "punctuation"; break;
				default: str = "unknown type"; break;
			}
			Error("LEX", "Expected a %s but found '%s'", str.c_str(), token->c_str());
			return 0;
		}
		if (token->type() == TT_NUMBER) {
			if ((token->subType() & subType) != subType) {
				str.clear();

				if (subType & TT_DECIMAL) 
					str = "decimal ";
				if (subType & TT_HEX) 
					str = "hex ";
				if (subType & TT_OCTAL) 
					str = "octal ";
				if (subType & TT_BINARY) 
					str = "binary ";
				if (subType & TT_UNSIGNED) 
					str += "unsigned ";
				if (subType & TT_LONG) 
					str += "long ";
				if (subType & TT_FLOAT) 
					str += "float ";
				if (subType & TT_INTEGER) 
					str += "integer ";

				Util::rtrim(str);
				Error("LEX", "Expected %s but found '%s'", str.c_str(), token->c_str());

				return 0;
			}
		}
		else if (token->type() == TT_PUNCTUATION) {
			if (subType < 0) {
				Error("LEX", "BUG: wrong punctuation subType");
				return 0;
			}
			if (token->subType() != subType) {
				Error("LEX", "Expected '%s' but found '%s'", getPunctuationFromId(subType), token->c_str());
				return 0;
			}
		}
		return 1;
	}

	int Lexer::expectAnyToken(Token* token) {
		if (!readToken(token)) {
			Error("LEX", "Couldn't read expected token");
			return 0;
		}
		else {
			return 1;
		}
	}

	int Lexer::checkTokenString(const char* string) {
		Token tok;

		if (!readToken(&tok)) {
			return 0;
		}
		// if the given string is available
		if (tok == string) {
			return 1;
		}
		// unread token
		m_script = m_lastScript;
		m_line = m_lastline;
		return 0;
	}

	int Lexer::checkTokenType(int type, int subType, Token* token) {
		Token tok;

		if (!readToken(&tok)) {
			return 0;
		}
		// if the type matches
		if (tok.type() == type && (tok.subType() & subType) == subType) {
			*token = tok;
			return 1;
		}
		// unread token
		m_script = m_lastScript;
		m_line = m_lastline;
		return 0;
	}

	int Lexer::peekTokenString(const char* string) {
		Token tok;

		if (!readToken(&tok)) {
			return 0;
		}

		// unread token
		m_script = m_lastScript;
		m_line = m_lastline;

		// if the given string is available
		if (tok == string) {
			return 1;
		}
		return 0;
	}

	int Lexer::peekTokenType(int type, int subType, Token* token) {
		Token tok;

		if (!readToken(&tok)) {
			return 0;
		}

		// unread token
		m_script = m_lastScript;
		m_line = m_lastline;

		// if the type matches
		if (tok.type() == type && (tok.subType() & subType) == subType) {
			*token = tok;
			return 1;
		}
		return 0;
	}

	int Lexer::skipUntilString(const char* string) {
		Token token;

		while (readToken(&token)) {
			if (token == string) {
				return 1;
			}
		}
		return 0;
	}

	int Lexer::skipRestOfLine() {
		Token token;

		while (readToken(&token)) {
			if (token.linesCrossed()) {
				m_script = m_lastScript;
				m_line = m_lastline;
				return 1;
			}
		}
		return 0;
	}

	/*
	=================
	Lexer::skipBracedSection

	Skips until a matching close brace is found.
	Internal brace depths are properly skipped.
	=================
	*/
	int Lexer::skipBracedSection(bool parseFirstBrace) {
		Token token;
		int depth;

		depth = parseFirstBrace ? 0 : 1;
		do {
			if (!readToken(&token)) {
				return false;
			}
			if (token.type() == TT_PUNCTUATION) {
				if (token == "{") {
					depth++;
				} else if (token == "}") {
					depth--;
				}
			}
		} while (depth);

		return true;
	}

	void Lexer::unreadToken(const Token* token) {
		if (m_tokenAvailable) {
			// idLib::common->FatalError("LEX", "Lexer::unreadToken, unread token twice\n");
		}

		m_token = *token;
		m_tokenAvailable = 1;
	}

	int Lexer::readTokenOnLine(Token* token) {
		Token tok;

		if (!readToken(&tok)) {
			m_script = m_lastScript;
			m_line = m_lastline;
			return false;
		}

		// if no lines were crossed before this token
		if (!tok.linesCrossed()) {
			*token = tok;
			return true;
		}
		// restore our position
		m_script = m_lastScript;
		m_line = m_lastline;
		token->clear();

		return false;
	}

	std::string Lexer::readRestOfLine() {
		std::string out;

		while (1) {

			if (*m_script == '\n') {
				m_line++;
				break;
			}

			if (!*m_script) {
				break;
			}

			if (*m_script <= ' ') {
				out += " ";
			} 
			else {
				out +=* m_script;
			}

			m_script++;

		}

		Util::trim(out);

		return out;
	}

	int Lexer::parseInt() {
		Token token;

		if (!readToken(&token)) {
			Error("LEX", "Couldn't read expected integer");
			return 0;
		}
		if (token.type() == TT_PUNCTUATION && token == "-") {
			expectTokenType(TT_NUMBER, TT_INTEGER, &token);
			return -((signed int) token.getIntValue());
		}
		else if (token.type() != TT_NUMBER || token.subType() == TT_FLOAT) {
			Error("LEX", "Expected integer value, found '%s'", token.c_str());
		}

		return token.getIntValue();
	}

	bool Lexer::parseBool() {
		Token token;

		if (!expectTokenType(TT_NUMBER, 0, &token)) {
			Error("LEX", "Couldn't read expected boolean");
			return false;
		}
		return (token.getIntValue() != 0);
	}

	float Lexer::parseFloat(bool* errorFlag) {
		Token token;

		if (errorFlag) {
			*errorFlag = false;
		}

		if (!readToken(&token)) {
			if (errorFlag) {
				Warning("LEX", "Couldn't read expected floating point number");
				*errorFlag = true;
			} else {
				Error("LEX", "Couldn't read expected floating point number");
			}
			return 0;
		}
		if (token.type() == TT_PUNCTUATION && token == "-") {
			expectTokenType(TT_NUMBER, 0, &token);
			return -token.getFloatValue();
		}
		else if (token.type() != TT_NUMBER) {
			if (errorFlag) {
				Warning("LEX", "Expected float value, found '%s'", token.c_str());
				*errorFlag = true;
			} else {
				Error("LEX", "Expected float value, found '%s'", token.c_str());
			}
		}
		return token.getFloatValue();
	}

	int Lexer::parse1DMatrix(int x, float* m) {
		int i;

		if (!expectTokenString("(")) {
			return false;
		}

		for (i = 0; i < x; i++) {
			m[i] = parseFloat();
		}

		if (!expectTokenString(")")) {
			return false;
		}
		return true;
	}

	int Lexer::parse2DMatrix(int y, int x, float* m) {
		int i;

		if (!expectTokenString("(")) {
			return false;
		}

		for (i = 0; i < y; i++) {
			if (!parse1DMatrix(x, m + i*  x)) {
				return false;
			}
		}

		if (!expectTokenString(")")) {
			return false;
		}

		return true;
	}

	int Lexer::parse3DMatrix(int z, int y, int x, float* m) {
		int i;

		if (!expectTokenString("(")) {
			return false;
		}

		for (i = 0 ; i < z; i++) {
			if (!parse2DMatrix(y, x, m + i*  x*y)) {
				return false;
			}
		}

		if (!expectTokenString(")")) {
			return false;
		}
		return true;
	}

	/*
	=================
	idParser::ParseBracedSection

	The next token should be an open brace.
	Parses until a matching close brace is found.
	Maintains exact characters between braces.

	  FIXME: this should use ReadToken and replace the token white space with correct indents and newlines
	=================
	*/
	std::string Lexer::parseBracedSectionExact(int tabs) {
		int		depth;
		bool	doTabs;
		bool	skipWhite;
		std::string out;

		out.clear();

		if (!expectTokenString("{")) {
			return out;
		}

		out = "{";
		depth = 1;	
		skipWhite = false;
		doTabs = tabs >= 0;

		while (depth &&* m_script) {
			char c =* (m_script++);

			switch (c) {
				case '\t':
				case ' ': {
					if (skipWhite) {
						continue;
					}
					break;
				}
				case '\n': {
					if (doTabs) {
						skipWhite = true;
						out += c;
						continue;
					}
					break;
				}
				case '{': {
					depth++;
					tabs++;
					break;
				}
				case '}': {
					depth--;
					tabs--;
					break;				
				}
			}

			if (skipWhite) {
				int i = tabs;
				if (c == '{') {
					i--;
				}
				skipWhite = false;
				for (; i > 0; i--) {
					out += '\t';
				}
			}
			out += c;
		}

		return out;
	}

	/*
	=================
	Lexer::parseBracedSection

	The next token should be an open brace.
	Parses until a matching close brace is found.
	Internal brace depths are properly skipped.
	=================
	*/
	std::string Lexer::parseBracedSection() {
		Token token;
		int i, depth;
		std::string out;

		if (!expectTokenString("{")) {
			return out;
		}

		out = "{";
		depth = 1;

		do {
			if (!readToken(&token)) {
				Error("LEX", "Missing closing brace");
				return out;
			}

			// if the token is on a new line
			for (i = 0; i < token.linesCrossed(); i++) {
				out += "\r\n";
			}

			if (token.type() == TT_PUNCTUATION) {
				if (token[0] == '{') {
					depth++;
				}
				else if (token[0] == '}') {
					depth--;
				}
			}

			if (token.type() == TT_STRING) {
				out += "\"" + token.toString() + "\"";
			}
			else {
				out += token;
			}
			out += " ";
		} while (depth);

		return out;
	}

	std::string Lexer::parseRestOfLine() {
		std::string out;
		Token token;

		while (readToken(&token)) {
			if (token.linesCrossed()) {
				m_script = m_lastScript;
				m_line = m_lastline;
				break;
			}
			if (out.length()) {
				out += " ";
			}
			out += token;
		}

		return out;
	}

	/*
	========================
	Lexer::parseCompleteLine

	Returns a string up to the \n, but doesn't eat any whitespace at the beginning of the next line.
	========================
	*/
	std::string Lexer::parseCompleteLine() {
		Token token;
		std::string out;
		const char* start;

		start = m_script;

		while (1) {
			// end of buffer
			if (*m_script == 0) {
				break;
			}

			if (*m_script == '\n') {
				m_line++;
				m_script++;
				break;
			}
			m_script++;
		}

		out.clear();

		size_t length = (size_t)(m_script - start);
		for (size_t i = 0; i < length; i++)
			out += (start + i);

		return out;
	}

	int Lexer::getLastWhiteSpace(std::string& whiteSpace) const {
		whiteSpace.clear();

		for (const char* p = m_whiteSpaceStart; p < m_whiteSpaceEnd; p++) 
			whiteSpace += *p;

		return (int)whiteSpace.length();
	}

	int Lexer::getLastWhiteSpaceStart() const {
		return (int)(m_whiteSpaceStart - m_buffer);
	}

	int Lexer::getLastWhiteSpaceEnd() const {
		return (int)(m_whiteSpaceEnd - m_buffer);
	}

	void Lexer::reset() {
		// pointer in script buffer
		m_script = m_buffer;
		// pointer in script buffer before reading token
		m_lastScript = m_buffer;
		// begin of white space
		m_whiteSpaceStart = NULL;
		// end of white space
		m_whiteSpaceEnd = NULL;
		// set if there's a token available in m_token
		m_tokenAvailable = 0;

		m_line = 1;
		m_lastline = 1;
		// clear the saved token
		m_token = "";
	}

	bool Lexer::endOfFile() {
		return m_script >= m_end;
	}

	int Lexer::numLinesCrossed() {
		return m_line - m_lastline;
	}

	int Lexer::loadFile(const char* filename, bool OSPath) {
		 std::string pathname;
		 size_t  length;
		 char* buf;

		 if (m_loaded) {
		 	Error("LEX", "loadFile: another script already loaded");
		 	return false;
		 }
		
		 if (!OSPath && (s_baseDir[0] != '\0')) {
			 std::ostringstream ss;
			 ss << s_baseDir << "/" << filename;
			 pathname = ss.str();
		 }
		 else {
		 	pathname = filename;
		 }

		 Poco::FileInputStream fp(pathname);

		 length = Util::getStreamSize(fp);
		 buf = new char [length + 1];
		 buf[length] = '\0';
		 fp.read(buf, length);

//		 m_fileTime = fp->Timestamp();
//		 m_filename = fp->GetFullPath();
//		 idLib::fileSystem->CloseFile(fp);

		 m_buffer = buf;
		 m_length = (int)length;
		 // pointer in script buffer
		 m_script = m_buffer;
		 // pointer in script buffer before reading token
		 m_lastScript = m_buffer;
		 // pointer to end of script buffer
		 m_end = &(m_buffer[length]);

		 m_tokenAvailable = 0;
		 m_line = 1;
		 m_lastline = 1;
		 m_allocated = true;
		 m_loaded = true;

		return true;
	}

	int Lexer::loadMemory(const char* ptr, int length, const char* name, int startLine) {
		if (m_loaded) {
			Error("LEX", "Lexer::LoadMemory: another script already loaded");
			return false;
		}

		m_filename = name;
		m_buffer = ptr;
		m_fileTime = 0;
		m_length = length;
		// pointer in script buffer
		m_script = m_buffer;
		// pointer in script buffer before reading token
		m_lastScript = m_buffer;
		// pointer to end of script buffer
		m_end = &(m_buffer[length]);

		m_tokenAvailable = 0;
		m_line = startLine;
		m_lastline = startLine;
		m_allocated = false;
		m_loaded = true;

		return true;
	}

	void Lexer::freeSource() {
	#ifdef PUNCTABLE
		if (m_punctuationTable && m_punctuationTable != g_defPuncTable) {
			delete[] m_punctuationTable;
			m_punctuationTable = NULL;
		}
		if (m_nextPunctuation && m_nextPunctuation != g_defNextPuncTable) {
			delete[] m_nextPunctuation;
			m_nextPunctuation = NULL;
		}
	#endif //PUNCTABLE
		if (m_allocated) {
			delete[] m_buffer;
			m_buffer = NULL;
			m_allocated = false;
		}
		m_tokenAvailable = 0;
		m_token = "";
		m_loaded = false;
	}

	Lexer::Lexer() {
		m_loaded = false;
		m_filename = "";
		m_flags = 0;
		setPunctuations(NULL);
		m_allocated = false;
		m_fileTime = 0;
		m_length = 0;
		m_line = 0;
		m_lastline = 0;
		m_tokenAvailable = 0;
		m_token = "";
		m_next = NULL;
		m_hadError = false;
	}

	Lexer::Lexer(int flags) {
		m_loaded = false;
		m_filename = "";
		m_flags = flags;
		setPunctuations(NULL);
		m_allocated = false;
		m_fileTime = 0;
		m_length = 0;
		m_line = 0;
		m_lastline = 0;
		m_tokenAvailable = 0;
		m_token = "";
		m_next = NULL;
		m_hadError = false;
	}

	Lexer::Lexer(const char* filename, int flags, bool OSPath) {
		m_loaded = false;
		m_flags = flags;
		setPunctuations(NULL);
		m_allocated = false;
		m_token = "";
		m_next = NULL;
		m_hadError = false;
		loadFile(filename, OSPath);
	}

	Lexer::Lexer(const char* ptr, int length, const char* name, int flags) {
		m_loaded = false;
		m_flags = flags;
		setPunctuations(NULL);
		m_allocated = false;
		m_token = "";
		m_next = NULL;
		m_hadError = false;
		loadMemory(ptr, length, name);
	}

	Lexer::~Lexer() {
		freeSource();
	}

	/*
	================
	Lexer::SetBaseFolder
	================
	*/
	void Lexer::setBaseFolder(const char* path) {
		// std::string::Copynz(s_baseDir, path, sizeof(s_baseDir));
	}

	bool Lexer::hadError() const {
		return m_hadError;
	}

} /// pesa namespace 
