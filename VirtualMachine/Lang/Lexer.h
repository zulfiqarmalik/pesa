/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Lexer.h
///
/// Created on: 31 Mar 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#ifndef PESA_LEXER_H_
#define PESA_LEXER_H_

/*
===============================================================================

	Lexicographical parser

	Does not use memory allocation during parsing. The lexer uses no
	memory allocation if a source is loaded with LoadMemory().
	However, Token may still allocate memory for large strings.
	
	A number directly following the escape character '\' in a string is
	assumed to be in decimal format instead of octal. Binary numbers of
	the form 0b.. or 0B.. can also be used.

===============================================================================
*/

#include "VirtualMachine/VirtualMachineDefs.h"
#include "Core/Lib/Util.h"
#include "./Token.h"

namespace pesa {

	// lexer flags
	enum LexerFlags {
		LEXFL_NOERRORS						= BIT(0),	// don't print any errors
		LEXFL_NOWARNINGS					= BIT(1),	// don't print any warnings
		LEXFL_NOFATALERRORS					= BIT(2),	// errors aren't fatal
		LEXFL_NOSTRINGCONCAT				= BIT(3),	// multiple strings seperated by whitespaces are not concatenated
		LEXFL_NOSTRINGESCAPECHARS			= BIT(4),	// no escape characters inside strings
		LEXFL_NODOLLARPRECOMPILE			= BIT(5),	// don't use the $ sign for precompilation
		LEXFL_NOBASEINCLUDES				= BIT(6),	// don't include files embraced with < >
		LEXFL_ALLOWPATHNAMES				= BIT(7),	// allow path separators in names
		LEXFL_ALLOWNUMBERNAMES				= BIT(8),	// allow names to start with a number
		LEXFL_ALLOWIPADDRESSES				= BIT(9),	// allow ip addresses to be parsed as numbers
		LEXFL_ALLOWFLOATEXCEPTIONS			= BIT(10),	// allow float exceptions like 1.#INF or 1.#IND to be parsed
		LEXFL_ALLOWMULTICHARLITERALS		= BIT(11),	// allow multi character literals
		LEXFL_ALLOWBACKSLASHSTRINGCONCAT	= BIT(12),	// allow multiple strings seperated by '\' to be concatenated
		LEXFL_ONLYSTRINGS					= BIT(13)	// parse as whitespace deliminated strings (quoted strings keep quotes)
	};

	// punctuation ids
	enum PuncId {
		P_NONE 						= 0,

		P_RSHIFT_ASSIGN				= 1,
		P_LSHIFT_ASSIGN				= 2,
		P_PARMS						= 3,
		P_PRECOMPMERGE				= 4,

		P_LOGIC_AND					= 5,
		P_LOGIC_OR					= 6,
		P_LOGIC_GEQ					= 7,
		P_LOGIC_LEQ					= 8,
		P_LOGIC_EQ					= 9,
		P_LOGIC_UNEQ				= 10,

		P_MUL_ASSIGN				= 11,
		P_DIV_ASSIGN				= 12,
		P_MOD_ASSIGN				= 13,
		P_ADD_ASSIGN				= 14,
		P_SUB_ASSIGN				= 15,
		P_INC						= 16,
		P_DEC						= 17,

		P_BIN_AND_ASSIGN			= 18,
		P_BIN_OR_ASSIGN				= 19,
		P_BIN_XOR_ASSIGN			= 20,
		P_RSHIFT					= 21,
		P_LSHIFT					= 22,

		P_POINTERREF				= 23,
		P_CPP1						= 24,
		P_CPP2						= 25,
		P_MUL						= 26,
		P_DIV						= 27,
		P_MOD						= 28,
		P_ADD						= 29,
		P_SUB						= 30,
		P_ASSIGN					= 31,

		P_BIN_AND					= 32,
		P_BIN_OR					= 33,
		P_BIN_XOR					= 34,
		P_BIN_NOT					= 35,

		P_LOGIC_NOT					= 36,
		P_LOGIC_GREATER				= 37,
		P_LOGIC_LESS				= 38,

		P_REF						= 39,
		P_COMMA						= 40,
		P_SEMICOLON					= 41,
		P_COLON						= 42,
		P_QUESTIONMARK				= 43,

		P_PARENTHESESOPEN			= 44,
		P_PARENTHESESCLOSE			= 45,
		P_BRACEOPEN					= 46,
		P_BRACECLOSE				= 47,
		P_SQBRACKETOPEN				= 48,
		P_SQBRACKETCLOSE			= 49,
		P_BACKSLASH					= 50,

		P_PRECOMP					= 51,
		P_DOLLAR					= 52,
	};

	// punctuation
	struct VirtualMachine_API Punctuation {
		char const*	p;						// Punctuation character(s)
		PuncId 		n;						// Punctuation id
	};


	class VirtualMachine_API Lexer {
	public:
		static const int 	s_maxPuncTable = 256;	// Max size of the punctuation table

	private:
		int					m_loaded;				// Set when a script file is loaded from file or memory
		std::string			m_filename;				// File name of the script
		int					m_allocated;			// True if buffer memory was allocated
		const char*			m_buffer;				// Buffer containing the script
		const char*			m_script;				// Current pointer in the script
		const char*			m_end;					// Pointer to the end of the script
		const char*			m_lastScript;			// Script pointer before reading token
		const char*			m_whiteSpaceStart;		// Start of last white space
		const char*			m_whiteSpaceEnd;		// End of last white space
		long				m_fileTime;				// File time
		int					m_length;				// Length of the script in bytes
		int					m_line;					// Current line in script
		int					m_lastline;				// Line before reading token
		int					m_tokenAvailable;		// Set by unreadToken
		int					m_flags;				// Several script flags
		const Punctuation*	m_punctuations;			// The punctuations used in the script
		int* 				m_punctuationTable;		// ASCII table with punctuations
		int* 				m_nextPunctuation;		// Next punctuation in chain
		Token				m_token;				// Available token
		Token				m_prevToken;			// What is the previous token
		Lexer*				m_next;					// Next script in a chain
		bool				m_hadError;				// Set by idLexer::Error, even if the error is supressed

		static char			s_baseDir[256];			// Base folder to load files from

	private:
		void				createPunctuationTable(const Punctuation* punctuations);
		int					readWhiteSpace();
		int					readEscapeCharacter(char* ch);
		int					readString(Token* token, int quote);
		int					readName(Token* token);
		int					readNumber(Token* token);
		int					readPunctuation(Token* token);
		int					readPrimitive(Token* token);
		int					checkString(const char* str) const;
		int					numLinesCrossed();

	public:
							// constructor
							Lexer();
							Lexer(int flags);
							Lexer(const char* filename, int flags = 0, bool OSPath = false);
							Lexer(const char* ptr, int length, const char* name, int flags = 0);

							// destructor
							~Lexer();

							// load a script from the given file at the given offset with the given length
		int					loadFile(const char* filename, bool OSPath = false);
							// load a script from the given memory with the given length and a specified line offset,
							// so source strings extracted from a file can still refer to proper line numbers in the file
							// NOTE: the ptr is expected to point at a valid C string: ptr[length] == '\0'
		int 				peekToken(Token* token);
		void 				consumeToken();
		int					loadMemory(const char* ptr, int length, const char* name, int startLine = 1);
							// free the script
		void				freeSource();
							// returns true if a script is loaded
		int					isLoaded() { return m_loaded; };
							// read a token
		int					readToken(Token* token);
							// expect a certain token, reads the token when available
		int					expectTokenString(const char* string);
							// expect a certain token type
		int					expectTokenType(int type, int subtype, Token* token);
							// expect a token
		int					expectAnyToken(Token* token);
							// returns true when the token is available
		int					checkTokenString(const char* string);
							// returns true an reads the token when a token with the given type is available
		int					checkTokenType(int type, int subtype, Token* token);
							// returns true if the next token equals the given string but does not remove the token from the source
		int					peekTokenString(const char* string);
							// returns true if the next token equals the given type but does not remove the token from the source
		int					peekTokenType(int type, int subtype, Token* token);
							// skip tokens until the given token string is read
		int					skipUntilString(const char* string);
							// skip the rest of the current line
		int					skipRestOfLine();
							// skip the braced section
		int					skipBracedSection(bool parseFirstBrace = true);
							// skips spaces, tabs, C-like comments etc. Returns false if there is no token left to read.
		bool				skipWhiteSpace(bool currentLine);
							// unread the given token
		void				unreadToken(const Token* token);
							// read a token only if on the same line
		int					readTokenOnLine(Token* token);
			
							// Returns the rest of the current line
		std::string 		readRestOfLine();

							// read a signed integer
		int					parseInt();
							// read a boolean
		bool				parseBool();
							// read a floating point number.  If errorFlag is NULL, a non-numeric token will
							// issue an Error().  If it isn't NULL, it will issue a Warning() and set* errorFlag = true
		float				parseFloat(bool* errorFlag = NULL);

							// parse matrices with floats
		int					parse1DMatrix(int x, float* m);
		int					parse2DMatrix(int y, int x, float* m);
		int					parse3DMatrix(int z, int y, int x, float* m);
							// parse a braced section into a string
		std::string 		parseBracedSection();
							// parse a braced section into a string, maintaining indents and newlines
		std::string 		parseBracedSectionExact(int tabs = -1);
							// parse the rest of the line
		std::string 		parseRestOfLine();
							// pulls the entire line, including the \n at the end
		std::string 		parseCompleteLine();
							// retrieves the white space characters before the last read token
		int					getLastWhiteSpace(std::string& whiteSpace) const;
							// returns start index into text buffer of last white space
		int					getLastWhiteSpaceStart() const;
							// returns end index into text buffer of last white space
		int					getLastWhiteSpaceEnd() const;
							// set an array with punctuations, NULL restores default C/C++ set, see default_punctuations for an example
		void				setPunctuations(const Punctuation* p);
							// returns a pointer to the punctuation with the given id
		const char* 		getPunctuationFromId(int id);
							// get the id for the given punctuation
		int					getPunctuationId(const char* p);
							// reset the lexer
		void				reset();
							// returns true if at the end of the file
		bool				endOfFile();
							// get offset in script
		const int			getFileOffset();
							// print an error message
							// returns true if Error() was called with LEXFL_NOFATALERRORS or LEXFL_NOERRORS set
		bool				hadError() const;

							// set the base folder to load files from
		static void			setBaseFolder(const char* path);

		////////////////////////////////////////////////////////////////////////////
		/// Inline functions
		////////////////////////////////////////////////////////////////////////////
		inline const std::string& 		filename() const 		{ return m_filename; 		}
		inline long 					fileTime() const 		{ return m_fileTime; 		}
		inline int 						line() const 			{ return m_line; 			}
		inline int& 					flags() 				{ return m_flags; 			}
		inline int 						flags() const 			{ return m_flags; 			}
		inline Token					prevToken() const		{ return m_prevToken;		}

	};

} /// pesa namespace 

#endif /// PESA_LEXER_H_ 

