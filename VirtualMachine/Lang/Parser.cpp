/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Parser.cpp
///
/// Created on: 23 May 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Parser.h"

#include "Core/Lib/Util.h"

namespace pesa {

#define PARSER_ASSERT(condition, message) \
    do { \
        if (!(condition)) { \
        	std::ostringstream ss; \
            ss << "Error `" #condition "`(line: " << m_lex.line() << "): "<< message << std::endl; \
            std::cerr << ss.str() << std::endl; \
            Poco::Debugger::enter(); \
            Util::printStackTrace(); \
			throw Poco::Exception(ss.str()); \
        } \
    } while (false)

	
	/// These are the same as C++ taken from:
	/// http://en.cppreference.com/w/cpp/language/operator_precedence
	
	int Parser::getPrecedence(PuncId op) {
		switch (op) {
			case P_PARENTHESESOPEN:
			case P_PARENTHESESCLOSE:
			case P_BRACEOPEN:
			case P_BRACECLOSE:
			case P_SQBRACKETOPEN:
			case P_SQBRACKETCLOSE:
				return 1;
			
			case P_INC:
			case P_DEC:
				return 2;
				
			case P_POINTERREF:
			case P_BIN_NOT:
			case P_LOGIC_NOT:
			case P_REF:
				return 3;
				
			case P_MUL:
			case P_DIV:
			case P_MOD:
				return 5;
			
			case P_ADD:
			case P_SUB:
				return 6;
				
			case P_RSHIFT:
			case P_LSHIFT:
				return 7;
				
			case P_LOGIC_LESS:
			case P_LOGIC_GREATER:
			case P_LOGIC_GEQ:
			case P_LOGIC_LEQ:
				return 8;
				
			case P_LOGIC_EQ:
			case P_LOGIC_UNEQ:
				return 9;
				
			case P_BIN_AND:
				return 10;
				
			case P_BIN_XOR:
				return 11;
				
			case P_BIN_OR:
				return 12;
			
			case P_LOGIC_AND:
				return 13;
				
			case P_LOGIC_OR:
				return 14;
			
			case P_MUL_ASSIGN:
			case P_DIV_ASSIGN:
			case P_MOD_ASSIGN:
			case P_ADD_ASSIGN:
			case P_SUB_ASSIGN:
			case P_BIN_AND_ASSIGN:
			case P_BIN_OR_ASSIGN:
			case P_BIN_XOR_ASSIGN:
			case P_QUESTIONMARK:
			case P_ASSIGN:
			case P_CPP1:
			case P_CPP2:
			case P_RSHIFT_ASSIGN:
			case P_LSHIFT_ASSIGN:
				return 15;
				
			default: 
				return 0;
		}
	}

	using namespace ast;

	ast::TreePtr Parser::parseFile(const std::string& filename) { 
		m_lex.loadFile(filename.c_str(), true); 
		parse(); 
		return m_tree;
	}

	ast::TreePtr Parser::parseString(const std::string& str) {
		m_lex.loadMemory(str.c_str(), (int)str.length(), "", 0);
		parse();
		return m_tree;
	}

	void Parser::parse() {
		m_tree = TreePtr(new Tree());

		while (parseExpression(m_tree)) 
			;
	}

	ExpressionPtr Parser::parseExpression(ExpressionPtr parent) {
		if (!readToken())
			return nullptr; 

		ExpressionPtr newExpr = nullptr;

		switch (m_tok.type()) {
		case TT_STRING:
			newExpr = parseStringConstant(parent);
			break;

		//case TT_LITERAL:
		//	if ((m_tok.subType() & TT_FLOAT) || 
		//		(m_tok.subType() & TT_SINGLE_PRECISION) ||
		//		(m_tok.subType() & TT_DOUBLE_PRECISION)) {
		//		newExpr = std::make_shared<FloatConstant>(Util::cast<float>(m_tok.toString()));
		//	}
		//	break;

		case TT_NUMBER:
			newExpr = parseFloatConstant(parent);
			break;

		case TT_NAME:
			{
				newExpr = parseName(parent);

				m_lex.peekToken(&m_tok);
				if (m_tok.type() == TT_PUNCTUATION && m_tok.subType() == P_ASSIGN) {
					std::string name = newExpr->toString();
					ast::MathExpressionPtr mexpr = parseMathExpression(parent);
					mexpr->name() = name;
					newExpr = mexpr;
				}
			}
			break;

		case TT_PUNCTUATION:
		{
			// MathExpression* mexpr = dynamic_cast<MathExpression*>(parent.get());

			// if (mexpr) {
			// 	OperatorPtr op = std::make_shared<Operator>(m_tok.toString(), m_tok.subType());
			// 	mexpr->push(op);
			// 	return op;
			// }

			switch (m_tok.subType()) {
			case P_PARENTHESESOPEN:
				m_lex.unreadToken(&m_tok);
				newExpr = parseMathExpression(parent);
				break;

			case P_MUL:
			case P_DIV:
			case P_MOD:
			case P_ADD:
			case P_SUB:
			case P_ASSIGN:
				m_lex.unreadToken(&m_tok);
				newExpr = parseMathExpression(parent);
				break;
			}
			break;
		}
		}

		if (newExpr) {
			if (parent) {
				bool didAccept = parent->push(newExpr);
				ASSERT(didAccept, "Unable to push expression to parent!");
			}
		}

		return newExpr;
	}

	bool Parser::expectToken(TokenType type, int subType /* = 0 */, bool doThrow /* = true */) {
		PARSER_ASSERT(readToken(), "Unable to read next token!");
		PARSER_ASSERT(m_tok.type() == type && (subType == 0 || m_tok.subType() == subType), "Expecting token type: " << TT_NAME << " - Found: " << m_tok.type() << " - Token: " << m_tok.toString());
		return true;
	}

	bool Parser::possibleToken(TokenType type, int subType /* = 0 */, bool consume /* = true */) {
		if (!m_lex.peekToken(&m_tok))
			return false;

		if (m_tok.type() == type && (subType == 0 || m_tok.subType() == subType)) {
			if (consume)
				m_lex.consumeToken();
			return true;
		}

		return false;
	}

	ExpressionPtr Parser::parseName(ExpressionPtr parent) {
		ASSERT(m_tok.type() == TT_NAME, "Invalid! Expecting token type: " << TT_NAME << " - Found: " << m_tok.type() << " - Token: " << m_tok.toString());
		StringVec parts;
		parts.push_back(m_tok.toString());

		while (possibleToken(TT_PUNCTUATION, (int)P_REF)) {
			expectToken(TT_NAME);
			parts.push_back(m_tok.toString());
		};

		NamePtr name =  std::make_shared<Name>(parts);

		/// now that we're done here 
		if (possibleToken(TT_PUNCTUATION, (int)P_PARENTHESESOPEN, false))
			return parseFunctionCall(parent, name);

		return name;
	}

	FloatConstantPtr Parser::parseFloatConstant(ExpressionPtr) {
		if (m_tok.subType() & TT_BOOLEAN || m_tok.subType() & TT_INTEGER)
			return std::make_shared<FloatConstant>((float)m_tok.getIntValue());
		return std::make_shared<FloatConstant>(m_tok.getFloatValue());
	}

	StringConstantPtr Parser::parseStringConstant(ExpressionPtr parent) {
		return std::make_shared<StringConstant>(m_tok.toString());
	}

	MathExpressionPtr Parser::parseMathExpression(ExpressionPtr parent) {
		readToken();

		MathExpressionPtr mexpr = std::make_shared<MathExpression>();
		OperatorPtrVec parenStack;
		Token prevToken = m_lex.prevToken();

		if (m_tok.isMathOperator() && !prevToken.isNone() && !prevToken.isComma()) {
			ExpressionPtr lastExpr = parent->pop();
            /// TODO: Handle unary operators over here
			ASSERT(lastExpr, "Must've had a last expression!");
			mexpr->push(lastExpr);
			mexpr->push(std::make_shared<BinaryOperator>(m_tok.toString(), m_tok.subType()));
		}
		else if (m_tok.isMathOperator() && (prevToken.isPunctuation() || prevToken.isNone())) {
			auto subType = m_tok.subType();
			if (subType == P_SUB)
				mexpr->push(std::make_shared<FloatConstant>(-1.0f));
			else if (subType == P_ADD)
				mexpr->push(std::make_shared<FloatConstant>(1.0f));
			else {
				ASSERT(false, "Invalid/Unsupported math expression");
			}
			mexpr->push(std::make_shared<BinaryOperator>("*", P_MUL));
		}
		else if (m_tok.isOpenParen()) {
			BinaryOperatorPtr op = std::make_shared<BinaryOperator>(m_tok.toString(), m_tok.subType());
            mexpr->push(std::make_shared<BinaryOperator>(m_tok.toString(), m_tok.subType()));
            parenStack.push_back(op);
		}

		while (m_lex.peekToken(&m_tok)) {
			ExpressionPtr lastExpr = mexpr->lastExpr();
			Type lastExprType = lastExpr ? lastExpr->type() : ast::kNone;

			if (m_tok.type() == TT_PUNCTUATION) {
				int subType = m_tok.subType();
				if (subType == P_COMMA) {
					break;
				}
				else if (subType == P_PARENTHESESCLOSE) {
					if (!parenStack.size())
						return mexpr;

					mexpr->push(std::make_shared<BinaryOperator>(m_tok.toString(), m_tok.subType()));
					parenStack.pop_back();
					m_lex.consumeToken();
					continue;
				}
                else if (subType == P_PARENTHESESOPEN) {
                    BinaryOperatorPtr op = std::make_shared<BinaryOperator>(m_tok.toString(), m_tok.subType());
                    mexpr->push(op);
                    parenStack.push_back(op);
                    m_lex.consumeToken();
                    continue;
                }
                else if (subType == P_MUL ||
						 subType == P_DIV ||
						 subType == P_MOD ||
						 subType == P_ADD ||
						 subType == P_SUB) {
					std::string lastExprStr = lastExpr ? lastExpr->toString() : "";

                	// Check if its a unary operator
                	if (subType == P_SUB && (lastExprType == kNone ||
						((lastExprType == ast::kUnaryOperator || lastExprType == ast::kBinaryOperator) &&
						lastExprStr != ")" && lastExprStr != "("))) {
						ASSERT(subType == P_SUB, "Only subtraction unary operators are supported!");
						mexpr->push(std::make_shared<FloatConstant>(-1.0f));
						mexpr->push(std::make_shared<BinaryOperator>("*", P_MUL));
                	}
                	else {
						mexpr->push(std::make_shared<BinaryOperator>(m_tok.toString(), m_tok.subType()));
                	}
					m_lex.consumeToken();
					continue;
				}
			}

			parseExpression(mexpr);
		}

        PARSER_ASSERT(!parenStack.size(), "Unmatched parenthesis in expression!");

		return mexpr;
	}

	ExpressionPtr Parser::parseFunctionCallArgument(ast::FunctionCallPtr fcall) {
		ExpressionPtr expr;

		while (	!possibleToken(TT_PUNCTUATION, P_COMMA, false) &&
				!possibleToken(TT_PUNCTUATION, P_PARENTHESESCLOSE, false)) {
			expr = parseExpression(fcall);
			if (!expr)
				break;
		}

		PARSER_ASSERT(expr, "Parse error evaluating function call arguments: " << fcall->toString());;
		//fcall->push(expr);

		return expr;
	}

	FunctionCallPtr Parser::parseFunctionCall(ExpressionPtr parent, NamePtr name) {
		expectToken(TT_PUNCTUATION, P_PARENTHESESOPEN);

		FunctionCallPtr fcall = std::make_shared<FunctionCall>(name);

		while (!possibleToken(TT_PUNCTUATION, P_PARENTHESESCLOSE)) {
			ExpressionPtr expr = parseFunctionCallArgument(fcall);

			/// If the next token is not a comma then the function must end!
			if (!possibleToken(TT_PUNCTUATION, P_COMMA)) {
				expectToken(TT_PUNCTUATION, P_PARENTHESESCLOSE, true);
				break;
			}
			//PARSER_ASSERT(expr, "Invalid expression. Missing closing ')'");
		}

		return fcall;
	}
} /// pesa namespace 
