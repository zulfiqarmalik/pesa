/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Parser.h
///
/// Created on: 23 May 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#ifndef PESA_LANG_PARSER_H_
#define PESA_LANG_PARSER_H_

#include "VirtualMachine/VirtualMachineDefs.h"
#include "AST.h"
#include "Lexer.h"
#include "Token.h"
#include <string>

namespace pesa {
	class VirtualMachine_API Parser {
	private:
		Lexer 					m_lex;
		Token 					m_tok;
		ast::TreePtr 			m_tree = nullptr;

		void 					parse();
		ast::ExpressionPtr 		parseExpression(ast::ExpressionPtr parent);
		ast::ExpressionPtr		parseName(ast::ExpressionPtr parent);
		ast::FloatConstantPtr	parseFloatConstant(ast::ExpressionPtr parent);
		ast::StringConstantPtr	parseStringConstant(ast::ExpressionPtr parent);
		ast::MathExpressionPtr	parseMathExpression(ast::ExpressionPtr parent);
		ast::FunctionCallPtr	parseFunctionCall(ast::ExpressionPtr parent, ast::NamePtr name);
		ast::ExpressionPtr		parseFunctionCallArgument(ast::FunctionCallPtr parent);

		bool 					expectToken(TokenType type, int subType = 0, bool doThrow = true);
		bool 					possibleToken(TokenType type, int subType = 0, bool consume = true);

	public:
		ast::TreePtr			parseString(const std::string& str);
		ast::TreePtr			parseFile(const std::string& filename);

		////////////////////////////////////////////////////////////////////////////
		/// Inline functions
		////////////////////////////////////////////////////////////////////////////
		inline int 				readToken() { return m_lex.readToken(&m_tok); }
		inline ast::TreePtr 	tree() { return m_tree; }

		////////////////////////////////////////////////////////////////////////////
		/// Static functions
		////////////////////////////////////////////////////////////////////////////
		static int 				getPrecedence(PuncId op);
	};
}

#endif /// PESA_LANG_PARSER_H_
