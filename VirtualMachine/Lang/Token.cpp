/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Token.cpp
///
/// Created on: 31 Mar 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Token.h"
#include "Lexer.h"
#include "Core/Lib/Util.h"

namespace pesa {


	Token::Token() : m_type(), m_subType(), m_line(), m_linesCrossed(), m_flags() {
	}

	Token::Token(const Token& rhs) {
		*this = rhs;
	}

	Token::~Token() {
	}

	Token::operator std::string () {
		return m_tokenValue;
	}

	std::string Token::toString() const {
		return m_tokenValue;
	}

	void Token::operator = (const char *text) {
		m_tokenValue = text;
	}

	void Token::operator = (const std::string& text) {
		m_tokenValue = text;
	}

	void Token::setWhiteSpaceStart(const char* s) {
		m_whiteSpaceStart = s;
	}

	void Token::setWhiteSpaceEnd(const char* s) {
		m_whiteSpaceEnd = s;
	}

	double Token::getDoubleValue() {
		if (m_type != TT_NUMBER) {
			return 0.0;
		}
		if (!(m_subType & TT_VALUESVALID)) {
			numberValue();
		}
		return m_floatValue;
	}

	float Token::getFloatValue() {
		return (float)getDoubleValue();
	}

	unsigned long Token::getUnsignedLongValue() {
		if (m_type != TT_NUMBER) {
			return 0;
		}
		if (!(m_subType & TT_VALUESVALID)) {
			numberValue();
		}
		return m_intValue;
	}

	bool Token::getBoolValue() {
		return getUnsignedLongValue() != 0 ? true : false;
	}

	int Token::getIntValue() {
		return (int)getUnsignedLongValue();
	}

	int Token::whiteSpaceBeforeToken() const {
		return (m_whiteSpaceEnd > m_whiteSpaceStart);
	}

	void Token::append(const char* s, size_t length) {
		for (size_t i = 0; i < length; i++)
			m_tokenValue += (s + i);
	}

	void Token::appendDirty(const char a) {
		// just call the base std::string implementation ...
		m_tokenValue += a;
	}

	void Token::ensureAllocated(size_t requiredLength) {
		// we do not want to downsize
		if (length() >= requiredLength)
			return;

		resize(requiredLength);
	}

	void Token::numberValue() {
		int i, pow, div, c;
		const char *p;
		double m;

		ASSERT(m_type == TT_NUMBER, "Type is not a number: " << m_type);

		p = c_str();
		m_floatValue = 0;
		m_intValue = 0;

		// floating point number
		if (m_subType & TT_FLOAT) {
			if (m_subType & (TT_INFINITE | TT_INDEFINITE | TT_NAN)) {
				if (m_subType & TT_INFINITE) {			// 1.#INF
					unsigned int inf = 0x7f800000;
					m_floatValue = (double) *(float*)&inf;
				}
				else if (m_subType & TT_INDEFINITE) {	// 1.#IND
					unsigned int ind = 0xffc00000;
					m_floatValue = (double) *(float*)&ind;
				}
				else if (m_subType & TT_NAN) {			// 1.#QNAN
					unsigned int nan = 0x7fc00000;
					m_floatValue = (double) *(float*)&nan;
				}
			}
			else {
				while(*p && *p != '.' && *p != 'e') {
					m_floatValue = m_floatValue * 10.0 + (double) (*p - '0');
					p++;
				}
				if (*p == '.') {
					p++;
					for(m = 0.1; *p && *p != 'e'; p++) {
						m_floatValue = m_floatValue + (double) (*p - '0') * m;
						m *= 0.1;
					}
				}
				if (*p == 'e') {
					p++;
					if (*p == '-') {
						div = true;
						p++;
					}
					else if (*p == '+') {
						div = false;
						p++;
					}
					else {
						div = false;
					}
					pow = 0;
					for (pow = 0; *p; p++) {
						pow = pow * 10 + (int) (*p - '0');
					}
					for (m = 1.0, i = 0; i < pow; i++) {
						m *= 10.0;
					}
					if (div) {
						m_floatValue /= m;
					}
					else {
						m_floatValue *= m;
					}
				}
			}

			m_intValue = (int)(m_floatValue);
		}
		else if (m_subType & TT_DECIMAL) {
			while(*p) {
				m_intValue = m_intValue * 10 + (*p - '0');
				p++;
			}
			m_floatValue = m_intValue;
		}
		else if (m_subType & TT_IPADDRESS) {
			c = 0;
			while(*p && *p != ':') {
				if (*p == '.') {
					while(c != 3) {
						m_intValue = m_intValue * 10;
						c++;
					}
					c = 0;
				}
				else {
					m_intValue = m_intValue * 10 + (*p - '0');
					c++;
				}
				p++;
			}
			while(c != 3) {
				m_intValue = m_intValue * 10;
				c++;
			}
			m_floatValue = m_intValue;
		}
		else if (m_subType & TT_OCTAL) {
			// step over the first zero
			p += 1;
			while(*p) {
				m_intValue = (m_intValue << 3) + (*p - '0');
				p++;
			}
			m_floatValue = m_intValue;
		}
		else if (m_subType & TT_HEX) {
			// step over the leading 0x or 0X
			p += 2;
			while(*p) {
				m_intValue <<= 4;
				if (*p >= 'a' && *p <= 'f')
					m_intValue += *p - 'a' + 10;
				else if (*p >= 'A' && *p <= 'F')
					m_intValue += *p - 'A' + 10;
				else
					m_intValue += *p - '0';
				p++;
			}
			m_floatValue = m_intValue;
		}
		else if (m_subType & TT_BINARY) {
			// step over the leading 0b or 0B
			p += 2;
			while(*p) {
				m_intValue = (m_intValue << 1) + (*p - '0');
				p++;
			}
			m_floatValue = m_intValue;
		}
		m_subType |= TT_VALUESVALID;
	}

	void Token::clearTokenWhiteSpace() {
		m_whiteSpaceStart = NULL;
		m_whiteSpaceEnd = NULL;
		m_linesCrossed = 0;
	}

	bool Token::isPunctuation() const {
		return m_type == TT_PUNCTUATION;
	}

	bool Token::isOpenParen() const {
		return m_type == TT_PUNCTUATION && m_subType == P_PARENTHESESOPEN;
	}

	bool Token::isCloseParen() const {
		return m_type == TT_PUNCTUATION && m_subType == P_PARENTHESESCLOSE;
	}

	bool Token::isMathOperator() const {
		return m_type == TT_PUNCTUATION && 
			(
				m_subType == P_MUL || 
				m_subType == P_DIV || 
				m_subType == P_MOD || 
				m_subType == P_ADD || 
				m_subType == P_SUB
			);
	}

	bool Token::isComma() const {
		return m_type == TT_PUNCTUATION && m_subType == P_COMMA;
	}
} /// pesa namespace 