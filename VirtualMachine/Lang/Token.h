/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Token.h
///
/// Created on: 31 Mar 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#ifndef PESA_TOKEN_H_
#define PESA_TOKEN_H_

#include "VirtualMachine/VirtualMachineDefs.h"
#include <string>

#ifdef __WINDOWS__
    #pragma warning(disable : 4251) /// class needs to have a dll-interface 
#endif 

/*
===============================================================================
	Token is a token read from a file or memory with Lexer or Parser
===============================================================================
*/

namespace pesa {
	// token types
	enum TokenType {
		TT_NONE						= 0,			// Invalid token
		TT_STRING					= 1,			// String
		TT_LITERAL					= 2,			// Literal
		TT_NUMBER					= 3,			// Number
		TT_NAME						= 4,			// Name
		TT_PUNCTUATION				= 5,			// Punctuation
	};

	// number sub types
	enum TokenSubType {
		TT_INTEGER					= 0x00001,		// Integer
		TT_DECIMAL					= 0x00002,		// Decimal number
		TT_HEX						= 0x00004,		// Hexadecimal number
		TT_OCTAL					= 0x00008,		// Octal number
		TT_BINARY					= 0x00010,		// Binary number
		TT_LONG						= 0x00020,		// Long int
		TT_UNSIGNED					= 0x00040,		// Unsigned int
		TT_FLOAT					= 0x00080,		// Floating point number
		TT_SINGLE_PRECISION			= 0x00100,		// Float
		TT_DOUBLE_PRECISION			= 0x00200,		// Double
		TT_EXTENDED_PRECISION		= 0x00400,		// Long double
		TT_INFINITE					= 0x00800,		// Infinite 1.#INF
		TT_INDEFINITE				= 0x01000,		// Indefinite 1.#IND
		TT_NAN						= 0x02000,		// NaN
		TT_IPADDRESS				= 0x04000,		// Ip address
		TT_IPPORT					= 0x08000,		// Ip port
		TT_VALUESVALID				= 0x10000,		// Set if intvalue and floatvalue are valid
		TT_BOOLEAN					= 0x20000,		// For booleans
	};

	// string sub type is the length of the string
	// literal sub type is the ASCII code
	// punctuation sub type is the punctuation id
	// name sub type is the length of the name

	class VirtualMachine_API Token {
	private:
		std::string 	m_tokenValue;						// The actual string token value

		unsigned long	m_intValue;							// Integer value
		double			m_floatValue;						// Floating point value
		const char*		m_whiteSpaceStart;					// Start of white space before token, only used by idLexer
		const char*		m_whiteSpaceEnd;					// End of white space before token, only used by idLexer
		Token*			m_next;								// Next token in chain, only used by idParser

		int				m_type = (int)TT_NONE;				// Token type
		int				m_subType;							// Token sub type
		int				m_line;								// Line in script the token was on
		int				m_linesCrossed;						// Number of lines crossed in white space before token
		int				m_flags;							// Token flags, used for recursive defines


	public:
						Token();
						Token(const Token& token);
						~Token();

		void			operator = (const std::string& text);
		void			operator = (const char *text);

		double			getDoubleValue();					// Double value of TT_NUMBER
		float			getFloatValue();					// Float value of TT_NUMBER
		unsigned long	getUnsignedLongValue();				// Unsigned long value of TT_NUMBER
		int				getIntValue();						// Int value of TT_NUMBER
		bool			getBoolValue();						// Int value of TT_BOOLEAN
		int				whiteSpaceBeforeToken() const;		// Returns length of whitespace before token
		void			clearTokenWhiteSpace();				// Forget whitespace before token

		void			numberValue();						// Calculate values for a TT_NUMBER
		void			appendDirty(const char a);			// Append character without adding trailing zero
		void 			append(const char* s, size_t length);
		void 			ensureAllocated(size_t length);
		void 			setWhiteSpaceStart(const char* s);
		void 			setWhiteSpaceEnd(const char* s);

		bool 			isMathOperator() const;
		bool			isPunctuation() const;
		bool			isOpenParen() const;
		bool			isCloseParen() const;
		bool			isComma() const;
		inline bool		isNone() const { return m_type == TT_NONE; }

		std::string 	toString() const;

						operator std::string ();

		////////////////////////////////////////////////////////////////////////////
		/// Inline functions
		////////////////////////////////////////////////////////////////////////////
		inline size_t 	size() const 			{ return m_tokenValue.size(); 	}
		inline size_t 	length() const 			{ return m_tokenValue.length(); }
		inline bool 	empty() const 			{ return m_tokenValue.empty(); 	}
		inline void 	clear()  				{ m_tokenValue.clear(); 		}
		inline const char* c_str() const 		{ return m_tokenValue.c_str(); 	}
		inline void		resize(size_t size)		{ m_tokenValue.resize(size);	}
		inline char&	operator [] (size_t i)	{ return m_tokenValue[i]; }
		inline char		operator [] (size_t i) const { return m_tokenValue[i];		}
		inline bool 	operator == (const std::string& rhs) const { return m_tokenValue == rhs; }
		inline bool 	operator != (const std::string& rhs) const { return m_tokenValue != rhs; }

		inline int& 	type() 					{ return m_type; 		}
		inline int 		type() const			{ return m_type; 		}
		inline int& 	subType() 				{ return m_subType; 	}
		inline int 		subType() const 		{ return m_subType;		}
		inline int& 	line() 					{ return m_line; 		}
		inline int 		line() const 			{ return m_line; 		}
		inline int& 	linesCrossed() 			{ return m_linesCrossed;}
		inline int 		linesCrossed() const 	{ return m_linesCrossed;}
		inline int& 	flags() 				{ return m_flags; 		}
		inline int 		flags() const 			{ return m_flags; 		}
		inline unsigned long& intValue() 		{ return m_intValue; 	}
		inline double& 	floatValue() 			{ return m_floatValue;	}

		inline const char* whiteSpaceStart()  		{ return m_whiteSpaceStart; }
		inline const char* whiteSpaceEnd()  		{ return m_whiteSpaceEnd; 	}
		inline const char* whiteSpaceStart() const 	{ return m_whiteSpaceStart; }
		inline const char* whiteSpaceEnd() const 	{ return m_whiteSpaceEnd; 	}
	};

}


#endif /* !PESA_TOKEN_H_ */
