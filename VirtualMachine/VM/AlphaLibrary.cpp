/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// AlphaLibrary.cpp
///
/// Created on: 21 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "AlphaLibrary.h"
#include "IMachine.h"
#include "VirtualMachine/Lang/Parser.h"
#include "Variable.h"
#include "Core/Math/Stats.h"

#include "Core/Lib/Profiler.h"

namespace pesa {
namespace vm {
	////////////////////////////////////////////////////////////////////////////
	/// AlphaLibrary
	////////////////////////////////////////////////////////////////////////////
	AlphaLibrary::~AlphaLibrary() {
	}

	MethodPtr AlphaLibrary::find(const std::string& name, const std::string& libName) {
		return std::make_shared<AlphaMethod>(name, libName);
	}

	////////////////////////////////////////////////////////////////////////////
	/// AlphaMethod
	////////////////////////////////////////////////////////////////////////////
	AlphaMethod::AlphaMethod(const std::string& name, const std::string& libName) 
		: Method(name, 1)
		, m_libName(libName) 
		, m_rt(0) {
	}

	AlphaMethod::~AlphaMethod() {
		m_args.clear();
		m_alpha = nullptr;
		delete m_rt;
	}

	IAlphaPtr AlphaMethod::loadAlpha(ConfigSection& config, const std::string& name, const std::string& libName) {
		config.set("id", name);
		config.set("moduleId", libName);
		return helper::ShLib::global().createComponent<IAlpha>(config, nullptr);
	}

	Data* AlphaMethod::call(IMachine& vm, size_t numArgs) {
		PROFILE_SCOPED();

		if (!m_alpha) {
			m_config = vm.getConfig();
			m_alpha = loadAlpha(m_config, m_name, m_libName);

			ASSERT(m_alpha, "Unable to dynamically load alpha: " << m_libName << "." << m_name);
		}

		/// Reinit the alpha every time 
		vm.prepareAlpha(m_alpha.get());

		FloatMatrixDataPtrVec dynamicArgs;

		for (size_t i = 0; i < numArgs; i++) {
			DataPtr var = vm.pop();
			std::string name = var->name();

			/// Only do this if its a named argument
			if (var->namedArgument()) {
				if (name == "days")
					m_alpha->pinfo().days = (IntDay)std::dynamic_pointer_cast<ConstantFloat>(var)->value();
				else if (name == "shortTermDays")
					m_alpha->pinfo().shortTermDays = (IntDay)std::dynamic_pointer_cast<ConstantFloat>(var)->value();
				else if (name == "longTermDays")
					m_alpha->pinfo().longTermDays = (IntDay)std::dynamic_pointer_cast<ConstantFloat>(var)->value();
				else if (name == "omitDays")
					m_alpha->pinfo().omitDays = (IntDay)std::dynamic_pointer_cast<ConstantFloat>(var)->value();
				else if (name == "delta")
					m_alpha->pinfo().delta = (IntDay)std::dynamic_pointer_cast<ConstantFloat>(var)->value();
				else if (name == "dayOfWeek")
					m_alpha->pinfo().dayOfWeek= (IntDay)std::dynamic_pointer_cast<ConstantFloat>(var)->value();
				else if (name == "delay")
					m_alpha->pinfo().delay = (IntDay)std::dynamic_pointer_cast<ConstantFloat>(var)->value();
				else if (name == "weight") 
					m_alpha->pinfo().weight = (float)std::dynamic_pointer_cast<ConstantFloat>(var)->value();
				else if (name == "reversion")
					m_alpha->pinfo().reversion = (float)std::dynamic_pointer_cast<ConstantFloat>(var)->value() > 0.0001f ? -1.0f : 1.0f;
				else {
					/// set it as a named value in the 
					ConfigSection& alphaConfig = const_cast<ConfigSection&>(m_alpha->config());
					FloatMatrixDataPtr value = std::dynamic_pointer_cast<FloatMatrixData>(var);
					ASSERT(value, "Unknown values can ONLY be FloatMatrixData type (or equivalent e.g. booleans or integers converted to float)");
					m_alpha->addNamedVar(name, value);
				}

				continue;
			}

			m_args.push_back(var);
			FloatMatrixDataPtr dynamicArg = std::dynamic_pointer_cast<FloatMatrixData>(var);
			ASSERT(dynamicArg, "Invalid variable used in the alpha expression. Alphas can only accept floating point variables!");
			dynamicArgs.push_back(dynamicArg);
		}

		for (int i = (int)dynamicArgs.size() - 1; i >= 0; i--)
			m_alpha->dynamicArgs().push_back(dynamicArgs[i]);

		const RuntimeData& rt	= vm.dataRegistry()->pipelineHandler()->runtimeData();
		if (!m_rt)	
			m_rt				= new RuntimeData(rt);
		else
			m_rt->di			= rt.di;

		/// we say that we have backdays worth of data because that is how much we can get
		m_results.resize(rt.dates.size());

		size_t universeSize		= vm.universe()->size(rt.di);
		m_alpha->data().ensureAlphaSize(1, universeSize);

		return nullptr;
	}

	FloatMatrixDataPtr AlphaMethod::call(IntDay di) const {
		size_t diIndex = di; ///(size_t)std::max(m_rt.di - di, 0);
		ASSERT(m_results.size() > diIndex, "Invalid di: " << di);
		if (m_results[diIndex])
			return m_results[diIndex];

		(const_cast<AlphaMethod*>(this))->call(m_alpha.get(), *m_rt, di, 0);
		ASSERT(m_results[diIndex], "Unable to run AlphaMethod on di: " << di);
		return m_results[diIndex];
	}

	float AlphaMethod::call(IAlpha* alpha, const RuntimeData& rt_, IntDay di, size_t ii) const {
		PROFILE_SCOPED();

		//ASSERT(di == 0, "Alpha results are 1-Dimensional. Invalid di = " << di);
		//ASSERT(m_alpha, "Function call data has not been initialised!");
		//ASSERT(ii < m_alpha->alpha().cols(), "Invalid index: " << ii << " - Max Size: " << m_alpha->alpha().cols());
		//return m_alpha->alpha()(0, ii);

		size_t diIndex = di; /// (size_t)std::max(m_rt.di - di, 0);
		ASSERT(m_results.size() > diIndex, "Invalid di: " << di);

		/// If we already have that data in the cache then we just get that ...
		if (m_results[diIndex])
			return (*m_results[diIndex])(0, ii);

		/// Pop the last result results from the back 
		int popIndex = (int)diIndex - ((int)m_alpha->parentConfig()->defs().backdays() + 1);
		if (popIndex >= 0)
			m_results[popIndex] = nullptr;

		/// If not then we cache it just then
		/// 1. Make a copy of the runtime data. We do this to fudge the di
		//RuntimeData rt = rt_.di != Constants::s_invalidDay ? rt_ : m_rt;
		//rt.di = di + m_alpha->delay();
		//rt.di = di;

		//ASSERT(rt_.di != Constants::s_invalidDay, "Invalid di in runtimeData!");
		m_rt->di = di;

		/// 2. Then we run the alpha
		m_alpha->run(*m_rt);

		/// 3. Then we make a copy of the data
		m_results[diIndex] = m_alpha->data().alpha;

		/// 4. Then we reset the buffer so that our data is not over-writen if this function is run again 
		m_alpha->data().resetAlpha();
		float value = (*m_results[diIndex])(0, ii);
		return value;
	}

	size_t AlphaMethod::rows() const {
		//return m_alpha->alpha().rows();
		//return m_alpha->parentConfig()->defs().backdays();
		return m_results.size();
	}

	size_t AlphaMethod::cols() const {
		return m_alpha->alpha().cols();
	}

	//////////////////////////////////////////////////////////////////////////
	/// PyAlphaLibrary
	//////////////////////////////////////////////////////////////////////////
	MethodPtr PyAlphaLibrary::find(const std::string& name, const std::string& libName) {
		return std::make_shared<PyAlphaMethod>(name, libName);
	}

	//////////////////////////////////////////////////////////////////////////
	/// PyAlphaMethod
	//////////////////////////////////////////////////////////////////////////
	IAlphaPtr PyAlphaMethod::loadAlpha(ConfigSection& config, const std::string& name, const std::string& libName) {
		config.remove("path");
		config.set("id", "PyAlpha");
		config.set("pyClassName", name);
		config.set("pyPath", name + ".py");
		config.set("moduleId", "PyImpl");
		return helper::ShLib::global().createComponent<IAlpha>(config, nullptr);
	}

}
} /// namespace pesa
