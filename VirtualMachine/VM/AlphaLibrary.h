/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// AlphaLibrary.h
///
/// Created on: 21 Jul 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#ifndef VirtualMachine_AlphaLibrary_h_
#define VirtualMachine_AlphaLibrary_h_

#include "Framework/Framework.h"
#include "VirtualMachine/VirtualMachineDefs.h"
#include <memory>
#include <unordered_map>
#include "Library.h"

namespace pesa {
	class FloatMatrixData;

namespace vm {

	////////////////////////////////////////////////////////////////////////////
	/// AlphaLibrary - Can handle any number of arguments
	////////////////////////////////////////////////////////////////////////////
	class VirtualMachine_API AlphaLibrary : public Library {
	protected:
		MethodMap				m_methods; 		/// The methods that exist within this library

	public:
		using					Library::Library;
		virtual 				~AlphaLibrary();

		virtual MethodPtr 		find(const std::string& name, const std::string& libName);
	};

	////////////////////////////////////////////////////////////////////////////
	/// AlphaMethod - Can handle any number of arguments
	////////////////////////////////////////////////////////////////////////////
	class VirtualMachine_API AlphaMethod : public Method {
	protected:
		std::string				m_libName;			/// The lib name
		mutable IAlphaPtr		m_alpha = nullptr;	/// The alpha
		mutable FloatMatrixDataPtrVec m_results;	/// The results that we have so far
		DataPtrVec				m_args; 			/// The arguments
		ConfigSection			m_config = nullptr;	/// The config copy
		RuntimeData*			m_rt = nullptr;		/// The data that we have
		IMachine*				m_vm; ;				/// Virtual machine instance 

	public:
								AlphaMethod(const std::string& name, const std::string& libName);
								~AlphaMethod();
		virtual Data*			call(IMachine& vm, size_t numArgs);
		
		virtual float 			call(IAlpha* alpha, const RuntimeData& rt, IntDay di, size_t ii) const;
		virtual FloatMatrixDataPtr	call(IntDay di) const;
		virtual size_t 			rows() const;
		virtual size_t 			cols() const;
		virtual IAlphaPtr		loadAlpha(ConfigSection& config, const std::string& name, const std::string& libName);
	};

	//////////////////////////////////////////////////////////////////////////
	/// PyAlphaLibrary - For PYTHON related alphas
	//////////////////////////////////////////////////////////////////////////
	class VirtualMachine_API PyAlphaLibrary : public AlphaLibrary {
	public:
		using					AlphaLibrary::AlphaLibrary;
		virtual MethodPtr 		find(const std::string& name, const std::string& libName);
	};

	//////////////////////////////////////////////////////////////////////////
	/// PyAlphaMethod - Can handle any number of arguments
	//////////////////////////////////////////////////////////////////////////
	class VirtualMachine_API PyAlphaMethod : public AlphaMethod {
	public:
		using					AlphaMethod::AlphaMethod;

		virtual IAlphaPtr		loadAlpha(ConfigSection& config, const std::string& name, const std::string& libName);
	};
} /// namespace vm
} /// namespace pesa

#endif /// VirtualMachine_AlphaLibrary_h_
