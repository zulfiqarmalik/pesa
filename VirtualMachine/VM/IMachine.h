/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// IMachine.h
///
/// Created on: 23 May 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#ifndef VirtualMachine_IMachine_h_
#define VirtualMachine_IMachine_h_

#include "VirtualMachine/VirtualMachineDefs.h"
#include "Framework/Data/Data.h"
#include <string>

namespace pesa {
namespace vm {
	class VirtualMachine_API IMachine {
	public:
		virtual IDataRegistry* 			dataRegistry() = 0;
		virtual IUniverse* 				universe() = 0;
		virtual const ConfigSection&	getConfig() = 0;
		virtual void					prepareAlpha(IAlpha* alpha) = 0;

		virtual void 					exec() = 0;
		virtual void 					push(DataPtr var) = 0;
		virtual DataPtr 				pop() = 0;
		virtual size_t					size() = 0;
		virtual size_t					orgSize() { return size(); }

		virtual void					setStackPtr(int sp, int spCount) = 0;
		virtual void					getStackPtr(int* sp, int* spCount) = 0;

		virtual IAlpha*					alpha() = 0;
		virtual const RuntimeData&		rt() const = 0;

		virtual bool					applyDelay() { return false; }

		/// Override for optimal implementation ...
		virtual DataPtr 			peek() {
			DataPtr var = pop();
			push(var);
			return var;
		}
	};
} /// namespace vm
} /// namespace pesa

#endif /// VirtualMachine_IMachine_h_
