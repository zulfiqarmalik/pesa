/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Instruction.cpp
///
/// Created on: 23 Jun 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Instruction.h"
#include "IMachine.h"
#include "VirtualMachine/Lang/Parser.h"

namespace pesa {
namespace vm {
	
	////////////////////////////////////////////////////////////////////////////
	/// Operand
	////////////////////////////////////////////////////////////////////////////
	void Operand::exec(IMachine& vm) {
		m_var->applyDelay() = vm.applyDelay();
		vm.push(m_var);
	}

	////////////////////////////////////////////////////////////////////////////
	/// Operator
	////////////////////////////////////////////////////////////////////////////
	void Operator::exec(IMachine& vm) {
	}

	////////////////////////////////////////////////////////////////////////////
	/// BinaryOperator
	////////////////////////////////////////////////////////////////////////////
	BinaryOperator::BinaryOperator(const std::string& op, int opType) 
		: Operator(op, opType) {
		switch (m_opType) {
		case P_MUL:
			m_func = BinaryOperator::mul;
			break;
		case P_DIV:
			m_func = BinaryOperator::div;
			break;
		case P_ADD:
			m_func = BinaryOperator::add;
			break;
		case P_SUB:
			m_func = BinaryOperator::sub;
			break;
		case P_MOD:
			m_func = BinaryOperator::mod;
			break;
		default:
			ASSERT(false, "Unsupported binary operator: " << op);
			break;
		}
	}

	void BinaryOperator::exec(IMachine& vm) {
		DataPtr rhs = vm.pop();
		DataPtr lhs = vm.pop();

		ASSERT(lhs && rhs, "Invalid parameters on the stack!");

		BinOpData* result = new BinOpData(vm.universe(), m_func, m_opType, lhs, rhs);
		ASSERT(result, "NULL result of a binary operation!");

		result->applyDelay() = vm.applyDelay();

		vm.push(DataPtr(result));
	}

	float BinaryOperator::exec(float lhs, float rhs) const {
		ASSERT(m_func, "No func found for operator: " << m_op);
		return m_func(lhs, rhs);
	}
	////////////////////////////////////////////////////////////////////////////
	/// MathInstruction
	////////////////////////////////////////////////////////////////////////////
	void MathInstruction::exec(IMachine& vm) {
		int sp = -1, spCount = -1;

		if (m_sp > 0 && m_sp < (int)vm.orgSize() - 1) {
			vm.getStackPtr(&sp, &spCount);
			vm.setStackPtr(m_sp, m_spCount);
		}

		size_t stackSize = vm.size();

		for (InstructionPtr inst : m_instructions)
			inst->exec(vm);

		if (vm.size() > stackSize && !m_name.empty()) {
			DataPtr lastVar = vm.peek();
			lastVar->setName(m_name);
			lastVar->namedArgument() = true;
			lastVar->applyDelay() = vm.applyDelay();
			//vm.push(lastVar);
		}

		vm.setStackPtr(sp, spCount);
	}

	void MathInstruction::push(MathInstruction* inst) {
		m_instructions.push_back(std::shared_ptr<MathInstruction>(inst));
	}

	void MathInstruction::push(FunctionCallPtr inst) {
		m_instructions.push_back(inst);
	}

	void MathInstruction::push(DataPtr oper) {
		m_instructions.push_back(std::make_shared<Operand>(oper));
	}

	int MathInstruction::lastOp() const {
		if (!m_opStack.size())
			return 0;
		return m_opStack[m_opStack.size() - 1]->opType();
	}

	void MathInstruction::end() {
		while (m_opStack.size()) {
			if (lastOp() != P_PARENTHESESOPEN)
				m_instructions.push_back(m_opStack[m_opStack.size() - 1]);
			m_opStack.pop_back();
		}
	}

	void MathInstruction::push(OperatorPtr op) {
		int lastOpType = lastOp();
		int currOpType = op->opType();

		int lastOpPrec = Parser::getPrecedence((PuncId)lastOpType);
		int currOpPrec = Parser::getPrecedence((PuncId)currOpType);


		/// Opening parenthesis go straight to the stack
		if (currOpType == P_PARENTHESESOPEN)
			pushOp(op);
		else if (currOpType == P_PARENTHESESCLOSE) {
			/// Closing parentesis means that we keep on popping 
			/// until we reach the last open parenthesis
			while (lastOpType != P_PARENTHESESOPEN) {
				m_instructions.push_back(popOp());
				lastOpType = lastOp();
				lastOpPrec = Parser::getPrecedence((PuncId)lastOpType);
			}
		}
		else {
			/// Less value means its more precedence (following C++ convention)
			while (currOpPrec <= lastOpPrec) {
				m_instructions.push_back(popOp());
				lastOpType = lastOp();
				lastOpPrec = Parser::getPrecedence((PuncId)lastOpType);
			}

			pushOp(op);
		}
	}

	OperatorPtr MathInstruction::popOp() {
		if (!m_opStack.size())
			return nullptr;

		OperatorPtr op = m_opStack[m_opStack.size() - 1];
		m_opStack.pop_back();
		return op;
	}

	void MathInstruction::pushOp(OperatorPtr op) {
		m_opStack.push_back(op);
	}

	////////////////////////////////////////////////////////////////////////////
	/// FunctionCall
	////////////////////////////////////////////////////////////////////////////
	FunctionCall::FunctionCall(const StringVec& functionName, size_t numArgs) 
		: m_numArgs(numArgs) {
		m_functionName = Util::combine(functionName);

		LibraryPtr lib = LibraryManager::instance().find(functionName[0]);
		ASSERT(lib, "Unable to find library: " << functionName[0]);

		std::string methodName = Util::combine(functionName, ".", 1, functionName.size());
		ASSERT(!methodName.empty(), "Invalid function name!");

		m_method = lib->find(methodName, functionName[0]);
		ASSERT(m_method, "Unable to find method: " << methodName);
	}

	void FunctionCall::exec(IMachine& vm) {
		Trace("VM", "Executing function: %s", m_functionName);

		int sp = -1, spCount = -1;

		if (m_sp > 0 && m_sp < (int)vm.orgSize() - 1) {
			vm.getStackPtr(&sp, &spCount);
			vm.setStackPtr(m_sp, (int)m_numArgs);
		}

		Data* result = m_method->call(vm, m_numArgs);
		if (!result) {
			auto rptr = DataPtr(new FunctionCallData(vm.universe(), m_method));
			rptr->applyDelay() = vm.applyDelay();
			vm.push(rptr);
		}
		else {
			auto rptr = DataPtr(result);
			rptr->applyDelay() = vm.applyDelay();
			vm.push(rptr);
		}

		vm.setStackPtr(sp, spCount);
	}
}
} /// namespace pesa
