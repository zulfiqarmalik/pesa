/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Instruction.h
///
/// Created on: 23 Jun 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#pragma once

#include "Framework/Framework.h"
#include "VirtualMachine/VirtualMachineDefs.h"
#include "Variable.h"
#include "Library.h"
#include "Core/Math/Math.h"

namespace pesa {
namespace vm {

	class IMachine;

	////////////////////////////////////////////////////////////////////////////
	/// Instruciton - Generic base instructions
	////////////////////////////////////////////////////////////////////////////
	class VirtualMachine_API Instruction {
	public:
		virtual 				~Instruction() {}
		
		virtual void 			exec(IMachine& vm) = 0;
	};

	typedef std::shared_ptr<Instruction> 	InstructionPtr;
	typedef std::vector<InstructionPtr> 	InstructionPtrVec;

	////////////////////////////////////////////////////////////////////////////
	/// Operand
	////////////////////////////////////////////////////////////////////////////
	class VirtualMachine_API Operand : public Instruction {
	protected:
		DataPtr					m_var;

	public:
								Operand(DataPtr var) : m_var(var) {}
		virtual void 			exec(IMachine& vm);
		inline const DataPtr 	var() const { return m_var; }
	};
	typedef std::shared_ptr<Operator> 		OperandPtr;
	typedef std::vector<OperandPtr>			OperandPtrVec;

	////////////////////////////////////////////////////////////////////////////
	/// Operator
	////////////////////////////////////////////////////////////////////////////
	class VirtualMachine_API Operator : public Instruction {
	protected:
		std::string 			m_op;
		int 					m_opType;

	public:
								Operator(const std::string& op, int opType) : m_op(op), m_opType(opType) {}

		virtual void 			exec(IMachine& vm);
		inline const std::string& op() const { return m_op; }
		inline int 				opType() const { return m_opType; }
	};
	typedef std::shared_ptr<Operator> OperatorPtr;
	typedef std::vector<OperatorPtr> OperatorPtrVec;

	////////////////////////////////////////////////////////////////////////////
	/// BinaryOperator
	////////////////////////////////////////////////////////////////////////////
	typedef const Eigen::CwiseBinaryOp<Eigen::internal::scalar_sum_op<float>, const EigenRowMatrixf, const EigenRowMatrixf> VecOp;

	class VirtualMachine_API BinaryOperator : public Operator {
	public:
		typedef float 			mathOpFunc(float lhs, float rhs); 
		typedef const VecOp		vecMathOpFunc(const EigenBlockf& lhs, const EigenBlockf& rhs);

	protected:
		mathOpFunc* 			m_func = nullptr;

		static float 			add(float lhs, float rhs) { return lhs + rhs; }
		static float 			sub(float lhs, float rhs) { return lhs - rhs; }
		static float 			mul(float lhs, float rhs) { return lhs * rhs; }
		static float 			div(float lhs, float rhs) { return rhs != 0.0f ? lhs / rhs : std::nanf("0"); }
		static float 			mod(float lhs, float rhs) { return (float)((int)lhs % (int)rhs); }

		//static const VecOp		vadd(const EigenBlockf& lhs, const EigenBlockf& rhs) { return lhs + rhs; }
		//static const VecOp		vsub(const EigenBlockf& lhs, const EigenBlockf& rhs) { return lhs - rhs; }
		//static const VecOp		vmul(const EigenBlockf& lhs, const EigenBlockf& rhs) { return lhs * rhs; }
		//static const VecOp		vdiv(const EigenBlockf& lhs, const EigenBlockf& rhs) { return lhs / rhs; }
	public:

        						BinaryOperator(const std::string& op, int opType);

		virtual void 			exec(IMachine& vm);
		float 					exec(float lhs, float rhs) const;
		mathOpFunc* 			func() { return m_func; }
	};
	typedef std::shared_ptr<BinaryOperator> BinaryOperatorPtr;
	typedef std::vector<BinaryOperatorPtr> BinaryOperatorPtrVec;

	////////////////////////////////////////////////////////////////////////////
	/// FunctionCall - All sorts of function calls
	////////////////////////////////////////////////////////////////////////////
	class VirtualMachine_API FunctionCall : public Instruction {
	protected:
		std::string				m_functionName;
		size_t 					m_numArgs = 0;
		int						m_sp = -1;
		MethodPtr 				m_method = nullptr;

	public:
		FunctionCall(const StringVec& functionName, size_t numArgs);
		virtual void 			exec(IMachine& vm);

		inline int&				sp() { return m_sp; } 
		inline int				sp() const { return m_sp; }
	};

	typedef std::shared_ptr<FunctionCall> FunctionCallPtr;

	////////////////////////////////////////////////////////////////////////////
	/// MathInstruction
	////////////////////////////////////////////////////////////////////////////
	class VirtualMachine_API MathInstruction : public Instruction {
	private:
		InstructionPtrVec 		m_instructions;			/// The operand list
		OperatorPtrVec 			m_opStack;				/// For converting from infix to postfix
		std::string				m_name;
		int						m_sp = -1;
		int						m_spCount = 0;

		int 					lastOp() const;
		OperatorPtr				popOp();
		void 					pushOp(OperatorPtr op);

	public:
		virtual void 			exec(IMachine& vm);
		virtual void 			push(MathInstruction* inst);
		virtual void 			push(FunctionCallPtr inst);
		virtual void 			push(DataPtr oper);
		virtual void 			push(OperatorPtr op);
		void 					end();

		inline std::string&		name() { return m_name; }
		inline int&				sp() { return m_sp; }
		inline int				sp() const { return m_sp; }
	};

} /// namespace vm
} /// namespace pesa

