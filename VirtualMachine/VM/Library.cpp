/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Library.cpp
///
/// Created on: 28 Jun 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Library.h"
#include "NativeLibrary.h"
#include "AlphaLibrary.h"
#include "IMachine.h"
#include "VirtualMachine/Lang/Parser.h"

namespace pesa {
namespace vm {
	////////////////////////////////////////////////////////////////////////////
	/// Library Method
	////////////////////////////////////////////////////////////////////////////
	Method::Method(const std::string& name, size_t minArgs) 
		: m_name(name)
		, m_minArgs(minArgs) {
	}

	Method::~Method() {}

	//float Method::floatVar(IMachine& vm) {
	//	return (dynamic_cast<FloatConstant*>(vm.pop().get()))->value();
	//}

	//int Method::intVar(IMachine& vm) {
	//	return (int)floatVar(vm);
	//}

	//DataVariable* Method::dataVar(IMachine& vm) {
	//	retrun (dynamic_cast<DataVariable*>(vm.pop().get()));
	//}

	//FloatMatrixData* Method::floatMatVar(IMachine& vm) {
	//	return static_cast<FloatMatrixData*>(dataVar(vm));
	//}

	FloatMatrixDataPtr Method::call(IntDay di) const {
		return nullptr;
	}

	void Method::flush(IntDay di) const {
		call(di);
	}

	Data* Method::call(IMachine& vm, size_t numArgs) {
		ASSERT(numArgs >= m_minArgs, "Invalid number of arguments. Function excpecting: " << m_minArgs << " but got: " << numArgs);
		//if (numArgs == m_minArgs + 3) {
		//	m_pinfo.delta 		= intVar(vm);
		//	m_pinfo.omitDays 	= intVar(vm);
		//	m_pinfo.days 		= intVar(vm);
		//}
		//else if (numArgs == m_minArgs + 2) {
		//	m_pinfo.omitDays 	= intVar(vm);
		//	m_pinfo.days 		= intVar(vm);
		//}
		//else if (numArgs == m_minArgs + 1) {
		//	m_pinfo.days 		= intVar(vm);
		//}

		return nullptr;
	}

	float Method::call(IAlpha* alpha, const RuntimeData& rt, IntDay di, size_t ii) const {
		return call(alpha, rt, di, ii);
	}

	size_t Method::rows() const {
		return 0;
	}

	size_t Method::cols() const {
		return 0;
	}

	float Method::call(IntDay di, size_t ii) {
		RuntimeData rt;
		return call(nullptr, rt, di, ii);
	}

	////////////////////////////////////////////////////////////////////////////
	/// Library
	////////////////////////////////////////////////////////////////////////////
	Library::Library(const std::string& name) : m_name(name){
	}

	Library::~Library() {}

	Library& Library::add(MethodPtr method) {
		ASSERT(m_methods.find(method->name()) == m_methods.end(), "Library " << m_name << ": Method already exists: " << method->name());
		m_methods[method->name()] = method;
		return *this;
	}

	MethodPtr Library::find(const std::string& name, const std::string&) {
		MethodMap::iterator iter = m_methods.find(name);

		if (iter == m_methods.end())
			return nullptr;

		return iter->second;
	}

	////////////////////////////////////////////////////////////////////////////
	/// LibraryManager
	////////////////////////////////////////////////////////////////////////////
	LibraryManager LibraryManager::s_instance;

	LibraryManager::LibraryManager() {
		initNative();
	}

	LibraryManager::~LibraryManager() {}

	void LibraryManager::initNative() {
		initMath();
	}

	void LibraryManager::initMath() {
		add(std::make_shared<Library>("math"))
			.add(std::make_shared<NativeMethod1>("abs", native::abs))
			.add(std::make_shared<NativeMethod1>("exp", native::exp))
			.add(std::make_shared<NativeMethod1>("sign", native::sign))
			;

		m_defaultLib = std::make_shared<AlphaLibrary>("Alpha");
		add(m_defaultLib);

		auto pyLib = std::make_shared<PyAlphaLibrary>("PyImpl");
		add(pyLib);

		/// Allow the users to access Python through either PyImpl.<AlphaName> or simply Py.<AlphaName>
		auto pyLib2 = std::make_shared<PyAlphaLibrary>("Py");
		add(pyLib2);
	}

	LibraryManager& LibraryManager::instance() {
		return s_instance;
	}

	Library& LibraryManager::add(LibraryPtr lib) {
		ASSERT(m_libs.find(lib->name()) == m_libs.end(), "Library " << lib->name() << " already registered!");
		m_libs[lib->name()] = lib;
		return *lib;
	}

	LibraryPtr LibraryManager::find(const std::string& name) {
		LibraryMap::iterator iter = m_libs.find(name);

		if (iter == m_libs.end())
			return m_defaultLib;

		return iter->second;
	}
}
} /// namespace pesa
