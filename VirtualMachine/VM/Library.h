/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Library.h
///
/// Created on: 28 Jun 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#ifndef VirtualMachine_Library_h_
#define VirtualMachine_Library_h_

#include "Framework/Framework.h"
#include "VirtualMachine/VirtualMachineDefs.h"
#include <memory>
#include <unordered_map>

namespace pesa {
namespace vm {
	class IMachine;

	////////////////////////////////////////////////////////////////////////////
	/// Library Method
	////////////////////////////////////////////////////////////////////////////
	class VirtualMachine_API Method {
	protected:
		std::string 			m_name;			/// The name of the method
		size_t 					m_minArgs; 		/// The minimum number of arguments that should be passed!

		AlphaProcessInfo 		m_pinfo; 		/// The processing information 

		//float					floatVar(IMachine& vm);
		//int 					intVar(IMachine& vm);
		//DataData* 				dataVar(IMachine& vm);
		//FloatMatrixData* 		floatMatVar(IMachine& vm);

	public:
								Method(const std::string& name, size_t minArgs);
		virtual 				~Method();

		virtual Data*			call(IMachine& vm, size_t numArgs);

		virtual float 			call(IAlpha* alpha, const RuntimeData& rt, IntDay di, size_t ii) const;
		virtual size_t 			rows() const;
		virtual size_t 			cols() const;
		virtual void			flush(IntDay di) const;

		float					call(IntDay di, size_t ii);
		virtual FloatMatrixDataPtr	call(IntDay di) const;
		inline const std::string& name() const { return m_name; }
		inline std::string 		toString() const { return m_name; }
	};

	typedef std::shared_ptr<Method> MethodPtr;

	////////////////////////////////////////////////////////////////////////////
	/// Library
	////////////////////////////////////////////////////////////////////////////
	class VirtualMachine_API Library {
	protected:
		typedef std::unordered_map<std::string, MethodPtr> MethodMap;
		std::string 			m_name; 		/// The name of the library
		MethodMap				m_methods; 		/// The methods that exist within this library

	public:
								Library(const std::string& name);
		virtual 				~Library();

		virtual Library&		add(MethodPtr method);
		virtual MethodPtr 		find(const std::string& name, const std::string& libName);

		inline const std::string& name() const { return m_name; }
		inline std::string 		toString() const { return m_name; }
	};

	typedef std::shared_ptr<Library> LibraryPtr;

	////////////////////////////////////////////////////////////////////////////
	/// LibraryManager
	////////////////////////////////////////////////////////////////////////////
	class VirtualMachine_API LibraryManager {
	private:
		typedef std::unordered_map<std::string, LibraryPtr> LibraryMap;
		LibraryMap 				m_libs; 		/// The libraries that exist within the system
		LibraryPtr				m_defaultLib;	/// What is the default library ...

		static LibraryManager 	s_instance;

								LibraryManager();
		virtual 				~LibraryManager();

		void 					initMath();
		void 					initNative();

	public:
		static LibraryManager& 	instance();

		virtual Library&		add(LibraryPtr lib);
		virtual LibraryPtr 		find(const std::string& name);
	};

} /// namespace vm
} /// namespace pesa

#endif /// VirtualMachine_Library_h_
