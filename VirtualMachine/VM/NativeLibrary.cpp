/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// NativeLibrary.cpp
///
/// Created on: 28 Jun 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "NativeLibrary.h"
#include "IMachine.h"
#include "VirtualMachine/Lang/Parser.h"
#include "Variable.h"
#include "Core/Math/Stats.h"

namespace pesa {
namespace vm {

	////////////////////////////////////////////////////////////////////////////
	/// Native functions
	////////////////////////////////////////////////////////////////////////////
	namespace native {
		#define DI_DATA(p)			FloatVec data; \
									for (IntDay jj = pinfo.omitDays; jj < pinfo.omitDays + pinfo.days; jj += pinfo.delta) \
										data.push_back((*p)(di - jj, ii));

		float abs(const FloatMatrixData* p0, IAlpha* alpha, const RuntimeData& rt, IntDay di, size_t ii, const AlphaProcessInfo& pinfo) {
			return std::abs((*p0)(di, ii));
		}

		float mean(const FloatMatrixData* p0, IAlpha* alpha, const RuntimeData& rt, IntDay di, size_t ii, const AlphaProcessInfo& pinfo) {
			DI_DATA(p0);
			return Stats::mean(&data[0], data.size());
		}

		float stddev(const FloatMatrixData* p0, IAlpha* alpha, const RuntimeData& rt, IntDay di, size_t ii, const AlphaProcessInfo& pinfo) {
			DI_DATA(p0);
			return Stats::standardDeviation(&data[0], data.size());
		}

		float exp(const FloatMatrixData* p0, IAlpha* alpha, const RuntimeData& rt, IntDay di, size_t ii, const AlphaProcessInfo& pinfo) {
			float value = (*p0)(di, ii);
			return std::exp(value);
		}

		float sign(const FloatMatrixData* p0, IAlpha* alpha, const RuntimeData& rt, IntDay di, size_t ii, const AlphaProcessInfo& pinfo) {
			float value = (*p0)(di, ii);
			
			if (value > 0.0f)
				return 1.0f;
			else if (value < 0.0f)
				return -1.0f;

			return 0.0f;  /// Will hit for NaNs too.
		}
	}

	////////////////////////////////////////////////////////////////////////////
	/// NativeMethod1 - With 1 argument
	////////////////////////////////////////////////////////////////////////////
	NativeMethod1::NativeMethod1(const std::string& name, Func1 func) 
		: Method(name, 1)
		, m_func(func) {
	}

	Data* NativeMethod1::call(IMachine& vm, size_t numArgs) {
		Method::call(vm, numArgs);
		m_arg0 = std::dynamic_pointer_cast<FloatMatrixData>(vm.pop());
		return nullptr;
	}

	float NativeMethod1::call(IAlpha* alpha, const RuntimeData& rt, IntDay di, size_t ii) const {
		// return (*m_arg0)(di, ii);
		return m_func(m_arg0.get(), alpha, rt, di, ii, m_pinfo);
	}

	size_t NativeMethod1::rows() const { return m_arg0->rows();	}
	size_t NativeMethod1::cols() const { return m_arg0->cols();	}

	////////////////////////////////////////////////////////////////////////////
	/// NativeMethod2 - With 2 arguments
	////////////////////////////////////////////////////////////////////////////
	NativeMethod2::NativeMethod2(const std::string& name, Func2 func) 
		: Method(name, 2)
		, m_func(func) {
	}

	Data* NativeMethod2::call(IMachine& vm, size_t numArgs) {
		Method::call(vm, numArgs);

		/// arguments are popped in reverse order
		m_arg1 = std::dynamic_pointer_cast<FloatMatrixData>(vm.pop());
		m_arg0 = std::dynamic_pointer_cast<FloatMatrixData>(vm.pop());

		ASSERT(m_arg0->rows() == m_arg1->rows(), "Num rows arg0 = " << m_arg0->rows() << " vs arg1 = " << m_arg1->rows());
		ASSERT(m_arg0->cols() == m_arg1->cols(), "Num cols arg0 = " << m_arg0->cols() << " vs arg1 = " << m_arg1->cols());

		return nullptr;
	}

	float NativeMethod2::call(IAlpha* alpha, const RuntimeData& rt, IntDay di, size_t ii) const {
		// return (*m_arg0)(di, ii);
		return m_func(m_arg0.get(), m_arg1.get(), alpha, rt, di, ii, m_pinfo);
	}

	size_t NativeMethod2::rows() const { return m_arg0->rows();	}
	size_t NativeMethod2::cols() const { return m_arg0->cols();	}

}
} /// namespace pesa
