/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// NativeLibrary.h
///
/// Created on: 28 Jun 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#ifndef VirtualMachine_NativeNativeLibrary_h_
#define VirtualMachine_NativeNativeLibrary_h_

#include "Framework/Framework.h"
#include "VirtualMachine/VirtualMachineDefs.h"
#include <memory>
#include <unordered_map>
#include "Library.h"

namespace pesa {
	class FloatMatrixData;

namespace vm {
	typedef 					std::function<float(const FloatMatrixData* p0, IAlpha* alpha, const RuntimeData& rt, IntDay di, size_t ii, const AlphaProcessInfo& pinfo)> Func1;
	typedef 					std::function<float(const FloatMatrixData* p0, FloatMatrixData* p1, IAlpha* alpha, const RuntimeData& rt, IntDay di, size_t ii, const AlphaProcessInfo& pinfo)> Func2;
	// typedef float 				Func1(FloatMatrixData* p0, IntDay di, size_t ii, const AlphaProcessInfo& pinfo);
	// typedef float				Func2(FloatMatrixData* p0, FloatMatrixData* p1, IntDay di, size_t ii, const AlphaProcessInfo& pinfo);
	////////////////////////////////////////////////////////////////////////////
	/// Native functions
	////////////////////////////////////////////////////////////////////////////
	namespace native {
		extern float 			abs(const FloatMatrixData* p0, IAlpha* alpha, const RuntimeData& rt, IntDay di, size_t ii, const AlphaProcessInfo& pinfo);
		extern float 			mean(const FloatMatrixData* p0, IAlpha* alpha, const RuntimeData& rt, IntDay di, size_t ii, const AlphaProcessInfo& pinfo);
		extern float 			stddev(const FloatMatrixData* p0, IAlpha* alpha, const RuntimeData& rt, IntDay di, size_t ii, const AlphaProcessInfo& pinfo);

		extern float 			exp(const FloatMatrixData* p0, IAlpha* alpha, const RuntimeData& rt, IntDay di, size_t ii, const AlphaProcessInfo& pinfo);
		extern float 			sign(const FloatMatrixData* p0, IAlpha* alpha, const RuntimeData& rt, IntDay di, size_t ii, const AlphaProcessInfo& pinfo);
	}

	////////////////////////////////////////////////////////////////////////////
	/// NativeMethod1 - With 1 argument
	////////////////////////////////////////////////////////////////////////////

	class VirtualMachine_API NativeMethod1 : public Method {
	private:
		Func1 					m_func = nullptr;
		FloatMatrixDataPtr		m_arg0; 			/// The argument

	public:
								NativeMethod1(const std::string& name, Func1 func);
		virtual Data*			call(IMachine& vm, size_t numArgs);
		
		virtual float 			call(IAlpha* alpha, const RuntimeData& rt, IntDay di, size_t ii) const;
		virtual size_t 			rows() const;
		virtual size_t 			cols() const;
	};

	////////////////////////////////////////////////////////////////////////////
	/// NativeMethod2 - With 2 arguments
	////////////////////////////////////////////////////////////////////////////

	class VirtualMachine_API NativeMethod2 : public Method {
	private:
		Func2 					m_func = nullptr;
		FloatMatrixDataPtr		m_arg0; 			/// arg0
		FloatMatrixDataPtr		m_arg1; 			/// arg1

	public:
								NativeMethod2(const std::string& name, Func2 func);
		virtual Data*			call(IMachine& vm, size_t numArgs);
		
		virtual float 			call(IAlpha* alpha, const RuntimeData& rt, IntDay di, size_t ii) const;
		virtual size_t 			rows() const;
		virtual size_t 			cols() const;
	};

} /// namespace vm
} /// namespace pesa

#endif /// VirtualMachine_NativeNativeLibrary_h_
