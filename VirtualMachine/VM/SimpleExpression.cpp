/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// SimpleExpression.cpp
///
/// Created on: 27 Jun 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "SimpleExpression.h"
#include "VirtualMachine/Lang/Parser.h"
#include "VirtualMachine/VM/Library.h"

#include "Core/Lib/Profiler.h"

namespace pesa {
namespace vm {

	void EvaluatedExpression::operator = (const SimpleExpression& rhs) {
		instructions = rhs.m_instructions;
		varStack = rhs.m_varStack;
		expr = rhs.m_expr;
	}

	SimpleExpression::TreePtrStrMap SimpleExpression::s_exprCache;
	std::recursive_mutex SimpleExpression::s_exprMutex;
	SimpleExpression::EvaluatedExpressionMap SimpleExpression::s_exprEval;

	SimpleExpression::SimpleExpression() {
	}

	SimpleExpression::~SimpleExpression() {
	}

	ast::TreePtr SimpleExpression::parse(const std::string& expr) {
		PROFILE_SCOPED();

		AUTO_REC_LOCK(s_exprMutex);

		TreePtrStrMap::iterator iter = s_exprCache.find(expr);

		if (iter != s_exprCache.end())
			return iter->second;

		Parser p;
		ast::TreePtr tree = p.parseString(expr);
		s_exprCache[expr] = tree;

		return tree;
	}

	void SimpleExpression::operator = (const EvaluatedExpression& rhs) {
		m_instructions = rhs.instructions;
		m_varStack = rhs.varStack;
		m_expr = rhs.expr;
	}

	void SimpleExpression::parseString(const std::string& expr) {
		PROFILE_SCOPED();

		AUTO_REC_LOCK(s_exprMutex);

		if (!m_instructions.size()) {
			//auto iter = s_exprEval.find(expr);

			//if (iter != s_exprEval.end()) {
			//	*this = iter->second;
			//}
			//else {
			PROFILE_SCOPED();

			ast::TreePtr tree = SimpleExpression::parse(expr);
			tree->eval(this);
				//EvaluatedExpression eexpr;
				//eexpr = *this;
				//s_exprEval[expr] = eexpr;
			//}
		}
	}

	void SimpleExpression::exec() {
		PROFILE_SCOPED();

		m_orgSize = m_varStack.size();

		for (InstructionPtr inst : m_instructions)
			inst->exec(*this);

		// ASSERT(m_varStack.size() == 0, "Fatal Error: Stack NOT EMPTY: " << m_varStack.size());
	}

	////////////////////////////////////////////////////////////////////////////
	/// ICodeGenerator overrides
	////////////////////////////////////////////////////////////////////////////
	void SimpleExpression::begin(ast::MathExpression* expr) {
		//ASSERT(!m_mathExpr, "Runtime Error: Already parsing a mathematical expression. How can we have a nested one!");
		if (m_mathExpr)
			m_mexprStack.push_back(m_mathExpr);

		m_mathExpr = new MathInstruction();
		m_mathExpr->name() = expr->name();
		m_mathExpr->sp() = (int)size() - 1;
	}

	void SimpleExpression::end(ast::MathExpression* expr) {
		ASSERT(m_mathExpr, "Runtime Error: Not parsing any math expression!");
		m_mathExpr->end();

		if (m_mexprStack.size()) {
			MathInstruction* prev = m_mathExpr;
			m_mathExpr = m_mexprStack[m_mexprStack.size() - 1];
			m_mexprStack.pop_back();
			m_mathExpr->push(prev);
		}
		else {
			m_instructions.push_back(std::shared_ptr<MathInstruction>(m_mathExpr));
			m_mathExpr = nullptr;
		}
	}

	void SimpleExpression::eval(ast::BinaryOperator* expr) {
		ASSERT(m_mathExpr, "Runtime Error: Cannot have an operator outside a math expression!");

		int opType = expr->opType();
		const std::string& op = expr->op();

		switch (opType) {
		case P_ADD:
		case P_SUB:
		case P_MUL:
		case P_DIV:
		case P_MOD:
			m_mathExpr->push(std::make_shared<BinaryOperator>(op, opType));
			return;
		}

		m_mathExpr->push(std::make_shared<Operator>(op, opType));
	}

	void SimpleExpression::eval(ast::FloatConstant* expr) {
		ConstantFloatPtr var = std::make_shared<ConstantFloat>(expr->value(), DataDefinition());

		if (m_mathExpr)
			m_mathExpr->push(var);
		else
			push(var);
	}

	void SimpleExpression::eval(ast::Name* expr) {
		const StringVec& name = expr->parts();
		DataPtr var = nullptr;

		/// If its not a runtime variable
		if (name[0] == "CFG") {
			const ConfigSection& config = getConfig();
			//std::string varName = config.get(name[1]);

			//if (!varName.empty()) {
			//	ASSERT(!varName.empty(), "Undefined variable named: " << name[1] << ". Must be defined as part of the alpha config section");
			//	Trace("EXPR_VM", "Mapped symbol - %s = %s", name[1], varName);
			//	var = dataRegistry()->getDataPtr(universe(), varName);
			//}
			//else 
			{
				size_t index = atoi(name[1].c_str());
				var = getVar(index);
			}
		}
		else if (name[0] == "RT") {
			ASSERT(name.size() == 2, "Invalid variable: " << expr->toString());
			var = dataRegistry()->setData(expr->toString(), new LateBoundData(*this, name[1]));
		}
		else {
			DataInfo dinfo;
			//dinfo.dataWindow = true;
			var = dataRegistry()->getDataPtr(universe(), expr->toString(), &dinfo);
		}

        ASSERT(var, "Unable to load data: " << expr->toString());
		var->setName(expr->toString());

		if (m_mathExpr)
			m_mathExpr->push(var);
		else
			push(var);
	}

	void SimpleExpression::eval(ast::FunctionCall* expr) {
		StringVec& parts = expr->name()->parts();

		/// If no library ID is given then just assume AlphaLib
		if (parts.size() == 1)
			parts.insert(parts.begin(), 1, "AlphaLib");

		ASSERT(parts.size() > 1, "Invalid name for function: " << expr->name()->toString());

		ast::ExpressionPtrVec& args = expr->arguments();

		//for (ast::ExpressionPtr arg : args) {
		//	/// Other expressions automatically push the result on the top of the
		//	/// stack
		//	ast::Type type = arg->type();
		//	InstructionPtr inst;

		//	if (type == ast::kFloatConstant) 
		//		eval(dynamic_cast<ast::FloatConstant*>(arg.get()));
		//	else if (type == ast::kName) 
		//		eval(dynamic_cast<ast::Name*>(arg.get()));
		//	/// else do nothing since the results should've been pushed
		//}

		FunctionCallPtr fcall = std::make_shared<FunctionCall>(parts, args.size());

		/// Set the stack pointer for the function call (if we have more than one)
		fcall->sp() = (int)size() - 1;

		if (m_mathExpr)
			m_mathExpr->push(fcall);
		else
			m_instructions.push_back(fcall);
	}
	
	////////////////////////////////////////////////////////////////////////////
	/// IMachine overrides
	////////////////////////////////////////////////////////////////////////////
	void SimpleExpression::push(DataPtr var) {
		var->applyDelay() = this->applyDelay();

		if (m_sp < 0) {
			m_varStack.push_back(var);
			m_stackValid.push_back(1);
			return;
		}

		if (m_orgSPCount > 0) {
			m_varStack[m_sp + m_spCount + 1] = var;
			m_stackValid[m_sp + m_spCount + 1] = 1;
			m_spCount++;
		}
		else {
			m_varStack.insert(m_varStack.begin() + m_sp + m_spCount + 1, 1, var);
			m_stackValid.insert(m_stackValid.begin() + m_sp + m_spCount + 1, 1, 1);
			m_spCount++;
		}
	}

	DataPtr SimpleExpression::pop() {
		DataPtr var = nullptr;
		bool isValid = 0;

		if (m_sp < 0) {
			while (!isValid) {
				ASSERT(m_varStack.size(), "Variable stack underflow: " << m_expr);
				var = m_varStack[m_varStack.size() - 1];
				isValid = !!(m_stackValid[m_varStack.size() - 1]); /// Convert to boolean
				m_varStack.pop_back();
				m_stackValid.pop_back();
			}

			return var;
		}

		ASSERT(m_spCount > 0, "Variable stack underflow. SP: " << m_sp << " [SPC: " << m_spCount << "]");
		int index = m_sp + m_spCount;
		var = m_varStack[index];
		m_stackValid[index] = 0;
		m_spCount--;

		return var;
	}

	void SimpleExpression::setStackPtr(int sp, int spCount) {
		ASSERT(m_sp < 0 || sp + spCount <= (int)m_varStack.size(), "setStackPtr invalid SP: " << sp << " [SPC: " << spCount << "]");
		m_sp = sp;
		m_spCount = spCount;
		m_orgSPCount = spCount;
	}

	void SimpleExpression::getStackPtr(int* sp, int* spCount) {
		*sp = m_sp;
		*spCount = m_spCount;
	}

	DataPtr SimpleExpression::peek() {
		if (!m_varStack.size())
			return nullptr;

		if (m_sp < 0) {
			DataPtr var = m_varStack[m_varStack.size() - 1];
			return var;
		}

		ASSERT(m_sp < (int)m_varStack.size(), "Invalid SP: " << m_sp);
		DataPtr var = m_varStack[m_sp - 1];
		return var;
	}

	size_t SimpleExpression::size() {
		return m_varStack.size();
	}

	size_t SimpleExpression::orgSize() {
		return m_orgSize;
	}
}
} /// namespace pesa
