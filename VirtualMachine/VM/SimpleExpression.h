/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// SimpleExpression.h
///
/// Created on: 27 Jun 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#ifndef VirtualMachine_SimpleExpression_h_
#define VirtualMachine_SimpleExpression_h_

#include "Framework/Framework.h"
#include "VirtualMachine/VirtualMachineDefs.h"
#include "VirtualMachine/Lang/ICodeGenerator.h"
#include "Instruction.h"
#include "Variable.h"
#include "IMachine.h"
#include "VirtualMachine/Lang/AST.h"

#include <unordered_map>

namespace pesa {
namespace vm {

	class SimpleExpression;
	struct EvaluatedExpression {
		InstructionPtrVec 		instructions;			/// The instructions that we execute sequentially 
		DataPtrVec 				varStack; 				/// The variable stack
		std::string 			expr;					/// The actual expression

		void					operator = (const SimpleExpression& rhs);
	};

	class VirtualMachine_API SimpleExpression : public ICodeGenerator, public IMachine {
	protected:
		friend struct EvaluatedExpression;
		typedef std::vector<MathInstruction*> MathInstructionPVec;
		typedef std::unordered_map<std::string, ast::TreePtr> TreePtrStrMap;
		typedef std::unordered_map<std::string, EvaluatedExpression> EvaluatedExpressionMap;

		static std::recursive_mutex s_exprMutex;
		static TreePtrStrMap	s_exprCache;
		static ast::TreePtr		parse(const std::string& expr);
		static EvaluatedExpressionMap s_exprEval;

		InstructionPtrVec 		m_instructions;			/// The instructions that we execute sequentially 
		DataPtrVec 				m_varStack; 			/// The variable stack
		IntVec					m_stackValid;			/// What part(s) of the stack are actually valid (this is used in modified SP)
		std::string 			m_expr;					/// The actual expression
		MathInstruction* 		m_mathExpr = nullptr; 	/// Currently parsed math expression
		MathInstructionPVec		m_mexprStack;			/// To handle nested math expressions

		size_t					m_orgSize = 0;			/// What was the original size of the stack

		int						m_sp = -1;				/// The stack pointer
		int						m_spCount = -1;			/// The count which indicates the end of the stack
		int						m_orgSPCount = -1;		/// What was the original SP Count

		virtual DataPtr			getVar(size_t index) = 0;

	public:
								SimpleExpression();
		virtual 				~SimpleExpression();

		void					operator = (const EvaluatedExpression& rhs);

		void 					parseString(const std::string& expr);
		virtual void 			exec();
		
		////////////////////////////////////////////////////////////////////////////
		/// ICodeGenerator overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			begin(ast::MathExpression* expr);
		virtual void 			end(ast::MathExpression* expr);
	

		virtual void 			eval(ast::BinaryOperator* expr);
		virtual void 			eval(ast::FloatConstant* expr);
		virtual void 			eval(ast::Name* expr);
		virtual void 			eval(ast::FunctionCall* expr);

		////////////////////////////////////////////////////////////////////////////
		/// IMachine overrides
		////////////////////////////////////////////////////////////////////////////
		virtual void 			push(DataPtr var);
		virtual DataPtr 		pop();
		virtual DataPtr 		peek();
		virtual size_t			size();
		virtual size_t			orgSize();
		virtual void			setStackPtr(int sp, int spCount);
		virtual void			getStackPtr(int* sp, int* spCount);
	};
} /// namespace vm
} /// namespace pesa

#endif /// VirtualMachine_SimpleExpression_h_
