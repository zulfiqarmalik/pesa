/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Variable.cpp
///
/// Created on: 23 Jun 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#include "Variable.h"
#include "Instruction.h"
#include "IMachine.h"
#include <algorithm>
#include "VirtualMachine/Lang/Lexer.h"
#include "Core/Lib/Profiler.h"

namespace pesa {
namespace vm {
	
	////////////////////////////////////////////////////////////////////////////
	/// FunctionCallData
	////////////////////////////////////////////////////////////////////////////
	FunctionCallData::FunctionCallData(IUniverse* universe, MethodPtr method)
		: FloatMatrixData(universe, DataDefinition::s_float, 0, 0, false)
		, m_method(method) {
		m_def.setName(method->name());
	}

	float FunctionCallData::operator() (IntDay di, size_t ii) const {
		return m_method->call(di, ii);
	}

	IntDay FunctionCallData::DI(IntDay di) const {
		(const_cast<FunctionCallData*>(this))->m_delay = 0;
		return Data::DI(di);
	}

	void FunctionCallData::flush(IntDay di) const {
		PROFILE_SCOPED();

		auto iter = m_results.find(di);
		if (iter != m_results.end())
			return;

		if (!this->isAllocated()) {
			PROFILE_SCOPED();
			(const_cast<FunctionCallData*>(this))->ensureSize(m_method->rows(), m_method->cols());
		}

		/// We need to set this to avoid a recursive call
		(const_cast<FunctionCallData*>(this))->m_results[di] = true;

		auto result = m_method->call(di);
		ASSERT(result, "Unable to get result from method: " << m_method->name() << " on di = " << di);
		FloatMatrixData& rmat = *(const_cast<FunctionCallData*>(this));
		rmat[di] = (*result)[0];
	}

	float& FunctionCallData::operator() (IntDay di, size_t ii) {
		throw Poco::Exception("FunctionCallData - Fatal Error: Attempting to write a read-only variable!");
		return FloatMatrixData::operator()(di, ii);
	}

	//float FunctionCallData::operator() (pesa::IAlpha* alpha, const RuntimeData& rt, IntDay di, size_t ii) const {
	//	return m_method->call(alpha, rt, di, ii);
	//}

	size_t FunctionCallData::rowsForTranspose() const {
		return std::max(m_method->rows(), m_data.size() && m_data[0] ? m_data[0]->rows() : 0);
	}

	size_t FunctionCallData::colsForTranspose() const {
		return std::max(m_method->cols(), m_data.size() && m_data[0] ? m_data[0]->cols() : 0);
	}

	//////////////////////////////////////////////////////////////////////////
	/// LateBoundData 
	//////////////////////////////////////////////////////////////////////////
	LateBoundData::LateBoundData(IMachine& vm, const std::string& fieldId)
		: FloatMatrixData(vm.universe(), DataDefinition(fieldId.c_str()), 0, 0, false)
		, m_vm(vm)
		, m_fieldId(fieldId) {
	}

	size_t LateBoundData::rows() const {
		return 1;
	}

	size_t LateBoundData::cols() const {
		return 1;
	}

	float LateBoundData::operator() (IntDay di, size_t ii) const {
		PROFILE_SCOPED();

		//ASSERT(false, "Cannot used this function to access data on a LateBound variable. Use var(rt, alpha, di, ii)");
		IAlpha* alpha = m_vm.alpha();
		const RuntimeData& rt = m_vm.rt();

		if (m_fieldId == "ii")
			return (float)ii;
		else if (m_fieldId == "di")
			return (float)di;
		else if (m_fieldId == "jj")
			return (float)(rt.di - di - alpha->delay());
		else if (m_fieldId == "orgDi")
			return (float)rt.di;
		else if (m_fieldId == "delay")
			return (float)alpha->pinfo().delay;
		else if (m_fieldId == "days")
			return (float)alpha->pinfo().days;
		else if (m_fieldId == "omitDays")
			return (float)alpha->pinfo().omitDays;
		else if (m_fieldId == "delta")
			return (float)alpha->pinfo().delta;
		else if (m_fieldId == "weight")
			return (float)alpha->pinfo().weight;
		else if (m_fieldId == "reversion")
			return alpha->reversion();
		else
			return alpha->getNamedVar(m_fieldId)(di, ii);
	}

	float& LateBoundData::operator() (IntDay di, size_t ii) {
		ASSERT(false, "Cannot use non-constant function to access LateBound variable. Make sure you use a 'const auto*' and then access the dataset!");
		throw Poco::Exception("Cannot use non-constant function to access LateBound variable. Make sure you use a 'const auto*' and then access the dataset!");
	}

	//float LateBoundData::operator() (IAlpha* alpha, const RuntimeData& rt_, IntDay di, size_t ii) const {
	//	if (!alpha)
	//		alpha = m_vm.alpha();

	//	const RuntimeData& rt = m_vm.rt();

	//	if (m_fieldId == "ii")
	//		return (float)ii;
	//	else if (m_fieldId == "di")
	//		return (float)di;
	//	else if (m_fieldId == "jj")
	//		return (float)(rt.di - di - alpha->delay());
	//	else if (m_fieldId == "orgDi")
	//		return (float)rt.di;
	//	else if (m_fieldId == "delay")
	//		return (float)alpha->pinfo().delay;
	//	else if (m_fieldId == "days")
	//		return (float)alpha->pinfo().days;
	//	else if (m_fieldId == "omitDays")
	//		return (float)alpha->pinfo().omitDays;
	//	else if (m_fieldId == "delta")
	//		return (float)alpha->pinfo().delta;
	//	else if (m_fieldId == "weight")
	//		return (float)alpha->pinfo().weight;
	//	else if (m_fieldId == "reversion")
	//		return alpha->reversion();
	//	else
	//		return alpha->getNamedVar(m_fieldId)(di, ii);
	////}

	//////////////////////////////////////////////////////////////////////////
	/// BinOpData - Binary operator
	//////////////////////////////////////////////////////////////////////////
	BinOpData::BinOpData(IUniverse* universe, mathOpFunc* op, int opType, DataPtr lhs, DataPtr rhs)
		: FloatMatrixData(universe, DataDefinition::s_float, 0, 0, false)
		, m_operator(op)
		, m_opType(opType)
		, m_lhs(std::dynamic_pointer_cast<FloatMatrixData>(lhs)) 
		, m_rhs(std::dynamic_pointer_cast<FloatMatrixData>(rhs)) {
	}

	void BinOpData::flush(IntDay di) const {
		PROFILE_SCOPED();

		IntDay diDst = DI(di);
		auto iter = m_results.find(diDst);
		if (iter != m_results.end())
			return;

		/// We need to set this to avoid a recursive call
		(const_cast<BinOpData*>(this))->m_results[diDst] = true;

		/// Otherwise we calculate it
		size_t cols = std::max(m_lhs->cols(), m_rhs->cols());
		//FloatMatrixDataPtr result = std::make_shared<FloatMatrixData>(universe(), def(), 1, cols);
		const FloatMatrixData& lhsMat = *m_lhs;
		const FloatMatrixData& rhsMat = *m_rhs;

		//if (!this->isAllocated())
		//	(const_cast<BinOpData*>(this))->ensureSize(m_lhs->rows(), m_lhs->cols());

		IntDay diLhs = di;
		IntDay diRhs = di;

		/// Dealing with ConstantFloat data
		if (lhsMat.rows() == 1) 
			diLhs = 0;
		if (rhsMat.rows() == 1)
			diRhs = 0;

		auto myCols = this->cols();
		auto myRows = this->rows();

		/// Flush the matrices
		lhsMat.flush(diLhs);
		rhsMat.flush(diRhs);

		(const_cast<FloatMatrixData&>(lhsMat)).ensureCols(diLhs, std::max(rhsMat.cols(), myCols));
		(const_cast<FloatMatrixData&>(rhsMat)).ensureCols(diRhs, std::max(lhsMat.cols(), myCols));
		//(const_cast<BinOpData*>(this))->ensureCols(di, std::max(lhsMat.cols(), rhsMat.cols()));

		auto maxRows = std::max(std::max((IntDay)m_lhs->rows(), (IntDay)m_rhs->rows()), diDst + 1);
		auto maxCols = std::max(m_lhs->cols(), m_rhs->cols());
		(const_cast<BinOpData*>(this))->ensureSize(maxRows, maxCols, true);

		/// If either one of these is a constant then we need to do something different
		FloatMatrixData& rmat = *(const_cast<BinOpData*>(this));

		bool doApplyDelay = this->applyDelay();

		(const_cast<BinOpData*>(this))->m_applyDelay = true;
		(const_cast<BinOpData*>(this))->m_delay = std::max(lhsMat.delay(), rhsMat.delay());

		const_cast<FloatMatrixData&>(lhsMat).applyDelay() = doApplyDelay;
		const_cast<FloatMatrixData&>(rhsMat).applyDelay() = doApplyDelay;

		switch ((PuncId)m_opType) {
		case P_MUL:
			rmat[diDst] = (lhsMat[diLhs] * rhsMat[diRhs]).eval();
			break;
		case P_DIV:
			rmat[diDst] = (lhsMat[diLhs] / rhsMat[diRhs]).eval();
			break;
		case P_ADD:
			rmat[diDst] = (lhsMat[diLhs] + rhsMat[diRhs]).eval();
			break;
		case P_SUB:
			rmat[diDst] = (lhsMat[diLhs] - rhsMat[diRhs]).eval();
			break;
		default:
			ASSERT(false, "Unsupported operation: " << m_opType);
		}

		const_cast<FloatMatrixData&>(lhsMat).applyDelay() = false;
		const_cast<FloatMatrixData&>(rhsMat).applyDelay() = false;

		//this->innerData() = result->innerData();
	}

	//void BinOpData::ensureCols(size_t di, size_t numCols) {
	//	if (!this->isAllocated())
	//		(const_cast<BinOpData*>(this))->ensureSize(m_lhs->rows(), m_lhs->cols());

	//	auto* data = MatrixData<float>::m_data[0].get();
	//	if (!data || data->cols() >= numCols) {
	//		FloatMatrixData::ensureCols(di, numCols);
	//		return;
	//	}

	//	/// Otherwise we allocated
	//	//float value = (*this)(DI(di), 0);

	//	ensureSizeAt(0, this->rows(), numCols, false);
	//	//std::fill_n(this->rowPtr(di), (1 * numCols), value);
	//}

	IntDay BinOpData::DI(IntDay di) const {
		//const FloatMatrixData& lhsMat = *m_lhs;
		//const FloatMatrixData& rhsMat = *m_rhs;

		(const_cast<BinOpData*>(this))->m_delay = 0;

		return Data::DI(di);
	}

	float BinOpData::operator() (IntDay di, size_t ii) const {
		if (di < 0)
			di = 0;

		const FloatMatrixData& lhsMat = *m_lhs;
		const FloatMatrixData& rhsMat = *m_rhs; 

		//lhsMat.flush();
		//rhsMat.flush();

		float lhs = lhsMat(di, ii);
		float rhs = rhsMat(di, ii);

		return m_operator(lhs, rhs);
	}

	//float BinOpData::operator() (pesa::IAlpha* alpha, const RuntimeData& rt, IntDay di, size_t ii) const {
	//	const FloatMatrixData& lhsMat = *m_lhs;
	//	const FloatMatrixData& rhsMat = *m_rhs;

	//	float lhs = lhsMat(alpha, rt, di, ii);
	//	float rhs = rhsMat(alpha, rt, di, ii);

	//	return m_operator(lhs, rhs);
	//}

	float& BinOpData::operator() (IntDay di, size_t ii) {
		throw Poco::Exception("BinOpData - Fatal Error: Attempting to write a read-only variable!");
		return FloatMatrixData::operator()(di, ii);
	}

	size_t BinOpData::rowsForTranspose() const {
		return std::max(std::max(m_lhs->rows(), m_data.size() && m_data[0] ? m_data[0]->rows() : 0), m_rhs->rows());
	}

	size_t BinOpData::colsForTranspose() const {
		return std::max(std::max(m_lhs->cols(), m_data.size() && m_data[0] ? m_data[0]->cols() : 0), m_rhs->cols());
	}
} /// namespace vm
} /// namespace pesa

