/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// DataVariable.h
///
/// Created on: 23 Jun 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#ifndef VirtualMachine_Variable_h_
#define VirtualMachine_Variable_h_

#include "Framework/Framework.h"
#include "VirtualMachine/VirtualMachineDefs.h"
#include "Framework/Data/MatrixData.h"

namespace pesa {
namespace vm {

	class Operator;
    class BinaryOperator;
	class IMachine;

	class Method;
	typedef std::shared_ptr<Method> MethodPtr;

	//////////////////////////////////////////////////////////////////////////
	/// FunctionCallData - Lazily evaluates a function call. Be it native or Alpha
	//////////////////////////////////////////////////////////////////////////
	class VirtualMachine_API FunctionCallData : public FloatMatrixData {
	protected:
		typedef std::map<IntDay, bool> BoolMap;
		MethodPtr 					m_method; 		/// The internal method 
		BoolMap						m_results;

	public:
									FunctionCallData(IUniverse* universe, MethodPtr method);

		////////////////////////////////////////////////////////////////////////////
		/// FloatMatrixData overrides
		////////////////////////////////////////////////////////////////////////////
		virtual float 		        operator() (IntDay di, size_t ii) const;
		virtual float& 				operator() (IntDay di, size_t ii);
		virtual float				operator() (IntDay di, size_t ii, size_t kk) const {
			ASSERT(kk == 0, "Unsupported kk: " << kk);
			return (*this)(di, ii);
		}
		virtual void				flush(IntDay di) const;
		virtual IntDay				DI(IntDay di) const;
		// virtual float				operator() (pesa::IAlpha* alpha, const RuntimeData& rt, IntDay di, size_t ii) const;

		virtual size_t 				rowsForTranspose() const;
		virtual size_t 				colsForTranspose() const;
		//virtual Eigen::Block<Eigen::Array<float, -1, -1, Eigen::RowMajor>, 1, -1, true > row(IntDay di, size_t kk = 0) { flush(di); return mat(kk).mat().row(0); }
		//virtual const Eigen::Block<const Eigen::Array<float, -1, -1, Eigen::RowMajor>, 1, -1, true >	row(IntDay di, size_t kk = 0) const { flush(di); return mat(kk).mat().row(0); }
	};

	typedef std::shared_ptr<FunctionCallData> FunctionCallDataPtr;

	//////////////////////////////////////////////////////////////////////////
	/// LateBoundData - Keeps historical data of the alpha
	//////////////////////////////////////////////////////////////////////////
	class VirtualMachine_API LateBoundData : public FloatMatrixData {
	protected:
		std::string					m_fieldId;
		IMachine&					m_vm;

	public:
									LateBoundData(IMachine& vm, const std::string& fieldId);

		////////////////////////////////////////////////////////////////////////////
		/// FloatMatrixData overrides
		////////////////////////////////////////////////////////////////////////////
		virtual float 		        operator() (IntDay di, size_t ii) const;
		virtual float& 				operator() (IntDay di, size_t ii);
		//virtual float 				operator() (IAlpha* alpha, const RuntimeData& rt, IntDay di, size_t ii) const;

		virtual size_t 				rows() const;
		virtual size_t 				cols() const;
	};

	typedef std::shared_ptr<LateBoundData> LateBoundDataPtr;

	//////////////////////////////////////////////////////////////////////////
	/// BinOpData - Binary operator
	//////////////////////////////////////////////////////////////////////////
	class VirtualMachine_API BinOpData : public FloatMatrixData {
	protected:
		typedef std::map<IntDay, bool> BoolMap;
		typedef float 				mathOpFunc(float lhs, float rhs);

		mathOpFunc*					m_operator = nullptr;
		int							m_opType;
		FloatMatrixDataPtr			m_lhs;
		FloatMatrixDataPtr			m_rhs;
		BoolMap						m_results;

	public:
									BinOpData(IUniverse* universe, mathOpFunc* op, int opType, DataPtr lhs, DataPtr rhs);

		////////////////////////////////////////////////////////////////////////////
		/// FloatMatrixData overrides
		////////////////////////////////////////////////////////////////////////////
		virtual float 		        operator() (IntDay di, size_t ii) const;
		virtual float& 				operator() (IntDay di, size_t ii);
		virtual float				operator() (IntDay di, size_t ii, size_t kk) const {
			ASSERT(kk == 0, "Unsupported kk: " << kk);
			return (*this)(di, ii);
		}
		virtual void				flush(IntDay di) const;
		virtual IntDay				DI(IntDay di) const;
		//void						ensureCols(size_t di, size_t numCols);
		// virtual float				operator() (pesa::IAlpha* alpha, const RuntimeData& rt, IntDay di, size_t ii) const;

		virtual size_t 				rowsForTranspose() const;
		virtual size_t 				colsForTranspose() const;
		//virtual Eigen::Block<Eigen::Array<float, -1, -1, Eigen::RowMajor>, 1, -1, true > row(IntDay di, size_t kk = 0) { flush(di); return mat(kk).mat().row(0); }
		//virtual const Eigen::Block<const Eigen::Array<float, -1, -1, Eigen::RowMajor>, 1, -1, true >	row(IntDay di, size_t kk = 0) const { flush(di); return mat(kk).mat().row(0); }
	};

	typedef std::shared_ptr<BinOpData> BinOpDataPtr;

} /// namespace vm
} /// namespace pesa

#endif /// VirtualMachine_Variable_h_
