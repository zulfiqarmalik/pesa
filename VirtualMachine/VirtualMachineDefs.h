/// *** BEGIN LICENSE ***
/// Copyright (C) QVTechnologies Inc. - All Rights Reserved
/// Unauthorized copying, usage, dissemination of information or reproduction of this material, 
/// via any medium, including but not limited to, compiled form is strictly prohibited without prior permission.
/// Proprietary and confidential.
/// *** END LICENSE ***

/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
/// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
/// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
/// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF 
/// OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////
/// Core.h
///
/// Created on: 7 Jun 2016
/// 	Author: zulfiqar
//////////////////////////////////////////////////////

#ifndef VM_VMDEFS_H_
#define VM_VMDEFS_H_

#ifdef __WINDOWS__
	#ifdef VirtualMachine_EXPORTS
		#define VirtualMachine_API __declspec(dllexport)
	#else
		#define VirtualMachine_API __declspec(dllimport)
	#endif
#else
	#define VirtualMachine_API
#endif


#endif /// VM_VMDEFS_H_ 
