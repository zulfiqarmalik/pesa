
Server IP: 5.101.137.130
Server OS: Ubuntu 16.04 LTS

PSIM Dependencies:
	1. Install gcc 5.5.x:
		- sudo add-apt-repository ppa:ubuntu-toolchain-r/test
		- sudo apt-get update
		- sudo apt-get install gcc-5 g++-5
		- sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-5 60 --slave /usr/bin/g++ g++ /usr/bin/g++-5
		
	2. Install anaconda
		- Download anaconda from the anaconda website (any version will do)
		- Run the .sh script to install it
		
	3. Create anaconda environment
		- conda create -n qvdev python=3.5.1

	
DATABASE:

	1. Edit /etc/mongod.conf with the contents of Pesa/mongod.conf
	2. Edit /lib/systemd/system/mongodb.service with the following contents:

	[Unit]
	Description=MongoDB Database Service
	Wants=network.target
	After=network.target

	[Service]
	ExecStart=/usr/bin/mongod --config /etc/mongod.conf
	ExecReload=/bin/kill -HUP $MAINPID
	Restart=always
	User=zmalik
	Group=qvadmins
	StandardOutput=syslog
	StandardError=syslog

	[Install]
	WantedBy=multi-user.target

	3. Start/Stop the service using the follwing commands:
		- Start: sudo systemctl start mongodb
		- Stop: sudo systemctl stop mongodb
		- Run at startup: sudo systemctl enable mongodb.service
		- Refresh service: sudo systemctl daemon-reload
		
NODE:

	1. Install 
		